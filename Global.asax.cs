﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SGS
{
    // Nota: para obtener instrucciones sobre cómo habilitar el modo clásico de IIS6 o IIS7, 
    // visite http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
           
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            // RouteTable.Routes.MapPageRoute("aaa", "aaa", "~/Clientes");
            RouteTable.Routes.MapRoute(
                 name: "aaaa",
                  url: "{controller}/{action}/{id}",
                defaults: new { controller = "IngresarHDR", action = "Index", id = 1 }
            );
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            Server.ClearError();

            this.WriteLog(exception);
        }

        public void WriteLog(Exception ex)
        {
            using (System.IO.StreamWriter sw = new System.IO.StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\logSGS_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", true))
            {
                sw.WriteLine(string.Format("{0} - {1}:{2}:{3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message, ex.StackTrace, ex.Source));
                sw.Close();
            }
        }

    }
}