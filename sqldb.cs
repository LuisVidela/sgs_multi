using System;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public class BD
{
    String strError;
    Boolean BlError; 
    String cnnStr;
    SqlCommand Cmm;
    SqlConnection cnnSQL;
    int FRet;

    public BD()
    {
        
        //cnnSQL = new SqlConnection("Data Source=MEXS1\\MEXS2012;Initial Catalog=sgsbd1;User ID=sa;Password=Pa$$w0rd;");
        cnnSQL = new SqlConnection("data source=192.168.3.51;initial catalog=RS_SGS_ARAUCO;User Id=user_sgs_arauco;Password=user_sgs_araucoprd");
        BlError = false;
        Cmm = new SqlCommand();
        Cmm.Connection = cnnSQL;
    }

      DataTable ReaderToDataTable(SqlDataReader oRd)
      {
        DataTable Tabla=new DataTable();

        int NumCampos = oRd.FieldCount - 1;
        int i=0;
        for (i=0;i<=NumCampos;i++)
            Tabla.Columns.Add(new DataColumn(oRd.GetName(i), oRd.GetFieldType(i)));
         DataRow dtRow;
        while (oRd.Read())
        {
            dtRow = Tabla.NewRow();
            for (i=0;i<=NumCampos;i++)
                dtRow[i] = oRd.GetValue(i);
            Tabla.Rows.Add(dtRow);
        }
        oRd.Close();
        Tabla.AcceptChanges();
        return Tabla;
      }

    public int ExecSQL( String strSql)
    {
        int FilasA;
        Cmm.CommandText = strSql;
        Cmm.CommandType = CommandType.Text;
        try
        {
            cnnSQL.Open();
            FilasA = (int)(Cmm.ExecuteNonQuery());
            cnnSQL.Close();
            strError = "";
            BlError = false;
            FRet = 0;
        }
        catch (Exception Ex)
            {
            strError = Ex.Message;
            FilasA = 0;
            BlError = true;
            }
        finally
        {
            if (cnnSQL.State == ConnectionState.Open)
                cnnSQL.Close();
        }
        return FilasA;
    }

    public  DataTable ExecQry(String strSql)
    {
        SqlDataReader Dr; 
        DataTable Dt=null;
        Cmm.CommandText = strSql;
        Cmm.CommandType = CommandType.Text;
        long Ret= 0;
        try
        {
            cnnSQL.Open();
            Dr = Cmm.ExecuteReader();
            if (Dr.HasRows)
            {
                Dt = ReaderToDataTable(Dr);
                FRet = Dt.Rows.Count;
            }
            else
                FRet = 0;
            cnnSQL.Close();
            strError = "";
            BlError = false;
        }
        catch (Exception Ex)
        {
            FRet = 0;
            strError = Ex.Message;
            BlError = true;
            HttpContext.Current.Response.Write(Ex.Message.ToString());
        }
        finally
        {
            if (cnnSQL.State == ConnectionState.Open)
                cnnSQL.Close();
        }
        return Dt;
    }




    }
