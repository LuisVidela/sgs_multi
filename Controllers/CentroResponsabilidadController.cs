﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;

namespace SGS.Controllers
{
    public class CentroResponsabilidadController : Controller
    {

        private sgsbdEntities ctx = new sgsbdEntities();

        public ActionResult Index()
        {
            List<centro_responsabilidad> data = ctx.centro_responsabilidad.OrderBy(c => c.centro_responsabilidad1).ToList();


            return View(data);
        }


        public ActionResult Create() {


            return View("Create");
        }


        public ActionResult Edit(int id) {

            centro_responsabilidad model = ctx.centro_responsabilidad.Where(c=>c.id_centro==id).FirstOrDefault();
            return View("Create",model);
        }

        [HttpPost]
        public ActionResult Create(centro_responsabilidad model) {

            if (ModelState.IsValid) {

                if (model.id_centro == 0)
                {
                    ctx.centro_responsabilidad.Add(model);

                }
                else {

                    var entry=ctx.Entry(model);
                    entry.State = System.Data.EntityState.Modified;
                }

                ctx.SaveChanges();
                return RedirectToAction("Index","CentroResponsabilidad");
            
            }

            return View("Create");
        }



        public ActionResult Delete(int id) {

            centro_responsabilidad model= ctx.centro_responsabilidad.Where(c=>c.id_centro==id).FirstOrDefault();
            ctx.centro_responsabilidad.Remove(model);
            ctx.SaveChanges();

            return RedirectToAction("Index","CentroResponsabilidad");
        }
    }
}
