﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models;
using System.Text;
using Microsoft.VisualBasic;
using SGS.Models.ShadowBox;
using System.IO;
using ClosedXML.Excel;

namespace SGS.Controllers
{
    public class FranquiciadosController : Controller
    {


        private FranquiciadosRepository rs;
        public ActionResult descuentosMasivos()
        {
            DescuentosMasivos model = new DescuentosMasivos();
            model.tabla = new List<rowDescuentosMasivos>();
            using (var db = new sgsbdEntities())
            {
                int id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var franquiciados = db.camiones.Where(u =>u.estado == "Activo").ToList();
                foreach (var item in franquiciados)
                {
                    if (db.franquiciado.Where(u => u.id_unidad == id_unidad && u.id_franquiciado == item.id_franquiciado).FirstOrDefault() != null)
                    {
                        rowDescuentosMasivos row = new rowDescuentosMasivos();
                        row.idCamion = item.id_camion;
                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado && u.estado=="Operativo").FirstOrDefault() != null)
                        {
                            var franq = db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault();
                            row.nombreFranquiciado = franq.razon_social;
                            row.patenteCamion = item.patente;
                            model.tabla.Add(row);
                        }
                    }
                }
            }

            return View(model.tabla.OrderBy(u => u.nombreFranquiciado).ToArray());
        }
        [HttpPost]
        public ActionResult descuentosMasivos(rowDescuentosMasivos[] model)
        {
            for (int i = 0; i < model.Count(); i++)
            {
                using (var db = new sgsbdEntities())
                {
                    int tempCamion = model[i].idCamion;
                    var camion = db.camiones.Where(u => u.id_camion == tempCamion).FirstOrDefault();
                    if (model[i].ampliroll != null && model[i].ampliroll.Replace(".", "") != "0,00")
                    {
                        var nuevaAmpi = db.descuento.Create();
                        nuevaAmpi.cuotas = 1;
                        nuevaAmpi.en_uf = true;
                        nuevaAmpi.estado_descuento = 0;
                        nuevaAmpi.lleva_iva = true;
                        nuevaAmpi.fecha = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        nuevaAmpi.id_franquiciado = camion.id_franquiciado;
                        nuevaAmpi.id_camion = camion.id_camion;
                        nuevaAmpi.id_tipo_descuento = 5;
                        nuevaAmpi.neto = Convert.ToDouble(model[i].ampliroll.Replace(".", ""));
                        nuevaAmpi.en_uf = true;
                        double iva = nuevaAmpi.neto.Value * 0.19;
                        double total = nuevaAmpi.neto.Value * 1.19;
                        nuevaAmpi.iva = iva;
                        nuevaAmpi.total = total;
                        db.descuento.Add(nuevaAmpi);
                        db.SaveChanges();
                        var nuevaDetalle = db.detalle_descuento.Create();
                        DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = db.descuento.Max(u => u.id_descuento);
                        nuevoDetalle.saldo = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagar = nuevaAmpi.neto;
                        nuevoDetalle.valor_cuota = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }
                    if (model[i].seguro != null && model[i].seguro.Replace(".", "") != "0,00")
                    {
                        var nuevaAmpi = db.descuento.Create();
                        nuevaAmpi.cuotas = 1;
                        nuevaAmpi.en_uf = true;
                        nuevaAmpi.lleva_iva = true;
                        nuevaAmpi.estado_descuento = 0;
                        nuevaAmpi.fecha = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        nuevaAmpi.id_franquiciado = camion.id_franquiciado;
                        nuevaAmpi.id_camion = camion.id_camion;
                        nuevaAmpi.id_tipo_descuento = 7;
                        nuevaAmpi.neto = Convert.ToDouble(model[i].seguro.Replace(".", ""));
                        nuevaAmpi.en_uf = true;
                        double iva = nuevaAmpi.neto.Value * 0.19;
                        double total = nuevaAmpi.neto.Value * 1.19;
                        nuevaAmpi.iva = iva;
                        nuevaAmpi.total = total;
                        db.descuento.Add(nuevaAmpi);
                        db.SaveChanges();
                        var nuevaDetalle = db.detalle_descuento.Create();
                        DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = db.descuento.Max(u => u.id_descuento);
                        nuevoDetalle.saldo = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagar = nuevaAmpi.neto;
                        nuevoDetalle.valor_cuota = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }
                    if (model[i].litrosPetroleo != null && model[i].litrosPetroleo.Replace(".", "") != "0")
                    {
                        var nuevaAmpi = db.descuento.Create();
                        var petroleo = db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault();
                        nuevaAmpi.cuotas = 1;
                        nuevaAmpi.estado_descuento = 0;
                        nuevaAmpi.lleva_iva = true;
                        nuevaAmpi.fecha = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        nuevaAmpi.id_franquiciado = camion.id_franquiciado;
                        nuevaAmpi.id_camion = camion.id_camion;
                        nuevaAmpi.id_tipo_descuento = 1;
                        nuevaAmpi.neto = Convert.ToDouble(model[i].litrosPetroleo.Replace(".", "")) * petroleo.valor;
                        nuevaAmpi.en_uf = false;
                        double iva = nuevaAmpi.neto.Value * 0.19;
                        double total = nuevaAmpi.neto.Value * 1.19;
                        nuevaAmpi.iva = iva;
                        nuevaAmpi.total = total;
                        nuevaAmpi.observacion = model[i].litrosPetroleo + " lts.";
                        db.descuento.Add(nuevaAmpi);
                        db.SaveChanges();
                        DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = db.descuento.Max(u => u.id_descuento);
                        nuevoDetalle.saldo = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagar = nuevaAmpi.neto;
                        nuevoDetalle.valor_cuota = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }
                    if (model[i].tagValor != null && model[i].tagValor.Replace(".", "") != "0")
                    {
                        var nuevaAmpi = db.descuento.Create();
                        nuevaAmpi.cuotas = 1;
                        nuevaAmpi.en_uf = false;
                        nuevaAmpi.estado_descuento = 0;
                        nuevaAmpi.lleva_iva = true;
                        nuevaAmpi.fecha = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        nuevaAmpi.id_franquiciado = camion.id_franquiciado;
                        nuevaAmpi.id_camion = camion.id_camion;
                        nuevaAmpi.id_tipo_descuento = 8;
                        nuevaAmpi.neto = Convert.ToDouble(model[i].tagValor.Replace(".", ""));
                        double iva = nuevaAmpi.neto.Value * 0.19;
                        double total = nuevaAmpi.neto.Value * 1.19;
                        nuevaAmpi.iva = iva;
                        nuevaAmpi.total = total;
                        nuevaAmpi.observacion = model[i].obsTag;
                        db.descuento.Add(nuevaAmpi);
                        db.SaveChanges();
                        DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = db.descuento.Max(u => u.id_descuento);
                        nuevoDetalle.saldo = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagar = nuevaAmpi.neto;
                        nuevoDetalle.valor_cuota = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }
                    if (model[i].repuestoValor != null && model[i].repuestoValor.Replace(".", "") != "0")
                    {
                        var nuevaAmpi = db.descuento.Create();
                        nuevaAmpi.cuotas = 1;
                        nuevaAmpi.en_uf = false;
                        nuevaAmpi.estado_descuento = 0;
                        nuevaAmpi.lleva_iva = true;
                        nuevaAmpi.fecha = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        nuevaAmpi.id_franquiciado = camion.id_franquiciado;
                        nuevaAmpi.id_camion = camion.id_camion;
                        nuevaAmpi.id_tipo_descuento = 2;
                        nuevaAmpi.neto = Convert.ToDouble(model[i].repuestoValor.Replace(".", ""));
                        double iva = nuevaAmpi.neto.Value * 0.19;
                        double total = nuevaAmpi.neto.Value * 1.19;
                        nuevaAmpi.iva = iva;
                        nuevaAmpi.total = total;
                        nuevaAmpi.numero_factura = model[i].repuestoFactura;
                        nuevaAmpi.observacion = model[i].repuestoObs;
                        db.descuento.Add(nuevaAmpi);
                        db.SaveChanges();
                        DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = db.descuento.Max(u => u.id_descuento);
                        nuevoDetalle.saldo = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagar = nuevaAmpi.neto;
                        nuevoDetalle.valor_cuota = nuevaAmpi.neto;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }

                }
            }
            return RedirectToAction("Descuentos", "Franquiciados");
        }
        [Authorize]
        public ActionResult Index()
        {
            this.rs = new FranquiciadosRepository(new sgsbdEntities());
            //List<FranquiciadosView> data = (List<FranquiciadosView>)rs.GetFranquiciadosView();
            List<FranquiciadosView> data = (List<FranquiciadosView>)rs.GetFranquiciadosView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            foreach (FranquiciadosView item in data)
            {
                string aux;
                aux = string.Format("{0:d}", item.fecha_contrato);
                if (string.IsNullOrEmpty(aux))
                {
                    item.fecha_contrato = null;
                }
                else
                {
                    item.fecha_contrato = Convert.ToDateTime(aux); // DateTime.Parse(aux.Substring(0, 9));
                }


            }
            using (var db = new sgsbdEntities())
            {
                var hrE = db.hoja_ruta.ToList();
                foreach (var hr in hrE)
                {
                    var hrD = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == hr.id_hoja_ruta).ToList();
                    int i = 0;
                    var p1 = hrD.Where(u => u.id_punto_servicio == 139).FirstOrDefault();
                    var p2 = hrD.Where(u => u.id_punto_servicio == 140).FirstOrDefault();
                    var p3 = hrD.Where(u => u.id_punto_servicio == 147).FirstOrDefault();
                    var p4 = hrD.Where(u => u.id_punto_servicio == 151).FirstOrDefault();
                    var p5 = hrD.Where(u => u.id_punto_servicio == 149).FirstOrDefault();
                    var p6 = hrD.Where(u => u.id_punto_servicio == 142).FirstOrDefault();
                    var p7 = hrD.Where(u => u.id_punto_servicio == 148).FirstOrDefault();
                    var p8 = hrD.Where(u => u.id_punto_servicio == 146).FirstOrDefault();
                    var p9 = hrD.Where(u => u.id_punto_servicio == 152).FirstOrDefault();
                    var p10 = hrD.Where(u => u.id_punto_servicio == 141).FirstOrDefault();
                    var p11 = hrD.Where(u => u.id_punto_servicio == 150).FirstOrDefault();
                    var p12 = hrD.Where(u => u.id_punto_servicio == 136).FirstOrDefault();
                    var p13 = hrD.Where(u => u.id_punto_servicio == 135).FirstOrDefault();
                    var p17 = hrD.Where(u => u.id_punto_servicio == 144).FirstOrDefault();

                    var p14 = hrD.Where(u => u.id_punto_servicio == 138).FirstOrDefault();
                    var p15 = hrD.Where(u => u.id_punto_servicio == 143).FirstOrDefault();
                    var p16 = hrD.Where(u => u.id_punto_servicio == 393).FirstOrDefault();
                    var p18 = hrD.Where(u => u.id_punto_servicio == 394).FirstOrDefault();
                    var p19 = hrD.Where(u => u.id_punto_servicio == 395).FirstOrDefault();
                    if (p1 != null) i++;
                    if (p2 != null) i++;
                    if (p3 != null) i++;
                    if (p4 != null) i++;
                    if (p5 != null) i++;
                    if (p6 != null) i++;
                    if (p7 != null) i++;
                    if (p9 != null) i++;
                    if (p10 != null) i++;
                    if (p11 != null) i++;
                    if (p12 != null) i++;
                    if (p13 != null) i++;
                    if (p14 != null) i++;
                    if (p15 != null) i++;
                    if (p16 != null) i++;
                    if (p17 != null) i++;
                    if (p18 != null) i++;
                    if (p19 != null) i++;
                    if (i >= 8)
                    {
                        hr.ruta_modelo = 54;
                        db.SaveChanges();
                    }
                }
            }


            return View(data);
        }
        public ActionResult Excel()
        {

            this.rs = new FranquiciadosRepository(new sgsbdEntities());
            List<FranquiciadosView> data = (List<FranquiciadosView>)rs.GetFranquiciadosView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());


            return View(data);
        }
        [Authorize]
        public ActionResult view(int id)
        {
            FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());

            franquiciado c = rs.GetFranquiciadoByID(id);
            return View(c);

        }
        [Authorize]
        public ActionResult descuentosExcel(int tipo, int idFranquiciado)
        {
            DescuentosView model = new DescuentosView();
            using (var db = new sgsbdEntities())
            {
                var descuentos = db.descuento.ToList();
                if (tipo == 1) descuentos = db.descuento.Where(u => u.estado_descuento == 1).ToList();
                if (tipo == 0) descuentos = db.descuento.Where(u => u.estado_descuento == 0).ToList();
                if (idFranquiciado != -1)
                {
                    descuentos = descuentos.Where(u => u.id_franquiciado.Value == idFranquiciado).ToList();
                }
                if (descuentos != null)
                {
                    int id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                    model.encabezado = new List<DescuentosViewTablaEncabezado>();
                    foreach (descuento item in descuentos)
                    {
                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado && u.id_unidad == id_unidad).FirstOrDefault() != null)
                        {
                            var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                            var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == camion.id_franquiciado).FirstOrDefault();
                            DescuentosViewTablaEncabezado encabezadoNuevo = new DescuentosViewTablaEncabezado();
                            encabezadoNuevo.editable = true;
                            encabezadoNuevo.pateneteCamion = camion.patente;
                            encabezadoNuevo.razonSocial = franquiciado.razon_social;
                            if (item.estado_descuento == 1) encabezadoNuevo.estado_descuento = "Cancelado";
                            else encabezadoNuevo.estado_descuento = "Vigente";
                            encabezadoNuevo.fecha = item.fecha.Value;
                            encabezadoNuevo.id_descuento = item.id_descuento;
                            encabezadoNuevo.id_franquiciado = item.id_franquiciado.Value;
                            encabezadoNuevo.iva = item.iva.Value;
                            encabezadoNuevo.neto = item.neto.Value;
                            encabezadoNuevo.oc = item.oc;
                            encabezadoNuevo.factura = item.numero_factura;
                            encabezadoNuevo.total = item.total.Value;
                            encabezadoNuevo.cuotasPagadas = db.detalle_descuento.Where(u => u.id_descuento == item.id_descuento && u.estado == 1).ToList().Count;
                            encabezadoNuevo.concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento.Value).FirstOrDefault().nombre;
                            encabezadoNuevo.cuotas = item.cuotas.Value;
                            var detalle = db.detalle_descuento.Where(u => u.id_descuento == item.id_descuento).ToList();
                            foreach (detalle_descuento itemDetalle in detalle)
                            {
                                if (itemDetalle.estado == 1) encabezadoNuevo.editable = false;
                            }
                            encabezadoNuevo.id_tipo_descuento = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento.Value).FirstOrDefault().nombre;
                            model.encabezado.Add(encabezadoNuevo);
                        }
                    }
                }
            }
            return View(model);
        }
        [Authorize]
        [HttpGet]
        public ActionResult descuentos()
        {
            DescuentosView model = new DescuentosView();
            using (var db = new sgsbdEntities())
            {
                //var descuentos = db.descuento.ToList();
                var descuentos = db.descuento.Where(u => u.estado_descuento == 0).ToList();
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var petroleo = db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month && u.id_unidad_negocio == unidad).FirstOrDefault();
                if (petroleo != null)
                {
                    model.agregarMasivo = true;
                }
                else
                {
                    model.agregarMasivo = false;
                }
                if (descuentos != null)
                {

                    model.encabezado = new List<DescuentosViewTablaEncabezado>();
                    foreach (descuento item in descuentos)
                    {
                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado && u.id_unidad == unidad).FirstOrDefault() != null)
                        {
                            var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                            var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == camion.id_franquiciado).FirstOrDefault();
                            DescuentosViewTablaEncabezado encabezadoNuevo = new DescuentosViewTablaEncabezado();
                            encabezadoNuevo.editable = true;
                            encabezadoNuevo.pateneteCamion = camion.patente;
                            encabezadoNuevo.razonSocial = franquiciado.razon_social;
                            if (item.estado_descuento == 1) encabezadoNuevo.estado_descuento = "Cancelado";
                            else encabezadoNuevo.estado_descuento = "Vigente";
                            encabezadoNuevo.fecha = item.fecha.Value;
                            encabezadoNuevo.id_descuento = item.id_descuento;
                            encabezadoNuevo.id_franquiciado = item.id_franquiciado.Value;
                            encabezadoNuevo.iva = item.iva.Value;
                            encabezadoNuevo.neto = item.neto.Value;
                            encabezadoNuevo.enUF = item.en_uf;
                            encabezadoNuevo.total = item.total.Value;
                            encabezadoNuevo.concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento.Value).FirstOrDefault().nombre;
                            encabezadoNuevo.cuotas = item.cuotas.Value;
                            var detalle = db.detalle_descuento.Where(u => u.id_descuento == item.id_descuento).ToList();
                            foreach (detalle_descuento itemDetalle in detalle)
                            {
                                if (itemDetalle.estado == 1) encabezadoNuevo.editable = false;
                            }
                            encabezadoNuevo.id_tipo_descuento = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento.Value).FirstOrDefault().nombre;
                            model.encabezado.Add(encabezadoNuevo);
                        }
                    }
                }
            }
            llenaEstados(model);
            llenaFranquiciadosFiltro(model);
            return View(model);

        }
        [Authorize]
        [HttpPost]
        public ActionResult descuentos(DescuentosView model)
        {
            using (var db = new sgsbdEntities())
            {
                var descuentos = db.descuento.ToList();
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var petroleo = db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month && u.id_unidad_negocio == unidad).FirstOrDefault();
                if (petroleo != null)
                {
                    model.agregarMasivo = true;
                }
                else
                {
                    model.agregarMasivo = false;
                }
                if (model.selectListEstados.estadoSelect == 1) descuentos = db.descuento.Where(u => u.estado_descuento == 1).ToList();
                if (model.selectListEstados.estadoSelect == 0) descuentos = db.descuento.Where(u => u.estado_descuento == 0).ToList();
                llenaEstados(model);
                if (model.selecListFranquiciados.franquiciadoSelect != null)
                {
                    descuentos = descuentos.Where(u => u.id_franquiciado.Value == model.selecListFranquiciados.franquiciadoSelect.Value).ToList();
                }
                llenaFranquiciadosFiltro(model);
                if (descuentos != null)
                {

                    model.encabezado = new List<DescuentosViewTablaEncabezado>();
                    foreach (descuento item in descuentos)
                    {

                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado && u.id_unidad == unidad).FirstOrDefault() != null)
                        {
                            var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                            var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == camion.id_franquiciado).FirstOrDefault();
                            DescuentosViewTablaEncabezado encabezadoNuevo = new DescuentosViewTablaEncabezado();
                            encabezadoNuevo.editable = true;
                            encabezadoNuevo.pateneteCamion = camion.patente;
                            encabezadoNuevo.razonSocial = franquiciado.razon_social;
                            if (item.estado_descuento == 1) encabezadoNuevo.estado_descuento = "Cancelado";
                            else encabezadoNuevo.estado_descuento = "Vigente";
                            encabezadoNuevo.fecha = item.fecha.Value;
                            encabezadoNuevo.id_descuento = item.id_descuento;
                            encabezadoNuevo.id_franquiciado = item.id_franquiciado.Value;
                            encabezadoNuevo.iva = item.iva.Value;
                            encabezadoNuevo.enUF = item.en_uf;
                            encabezadoNuevo.neto = item.neto.Value;
                            encabezadoNuevo.total = item.total.Value;
                            encabezadoNuevo.concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento).FirstOrDefault().nombre;
                            encabezadoNuevo.cuotas = item.cuotas.Value;
                            var detalle = db.detalle_descuento.Where(u => u.id_descuento == item.id_descuento).ToList();
                            foreach (detalle_descuento itemDetalle in detalle)
                            {
                                if (itemDetalle.valor_pagado != 0) encabezadoNuevo.editable = false;
                            }
                            encabezadoNuevo.id_tipo_descuento = db.tipo_descuento.Where(u => u.id_tipo_descuento == item.id_tipo_descuento.Value).FirstOrDefault().nombre;
                            model.encabezado.Add(encabezadoNuevo);
                        }
                    }

                }
            }
            return View(model);
        }
        [Authorize]
        [HttpGet]
        public ActionResult crearLeasing(int idLeasing)
        {
            CrearLeasingView model = new CrearLeasingView();
            model.idLeasing = idLeasing;
            model.fechaPrimeraCuota = new selectListLeasing();
            if (idLeasing == -1)
            {
                model.idFranquiciado = -1;
                using (var db = new sgsbdEntities())
                {
                    var uf = db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault();
                    model.valorUfDia = uf.valor_uf.ToString("N2");
                }
            }
            else
            {
                using (var db = new sgsbdEntities())
                {
                    var leasing = db.leasing.Where(u => u.id_leasing == idLeasing).FirstOrDefault();
                    int año = leasing.fecha.Value.Year;
                    int mes = leasing.fecha.Value.Month;
                    var uf = db.uf.Where(u => u.mes == mes && u.año == año).FirstOrDefault();
                    var camion = db.camiones.Where(u => u.id_camion == leasing.id_camion.Value).FirstOrDefault();
                    var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == camion.id_franquiciado).FirstOrDefault();
                    model.franquiciadosSelectList = new selectListFranquiciados();
                    model.franquiciadosSelectList.franquiciadoSelect = franquiciado.id_franquiciado;
                    model.camionesSelectList = new selectListCamiones();
                    model.camionesSelectList.camionSelect = camion.id_camion;
                    model.interes = leasing.interes.Value.ToString("N2"); //Convert.ToInt32(leasing.interes.Value);
                    model.cuotas = leasing.cuotas;
                    var fecha = leasing.fecha.Value.Day;
                    model.fechaPrimeraCuota = new selectListLeasing();
                    DateTime fecha0 = Convert.ToDateTime(fecha + "-" + DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString());
                    DateTime fecha4 = Convert.ToDateTime(fecha + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                    DateTime fecha5 = Convert.ToDateTime(fecha + "-" + DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString());
                    DateTime fecha6 = Convert.ToDateTime(fecha + "-" + DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString());
                    DateTime fecha1 = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                    DateTime fecha2 = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString());
                    DateTime fecha3 = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString());
                    if (leasing.fecha.Value.Date == fecha0) model.fechaPrimeraCuota.leasingSelect = 0;
                    if (leasing.fecha.Value == fecha1 || leasing.fecha.Value.Date == fecha4) model.fechaPrimeraCuota.leasingSelect = 1;
                    if (leasing.fecha.Value == fecha2 || leasing.fecha.Value.Date == fecha5) model.fechaPrimeraCuota.leasingSelect = 2;
                    if (leasing.fecha.Value == fecha3 || leasing.fecha.Value.Date == fecha6) model.fechaPrimeraCuota.leasingSelect = 3;
                    llenaConceptos(model);
                    model.iva = leasing.iva.Value.ToString("N0");
                    model.neto = leasing.neto.Value.ToString("N0");
                    model.pie = (leasing.pie_uf.Value * leasing.valor_uf.Value).ToString("N0");
                    model.pieUF = leasing.pie_uf.Value.ToString("N2");
                    model.total = leasing.total.Value.ToString("N0");
                    model.totalUF = leasing.total_uf.Value.ToString("N2");
                    model.valorUfDia = leasing.valor_uf.Value.ToString("N2");
                    var cuotaFinal = db.detalle_leasing.Where(u => u.id_leasing == leasing.id_leasing).OrderByDescending(u => u.cuota).First();

                    if (cuotaFinal != null)
                    {
                        model.cuotaFinal = Convert.ToDouble(cuotaFinal.cuota_neta).ToString("N2").Replace(".", ",");
                    }
                    else
                    {
                        model.cuotaFinal = "";
                    }
                    llenaCamionesLeasing(model);
                }
            }
            llenaConceptos(model);
            llenaFranquiciadosLeasing(model);
            model.ok = 2000;
            return View(model);
        }
        [Authorize]
        [HttpPost]
        public ActionResult crearLeasing(CrearLeasingView model)
        {
            if (model.idLeasing == -1)
            {
                using (var db = new sgsbdEntities())
                {
                    llenaFranquiciadosLeasing(model);
                    if (model.camionesSelectList == null || model.idFranquiciado != model.franquiciadosSelectList.franquiciadoSelect)
                    {
                        model.fechaPrimeraCuota = new selectListLeasing();
                        model.fechaPrimeraCuota.leasingSelect = 1;
                        llenaConceptos(model);
                        llenaCamionesLeasing(model);
                        llenaFranquiciadosLeasing(model);
                        model.idFranquiciado = model.franquiciadosSelectList.franquiciadoSelect;
                        var uf = db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault();
                        model.valorUfDia = uf.valor_uf.ToString("N2");
                        return View(model);
                    }
                    else
                    {
                        var nuevoLeasing = db.leasing.Create();
                        string fecha1 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                        string fecha2 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
                        string fecha3 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
                        if (model.fechaPrimeraCuota.leasingSelect == 1) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha1);
                        if (model.fechaPrimeraCuota.leasingSelect == 2) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha2);
                        if (model.fechaPrimeraCuota.leasingSelect == 3) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha3);
                        model.camion = db.camiones.Where(u => u.id_camion == model.camionesSelectList.camionSelect).FirstOrDefault();
                        nuevoLeasing.cuotas = model.cuotas +1;
                        //nuevoLeasing.fecha = DateTime.Now;
                        nuevoLeasing.id_franquiciados = model.camion.id_franquiciado;
                        nuevoLeasing.id_camion = model.camion.id_camion;
                        double valorInteres = double.Parse(model.interes.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                        nuevoLeasing.interes = valorInteres;//model.interes;
                        nuevoLeasing.neto = Convert.ToInt32(model.neto.Replace(".", ""));
                        int iva = Convert.ToInt32(Convert.ToInt64(nuevoLeasing.neto.Value) * 0.19);
                        int total = Convert.ToInt32(Convert.ToInt64(nuevoLeasing.neto.Value) * 1.19);
                        var uf = db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault();
                        model.valorUfDia = uf.valor_uf.ToString("N2");
                        string tempo = model.valorUfDia.Replace(".", "").Replace(",", ".");
                        double valorUF = double.Parse(tempo, System.Globalization.CultureInfo.InvariantCulture);
                        nuevoLeasing.pie_uf = Convert.ToInt64(model.pie.Replace(".", "")) / valorUF;
                        nuevoLeasing.total = total - Convert.ToInt32(model.pie.Replace(".", ""));
                        nuevoLeasing.total_uf = (Convert.ToInt64(total) / valorUF) - nuevoLeasing.pie_uf;
                        nuevoLeasing.valor_uf = valorUF;
                        nuevoLeasing.iva = iva;
                        nuevoLeasing.fecha_creacion = DateTime.Now;
                        int cuenta = db.leasing.Count();
                        if (cuenta == 0) nuevoLeasing.id_leasing = 1;
                        else nuevoLeasing.id_leasing = db.leasing.Max(u => u.id_leasing) + 1;
                        db.leasing.Add(nuevoLeasing);
                        db.SaveChanges();
                        int id_leasing = db.leasing.Max(u => u.id_leasing);
                        double tasaMensual = (nuevoLeasing.interes.Value / 12) / 100;
                        double saldoTemporal = 0;
                        double iTemporal = 0;
                        var mes = 0;
                        for (int i = 1; i <= nuevoLeasing.cuotas; i++)
                        {
                            //DateTime fechaCuota = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(i).Month.ToString() + "-" + DateTime.Now.AddMonths(i).Year.ToString());
                            DateTime fechaCuota = Convert.ToDateTime("01-" + nuevoLeasing.fecha.Value.AddMonths(mes).Month.ToString() + "-" + nuevoLeasing.fecha.Value.AddMonths(mes).Year.ToString());
                            detalle_leasing detalle = new detalle_leasing();
                            if (i == 1)
                            {
                                detalle.id_leasing = id_leasing;
                                detalle.cuota = i;
                                detalle.cuota_bruta = Financial.Pmt(tasaMensual, (nuevoLeasing.cuotas - 1), -nuevoLeasing.total_uf.Value);
                                detalle.cuota_neta = detalle.cuota_bruta / 1.19;
                                detalle.i = tasaMensual * nuevoLeasing.total_uf;
                                detalle.k = detalle.cuota_bruta - detalle.i;
                                detalle.saldo = nuevoLeasing.total_uf - detalle.k;
                                detalle.saldoCuota = Math.Round(detalle.cuota_neta.Value, 2, MidpointRounding.AwayFromZero);
                                detalle.valor_pagar = Math.Round(detalle.cuota_neta.Value, 2, MidpointRounding.AwayFromZero);
                                detalle.valor_pagado = 0;
                                saldoTemporal = detalle.saldo.Value;
                                iTemporal = detalle.i.Value;
                                detalle.fecha_pago = fechaCuota.Date;
                                db.detalle_leasing.Add(detalle);
                                db.SaveChanges();
                            }
                            else
                            {
                                detalle.id_leasing = id_leasing;
                                detalle.cuota = i;
                                detalle.cuota_bruta = Financial.Pmt(tasaMensual, (nuevoLeasing.cuotas - 1), -nuevoLeasing.total_uf.Value);
                                detalle.cuota_neta = detalle.cuota_bruta / 1.19;
                                detalle.i = tasaMensual * saldoTemporal;
                                detalle.k = detalle.cuota_bruta - detalle.i;
                                detalle.saldo = saldoTemporal - detalle.k;
                                saldoTemporal = detalle.saldo.Value;
                                detalle.saldoCuota = detalle.cuota_neta;
                                detalle.valor_pagar = detalle.cuota_neta;
                                detalle.valor_pagado = 0;
                                iTemporal = detalle.i.Value;
                                detalle.fecha_pago = fechaCuota.Date;
                                db.detalle_leasing.Add(detalle);
                                db.SaveChanges();
                            }
                            mes++;
                        }
                    }
                }
                model.ok = 1;
                model.neto = "0";
                model.cuotas = 1;
                model.interes = "1";
                model.pie = "0";
                return RedirectToAction("leasing", "Franquiciados");
            }
            else
            {
                using (var db = new sgsbdEntities())
                {
                    var nuevoLeasing = db.leasing.Where(u => u.id_leasing == model.idLeasing).FirstOrDefault();
                    double valorInteres = double.Parse(model.interes.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                    nuevoLeasing.interes = valorInteres; //model.interes;
                    nuevoLeasing.neto = Convert.ToInt32(model.neto.Replace(".", ""));
                    int iva = Convert.ToInt32(Convert.ToInt64(nuevoLeasing.neto.Value) * 0.19);
                    int total = Convert.ToInt32(Convert.ToInt64(nuevoLeasing.neto.Value) * 1.19);
                    nuevoLeasing.pie_uf = Convert.ToInt64(model.pie.Replace(".", "")) / nuevoLeasing.valor_uf.Value;
                    nuevoLeasing.total = total - Convert.ToInt32(model.pie.Replace(".", ""));
                    nuevoLeasing.total_uf = (Convert.ToInt64(total) / nuevoLeasing.valor_uf.Value) - nuevoLeasing.pie_uf;
                    string fecha0 = DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString();
                    string fecha1 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                    string fecha2 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
                    string fecha3 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
                    if (model.fechaPrimeraCuota.leasingSelect == 0) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha0);
                    if (model.fechaPrimeraCuota.leasingSelect == 1) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha1);
                    if (model.fechaPrimeraCuota.leasingSelect == 2) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha2);
                    if (model.fechaPrimeraCuota.leasingSelect == 3) nuevoLeasing.fecha = Convert.ToDateTime("01-" + fecha3);
                    nuevoLeasing.iva = iva;
                    db.SaveChanges();
                    double tasaMensual = (nuevoLeasing.interes.Value / 12) / 100;
                    double saldoTemporal = 0;
                    double iTemporal = 0;
                    var detalles = db.detalle_leasing.Where(u => u.id_leasing == nuevoLeasing.id_leasing).ToList();
                    int i = 1;
                    int mes = 0;
                    foreach (var detalle in detalles)
                    {
                        DateTime fechaCuota = Convert.ToDateTime("01-" + nuevoLeasing.fecha.Value.AddMonths(mes).Month.ToString() + "-" + nuevoLeasing.fecha.Value.AddMonths(mes).Year.ToString());
                        if (i == 1)
                        {
                            detalle.cuota_bruta = Financial.Pmt(tasaMensual, (nuevoLeasing.cuotas -1), -nuevoLeasing.total_uf.Value);
                            detalle.cuota_neta = detalle.cuota_bruta / 1.19;
                            detalle.i = tasaMensual * nuevoLeasing.total_uf;
                            detalle.k = detalle.cuota_bruta - detalle.i;
                            detalle.saldo = nuevoLeasing.total_uf - detalle.k;
                            detalle.saldoCuota = Math.Round(detalle.cuota_neta.Value, 2, MidpointRounding.AwayFromZero);
                            detalle.valor_pagar = Math.Round(detalle.cuota_neta.Value, 2, MidpointRounding.AwayFromZero);
                            detalle.valor_pagado = 0;
                            detalle.fecha_pago = fechaCuota.Date;
                            saldoTemporal = detalle.saldo.Value;
                            db.SaveChanges();
                        }
                        else
                        {
                            detalle.cuota_bruta = Financial.Pmt(tasaMensual, (nuevoLeasing.cuotas - 1), -nuevoLeasing.total_uf.Value);
                            detalle.cuota_neta = detalle.cuota_bruta / 1.19;
                            detalle.i = tasaMensual * saldoTemporal;
                            detalle.k = detalle.cuota_bruta - detalle.i;
                            detalle.saldo = saldoTemporal - detalle.k;
                            saldoTemporal = detalle.saldo.Value;
                            detalle.saldoCuota = detalle.cuota_neta;
                            detalle.valor_pagar = detalle.cuota_neta;
                            detalle.fecha_pago = fechaCuota.Date;
                            detalle.valor_pagado = 0;
                            db.SaveChanges();
                        }
                        i++;
                        mes++;
                    }
                }
                model.ok = 1;
                model.neto = "0";
                model.cuotas = 1;
                model.interes = "1";
                model.pie = "0";
                return RedirectToAction("leasing", "Franquiciados");
            }

        }
        [Authorize]
        public ActionResult editarLeasing(int idLeasing)
        {
            return View();
        }
        [Authorize]
        public ActionResult crearDescuento()
        {
            CrearDescuentoView model = new CrearDescuentoView();
            model.idFranquiciado = -1;
            model.llevaIVA = true;
            llenaFranquiciados(model);
            model.enUF = false;
            //model.idCamion = id;
            //using( var db = new sgsbdEntities())
            //{
            //    model.camion = db.camiones.Where(u => u.id_camion == model.idCamion).FirstOrDefault();
            //    model.cuotas = 1;
            //}
            //llenaConceptos(model);
            model.ok = 0;
            return View(model);
        }
        [HttpPost]
        public ActionResult crearDescuento(CrearDescuentoView model)
        {
            using (var db = new sgsbdEntities())
            {
                llenaFranquiciados(model);
                if (model.camionesSelectList == null || model.idFranquiciado != model.franquiciadosSelectList.franquiciadoSelect)
                {
                    model.fechaPrimeraCuota = new selectListCamiones();
                    model.fechaPrimeraCuota.camionSelect = 1;
                    llenaCamiones(model);
                    llenaConceptos(model);
                    llenaFranquiciados(model);
                    model.enUF = false;
                    model.llevaIVA = true;
                    model.idFranquiciado = model.franquiciadosSelectList.franquiciadoSelect;
                    return View(model);
                }
                else
                {
                    model.camion = db.camiones.Where(u => u.id_camion == model.camionesSelectList.camionSelect).FirstOrDefault();
                    var nuevoEncabezado = db.descuento.Create();
                    nuevoEncabezado.id_camion = model.camion.id_camion;
                    string fecha1 = DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString();
                    string fecha2 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                    string fecha3 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
                    string fecha4 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
                    if (model.fechaPrimeraCuota.camionSelect == 1) nuevoEncabezado.fecha = Convert.ToDateTime("01-" + fecha1);
                    if (model.fechaPrimeraCuota.camionSelect == 2) nuevoEncabezado.fecha = Convert.ToDateTime("01-" + fecha2);
                    if (model.fechaPrimeraCuota.camionSelect == 3) nuevoEncabezado.fecha = Convert.ToDateTime("01-" + fecha3);
                    if (model.fechaPrimeraCuota.camionSelect == 4) nuevoEncabezado.fecha = Convert.ToDateTime("01-" + fecha4);
                    nuevoEncabezado.estado_descuento = 0;
                    nuevoEncabezado.cuotas = model.cuotas;
                    nuevoEncabezado.oc = model.oc;
                    nuevoEncabezado.en_uf = model.enUF;
                    nuevoEncabezado.numero_factura = model.factura;
                    nuevoEncabezado.id_franquiciado = model.camion.id_franquiciado;
                    nuevoEncabezado.id_tipo_descuento = model.conceptoSelectList.conceptoSelect;
                    if (nuevoEncabezado.id_tipo_descuento == 7)
                    {
                        model.enUF = true;
                        nuevoEncabezado.en_uf = true;

                    }
                    nuevoEncabezado.neto = (model.enUF == false) ? Convert.ToInt32(model.neto.Replace(".", "")) : Convert.ToDouble(model.neto.Replace(".", ""));
                    nuevoEncabezado.observacion = model.observacion;
                    //nuevoEncabezado.fecha_creacion = DateTime.Now;
                    double iva = nuevoEncabezado.neto.Value * 0.19;
                    double total = nuevoEncabezado.neto.Value * 1.19;
                    if (model.llevaIVA == false)
                    {
                        nuevoEncabezado.iva = 0;
                        nuevoEncabezado.total = nuevoEncabezado.neto;
                    }
                    else
                    {
                        nuevoEncabezado.iva = iva;
                        nuevoEncabezado.total = total;
                    }
                    nuevoEncabezado.lleva_iva = model.llevaIVA;
                    nuevoEncabezado.fecha_creacion = DateTime.Now;
                    db.descuento.Add(nuevoEncabezado);
                    db.SaveChanges();
                    nuevoEncabezado.id_descuento = db.descuento.Max(u => u.id_descuento);
                    double cuota = nuevoEncabezado.neto.Value / nuevoEncabezado.cuotas.Value;
                    for (int i = 0; i < nuevoEncabezado.cuotas; i++)
                    {
                        DateTime fechaCuota = Convert.ToDateTime("01-" + nuevoEncabezado.fecha.Value.AddMonths(i).Month.ToString() + "-" + nuevoEncabezado.fecha.Value.AddMonths(i).Year.ToString());
                        var nuevoDetalle = db.detalle_descuento.Create();
                        nuevoDetalle.cuota = i + 1;
                        nuevoDetalle.estado = 0;
                        nuevoDetalle.fecha = fechaCuota.Date;
                        nuevoDetalle.id_descuento = nuevoEncabezado.id_descuento;
                        nuevoDetalle.saldo = cuota;
                        nuevoDetalle.valor_pagar = cuota;
                        nuevoDetalle.valor_cuota = cuota;
                        nuevoDetalle.valor_pagado = 0;
                        db.detalle_descuento.Add(nuevoDetalle);
                        db.SaveChanges();
                    }
                    model.ok = 1;
                    model.neto = "0";
                    model.cuotas = 0;
                    llenaConceptos(model);
                    llenaCamiones(model);
                    llenaFranquiciados(model);
                }
            }
            return RedirectToAction("descuentos", "Franquiciados");
        }
        public ActionResult detalleLeasingExcel(int idLeasing)
        {
            detalleLeasingView model = new detalleLeasingView();
            using (var db = new sgsbdEntities())
            {
                model.leasing = db.leasing.Where(u => u.id_leasing == idLeasing).FirstOrDefault();
                model.camion = db.camiones.Where(u => u.id_camion == model.leasing.id_camion).FirstOrDefault();
                var detalles = db.detalle_leasing.Where(u => u.id_leasing == model.leasing.id_leasing).ToList();
                model.detalle = new List<leasingTablaDetalle>();
                foreach (detalle_leasing item in detalles)
                {
                    leasingTablaDetalle nuevoDetalle = new leasingTablaDetalle();
                    nuevoDetalle.cuota = item.cuota.Value;
                    nuevoDetalle.idLeasing = item.id_leasing.Value;
                    nuevoDetalle.idDetalle = item.id_detalle_leasing;
                    if (item.fecha_pagado == null) nuevoDetalle.estado = "Pendiente";
                    if (item.fecha_pagado != null) nuevoDetalle.estado = "Pagada";
                    nuevoDetalle.fechaCuota = item.fecha_pago.Value.ToShortDateString();
                    if (item.saldoCuota != null) nuevoDetalle.saldo = item.saldoCuota.Value.ToString("N2");
                    else nuevoDetalle.saldo = item.cuota_bruta.Value.ToString("N2");
                    nuevoDetalle.valorPagado = item.valor_pagado.Value.ToString("N2");
                    nuevoDetalle.cuotaBruta = item.cuota_bruta.Value.ToString("N2");
                    nuevoDetalle.cuotaNeta = item.cuota_neta.Value.ToString("N2");
                    nuevoDetalle.valorPagar = item.valor_pagar.Value.ToString("N2");
                    nuevoDetalle.editable = false;
                    if (DateTime.Now.Year > item.fecha_pago.Value.Year && item.saldoCuota.Value != 0) nuevoDetalle.editable = true;
                    if (DateTime.Now.Year == item.fecha_pago.Value.Year && DateTime.Now.Month >= item.fecha_pago.Value.Month && item.saldoCuota.Value != 0) nuevoDetalle.editable = true;
                    model.detalle.Add(nuevoDetalle);
                }
            }
            return View(model);
        }
        public ActionResult detalleLeasing(int idLeasing)
        {
            detalleLeasingView model = new detalleLeasingView();
            using (var db = new sgsbdEntities())
            {
                model.leasing = db.leasing.Where(u => u.id_leasing == idLeasing).FirstOrDefault();
                model.camion = db.camiones.Where(u => u.id_camion == model.leasing.id_camion).FirstOrDefault();
                var detalles = db.detalle_leasing.Where(u => u.id_leasing == model.leasing.id_leasing).ToList();
                model.detalle = new List<leasingTablaDetalle>();
                foreach (detalle_leasing item in detalles)
                {
                    leasingTablaDetalle nuevoDetalle = new leasingTablaDetalle();
                    nuevoDetalle.cuota = item.cuota.Value;
                    nuevoDetalle.idLeasing = item.id_leasing.Value;
                    nuevoDetalle.idDetalle = item.id_detalle_leasing;
                    if (item.fecha_pagado == null) nuevoDetalle.estado = "Pendiente";
                    if (item.fecha_pagado != null) nuevoDetalle.estado = "Pagada";
                    nuevoDetalle.fechaCuota = item.fecha_pago.Value.ToShortDateString();
                    if (item.saldoCuota != null) nuevoDetalle.saldo = item.saldoCuota.Value.ToString("N2");
                    else nuevoDetalle.saldo = item.cuota_bruta.Value.ToString("N2");
                    nuevoDetalle.valorPagado = item.valor_pagado.Value.ToString("N2");
                    nuevoDetalle.cuotaBruta = item.cuota_bruta.Value.ToString("N2");
                    nuevoDetalle.cuotaNeta = item.cuota_neta.Value.ToString("N2");
                    nuevoDetalle.valorPagar = item.valor_pagar.Value.ToString("N2");
                    nuevoDetalle.editable = false;
                    if (DateTime.Now.Year > item.fecha_pago.Value.Year && item.saldoCuota.Value != 0) nuevoDetalle.editable = true;
                    if (DateTime.Now.Year == item.fecha_pago.Value.Year && DateTime.Now.Month >= item.fecha_pago.Value.Month && item.saldoCuota.Value != 0) nuevoDetalle.editable = true;
                    if (item.fecha_pagado != null) nuevoDetalle.editable = false;
                    model.detalle.Add(nuevoDetalle);
                }
            }
            return View(model);
        }
        public ActionResult detalleDescuentoExcel(int idDescuento)
        {
            detalleDescuentoView model = new detalleDescuentoView();
            using (var db = new sgsbdEntities())
            {
                model.descuento = db.descuento.Where(u => u.id_descuento == idDescuento).FirstOrDefault();
                model.camion = db.camiones.Where(u => u.id_camion == model.descuento.id_camion).FirstOrDefault();
                var detalles = db.detalle_descuento.Where(u => u.id_descuento == model.descuento.id_descuento).ToList();
                model.detalle = new List<DescuentosViewTablaDetalle>();
                foreach (detalle_descuento item in detalles)
                {
                    DescuentosViewTablaDetalle nuevoDetalle = new DescuentosViewTablaDetalle();
                    nuevoDetalle.cuota = item.cuota.Value;
                    nuevoDetalle.idDescuento = item.id_descuento.Value;
                    nuevoDetalle.idDetalle = item.id_detalle_descuento;
                    if (item.estado.Value == 0) nuevoDetalle.estado = "Pendiente";
                    if (item.estado.Value == 1) nuevoDetalle.estado = "Pagada";
                    nuevoDetalle.fecha = item.fecha.Value;
                    nuevoDetalle.saldo = item.saldo.Value.ToString("N0");
                    nuevoDetalle.valorCuota = item.valor_cuota.Value.ToString("N0");
                    nuevoDetalle.valorPagado = item.valor_pagado.Value.ToString("N0");
                    nuevoDetalle.valorPagar = item.valor_pagar.Value.ToString("N0");
                    if (DateTime.Now.Year > item.fecha.Value.Year && item.saldo.Value != 0) nuevoDetalle.editable = true;
                    if (DateTime.Now.Year == item.fecha.Value.Year && DateTime.Now.Month >= item.fecha.Value.Month && item.saldo.Value != 0) nuevoDetalle.editable = true;
                    if (item.estado == 1) nuevoDetalle.editable = false;
                    model.detalle.Add(nuevoDetalle);
                }
            }
            return View(model);
        }
        public ActionResult detalleDescuento(int idDescuento)
        {
            detalleDescuentoView model = new detalleDescuentoView();
            using (var db = new sgsbdEntities())
            {
                model.descuento = db.descuento.Where(u => u.id_descuento == idDescuento).FirstOrDefault();
                model.camion = db.camiones.Where(u => u.id_camion == model.descuento.id_camion).FirstOrDefault();
                var detalles = db.detalle_descuento.Where(u => u.id_descuento == model.descuento.id_descuento).ToList();
                model.detalle = new List<DescuentosViewTablaDetalle>();
                foreach (detalle_descuento item in detalles)
                {
                    DescuentosViewTablaDetalle nuevoDetalle = new DescuentosViewTablaDetalle();
                    nuevoDetalle.cuota = item.cuota.Value;
                    nuevoDetalle.idDescuento = item.id_descuento.Value;
                    nuevoDetalle.idDetalle = item.id_detalle_descuento;
                    if (item.estado.Value == 0) nuevoDetalle.estado = "Pendiente";
                    if (item.estado.Value == 1) nuevoDetalle.estado = "Pagada";
                    nuevoDetalle.fecha = item.fecha.Value;
                    if (model.descuento.en_uf == false)
                    {
                        nuevoDetalle.saldo = item.saldo.Value.ToString("N0");
                        nuevoDetalle.valorCuota = item.valor_cuota.Value.ToString("N0");
                        nuevoDetalle.valorPagado = item.valor_pagado.Value.ToString("N0");
                        nuevoDetalle.valorPagar = item.valor_pagar.Value.ToString("N0");
                    }
                    else
                    {
                        nuevoDetalle.saldo = item.saldo.Value.ToString("N2");
                        nuevoDetalle.valorCuota = item.valor_cuota.Value.ToString("N2");
                        nuevoDetalle.valorPagado = item.valor_pagado.Value.ToString("N2");
                        nuevoDetalle.valorPagar = item.valor_pagar.Value.ToString("N2");
                    }
                    if (DateTime.Now.Year > item.fecha.Value.Year && item.saldo.Value != 0) nuevoDetalle.editable = true;
                    if (DateTime.Now.Year == item.fecha.Value.Year && DateTime.Now.Month >= item.fecha.Value.Month && item.saldo.Value != 0) nuevoDetalle.editable = true;
                    model.detalle.Add(nuevoDetalle);
                }
            }
            return View(model);
        }
        public ActionResult editarDescuento(int idDescuento)
        {
            CrearDescuentoView model = new CrearDescuentoView();
            using (var db = new sgsbdEntities())
            {
                var descuento = db.descuento.Where(u => u.id_descuento == idDescuento).FirstOrDefault();
                model.camion = db.camiones.Where(u => u.id_camion == descuento.id_camion).FirstOrDefault();
                model.idDescuento = idDescuento;
                model.cuotas = descuento.cuotas.Value;
                model.enUF = descuento.en_uf;
                model.llevaIVA = descuento.lleva_iva;
                if (descuento.en_uf == false)
                {

                    model.neto = descuento.neto.Value.ToString("N0");
                }
                else
                {

                    model.neto = descuento.neto.Value.ToString("N2");
                }
                model.oc = descuento.oc;
                model.factura = descuento.numero_factura;
                model.fechaPrimeraCuota = new selectListCamiones();
                DateTime fecha1 = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString());
                DateTime fecha2 = Convert.ToDateTime("01-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString());
                DateTime fecha3 = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString());
                DateTime fecha4 = Convert.ToDateTime("01-" + DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString());
                if (descuento.fecha.Value == fecha1) model.fechaPrimeraCuota.camionSelect = 1;
                if (descuento.fecha.Value == fecha2) model.fechaPrimeraCuota.camionSelect = 2;
                if (descuento.fecha.Value == fecha3) model.fechaPrimeraCuota.camionSelect = 3;
                if (descuento.fecha.Value == fecha4) model.fechaPrimeraCuota.camionSelect = 4;
                llenaConceptos(model);
 
                model.observacion = descuento.observacion;
                model.oc = descuento.oc;
                model.factura = descuento.numero_factura;
                model.conceptoSelectList.conceptoSelect = descuento.id_tipo_descuento != null ? Convert.ToInt32(descuento.id_tipo_descuento) : 0;
                model.ok = 0;
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult editarDescuento(CrearDescuentoView model)
        {
            using (var db = new sgsbdEntities())
            {
                var descuento = db.descuento.Where(u => u.id_descuento == model.idDescuento).FirstOrDefault();
                descuento.neto = (model.enUF == false) ? Convert.ToInt32(model.neto.Replace(".", "")) : Convert.ToDouble(model.neto.Replace(".", ""));
                string fecha1 = DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString();
                string fecha2 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
                string fecha3 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
                string fecha4 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
                if (model.fechaPrimeraCuota.camionSelect == 1) descuento.fecha = Convert.ToDateTime("01-" + fecha1);
                if (model.fechaPrimeraCuota.camionSelect == 2) descuento.fecha = Convert.ToDateTime("01-" + fecha2);
                if (model.fechaPrimeraCuota.camionSelect == 3) descuento.fecha = Convert.ToDateTime("01-" + fecha3);
                if (model.fechaPrimeraCuota.camionSelect == 4) descuento.fecha = Convert.ToDateTime("01-" + fecha4);
                model.camion = db.camiones.Where(u => u.id_camion == descuento.id_camion).FirstOrDefault();
                model.idCamion = model.camion.id_camion;
                descuento.observacion = model.observacion;
                descuento.numero_factura = model.factura;
                descuento.oc = model.oc;
                double iva = descuento.neto.Value * 0.19;
                double total = descuento.neto.Value * 1.19;
                if (model.llevaIVA == false)
                {
                    descuento.iva = 0;
                    descuento.total = descuento.neto;
                }
                else
                {
                    descuento.iva = iva;
                    descuento.total = total;
                }
                descuento.id_tipo_descuento = model.conceptoSelectList.conceptoSelect;
                descuento.lleva_iva = model.llevaIVA;
                db.SaveChanges();
                var detalleDescuento = db.detalle_descuento.Where(u => u.id_descuento == model.idDescuento).ToList();
                double cuota = descuento.neto.Value / descuento.cuotas.Value;
                int i = 0;
                foreach (detalle_descuento item in detalleDescuento)
                {
                    DateTime fechaCuota = Convert.ToDateTime("01-" + descuento.fecha.Value.AddMonths(i).Month.ToString() + "-" + descuento.fecha.Value.AddMonths(i).Year.ToString());
                    item.valor_cuota = cuota;
                    item.fecha = fechaCuota.Date;
                    item.valor_pagar = cuota;
                    item.saldo = cuota;
                    db.SaveChanges();
                    i++;
                }
                model.ok = 1;
                llenaConceptos(model);
            }
            return RedirectToAction("descuentos", "Franquiciados");
        }
        public ActionResult editarPagoLeasing(int idDetalle)
        {
            editarPagoLeasingView model = new editarPagoLeasingView();
            using (var db = new sgsbdEntities())
            {
                model.idDetalle = idDetalle;
                var detalle = db.detalle_leasing.Where(u => u.id_detalle_leasing == idDetalle).FirstOrDefault();
                model.idLeasing = db.leasing.Where(u => u.id_leasing == detalle.id_leasing).FirstOrDefault().id_leasing;
                if (detalle.fecha_pagado == null && detalle.saldoCuota != 0)
                {
                    if (detalle.valor_pagado != null)
                    {
                        model.valorPagado = detalle.valor_pagado.Value.ToString("N2");
                    }
                    else model.valorPagado = (0).ToString("N2");
                    model.valorPagar = detalle.valor_pagar.Value.ToString("N2");
                    model.valorCuota = detalle.cuota_bruta.Value.ToString("N2");
                    model.saldo = detalle.saldoCuota.Value.ToString("N2");

                }
            }
            return View(model);

        }
        [HttpPost]
        public ActionResult editarPagoLeasing(editarPagoLeasingView model)
        {
            using (var db = new sgsbdEntities())
            {
                var detalle = db.detalle_leasing.Where(u => u.id_detalle_leasing == model.idDetalle).FirstOrDefault();
                var detalles = detalle.leasing.detalle_leasing;
                detalle.valor_pagar = Convert.ToDouble(model.valorPagar);
                //Nombre: Jonathan Ancán
                //Empresa: Valentys
                //Fecha: 04-06-2015
                //Observación: se corrigio las líneas de Código porque esta tratando de convertir un valor double a Int32
                //-------------------------------------------------------------------------------------------------------
                //int valorPagar = Convert.ToInt32(model.valorPagar);
                //if (valorPagar == 0)
                if (detalle.valor_pagar == 0)
                {
                    foreach (var item in detalles.Where(u => u.fecha_pago.Value > detalle.fecha_pago.Value).ToList())
                    {
                        item.fecha_pago = item.fecha_pago.Value.AddMonths(1);
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
            }
            return RedirectToAction("detalleLeasing", "Franquiciados", new { idLeasing = model.idLeasing });
        }
        public ActionResult editarPagoDescuento(int idDetalle)
        {
            editarPagoDescuentoView model = new editarPagoDescuentoView();
            using (var db = new sgsbdEntities())
            {
                model.idDetalle = idDetalle;
                var detalle = db.detalle_descuento.Where(u => u.id_detalle_descuento == idDetalle).FirstOrDefault();
                model.descuento = db.descuento.Where(u => u.id_descuento == detalle.id_descuento).FirstOrDefault();
                model.idDescuento = detalle.id_descuento.Value;
                if (detalle.estado == 0 && detalle.saldo != 0)
                {
                    if (model.descuento.en_uf == false)
                    {
                        if (detalle.valor_pagado != null)
                        {
                            model.valorPagado = detalle.valor_pagado.Value.ToString("N2");
                        }
                        else model.valorPagado = (0).ToString("N0");
                        model.valorPagar = detalle.valor_pagar.Value.ToString("N2");
                        model.valorCuota = detalle.valor_cuota.Value.ToString("N0");
                        model.saldo = detalle.saldo.Value.ToString("N0");
                    }
                    else
                    {

                        if (detalle.valor_pagado != null)
                        {
                            model.valorPagado = detalle.valor_pagado.Value.ToString("N2");
                        }
                        else model.valorPagado = (0).ToString("N2");
                        model.valorPagar = detalle.valor_pagar.Value.ToString("N2");
                        model.valorCuota = detalle.valor_cuota.Value.ToString("N2");
                        model.saldo = detalle.saldo.Value.ToString("N2");
                    }

                }
            }
            return View(model);

        }
        [HttpPost]
        public ActionResult editarPagoDescuento(editarPagoDescuentoView model)
        {
            using (var db = new sgsbdEntities())
            {
                var detalle = db.detalle_descuento.Where(u => u.id_detalle_descuento == model.idDetalle).FirstOrDefault();
                var detalles = db.detalle_descuento.Where(u => u.id_descuento == detalle.id_descuento).ToList();
                detalle.valor_pagar = Convert.ToDouble(model.valorPagar.Replace(".", ""));
                double valorPagar = Convert.ToDouble(model.valorPagar.Replace(".", ""));
                if (valorPagar == 0)
                {
                    foreach (var item in detalles.Where(u => u.fecha.Value > detalle.fecha.Value).ToList())
                    {
                        item.fecha = item.fecha.Value.AddMonths(1);
                        db.SaveChanges();
                    }
                }
                db.SaveChanges();
            }
            return RedirectToAction("detalleDescuento", "Franquiciados", new { idDescuento = model.idDescuento });
        }
        public ActionResult leasingExcel(int estado, int idFranquiciado)
        {
            LeasingView model = new LeasingView();
            model.encabezadoLeasing = new List<leasingTablaEncabezado>();
            using (var db = new sgsbdEntities())
            {
                var leasing = db.leasing.ToList();
                if (estado == 1) leasing = db.leasing.Where(u => u.fechaFin != null).ToList();
                if (estado == 0) leasing = db.leasing.Where(u => u.fechaFin == null).ToList();
                if (idFranquiciado != -1)
                {
                    leasing = leasing.Where(u => u.id_franquiciados.Value == idFranquiciado).ToList();
                }
                if (leasing != null)
                {
                    model.encabezadoLeasing = new List<leasingTablaEncabezado>();
                    foreach (leasing item in leasing)
                    {
                        var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                        leasingTablaEncabezado row = new leasingTablaEncabezado();
                        row.patenteCamion = camion.patente;
                        row.idLeasing = item.id_leasing;
                        row.cuotas = item.cuotas;
                        if (item.fechaFin == null) row.estado = "Vigente";
                        else row.estado = "Terminado";
                        row.fecha = item.fecha.Value.ToShortDateString();
                        row.interes = item.interes.Value;
                        row.iva = item.iva.Value;
                        row.neto = item.neto.Value;
                        if (item.pie_uf == null) row.pie = 0;
                        else row.pie = item.pie_uf.Value;
                        row.total = item.total.Value;
                        row.totalUF = item.total_uf.Value;
                        row.valorUfDia = item.valor_uf.Value;
                        model.encabezadoLeasing.Add(row);
                    }
                }
            }
            llenaEstadosLeasing(model);
            return View(model);
        }
        public ActionResult leasing()
        {
            LeasingView model = new LeasingView();
            model.encabezadoLeasing = new List<leasingTablaEncabezado>();
            using (var db = new sgsbdEntities())
            {
                if (db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault() != null)
                {
                    model.puedeAgregar = true;

                }
                else
                {
                    model.puedeAgregar = false;
                }
                var leasing = db.leasing.ToList();
                //foreach (var item in leasing)
                //{
                //    int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                //    if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciados && u.id_unidad == unidad).FirstOrDefault() != null)
                //    {
                //        int mes = 0;
                //        var detalles = db.detalle_leasing.Where(u => u.id_leasing == item.id_leasing).ToList();
                //        foreach (var detalle in detalles.Where(u => u.fecha_pagado == null).OrderBy(u => u.cuota).ToList())
                //        {
                //            detalle.fecha_pago = Convert.ToDateTime("1-" + DateTime.Now.AddMonths(mes).Month.ToString() + "-" + DateTime.Now.AddMonths(mes).Year.ToString());
                //            db.SaveChanges();
                //            mes++;
                //        }
                //    }
                //}
                if (leasing != null)
                {
                    model.encabezadoLeasing = new List<leasingTablaEncabezado>();
                    foreach (leasing item in leasing)
                    {
                        int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciados && u.id_unidad == unidad).FirstOrDefault() != null)
                        {
                            var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                            leasingTablaEncabezado row = new leasingTablaEncabezado();
                            row.patenteCamion = camion.patente;
                            row.idLeasing = item.id_leasing;
                            row.cuotas = item.cuotas;
                            if (item.fechaFin == null) row.estado = "Vigente";
                            else row.estado = "Terminado";
                            row.fecha = item.fecha.Value.ToShortDateString();
                            row.interes = item.interes.Value;
                            row.iva = item.iva.Value;
                            row.neto = item.neto.Value;
                            if (item.pie_uf == null) row.pie = 0;
                            else row.pie = item.pie_uf.Value;
                            row.total = item.total.Value;
                            row.totalUF = item.total_uf.Value;
                            row.valorUfDia = item.valor_uf.Value;
                            var detalles = db.detalle_leasing.Where(u => u.id_leasing == item.id_leasing).ToList();
                            foreach (var detalle in detalles)
                            {
                                if (detalle.valor_pagado != 0)
                                {
                                    row.puedeEditar = false;
                                    break;
                                }
                                else
                                {
                                    row.puedeEditar = true;
                                }
                            }
                            model.encabezadoLeasing.Add(row);

                        }
                    }
                }
                llenaEstadosLeasing(model);
                llenaFranquiciadosFiltroLeasing(model);
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult leasing(LeasingView model)
        {
            using (var db = new sgsbdEntities())
            {
                if (db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault() != null)
                {
                    model.puedeAgregar = true;

                }
                else
                {
                    model.puedeAgregar = false;
                }
                var leasing = db.leasing.ToList();
                if (model.selectListEstados.estadoSelect == 1) leasing = db.leasing.Where(u => u.fechaFin != null).ToList();
                if (model.selectListEstados.estadoSelect == 0) leasing = db.leasing.Where(u => u.fechaFin == null).ToList();
                if (model.selecListFranquiciados.franquiciadoSelect != null)
                {
                    leasing = leasing.Where(u => u.id_franquiciados.Value == model.selecListFranquiciados.franquiciadoSelect).ToList();
                }
                if (leasing != null)
                {

                    int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                    model.encabezadoLeasing = new List<leasingTablaEncabezado>();
                    foreach (leasing item in leasing)
                    {
                        if (db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciados && u.id_unidad == unidad).FirstOrDefault() != null)
                        {
                            var camion = db.camiones.Where(u => u.id_camion == item.id_camion).FirstOrDefault();
                            leasingTablaEncabezado row = new leasingTablaEncabezado();
                            row.patenteCamion = camion.patente;
                            row.idLeasing = item.id_leasing;
                            row.cuotas = item.cuotas;
                            if (item.fechaFin == null) row.estado = "Vigente";
                            else row.estado = "Terminado";
                            row.fecha = item.fecha.Value.ToShortDateString();
                            row.interes = item.interes.Value;
                            row.iva = item.iva.Value;
                            row.neto = item.neto.Value;
                            if (item.pie_uf == null) row.pie = 0;
                            else row.pie = item.pie_uf.Value;
                            row.total = item.total.Value;
                            row.totalUF = item.total_uf.Value;
                            row.valorUfDia = item.valor_uf.Value;
                            model.encabezadoLeasing.Add(row);
                        }
                    }
                }
                llenaEstadosLeasing(model);
                llenaFranquiciadosFiltroLeasing(model);
            }

            return View(model);
        }
        public ActionResult Create()
        {
            this.buildViewBagList();

            return View();

        }
        [HttpPost]
        public ActionResult desc(descuento model, string id)
        {
            DescuentosRepository rs = new DescuentosRepository(new sgsbdEntities());
            model.id_franquiciado = Int32.Parse(id);
            rs.InsertDescuento(model);
            rs.Save();
            return this.RedirectToAction("Descuentos", "Franquiciados", new { id = Int32.Parse(id) });
        }
        public ActionResult deleteDescuento(int idDescuento)
        {
            using (var db = new sgsbdEntities())
            {
                var leasingPrueba = db.descuento.Where(u => u.id_descuento == idDescuento).FirstOrDefault();
                var detallesPrueba = db.detalle_descuento.Where(u => u.id_descuento == idDescuento).ToList();
                bool puedeEliminar = true;
                foreach (var detalle in detallesPrueba)
                {
                    if (detalle.valor_pagado.Value != 0)
                    {
                        puedeEliminar = false;
                        break;
                    }
                }
                if (puedeEliminar == true)
                {
                    var leasing = db.descuento.Where(u => u.id_descuento == idDescuento).FirstOrDefault();
                    var detalles = db.detalle_descuento.Where(u => u.id_descuento == idDescuento).ToList();
                    foreach (var detalle in detalles)
                    {
                        db.detalle_descuento.Remove(detalle);
                        db.SaveChanges();
                    }
                    db.descuento.Remove(leasing);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("descuentos", "franquiciados");
        }
        public ActionResult deleteLeasing(int idLeasing)
        {
            using (var db = new sgsbdEntities())
            {
                var leasingPrueba = db.leasing.Where(u => u.id_leasing == idLeasing).FirstOrDefault();
                var detallesPrueba = db.detalle_leasing.Where(u => u.id_leasing == idLeasing).ToList();
                bool puedeEliminar = true;
                foreach (var detalle in detallesPrueba)
                {
                    if (detalle.valor_pagado.Value != 0)
                    {
                        puedeEliminar = false;
                        break;
                    }
                }
                if (puedeEliminar == true)
                {
                    var leasing = db.leasing.Where(u => u.id_leasing == idLeasing).FirstOrDefault();
                    var detalles = db.detalle_leasing.Where(u => u.id_leasing == idLeasing).ToList();
                    foreach (var detalle in detalles)
                    {
                        db.detalle_leasing.Remove(detalle);
                        db.SaveChanges();
                    }
                    db.leasing.Remove(leasing);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("leasing", "franquiciados");
        }
        [HttpPost]
        public ActionResult Create(franquiciado model, string numero_direccion, string direccion, string tipoc, string datepickerNoModel)
        {

            if (numero_direccion.Equals("") || numero_direccion.Equals(""))
            {
                ModelState.AddModelError("direccion", "Se requieren todos los campos de la direccion");
            }

            if (datepickerNoModel.Equals("") || datepickerNoModel.Equals(""))
            {
                ModelState.AddModelError("fecha_contrato", "Debe ingresar una fecha de contrato");
            }



            model.id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            if (ModelState.IsValid)
            {
                string fechaval = "";
                if (String.IsNullOrEmpty(Convert.ToString(datepickerNoModel)))
                {
                    fechaval = "00/00/0000";

                }
                else
                {
                    fechaval = String.Format("{0:mm-dd-yyyy}", datepickerNoModel);
                }

                string mes = fechaval.Substring(0, 2);
                string dia = fechaval.Substring(3, 2);
                string ano = fechaval.Substring(6, 4);

                model.fecha_contrato = Convert.ToDateTime(String.Format("{0:dd-mm-yyyy}", dia + "-" + mes + "-" + ano));

                //model.fecha_contrato = Convert.ToDateTime(fechaval);

                model.direccion = tipoc.Trim() + " " + direccion.Trim() + " " + numero_direccion.Trim();
                FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
                if (model.id_franquiciado == 0 && model.razon_social.ToString().Trim() != "Alexis Fierro")
                {
                    rs.InsertFranquiciado(model);
                }
                else
                {
                    rs.UpdateFranquiciado(model);
                }

                rs.Save();
                return this.RedirectToAction("Index", "Franquiciados");

            }
            this.buildViewBagList();
            return View(model);
        }
        public ActionResult ProcesosExcel(string fechaInicio, string fechaFinal)
        {
            FranquiciadosProcesosView model = new FranquiciadosProcesosView();
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaFin = Convert.ToDateTime(fechaFinal);
            this.rs = new FranquiciadosRepository(new sgsbdEntities());
            FranquiciadosProcesosView data = new FranquiciadosProcesosView();
            model.tabla = rs.GetFranquiciadosProcesos(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada(), fechaDesde.Date, fechaFin.Date);
            return View(model);
        }
        public ActionResult Procesos()
        {
            //FranquiciadosProcesosView model = new FranquiciadosProcesosView();
            //DateTime fechaInicio = DateTime.Now;
            //model.fechaInicio = fechaInicio.ToShortDateString();
            //model.fechaFin = Convert.ToDateTime("24-" + DateTime.Now.AddMonths(0).Month.ToString() + "-" + DateTime.Now.AddMonths(0).Year.ToString()).ToShortDateString();

            //using (var db = new sgsbdEntities())
            //{
            //    int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            //    var ultimoPago = db.pago_franquiciados.Where(u => u.id_pago == db.pago_franquiciados.Max(a => a.id_pago && a.)).FirstOrDefault();
            //    if (ultimoPago != null)
            //    {
            //        fechaInicio = ultimoPago.fechaFin.AddDays(1);
            //        model.fechaInicio = fechaInicio.ToShortDateString();
            //        model.fechaFin = model.fechaInicio;
            //    }
            //    else
            //    {
            //        fechaInicio = DateTime.Now;
            //        model.fechaInicio = fechaInicio.ToShortDateString();
            //        model.fechaFin = model.fechaInicio;
            //    }
            //    if (db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault() != null)
            //    {
            //        model.puedePrePagar = true;
            //    }
            //    else
            //    {
            //        model.puedePrePagar = false;
            //    }
            //}
            //DateTime fechaFin = Convert.ToDateTime(fechaInicio).AddDays(1).AddMilliseconds(-1);
            //this.rs = new FranquiciadosRepository(new sgsbdEntities());
            //FranquiciadosProcesosView data = new FranquiciadosProcesosView();
            //model.tabla = rs.GetFranquiciadosProcesos(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada(), fechaInicio, fechaFin);
            //return View(model);
            FranquiciadosProcesosView model = new FranquiciadosProcesosView();
            DateTime fechaInicio = DateTime.Now;
            model.fechaInicio = fechaInicio.ToShortDateString();
            model.fechaFin = Convert.ToDateTime("24-" + DateTime.Now.AddMonths(0).Month + "-" + DateTime.Now.AddMonths(0).Year).ToShortDateString();

            using (var db = new sgsbdEntities())
            {
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                //var ultimoPago1 = db.pago_franquiciados.Where(u => u.id_pago == db.pago_franquiciados.Max(a => a.id_pago)).FirstOrDefault();
                var ultimoPago = db.pago_franquiciados.Where(u => u.id_unidad == unidad).ToList().LastOrDefault();
                if(ultimoPago == null)
                {
                    fechaInicio = Convert.ToDateTime("01/01/2010");
                }
                else
                {
                    fechaInicio = ultimoPago.fechaFin.AddDays(1);
                }
                
                model.fechaInicio = fechaInicio.ToShortDateString();
                model.fechaFin = model.fechaInicio;

                if (db.uf.Where(u => u.mes == DateTime.Now.Month && u.año == DateTime.Now.Year).FirstOrDefault() != null)
                {
                    model.puedePrePagar = true;
                }
                else
                {
                    model.puedePrePagar = false;
                }
            }
            DateTime fechaFin = Convert.ToDateTime(fechaInicio).AddDays(1).AddMilliseconds(-1);
            FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
            model.tabla = rs.GetFranquiciadosProcesos(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada(),fechaInicio, fechaFin);
            return View(model);
        }

        [HttpPost]
        public ActionResult Procesos(FranquiciadosProcesosView model)
        {
            DateTime fechaInicio;
            DateTime fechaFin;
            model.puedePagar = true;

            using (var db = new sgsbdEntities())
            {
                fechaFin = Convert.ToDateTime(model.fechaFin).AddDays(1).AddMilliseconds(-1);
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var ultimoPago = db.pago_franquiciados.Where(u => u.id_unidad ==unidad).ToList().LastOrDefault(); ;
                if (ultimoPago != null)
                {
                    fechaInicio = ultimoPago.fechaFin.AddDays(1);
                }
                else
                {
                    fechaInicio = fechaFin.AddMonths(-1);
                }
            }
            int año;
            int mes;
            if (fechaInicio.Date >= fechaFin.Date)
            {
                año = fechaFin.Year;
                mes = fechaFin.Month;
            }
            else
            {
                año = fechaFin.Year;
                mes = fechaFin.Month;
            }
            using (var db = new sgsbdEntities())
            {
                if (db.uf.Where(u => u.año == año && u.mes == mes).FirstOrDefault() != null)
                {
                    model.puedePrePagar = true;
                }
                else
                {
                    model.puedePrePagar = false;
                    model.puedePagar = false;
                }

            }
            this.rs = new FranquiciadosRepository(new sgsbdEntities());
            FranquiciadosProcesosView data = new FranquiciadosProcesosView();
            model.tabla = rs.GetFranquiciadosProcesos(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada(), fechaInicio.Date, fechaFin.Date);
            //model.puedePagar = true;
            foreach (var item in model.tabla)
            {
                if (item.tienePendientes)
                {
                    model.puedePagar = false;
                    break;
                }
            }
            model.fechaFin = fechaFin.ToShortDateString();
            model.fechaInicio = fechaInicio.ToShortDateString();
            if (fechaFin.Day < 25)
            {
                model.periodoPagar = fechaFin.Month.ToString().PadLeft(2, '0') + "-" + fechaFin.Year;
            }
            else
            {
                model.periodoPagar = fechaFin.AddMonths(1).Month.ToString().PadLeft(2, '0') + "-" + fechaFin.AddMonths(1).Year.ToString();
            }

            return View(model);

        }
        public ActionResult franquiciadosPagadosExcel(int periodo, int idFranquiciado)
        {
            franquiciadosPagadosView model = new franquiciadosPagadosView();
            using (var db = new sgsbdEntities())
            {
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var pagos = db.pago_franquiciados.Where(u => u.id_unidad == unidad).ToList();
                if (periodo == 1) pagos = pagos.Where(u => u.periodo == "11-2014").ToList();
                if (periodo == 2) pagos = pagos.Where(u => u.periodo == "12-2014").ToList();
                if (periodo == 3) pagos = pagos.Where(u => u.periodo == "01-2015").ToList();
                if (periodo == 4) pagos = pagos.Where(u => u.periodo == "02-2015").ToList();
                if (periodo == 5) pagos = pagos.Where(u => u.periodo == "03-2015").ToList();
                if (periodo == 6) pagos = pagos.Where(u => u.periodo == "04-2015").ToList();
                if (periodo == 7) pagos = pagos.Where(u => u.periodo == "05-2015").ToList();
                if (periodo == 8) pagos = pagos.Where(u => u.periodo == "06-2015").ToList();
                if (periodo == 9) pagos = pagos.Where(u => u.periodo == "07-2015").ToList();
                if (periodo == 10) pagos = pagos.Where(u => u.periodo == "08-2015").ToList();
                if (periodo == 11) pagos = pagos.Where(u => u.periodo == "09-2015").ToList();
                if (periodo == 12) pagos = pagos.Where(u => u.periodo == "10-2015").ToList();
                if (periodo == 13) pagos = pagos.Where(u => u.periodo == "11-2015").ToList();
                if (periodo == 14) pagos = pagos.Where(u => u.periodo == "12-2015").ToList();
                if (periodo == 15) pagos = pagos.Where(u => u.periodo == "01-2016").ToList();
                if (periodo == 16) pagos = pagos.Where(u => u.periodo == "02-2016").ToList();
                if (periodo == 17) pagos = pagos.Where(u => u.periodo == "03-2016").ToList();
                if (periodo == 18) pagos = pagos.Where(u => u.periodo == "04-2016").ToList();
                if (periodo == 19) pagos = pagos.Where(u => u.periodo == "05-2016").ToList();
                if (periodo == 20) pagos = pagos.Where(u => u.periodo == "06-2016").ToList();
                //if (periodo == 21) pagos = pagos.Where(u => u.periodo == "07-2016").ToList();
                if (idFranquiciado != -1)
                {
                    pagos = pagos.Where(u => u.id_franquiciado == idFranquiciado).ToList();
                }
                if (pagos.Count != 0)
                {
                    model.tabla = new List<franquiciadosPagadosTabla>();
                    foreach (var item in pagos)
                    {
                        franquiciadosPagadosTabla row = new franquiciadosPagadosTabla();
                        row.fechaInicio = item.fechaInicio.ToShortDateString();
                        row.idPago = item.id_pago;
                        row.fechaFin = item.fechaFin.ToShortDateString();
                        var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault();
                        row.franquiciado = franquiciado.razon_social;
                        row.idFranquiciado = item.id_franquiciado;
                        row.montoBruto = item.montoBruto.ToString("N0");
                        row.montoNeto = item.montoNeto.ToString("N0");
                        model.tabla.Add(row);
                    }
                }
            }
            return View(model);
        }
        //public ActionResult franquiciadosPagados()
        //{
        //    franquiciadosPagadosView model = new franquiciadosPagadosView();
        //    llenaFranquiciadosFiltroPagados(model);
        //    llenaPeriodosFiltroPagados(model);
        //    using (var db = new sgsbdEntities())
        //    {
        //        int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
        //        var pagos = db.pago_franquiciados.Where(u => u.id_unidad == unidad).ToList();

        //        if (pagos.Count != 0)
        //        {
        //            model.tabla = new List<franquiciadosPagadosTabla>();
        //            foreach (var item in pagos)
        //            {
        //                franquiciadosPagadosTabla row = new franquiciadosPagadosTabla();
        //                row.fechaInicio = item.fechaInicio.ToShortDateString();
        //                row.idPago = item.id_pago;
        //                row.fechaFin = item.fechaFin.ToShortDateString();
        //                var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault();
        //                row.franquiciado = franquiciado.razon_social;
        //                row.idFranquiciado = item.id_franquiciado;
        //                row.montoBruto = item.montoBruto.ToString("N0");
        //                row.montoNeto = item.montoNeto.ToString("N0");
        //                model.tabla.Add(row);
        //            }
        //        }
        //    }
        //    return View(model);
        //}

        public ActionResult franquiciadosPagados()
        {

                ViewBag.consolidadoFechaInicio = "";
                ViewBag.consolidadoFechaFin = "";

                franquiciadosPagadosView model = new franquiciadosPagadosView();
                llenaFranquiciadosFiltroPagados(model);
                llenaPeriodosFiltroPagados(model);
                using (var db = new sgsbdEntities())
                {
                       int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                       var pagos = db.pago_franquiciados.Where(u => u.id_unidad == unidad).ToList();

                if (pagos.Count != 0)
                    {
                        model.tabla = new List<franquiciadosPagadosTabla>();
                        foreach (var item in pagos)
                        {
                            franquiciadosPagadosTabla row = new franquiciadosPagadosTabla();
                            row.fechaInicio = item.fechaInicio.ToShortDateString();
                            row.idPago = item.id_pago;
                            row.fechaFin = item.fechaFin.ToShortDateString();
         

                            var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault();

                            if (franquiciado != null)
                            {
                                row.franquiciado = franquiciado.razon_social.ToString();
                                row.idFranquiciado = item.id_franquiciado;
                                row.montoBruto = item.montoBruto.ToString("N0");
                                row.montoNeto = item.montoNeto.ToString("N0");
                                
                            }

                            model.tabla.Add(row);
                        }
                    }
                }
            return View(model);
        }


        [HttpPost]
        public ActionResult franquiciadosPagados(franquiciadosPagadosView model)
        {
            ViewBag.consolidadoFechaInicio = "";
            ViewBag.consolidadoFechaFin = "";
            using (var db = new sgsbdEntities())
            {
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var pagos = db.pago_franquiciados.Where(u => u.id_unidad == unidad).ToList();


                var id_pago_periodoSeleccionado = model.selecListPeriodos.periodoSelect;

                if (id_pago_periodoSeleccionado != null)
                {
                    var periodo = db.pago_franquiciados.Where(x => x.id_pago == id_pago_periodoSeleccionado).FirstOrDefault();
                    //var periodoSeleccionado = db.pago_franquiciados.Where(x => x.id_pago == id_pago_periodoSeleccionado).Select(x => x.periodo).FirstOrDefault().ToString();

                    if (DateTime.Compare(Convert.ToDateTime("1/" + periodo.periodo.Replace("-", "/")), Convert.ToDateTime("01/05/2018")) >= 0) //Desde Mayo 2018 se genera el consolidado
                    {
                        ViewBag.consolidadoFechaInicio = periodo.fechaInicio.ToShortDateString();
                        ViewBag.consolidadoFechaFin = periodo.fechaFin.ToShortDateString();
                    }

                    pagos = pagos.Where(u => u.periodo == periodo.periodo).ToList();
                }

                if (model.selecListFranquiciados.franquiciadoSelect != null)
                {
                    pagos = pagos.Where(u => u.id_franquiciado == model.selecListFranquiciados.franquiciadoSelect).ToList();
                }
                if (pagos.Count != 0)
                {
                    model.tabla = new List<franquiciadosPagadosTabla>();
                    foreach (var item in pagos)
                    {
                        franquiciadosPagadosTabla row = new franquiciadosPagadosTabla();
                        row.fechaInicio = item.fechaInicio.ToShortDateString();
                        row.idPago = item.id_pago;
                        row.fechaFin = item.fechaFin.ToShortDateString();
                        var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault();
                        row.franquiciado = franquiciado.razon_social;
                        row.idFranquiciado = item.id_franquiciado;
                        row.montoBruto = item.montoBruto.ToString("N0");
                        row.montoNeto = item.montoNeto.ToString("N0");
                        model.tabla.Add(row);
                    }
                }
            }
            llenaFranquiciadosFiltroPagados(model);
            llenaPeriodosFiltroPagados(model);
            return View(model);
        }
        public ActionResult excelHRSinCerrar(string fechaInicio, string fechaFin, int idFranquiciado)
        {
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
            hrSinCerrarExcel model = new hrSinCerrarExcel();
            model.tabla = new List<tablaHRSinCerrarExcel>();
            using (var db = new sgsbdEntities())
            {
                var camiones = db.camiones.Where(u => u.id_franquiciado == idFranquiciado).ToList();
                foreach (var camion in camiones)
                {

                    //Nombre: Jonathan Ancán
                    //Empresa: Valentys
                    //Fecha: 25-05-2015
                    //Obervacion se modifica el estado para que compruebe solo los que son estado = 1 (pendiente)
                    //Estados: 1 = Asignado, 2 = Realizado, 3 = Suspendido, 4 = No Realizado , 5 = Bloqueado, 6 = No Atendido, 7 = Mal Estibado, 8 = Sin Pesaje
                    //*********************************************************************************************************************************************
                    //var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => (u.estado == 1 || u.estado == 2) && u.fecha.Value > fechaDesde && u.fecha.Value < fechaHasta && u.id_camion.Value == camion.id_camion).ToList();
                    var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => u.id_camion == camion.id_camion && u.fecha >= fechaDesde && u.fecha <= fechaHasta && u.fecha_pagado == null && (u.estado == 1 || u.estado == null)).ToList();
                    //*********************************************************************************************************************************************

                    foreach (var hoja in hojasDeRutaEncabezado)
                    {
                        tablaHRSinCerrarExcel row = new tablaHRSinCerrarExcel();
                        row.chofer = db.conductores.Where(u => u.id_conductor == hoja.id_conductor).FirstOrDefault().nombre_conductor;
                        row.fecha = hoja.fecha.Value.ToShortDateString();
                        row.franquiciado = db.franquiciado.Where(u => u.id_franquiciado == idFranquiciado).FirstOrDefault().razon_social;
                        row.idHoja = hoja.id_hoja_ruta.ToString();
                        row.patente = camion.patente;
                        model.tabla.Add(row);
                    }
                }
            }
            return View(model);
        }
        public ActionResult ObtenerPagoFranquiciadosByCliente(string fechaInicio, string fechaFin, int idcliente)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            FranquiciadosRepository rf = new FranquiciadosRepository(ctx);
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
            excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();

            //1- declarar listaview con los pagos franquiaciados
            List<camionExcelFranquiciadoPago> listaFranquiciadosPagos = new List<camionExcelFranquiciadoPago>();

            //2- Obtener todos los franquiciados asociados al cliente
            //  List<FranquiciadosView> listaFranquiciados = (List<FranquiciadosView>)rf.GetFranquiciadosViewByClientes(idcliente);

            return View();

        }
        //public ActionResult excelPagoFranquiciados(string _fechaInicio, string _fechaFin)
        //{
        //    string fechaFin = _fechaFin;
        //    string fechaInicio = _fechaInicio;
        //    DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
        //    DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
        //    excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();
        //    using (var db = new sgsbdEntities())
        //    {
        //        int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
        //        var franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).ToList();
        //        foreach (var franquiciado in franquiciados)
        //        {
        //            model.fechaFin = fechaFin;
        //            model.fechaInicio = fechaInicio;
        //            model.franquiciado = franquiciado.razon_social;
        //            model.rutFranquiciado = franquiciado.rut_franquiciado;
        //            var camionesFranquiciado = db.camiones.Where(u => u.id_franquiciado == franquiciado.id_franquiciado).ToList();
        //            if (camionesFranquiciado != null)
        //            {
        //                model.hojaCamion = new List<camionExcelFranquiciadoPago>();
        //                model.hojaDetalleCamion = new List<hojaDetalleCamionExcelPrePago>();
        //                int montoNeto = 0;
        //                int montoBruto = 0;
        //                foreach (camiones camion in camionesFranquiciado)
        //                {
        //                    var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value >= fechaDesde && u.fecha.Value <= fechaHasta && u.id_camion.Value == camion.id_camion && u.estado == 3).ToList();
        //                    if (hojasDeRutaEncabezado != null)
        //                    {
        //                        camionExcelFranquiciadoPago hoja = new camionExcelFranquiciadoPago();
        //                        hojaDetalleCamionExcelPrePago hojaDetalle = new hojaDetalleCamionExcelPrePago();
        //                        hoja.patenteCamion = camion.patente;
        //                        hoja.HRcantidadTotal = 0;
        //                        hoja.HRNetoTotal = 0;
        //                        hoja.leasingIva = 0;
        //                        hoja.leasingNeto = 0;
        //                        int año;
        //                        int mes;
        //                        if (fechaDesde.Date >= fechaHasta.Date)
        //                        {
        //                            año = fechaHasta.Year;
        //                            mes = fechaHasta.Month;
        //                        }
        //                        else
        //                        {
        //                            año = fechaHasta.Year;
        //                            mes = fechaHasta.Month;
        //                        }
        //                        var uf = db.uf.Where(u => u.año == año && u.mes == mes).FirstOrDefault();
        //                        hoja.valorUF = uf.valor_uf;
        //                        hoja.leasingTotal = 0;
        //                        hoja.periodo = calculoPeriodo(fechaDesde, fechaHasta);
        //                        hoja.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
        //                        hojaDetalle.tabla = new List<tablaDetalleCamionExcelPrePago>();
        //                        hojaDetalle.patenteCamion = camion.patente;
        //                        var tipos = db.tipo_servicio.ToList();
        //                        foreach (tipo_servicio tipo in tipos)
        //                        {
        //                            tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
        //                            nuevaTipo.total = 0;
        //                            nuevaTipo.tipo = tipo.tipo_servicio1;
        //                            nuevaTipo.neto = 0;
        //                            nuevaTipo.iva = 0;
        //                            nuevaTipo.cantidad = 0;
        //                            hoja.tablaHR.Add(nuevaTipo);
        //                        }
        //                        var clientesEspeciales = db.clientes.Where(u => u.cobro_especial == true).ToList();
        //                        foreach (clientes cliente in clientesEspeciales)
        //                        {
        //                            tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
        //                            nuevaTipo.total = 0;
        //                            nuevaTipo.tipo = cliente.razon_social;
        //                            nuevaTipo.neto = 0;
        //                            nuevaTipo.iva = 0;
        //                            nuevaTipo.cantidad = 0;
        //                            hoja.tablaHR.Add(nuevaTipo);
        //                        }
        //                        int numero = 1;
        //                        foreach (hoja_ruta hrEncabezado in hojasDeRutaEncabezado.OrderBy(u => u.fecha))
        //                        {

        //                            conductores conductor = db.conductores.Where(u => u.id_conductor == hrEncabezado.id_conductor).FirstOrDefault();
        //                            var hojasDeRutaDetalle = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == hrEncabezado.id_hoja_ruta).ToList();
        //                            IEnumerable<ruta_punto> listaPuntosModelo = new List<ruta_punto>();
        //                            if (hrEncabezado.ruta_modelo != null)
        //                            {
        //                                var rutaModelo = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault();
        //                                listaPuntosModelo = rutaModelo.ruta_punto;
        //                            }
        //                            bool comboUltimoDetalle = false;
        //                            bool comboUltimoTarifa = false;
        //                            if (hrEncabezado.id_hoja_ruta == 749)
        //                            {
        //                                comboUltimoTarifa = false;
        //                            }
        //                            foreach (hoja_ruta_detalle hrDetalle in hojasDeRutaDetalle)
        //                            {
        //                                bool tarifaEspecial = false;
        //                                int valorTarifaEspecial = 0;
        //                                int cantidad = 1;
        //                                int idPuntoServicio = hrDetalle.id_punto_servicio;
        //                                var punto = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault();
        //                                int idContrato = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault().id_contrato;
        //                                int idCliente = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault().id_cliente;
        //                                var cliente = db.clientes.Where(u => u.id_cliente == idCliente).FirstOrDefault();
        //                                var tipoServicio = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault();
        //                                if ((hrEncabezado.id_tipo_servicio == 2) || (hrEncabezado.id_tipo_servicio == 3)) cantidad = 1;
        //                                else
        //                                {
        //                                    if (hrDetalle.cantidad != null) cantidad = hrDetalle.cantidad.Value;
        //                                    else cantidad = 1;
        //                                }
        //                                tipo_tarifa tarifa = new tipo_tarifa();
        //                                if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
        //                                {
        //                                    if (hrDetalle.estado == 6 || hrDetalle.estado == 5 || hrDetalle.estado == 7)
        //                                    {
        //                                        if (unidad == 31 || unidad == 32 || unidad == 33)
        //                                        {
        //                                            comboUltimoTarifa = true;
        //                                            int idTipoTarifa = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
        //                                            tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
        //                                            tarifa.valor_tarifa = tarifa.valor_tarifa * 0.6;
        //                                        }
        //                                        else
        //                                            tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11541).FirstOrDefault(); //60%
        //                                    }
        //                                    else
        //                                    {
        //                                        if (hrDetalle.estado == 3 || hrDetalle.estado == 4)
        //                                        {
        //                                            //tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11249).FirstOrDefault();//test
        //                                            //tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 14365).FirstOrDefault();//local
        //                                            tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11542).FirstOrDefault();//prod
        //                                        }
        //                                        else
        //                                        {
        //                                            tarifa = db.puntos_servicio.Where(l => l.id_punto_servicio == idPuntoServicio).FirstOrDefault().tipo_tarifa;
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (comboUltimoTarifa == false && hrDetalle.estado == 2)
        //                                    {
        //                                        comboUltimoTarifa = true;
        //                                        int idTipoTarifa = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
        //                                        tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
        //                                    }
        //                                    else
        //                                    {
        //                                        //tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11249).FirstOrDefault();//test
        //                                        //tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 14365).FirstOrDefault();//local
        //                                        tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11542).FirstOrDefault();//prod
        //                                    }
        //                                }
        //                                if (tarifa.tarifa_escalable == true)
        //                                {
        //                                    if (db.detalle_calculo_tarifa_escalable.Where(u => u.id == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && DateTime.Now == u.fechaEjecucion).FirstOrDefault() != null)
        //                                    {
        //                                        var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && DateTime.Now == u.fechaEjecucion).FirstOrDefault();
        //                                        var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
        //                                        if (tarifaFinal != null)
        //                                        {
        //                                            tarifaEspecial = true;
        //                                            valorTarifaEspecial = tarifaFinal.valor;
        //                                        }
        //                                        else
        //                                        {
        //                                            valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        calculoCantidadPuntos(fechaDesde, fechaHasta, tarifa);
        //                                        var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && DateTime.Now == u.fechaEjecucion).FirstOrDefault();
        //                                        if (calculoTarifa != null)
        //                                        {
        //                                            var tarifaFinales = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max);
        //                                            if (tarifaFinales != null)
        //                                            {
        //                                                var tarifaFinal = tarifaFinales.FirstOrDefault();
        //                                                tarifaEspecial = true;
        //                                                valorTarifaEspecial = tarifaFinal.valor;
        //                                            }
        //                                            else
        //                                            {
        //                                                valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                int valorTarifa = 0;
        //                                if (tarifaEspecial == false)
        //                                    valorTarifa = Convert.ToInt32(Math.Round(tarifa.valor_tarifa, 0, MidpointRounding.AwayFromZero));
        //                                else valorTarifa = valorTarifaEspecial;
        //                                //bool porPeso;
        //                                //if (punto.pago_por_peso == true) porPeso = true;
        //                                //else porPeso = false;
        //                                bool pagoXpeso = punto.pago_por_peso;
        //                                bool pagoXm3 = punto.pago_por_m3;
        //                                string estados = "2";
        //                                if (hrDetalle.estado == 1) estados = "Asignado";
        //                                if (hrDetalle.estado == 2) estados = "Realizado";
        //                                if (hrDetalle.estado == 3) estados = "Suspendido";//sin
        //                                if (hrDetalle.estado == 4) estados = "No realizado";//sus
        //                                if (hrDetalle.estado == 5) estados = "Bloqueado";
        //                                if (hrDetalle.estado == 6) estados = "No atendido";
        //                                if (hrDetalle.estado == 7) estados = "Mal estibado";
        //                                if (hrDetalle.estado == 8) estados = "Sin pesaje";
        //                                if (pagoXpeso == false)
        //                                {
        //                                    if (hrDetalle.cantidad == null) cantidad = 1;
        //                                    //if (hrDetalle.cantidad != null) hoja.HRcantidadTotal = hoja.HRcantidadTotal + hrDetalle.cantidad.Value;
        //                                    if (tipoServicio.id_tipo_servicio == 2)
        //                                    {
        //                                        for (int i = 1; i <= cantidad; i++)
        //                                        {
        //                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
        //                                            detalleExcel.tarifa = valorTarifa * cantidad;
        //                                            detalleExcel.numero = numero++;
        //                                            detalleExcel.id = hrDetalle.id_hoja_ruta;
        //                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
        //                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
        //                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
        //                                            detalleExcel.conductor = conductor.nombre_conductor;
        //                                            detalleExcel.estado = estados;
        //                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
        //                                            hojaDetalle.tabla.Add(detalleExcel);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
        //                                        {
        //                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
        //                                            detalleExcel.tarifa = valorTarifa;
        //                                            detalleExcel.numero = numero++;
        //                                            detalleExcel.id = hrDetalle.id_hoja_ruta;
        //                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
        //                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
        //                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
        //                                            detalleExcel.conductor = conductor.nombre_conductor;
        //                                            detalleExcel.estado = estados;
        //                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
        //                                            hojaDetalle.tabla.Add(detalleExcel);
        //                                        }
        //                                        else
        //                                        {
        //                                            if (comboUltimoDetalle == false && (hrDetalle.estado == 2))
        //                                            {
        //                                                tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
        //                                                detalleExcel.tarifa = valorTarifa;
        //                                                detalleExcel.numero = numero++;
        //                                                detalleExcel.id = hrDetalle.id_hoja_ruta;
        //                                                detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
        //                                                detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
        //                                                detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
        //                                                detalleExcel.conductor = conductor.nombre_conductor;
        //                                                detalleExcel.estado = estados;
        //                                                detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
        //                                                hojaDetalle.tabla.Add(detalleExcel);
        //                                                comboUltimoDetalle = true;
        //                                            }
        //                                            else
        //                                            {
        //                                                tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
        //                                                detalleExcel.tarifa = 0;
        //                                                detalleExcel.numero = numero++;
        //                                                detalleExcel.id = hrDetalle.id_hoja_ruta;
        //                                                detalleExcel.conductor = conductor.nombre_conductor;
        //                                                detalleExcel.estado = estados;
        //                                                detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
        //                                                detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
        //                                                detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
        //                                                detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
        //                                                hojaDetalle.tabla.Add(detalleExcel);
        //                                            }
        //                                        }
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    if (hrDetalle.peso == null) hrDetalle.peso = 1;
        //                                    tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
        //                                    double h = hrDetalle.peso.Value;   //JPIZARRO -- VARIABLE "h", "b" Y "c", UTILIZAR NOMBRE DE VARIABLES DESCRIPTIVO! 
        //                                    double b = 0;
        //                                    if (pagoXpeso == true && pagoXm3 == false)
        //                                    {
        //                                        b = h / 1000;
        //                                    }
        //                                    else
        //                                    {
        //                                        if (pagoXpeso == false && pagoXm3 == true)
        //                                        {
        //                                            b = h;
        //                                        }
        //                                    }
        //                                    double c = valorTarifa * b;
        //                                    detalleExcel.tarifa = Convert.ToInt32(Math.Round(c, 0, MidpointRounding.AwayFromZero));
        //                                    detalleExcel.numero = numero++;
        //                                    detalleExcel.id = hrDetalle.id_hoja_ruta;
        //                                    detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
        //                                    detalleExcel.conductor = conductor.nombre_conductor;
        //                                    detalleExcel.estado = estados;
        //                                    detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
        //                                    detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
        //                                    detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
        //                                    hojaDetalle.tabla.Add(detalleExcel);
        //                                }
        //                                if (cliente.cobro_especial == true)
        //                                {
        //                                    int valorReal = 0;
        //                                    if (pagoXpeso == false)
        //                                    {
        //                                        if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) cantidad = 1;
        //                                        if (cantidad != 0)
        //                                        {
        //                                            valorReal = valorTarifa * cantidad;
        //                                            hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * cantidad);
        //                                            hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad + cantidad;
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (hrDetalle.peso != null)
        //                                        {
        //                                            if (pagoXpeso == true && pagoXm3 == false)
        //                                            {
        //                                                double h = hrDetalle.peso.Value;
        //                                                double b = h / 1000;
        //                                                double c = valorTarifa * b;
        //                                                valorReal = Convert.ToInt32(c);
        //                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = Convert.ToInt32(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value / 1000)));
        //                                            }
        //                                            if (pagoXpeso == false && pagoXm3 == true)
        //                                            {
        //                                                double h = hrDetalle.peso.Value;
        //                                                double b = h;
        //                                                double c = valorTarifa * b;
        //                                                valorReal = Convert.ToInt32(c);
        //                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = Convert.ToInt32(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value)));
        //                                            }
        //                                        }

        //                                    }
        //                                    var detallePago = db.detalle_pago_franquiciados.Create();
        //                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                    detallePago.id_hdrDetalle = hrDetalle.Orden;
        //                                    detallePago.id_hdrHeader = hrEncabezado.id_hoja_ruta;
        //                                    detallePago.montoNeto = valorReal;
        //                                    detallePago.montoIva = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 0.19, 0));
        //                                    detallePago.montoBruto = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 1.19, 0));
        //                                    db.detalle_pago_franquiciados.Add(detallePago);
        //                                    db.SaveChanges();
        //                                    hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
        //                                    if (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) hoja.HRcantidadTotal = hoja.HRcantidadTotal + 1;
        //                                    hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                    hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                }
        //                                else
        //                                {
        //                                    var temporal = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
        //                                    int valorReal = 0;
        //                                    if (pagoXpeso == false)
        //                                    {
        //                                        if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) cantidad = 1;
        //                                        if (cantidad != 0)
        //                                        {

        //                                            valorReal = valorTarifa * cantidad;
        //                                            hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad;

        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        if (hrDetalle.peso != null)
        //                                        {
        //                                            if (pagoXpeso == true && pagoXm3 == false)
        //                                            {
        //                                                double h = hrDetalle.peso.Value;
        //                                                double b = h / 1000;
        //                                                double c = valorTarifa * b;
        //                                                valorReal = Convert.ToInt32(c);
        //                                                var temp1 = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
        //                                                hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad;
        //                                                //hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value / 1000));
        //                                            }
        //                                            if (pagoXpeso == false && pagoXm3 == true)
        //                                            {
        //                                                double h = hrDetalle.peso.Value;
        //                                                double b = h;
        //                                                double c = valorTarifa * b;
        //                                                valorReal = Convert.ToInt32(c);
        //                                                var temp1 = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
        //                                                hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad;
        //                                                //hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value / 1000));
        //                                            }
        //                                        }
        //                                    }
        //                                    var detallePago = db.detalle_pago_franquiciados.Create();
        //                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                    detallePago.id_hdrDetalle = hrDetalle.Orden;
        //                                    detallePago.id_hdrHeader = hrEncabezado.id_hoja_ruta;
        //                                    detallePago.montoNeto = valorReal;
        //                                    detallePago.montoIva = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 0.19, 0));
        //                                    detallePago.montoBruto = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 1.19, 0));
        //                                    db.detalle_pago_franquiciados.Add(detallePago);
        //                                    db.SaveChanges();
        //                                    var servicioExcel = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
        //                                    servicioExcel.neto = temporal.neto + valorReal;
        //                                    hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
        //                                    int iva = int.Parse(Math.Round(valorReal * 0.19, 0, MidpointRounding.AwayFromZero).ToString());
        //                                    servicioExcel.iva = servicioExcel.iva + iva;
        //                                    servicioExcel.total = servicioExcel.total + Convert.ToInt32(Math.Round(valorReal * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                }
        //                            }
        //                            hrEncabezado.fecha_pagado = DateTime.Now;
        //                            db.SaveChanges();
        //                        }
        //                        if (hoja.HRNetoTotal != 0)
        //                        {
        //                            hoja.HRcantidadTotal = 0;
        //                            foreach (var e in hoja.tablaHR.ToList())
        //                            {
        //                                hoja.HRcantidadTotal = hoja.HRcantidadTotal + e.cantidad;
        //                                e.iva = Convert.ToInt32(Math.Round(e.neto * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                e.total = Convert.ToInt32(Math.Round(e.neto * 1.19, 0, MidpointRounding.AwayFromZero));
        //                            }
        //                            hoja.HRIvaTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 0.19, 0, MidpointRounding.AwayFromZero));
        //                            hoja.HRTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 1.19, 0, MidpointRounding.AwayFromZero));
        //                        }
        //                        else
        //                        {
        //                            hoja.HRTotal = 0;
        //                            hoja.HRIvaTotal = 0;
        //                        }

        //                        hojaDetalle.total = hoja.HRNetoTotal;
        //                        hoja.subTotalNeto = hoja.HRNetoTotal;
        //                        var descuentos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0).ToList();
        //                        if (descuentos.Count() > 0)
        //                        {
        //                            hoja.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
        //                            foreach (descuento descuentoEncabezado in descuentos)
        //                            {
        //                                DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
        //                                var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
        //                                foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
        //                                {
        //                                    tablaDescuentoPrePagoExcel rowDescuento = new tablaDescuentoPrePagoExcel();
        //                                    if (descuentoDetalle.estado == 0)
        //                                    {
        //                                        int valorDesc = (descuentoEncabezado.en_uf == true) ? Convert.ToInt32(Math.Round(descuentoDetalle.valor_pagar.Value * uf.valor_uf, 0, MidpointRounding.AwayFromZero)) : Convert.ToInt32(descuentoDetalle.valor_pagar.Value);
        //                                        if (descuentoDetalle.fecha.Value.Year < DateTime.Now.Year)
        //                                        {
        //                                            var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
        //                                            rowDescuento.concepto = concepto.nombre;
        //                                            rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
        //                                            rowDescuento.oc = descuentoEncabezado.oc;
        //                                            rowDescuento.factura = descuentoEncabezado.numero_factura;
        //                                            rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
        //                                            if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
        //                                            else rowDescuento.llevaIVA = "NO";
        //                                            //if (hoja.subTotalNeto >= valorDesc)
        //                                            //{
        //                                            rowDescuento.neto = valorDesc.ToString("N0");
        //                                            rowDescuento.valorCuota = descuentoDetalle.valor_cuota.Value.ToString("N0");
        //                                            rowDescuento.valorPagar = valorDesc.ToString("N0");
        //                                            if (descuentoEncabezado.lleva_iva == true)
        //                                            {
        //                                                int valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                                int valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                                rowDescuento.total = valorPagar.ToString("N0");
        //                                                rowDescuento.iva = valorIva.ToString("N0");
        //                                                hoja.descuentoIva = hoja.descuentoIva + valorIva;
        //                                                hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
        //                                            }
        //                                            else
        //                                            {
        //                                                rowDescuento.iva = "0";
        //                                                rowDescuento.total = rowDescuento.neto;
        //                                                hoja.descuentoTotal = Convert.ToInt32(valorDesc);
        //                                            }

        //                                            hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);
        //                                            hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
        //                                            descuentoDetalle.fecha_avance = DateTime.Now;
        //                                            descuentoDetalle.valor_pagado = descuentoDetalle.valor_pagar;
        //                                            descuentoDetalle.saldo = descuentoDetalle.valor_cuota - descuentoDetalle.valor_pagar;
        //                                            descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
        //                                            var detallePago = db.detalle_pago_franquiciados.Create();
        //                                            detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                            detallePago.id_descuentoDetalle = descuentoDetalle.id_detalle_descuento;
        //                                            detallePago.id_descuentoHeader = descuentoEncabezado.id_descuento;
        //                                            detallePago.montoNeto = -Convert.ToInt32(rowDescuento.neto.Replace(".", ""));
        //                                            detallePago.montoIva = -Convert.ToInt32(rowDescuento.iva.Replace(".", ""));
        //                                            detallePago.montoBruto = -Convert.ToInt32(rowDescuento.total.Replace(".", ""));
        //                                            db.detalle_pago_franquiciados.Add(detallePago);
        //                                            db.SaveChanges();
        //                                            if (descuentoDetalle.saldo.Value == 0)
        //                                            {
        //                                                descuentoDetalle.estado = 1;
        //                                            }
        //                                            db.SaveChanges();
        //                                            hoja.tablaDescuentos.Add(rowDescuento);
        //                                            //}
        //                                            //else
        //                                            //{
        //                                            //    rowDescuento.neto = valorDesc.ToString("N0");
        //                                            //    rowDescuento.valorCuota = descuentoDetalle.valor_cuota.Value.ToString("N0");
        //                                            //    int valorPagarDif = hoja.subTotalNeto - Convert.ToInt32(valorDesc);
        //                                            //    rowDescuento.valorPagar = (valorDesc + valorPagarDif).ToString("N0");
        //                                            //    if (descuentoEncabezado.lleva_iva == true)
        //                                            //    {
        //                                            //        int valorPagar = Convert.ToInt32(Math.Round((valorDesc + valorPagarDif) * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                            //        int valorIva = Convert.ToInt32(Math.Round((valorDesc + valorPagarDif) * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                            //        rowDescuento.total = valorPagar.ToString("N0");
        //                                            //        rowDescuento.iva = valorIva.ToString("N0");
        //                                            //        hoja.descuentoIva = hoja.descuentoIva + valorIva;
        //                                            //        hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
        //                                            //    }
        //                                            //    else
        //                                            //    {
        //                                            //        rowDescuento.iva = "0";
        //                                            //        rowDescuento.total = rowDescuento.neto;
        //                                            //        hoja.descuentoTotal = Convert.ToInt32(valorDesc);
        //                                            //    }
        //                                            //    hoja.subTotalNeto = hoja.subTotalNeto - (Convert.ToInt32(valorDesc) + valorPagarDif);
        //                                            //    hoja.descuentoNeto = (Convert.ToInt32(valorDesc) + valorPagarDif) + hoja.descuentoNeto;
        //                                            //    descuentoDetalle.fecha_avance = DateTime.Now;
        //                                            //    descuentoDetalle.valor_pagado = valorDesc + valorPagarDif;
        //                                            //    descuentoDetalle.saldo = descuentoDetalle.valor_cuota - (descuentoDetalle.valor_pagar + valorPagarDif);
        //                                            //    descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
        //                                            //    if (descuentoDetalle.saldo.Value == 0)
        //                                            //    {
        //                                            //        descuentoDetalle.estado = 1;
        //                                            //    }
        //                                            //    db.SaveChanges();
        //                                            //    hoja.tablaDescuentos.Add(rowDescuento);
        //                                            //}
        //                                        }
        //                                        if (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= DateTime.Now.Month)
        //                                        {
        //                                            var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
        //                                            rowDescuento.concepto = concepto.nombre;
        //                                            rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
        //                                            rowDescuento.oc = descuentoEncabezado.oc;
        //                                            rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
        //                                            rowDescuento.factura = descuentoEncabezado.numero_factura;
        //                                            rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
        //                                            if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
        //                                            else rowDescuento.llevaIVA = "NO";
        //                                            //if (hoja.subTotalNeto >= valorDesc)
        //                                            //{
        //                                            rowDescuento.neto = valorDesc.ToString("N0");
        //                                            rowDescuento.valorCuota = descuentoDetalle.valor_cuota.Value.ToString("N0");
        //                                            rowDescuento.valorPagar = valorDesc.ToString("N0");
        //                                            if (descuentoEncabezado.lleva_iva == true)
        //                                            {
        //                                                int valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                                int valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                                rowDescuento.total = valorPagar.ToString("N0");
        //                                                rowDescuento.iva = valorIva.ToString("N0");
        //                                                hoja.descuentoIva = hoja.descuentoIva + valorIva;
        //                                                hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
        //                                            }
        //                                            else
        //                                            {
        //                                                rowDescuento.iva = "0";
        //                                                rowDescuento.total = rowDescuento.neto;
        //                                                hoja.descuentoTotal = valorDesc + hoja.descuentoTotal;
        //                                            }
        //                                            hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
        //                                            descuentoDetalle.fecha_avance = DateTime.Now;
        //                                            descuentoDetalle.valor_pagado = descuentoDetalle.valor_pagar;
        //                                            descuentoDetalle.saldo = descuentoDetalle.valor_cuota - descuentoDetalle.valor_pagar;
        //                                            descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
        //                                            if (descuentoDetalle.saldo.Value == 0)
        //                                            {
        //                                                descuentoDetalle.estado = 1;
        //                                            }
        //                                            db.SaveChanges();

        //                                            hoja.descuentoNeto = valorDesc + hoja.descuentoNeto;
        //                                            hoja.tablaDescuentos.Add(rowDescuento);
        //                                            var detallePago = db.detalle_pago_franquiciados.Create();
        //                                            detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                            detallePago.id_descuentoDetalle = descuentoDetalle.id_detalle_descuento;
        //                                            detallePago.id_descuentoHeader = descuentoEncabezado.id_descuento;
        //                                            detallePago.montoNeto = -Convert.ToInt32(rowDescuento.neto.Replace(".", ""));
        //                                            detallePago.montoIva = -Convert.ToInt32(rowDescuento.iva.Replace(".", ""));
        //                                            detallePago.montoBruto = -Convert.ToInt32(rowDescuento.total.Replace(".", ""));
        //                                            db.detalle_pago_franquiciados.Add(detallePago);
        //                                            db.SaveChanges();
        //                                            //}
        //                                            //else
        //                                            //{
        //                                            //    rowDescuento.neto = valorDesc.ToString("N0");
        //                                            //    rowDescuento.valorCuota = descuentoDetalle.valor_cuota.Value.ToString("N0");
        //                                            //    int valorPagarDif = hoja.subTotalNeto - valorDesc;
        //                                            //    rowDescuento.valorPagar = (valorDesc + valorPagarDif).ToString("N0");
        //                                            //    if (descuentoEncabezado.lleva_iva == true)
        //                                            //    {
        //                                            //        int valorPagar = Convert.ToInt32(Math.Round((valorDesc + valorPagarDif) * 1.19, 0, MidpointRounding.AwayFromZero));
        //                                            //        int valorIva = Convert.ToInt32(Math.Round((valorDesc + valorPagarDif) * 0.19, 0, MidpointRounding.AwayFromZero));
        //                                            //        rowDescuento.total = valorPagar.ToString("N0");
        //                                            //        rowDescuento.iva = valorIva.ToString("N0");
        //                                            //        hoja.descuentoIva = hoja.descuentoIva + valorIva;
        //                                            //        hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
        //                                            //    }
        //                                            //    else
        //                                            //    {
        //                                            //        rowDescuento.iva = "0";
        //                                            //        rowDescuento.total = rowDescuento.neto;
        //                                            //        hoja.descuentoTotal = valorDesc;
        //                                            //    }
        //                                            //    hoja.subTotalNeto = hoja.subTotalNeto - (valorDesc + valorPagarDif);
        //                                            //    hoja.descuentoNeto = (valorDesc + valorPagarDif) + hoja.descuentoNeto;
        //                                            //    descuentoDetalle.fecha_avance = DateTime.Now;
        //                                            //    descuentoDetalle.valor_pagado = valorDesc + valorPagarDif;
        //                                            //    descuentoDetalle.saldo = descuentoDetalle.valor_cuota - (descuentoDetalle.valor_pagar + valorPagarDif);
        //                                            //    descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
        //                                            //    if (descuentoDetalle.saldo.Value == 0)
        //                                            //    {
        //                                            //        descuentoDetalle.estado = 1;
        //                                            //    }
        //                                            //    //  db.SaveChanges();
        //                                            //    hoja.tablaDescuentos.Add(rowDescuento);
        //                                            //}
        //                                        }
        //                                    }

        //                                }
        //                                bool entroVerificacionDescuento = false;
        //                                var verificacionDetallesDescuentos = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento).ToList();
        //                                foreach (var verificacionDetalleDescuento in verificacionDetallesDescuentos)
        //                                {
        //                                    if (verificacionDetalleDescuento.estado == 0)
        //                                    {
        //                                        entroVerificacionDescuento = true;
        //                                        break;
        //                                    }
        //                                }
        //                                if (entroVerificacionDescuento == false)
        //                                {
        //                                    descuentoEncabezado.estado_descuento = 1;
        //                                    db.SaveChanges();
        //                                }
        //                            }
        //                        }
        //                        var leasings = db.leasing.Where(u => u.id_camion.Value == camion.id_camion && u.fechaFin == null).ToList();
        //                        if (leasings.Count() != 0)
        //                        {
        //                            hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
        //                            foreach (leasing leasing in leasings)
        //                            {
        //                                DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
        //                                var detallesLeasings = db.detalle_leasing.Where(u => u.id_leasing.Value == leasing.id_leasing && u.fecha_pago < fechaFinPeriodo && u.fecha_pagado == null).ToList();
        //                                if (hoja.subTotalNeto != 0)
        //                                {
        //                                    foreach (detalle_leasing detalleLeasing in detallesLeasings)
        //                                    {
        //                                        tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();
        //                                        int valorPagar = Convert.ToInt32(Math.Round(detalleLeasing.valor_pagar.Value * hoja.valorUF, 0, MidpointRounding.AwayFromZero));
        //                                        if (detalleLeasing.fecha_pago.Value.Year < DateTime.Now.Year)
        //                                        {
        //                                            //if (hoja.subTotalNeto >= valorPagar)
        //                                            //{
        //                                            descLeasing.cuota = detalleLeasing.cuota.Value;
        //                                            descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            descLeasing.neto = valorPagar.ToString("N0");
        //                                            descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
        //                                            descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
        //                                            descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
        //                                            descLeasing.valorUF = hoja.valorUF.ToString("N2");
        //                                            var detallePago = db.detalle_pago_franquiciados.Create();
        //                                            detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                            detallePago.id_leasingDetalle = detalleLeasing.id_detalle_leasing;
        //                                            detallePago.id_leasingHeader = leasing.id_leasing;
        //                                            detallePago.montoNeto = -Convert.ToInt32(descLeasing.neto.Replace(".", ""));
        //                                            detallePago.montoIva = -Convert.ToInt32(descLeasing.iva.Replace(".", ""));
        //                                            detallePago.montoBruto = -Convert.ToInt32(descLeasing.total.Replace(".", ""));
        //                                            db.detalle_pago_franquiciados.Add(detallePago);
        //                                            db.SaveChanges();
        //                                            hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
        //                                            detalleLeasing.valor_pagado = detalleLeasing.valor_pagar;
        //                                            detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagar;
        //                                            detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
        //                                            if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
        //                                            {
        //                                                detalleLeasing.fecha_pagado = DateTime.Now;
        //                                            }
        //                                            //   db.SaveChanges();
        //                                            //}
        //                                            //else
        //                                            //{
        //                                            //    int negativo = hoja.subTotalNeto - valorPagar;
        //                                            //    valorPagar = valorPagar + negativo;
        //                                            //    string valorPagarUF = (valorPagar / hoja.valorUF).ToString("N2");
        //                                            //    descLeasing.cuota = detalleLeasing.cuota.Value;
        //                                            //    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            //    descLeasing.neto = valorPagar.ToString("N0");
        //                                            //    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
        //                                            //    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            //    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
        //                                            //    descLeasing.valorPagar = valorPagarUF;
        //                                            //    descLeasing.valorUF = hoja.valorUF.ToString("N2");
        //                                            //    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
        //                                            //    hoja.subTotalNeto = 0;
        //                                            //    detalleLeasing.valor_pagado = valorPagar / hoja.valorUF;
        //                                            //    detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagado;
        //                                            //    detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
        //                                            //    if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
        //                                            //    {
        //                                            //        detalleLeasing.fecha_pagado = DateTime.Now;
        //                                            //    }
        //                                            //    //    db.SaveChanges();
        //                                            //}

        //                                            hoja.leasingNeto = valorPagar + hoja.leasingNeto;
        //                                            hoja.tablaLeasing.Add(descLeasing);
        //                                        }
        //                                        if (detalleLeasing.fecha_pago.Value.Year == DateTime.Now.Year && detalleLeasing.fecha_pago.Value.Month <= DateTime.Now.Month)
        //                                        {
        //                                            //if (hoja.subTotalNeto >= valorPagar)
        //                                            //{
        //                                            descLeasing.cuota = detalleLeasing.cuota.Value;
        //                                            descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            descLeasing.neto = valorPagar.ToString("N0");
        //                                            descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
        //                                            descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
        //                                            descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
        //                                            descLeasing.valorUF = hoja.valorUF.ToString("N2");
        //                                            hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
        //                                            detalleLeasing.valor_pagado = detalleLeasing.valor_pagar;
        //                                            detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagar;
        //                                            detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
        //                                            if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
        //                                            {
        //                                                detalleLeasing.fecha_pagado = DateTime.Now;
        //                                            }
        //                                            db.SaveChanges();
        //                                            var detallePago = db.detalle_pago_franquiciados.Create();
        //                                            detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
        //                                            detallePago.id_leasingDetalle = detalleLeasing.id_detalle_leasing;
        //                                            detallePago.id_leasingHeader = leasing.id_leasing;
        //                                            detallePago.montoNeto = -Convert.ToInt32(descLeasing.neto.Replace(".", ""));
        //                                            detallePago.montoIva = -Convert.ToInt32(descLeasing.iva.Replace(".", ""));
        //                                            detallePago.montoBruto = -Convert.ToInt32(descLeasing.total.Replace(".", ""));
        //                                            db.detalle_pago_franquiciados.Add(detallePago);
        //                                            db.SaveChanges();
        //                                            //}
        //                                            //else
        //                                            //{
        //                                            //    int negativo = hoja.subTotalNeto - valorPagar;
        //                                            //    valorPagar = valorPagar + negativo;
        //                                            //    string valorPagarUF = (valorPagar / hoja.valorUF).ToString("N2");
        //                                            //    descLeasing.cuota = detalleLeasing.cuota.Value;
        //                                            //    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            //    descLeasing.neto = valorPagar.ToString("N0");
        //                                            //    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
        //                                            //    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
        //                                            //    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
        //                                            //    descLeasing.valorPagar = valorPagarUF;
        //                                            //    descLeasing.valorUF = hoja.valorUF.ToString("N2");
        //                                            //    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
        //                                            //    hoja.subTotalNeto = 0;
        //                                            //    detalleLeasing.valor_pagado = valorPagar / hoja.valorUF;
        //                                            //    detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagado;
        //                                            //    detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
        //                                            //    if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
        //                                            //    {
        //                                            //        detalleLeasing.fecha_pagado = DateTime.Now;
        //                                            //    }
        //                                            //    //   db.SaveChanges();
        //                                            //}

        //                                            hoja.leasingNeto = valorPagar + hoja.leasingNeto;
        //                                            double temporalLeasingNeto = hoja.leasingNeto * 0.19;
        //                                            double temporalLeasingTotal = hoja.leasingNeto * 1.19;
        //                                            hoja.leasingIva = Convert.ToInt32(Math.Round(temporalLeasingNeto, 0, MidpointRounding.AwayFromZero));
        //                                            hoja.leasingTotal = Convert.ToInt32(Math.Round(temporalLeasingTotal, 0, MidpointRounding.AwayFromZero));
        //                                            hoja.tablaLeasing.Add(descLeasing);
        //                                        }
        //                                    }
        //                                    bool entroVerificacionLeasing = false;
        //                                    var verificacionLeasing = db.detalle_leasing.Where(u => u.id_leasing == leasing.id_leasing).ToList();
        //                                    foreach (var verificacionDetalle in verificacionLeasing)
        //                                    {
        //                                        if (verificacionDetalle.fecha_pagado == null)
        //                                        {
        //                                            entroVerificacionLeasing = true;
        //                                        }
        //                                    }
        //                                    if (entroVerificacionLeasing == false)
        //                                    {
        //                                        leasing.fechaFin = DateTime.Now;
        //                                        db.SaveChanges();
        //                                    }
        //                                }

        //                            }
        //                        }
        //                        hoja.totalDescuentosNeto = hoja.descuentoNeto + hoja.leasingNeto;
        //                        hoja.totalDescuentosIva = hoja.descuentoIva + hoja.leasingIva;
        //                        hoja.totalDescuentosTotal = hoja.descuentoTotal + hoja.leasingTotal;
        //                        hoja.subTotalIva = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
        //                        hoja.subTotal = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 1.19, 0, MidpointRounding.AwayFromZero));
        //                        hoja.retencionNeto = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.05, 0, MidpointRounding.AwayFromZero));
        //                        hoja.retencionIva = Convert.ToInt32(Math.Round(hoja.retencionNeto * 0.19, 0, MidpointRounding.AwayFromZero));
        //                        hoja.retencionTotal = Convert.ToInt32(Math.Round(hoja.retencionNeto * 1.19, 0, MidpointRounding.AwayFromZero));
        //                        hoja.total = hoja.subTotal - hoja.retencionTotal;
        //                        hoja.totalNeto = hoja.subTotalNeto - hoja.retencionNeto;
        //                        hoja.totalIva = hoja.subTotalIva - hoja.retencionIva;
        //                        montoNeto = montoNeto + hoja.subTotalNeto;
        //                        montoBruto = montoBruto + hoja.subTotal;
        //                        model.hojaCamion.Add(hoja);
        //                        model.hojaDetalleCamion.Add(hojaDetalle);
        //                    }
        //                }
        //                model.hojaTotal = new camionExcelFranquiciadoPago();
        //                foreach (var item in model.hojaCamion)
        //                {
        //                    model.hojaTotal.descuentoIva = item.descuentoIva + model.hojaTotal.descuentoIva;
        //                    model.hojaTotal.descuentoNeto = item.descuentoNeto + model.hojaTotal.descuentoNeto;
        //                    model.hojaTotal.descuentoTotal = item.descuentoTotal + model.hojaTotal.descuentoTotal;
        //                    model.hojaTotal.HRcantidadTotal = item.HRcantidadTotal + model.hojaTotal.HRcantidadTotal;
        //                    model.hojaTotal.HRIvaTotal = item.HRIvaTotal + model.hojaTotal.HRIvaTotal;
        //                    model.hojaTotal.HRNetoTotal = item.HRNetoTotal + model.hojaTotal.HRNetoTotal;
        //                    model.hojaTotal.HRTotal = item.HRTotal + model.hojaTotal.HRTotal;
        //                    model.hojaTotal.leasingIva = item.leasingIva + model.hojaTotal.leasingIva;
        //                    model.hojaTotal.leasingNeto = item.leasingNeto + model.hojaTotal.leasingNeto;
        //                    model.hojaTotal.leasingTotal = item.leasingTotal + model.hojaTotal.leasingTotal;
        //                    model.hojaTotal.patenteCamion = "Totales";
        //                    model.hojaTotal.periodo = item.periodo;
        //                    model.hojaTotal.retencionIva = item.retencionIva + model.hojaTotal.retencionIva;
        //                    model.hojaTotal.retencionNeto = item.retencionNeto + model.hojaTotal.retencionTotal;
        //                    model.hojaTotal.retencionTotal = item.retencionTotal + model.hojaTotal.retencionTotal;
        //                    model.hojaTotal.subTotal = item.subTotal + model.hojaTotal.subTotal;
        //                    model.hojaTotal.subTotalIva = item.subTotalIva + model.hojaTotal.subTotalIva;
        //                    model.hojaTotal.subTotalNeto = item.subTotalNeto + model.hojaTotal.subTotalNeto;
        //                    if (model.hojaTotal.tablaDescuentos == null) model.hojaTotal.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
        //                    if (item.tablaDescuentos != null)
        //                    {
        //                        foreach (var desc in item.tablaDescuentos)
        //                        {
        //                            model.hojaTotal.tablaDescuentos.Add(desc);
        //                        }
        //                    }
        //                    if (model.hojaTotal.tablaHR == null) model.hojaTotal.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
        //                    foreach (var hr in item.tablaHR)
        //                    {
        //                        if (model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault() == null)
        //                        {
        //                            tablaHojasDeRutaPorTipoExcel nuev = new tablaHojasDeRutaPorTipoExcel();
        //                            nuev.tipo = hr.tipo;
        //                            nuev.cantidad = hr.cantidad;
        //                            nuev.iva = hr.iva;
        //                            nuev.neto = hr.neto;
        //                            nuev.total = hr.total;
        //                            model.hojaTotal.tablaHR.Add(nuev);
        //                        }
        //                        else
        //                        {
        //                            var row = model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault();
        //                            int cant = hr.cantidad;
        //                            model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().cantidad = cant + row.cantidad;
        //                            model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().iva = hr.iva + row.iva;
        //                            model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().neto = hr.neto + row.neto;
        //                            model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().total = hr.total + row.total;
        //                        }
        //                    }

        //                    if (model.hojaTotal.tablaLeasing == null) model.hojaTotal.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
        //                    if (item.tablaLeasing != null)
        //                    {
        //                        foreach (var leasing in item.tablaLeasing)
        //                        {
        //                            model.hojaTotal.tablaLeasing.Add(leasing);
        //                        }
        //                    }
        //                    model.hojaTotal.total = item.total + model.hojaTotal.total;
        //                    model.hojaTotal.totalDescuentosIva = item.totalDescuentosIva + model.hojaTotal.totalDescuentosIva;
        //                    model.hojaTotal.totalDescuentosNeto = item.totalDescuentosNeto + model.hojaTotal.totalDescuentosNeto;
        //                    model.hojaTotal.totalDescuentosTotal = item.totalDescuentosTotal + model.hojaTotal.totalDescuentosTotal;
        //                    model.hojaTotal.totalIva = item.totalIva + model.hojaTotal.totalIva;
        //                    model.hojaTotal.totalNeto = item.totalNeto + model.hojaTotal.totalNeto;
        //                    model.hojaTotal.valorUF = item.valorUF;
        //                }
        //                if (franquiciado.anticipo == true && franquiciado.porcentaje_anticipo > 0)
        //                {
        //                    string periodo = fechaHasta.AddMonths(-1).Month.ToString() + "-" + fechaHasta.AddMonths(-1).Year.ToString();
        //                    var historial = db.pago_franquiciados.Where(u => u.periodo.Equals(periodo)).FirstOrDefault();
        //                    if (historial != null)
        //                    {
        //                        double montoAnticipo = ((historial.montoNeto * 0.5) * (franquiciado.porcentaje_anticipo.Value * 0.01));
        //                        model.anticipo = Convert.ToInt32(Math.Round(montoAnticipo, 0, MidpointRounding.AwayFromZero));
        //                        model.hojaTotal.totalNeto = model.hojaTotal.totalNeto - model.anticipo;
        //                        model.hojaTotal.totalIva = Convert.ToInt32(Math.Round(model.hojaTotal.totalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
        //                        model.hojaTotal.total = model.hojaTotal.totalIva + model.hojaTotal.totalNeto;
        //                    }
        //                }
        //                var pago = db.pago_franquiciados.Create();
        //                if (db.pago_franquiciados.Count() == 0) pago.id_pago = 1;
        //                else pago.id_pago = db.pago_franquiciados.Max(u => u.id_pago) + 1;
        //                pago.id_franquiciado = franquiciado.id_franquiciado;
        //                pago.fechaInicio = fechaDesde.Date;
        //                pago.fechaFin = fechaHasta.Date;
        //                pago.montoNeto = model.hojaTotal.totalNeto;
        //                pago.montoBruto = model.hojaTotal.total;
        //                pago.periodo = calculoPeriodo(fechaDesde, fechaHasta);
        //                pago.id_unidad = unidad;
        //                db.pago_franquiciados.Add(pago);
        //                db.SaveChanges();
        //            }

        //            string mydocpath = HttpContext.Server.MapPath("~/HistorialPagosFranquiciados");
        //            #region test
        //            using (StreamWriter writer = new StreamWriter(mydocpath + @"\PAGADO_" + model.rutFranquiciado + "_" + model.fechaInicio.Replace("/", "-") + "_" + model.fechaFin.Replace("/", "-") + ".xml", true))
        //            {
        //                writer.WriteLine(" <?xml version=\"1.0\"?>");
        //                writer.WriteLine("           <?mso-application progid=\"Excel.Sheet\"?>");
        //                writer.WriteLine("           <Workbook xmlns=\"urn:schemas-microsoft-com:office:spreadsheet\"");
        //                writer.WriteLine("            xmlns:o=\"urn:schemas-microsoft-com:office:office\"");
        //                writer.WriteLine("            xmlns:x=\"urn:schemas-microsoft-com:office:excel\"");
        //                writer.WriteLine("            xmlns:ss=\"urn:schemas-microsoft-com:office:spreadsheet\"");
        //                writer.WriteLine("            xmlns:html=\"http://www.w3.org/TR/REC-html40\">");
        //                writer.WriteLine("            <DocumentProperties xmlns=\"urn:schemas-microsoft-com:office:office\">");
        //                writer.WriteLine("             <Author>Matias Contreras</Author>");
        //                writer.WriteLine("             <LastAuthor>Matias Contreras</LastAuthor>");
        //                writer.WriteLine("             <Created>2014-12-12T23:30:43Z</Created>");
        //                writer.WriteLine("             <LastSaved>2014-12-13T02:25:48Z</LastSaved>");
        //                writer.WriteLine("             <Version>15.00</Version>");
        //                writer.WriteLine("            </DocumentProperties>");
        //                writer.WriteLine("            <OfficeDocumentSettings xmlns=\"urn:schemas-microsoft-com:office:office\">");
        //                writer.WriteLine("             <AllowPNG/>");
        //                writer.WriteLine("            </OfficeDocumentSettings>");
        //                writer.WriteLine("            <ExcelWorkbook xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                writer.WriteLine("             <SupBook>");
        //                writer.WriteLine("              <Path>Modelo de EEPP (1).xlsm</Path>");
        //                writer.WriteLine("              <SheetName>Datos y BD</SheetName>");
        //                writer.WriteLine("              <SheetName>BD Consolidada Mar</SheetName>");
        //                writer.WriteLine("              <SheetName>246-247 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>246-247 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>246(4)</SheetName>");
        //                writer.WriteLine("              <SheetName>Camion1</SheetName>");
        //                writer.WriteLine("              <SheetName>Camion2</SheetName>");
        //                writer.WriteLine("              <SheetName>320 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>321 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>321 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>323 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>323 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>323(3)</SheetName>");
        //                writer.WriteLine("              <SheetName>323(4)</SheetName>");
        //                writer.WriteLine("              <SheetName>337 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>337 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>342 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>342 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>342(3)</SheetName>");
        //                writer.WriteLine("              <SheetName>344 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>344 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>382-442 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>382-442 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>396 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>396(4)</SheetName>");
        //                writer.WriteLine("              <SheetName>396 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>408 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>408 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>427 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>427 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>427(3)</SheetName>");
        //                writer.WriteLine("              <SheetName>436 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>436 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>436(3)</SheetName>");
        //                writer.WriteLine("              <SheetName>442 (3)</SheetName>");
        //                writer.WriteLine("              <SheetName>442(4)</SheetName>");
        //                writer.WriteLine("              <SheetName>346-439 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>439 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>443 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>443 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>447 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>447 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>03-04 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>03-04 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>448 EP</SheetName>");
        //                writer.WriteLine("              <SheetName>448 DV</SheetName>");
        //                writer.WriteLine("              <SheetName>Hoja6</SheetName>");
        //                writer.WriteLine("              <SheetName>Hoja5</SheetName>");
        //                writer.WriteLine("              <SheetName>344(7)</SheetName>");
        //                writer.WriteLine("              <SheetName>S.I. EP</SheetName>");
        //                writer.WriteLine("              <SheetName>S.I. DV</SheetName>");
        //                writer.WriteLine("              <SheetName>HOJA 1</SheetName>");
        //                writer.WriteLine("              <SheetName>Hoja1</SheetName>");
        //                writer.WriteLine("              <SheetName>Hoja3</SheetName>");
        //                writer.WriteLine("              <SheetName>Hoja4</SheetName>");
        //                writer.WriteLine("              <SheetName>REC EP</SheetName>");
        //                writer.WriteLine("              <SheetName> REC DV</SheetName>");
        //                writer.WriteLine("              <SheetName>INFORME</SheetName>");
        //                writer.WriteLine("              <SheetName>Det_Franquicias Julio14</SheetName>");
        //                writer.WriteLine("              <SheetName>Res_Franquicias Julio 14</SheetName>");
        //                writer.WriteLine("              <SheetName>COMBUSTIBLE_ENE-14</SheetName>");
        //                writer.WriteLine("              <SheetName>MOLYMET</SheetName>");
        //                writer.WriteLine("              <SheetName>EL_SOLDADO</SheetName>");
        //                writer.WriteLine("              <SheetName>LOS BRONCES</SheetName>");
        //                writer.WriteLine("              <SheetName>Modelo de EEPP (1)</SheetName>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>0</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>1</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>2</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>3</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>4</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>5</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>6</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>7</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>8</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>9</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>10</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>11</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>12</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>13</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>14</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>15</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>16</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>17</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>18</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>19</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>20</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>21</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>22</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>23</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>24</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>25</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>26</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>27</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>28</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>29</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>30</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>31</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>32</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>33</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>34</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>35</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>36</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>37</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>38</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>39</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>40</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>41</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>42</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>43</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>44</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>45</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>46</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>47</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>48</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>49</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>50</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>51</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>52</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>53</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>54</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>55</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>56</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>57</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>58</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>59</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>60</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>61</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>62</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("              <Xct>");
        //                writer.WriteLine("               <Count>0</Count>");
        //                writer.WriteLine("               <SheetIndex>63</SheetIndex>");
        //                writer.WriteLine("              </Xct>");
        //                writer.WriteLine("             </SupBook>");
        //                writer.WriteLine("             <WindowHeight>8385</WindowHeight>");
        //                writer.WriteLine("             <WindowWidth>19200</WindowWidth>");
        //                writer.WriteLine("             <WindowTopX>0</WindowTopX>");
        //                writer.WriteLine("             <WindowTopY>0</WindowTopY>");
        //                writer.WriteLine("             <ProtectStructure>False</ProtectStructure>");
        //                writer.WriteLine("             <ProtectWindows>False</ProtectWindows>");
        //                writer.WriteLine("            </ExcelWorkbook>");
        //                writer.WriteLine("            <Styles>");
        //                writer.WriteLine("             <Style ss:ID=\"Default\" ss:Name=\"Normal\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders/>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("              <Protection/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s16\" ss:Name=\"Millares\">");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374004784\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"14\" ss:Color=\"#000000\"");
        //                writer.WriteLine("               ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374004804\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374004844\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374012728\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //                writer.WriteLine("               ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374012748\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"14\" ss:Color=\"#000000\"");
        //                writer.WriteLine("               ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374012768\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374012788\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"m777374008736\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"18\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"m777374008756\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"m777374008776\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"m777374008796\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"m777374008816\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"m777374008836\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s62\">");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s63\">");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s64\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s76\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s77\">");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s78\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s79\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s89\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s90\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s91\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s99\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s100\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s101\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s102\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s103\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s105\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s112\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"11\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s113\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s114\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s131\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s139\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s140\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s141\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s143\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s159\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s160\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s161\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\" ss:WrapText=\"1\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s162\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s163\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s164\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s166\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s167\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s168\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s169\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s170\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s171\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s172\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s173\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s174\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s175\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s176\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s177\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s178\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s179\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s180\">");
        //writer.WriteLine("              <Alignment ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s181\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s182\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s183\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s184\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s185\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s186\">");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s187\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s188\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s192\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s194\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s195\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s196\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s197\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s198\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s199\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s200\" ss:Parent=\"s16\">");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s201\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Right\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s202\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s203\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s204\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s205\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s206\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s207\">");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s208\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s209\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s210\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"");
        //writer.WriteLine("               ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s211\">");
        //writer.WriteLine("              <Borders/>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s212\">");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Size=\"12\" ss:Color=\"#000000\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s213\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //writer.WriteLine("              </Borders>");
        //writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //writer.WriteLine("              <NumberFormat/>");
        //writer.WriteLine("             </Style>");
        //writer.WriteLine("             <Style ss:ID=\"s214\">");
        //writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //writer.WriteLine("              <Borders>");
        //writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s215\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Center\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\" ss:Bold=\"1\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#C0C0C0\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s224\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s225\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Top\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s226\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s227\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders/>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s229\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Left\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s232\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s234\">");
        //                writer.WriteLine("              <Alignment ss:Horizontal=\"Center\" ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s237\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s238\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Right\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("             <Style ss:ID=\"s239\">");
        //                writer.WriteLine("              <Alignment ss:Vertical=\"Bottom\"/>");
        //                writer.WriteLine("              <Borders>");
        //                writer.WriteLine("               <Border ss:Position=\"Bottom\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("               <Border ss:Position=\"Left\" ss:LineStyle=\"Continuous\" ss:Weight=\"1\"/>");
        //                writer.WriteLine("              </Borders>");
        //                writer.WriteLine("              <Font ss:FontName=\"Calibri\" x:Family=\"Swiss\" ss:Color=\"#000000\"/>");
        //                writer.WriteLine("              <Interior ss:Color=\"#FFFFFF\" ss:Pattern=\"Solid\"/>");
        //                writer.WriteLine("              <NumberFormat/>");
        //                writer.WriteLine("             </Style>");
        //                writer.WriteLine("            </Styles>");
        //                writer.WriteLine("            <Names>");
        //                writer.WriteLine("             <NamedRange ss:Name=\"N°_interno\"");
        //                writer.WriteLine("              ss:RefersTo=\"='C:\\Users\\Matias\\Downloads\\Modelo de EEPP (1).xlsm'!Tabla6[N° interno]\"/>");
        //                writer.WriteLine("            </Names>");
        //                writer.WriteLine("                   <Worksheet ss:Name=\"Total\">");
        //                writer.WriteLine("     <Table ss:ExpandedColumnCount=\"12\" ss:ExpandedRowCount=\"1000\" x:FullColumns=\"1\"");
        //                writer.WriteLine("      x:FullRows=\"1\" ss:StyleID=\"s62\" ss:DefaultColumnWidth=\"60\"");
        //                writer.WriteLine("      ss:DefaultRowHeight=\"15\">");
        //                writer.WriteLine("      <Column ss:StyleID=\"s62\" ss:Hidden=\"1\" ss:AutoFitWidth=\"0\"/>");
        //                writer.WriteLine("      <Column ss:StyleID=\"s62\" ss:Width=\"150\"/>");
        //                writer.WriteLine("      <Column ss:Index=\"9\" ss:StyleID=\"s62\" ss:Width=\"66\"/>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"3\" ss:StyleID=\"m777374008736\"><Data");
        //                writer.WriteLine("         ss:Type=\"String\">ESTADO DE PAGO  UNIDAD CENTRO                 RESITER INDUSTRIAL S.A</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s79\"><Data ss:Type=\"String\">N° interno</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008756\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008776\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008796\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s89\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s90\"><Data ss:Type=\"String\">Periodo</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s90\"><Data ss:Type=\"String\">" + model.hojaTotal.periodo + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s91\"><Data ss:Type=\"String\">Franquicia</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374008816\"><Data ss:Type=\"String\">" + model.franquiciado.TrimEnd() + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s100\"><Data ss:Type=\"String\">Desde</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s101\"><Data ss:Type=\"String\">" + model.fechaInicio + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s102\"><Data ss:Type=\"String\">RUT franquicia</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374008836\"><Data ss:Type=\"String\">" + model.rutFranquiciado + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s100\"><Data ss:Type=\"String\">Hasta</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s101\"><Data ss:Type=\"String\">" + model.fechaFin + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s103\"><Data ss:Type=\"String\">Patente</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s105\"><Data ss:Type=\"String\">" + model.hojaTotal.patenteCamion + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s105\"><Data ss:Type=\"String\"></Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374012728\"><Data ss:Type=\"String\"></Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s112\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s113\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"1\" ss:StyleID=\"m777374012748\"><Data");
        //                writer.WriteLine("         ss:Type=\"String\">INGRESOS</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"6\" ss:StyleID=\"m777374012768\"><Data ss:Type=\"String\">DETALLE</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Nº</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                if (model.hojaTotal.tablaHR != null)
        //                {
        //                    foreach (var detalle in model.hojaTotal.tablaHR)
        //                    {
        //                        writer.WriteLine("");
        //                        writer.WriteLine("                  <Row ss:AutoFitHeight=\"0\">");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("                <Cell ss:MergeAcross=\"6\" ss:StyleID=\"m777374012788\"><Data ss:Type=\"String\">" + detalle.tipo + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s139\"><Data ss:Type=\"Number\">" + detalle.cantidad + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s140\"><Data ss:Type=\"Number\">" + detalle.neto + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s139\"><Data ss:Type=\"Number\">" + detalle.iva + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s141\"><Data ss:Type=\"Number\">" + detalle.total + "</Data></Cell>");
        //                        writer.WriteLine("              </Row>");
        //                    }
        //                }
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s159\"><Data ss:Type=\"String\">TOTAL INGRESOS</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s161\"><Data ss:Type=\"Number\">" + model.hojaTotal.HRcantidadTotal + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.HRNetoTotal + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.HRIvaTotal + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.HRTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"1\" ss:StyleID=\"m777374004784\"><Data");
        //                writer.WriteLine("         ss:Type=\"String\">DESCUENTOS</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s143\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s143\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s166\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s167\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s169\"><Data ss:Type=\"String\">CONCEPTO</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Cuota</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s171\"><Data ss:Type=\"String\">Fecha</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s172\"><Data ss:Type=\"String\">Valor UF</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor cuota UF</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor a pagar UF</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                if (model.hojaTotal.tablaLeasing != null)
        //                {
        //                    foreach (var detalleLeasing in model.hojaTotal.tablaLeasing)
        //                    {
        //                        writer.WriteLine("    <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">LEASING</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s175\"/>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s176\"/>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + detalleLeasing.cuota + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">" + detalleLeasing.periodo + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorUF + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorCuota + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorPagar + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.neto + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.iva + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.total + "</Data></Cell>");
        //                        writer.WriteLine("     </Row>");
        //                    }
        //                }
        //                else
        //                {
        //                    writer.WriteLine("        <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">No aplica</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s175\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s176\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s178\"><Data ss:Type=\"Number\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                }
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">SUB-TOTAL</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s184\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + model.hojaTotal.leasingNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + model.hojaTotal.leasingIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + model.hojaTotal.leasingTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s187\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s187\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("        <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s169\"><Data ss:Type=\"String\">CONCEPTO</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">OC</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Factura</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Total Cuotas</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s171\"><Data ss:Type=\"String\">Cuota Actual</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s172\"><Data ss:Type=\"String\">Fecha</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor cuota</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor a pagar</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                if (model.hojaTotal.tablaDescuentos != null)
        //                {
        //                    foreach (var descuento in model.hojaTotal.tablaDescuentos)
        //                    {
        //                        writer.WriteLine("             <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">" + descuento.concepto + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s176\"><Data ss:Type=\"String\">" + descuento.oc + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + descuento.factura + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + descuento.cuotaTotal + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s179\"><Data ss:Type=\"Number\">" + descuento.cuotaActual + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.fecha + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.valorCuota + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.valorPagar + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.neto + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.iva + "</Data></Cell>");
        //                        writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.total + "</Data></Cell>");
        //                        writer.WriteLine("     </Row>");
        //                    }
        //                }
        //                else
        //                {
        //                    writer.WriteLine("           <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">No aplica</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s175\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s176\"/>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s178\"><Data ss:Type=\"Number\"></Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                    writer.WriteLine("               </Row>");
        //                }
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">SUB-TOTAL</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s184\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s197\"><Data ss:Type=\"Number\">" + model.hojaTotal.descuentoNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + model.hojaTotal.descuentoIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + model.hojaTotal.descuentoTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s199\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s200\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s201\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">TOTAL DESCUENTOS</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s203\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.totalDescuentosNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.totalDescuentosIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + model.hojaTotal.totalDescuentosTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">ANTICIPO</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"><Data ss:Type=\"Number\">" + model.anticipo + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\"></Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\"></Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">SUBTOTAL</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"><Data ss:Type=\"Number\">" + model.hojaTotal.subTotalNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.subTotalIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.subTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">RETENCIÓN 5%</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s208\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s206\"><Data ss:Type=\"Number\">" + model.hojaTotal.retencionNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.retencionIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.retencionTotal + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s209\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.totalNeto + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.totalIva + "</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + model.hojaTotal.total + "</Data></Cell>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s211\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s212\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s212\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s213\"><Data ss:Type=\"String\">AUTORIZACIONES</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s215\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374004844\"><Data ss:Type=\"String\">GERENCIA RESIDUOS INDUSTRIALES</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s213\"><Data ss:Type=\"String\">CONTRALORIA</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s215\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s192\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s224\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s225\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"15.75\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"21\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"15.75\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s234\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s238\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s239\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s238\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                writer.WriteLine("      </Row>");
        //                writer.WriteLine("     </Table>");
        //                writer.WriteLine("     <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                writer.WriteLine("      <PageSetup>");
        //                writer.WriteLine("       <Header x:Margin=\"0.3\"/>");
        //                writer.WriteLine("       <Footer x:Margin=\"0.3\"/>");
        //                writer.WriteLine("       <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>");
        //                writer.WriteLine("      </PageSetup>");
        //                writer.WriteLine("      <Unsynced/>");
        //                writer.WriteLine("      <Print>");
        //                writer.WriteLine("       <ValidPrinterInfo/>");
        //                writer.WriteLine("       <VerticalResolution>0</VerticalResolution>");
        //                writer.WriteLine("       <NumberofCopies>0</NumberofCopies>");
        //                writer.WriteLine("      </Print>");
        //                writer.WriteLine("      <Selected/>");
        //                writer.WriteLine("      <LeftColumnVisible>1</LeftColumnVisible>");
        //                writer.WriteLine("      <Panes>");
        //                writer.WriteLine("       <Pane>");
        //                writer.WriteLine("        <Number>3</Number>");
        //                writer.WriteLine("        <ActiveRow>14</ActiveRow>");
        //                writer.WriteLine("        <ActiveCol>1</ActiveCol>");
        //                writer.WriteLine("        <RangeSelection>R15C2:R15C8</RangeSelection>");
        //                writer.WriteLine("       </Pane>");
        //                writer.WriteLine("      </Panes>");
        //                writer.WriteLine("      <ProtectObjects>False</ProtectObjects>");
        //                writer.WriteLine("      <ProtectScenarios>False</ProtectScenarios>");
        //                writer.WriteLine("     </WorksheetOptions>");
        //                writer.WriteLine("     <DataValidation xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                writer.WriteLine("      <Range>R7C3,R7C5,R7C7</Range>");
        //                writer.WriteLine("      <Type>List</Type>");
        //                writer.WriteLine("      <Value>N°_interno</Value>");
        //                writer.WriteLine("     </DataValidation>");
        //                writer.WriteLine("    </Worksheet>");

        //                #region hojasCamion
        //                foreach (var hoja in model.hojaCamion)
        //                {
        //                    writer.WriteLine("                   <Worksheet ss:Name=\"" + hoja.patenteCamion + "\">");
        //                    writer.WriteLine("     <Table ss:ExpandedColumnCount=\"12\" ss:ExpandedRowCount=\"1000\" x:FullColumns=\"1\"");
        //                    writer.WriteLine("      x:FullRows=\"1\" ss:StyleID=\"s62\" ss:DefaultColumnWidth=\"60\"");
        //                    writer.WriteLine("      ss:DefaultRowHeight=\"15\">");
        //                    writer.WriteLine("      <Column ss:StyleID=\"s62\" ss:Hidden=\"1\" ss:AutoFitWidth=\"0\"/>");
        //                    writer.WriteLine("      <Column ss:StyleID=\"s62\" ss:Width=\"150\"/>");
        //                    writer.WriteLine("      <Column ss:Index=\"9\" ss:StyleID=\"s62\" ss:Width=\"66\"/>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"3\" ss:StyleID=\"m777374008736\"><Data");
        //                    writer.WriteLine("         ss:Type=\"String\">ESTADO DE PAGO  UNIDAD CENTRO                 RESITER INDUSTRIAL S.A</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s76\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s79\"><Data ss:Type=\"String\">N° interno</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008756\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008776\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374008796\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s89\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s90\"><Data ss:Type=\"String\">Periodo</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s90\"><Data ss:Type=\"String\">" + hoja.periodo + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s91\"><Data ss:Type=\"String\">Franquicia</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374008816\"><Data ss:Type=\"String\">" + model.franquiciado.TrimEnd() + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s100\"><Data ss:Type=\"String\">Desde</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s101\"><Data ss:Type=\"String\">" + model.fechaInicio + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s102\"><Data ss:Type=\"String\">RUT franquicia</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374008836\"><Data ss:Type=\"String\">" + model.rutFranquiciado + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s100\"><Data ss:Type=\"String\">Hasta</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s101\"><Data ss:Type=\"String\">" + model.fechaFin + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s103\"><Data ss:Type=\"String\">Patente</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s105\"><Data ss:Type=\"String\">" + hoja.patenteCamion + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s105\"><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"m777374012728\"><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s99\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s112\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s113\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"1\" ss:StyleID=\"m777374012748\"><Data");
        //                    writer.WriteLine("         ss:Type=\"String\">INGRESOS</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"6\" ss:StyleID=\"m777374012768\"><Data ss:Type=\"String\">DETALLE</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Nº</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s131\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    foreach (var detalle in hoja.tablaHR)
        //                    {
        //                        writer.WriteLine("");
        //                        writer.WriteLine("                  <Row ss:AutoFitHeight=\"0\">");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("                <Cell ss:MergeAcross=\"6\" ss:StyleID=\"m777374012788\"><Data ss:Type=\"String\">" + detalle.tipo + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s139\"><Data ss:Type=\"Number\">" + detalle.cantidad + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s140\"><Data ss:Type=\"Number\">" + detalle.neto + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s139\"><Data ss:Type=\"Number\">" + detalle.iva + "</Data></Cell>");
        //                        writer.WriteLine("                <Cell ss:StyleID=\"s141\"><Data ss:Type=\"Number\">" + detalle.total + "</Data></Cell>");
        //                        writer.WriteLine("              </Row>");
        //                    }
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s159\"><Data ss:Type=\"String\">TOTAL INGRESOS</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s160\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s161\"><Data ss:Type=\"Number\">" + hoja.HRcantidadTotal + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.HRNetoTotal + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.HRIvaTotal + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.HRTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"10\" ss:MergeDown=\"1\" ss:StyleID=\"m777374004784\"><Data");
        //                    writer.WriteLine("         ss:Type=\"String\">DESCUENTOS</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s164\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s143\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s143\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s166\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s167\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s168\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s169\"><Data ss:Type=\"String\">CONCEPTO</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Cuota</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s171\"><Data ss:Type=\"String\">Fecha</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s172\"><Data ss:Type=\"String\">Valor UF</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor cuota UF</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor a pagar UF</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    if (hoja.tablaLeasing != null)
        //                    {
        //                        foreach (var detalleLeasing in hoja.tablaLeasing)
        //                        {
        //                            writer.WriteLine("    <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s63\"/>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">LEASING</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s175\"/>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s176\"/>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + detalleLeasing.cuota + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">" + detalleLeasing.periodo + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorUF + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorCuota + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.valorPagar + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.neto + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.iva + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + detalleLeasing.total + "</Data></Cell>");
        //                            writer.WriteLine("     </Row>");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        writer.WriteLine("        <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">No aplica</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s175\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s176\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s178\"><Data ss:Type=\"Number\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("      </Row>");
        //                    }
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">SUB-TOTAL</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s184\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + hoja.leasingNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + hoja.leasingIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + hoja.leasingTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s186\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s187\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s187\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s188\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("        <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s169\"><Data ss:Type=\"String\">CONCEPTO</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">OC</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Factura</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s170\"><Data ss:Type=\"String\">Total Cuotas</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s171\"><Data ss:Type=\"String\">Cuota Actual</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s172\"><Data ss:Type=\"String\">Fecha</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor cuota</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Valor a pagar</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Neto</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Iva</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s173\"><Data ss:Type=\"String\">Total</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    if (hoja.tablaDescuentos != null)
        //                    {
        //                        foreach (var descuento in hoja.tablaDescuentos)
        //                        {
        //                            writer.WriteLine("             <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s63\"/>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">" + descuento.concepto + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s176\"><Data ss:Type=\"String\">" + descuento.oc + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + descuento.factura + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s178\"><Data ss:Type=\"String\">" + descuento.cuotaTotal + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s179\"><Data ss:Type=\"Number\">" + descuento.cuotaActual + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.fecha + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.valorCuota + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.valorPagar + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.neto + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.iva + "</Data></Cell>");
        //                            writer.WriteLine("      <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">" + descuento.total + "</Data></Cell>");
        //                            writer.WriteLine("     </Row>");
        //                        }
        //                    }
        //                    else
        //                    {
        //                        writer.WriteLine("           <Row ss:AutoFitHeight=\"0\" ss:Height=\"26.25\">");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s63\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s174\"><Data ss:Type=\"String\">No aplica</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s175\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s176\"/>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s178\"><Data ss:Type=\"Number\"></Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s179\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("          <Cell ss:StyleID=\"s180\"><Data ss:Type=\"String\">0</Data></Cell>");
        //                        writer.WriteLine("               </Row>");
        //                    }
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">SUB-TOTAL</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s183\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s184\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s197\"><Data ss:Type=\"Number\">" + hoja.descuentoNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + hoja.descuentoIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s185\"><Data ss:Type=\"Number\">" + hoja.descuentoTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s198\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s199\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s200\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s201\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s202\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s181\"><Data ss:Type=\"String\">TOTAL DESCUENTOS</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s182\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s203\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.totalDescuentosNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.totalDescuentosIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s162\"><Data ss:Type=\"Number\">" + hoja.totalDescuentosTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s163\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s77\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">SUBTOTAL</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s206\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s206\"><Data ss:Type=\"Number\">" + hoja.subTotalNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.subTotalIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.subTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">RETENCIÓN 5%</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s208\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s206\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s206\"><Data ss:Type=\"Number\">" + hoja.retencionNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.retencionIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.retencionTotal + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s205\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s209\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.totalNeto + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.totalIva + "</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s207\"><Data ss:Type=\"Number\">" + hoja.total + "</Data></Cell>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s210\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s211\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s212\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s212\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s78\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s64\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s213\"><Data ss:Type=\"String\">AUTORIZACIONES</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s215\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"5\" ss:StyleID=\"m777374004844\"><Data ss:Type=\"String\">GERENCIA RESIDUOS INDUSTRIALES</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s213\"><Data ss:Type=\"String\">CONTRALORIA</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s214\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s215\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s192\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s224\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s225\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s204\"><Data ss:Type=\"String\">TOTAL LIQUIDO OPERACIÓN</Data></Cell>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"15.75\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"21\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\" ss:Height=\"15.75\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s229\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"1\" ss:StyleID=\"s114\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s226\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s227\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s232\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("       <Cell ss:MergeAcross=\"2\" ss:StyleID=\"s234\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s238\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s239\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s237\"/>");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s238\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("      <Row ss:AutoFitHeight=\"0\">");
        //                    writer.WriteLine("       <Cell ss:StyleID=\"s63\"/>");
        //                    writer.WriteLine("      </Row>");
        //                    writer.WriteLine("     </Table>");
        //                    writer.WriteLine("     <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                    writer.WriteLine("      <PageSetup>");
        //                    writer.WriteLine("       <Header x:Margin=\"0.3\"/>");
        //                    writer.WriteLine("       <Footer x:Margin=\"0.3\"/>");
        //                    writer.WriteLine("       <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>");
        //                    writer.WriteLine("      </PageSetup>");
        //                    writer.WriteLine("      <Unsynced/>");
        //                    writer.WriteLine("      <Print>");
        //                    writer.WriteLine("       <ValidPrinterInfo/>");
        //                    writer.WriteLine("       <VerticalResolution>0</VerticalResolution>");
        //                    writer.WriteLine("       <NumberofCopies>0</NumberofCopies>");
        //                    writer.WriteLine("      </Print>");
        //                    writer.WriteLine("      <Selected/>");
        //                    writer.WriteLine("      <LeftColumnVisible>1</LeftColumnVisible>");
        //                    writer.WriteLine("      <Panes>");
        //                    writer.WriteLine("       <Pane>");
        //                    writer.WriteLine("        <Number>3</Number>");
        //                    writer.WriteLine("        <ActiveRow>14</ActiveRow>");
        //                    writer.WriteLine("        <ActiveCol>1</ActiveCol>");
        //                    writer.WriteLine("        <RangeSelection>R15C2:R15C8</RangeSelection>");
        //                    writer.WriteLine("       </Pane>");
        //                    writer.WriteLine("      </Panes>");
        //                    writer.WriteLine("      <ProtectObjects>False</ProtectObjects>");
        //                    writer.WriteLine("      <ProtectScenarios>False</ProtectScenarios>");
        //                    writer.WriteLine("     </WorksheetOptions>");
        //                    writer.WriteLine("     <DataValidation xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                    writer.WriteLine("      <Range>R7C3,R7C5,R7C7</Range>");
        //                    writer.WriteLine("      <Type>List</Type>");
        //                    writer.WriteLine("      <Value>N°_interno</Value>");
        //                    writer.WriteLine("     </DataValidation>");
        //                    writer.WriteLine("    </Worksheet>");
        //                }
        //                #endregion
        //                foreach (var hoja in model.hojaDetalleCamion)
        //                {
        //                    writer.WriteLine("                   ");
        //                    writer.WriteLine("<Worksheet ss:Name=\"" + hoja.patenteCamion + " Detalle" + "\">");
        //                    writer.WriteLine("   <Table ss:ExpandedColumnCount=\"10\" ss:ExpandedRowCount=\"1000\" x:FullColumns=\"1\"");
        //                    writer.WriteLine("   x:FullRows=\"1\" ss:DefaultColumnWidth=\"60\" ss:DefaultRowHeight=\"15\">");
        //                    writer.WriteLine("   <Column ss:Index=\"2\" ss:AutoFitWidth=\"0\" ss:Width=\"85.5\"/>");
        //                    writer.WriteLine("   <Column ss:AutoFitWidth=\"0\" ss:Width=\"165.75\"/>");
        //                    writer.WriteLine("   <Column ss:AutoFitWidth=\"0\" ss:Width=\"66.75\"/>");
        //                    writer.WriteLine("   <Column ss:Index=\"6\" ss:AutoFitWidth=\"0\" ss:Width=\"100.5\"/>");
        //                    writer.WriteLine("   <Column ss:AutoFitWidth=\"0\" ss:Width=\"184.5\"/>");
        //                    writer.WriteLine("   <Column ss:AutoFitWidth=\"0\" ss:Width=\"34.5\"/>");
        //                    writer.WriteLine("   <Row>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">N°</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">FECHA</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">CLIENTE</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">TON</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">TRAMO</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">TARIFA</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">CONDUCTOR</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">ESTADO</Data></Cell>");
        //                    writer.WriteLine("   </Row>");
        //                    foreach (var row in hoja.tabla)
        //                    {
        //                        writer.WriteLine("           ");
        //                        writer.WriteLine("   <Row>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.numero.ToString() + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.fecha + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.cliente + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.id.ToString() + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.trama + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"Number\">" + row.tarifa + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.conductor + "</Data></Cell>");
        //                        writer.WriteLine("    <Cell><Data ss:Type=\"String\">" + row.estado + "</Data></Cell>");
        //                        writer.WriteLine("   </Row>");
        //                    }
        //                    writer.WriteLine("       <Row>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\">TOTAL</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"Number\">" + hoja.total + "</Data></Cell>");
        //                    writer.WriteLine("    <Cell><Data ss:Type=\"String\"></Data></Cell>");
        //                    writer.WriteLine("   </Row>");
        //                    writer.WriteLine("  </Table>");
        //                    writer.WriteLine("  <WorksheetOptions xmlns=\"urn:schemas-microsoft-com:office:excel\">");
        //                    writer.WriteLine("   <PageSetup>");
        //                    writer.WriteLine("    <Header x:Margin=\"0.3\"/>");
        //                    writer.WriteLine("    <Footer x:Margin=\"0.3\"/>");
        //                    writer.WriteLine("    <PageMargins x:Bottom=\"0.75\" x:Left=\"0.7\" x:Right=\"0.7\" x:Top=\"0.75\"/>");
        //                    writer.WriteLine("   </PageSetup>");
        //                    writer.WriteLine("   <Selected/>");
        //                    writer.WriteLine("   <Panes>");
        //                    writer.WriteLine("    <Pane>");
        //                    writer.WriteLine("     <Number>3</Number>");
        //                    writer.WriteLine("     <ActiveRow>3</ActiveRow>");
        //                    writer.WriteLine("    </Pane>");
        //                    writer.WriteLine("   </Panes>");
        //                    writer.WriteLine("   <ProtectObjects>False</ProtectObjects>");
        //                    writer.WriteLine("   <ProtectScenarios>False</ProtectScenarios>");
        //                    writer.WriteLine("  </WorksheetOptions>");
        //                    writer.WriteLine(" </Worksheet>");
        //                }
        //                writer.WriteLine("       </ss:Workbook>");
        //            }
        //            #endregion
        //        }
        //    }
        //    return RedirectToAction("franquiciadosPagados", "Franquiciados");
        //}
        public ActionResult excelPagoFranquiciados(string fechaInicio, string fechaFin, int idFranquiciado = -1)
        {
            XLWorkbook workbookConsolidado = null;
            IXLWorksheet worksheetResumen = null;
            int filaResumen = 1;
            int contarResumen = 0;

            /* Antes:
            string _fechaFin = fechaFin;
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(_fechaFin).AddDays(1).AddMilliseconds(-1);
            excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();*/

            DateTime fechaDesde = Convert.ToDateTime(fechaInicio); //Esta fecha es modificada después dependiendo del último pago del franquiciado
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
            //excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();

            //idFranquiciado = -1; // ***** Cliente solicita que temporalmente este método incluya todos los franquiciados aunque se seleccione uno específico, por eso esta línea modifica la variable asignando valor -1 porque este valor considera a todos los franquiciados

            using (var db = new sgsbdEntities())
            {
                if (db.uf.Where(u => u.año == fechaHasta.Year && u.mes == fechaHasta.Month).Count() == 0)
                    return this.RedirectToAction("Procesos", "Franquiciados");
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var franquiciados = db.franquiciado.Where(w => w.prop_dispositor == false && (w.estado == "Operativo" || w.estado == "Activo" || w.estado == null) && (w.id_franquiciado == idFranquiciado || idFranquiciado == -1) && w.id_unidad == unidad).ToList();

                if (franquiciados != null && idFranquiciado == -1)
                {
                    workbookConsolidado = new XLWorkbook();

                    worksheetResumen = workbookConsolidado.Worksheets.Add("Resumen");

                    worksheetResumen.ShowGridLines = false;

                    worksheetResumen.Style.Font.SetFontName("Calibri");
                    worksheetResumen.Style.Font.SetFontSize(11);

                    //worksheetResumen.Columns().Width = 10.71;
                    //worksheetResumen.Column(1 + colMargen).Width = 28;

                    worksheetResumen.Range("A1:K1")
                        .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Font.SetBold(true);

                    worksheetResumen.Cell("A" + filaResumen).SetValue("N°");
                    worksheetResumen.Cell("B" + filaResumen).SetValue("MICRO EMPRESARIO");
                    worksheetResumen.Cell("C" + filaResumen).SetValue("INGRESOS");
                    worksheetResumen.Cell("D" + filaResumen).SetValue("LEASING");
                    worksheetResumen.Cell("E" + filaResumen).SetValue("DESCUENTOS");
                    worksheetResumen.Cell("F" + filaResumen).SetValue("TOTAL DESCUENTOS");
                    worksheetResumen.Cell("G" + filaResumen).SetValue("SUBTOTAL");
                    worksheetResumen.Cell("H" + filaResumen).SetValue("RETENCIÓN 5%");
                    worksheetResumen.Cell("I" + filaResumen).SetValue("TOTAL LÍQUIDO");
                    worksheetResumen.Cell("J" + filaResumen).SetValue("ANTICIPO");
                    worksheetResumen.Cell("K" + filaResumen).SetValue("TOTAL A CANCELAR");
                }

                foreach (var franquiciado in franquiciados)
                {
                    var ultimaFechaPago = db.pago_franquiciados.Where(u => u.id_franquiciado == franquiciado.id_franquiciado && u.id_unidad == unidad).OrderByDescending(u => u.fechaFin);

                    if (ultimaFechaPago.Count() > 0)
                    {
                        fechaDesde = Convert.ToDateTime(ultimaFechaPago.FirstOrDefault().fechaFin).AddDays(1); //Se establece fecha inicial 1 día después del último día pagado
                    }
                    else //En caso de no haber una fecha de pago anterior entonces se cosidera el día 24 anterior a la fecha final
                    {
                        if (fechaHasta.Day <= 24)
                        {
                            fechaDesde = Convert.ToDateTime(fechaInicio).AddMonths(-1);
                            fechaDesde = Convert.ToDateTime("25/" + fechaDesde.Month.ToString() + "/" + fechaDesde.Year.ToString());
                        }
                        else
                        {
                            fechaDesde = Convert.ToDateTime(fechaInicio);
                            fechaDesde = Convert.ToDateTime("25/" + fechaDesde.Month.ToString() + "/" + fechaDesde.Year.ToString());
                        }
                    }

                    excelFranquiciadoPrePago model = new excelFranquiciadoPrePago(); //Agregada 15-11-2017 para limpiar

                    model.fechaFin = fechaHasta.ToShortDateString();    // fechaFin;
                    model.fechaInicio = fechaDesde.ToShortDateString(); //fechaInicio;
                    model.franquiciado = franquiciado.razon_social;
                    model.rutFranquiciado = franquiciado.rut_franquiciado;
                    var camionesFranquiciado = db.camiones.Where(u => u.id_franquiciado == franquiciado.id_franquiciado && u.estado == "Activo").ToList();
                    if (camionesFranquiciado != null)
                    {
                        model.hojaCamion = new List<camionExcelFranquiciadoPago>();
                        model.hojaDetalleCamion = new List<hojaDetalleCamionExcelPrePago>();
                        //int montoNeto = 0;  No se usaba?
                        //int montoBruto = 0; No se usaba?

                        #region Recorrer Camiones del franquiciado
                        foreach (camiones camion in camionesFranquiciado)
                        {
                            var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value <= fechaHasta && u.id_camion.Value == camion.id_camion && u.estado == 3).ToList();
                            //Antes se consideraba el pago con fecha inicial y fecha final, para pagar HDR rezagadas el 13/06/2018 se quitó condición de fecha inicial:     && u.fecha.Value >= fechaDesde

                            if (hojasDeRutaEncabezado != null)
                            {
                                camionExcelFranquiciadoPago hoja = new camionExcelFranquiciadoPago();
                                hojaDetalleCamionExcelPrePago hojaDetalle = new hojaDetalleCamionExcelPrePago();
                                hoja.patenteCamion = camion.patente;
                                hoja.HRcantidadTotal = 0;
                                hoja.HRNetoTotal = 0;
                                hoja.leasingIva = 0;
                                hoja.leasingNeto = 0;
                                int año;
                                int mes;
                                if (fechaDesde.Date >= fechaHasta.Date) //Siguiente if tiene las mismas instrucciones al cumplir condición y al no cumplir ¿?
                                {
                                    año = fechaHasta.Year;
                                    mes = fechaHasta.Month;
                                }
                                else
                                {
                                    año = fechaHasta.Year;
                                    mes = fechaHasta.Month;
                                }
                                var uf = db.uf.Where(u => u.año == año && u.mes == mes).FirstOrDefault();

                                if (uf != null) //Agregado 20/11/2017 para evitar error en casos en que se seleccionen periodos largos y alguno de los meses no tenga UF (definir con cliente qué sucede con los meses que no dispongan de UF o bien seleccionar solo 1 mes) Aplica en pago y en prepago
                                    hoja.valorUF = uf.valor_uf;

                                hoja.leasingTotal = 0;
                                hoja.periodo = calculoPeriodo(fechaDesde, fechaHasta);
                                hoja.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
                                hojaDetalle.tabla = new List<tablaDetalleCamionExcelPrePago>();
                                hojaDetalle.patenteCamion = camion.patente;

                                // ***** Revisar por qué PrePago no tiene este bloque *****
                                /*var tipos = db.tipo_servicio.ToList();
                                foreach (tipo_servicio tipo in tipos)
                                {
                                    tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
                                    nuevaTipo.total = 0;
                                    nuevaTipo.tipo = tipo.tipo_servicio1;
                                    nuevaTipo.neto = 0;
                                    nuevaTipo.iva = 0;
                                    nuevaTipo.cantidad = 0;
                                    hoja.tablaHR.Add(nuevaTipo);
                                }*/
                                // ***** Revisar por qué PrePago no tiene este bloque *****

                                var clientesEspeciales = db.clientes.Where(u => u.cobro_especial == true).ToList();
                                foreach (clientes cliente in clientesEspeciales)
                                {
                                    tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
                                    nuevaTipo.total = 0;
                                    nuevaTipo.tipo = cliente.razon_social;
                                    nuevaTipo.neto = 0;
                                    nuevaTipo.iva = 0;
                                    nuevaTipo.cantidad = 0;
                                    hoja.tablaHR.Add(nuevaTipo);
                                }
                                int numero = 1;

                                #region Recorrer Hojas de Ruta

                                foreach (hoja_ruta hrEncabezado in hojasDeRutaEncabezado.OrderBy(u => u.fecha))
                                {
                                    conductores conductor = db.conductores.Where(u => u.id_conductor == hrEncabezado.id_conductor).FirstOrDefault();
                                    var hojasDeRutaDetalle = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == hrEncabezado.id_hoja_ruta).ToList();
                                    IEnumerable<ruta_punto> listaPuntosModelo = new List<ruta_punto>();
                                    if (hrEncabezado.ruta_modelo != null)
                                    {
                                        var rutaModelo = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault();
                                        listaPuntosModelo = rutaModelo.ruta_punto;
                                    }
                                    bool comboUltimoDetalle = false;
                                    bool comboUltimoTarifa = false;
                                    if (hrEncabezado.id_hoja_ruta == 749)
                                    {
                                        comboUltimoTarifa = false;
                                    }
                                    foreach (hoja_ruta_detalle hrDetalle in hojasDeRutaDetalle)
                                    {
                                        bool tarifaEspecial = false;
                                        int valorTarifaEspecial = 0;
                                        int cantidad = 1;
                                        int idPuntoServicio = hrDetalle.id_punto_servicio;

                                        if (db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() != null)
                                        {
                                            var punto = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault();
                                            int idContrato = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault().id_contrato;
                                            int idCliente = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault().id_cliente;
                                            var cliente = db.clientes.Where(u => u.id_cliente == idCliente).FirstOrDefault();
                                            var contrato = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault(); //Agregado 12/12/2017 para corregir desglose pagos
                                            string nombreCRE = (contrato.id_centro_responsabilidad.HasValue) ? db.centro_responsabilidad.Where(u => u.id_centro == contrato.id_centro_responsabilidad).FirstOrDefault().centro_responsabilidad1 : "Sin CRE"; //Agregado 12/12/2017 para corregir desglose pagos
                                            var tipoServicio = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault();
                                            if ((hrEncabezado.id_tipo_servicio == 2) || (hrEncabezado.id_tipo_servicio == 3)) cantidad = 1;
                                            else
                                            {
                                                if (hrDetalle.cantidad != null) cantidad = hrDetalle.cantidad.Value;
                                                else cantidad = 1;
                                            }
                                            tipo_tarifa tarifa = new tipo_tarifa();
                                            if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                            {
                                                if (hrDetalle.estado == 6 || hrDetalle.estado == 5 || hrDetalle.estado == 7)
                                                {
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 3).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    if (hrDetalle.estado == 3 || hrDetalle.estado == 4)
                                                    {
                                                        tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                                    }
                                                    else
                                                    {
                                                        tarifa = db.puntos_servicio.Where(l => l.id_punto_servicio == idPuntoServicio).FirstOrDefault().tipo_tarifa;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (comboUltimoTarifa == false && hrDetalle.estado == 2)
                                                {
                                                    comboUltimoTarifa = true;
                                                    int idTipoTarifa = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                                }
                                            }
                                            DateTime fechaHoy = DateTime.Now.Date;
                                            var X = tarifa.tarifa_escalable;
                                            if (tarifa.tarifa_escalable == true)
                                            {
                                                if (db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && fechaHoy == u.fechaEjecucion).FirstOrDefault() != null)
                                                {
                                                    var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && fechaHoy == u.fechaEjecucion).FirstOrDefault();
                                                    var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                    if (tarifaFinal != null)
                                                    {
                                                        tarifaEspecial = true;
                                                        valorTarifaEspecial = tarifaFinal.valor;
                                                    }
                                                    else
                                                    {
                                                        valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                    }
                                                }
                                                else
                                                {
                                                    FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
                                                    rs.calculoCantidadPuntos(fechaDesde, fechaHasta, tarifa);
                                                    var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && fechaHoy == u.fechaEjecucion).FirstOrDefault();
                                                    var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                    if (tarifaFinal != null)
                                                    {
                                                        tarifaEspecial = true;
                                                        valorTarifaEspecial = tarifaFinal.valor;
                                                    }
                                                    else
                                                    {
                                                        valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                    }
                                                }
                                            }
                                            int valorTarifa = 0;
                                            if (tarifaEspecial == false)
                                                valorTarifa = Convert.ToInt32(Math.Round(tarifa.valor_tarifa, 0, MidpointRounding.AwayFromZero));
                                            else valorTarifa = valorTarifaEspecial;
                                            bool porPeso;
                                            if (punto.pago_por_peso == true) porPeso = true;
                                            else porPeso = false;
                                            string estados = "2";
                                            if (hrDetalle.estado == 1) estados = "Asignado";
                                            if (hrDetalle.estado == 2) estados = "Realizado";
                                            if (hrDetalle.estado == 3) estados = "Suspendido";//sin
                                            if (hrDetalle.estado == 4) estados = "No realizado";//sus
                                            if (hrDetalle.estado == 5) estados = "Bloqueado";
                                            if (hrDetalle.estado == 6) estados = "No atendido";
                                            if (hrDetalle.estado == 7) estados = "Mal estibado";
                                            if (hrDetalle.estado == 8) estados = "Sin pesaje";
                                            if (hrDetalle.estado == 13) estados = "Anulado";
                                            if (porPeso == false)
                                            {
                                                if (hrDetalle.cantidad == null) cantidad = 1;
                                                if (tipoServicio.id_tipo_servicio == 2)
                                                {
                                                    for (int i = 1; i <= cantidad; i++)
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = valorTarifa * cantidad;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                        try
                                                        {
                                                            detalleExcel.conductor = conductor.nombre_conductor;
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            detalleExcel.conductor = "";
                                                        }

                                                        detalleExcel.estado = estados;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                    }
                                                }
                                                else
                                                {
                                                    if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = valorTarifa;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                        if (conductor != null)
                                                        {
                                                            detalleExcel.conductor = conductor.nombre_conductor;
                                                        }
                                                        else
                                                        {
                                                            detalleExcel.conductor = "";
                                                        }

                                                        detalleExcel.estado = estados;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                    }
                                                    else
                                                    {
                                                        if (comboUltimoDetalle == false && (hrDetalle.estado == 2))
                                                        {
                                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                            detalleExcel.tarifa = valorTarifa;
                                                            detalleExcel.numero = numero++;
                                                            detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                            if (conductor != null)
                                                            {
                                                                detalleExcel.conductor = conductor.nombre_conductor;
                                                            }
                                                            else
                                                            {
                                                                detalleExcel.conductor = "";
                                                            }

                                                            detalleExcel.estado = estados;
                                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                            hojaDetalle.tabla.Add(detalleExcel);
                                                            comboUltimoDetalle = true;
                                                        }
                                                        else
                                                        {
                                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                            detalleExcel.tarifa = 0;
                                                            detalleExcel.numero = numero++;
                                                            detalleExcel.id = hrDetalle.id_hoja_ruta;

                                                            if (conductor != null)
                                                            {
                                                                detalleExcel.conductor = conductor.nombre_conductor;
                                                            }
                                                            else
                                                            {
                                                                detalleExcel.conductor = "";
                                                            }

                                                            detalleExcel.estado = estados;
                                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                            hojaDetalle.tabla.Add(detalleExcel);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (hrDetalle.peso == null) hrDetalle.peso = 1;
                                                tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                double h = hrDetalle.peso.Value;
                                                double b = h / 1000;
                                                double c = valorTarifa * b;
                                                detalleExcel.tarifa = Convert.ToInt32(Math.Round(c, 0, MidpointRounding.AwayFromZero));
                                                detalleExcel.numero = numero++;
                                                detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();

                                                if (conductor != null)
                                                {
                                                    detalleExcel.conductor = conductor.nombre_conductor;
                                                }
                                                else
                                                {
                                                    detalleExcel.conductor = "";
                                                }

                                                detalleExcel.estado = estados;
                                                detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                hojaDetalle.tabla.Add(detalleExcel);
                                            }
                                            if (cliente.cobro_especial == true)
                                            {
                                                int valorReal = 0;
                                                if (porPeso == false)
                                                {
                                                    if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) cantidad = 1;
                                                    if (cantidad != 0)
                                                    {
                                                        valorReal = valorTarifa * cantidad;
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * cantidad);
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad + cantidad;
                                                    }
                                                }
                                                else
                                                {
                                                    if (hrDetalle.peso != null)
                                                    {
                                                        double h = hrDetalle.peso.Value;
                                                        double b = h / 1000;
                                                        double c = valorTarifa * b;
                                                        valorReal = Convert.ToInt32(c);
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto +(valorTarifa * (hrDetalle.peso.Value/1000));
                                                    }
                                                }
                                                var detallePago = db.detalle_pago_franquiciados.Create();
                                                detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                detallePago.id_hdrDetalle = hrDetalle.Orden;
                                                detallePago.id_hdrHeader = hrEncabezado.id_hoja_ruta;
                                                detallePago.montoNeto = valorReal;
                                                detallePago.montoIva = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 0.19, 0));
                                                detallePago.montoBruto = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 1.19, 0));
                                                db.detalle_pago_franquiciados.Add(detallePago);
                                                //db.SaveChanges();
                                                hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                                if (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) hoja.HRcantidadTotal = hoja.HRcantidadTotal + 1;
                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                            }
                                            else
                                            {
                                                if (hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault() == null) crearTipo(hoja.tablaHR, nombreCRE);
                                                var temporal = hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault();
                                                //var temporal = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault(); Antes
                                                int valorReal = 0;
                                                if (porPeso == false)
                                                {
                                                    if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) cantidad = 1;
                                                    if (cantidad != 0)
                                                    {
                                                        valorReal = valorTarifa * cantidad;
                                                        hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad; //Agregado 12/12/2017 para corregir archivo pago
                                                    }
                                                }
                                                else
                                                {
                                                    if (hrDetalle.peso != null)
                                                    {
                                                        double h = hrDetalle.peso.Value;
                                                        double b = h / 1000;
                                                        double c = valorTarifa * b;
                                                        valorReal = Convert.ToInt32(c);
                                                        hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad; //Agregado 12/12/2017 para corregir archivo pago
                                                    }
                                                }
                                                var detallePago = db.detalle_pago_franquiciados.Create();
                                                detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                detallePago.id_hdrDetalle = hrDetalle.Orden;
                                                detallePago.id_hdrHeader = hrEncabezado.id_hoja_ruta;
                                                detallePago.montoNeto = valorReal;
                                                detallePago.montoIva = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 0.19, 0));
                                                detallePago.montoBruto = Convert.ToInt32(Math.Round(Convert.ToDouble(valorReal) * 1.19, 0));
                                                db.detalle_pago_franquiciados.Add(detallePago);
                                                //db.SaveChanges();
                                                var servicioExcel = hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault(); //Agregado 12/12/2017 para corregir archivo pago
                                                servicioExcel.neto = temporal.neto + valorReal;
                                                hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                                int iva = int.Parse(Math.Round(valorReal * 0.19, 0, MidpointRounding.AwayFromZero).ToString());
                                                servicioExcel.iva = servicioExcel.iva + iva;
                                                servicioExcel.total = servicioExcel.total + Convert.ToInt32(Math.Round(valorReal * 1.19, 0, MidpointRounding.AwayFromZero));
                                            }

                                        }

                                    }
                                    hrEncabezado.fecha_pagado = DateTime.Now;
                                    //db.SaveChanges();
                                }

                                #endregion

                                if (hoja.HRNetoTotal != 0)
                                {
                                    hoja.HRcantidadTotal = 0;
                                    foreach (var e in hoja.tablaHR.ToList())
                                    {
                                        hoja.HRcantidadTotal = hoja.HRcantidadTotal + e.cantidad;
                                        e.iva = Convert.ToInt32(Math.Round(e.neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                        e.total = Convert.ToInt32(Math.Round(e.neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                    }
                                    hoja.HRIvaTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 0.19, 0, MidpointRounding.AwayFromZero));
                                    hoja.HRTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 1.19, 0, MidpointRounding.AwayFromZero));
                                }
                                else
                                {
                                    hoja.HRTotal = 0;
                                    hoja.HRIvaTotal = 0;
                                }

                                hojaDetalle.total = hoja.HRNetoTotal;
                                hoja.subTotalNeto = hoja.HRNetoTotal;

                                if (model.franquiciado.ToString().Contains("Sera"))
                                {
                                    var fr_nom = model.franquiciado;
                                }

                                #region Recorrer Leasings

                                var leasings = db.leasing.Where(u => u.id_camion.Value == camion.id_camion && u.fechaFin == null).ToList();
                                if (leasings.Count() != 0)
                                {
                                    hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                                    foreach (leasing leasing in leasings)
                                    {
                                        DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                        var detallesLeasings = db.detalle_leasing.Where(u => u.id_leasing.Value == leasing.id_leasing && u.fecha_pagado == null && u.fecha_pago < fechaFinPeriodo).ToList();
                                        if (hoja.subTotalNeto != 0)
                                        {
                                            foreach (detalle_leasing detalleLeasing in detallesLeasings)
                                            {
                                                tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();
                                                int valorPagar = Convert.ToInt32(Math.Round(detalleLeasing.valor_pagar.Value * hoja.valorUF, 0, MidpointRounding.AwayFromZero));
                                                if (detalleLeasing.fecha_pago.Value.Year < DateTime.Now.Year)
                                                {
                                                    descLeasing.concepto = "LEASING " + camion.patente;
                                                    descLeasing.cuotas = leasing.cuotas;
                                                    descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    descLeasing.cuota = detalleLeasing.cuota.Value;
                                                    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.neto = valorPagar.ToString("N0");
                                                    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                    descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                    descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                    var detallePago = db.detalle_pago_franquiciados.Create();
                                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                    detallePago.id_leasingDetalle = detalleLeasing.id_detalle_leasing;
                                                    detallePago.id_leasingHeader = leasing.id_leasing;
                                                    detallePago.montoNeto = -Convert.ToInt32(descLeasing.neto.Replace(".", ""));
                                                    detallePago.montoIva = -Convert.ToInt32(descLeasing.iva.Replace(".", ""));
                                                    detallePago.montoBruto = -Convert.ToInt32(descLeasing.total.Replace(".", ""));
                                                    db.detalle_pago_franquiciados.Add(detallePago);
                                                    db.SaveChanges();
                                                    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                    detalleLeasing.valor_pagado = detalleLeasing.valor_pagar;
                                                    detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagar;
                                                    detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
                                                    if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
                                                    {
                                                        detalleLeasing.fecha_pagado = DateTime.Now;
                                                    }

                                                    hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                    hoja.tablaLeasing.Add(descLeasing);
                                                }
                                                if (detalleLeasing.fecha_pago.Value.Year == DateTime.Now.Year && detalleLeasing.fecha_pago.Value.Month <= DateTime.Now.Month)
                                                {
                                                    descLeasing.concepto = "LEASING " + camion.patente;
                                                    descLeasing.cuotas = leasing.cuotas;
                                                    descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    descLeasing.cuota = detalleLeasing.cuota.Value;
                                                    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.neto = valorPagar.ToString("N0");
                                                    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                    descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                    descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                    detalleLeasing.valor_pagado = detalleLeasing.valor_pagar;
                                                    detalleLeasing.saldoCuota = detalleLeasing.cuota_neta - detalleLeasing.valor_pagar;
                                                    detalleLeasing.valor_pagar = detalleLeasing.saldoCuota;
                                                    if (Math.Round(detalleLeasing.saldoCuota.Value, 0, MidpointRounding.AwayFromZero) == 0)
                                                    {
                                                        detalleLeasing.fecha_pagado = DateTime.Now;
                                                    }
                                                    db.SaveChanges();
                                                    var detallePago = db.detalle_pago_franquiciados.Create();
                                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                    detallePago.id_leasingDetalle = detalleLeasing.id_detalle_leasing;
                                                    detallePago.id_leasingHeader = leasing.id_leasing;
                                                    detallePago.montoNeto = -Convert.ToInt32(descLeasing.neto.Replace(".", ""));
                                                    detallePago.montoIva = -Convert.ToInt32(descLeasing.iva.Replace(".", ""));
                                                    detallePago.montoBruto = -Convert.ToInt32(descLeasing.total.Replace(".", ""));
                                                    db.detalle_pago_franquiciados.Add(detallePago);
                                                    db.SaveChanges();

                                                    hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                    double temporalLeasingNeto = hoja.leasingNeto * 0.19;
                                                    double temporalLeasingTotal = hoja.leasingNeto * 1.19;
                                                    hoja.leasingIva = Convert.ToInt32(Math.Round(temporalLeasingNeto, 0, MidpointRounding.AwayFromZero));
                                                    hoja.leasingTotal = Convert.ToInt32(Math.Round(temporalLeasingTotal, 0, MidpointRounding.AwayFromZero));
                                                    hoja.tablaLeasing.Add(descLeasing);
                                                }
                                            }
                                            bool entroVerificacionLeasing = false;
                                            var verificacionLeasing = db.detalle_leasing.Where(u => u.id_leasing == leasing.id_leasing).ToList();
                                            foreach (var verificacionDetalle in verificacionLeasing)
                                            {
                                                if (verificacionDetalle.fecha_pagado == null)
                                                {
                                                    entroVerificacionLeasing = true;
                                                }
                                            }
                                            if (entroVerificacionLeasing == false)
                                            {
                                                leasing.fechaFin = DateTime.Now;
                                                db.SaveChanges();
                                            }
                                        }
                                    }
                                }

                                #endregion

                                #region Recorrer Descuentos

                                var descuentos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento != 16).ToList();  // 13-11-2017 Agregado para compatibilizar con prepago:   && u.id_tipo_descuento != 16
                                if (descuentos.Count() > 0)
                                {
                                    hoja.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
                                    foreach (descuento descuentoEncabezado in descuentos)
                                    {
                                        DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                        var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                                        foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                                        {
                                            bool agregarDescuento = false;
                                            int valorPagar = 0;
                                            int valorIva = 0;
                                            tablaDescuentoPrePagoExcel rowDescuento = new tablaDescuentoPrePagoExcel();
                                            if (descuentoDetalle.estado == 0)
                                            {
                                                int valorDesc = (descuentoEncabezado.en_uf == true) ? Convert.ToInt32(Math.Round(descuentoDetalle.valor_pagar.Value * uf.valor_uf, 0, MidpointRounding.AwayFromZero)) : Convert.ToInt32(descuentoDetalle.valor_pagar.Value);
                                                if (descuentoDetalle.fecha.Value.Year < DateTime.Now.Year)
                                                {
                                                    var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                                    rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                                    rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                                    rowDescuento.oc = descuentoEncabezado.oc;
                                                    //Agregada 13-11-2017 *revisar* rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                                    rowDescuento.factura = descuentoEncabezado.numero_factura;
                                                    rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                                    if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                                    else rowDescuento.llevaIVA = "NO";

                                                    rowDescuento.neto = valorDesc.ToString("N0");
                                                    rowDescuento.valorCuota = (descuentoEncabezado.en_uf == false) ? descuentoDetalle.valor_cuota.Value.ToString("N0") : descuentoDetalle.valor_cuota.Value.ToString("N2"); // Modificado 13-11-2017, anterior: descuentoDetalle.valor_cuota.Value.ToString("N0");
                                                    rowDescuento.valorPagar = valorDesc.ToString("N0");
                                                    if (descuentoEncabezado.lleva_iva == true)
                                                    {
                                                        valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                        valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                        rowDescuento.total = valorPagar.ToString("N0");
                                                        rowDescuento.iva = valorIva.ToString("N0");
                                                        //hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                        //hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                                    }
                                                    else
                                                    {
                                                        rowDescuento.iva = "0";
                                                        rowDescuento.total = rowDescuento.neto;
                                                        //hoja.descuentoTotal = Convert.ToInt32(valorDesc) + hoja.descuentoTotal; //Se agrega "+ hoja.descuentoTotal" el 13-11-2107
                                                    }
                                                    //hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);
                                                    //hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
                                                    descuentoDetalle.fecha_avance = DateTime.Now;
                                                    descuentoDetalle.valor_pagado = descuentoDetalle.valor_pagar;
                                                    descuentoDetalle.saldo = descuentoDetalle.valor_cuota - descuentoDetalle.valor_pagar;
                                                    descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
                                                    var detallePago = db.detalle_pago_franquiciados.Create();
                                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                    detallePago.id_descuentoDetalle = descuentoDetalle.id_detalle_descuento;
                                                    detallePago.id_descuentoHeader = descuentoEncabezado.id_descuento;
                                                    detallePago.montoNeto = -Convert.ToInt32(rowDescuento.neto.Replace(".", ""));
                                                    detallePago.montoIva = -Convert.ToInt32(rowDescuento.iva.Replace(".", ""));
                                                    detallePago.montoBruto = -Convert.ToInt32(rowDescuento.total.Replace(".", ""));
                                                    db.detalle_pago_franquiciados.Add(detallePago);
                                                    //db.SaveChanges();
                                                    if (descuentoDetalle.saldo.Value == 0)
                                                    {
                                                        descuentoDetalle.estado = 1;
                                                    }
                                                    //db.SaveChanges();
                                                    //hoja.tablaDescuentos.Add(rowDescuento);
                                                    agregarDescuento = true;
                                                }
                                                /* Prepago no tiene las siguientes líneas */
                                                if (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= DateTime.Now.Month)
                                                {
                                                    var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                                    rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                                    rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                                    rowDescuento.oc = descuentoEncabezado.oc;
                                                    rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                                    rowDescuento.factura = descuentoEncabezado.numero_factura;
                                                    rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                                    if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                                    else rowDescuento.llevaIVA = "NO";
                                                    rowDescuento.neto = valorDesc.ToString("N0");
                                                    rowDescuento.valorCuota = descuentoDetalle.valor_cuota.Value.ToString("N0");
                                                    rowDescuento.valorPagar = valorDesc.ToString("N0");
                                                    if (descuentoEncabezado.lleva_iva == true)
                                                    {
                                                        valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                        valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                        rowDescuento.total = valorPagar.ToString("N0");
                                                        rowDescuento.iva = valorIva.ToString("N0");
                                                        //hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                        //hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                                    }
                                                    else
                                                    {
                                                        rowDescuento.iva = "0";
                                                        rowDescuento.total = rowDescuento.neto;
                                                        //hoja.descuentoTotal = valorDesc + hoja.descuentoTotal;
                                                    }
                                                    //hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
                                                    descuentoDetalle.fecha_avance = DateTime.Now;
                                                    descuentoDetalle.valor_pagado = descuentoDetalle.valor_pagar;
                                                    descuentoDetalle.saldo = descuentoDetalle.valor_cuota - descuentoDetalle.valor_pagar;
                                                    descuentoDetalle.valor_pagar = descuentoDetalle.saldo.Value;
                                                    if (descuentoDetalle.saldo.Value == 0)
                                                    {
                                                        descuentoDetalle.estado = 1;
                                                    }
                                                    //db.SaveChanges();

                                                    //hoja.descuentoNeto = valorDesc + hoja.descuentoNeto;
                                                    //hoja.tablaDescuentos.Add(rowDescuento);
                                                    agregarDescuento = true;
                                                    var detallePago = db.detalle_pago_franquiciados.Create();
                                                    detallePago.id_pagoHeader = db.pago_franquiciados.Count() + 1;
                                                    detallePago.id_descuentoDetalle = descuentoDetalle.id_detalle_descuento;
                                                    detallePago.id_descuentoHeader = descuentoEncabezado.id_descuento;
                                                    detallePago.montoNeto = -Convert.ToInt32(rowDescuento.neto.Replace(".", ""));
                                                    detallePago.montoIva = -Convert.ToInt32(rowDescuento.iva.Replace(".", ""));
                                                    detallePago.montoBruto = -Convert.ToInt32(rowDescuento.total.Replace(".", ""));
                                                    db.detalle_pago_franquiciados.Add(detallePago);
                                                    //db.SaveChanges();
                                                }/* Prepago no tiene las líneas anteriores */


                                                if (agregarDescuento)
                                                {
                                                    if (descuentoEncabezado.en_uf)
                                                    {

                                                        if (hoja.tablaLeasing == null)
                                                        {
                                                            hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                                                        }

                                                        tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();

                                                        descLeasing.concepto = rowDescuento.concepto;
                                                        descLeasing.cuotas = Convert.ToInt32(rowDescuento.cuotaTotal);
                                                        descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                        descLeasing.cuota = rowDescuento.cuotaActual;
                                                        descLeasing.iva = rowDescuento.iva;
                                                        descLeasing.neto = rowDescuento.neto;
                                                        descLeasing.periodo = rowDescuento.fecha;
                                                        descLeasing.total = rowDescuento.total;
                                                        descLeasing.valorCuota = rowDescuento.valorCuota;
                                                        descLeasing.valorPagar = rowDescuento.valorPagar;
                                                        descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                        hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
                                                        hoja.leasingNeto = valorDesc + hoja.leasingNeto;
                                                        hoja.leasingIva = valorIva + hoja.leasingIva;
                                                        hoja.leasingTotal = valorDesc + valorIva + hoja.leasingTotal;
                                                        hoja.tablaLeasing.Add(descLeasing);
                                                    }
                                                    else
                                                    {
                                                        hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);

                                                        hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
                                                        hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                        hoja.descuentoTotal = hoja.descuentoNeto + hoja.descuentoIva + hoja.descuentoTotal;

                                                        hoja.tablaDescuentos.Add(rowDescuento);
                                                    }
                                                }


                                            }
                                        }
                                        /* Prepago no tiene las siguientes líneas */
                                        bool entroVerificacionDescuento = false;
                                        var verificacionDetallesDescuentos = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento).ToList();
                                        foreach (var verificacionDetalleDescuento in verificacionDetallesDescuentos)
                                        {
                                            if (verificacionDetalleDescuento.estado == 0)
                                            {
                                                entroVerificacionDescuento = true;
                                                break;
                                            }
                                        }
                                        if (entroVerificacionDescuento == false)
                                        {
                                            descuentoEncabezado.estado_descuento = 1;
                                            //db.SaveChanges();
                                        }
                                        /* Prepago no tiene las líneas anteriores */
                                    }
                                }

                                #endregion

                                hoja.totalDescuentosNeto = hoja.descuentoNeto + hoja.leasingNeto;
                                hoja.totalDescuentosIva = hoja.descuentoIva + hoja.leasingIva;
                                hoja.totalDescuentosTotal = hoja.descuentoTotal + hoja.leasingTotal;
                                hoja.subTotalIva = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                                hoja.subTotal = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionNeto = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.05, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionIva = Convert.ToInt32(Math.Round(hoja.retencionNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionTotal = Convert.ToInt32(Math.Round(hoja.retencionNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                                hoja.total = hoja.subTotal - hoja.retencionTotal;
                                hoja.totalNeto = hoja.subTotalNeto - hoja.retencionNeto;
                                hoja.totalIva = hoja.subTotalIva - hoja.retencionIva;
                                //montoNeto = montoNeto + hoja.subTotalNeto; No se usaba?
                                //montoBruto = montoBruto + hoja.subTotal;   No se usaba?
                                model.hojaCamion.Add(hoja);
                                model.hojaDetalleCamion.Add(hojaDetalle);
                            }
                        }
                        #endregion

                        //if (model.hojaCamion.Count > 0) //Antes no estaba esta línea
                        model.hojaTotal = new camionExcelFranquiciadoPago();
                        foreach (var item in model.hojaCamion)
                        {
                            model.hojaTotal.descuentoIva = item.descuentoIva + model.hojaTotal.descuentoIva;
                            model.hojaTotal.descuentoNeto = item.descuentoNeto + model.hojaTotal.descuentoNeto;
                            model.hojaTotal.descuentoTotal = item.descuentoTotal + model.hojaTotal.descuentoTotal;
                            model.hojaTotal.HRcantidadTotal = item.HRcantidadTotal + model.hojaTotal.HRcantidadTotal;
                            model.hojaTotal.HRIvaTotal = item.HRIvaTotal + model.hojaTotal.HRIvaTotal;
                            model.hojaTotal.HRNetoTotal = item.HRNetoTotal + model.hojaTotal.HRNetoTotal;
                            model.hojaTotal.HRTotal = item.HRTotal + model.hojaTotal.HRTotal;
                            model.hojaTotal.leasingIva = item.leasingIva + model.hojaTotal.leasingIva;
                            model.hojaTotal.leasingNeto = item.leasingNeto + model.hojaTotal.leasingNeto;
                            model.hojaTotal.leasingTotal = item.leasingTotal + model.hojaTotal.leasingTotal;
                            model.hojaTotal.patenteCamion = "Totales";
                            model.hojaTotal.periodo = item.periodo;
                            model.hojaTotal.retencionIva = item.retencionIva + model.hojaTotal.retencionIva;
                            model.hojaTotal.retencionNeto = item.retencionNeto + model.hojaTotal.retencionNeto;
                            model.hojaTotal.retencionTotal = item.retencionTotal + model.hojaTotal.retencionTotal;
                            model.hojaTotal.subTotal = item.subTotal + model.hojaTotal.subTotal;
                            model.hojaTotal.subTotalIva = item.subTotalIva + model.hojaTotal.subTotalIva;
                            model.hojaTotal.subTotalNeto = item.subTotalNeto + model.hojaTotal.subTotalNeto;

                            if (model.hojaTotal.tablaDescuentos == null)
                                model.hojaTotal.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>(); // Antes:  model.hojaTotal.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();

                            if (item.tablaDescuentos != null)
                            {
                                foreach (var desc in item.tablaDescuentos)
                                {
                                    model.hojaTotal.tablaDescuentos.Add(desc);
                                }
                            }

                            if (model.hojaTotal.tablaHR == null)
                                model.hojaTotal.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>(); // Antes: model.hojaTotal.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();

                            foreach (var hr in item.tablaHR)
                            {
                                if (model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault() == null)
                                {
                                    tablaHojasDeRutaPorTipoExcel nuev = new tablaHojasDeRutaPorTipoExcel();
                                    nuev.tipo = hr.tipo;
                                    nuev.cantidad = hr.cantidad;
                                    nuev.iva = hr.iva;
                                    nuev.neto = hr.neto;
                                    nuev.total = hr.total;
                                    model.hojaTotal.tablaHR.Add(nuev);
                                }
                                else
                                {
                                    var row = model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault();
                                    int cant = hr.cantidad;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().cantidad = cant + row.cantidad;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().iva = hr.iva + row.iva;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().neto = hr.neto + row.neto;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().total = hr.total + row.total;
                                }
                            }

                            if (model.hojaTotal.tablaLeasing == null) model.hojaTotal.tablaLeasing = new List<tablaLeasingPrePagoExcel>();  // Antes: model.hojaTotal.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                            if (item.tablaLeasing != null)
                            {
                                foreach (var leasing in item.tablaLeasing)
                                {
                                    model.hojaTotal.tablaLeasing.Add(leasing);
                                }
                            }
                            model.hojaTotal.total = item.total + model.hojaTotal.total;
                            model.hojaTotal.totalDescuentosIva = item.totalDescuentosIva + model.hojaTotal.totalDescuentosIva;
                            model.hojaTotal.totalDescuentosNeto = item.totalDescuentosNeto + model.hojaTotal.totalDescuentosNeto;
                            model.hojaTotal.totalDescuentosTotal = item.totalDescuentosTotal + model.hojaTotal.totalDescuentosTotal;
                            model.hojaTotal.totalIva = item.totalIva + model.hojaTotal.totalIva;
                            model.hojaTotal.totalNeto = item.totalNeto + model.hojaTotal.totalNeto;
                            model.hojaTotal.valorUF = item.valorUF;
                        }

                        /* Antes if (franquiciado.anticipo == true && franquiciado.porcentaje_anticipo > 0)
                        {
                            string periodo = fechaHasta.AddMonths(-1).Month.ToString() + "-" + fechaHasta.AddMonths(-1).Year.ToString();
                            var historial = db.pago_franquiciados.Where(u => u.periodo.Equals(periodo)).FirstOrDefault();
                            if (historial != null)
                            {
                                double montoAnticipo = ((historial.montoNeto * 0.5) * (franquiciado.porcentaje_anticipo.Value * 0.01));
                                model.anticipo = Convert.ToInt32(Math.Round(montoAnticipo, 0, MidpointRounding.AwayFromZero));
                                model.hojaTotal.totalNeto = model.hojaTotal.totalNeto - model.anticipo;
                                model.hojaTotal.totalIva = Convert.ToInt32(Math.Round(model.hojaTotal.totalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                                model.hojaTotal.total = model.hojaTotal.totalIva + model.hojaTotal.totalNeto;
                            }
                        } */

                        /* Se agregó el 14/11/2017 para corregir lo de arriba comentado (rescatado desde prepago) */
                        foreach (var camion in camionesFranquiciado)
                        {
                            var anticipos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento == 16).ToList();
                            foreach (var anticipo in anticipos)
                            {
                                DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == anticipo.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                                foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                                {
                                    if ((descuentoDetalle.fecha.Value.Year < fechaFinPeriodo.Year) || (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= fechaFinPeriodo.Year))
                                    {
                                        if (anticipo.en_uf == false)
                                        {
                                            model.anticipo = model.anticipo + Convert.ToInt32(descuentoDetalle.valor_pagar);
                                            anticipo.estado_descuento = 1;
                                            //db.SaveChanges();
                                        }
                                    }
                                }
                            }
                        }
                        /* Se agregó el 14/11/2017 para corregir lo de arriba comentado (rescatado desde prepago) */

                        if (model.anticipo != 0)
                        {
                            model.hojaTotal.totalNeto = model.hojaTotal.totalNeto - model.anticipo;
                            model.hojaTotal.totalIva = model.hojaTotal.totalIva - Convert.ToInt32(Math.Round(model.anticipo * 0.19, 0, MidpointRounding.AwayFromZero));
                            model.hojaTotal.total = model.hojaTotal.totalIva + model.hojaTotal.totalNeto;
                        }

                        var pago = db.pago_franquiciados.Create();
                        if (db.pago_franquiciados.Count() == 0) pago.id_pago = 1;
                        else pago.id_pago = db.pago_franquiciados.Max(u => u.id_pago) + 1;
                        pago.id_franquiciado = franquiciado.id_franquiciado;
                        pago.fechaInicio = fechaDesde.Date;
                        pago.fechaFin = fechaHasta.Date;
                        pago.montoNeto = model.hojaTotal.totalNeto;
                        pago.montoBruto = model.hojaTotal.total;
                        pago.periodo = calculoPeriodo(fechaDesde, fechaHasta);
                        pago.id_unidad = unidad;
                        db.pago_franquiciados.Add(pago);
                        //db.SaveChanges();
                    }

                    //string mydocpath = HttpContext.Server.MapPath("~/HistorialPagosFranquiciados");
                    if (model.hojaTotal != null)
                    {
                        generarExcelPago(model);

                        if (idFranquiciado == -1)
                        {
                            generarHojaExcelPago(workbookConsolidado, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, model.hojaTotal, model.franquiciado.TrimEnd(), true, model.hojaCamion);
                            generarHojaExcelPagoDetalleConsolidado(workbookConsolidado, model);

                            filaResumen++;
                            contarResumen++;

                            worksheetResumen.Range("A" + filaResumen, "K" + filaResumen)
                                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                                .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                            worksheetResumen.Cell("A" + filaResumen)
                                .SetValue(contarResumen)
                                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                                .NumberFormat.SetNumberFormatId(1);

                            worksheetResumen.Range("C" + filaResumen + ":K" + filaResumen)
                                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                                .NumberFormat.SetFormat("$ #,##0");

                            worksheetResumen.Cell("A" + filaResumen).SetValue(contarResumen);
                            worksheetResumen.Cell("B" + filaResumen).SetValue(model.franquiciado.Trim());
                            worksheetResumen.Cell("C" + filaResumen).SetValue(model.hojaTotal.HRTotal);
                            worksheetResumen.Cell("D" + filaResumen).SetValue(model.hojaTotal.leasingTotal);
                            worksheetResumen.Cell("E" + filaResumen).SetValue(model.hojaTotal.descuentoTotal);
                            worksheetResumen.Cell("F" + filaResumen).SetValue(model.hojaTotal.totalDescuentosTotal);
                            worksheetResumen.Cell("G" + filaResumen).SetValue(model.hojaTotal.subTotal);
                            worksheetResumen.Cell("H" + filaResumen).SetValue(model.hojaTotal.retencionTotal);
                            worksheetResumen.Cell("I" + filaResumen).SetValue(model.hojaTotal.totalNeto + model.anticipo + model.hojaTotal.totalIva + model.anticipo * 0.19);
                            worksheetResumen.Cell("J" + filaResumen).SetValue(model.anticipo);
                            worksheetResumen.Cell("K" + filaResumen).SetValue(model.hojaTotal.total + (model.anticipo * 0.19));

                        }
                    }
                    db.SaveChanges();
                }
            }

            if (workbookConsolidado != null)
            {
                worksheetResumen.Columns().AdjustToContents();

                workbookConsolidado.SaveAs(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\CONSOLIDADO_" + fechaInicio.Replace("/", "-") + "_" + fechaFin.Replace("/", "-") + ".xlsx");
            }

            return RedirectToAction("franquiciadosPagados", "Franquiciados");
        }

        private void generarHojaExcelPrePago(XLWorkbook workbook, String franquiciado, String rutFranquiciado, String fechaInicio, String fechaFin, int anticipo, camionExcelFranquiciadoPago hoja, String nombreHoja, bool esHojaTotal = false, List<camionExcelFranquiciadoPago> camiones = null)
        {
            var nombreNuevaHoja = nombreHoja;



            var worksheet = workbook.Worksheets.Add(formatoNombreHojaExcel(workbook, nombreNuevaHoja, ""));

            worksheet.ShowGridLines = false;


            //var imagePath = HttpContext.Server.MapPath("~/images") + "/logo.jpg";

            //var image = worksheet.AddPicture(imagePath)
            //    .MoveTo(worksheet.Cell("A2").Address, 55, 2);
            //    //.Scale(0.5); // redimensionar imagen (opcional)


            int colMargen = 0;
            int fila = 2;

            worksheet.Style.Font.SetFontName("Calibri");
            worksheet.Style.Font.SetFontSize(11);

            worksheet.Columns().Width = 10.71;
            worksheet.Column(1 + colMargen).Width = 33;
            worksheet.Column(8 + colMargen).Width = 11.86;
            worksheet.Column(9 + colMargen).Width = 14;
            worksheet.Column(10 + colMargen).Width = 14;
            worksheet.Column(11 + colMargen).Width = 14;

            #region Encabezado

            worksheet.Range(fila, 1 + colMargen, fila + 2, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Cell(fila + 1, 2 + colMargen)
                .SetValue("ESTADO DE PAGO  UNIDAD CENTRO                 RESITER INDUSTRIAL S.A")
                .Style.Font.SetFontSize(18)
                .Font.SetBold(true);

            worksheet.Row(fila + 3).Height = 10;
            worksheet.Row(fila + 4).Height = 10;

            fila = fila + 5;

            worksheet.Range(fila, 1 + colMargen, fila + 1, 1 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Fill.SetBackgroundColor(XLColor.LightGray)
                .Font.SetBold(true);


            worksheet.Range(fila, 2 + colMargen, fila, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila, 1 + colMargen).SetValue("Microempresario");
            worksheet.Cell(fila, 2 + colMargen).SetValue(franquiciado);  //Franquiciado


            worksheet.Range(fila + 1, 2 + colMargen, fila + 1, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 1, 1 + colMargen).SetValue("RUT");
            worksheet.Cell(fila + 1, 2 + colMargen).SetValue(rutFranquiciado);  //RUT Franquiciado


            worksheet.Range(fila + 2, 2 + colMargen, fila + 2, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 2, 1 + colMargen)
                .SetValue("Patente")
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            int filaAdicional = 0;

            if (!esHojaTotal)
            {
                worksheet.Cell(fila + 2, 2 + colMargen)
                    .SetValue(hoja.patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            }
            else
            {
                int colPatente = 2 + colMargen;

                for (int i = 0; i <= camiones.Count - 1; i++)
                {
                    worksheet.Cell(fila + 2 + filaAdicional, colPatente)
                        .SetValue(camiones[i].patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Font.SetBold(true);

                    colPatente++;

                    if (colPatente > 7)
                    {
                        colPatente = 2 + colMargen;
                        filaAdicional++;
                    }
                }
            }


            worksheet.Range(fila + 3 + filaAdicional, 2 + colMargen, fila + 3 + filaAdicional, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 3 + filaAdicional, 1 + colMargen)
                .SetValue("N° interno")
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            if (!esHojaTotal)
            {
                worksheet.Cell(fila + 2, 2 + colMargen)
                    .SetValue(hoja.patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            }
            else
            {
                int colPatente = 2 + colMargen;

                for (int i = 0; i <= camiones.Count - 1; i++)
                {
                    worksheet.Cell(fila + 3 + filaAdicional, colPatente)
                        .SetValue("")
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Font.SetBold(true);

                    colPatente++;

                    if (colPatente > 7)
                    {
                        colPatente = 2 + colMargen;
                        filaAdicional++;
                    }
                }
            }

            worksheet.Range(fila, 10 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            worksheet.Range(fila, 10 + colMargen, fila, 11 + colMargen).Style.Fill.BackgroundColor = XLColor.LightGray;
            worksheet.Range(fila, 10 + colMargen, fila, 11 + colMargen).Style.Font.Bold = true;
            worksheet.Range(fila, 10 + colMargen, fila + 4, 10 + colMargen).Style.Font.Bold = true;

            worksheet.Cell(fila, 10 + colMargen).SetValue("Periodo");
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.periodo);

            worksheet.Cell(fila + 1, 10 + colMargen).SetValue("Desde");
            worksheet.Cell(fila + 1, 11 + colMargen).SetValue(fechaInicio);

            worksheet.Cell(fila + 2, 10 + colMargen).SetValue("Hasta");
            worksheet.Cell(fila + 2, 11 + colMargen).SetValue(fechaFin);

            worksheet.Cell(fila + 3, 10 + colMargen).SetValue("UF");
            worksheet.Cell(fila + 3, 11 + colMargen)
                .SetValue(hoja.valorUF)
                .Style.NumberFormat.SetFormat("$ #,##0.00");

            worksheet.Cell(fila + 4, 10 + colMargen).SetValue("Petróleo");
            worksheet.Cell(fila + 4, 11 + colMargen)
                .SetValue(valorActualPetroleo())
                .Style.NumberFormat.SetFormat("$ #,##0.00");

            fila = fila + filaAdicional + (filaAdicional > 0 ? -1 : 0);

            #endregion

            #region Ingresos

            fila = fila + 6;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("                INGRESOS")
                .Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetFontSize(14)
                .Font.SetBold(true);

            worksheet.Row(fila).Height = 30;

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("DETALLE POR CENTRO DE RESPONSABILIDAD")
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Range(fila, 1 + colMargen, fila, 7 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 8 + colMargen).SetValue("N°");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");

            #region Recorrer Ingresos

            if (hoja.tablaHR != null)
            {
                foreach (var detalle in hoja.tablaHR)
                {
                    fila++;

                    worksheet.Row(fila).Style.Alignment.WrapText = true;

                    worksheet.Cell(fila, 1 + colMargen)
                        .SetValue(detalle.tipo)
                        .Style.Font.SetBold(true);

                    worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                    worksheet.Range(fila, 7 + colMargen, fila, 11 + colMargen)
                        .Style.Border.SetInsideBorder(XLBorderStyleValues.Hair);

                    worksheet.Cell(fila, 8 + colMargen)
                        .SetValue(detalle.cantidad)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .NumberFormat.SetNumberFormatId(1);

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                        .NumberFormat.SetFormat("$ #,##0");

                    worksheet.Cell(fila, 9 + colMargen).SetValue(detalle.neto);
                    worksheet.Cell(fila, 10 + colMargen).SetValue(detalle.iva);
                    worksheet.Cell(fila, 11 + colMargen).SetValue(detalle.total);
                }
            }

            #endregion

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL INGRESOS")
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Range(fila, 1 + colMargen, fila, 7 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue(hoja.HRcantidadTotal)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .NumberFormat.SetNumberFormatId(1);

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.HRNetoTotal);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.HRIvaTotal);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.HRTotal);

            #endregion

            #region Descuentos Leasing

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("           DESCUENTOS")
                .Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetFontSize(14)
                .Font.SetBold(true);

            worksheet.Row(fila).Height = 30;

            fila++;

            worksheet.Row(fila).Height = 28;
            worksheet.Row(fila).Style.Alignment.WrapText = true;

            worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 1 + colMargen).SetValue("CONCEPTO");

            worksheet.Cell(fila, 3 + colMargen).SetValue("Factura");
            worksheet.Cell(fila, 4 + colMargen).SetValue("Pactadas");
            worksheet.Cell(fila, 5 + colMargen).SetValue("Pendientes");
            worksheet.Cell(fila, 6 + colMargen).SetValue("Cuota a Pago");
            worksheet.Cell(fila, 7 + colMargen).SetValue("Fecha");
            worksheet.Cell(fila, 8 + colMargen).SetValue("Cuota UF");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");


            #region Recorrer leasing

            if (hoja.tablaLeasing != null)
            {

                foreach (var detalleLeasing in hoja.tablaLeasing)
                {
                    fila++;

                    worksheet.Row(fila).Style.Alignment.WrapText = true;

                    worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    worksheet.Range(fila, 2 + colMargen, fila, 8 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                    worksheet.Range(fila, 3 + colMargen, fila, 6 + colMargen)
                        .Style.NumberFormat.SetNumberFormatId(1);

                    worksheet.Cell(fila, 8 + colMargen)
                        .Style.NumberFormat.SetFormat("#,##0.00");

                    //worksheet.Cell(fila, 6 + colMargen)
                    //    .Style.NumberFormat.SetFormat("$ #,##0.00");

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.NumberFormat.SetFormat("$ #,##0")
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);


                    worksheet.Cell(fila, 1 + colMargen).SetValue(detalleLeasing.concepto);
                    worksheet.Cell(fila, 1 + colMargen).Style.Font.Bold = true;

                    var cuotasPendientes = Convert.ToInt64(detalleLeasing.cuotas) - Convert.ToInt64(detalleLeasing.cuota);
                    if (cuotasPendientes < 0) cuotasPendientes = 0;

                    //worksheet.Cell(fila, 2 + colMargen).SetValue(  ); //Cuotas pactadas
                    worksheet.Cell(fila, 4 + colMargen).SetValue(Convert.ToInt64(detalleLeasing.cuotas));
                    worksheet.Cell(fila, 5 + colMargen).SetValue(cuotasPendientes);
                    worksheet.Cell(fila, 6 + colMargen).SetValue(convertirEnNumero(detalleLeasing.cuota, 2));
                    worksheet.Cell(fila, 7 + colMargen).SetValue(detalleLeasing.periodo);
                    worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorCuota, 3));
                    //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorUF, 3));
                    //worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorPagar, 3));
                    worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(detalleLeasing.neto, 2));
                    worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(detalleLeasing.iva, 2));
                    worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(detalleLeasing.total, 2));
                }
            }
            else
            {
                //En caso de no haber descuentos leasing
            }

            #endregion


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUB-TOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.leasingNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.leasingIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.leasingTotal);

            #endregion

            #region Otros Descuentos

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 2 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            worksheet.Cell(fila, 1 + colMargen).SetValue("CONCEPTO");
            worksheet.Cell(fila, 3 + colMargen).SetValue("OC");
            worksheet.Cell(fila, 4 + colMargen).SetValue("Factura");
            worksheet.Cell(fila, 5 + colMargen).SetValue("Total Cuotas");
            worksheet.Cell(fila, 6 + colMargen).SetValue("Cuota Actual");
            worksheet.Cell(fila, 7 + colMargen).SetValue("Fecha");
            //worksheet.Cell(fila, 7 + colMargen).SetValue("Valor");
            worksheet.Cell(fila, 8 + colMargen).SetValue("Valor Cuota");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");

            #region Recorrer descuentos

            using (var dbd = new sgsbdEntities())
            {
                var tiposDescuentos = new TipoDescuentoRepository(dbd);
                var tipos = tiposDescuentos.GetTipoDescuento().Where(u => u.id_tipo_descuento != 16).OrderBy(u => u.nombre).ToList();

                foreach (var listaTiposDescuentos in tipos)
                {
                    List<tablaDescuentoPrePagoExcel> rowsDescuentos = null;
                    int contador = 0;

                    if (hoja.tablaDescuentos != null)
                    {
                        rowsDescuentos = hoja.tablaDescuentos.Where(u => u.idTipoDescuento == listaTiposDescuentos.id_tipo_descuento).OrderBy(u => u.concepto).ToList();
                        contador = rowsDescuentos.Count();
                    }


                    while (contador >= 0)
                    {
                        fila++;

                        tablaDescuentoPrePagoExcel rowDescuento = null;

                        if (contador > 0) rowDescuento = rowsDescuentos.ElementAt(contador - 1);

                        worksheet.Row(fila).Style.Alignment.WrapText = true;

                        worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                            .Style.Font.SetFontSize(10)
                            .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                        worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                            .Style.Font.SetFontSize(10)
                            .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                            .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                            .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                        worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                            .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        worksheet.Range(fila, 3 + colMargen, fila, 7 + colMargen)
                            .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        worksheet.Range(fila, 3 + colMargen, fila, 6 + colMargen)
                            .Style.NumberFormat.SetNumberFormatId(1);

                        //worksheet.Range(fila, 7 + colMargen, fila, 8 + colMargen)
                        //    .Style.NumberFormat.SetFormat("#,##0.00");

                        worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                            .Style.NumberFormat.SetFormat("$ #,##0")
                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        if (contador == 0)
                        {
                            if (rowsDescuentos == null)
                            {
                                worksheet.Cell(fila, 1 + colMargen).SetValue(listaTiposDescuentos.nombre);
                            }
                            else
                            {
                                if (rowsDescuentos.Count() == 0)
                                    worksheet.Cell(fila, 1 + colMargen).SetValue(listaTiposDescuentos.nombre);
                                else
                                    fila--;
                            }
                        }
                        else
                        {
                            worksheet.Cell(fila, 1 + colMargen).SetValue(rowDescuento.concepto);
                            worksheet.Cell(fila, 3 + colMargen).SetValue(convertirEnNumero(rowDescuento.oc, 2));
                            worksheet.Cell(fila, 4 + colMargen).SetValue(convertirEnNumero(rowDescuento.factura, 2));
                            worksheet.Cell(fila, 5 + colMargen).SetValue(convertirEnNumero(rowDescuento.cuotaTotal, 2));
                            worksheet.Cell(fila, 6 + colMargen).SetValue(rowDescuento.cuotaActual);
                            worksheet.Cell(fila, 7 + colMargen).SetValue(rowDescuento.fecha);
                            //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(rowDescuento.valorCuota, 2));
                            worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(rowDescuento.valorPagar, 2));
                            worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(rowDescuento.neto, 2));
                            worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(rowDescuento.iva, 2));
                            worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(rowDescuento.total, 2));
                        }

                        worksheet.Cell(fila, 1 + colMargen).Style.Font.Bold = true;

                        /*
                        worksheet.Cell(fila, 1 + colMargen).SetValue(descuento.concepto);
                        worksheet.Cell(fila, 3 + colMargen).SetValue(convertirEnNumero(descuento.oc, 2));
                        worksheet.Cell(fila, 4 + colMargen).SetValue(convertirEnNumero(descuento.factura, 2));
                        worksheet.Cell(fila, 5 + colMargen).SetValue(convertirEnNumero(descuento.cuotaTotal, 2));
                        worksheet.Cell(fila, 6 + colMargen).SetValue(descuento.cuotaActual);
                        worksheet.Cell(fila, 7 + colMargen).SetValue(descuento.fecha);
                        //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(descuento.valorCuota, 2));
                        worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(descuento.valorPagar, 2));
                        worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(descuento.neto, 2));
                        worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(descuento.iva, 2));
                        worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(descuento.total, 2));
                        */

                        contador--;
                    }

                }

                /*
                if (hoja.tablaDescuentos != null)
                {

                    foreach (var descuento in hoja.tablaDescuentos)
                    {
                    }
                }
                else
                {
                    //En caso de no haber otros descuentos
                }
                */

            }

            #endregion

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUB-TOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.descuentoNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.descuentoIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.descuentoTotal);

            #endregion

            #region Total Descuentos

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL DESCUENTOS");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.totalDescuentosNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.totalDescuentosIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.totalDescuentosTotal);

            #endregion

            #region Totales

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUBTOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.subTotalNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.subTotalIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.subTotal);


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("RETENCIÓN 5%");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.retencionNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.retencionIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.retencionTotal);


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL LÍQUIDO OPERACIÓN");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.totalNeto + anticipo);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.totalIva + anticipo * 0.19);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.totalNeto + anticipo + hoja.totalIva + anticipo * 0.19);

            #endregion

            #region Anticipo

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("ANTICIPO");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue(anticipo)
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Rendición

            fila = fila + 1;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("RENDICIÓN");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue("")
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Total a Cancelar

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL A CANCELAR");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue(hoja.total + (anticipo * 0.19))
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Firmas

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 1 + colMargen, fila + 10, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("           AUTORIZACIONES");

            fila++;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 2 + colMargen)
                .SetValue("GERENCIA ZONA");

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue("           GERENCIA RESITER INDUSTRIAL");


            worksheet.Range(fila, 1 + colMargen, fila + 4, 5 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 6 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);


            fila = fila + 5;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("                                              FIRMA RECEPCIÓN CONFORME MICROEMPRESARIO");

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue("");


            worksheet.Range(fila, 1 + colMargen, fila + 4, 5 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 6 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            #endregion

        }
        private void generarExcelPago(excelFranquiciadoPrePago model)
        {
            var workbook = new XLWorkbook();

            generarHojaExcelPago(workbook, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, model.hojaTotal, model.hojaTotal.patenteCamion, true, model.hojaCamion);

            foreach (var hoja in model.hojaCamion)
            {
                generarHojaExcelPago(workbook, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, hoja, hoja.patenteCamion);
            }

            foreach (var hoja in model.hojaDetalleCamion)
            {
                generarHojaExcelPagoDetalle(workbook, hoja);
            }

            workbook.SaveAs(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\PAGADO_" + model.rutFranquiciado + "_" + model.fechaInicio.Replace("/", "-") + "_" + model.fechaFin.Replace("/", "-") + ".xlsx");
              
        }

        private void generarHojaExcelPagoDetalle(XLWorkbook workbook, hojaDetalleCamionExcelPrePago hoja)
        {
            var worksheet = workbook.Worksheets.Add(formatoNombreHojaExcel(workbook, hoja.patenteCamion,"Detalle"));

            worksheet.ShowGridLines = false;

            int colMargen = 0;
            int fila = 1;

            worksheet.Style.Font.SetFontName("Calibri");
            worksheet.Style.Font.SetFontSize(11);

            //worksheet.Columns().Width = 10.71;
            //worksheet.Column(1 + colMargen).Width = 28;

            worksheet.Range(fila, 1 + colMargen, fila, 9 + colMargen)
                .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila, 1 + colMargen).SetValue("N°");
            worksheet.Cell(fila, 2 + colMargen).SetValue("FECHA");
            worksheet.Cell(fila, 3 + colMargen).SetValue("CLIENTE");
            worksheet.Cell(fila, 4 + colMargen).SetValue("HDR");
            worksheet.Cell(fila, 5 + colMargen).SetValue("TRAMO");
            worksheet.Cell(fila, 6 + colMargen).SetValue("TARIFA");
            worksheet.Cell(fila, 7 + colMargen).SetValue("CONDUCTOR");
            worksheet.Cell(fila, 8 + colMargen).SetValue("ESTADO");
            worksheet.Cell(fila, 9 + colMargen).SetValue("TIPO");

            foreach (var row in hoja.tabla)
            {
                fila++;

                worksheet.Range(fila, 1 + colMargen, fila, 9 + colMargen)
                    .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                worksheet.Cell(fila, 1 + colMargen)
                    .SetValue(row.numero)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetNumberFormatId(1);

                worksheet.Cell(fila, 2 + colMargen).SetValue(row.fecha);
                worksheet.Cell(fila, 3 + colMargen).SetValue(row.cliente);

                worksheet.Cell(fila, 4 + colMargen)
                    .SetValue(row.id)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetNumberFormatId(1);

                worksheet.Cell(fila, 5 + colMargen).SetValue(row.trama);

                worksheet.Cell(fila, 6 + colMargen)
                    .SetValue(row.tarifa)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetFormat("$ #,##0");

                worksheet.Cell(fila, 7 + colMargen).SetValue(row.conductor);
                worksheet.Cell(fila, 8 + colMargen).SetValue(row.estado);
                worksheet.Cell(fila, 9 + colMargen).SetValue(row.tipo);
            }

            worksheet.Columns().AdjustToContents();

        }

        private void generarHojaExcelPagoDetalleConsolidado(XLWorkbook workbook, excelFranquiciadoPrePago model)
        {
            var worksheet = workbook.Worksheets.Add(formatoNombreHojaExcel(workbook, model.franquiciado, " Det"));
            worksheet.ShowGridLines = false;

            int colMargen = 0;
            int fila = 1;

            worksheet.Style.Font.SetFontName("Calibri");
            worksheet.Style.Font.SetFontSize(11);

            //worksheet.Columns().Width = 10.71;
            //worksheet.Column(1 + colMargen).Width = 28;

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila, 1 + colMargen).SetValue("N°");
            worksheet.Cell(fila, 2 + colMargen).SetValue("FECHA");
            worksheet.Cell(fila, 3 + colMargen).SetValue("CLIENTE");
            worksheet.Cell(fila, 4 + colMargen).SetValue("HDR");
            worksheet.Cell(fila, 5 + colMargen).SetValue("TRAMO");
            worksheet.Cell(fila, 6 + colMargen).SetValue("TARIFA");
            worksheet.Cell(fila, 7 + colMargen).SetValue("CONDUCTOR");
            worksheet.Cell(fila, 8 + colMargen).SetValue("ESTADO");
            worksheet.Cell(fila, 9 + colMargen).SetValue("TIPO");
            worksheet.Cell(fila, 10 + colMargen).SetValue("PATENTE");

            int contar = 0;

            #region Recorrer detalle

            foreach (var hoja in model.hojaDetalleCamion)
            {
                foreach (var row in hoja.tabla)
                {
                    fila++;
                    contar++;

                    worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                        .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                    worksheet.Cell(fila, 1 + colMargen)
                        .SetValue(contar)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                        .NumberFormat.SetNumberFormatId(1);

                    worksheet.Cell(fila, 2 + colMargen).SetValue(row.fecha);
                    worksheet.Cell(fila, 3 + colMargen).SetValue(row.cliente);

                    worksheet.Cell(fila, 4 + colMargen)
                        .SetValue(row.id)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                        .NumberFormat.SetNumberFormatId(1);

                    worksheet.Cell(fila, 5 + colMargen).SetValue(row.trama);

                    worksheet.Cell(fila, 6 + colMargen)
                        .SetValue(row.tarifa)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                        .NumberFormat.SetFormat("$ #,##0");

                    worksheet.Cell(fila, 7 + colMargen).SetValue(row.conductor);
                    worksheet.Cell(fila, 8 + colMargen).SetValue(row.estado);
                    worksheet.Cell(fila, 9 + colMargen).SetValue(row.tipo);
                    worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.patenteCamion);
                }
            }
            #endregion

            worksheet.Columns().AdjustToContents();
        }


        private void generarHojaExcelPago(XLWorkbook workbook, String franquiciado, String rutFranquiciado, String fechaInicio, String fechaFin, int anticipo, camionExcelFranquiciadoPago hoja, String nombreHoja, bool esHojaTotal = false, List<camionExcelFranquiciadoPago> camiones = null)
        {
            var worksheet = workbook.Worksheets.Add(formatoNombreHojaExcel(workbook, nombreHoja, ""));

            worksheet.ShowGridLines = false;


            //var imagePath = HttpContext.Server.MapPath("~/images") + "/logo.jpg";

            //var image = worksheet.AddPicture(imagePath)
            //    .MoveTo(worksheet.Cell("A2").Address, 55, 2);
            // //.Scale(0.5); // redimensionar imagen (opcional)


            int colMargen = 0;
            int fila = 2;

            worksheet.Style.Font.SetFontName("Calibri");
            worksheet.Style.Font.SetFontSize(11);

            worksheet.Columns().Width = 10.71;
            worksheet.Column(1 + colMargen).Width = 33;
            worksheet.Column(8 + colMargen).Width = 11.86;
            worksheet.Column(9 + colMargen).Width = 14;
            worksheet.Column(10 + colMargen).Width = 14;
            worksheet.Column(11 + colMargen).Width = 14;

            #region Encabezado

            worksheet.Range(fila, 1 + colMargen, fila + 2, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Cell(fila + 1, 2 + colMargen)
                .SetValue("ESTADO DE PAGO  UNIDAD CENTRO                 RESITER INDUSTRIAL S.A")
                .Style.Font.SetFontSize(18)
                .Font.SetBold(true);

            worksheet.Row(fila + 3).Height = 10;
            worksheet.Row(fila + 4).Height = 10;

            fila = fila + 5;

            worksheet.Range(fila, 1 + colMargen, fila + 1, 1 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Fill.SetBackgroundColor(XLColor.LightGray)
                .Font.SetBold(true);


            worksheet.Range(fila, 2 + colMargen, fila, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila, 1 + colMargen).SetValue("Microempresario");
            worksheet.Cell(fila, 2 + colMargen).SetValue(franquiciado);  //Franquiciado


            worksheet.Range(fila + 1, 2 + colMargen, fila + 1, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 1, 1 + colMargen).SetValue("RUT");
            worksheet.Cell(fila + 1, 2 + colMargen).SetValue(rutFranquiciado);  //RUT Franquiciado


            worksheet.Range(fila + 2, 2 + colMargen, fila + 2, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 2, 1 + colMargen)
                .SetValue("Patente")
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            int filaAdicional = 0;

            if (!esHojaTotal)
            {
                worksheet.Cell(fila + 2, 2 + colMargen)
                    .SetValue(hoja.patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            }
            else
            {
                int colPatente = 2 + colMargen;

                for (int i = 0; i <= camiones.Count - 1; i++)
                {
                    worksheet.Cell(fila + 2 + filaAdicional, colPatente)
                        .SetValue(camiones[i].patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Font.SetBold(true);

                    colPatente++;

                    if (colPatente > 7)
                    {
                        colPatente = 2 + colMargen;
                        filaAdicional++;
                    }
                }
            }


            worksheet.Range(fila + 3 + filaAdicional, 2 + colMargen, fila + 3 + filaAdicional, 7 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila + 3 + filaAdicional, 1 + colMargen)
                .SetValue("N° interno")
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            if (!esHojaTotal)
            {
                worksheet.Cell(fila + 2, 2 + colMargen)
                    .SetValue(hoja.patenteCamion) //Patente (texto "Totales" para el caso de la primera hoja)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);
            }
            else
            {
                int colPatente = 2 + colMargen;

                for (int i = 0; i <= camiones.Count - 1; i++)
                {
                    worksheet.Cell(fila + 3 + filaAdicional, colPatente)
                        .SetValue("")
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Font.SetBold(true);

                    colPatente++;

                    if (colPatente > 7)
                    {
                        colPatente = 2 + colMargen;
                        filaAdicional++;
                    }
                }
            }

            worksheet.Range(fila, 10 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            worksheet.Range(fila, 10 + colMargen, fila, 11 + colMargen).Style.Fill.BackgroundColor = XLColor.LightGray;
            worksheet.Range(fila, 10 + colMargen, fila, 11 + colMargen).Style.Font.Bold = true;
            worksheet.Range(fila, 10 + colMargen, fila + 4, 10 + colMargen).Style.Font.Bold = true;

            worksheet.Cell(fila, 10 + colMargen).SetValue("Periodo");
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.periodo);

            worksheet.Cell(fila + 1, 10 + colMargen).SetValue("Desde");
            worksheet.Cell(fila + 1, 11 + colMargen).SetValue(fechaInicio);

            worksheet.Cell(fila + 2, 10 + colMargen).SetValue("Hasta");
            worksheet.Cell(fila + 2, 11 + colMargen).SetValue(fechaFin);

            worksheet.Cell(fila + 3, 10 + colMargen).SetValue("UF");
            worksheet.Cell(fila + 3, 11 + colMargen)
                .SetValue(hoja.valorUF)
                .Style.NumberFormat.SetFormat("$ #,##0.00");

            worksheet.Cell(fila + 4, 10 + colMargen).SetValue("Petróleo");
            worksheet.Cell(fila + 4, 11 + colMargen)
                .SetValue(valorActualPetroleo())
                .Style.NumberFormat.SetFormat("$ #,##0.00");

            fila = fila + filaAdicional + (filaAdicional > 0 ? -1 : 0);

            #endregion

            #region Ingresos

            fila = fila + 6;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("                INGRESOS")
                .Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetFontSize(14)
                .Font.SetBold(true);

            worksheet.Row(fila).Height = 30;

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("DETALLE POR CENTRO DE RESPONSABILIDAD")
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Range(fila, 1 + colMargen, fila, 7 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 8 + colMargen).SetValue("N°");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");

            #region Recorrer Ingresos

            if (hoja.tablaHR != null)
            {
                foreach (var detalle in hoja.tablaHR)
                {
                    fila++;

                    worksheet.Row(fila).Style.Alignment.WrapText = true;

                    worksheet.Cell(fila, 1 + colMargen)
                        .SetValue(detalle.tipo)
                        .Style.Font.SetBold(true);

                    worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                    worksheet.Range(fila, 7 + colMargen, fila, 11 + colMargen)
                        .Style.Border.SetInsideBorder(XLBorderStyleValues.Hair);

                    worksheet.Cell(fila, 8 + colMargen)
                        .SetValue(detalle.cantidad)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .NumberFormat.SetNumberFormatId(1);

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                        .NumberFormat.SetFormat("$ #,##0");

                    worksheet.Cell(fila, 9 + colMargen).SetValue(detalle.neto);
                    worksheet.Cell(fila, 10 + colMargen).SetValue(detalle.iva);
                    worksheet.Cell(fila, 11 + colMargen).SetValue(detalle.total);
                }
            }

            #endregion

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL INGRESOS")
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Range(fila, 1 + colMargen, fila, 7 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                .Style.Fill.SetBackgroundColor(XLColor.LightGray)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue(hoja.HRcantidadTotal)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .NumberFormat.SetNumberFormatId(1);

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.HRNetoTotal);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.HRIvaTotal);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.HRTotal);

            #endregion

            #region Descuentos Leasing

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("           DESCUENTOS")
                .Style
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetFontSize(14)
                .Font.SetBold(true);

            worksheet.Row(fila).Height = 30;

            fila++;

            worksheet.Row(fila).Height = 28;
            worksheet.Row(fila).Style.Alignment.WrapText = true;

            worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 1 + colMargen).SetValue("CONCEPTO");

            worksheet.Cell(fila, 3 + colMargen).SetValue("Factura");
            worksheet.Cell(fila, 4 + colMargen).SetValue("Pactadas");
            worksheet.Cell(fila, 5 + colMargen).SetValue("Pendientes");
            worksheet.Cell(fila, 6 + colMargen).SetValue("Cuota a Pago");
            worksheet.Cell(fila, 7 + colMargen).SetValue("Fecha");
            worksheet.Cell(fila, 8 + colMargen).SetValue("Cuota UF");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");

            #region Recorrer leasing

            if (hoja.tablaLeasing != null)
            {

                foreach (var detalleLeasing in hoja.tablaLeasing)
                {
                    fila++;

                    worksheet.Row(fila).Style.Alignment.WrapText = true;

                    worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                        .Style.Font.SetFontSize(10)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                    worksheet.Range(fila, 2 + colMargen, fila, 8 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                    worksheet.Range(fila, 3 + colMargen, fila, 6 + colMargen)
                        .Style.NumberFormat.SetNumberFormatId(1);

                    worksheet.Cell(fila, 8 + colMargen)
                        .Style.NumberFormat.SetFormat("#,##0.00");

                    //worksheet.Cell(fila, 6 + colMargen)
                    //    .Style.NumberFormat.SetFormat("$ #,##0.00");

                    worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                        .Style.NumberFormat.SetFormat("$ #,##0")
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);


                    worksheet.Cell(fila, 1 + colMargen).SetValue(detalleLeasing.concepto);
                    worksheet.Cell(fila, 1 + colMargen).Style.Font.Bold = true;

                    var cuotasPendientes = Convert.ToInt64(detalleLeasing.cuotas) - Convert.ToInt64(detalleLeasing.cuota);
                    if (cuotasPendientes < 0) cuotasPendientes = 0;

                    //worksheet.Cell(fila, 2 + colMargen).SetValue(  ); //Cuotas pactadas
                    worksheet.Cell(fila, 4 + colMargen).SetValue(Convert.ToInt64(detalleLeasing.cuotas));
                    worksheet.Cell(fila, 5 + colMargen).SetValue(cuotasPendientes);
                    worksheet.Cell(fila, 6 + colMargen).SetValue(convertirEnNumero(detalleLeasing.cuota, 2));
                    worksheet.Cell(fila, 7 + colMargen).SetValue(detalleLeasing.periodo);
                    worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorCuota, 3));
                    //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorUF, 3));
                    //worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(detalleLeasing.valorPagar, 3));
                    worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(detalleLeasing.neto, 2));
                    worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(detalleLeasing.iva, 2));
                    worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(detalleLeasing.total, 2));
                }
            }
            else
            {
                //En caso de no haber descuentos leasing
            }

            #endregion


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUB-TOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.leasingNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.leasingIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.leasingTotal);

            #endregion

            #region Otros Descuentos

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 2 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

            worksheet.Cell(fila, 1 + colMargen).SetValue("CONCEPTO");
            worksheet.Cell(fila, 3 + colMargen).SetValue("OC");
            worksheet.Cell(fila, 4 + colMargen).SetValue("Factura");
            worksheet.Cell(fila, 5 + colMargen).SetValue("Total Cuotas");
            worksheet.Cell(fila, 6 + colMargen).SetValue("Cuota Actual");
            worksheet.Cell(fila, 7 + colMargen).SetValue("Fecha");
            //worksheet.Cell(fila, 7 + colMargen).SetValue("Valor");
            worksheet.Cell(fila, 8 + colMargen).SetValue("Valor Cuota");
            worksheet.Cell(fila, 9 + colMargen).SetValue("Neto");
            worksheet.Cell(fila, 10 + colMargen).SetValue("IVA");
            worksheet.Cell(fila, 11 + colMargen).SetValue("Total");

            #region Recorrer descuentos

            using (var dbd = new sgsbdEntities())
            {
                var tiposDescuentos = new TipoDescuentoRepository(dbd);
                var tipos = tiposDescuentos.GetTipoDescuento().Where(u => u.id_tipo_descuento != 16).OrderBy(u => u.nombre).ToList();

                foreach (var listaTiposDescuentos in tipos)
                {
                    List<tablaDescuentoPrePagoExcel> rowsDescuentos = null;
                    int contador = 0;

                    if (hoja.tablaDescuentos != null)
                    {
                        rowsDescuentos = hoja.tablaDescuentos.Where(u => u.idTipoDescuento == listaTiposDescuentos.id_tipo_descuento).OrderBy(u => u.concepto).ToList();
                        contador = rowsDescuentos.Count();
                    }


                    while (contador >= 0)
                    {
                        fila++;

                        tablaDescuentoPrePagoExcel rowDescuento = null;

                        if (contador > 0) rowDescuento = rowsDescuentos.ElementAt(contador - 1);

                        worksheet.Row(fila).Style.Alignment.WrapText = true;

                        worksheet.Range(fila, 1 + colMargen, fila, 2 + colMargen)
                            .Style.Font.SetFontSize(10)
                            .Border.SetOutsideBorder(XLBorderStyleValues.Thin);

                        worksheet.Range(fila, 3 + colMargen, fila, 11 + colMargen)
                            .Style.Font.SetFontSize(10)
                            .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                            .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                            .Alignment.SetVertical(XLAlignmentVerticalValues.Center);

                        worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                            .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        worksheet.Range(fila, 3 + colMargen, fila, 7 + colMargen)
                            .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center);

                        worksheet.Range(fila, 3 + colMargen, fila, 6 + colMargen)
                            .Style.NumberFormat.SetNumberFormatId(1);

                        //worksheet.Range(fila, 7 + colMargen, fila, 8 + colMargen)
                        //    .Style.NumberFormat.SetFormat("#,##0.00");

                        worksheet.Range(fila, 8 + colMargen, fila, 11 + colMargen)
                            .Style.NumberFormat.SetFormat("$ #,##0")
                            .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right);

                        if (contador == 0)
                        {
                            if (rowsDescuentos == null)
                            {
                                worksheet.Cell(fila, 1 + colMargen).SetValue(listaTiposDescuentos.nombre);
                            }
                            else
                            {
                                if (rowsDescuentos.Count() == 0)
                                    worksheet.Cell(fila, 1 + colMargen).SetValue(listaTiposDescuentos.nombre);
                                else
                                    fila--;
                            }
                        }
                        else
                        {
                            worksheet.Cell(fila, 1 + colMargen).SetValue(rowDescuento.concepto);
                            worksheet.Cell(fila, 3 + colMargen).SetValue(convertirEnNumero(rowDescuento.oc, 2));
                            worksheet.Cell(fila, 4 + colMargen).SetValue(convertirEnNumero(rowDescuento.factura, 2));
                            worksheet.Cell(fila, 5 + colMargen).SetValue(convertirEnNumero(rowDescuento.cuotaTotal, 2));
                            worksheet.Cell(fila, 6 + colMargen).SetValue(rowDescuento.cuotaActual);
                            worksheet.Cell(fila, 7 + colMargen).SetValue(rowDescuento.fecha);
                            //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(rowDescuento.valorCuota, 2));
                            worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(rowDescuento.valorPagar, 2));
                            worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(rowDescuento.neto, 2));
                            worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(rowDescuento.iva, 2));
                            worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(rowDescuento.total, 2));
                        }

                        worksheet.Cell(fila, 1 + colMargen).Style.Font.Bold = true;

                        /*
                        worksheet.Cell(fila, 1 + colMargen).SetValue(descuento.concepto);
                        worksheet.Cell(fila, 3 + colMargen).SetValue(convertirEnNumero(descuento.oc, 2));
                        worksheet.Cell(fila, 4 + colMargen).SetValue(convertirEnNumero(descuento.factura, 2));
                        worksheet.Cell(fila, 5 + colMargen).SetValue(convertirEnNumero(descuento.cuotaTotal, 2));
                        worksheet.Cell(fila, 6 + colMargen).SetValue(descuento.cuotaActual);
                        worksheet.Cell(fila, 7 + colMargen).SetValue(descuento.fecha);
                        //worksheet.Cell(fila, 7 + colMargen).SetValue(convertirEnNumero(descuento.valorCuota, 2));
                        worksheet.Cell(fila, 8 + colMargen).SetValue(convertirEnNumero(descuento.valorPagar, 2));
                        worksheet.Cell(fila, 9 + colMargen).SetValue(convertirEnNumero(descuento.neto, 2));
                        worksheet.Cell(fila, 10 + colMargen).SetValue(convertirEnNumero(descuento.iva, 2));
                        worksheet.Cell(fila, 11 + colMargen).SetValue(convertirEnNumero(descuento.total, 2));
                        */

                        contador--;
                    }

                }

                /*
                if (hoja.tablaDescuentos != null)
                {

                    foreach (var descuento in hoja.tablaDescuentos)
                    {
                    }
                }
                else
                {
                    //En caso de no haber otros descuentos
                }
                */

            }

            #endregion

            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUB-TOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.descuentoNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.descuentoIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.descuentoTotal);

            #endregion

            #region Total Descuentos

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL DESCUENTOS");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.totalDescuentosNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.totalDescuentosIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.totalDescuentosTotal);

            #endregion

            #region Totales

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("SUBTOTAL");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.subTotalNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.subTotalIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.subTotal);


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("RETENCIÓN 5%");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.retencionNeto);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.retencionIva);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.retencionTotal);


            fila++;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL LÍQUIDO OPERACIÓN");

            worksheet.Range(fila, 1 + colMargen, fila, 8 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 9 + colMargen, fila, 11 + colMargen)
                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                .NumberFormat.SetFormat("$ #,##0");

            worksheet.Cell(fila, 9 + colMargen).SetValue(hoja.totalNeto + anticipo);
            worksheet.Cell(fila, 10 + colMargen).SetValue(hoja.totalIva + anticipo * 0.19);
            worksheet.Cell(fila, 11 + colMargen).SetValue(hoja.totalNeto + anticipo + hoja.totalIva + anticipo * 0.19);

            #endregion

            #region Anticipo

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("ANTICIPO");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue(anticipo)
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Rendición

            fila = fila + 1;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("RENDICIÓN");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue("")
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Total a Cancelar

            fila = fila + 2;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("TOTAL A CANCELAR");

            worksheet.Range(fila, 1 + colMargen, fila, 10 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .Style.Font.SetFontSize(12)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 11 + colMargen)
                .SetValue(hoja.total + (anticipo * 0.19))
                .Style.NumberFormat.SetFormat("$ #,##0");

            #endregion

            #region Firmas

            fila = fila + 2;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Range(fila, 1 + colMargen, fila + 10, 11 + colMargen)
                .Style.Font.SetFontSize(10)
                .Font.SetBold(true);

            worksheet.Cell(fila, 5 + colMargen)
                .SetValue("           AUTORIZACIONES");

            fila++;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 2 + colMargen)
                .SetValue("GERENCIA ZONA");

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue("           GERENCIA RESITER INDUSTRIAL");


            worksheet.Range(fila, 1 + colMargen, fila + 4, 5 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 6 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);


            fila = fila + 5;

            worksheet.Range(fila, 1 + colMargen, fila, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Fill.BackgroundColor = XLColor.LightGray;

            worksheet.Cell(fila, 1 + colMargen)
                .SetValue("                                              FIRMA RECEPCIÓN CONFORME MICROEMPRESARIO");

            worksheet.Cell(fila, 8 + colMargen)
                .SetValue("");


            worksheet.Range(fila, 1 + colMargen, fila + 4, 5 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            worksheet.Range(fila, 6 + colMargen, fila + 4, 11 + colMargen)
                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin);

            #endregion

        }


        public ActionResult excelPrePagoFranquiciados(string fechaInicio, string fechaFin, int idFranquiciado)
        {
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
            excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();
            using (var db = new sgsbdEntities())
            {
                var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == idFranquiciado).FirstOrDefault();
                model.fechaFin = fechaFin;
                model.fechaInicio = fechaInicio;
                model.franquiciado = franquiciado.razon_social;
                model.rutFranquiciado = franquiciado.rut_franquiciado;
                var camionesFranquiciado = db.camiones.Where(u => u.id_franquiciado == franquiciado.id_franquiciado && u.estado == "Activo").ToList();
                if (camionesFranquiciado != null)
                {
                    model.hojaCamion = new List<camionExcelFranquiciadoPago>();
                    model.hojaDetalleCamion = new List<hojaDetalleCamionExcelPrePago>();
                    foreach (camiones camion in camionesFranquiciado)
                    {
                        var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value >= fechaDesde && u.fecha.Value <= fechaHasta && u.id_camion.Value == camion.id_camion && u.estado == 3).ToList();
                        if (hojasDeRutaEncabezado != null)
                        {
                            camionExcelFranquiciadoPago hoja = new camionExcelFranquiciadoPago();
                            hojaDetalleCamionExcelPrePago hojaDetalle = new hojaDetalleCamionExcelPrePago();
                            hoja.patenteCamion = camion.patente;
                            hoja.HRcantidadTotal = 0;
                            hoja.HRNetoTotal = 0;
                            hoja.leasingIva = 0;
                            hoja.leasingNeto = 0;
                            int año;
                            int mes;
                            if (fechaDesde.Date >= fechaHasta.Date)
                            {
                                año = fechaHasta.Year;
                                mes = fechaHasta.Month;
                            }
                            else
                            {
                                año = fechaHasta.Year;
                                mes = fechaHasta.Month;
                            }
                            var uf = db.uf.Where(u => u.año == año && u.mes == mes).FirstOrDefault();

                            if (uf != null) //Agregado 20/11/2017 para evitar error en casos en que se seleccionen periodos largos y alguno de los meses no tenga UF (definir con cliente qué sucede con los meses que no dispongan de UF o bien seleccionar solo 1 mes) Aplica en pago y en prepago
                                hoja.valorUF = uf.valor_uf;

                            hoja.leasingTotal = 0;
                            hoja.periodo = calculoPeriodo(fechaDesde, fechaHasta);
                            hoja.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
                            hojaDetalle.tabla = new List<tablaDetalleCamionExcelPrePago>();
                            hojaDetalle.patenteCamion = camion.patente;
                            var tipos = db.tipo_servicio.ToList();
                            foreach (tipo_servicio tipo in tipos)
                            {
                                tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
                                nuevaTipo.total = 0;
                                nuevaTipo.tipo = tipo.tipo_servicio1;
                                nuevaTipo.neto = 0;
                                nuevaTipo.iva = 0;
                                nuevaTipo.cantidad = 0;
                                hoja.tablaHR.Add(nuevaTipo);
                            }
                            var clientesEspeciales = db.clientes.Where(u => u.cobro_especial == true).ToList();
                            foreach (clientes cliente in clientesEspeciales)
                            {
                                tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
                                nuevaTipo.total = 0;
                                nuevaTipo.tipo = cliente.razon_social;
                                nuevaTipo.neto = 0;
                                nuevaTipo.iva = 0;
                                nuevaTipo.cantidad = 0;
                                hoja.tablaHR.Add(nuevaTipo);
                            }
                            int numero = 1;
                            //int numeroTemp = 2; No se usaba ¿?
                            foreach (hoja_ruta hrEncabezado in hojasDeRutaEncabezado.OrderBy(u => u.fecha))
                            {

                                conductores conductor = db.conductores.Where(u => u.id_conductor == hrEncabezado.id_conductor).FirstOrDefault();
                                var hojasDeRutaDetalle = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == hrEncabezado.id_hoja_ruta).ToList();
                                IEnumerable<ruta_punto> listaPuntosModelo = new List<ruta_punto>();
                                if (hrEncabezado.ruta_modelo != null)
                                {
                                    var rutaModelo = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault();
                                    listaPuntosModelo = rutaModelo.ruta_punto;
                                }
                                bool comboUltimoDetalle = false;
                                bool comboUltimoTarifa = false;
                                if (hrEncabezado.id_hoja_ruta == 749)
                                {
                                    comboUltimoTarifa = false;
                                }
                                //bool debugTemp = false; No se usaba ¿?

                                foreach (hoja_ruta_detalle hrDetalle in hojasDeRutaDetalle)
                                {
                                    bool tarifaEspecial = false;
                                    int valorTarifaEspecial = 0;
                                    int idPuntoServicio = hrDetalle.id_punto_servicio;

                                    if (db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() != null)
                                    {
                                        var punto = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault();
                                        int idContrato = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault().id_contrato;
                                        int idCliente = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault().id_cliente;
                                        var cliente = db.clientes.Where(u => u.id_cliente == idCliente).FirstOrDefault();
                                        var tipoServicio = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault();
                                        if ((hrEncabezado.id_tipo_servicio == 2) || (hrEncabezado.id_tipo_servicio == 3)) hrDetalle.cantidad = 1;
                                        tipo_tarifa tarifa = new tipo_tarifa();
                                        if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                        {
                                            if (hrDetalle.estado == 6 || hrDetalle.estado == 5 || hrDetalle.estado == 7)
                                            {
                                                tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 3).FirstOrDefault();
                                            }
                                            else
                                            {
                                                if (hrDetalle.estado == 3 || hrDetalle.estado == 4)
                                                {
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    tarifa = db.puntos_servicio.Where(l => l.id_punto_servicio == idPuntoServicio).FirstOrDefault().tipo_tarifa;
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (comboUltimoTarifa == false && hrDetalle.estado == 2)
                                            {
                                                comboUltimoTarifa = true;
                                                int idTipoTarifa = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
                                                tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
                                            }
                                            else
                                            {
                                                tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                            }
                                        }
                                        DateTime ahora = DateTime.Now.Date;
                                        if (tarifa.tarifa_escalable == true)
                                        {
                                            if (db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault() != null)
                                            {
                                                var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault();
                                                var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                if (tarifaFinal != null)
                                                {
                                                    tarifaEspecial = true;
                                                    valorTarifaEspecial = tarifaFinal.valor;
                                                }
                                                else
                                                {
                                                    valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                }
                                            }
                                            else
                                            {
                                                FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
                                                rs.calculoCantidadPuntos(fechaDesde, fechaHasta, tarifa);
                                                var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault();
                                                var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                if (tarifaFinal != null)
                                                {
                                                    tarifaEspecial = true;
                                                    valorTarifaEspecial = tarifaFinal.valor;
                                                }
                                                else
                                                {
                                                    valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                }
                                            }
                                        }
                                        int valorTarifa = 0;
                                        if (tarifaEspecial == false)
                                            valorTarifa = Convert.ToInt32(Math.Round(tarifa.valor_tarifa, 0, MidpointRounding.AwayFromZero));
                                        else valorTarifa = valorTarifaEspecial;
                                        bool porPeso;
                                        if (punto.pago_por_peso == true) porPeso = true;
                                        else porPeso = false;
                                        string estados = "2";
                                        if (hrDetalle.estado == 1) estados = "Asignado";
                                        if (hrDetalle.estado == 2) estados = "Realizado";
                                        if (hrDetalle.estado == 3) estados = "Suspendido";//sin
                                        if (hrDetalle.estado == 4) estados = "No realizado";//sus
                                        if (hrDetalle.estado == 5) estados = "Bloqueado";
                                        if (hrDetalle.estado == 6) estados = "No atendido";
                                        if (hrDetalle.estado == 7) estados = "Mal estibado";
                                        if (hrDetalle.estado == 8) estados = "Sin pesaje";
                                        if (hrDetalle.estado == 13) estados = "Anulado";
                                        if (porPeso == false)
                                        {
                                            if (hrDetalle.cantidad == null) hrDetalle.cantidad = 1;
                                            if (tipoServicio.id_tipo_servicio == 2)
                                            {
                                                for (int i = 1; i <= hrDetalle.cantidad.Value; i++)
                                                {
                                                    tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                    detalleExcel.tarifa = valorTarifa * hrDetalle.cantidad.Value;
                                                    detalleExcel.numero = numero++;
                                                    detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                    detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                    detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                    detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                    if (conductor != null)
                                                    {
                                                        detalleExcel.conductor = conductor.nombre_conductor;
                                                    }
                                                    else
                                                    {
                                                        detalleExcel.conductor = "";
                                                    }
                                                    detalleExcel.estado = estados;
                                                    detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                    hojaDetalle.tabla.Add(detalleExcel);
                                                }
                                            }
                                            else
                                            {
                                                if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                                {
                                                    tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                    detalleExcel.tarifa = valorTarifa;
                                                    detalleExcel.numero = numero++;
                                                    detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                    detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                    detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                    detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                    detalleExcel.conductor = conductor.nombre_conductor;
                                                    detalleExcel.estado = estados;
                                                    detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                    hojaDetalle.tabla.Add(detalleExcel);
                                                }
                                                else
                                                {
                                                    if (comboUltimoDetalle == false && (hrDetalle.estado == 2))
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = valorTarifa;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                        detalleExcel.conductor = conductor.nombre_conductor;
                                                        detalleExcel.estado = estados;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                        comboUltimoDetalle = true;
                                                    }
                                                    else
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = 0;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.conductor = conductor.nombre_conductor;
                                                        detalleExcel.estado = estados;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (hrDetalle.peso == null) hrDetalle.peso = 1;
                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                            double h = hrDetalle.peso.Value;
                                            double b = h / 1000;
                                            double c = valorTarifa * b;
                                            detalleExcel.tarifa = Convert.ToInt32(Math.Round(c, 0, MidpointRounding.AwayFromZero));
                                            detalleExcel.numero = numero++;
                                            detalleExcel.id = hrDetalle.id_hoja_ruta;
                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                            detalleExcel.conductor = conductor.nombre_conductor;
                                            detalleExcel.estado = estados;
                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                            hojaDetalle.tabla.Add(detalleExcel);
                                        }
                                        if (cliente.cobro_especial == true)
                                        {
                                            int valorReal = 0;
                                            if (porPeso == false)
                                            {
                                                if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) hrDetalle.cantidad = 1;
                                                if (hrDetalle.cantidad != null)
                                                {
                                                    valorReal = valorTarifa * hrDetalle.cantidad.Value;
                                                    hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * hrDetalle.cantidad.Value);
                                                    hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad + hrDetalle.cantidad.Value;
                                                }
                                            }
                                            else
                                            {
                                                if (hrDetalle.peso != null)
                                                {
                                                    double h = hrDetalle.peso.Value;
                                                    double b = h / 1000;
                                                    double c = valorTarifa * b;
                                                    valorReal = Convert.ToInt32(c);
                                                    hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value / 1000));
                                                }
                                            }
                                            hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                            if (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) hoja.HRcantidadTotal = hoja.HRcantidadTotal + 1;
                                            hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                            hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                        }
                                        else
                                        {
                                            var temporal = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
                                            int valorReal = 0;
                                            if (porPeso == false)
                                            {
                                                if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) hrDetalle.cantidad = 1;
                                                if (hrDetalle.cantidad != null)
                                                {
                                                    valorReal = valorTarifa * hrDetalle.cantidad.Value;
                                                    hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad;
                                                }
                                            }
                                            else
                                            {
                                                if (hrDetalle.peso != null)
                                                {
                                                    double h = hrDetalle.peso.Value;
                                                    double b = h / 1000;
                                                    double c = valorTarifa * b;
                                                    valorReal = Convert.ToInt32(c);
                                                    hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault().cantidad;
                                                }
                                            }
                                            var servicioExcel = hoja.tablaHR.Where(u => u.tipo == tipoServicio.tipo_servicio1).FirstOrDefault();
                                            servicioExcel.neto = temporal.neto + valorReal;
                                            hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                            int iva = int.Parse(Math.Round(valorReal * 0.19, 0, MidpointRounding.AwayFromZero).ToString());
                                            servicioExcel.iva = servicioExcel.iva + iva;
                                            servicioExcel.total = servicioExcel.total + Convert.ToInt32(Math.Round(valorReal * 1.19, 0, MidpointRounding.AwayFromZero));
                                        }

                                    }

                                }
                            }
                            if (hoja.HRNetoTotal != 0)
                            {
                                hoja.HRcantidadTotal = 0;
                                foreach (var e in hoja.tablaHR.ToList())
                                {
                                    hoja.HRcantidadTotal = hoja.HRcantidadTotal + e.cantidad;
                                    e.iva = Convert.ToInt32(Math.Round(e.neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                    e.total = Convert.ToInt32(Math.Round(e.neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                }
                                hoja.HRIvaTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 0.19, 0, MidpointRounding.AwayFromZero));
                                hoja.HRTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 1.19, 0, MidpointRounding.AwayFromZero));
                            }
                            else
                            {
                                hoja.HRTotal = 0;
                                hoja.HRIvaTotal = 0;
                            }

                            hojaDetalle.total = hoja.HRNetoTotal;
                            hoja.subTotalNeto = hoja.HRNetoTotal;
                            var descuentos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento != 16).ToList();
                            if (descuentos.Count() > 0)
                            {
                                hoja.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
                                foreach (descuento descuentoEncabezado in descuentos)
                                {
                                    DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                    var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                                    foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                                    {
                                        tablaDescuentoPrePagoExcel rowDescuento = new tablaDescuentoPrePagoExcel();
                                        int valorDesc = (descuentoEncabezado.en_uf == true) ? Convert.ToInt32(Math.Round(descuentoDetalle.valor_pagar.Value * uf.valor_uf, 0, MidpointRounding.AwayFromZero)) : Convert.ToInt32(descuentoDetalle.valor_pagar.Value);
                                        if (descuentoDetalle.fecha.Value.Year < DateTime.Now.Year)
                                        {
                                            var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                            rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                            rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                            rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                            rowDescuento.oc = descuentoEncabezado.oc;
                                            rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                            rowDescuento.factura = descuentoEncabezado.numero_factura;
                                            rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                            if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                            else rowDescuento.llevaIVA = "NO";

                                            rowDescuento.neto = valorDesc.ToString("N0");
                                            rowDescuento.valorCuota = (descuentoEncabezado.en_uf == false) ? descuentoDetalle.valor_cuota.Value.ToString("N0") : descuentoDetalle.valor_cuota.Value.ToString("N2");
                                            rowDescuento.valorPagar = valorDesc.ToString("N0");
                                            if (descuentoEncabezado.lleva_iva == true)
                                            {
                                                int valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                int valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                rowDescuento.total = valorPagar.ToString("N0");
                                                rowDescuento.iva = valorIva.ToString("N0");
                                                hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                            }
                                            else
                                            {
                                                rowDescuento.iva = "0";
                                                rowDescuento.total = rowDescuento.neto;
                                                hoja.descuentoTotal = Convert.ToInt32(valorDesc) + hoja.descuentoTotal;
                                            }
                                            hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);
                                            hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
                                            hoja.tablaDescuentos.Add(rowDescuento);
                                        }
                                        if (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= DateTime.Now.Month)
                                        {
                                            var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                            rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                            rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                            rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                            rowDescuento.oc = descuentoEncabezado.oc;
                                            rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                            rowDescuento.factura = descuentoEncabezado.numero_factura;
                                            rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                            if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                            else rowDescuento.llevaIVA = "NO";

                                            rowDescuento.neto = valorDesc.ToString("N0");

                                            rowDescuento.valorCuota = (descuentoEncabezado.en_uf == false) ? descuentoDetalle.valor_cuota.Value.ToString("N0") : descuentoDetalle.valor_cuota.Value.ToString("N2"); rowDescuento.valorPagar = valorDesc.ToString("N0");
                                            if (descuentoEncabezado.lleva_iva == true)
                                            {
                                                int valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                int valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                rowDescuento.total = valorPagar.ToString("N0");
                                                rowDescuento.iva = valorIva.ToString("N0");
                                                hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                            }
                                            else
                                            {
                                                rowDescuento.iva = "0";
                                                rowDescuento.total = rowDescuento.neto;
                                                hoja.descuentoTotal = Convert.ToInt32(valorDesc) + hoja.descuentoTotal;
                                            }
                                            hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
                                            hoja.descuentoNeto = valorDesc + hoja.descuentoNeto;
                                            hoja.tablaDescuentos.Add(rowDescuento);
                                        }
                                    }
                                }
                            }
                            var leasings = db.leasing.Where(u => u.id_camion.Value == camion.id_camion && u.fechaFin == null).ToList();
                            if (leasings.Count() != 0)
                            {
                                hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                                foreach (leasing leasing in leasings)
                                {
                                    DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                    var detallesLeasings = db.detalle_leasing.Where(u => u.id_leasing.Value == leasing.id_leasing && u.fecha_pagado == null && u.fecha_pago < fechaFinPeriodo).ToList();
                                    if (hoja.subTotalNeto != 0)
                                    {
                                        foreach (detalle_leasing detalleLeasing in detallesLeasings)
                                        {
                                            tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();
                                            int valorPagar = Convert.ToInt32(Math.Round(detalleLeasing.valor_pagar.Value * hoja.valorUF, 0, MidpointRounding.AwayFromZero));
                                            if (detalleLeasing.fecha_pago.Value.Year < DateTime.Now.Year)
                                            {
                                                descLeasing.concepto = "LEASING " + camion.patente;
                                                descLeasing.cuotas = leasing.cuotas;
                                                descLeasing.cuota = detalleLeasing.cuota.Value;
                                                descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                descLeasing.neto = valorPagar.ToString("N0");
                                                descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                hoja.tablaLeasing.Add(descLeasing);
                                            }
                                            if (detalleLeasing.fecha_pago.Value.Year == DateTime.Now.Year && detalleLeasing.fecha_pago.Value.Month <= DateTime.Now.Month)
                                            {
                                                descLeasing.concepto = "LEASING " + camion.patente;
                                                descLeasing.cuotas = leasing.cuotas;
                                                descLeasing.cuota = detalleLeasing.cuota.Value;
                                                descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                descLeasing.neto = valorPagar.ToString("N0");
                                                descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                double temporalLeasingNeto = hoja.leasingNeto * 0.19;
                                                double temporalLeasingTotal = hoja.leasingNeto * 1.19;
                                                hoja.leasingIva = Convert.ToInt32(Math.Round(temporalLeasingNeto, 0, MidpointRounding.AwayFromZero));
                                                hoja.leasingTotal = Convert.ToInt32(Math.Round(temporalLeasingTotal, 0, MidpointRounding.AwayFromZero));
                                                hoja.tablaLeasing.Add(descLeasing);
                                            }
                                        }
                                    }
                                }
                            }
                            hoja.totalDescuentosNeto = hoja.descuentoNeto + hoja.leasingNeto;
                            hoja.totalDescuentosIva = hoja.descuentoIva + hoja.leasingIva;
                            hoja.totalDescuentosTotal = hoja.descuentoTotal + hoja.leasingTotal;
                            hoja.subTotalIva = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                            hoja.subTotal = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                            hoja.retencionNeto = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.05, 0, MidpointRounding.AwayFromZero));
                            hoja.retencionIva = Convert.ToInt32(Math.Round(hoja.retencionNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                            hoja.retencionTotal = Convert.ToInt32(Math.Round(hoja.retencionNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                            hoja.total = hoja.subTotal - hoja.retencionTotal;
                            hoja.totalNeto = hoja.subTotalNeto - hoja.retencionNeto;
                            hoja.totalIva = hoja.subTotalIva - hoja.retencionIva;
                            model.hojaCamion.Add(hoja);
                            model.hojaDetalleCamion.Add(hojaDetalle);
                        }
                    }

                    model.hojaTotal = new camionExcelFranquiciadoPago();
                    foreach (var item in model.hojaCamion)
                    {
                        model.hojaTotal.descuentoIva = item.descuentoIva + model.hojaTotal.descuentoIva;
                        model.hojaTotal.descuentoNeto = item.descuentoNeto + model.hojaTotal.descuentoNeto;
                        model.hojaTotal.descuentoTotal = item.descuentoTotal + model.hojaTotal.descuentoTotal;
                        model.hojaTotal.HRcantidadTotal = item.HRcantidadTotal + model.hojaTotal.HRcantidadTotal;
                        model.hojaTotal.HRIvaTotal = item.HRIvaTotal + model.hojaTotal.HRIvaTotal;
                        model.hojaTotal.HRNetoTotal = item.HRNetoTotal + model.hojaTotal.HRNetoTotal;
                        model.hojaTotal.HRTotal = item.HRTotal + model.hojaTotal.HRTotal;
                        model.hojaTotal.leasingIva = item.leasingIva + model.hojaTotal.leasingIva;
                        model.hojaTotal.leasingNeto = item.leasingNeto + model.hojaTotal.leasingNeto;
                        model.hojaTotal.leasingTotal = item.leasingTotal + model.hojaTotal.leasingTotal;
                        model.hojaTotal.patenteCamion = "Totales";
                        model.hojaTotal.periodo = item.periodo;
                        model.hojaTotal.retencionIva = item.retencionIva + model.hojaTotal.retencionIva;
                        model.hojaTotal.retencionNeto = item.retencionNeto + model.hojaTotal.retencionNeto;
                        model.hojaTotal.retencionTotal = item.retencionTotal + model.hojaTotal.retencionTotal;
                        model.hojaTotal.subTotal = item.subTotal + model.hojaTotal.subTotal;
                        model.hojaTotal.subTotalIva = item.subTotalIva + model.hojaTotal.subTotalIva;
                        model.hojaTotal.subTotalNeto = item.subTotalNeto + model.hojaTotal.subTotalNeto;
                        if (model.hojaTotal.tablaDescuentos == null) model.hojaTotal.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
                        if (item.tablaDescuentos != null)
                        {
                            foreach (var desc in item.tablaDescuentos)
                            {
                                model.hojaTotal.tablaDescuentos.Add(desc);
                            }
                        }
                        if (model.hojaTotal.tablaHR == null) model.hojaTotal.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
                        foreach (var hr in item.tablaHR)
                        {
                            if (model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault() == null)
                            {
                                tablaHojasDeRutaPorTipoExcel nuev = new tablaHojasDeRutaPorTipoExcel();
                                nuev.tipo = hr.tipo;
                                nuev.cantidad = hr.cantidad;
                                nuev.iva = hr.iva;
                                nuev.neto = hr.neto;
                                nuev.total = hr.total;
                                model.hojaTotal.tablaHR.Add(nuev);
                            }
                            else
                            {
                                var row = model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault();
                                int cant = hr.cantidad;
                                model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().cantidad = cant + row.cantidad;
                                model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().iva = hr.iva + row.iva;
                                model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().neto = hr.neto + row.neto;
                                model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().total = hr.total + row.total;
                            }
                        }

                        if (model.hojaTotal.tablaLeasing == null) model.hojaTotal.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                        if (item.tablaLeasing != null)
                        {
                            foreach (var leasing in item.tablaLeasing)
                            {
                                model.hojaTotal.tablaLeasing.Add(leasing);
                            }
                        }
                        model.hojaTotal.total = item.total + model.hojaTotal.total;
                        model.hojaTotal.totalDescuentosIva = item.totalDescuentosIva + model.hojaTotal.totalDescuentosIva;
                        model.hojaTotal.totalDescuentosNeto = item.totalDescuentosNeto + model.hojaTotal.totalDescuentosNeto;
                        model.hojaTotal.totalDescuentosTotal = item.totalDescuentosTotal + model.hojaTotal.totalDescuentosTotal;
                        model.hojaTotal.totalIva = item.totalIva + model.hojaTotal.totalIva;
                        model.hojaTotal.totalNeto = item.totalNeto + model.hojaTotal.totalNeto;
                        model.hojaTotal.valorUF = item.valorUF;
                    }
                    foreach (var camion in camionesFranquiciado)
                    {
                        var anticipos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento == 16).ToList();
                        foreach (var anticipo in anticipos)
                        {
                            DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                            var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == anticipo.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                            foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                            {
                                if ((descuentoDetalle.fecha.Value.Year < fechaFinPeriodo.Year) || (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= fechaFinPeriodo.Year))
                                {
                                    if (anticipo.en_uf == false)
                                    {
                                        model.anticipo = model.anticipo + Convert.ToInt32(descuentoDetalle.valor_pagar);
                                    }
                                }
                            }
                        }
                    }
                    if (model.anticipo != 0)
                    {
                        model.hojaTotal.totalNeto = model.hojaTotal.totalNeto - model.anticipo;
                        model.hojaTotal.totalIva = model.hojaTotal.totalIva - Convert.ToInt32(Math.Round(model.anticipo * 0.19, 0, MidpointRounding.AwayFromZero));
                        model.hojaTotal.total = model.hojaTotal.totalIva + model.hojaTotal.totalNeto;
                    }
                }
            }
            return View(model);
        }

        private String formatoNombreHojaExcel(XLWorkbook libro, String nombreHoja, String adicional)
        {
            nombreHoja = nombreHoja.Trim().Replace("\\", "").Replace("/", "").Replace("?", "").Replace("*", "").Replace("[", "").Replace("]", "");

            if (nombreHoja.Length + adicional.Length > 28) nombreHoja = nombreHoja.Substring(0, 29 - adicional.Length - 1);

            nombreHoja = nombreHoja + adicional;

            nombreHoja = generarNombreHojaUnico(libro, nombreHoja);

            return nombreHoja;
        }
        private string generarNombreHojaUnico(XLWorkbook libro, string nombreHoja)
        {
            var encontrado = false;

            foreach (var hojas in libro.Worksheets)
            {
                if (nombreHoja == hojas.Name)
                {
                    encontrado = true;
                    break;
                }
            }

            int adicional = 1;
            string nombreHojaTemp;

            while (encontrado == true)
            {
                adicional++;
                nombreHojaTemp = nombreHoja + "_" + adicional.ToString();
                encontrado = false;

                foreach (var hojas in libro.Worksheets)
                {
                    if (nombreHojaTemp == hojas.Name)
                    {
                        encontrado = true;
                        break;
                    }
                }

                if (encontrado == false) return nombreHojaTemp;
            }

            return nombreHoja;
        }

        private object convertirEnNumero(Object valor, int tipo)  //tipo:  1=Int32 2=Int64 3=Decimal
        {
            String nuevoValor = "";

            if (valor != null) nuevoValor = valor.ToString();

            try
            {

                nuevoValor = nuevoValor.ToString().Replace(".", "");

                String valor2 = "";

                for (int i = 0; i <= nuevoValor.ToString().Length; i++)
                {
                    if (Char.IsNumber(nuevoValor.ToString(), i) == true || nuevoValor.ToString().Substring(i, 1) == ",")
                    {
                        valor2 = valor2 + nuevoValor.ToString().Substring(i, 1);
                    }
                    else
                    {
                        if (i == 0 && nuevoValor.ToString().Substring(i, 1) == "-") valor2 = valor2 + nuevoValor.ToString().Substring(i, 1);
                    }
                }

                nuevoValor = valor2;
            }
            catch (Exception ex) { }


            try
            {
                if (tipo == 1)
                {
                    return Convert.ToInt32(nuevoValor);
                }

                if (tipo == 2)
                {
                    return Convert.ToInt64(nuevoValor);
                }

                if (tipo == 3)
                {
                    return Convert.ToDecimal(nuevoValor);
                }
            }
            catch (Exception ex) { }

            return valor;
        }
        public void crearTipo(List<tablaHojasDeRutaPorTipoExcel> tabla, string centroCosto)
        {
            tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
            nuevaTipo.total = 0;
            nuevaTipo.tipo = centroCosto;
            nuevaTipo.neto = 0;
            nuevaTipo.iva = 0;
            nuevaTipo.cantidad = 0;
            tabla.Add(nuevaTipo);
        }

       
        public ActionResult excelPrePagoFranquiciados2(string fechaInicio, string fechaFin, int idFranquiciado = -1)
        {
            XLWorkbook workbookConsolidado = null;
            IXLWorksheet worksheetResumen = null;
            int filaResumen = 1;
            int contarResumen = 0;

            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);

            using (var db = new sgsbdEntities())
            {
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var franquiciados = db.franquiciado.Where(w => w.prop_dispositor == false && (w.estado == "Operativo" || w.estado == "Activo" || w.estado == null) && (w.id_franquiciado == idFranquiciado || idFranquiciado == -1) && w.id_unidad == unidad).OrderBy(w => w.razon_social).ToList();

                if (franquiciados != null && idFranquiciado == -1)
                {
                    // *** Se solicitó en 07/2018 (ticket 709) que el archivo Excel NO tenga celdas combinadas ***
                    workbookConsolidado = new XLWorkbook();

                    worksheetResumen = workbookConsolidado.Worksheets.Add("Resumen");

                    worksheetResumen.ShowGridLines = false;

                    worksheetResumen.Style.Font.SetFontName("Calibri");
                    worksheetResumen.Style.Font.SetFontSize(11);

                    //worksheetResumen.Columns().Width = 10.71;
                    //worksheetResumen.Column(1 + colMargen).Width = 28;

                    worksheetResumen.Row(filaResumen).Height = 33;

                    worksheetResumen.Range("A" + filaResumen + ":K" + filaResumen)
                        .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Fill.SetBackgroundColor(XLColor.LightGray);

                    worksheetResumen.Cell("E" + filaResumen)
                        .SetValue("           RESUMEN PREPAGO")
                        .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                        .Font.SetFontSize(18)
                        .Font.SetBold(true);

                    //worksheet.Range(fila, 10 + colMargen, fila, 11 + colMargen).Style.Fill.BackgroundColor = XLColor.LightGray;

                    filaResumen = filaResumen + 2;

                    worksheetResumen.Range("B" + filaResumen + ":B" + (filaResumen + 1))
                        .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Fill.SetBackgroundColor(XLColor.LightGray)
                        .Font.SetBold(true);

                    worksheetResumen.Cell("B" + filaResumen).SetValue("  FECHA INICIO:");
                    worksheetResumen.Cell("B" + (filaResumen + 1)).SetValue("  FECHA TÉRMINO:");

                    worksheetResumen.Range("C" + filaResumen + ":C" + (filaResumen + 1))
                        .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                    worksheetResumen.Cell("C" + filaResumen).SetValue(fechaInicio);
                    worksheetResumen.Cell("C" + (filaResumen + 1)).SetValue(fechaFin);


                    filaResumen = filaResumen + 3;

                    worksheetResumen.Range("A" + filaResumen + ":K" + filaResumen)
                        .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                        .Alignment.SetHorizontal(XLAlignmentHorizontalValues.Center)
                        .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                        .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                        .Fill.SetBackgroundColor(XLColor.LightGray)
                        .Font.SetBold(true);

                    worksheetResumen.Cell("A" + filaResumen).SetValue("N°");
                    worksheetResumen.Cell("B" + filaResumen).SetValue("MICRO EMPRESARIO");
                    worksheetResumen.Cell("C" + filaResumen).SetValue("INGRESOS");
                    worksheetResumen.Cell("D" + filaResumen).SetValue("LEASING");
                    worksheetResumen.Cell("E" + filaResumen).SetValue("DESCUENTOS");
                    worksheetResumen.Cell("F" + filaResumen).SetValue("TOTAL DESCUENTOS");
                    worksheetResumen.Cell("G" + filaResumen).SetValue("SUBTOTAL");
                    worksheetResumen.Cell("H" + filaResumen).SetValue("RETENCIÓN 5%");
                    worksheetResumen.Cell("I" + filaResumen).SetValue("TOTAL LÍQUIDO");
                    worksheetResumen.Cell("J" + filaResumen).SetValue("ANTICIPO");
                    worksheetResumen.Cell("K" + filaResumen).SetValue("TOTAL A CANCELAR");

                    worksheetResumen.Column(1).Width = 4;
                    worksheetResumen.Column(2).Width = 29;
                    worksheetResumen.Columns(3, 11).Width = 12;

                    worksheetResumen.Column("F").Width = 19;
                    worksheetResumen.Column("H").Width = 14;
                    worksheetResumen.Column("I").Width = 14;
                    worksheetResumen.Column("K").Width = 18;
                }

                foreach (var franquiciado in franquiciados)
                {
                    var ultimaFechaPago = db.pago_franquiciados.Where(u => u.id_franquiciado == franquiciado.id_franquiciado && u.id_unidad == unidad).OrderByDescending(u => u.fechaFin);

                    if (ultimaFechaPago.Count() > 0)
                    {
                        fechaDesde = Convert.ToDateTime(ultimaFechaPago.FirstOrDefault().fechaFin).AddDays(1); //Se establece fecha inicial 1 día después del último día pagado
                    }
                    else //En caso de no haber una fecha de pago anterior entonces se cosidera el día 24 anterior a la fecha final
                    {
                        if (fechaHasta.Day <= 24)
                        {
                            fechaDesde = Convert.ToDateTime(fechaInicio).AddMonths(-1);
                            fechaDesde = Convert.ToDateTime("25/" + fechaDesde.Month.ToString() + "/" + fechaDesde.Year.ToString());
                        }
                        else
                        {
                            fechaDesde = Convert.ToDateTime(fechaInicio);
                            fechaDesde = Convert.ToDateTime("25/" + fechaDesde.Month.ToString() + "/" + fechaDesde.Year.ToString());
                        }
                    }

                    excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();

                    model.fechaFin = fechaHasta.ToShortDateString();    //antes fechaFin;
                    model.fechaInicio = fechaDesde.ToShortDateString(); //antes fechaInicio;
                    model.franquiciado = franquiciado.razon_social;
                    model.rutFranquiciado = franquiciado.rut_franquiciado;
                    var camionesFranquiciado = db.camiones.Where(u => u.id_franquiciado == franquiciado.id_franquiciado && u.estado == "Activo").ToList();
                    if (camionesFranquiciado != null)
                    {
                        model.hojaCamion = new List<camionExcelFranquiciadoPago>();
                        model.hojaDetalleCamion = new List<hojaDetalleCamionExcelPrePago>();


                        #region Recorrer Camiones del franquiciado

                        foreach (camiones camion in camionesFranquiciado)
                        {
                            var hojasDeRutaEncabezado = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value <= fechaHasta && u.id_camion.Value == camion.id_camion && u.estado == 3).ToList();
                            //Antes se consideraba el pago con fecha inicial y fecha final, para pagar HDR rezagadas el 13/06/2018 se quitó condición de fecha inicial:     && u.fecha.Value >= fechaDesde
                            if (hojasDeRutaEncabezado != null)
                            {
                                camionExcelFranquiciadoPago hoja = new camionExcelFranquiciadoPago();
                                hojaDetalleCamionExcelPrePago hojaDetalle = new hojaDetalleCamionExcelPrePago();
                                hoja.patenteCamion = camion.patente;
                                hoja.HRcantidadTotal = 0;
                                hoja.HRNetoTotal = 0;
                                hoja.leasingIva = 0;
                                hoja.leasingNeto = 0;
                                int año;
                                int mes;
                                if (fechaDesde.Date >= fechaHasta.Date)
                                {
                                    año = fechaHasta.Year;
                                    mes = fechaHasta.Month;
                                }
                                else
                                {
                                    año = fechaHasta.Year;
                                    mes = fechaHasta.Month;
                                }
                                var uf = db.uf.Where(u => u.año == año && u.mes == mes).FirstOrDefault();

                                if (uf != null) //Agregado 20/11/2017 para evitar error en casos en que se seleccionen periodos largos y alguno de los meses no tenga UF (definir con cliente qué sucede con los meses que no dispongan de UF o bien seleccionar solo 1 mes) Aplica en pago y en prepago
                                    hoja.valorUF = uf.valor_uf;

                                hoja.leasingTotal = 0;
                                hoja.periodo = calculoPeriodo(fechaDesde, fechaHasta);
                                hoja.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
                                hojaDetalle.tabla = new List<tablaDetalleCamionExcelPrePago>();
                                hojaDetalle.patenteCamion = camion.patente;

                                var clientesEspeciales = db.clientes.Where(u => u.cobro_especial == true).ToList();
                                foreach (clientes cliente in clientesEspeciales)
                                {
                                    tablaHojasDeRutaPorTipoExcel nuevaTipo = new tablaHojasDeRutaPorTipoExcel();
                                    nuevaTipo.total = 0;
                                    nuevaTipo.tipo = cliente.razon_social;
                                    nuevaTipo.neto = 0;
                                    nuevaTipo.iva = 0;
                                    nuevaTipo.cantidad = 0;
                                    hoja.tablaHR.Add(nuevaTipo);
                                }
                                int numero = 1;
                                int numeroTemp = 2;

                                #region Recorrer Hojas de Ruta

                                foreach (hoja_ruta hrEncabezado in hojasDeRutaEncabezado.OrderBy(u => u.fecha))
                                {
                                    var nrohdr = hrEncabezado.id_hoja_ruta;
                                    conductores conductor = db.conductores.Where(u => u.id_conductor == hrEncabezado.id_conductor).FirstOrDefault();
                                    var hojasDeRutaDetalle = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == hrEncabezado.id_hoja_ruta).ToList();
                                    IEnumerable<ruta_punto> listaPuntosModelo = new List<ruta_punto>();
                                    if (hrEncabezado.ruta_modelo != null)
                                    {
                                        var rutaModelo = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault();
                                        listaPuntosModelo = rutaModelo.ruta_punto;
                                    }
                                    bool comboUltimoDetalle = false;
                                    bool comboUltimoTarifa = false;
                                    if (hrEncabezado.id_hoja_ruta == 749)
                                    {
                                        comboUltimoTarifa = false;
                                    }
                                    bool debugTemp = false;

                                    foreach (hoja_ruta_detalle hrDetalle in hojasDeRutaDetalle)
                                    {
                                        bool tarifaEspecial = false;
                                        int valorTarifaEspecial = 0;
                                        int idPuntoServicio = hrDetalle.id_punto_servicio;

                                        if (db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() != null)
                                        {
                                            var punto = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault();
                                            int idContrato = db.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault().id_contrato;
                                            int idCliente = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault().id_cliente;
                                            var contrato = db.contratos.Where(u => u.id_contrato == idContrato).FirstOrDefault();
                                            string nombreCRE = (contrato.id_centro_responsabilidad.HasValue) ? db.centro_responsabilidad.Where(u => u.id_centro == contrato.id_centro_responsabilidad).FirstOrDefault().centro_responsabilidad1 : "Sin CRE";
                                            var cliente = db.clientes.Where(u => u.id_cliente == idCliente).FirstOrDefault();
                                            var centroCosto = db.centro_costo.Where(u => u.id == cliente.id_centro_costo).FirstOrDefault();
                                            var tipoServicio = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault();
                                            if ((hrEncabezado.id_tipo_servicio == 2) || (hrEncabezado.id_tipo_servicio == 3)) hrDetalle.cantidad = 1;
                                            tipo_tarifa tarifa = new tipo_tarifa();
                                            if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                            {
                                                if (hrDetalle.estado == 6 || hrDetalle.estado == 5 || hrDetalle.estado == 7)
                                                {
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 2).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    if (hrDetalle.estado == 3 || hrDetalle.estado == 4)
                                                    {
                                                        tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11542).FirstOrDefault();
                                                    }
                                                    else
                                                    {
                                                        tarifa = db.puntos_servicio.Where(l => l.id_punto_servicio == idPuntoServicio).FirstOrDefault().tipo_tarifa;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (comboUltimoTarifa == false && hrDetalle.estado == 2)
                                                {
                                                    comboUltimoTarifa = true;
                                                    int idTipoTarifa = db.ruta_modelo.Where(u => u.id_ruta == hrEncabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11542).FirstOrDefault();

                                                }
                                                tarifa = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == 11542).FirstOrDefault();
                                            }
                                            DateTime ahora = DateTime.Now.Date;

                                            if (tarifa.tarifa_escalable == true)
                                            {
                                                if (db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault() != null)
                                                {
                                                    var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault();
                                                    var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                    if (tarifaFinal != null)
                                                    {
                                                        tarifaEspecial = true;
                                                        valorTarifaEspecial = tarifaFinal.valor;
                                                    }
                                                    else
                                                    {
                                                        valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                    }
                                                }
                                                else
                                                {
                                                    FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
                                                    rs.calculoCantidadPuntos(fechaDesde, fechaHasta, tarifa);
                                                    var calculoTarifa = db.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaDesde.Date && u.fecha_final == fechaHasta.Date && ahora == u.fechaEjecucion).FirstOrDefault();
                                                    var tarifaFinal = db.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                    if (tarifaFinal != null)
                                                    {
                                                        tarifaEspecial = true;
                                                        valorTarifaEspecial = tarifaFinal.valor;
                                                    }
                                                    else
                                                    {
                                                        valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                    }
                                                }
                                            }
                                            int valorTarifa = 0;
                                            if (tarifaEspecial == false)
                                                valorTarifa = Convert.ToInt32(Math.Round(tarifa.valor_tarifa, 0, MidpointRounding.AwayFromZero));
                                            else valorTarifa = valorTarifaEspecial;
                                            bool porPeso;
                                            if (punto.pago_por_peso == true) porPeso = true;
                                            else porPeso = false;
                                            string estados = "2";
                                            if (hrDetalle.estado == 1) estados = "Asignado";
                                            if (hrDetalle.estado == 2) estados = "Realizado";
                                            if (hrDetalle.estado == 3) estados = "Suspendido";//sin
                                            if (hrDetalle.estado == 4) estados = "No realizado";//sus
                                            if (hrDetalle.estado == 5) estados = "Bloqueado";
                                            if (hrDetalle.estado == 6) estados = "No atendido";
                                            if (hrDetalle.estado == 7) estados = "Mal estibado";
                                            if (hrDetalle.estado == 8) estados = "Sin pesaje";
                                            if (hrDetalle.estado == 13) estados = "Anulado";
                                            if (porPeso == false)
                                            {
                                                if (hrDetalle.cantidad == null) hrDetalle.cantidad = 1;
                                                if (tipoServicio.id_tipo_servicio == 2)
                                                {
                                                    for (int i = 1; i <= hrDetalle.cantidad.Value; i++)
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = valorTarifa * hrDetalle.cantidad.Value;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                        if (conductor != null)
                                                        {
                                                            detalleExcel.conductor = conductor.nombre_conductor;
                                                        }
                                                        else
                                                        {
                                                            detalleExcel.conductor = "";
                                                        }

                                                        detalleExcel.estado = estados;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                    }
                                                }
                                                else
                                                {
                                                    if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                                    {
                                                        tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                        detalleExcel.tarifa = valorTarifa;
                                                        detalleExcel.numero = numero++;
                                                        detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                        detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                        detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                        detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                        if (conductor != null)
                                                        {
                                                            detalleExcel.conductor = conductor.nombre_conductor;
                                                        }
                                                        else
                                                        {
                                                            detalleExcel.conductor = "";
                                                        }

                                                        detalleExcel.estado = estados;
                                                        detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                        hojaDetalle.tabla.Add(detalleExcel);
                                                    }
                                                    else
                                                    {
                                                        if (comboUltimoDetalle == false && (hrDetalle.estado == 2))
                                                        {
                                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                            detalleExcel.tarifa = valorTarifa;
                                                            detalleExcel.numero = numero++;
                                                            detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;

                                                            if (conductor != null)
                                                            {
                                                                detalleExcel.conductor = conductor.nombre_conductor;
                                                            }
                                                            else
                                                            {
                                                                detalleExcel.conductor = "";
                                                            }

                                                            detalleExcel.estado = estados;
                                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                            hojaDetalle.tabla.Add(detalleExcel);
                                                            comboUltimoDetalle = true;
                                                        }
                                                        else
                                                        {
                                                            tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                            detalleExcel.tarifa = 0;
                                                            detalleExcel.numero = numero++;
                                                            detalleExcel.id = hrDetalle.id_hoja_ruta;

                                                            if (conductor != null)
                                                            {
                                                                detalleExcel.conductor = conductor.nombre_conductor;
                                                            }
                                                            else
                                                            {
                                                                detalleExcel.conductor = "";
                                                            }

                                                            detalleExcel.estado = estados;
                                                            detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();
                                                            detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                            detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                            detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                            hojaDetalle.tabla.Add(detalleExcel);
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (hrDetalle.peso == null) hrDetalle.peso = 1;
                                                tablaDetalleCamionExcelPrePago detalleExcel = new tablaDetalleCamionExcelPrePago();
                                                double h = hrDetalle.peso.Value;
                                                double b = h / 1000;
                                                double c = valorTarifa * b;
                                                detalleExcel.tarifa = Convert.ToInt32(Math.Round(c, 0, MidpointRounding.AwayFromZero));
                                                detalleExcel.numero = numero++;
                                                detalleExcel.id = hrDetalle.id_hoja_ruta;
                                                detalleExcel.fecha = hrEncabezado.fecha.Value.ToShortDateString();

                                                if (conductor != null)
                                                {
                                                    detalleExcel.conductor = conductor.nombre_conductor;
                                                }
                                                else
                                                {
                                                    detalleExcel.conductor = "";
                                                }

                                                detalleExcel.estado = estados;
                                                detalleExcel.trama = db.tipo_tarifa.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).FirstOrDefault().tipo_tarifa1;
                                                detalleExcel.cliente = punto.nombre_punto + " " + hrDetalle.observacion;
                                                detalleExcel.tipo = db.tipo_servicio.Where(u => u.id_tipo_servicio == hrEncabezado.id_tipo_servicio).FirstOrDefault().tipo_servicio1;
                                                hojaDetalle.tabla.Add(detalleExcel);
                                            }
                                            if (cliente.cobro_especial == true)
                                            {
                                                int valorReal = 0;
                                                if (porPeso == false)
                                                {
                                                    if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) hrDetalle.cantidad = 1;
                                                    if (hrDetalle.cantidad != null)
                                                    {
                                                        valorReal = valorTarifa * hrDetalle.cantidad.Value;
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * hrDetalle.cantidad.Value);
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().cantidad + hrDetalle.cantidad.Value;
                                                    }
                                                }
                                                else
                                                {
                                                    if (hrDetalle.peso != null)
                                                    {
                                                        double h = hrDetalle.peso.Value;
                                                        double b = h / 1000;
                                                        double c = valorTarifa * b;
                                                        valorReal = Convert.ToInt32(c);
                                                        hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto + (valorTarifa * (hrDetalle.peso.Value / 1000));
                                                    }
                                                }
                                                hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                                if (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) hoja.HRcantidadTotal = hoja.HRcantidadTotal + 1;
                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().iva + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                                hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total = hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().total + Convert.ToInt32(Math.Round(hoja.tablaHR.Where(u => u.tipo == cliente.razon_social).FirstOrDefault().neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                            }
                                            else
                                            {
                                                if (hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault() == null) crearTipo(hoja.tablaHR, nombreCRE);
                                                var temporal = hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault();
                                                int valorReal = 0;
                                                if (porPeso == false)
                                                {
                                                    if (hrDetalle.cantidad == 0 && hrEncabezado.id_tipo_servicio == 2) hrDetalle.cantidad = 1;
                                                    if (hrDetalle.cantidad != null)
                                                    {
                                                        valorReal = valorTarifa * hrDetalle.cantidad.Value;
                                                        hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad;
                                                    }
                                                }
                                                else
                                                {
                                                    if (hrDetalle.peso != null)
                                                    {
                                                        double h = hrDetalle.peso.Value;
                                                        double b = h / 1000;
                                                        double c = valorTarifa * b;
                                                        valorReal = Convert.ToInt32(c);
                                                        hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad = (hrDetalle.estado == 5 || hrDetalle.estado == 6 || hrDetalle.estado == 7 || hrDetalle.estado == 2) ? hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad + 1 : hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault().cantidad;
                                                    }
                                                }
                                                var servicioExcel = hoja.tablaHR.Where(u => u.tipo == nombreCRE).FirstOrDefault();
                                                servicioExcel.neto = temporal.neto + valorReal;
                                                hoja.HRNetoTotal = hoja.HRNetoTotal + valorReal;
                                                int iva = int.Parse(Math.Round(valorReal * 0.19, 0, MidpointRounding.AwayFromZero).ToString());
                                                servicioExcel.iva = servicioExcel.iva + iva;
                                                servicioExcel.total = servicioExcel.total + Convert.ToInt32(Math.Round(valorReal * 1.19, 0, MidpointRounding.AwayFromZero));
                                            }

                                        }

                                    }
                                }

                                #endregion

                                if (hoja.HRNetoTotal != 0)
                                {
                                    hoja.HRcantidadTotal = 0;
                                    foreach (var e in hoja.tablaHR.ToList())
                                    {
                                        hoja.HRcantidadTotal = hoja.HRcantidadTotal + e.cantidad;
                                        e.iva = Convert.ToInt32(Math.Round(e.neto * 0.19, 0, MidpointRounding.AwayFromZero));
                                        e.total = Convert.ToInt32(Math.Round(e.neto * 1.19, 0, MidpointRounding.AwayFromZero));
                                    }
                                    hoja.HRIvaTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 0.19, 0, MidpointRounding.AwayFromZero));
                                    hoja.HRTotal = Convert.ToInt32(Math.Round(hoja.HRNetoTotal * 1.19, 0, MidpointRounding.AwayFromZero));
                                }
                                else
                                {
                                    hoja.HRTotal = 0;
                                    hoja.HRIvaTotal = 0;
                                }

                                hojaDetalle.total = hoja.HRNetoTotal;
                                hoja.subTotalNeto = hoja.HRNetoTotal;

                                #region Recorrer Leasings

                                var leasings = db.leasing.Where(u => u.id_camion.Value == camion.id_camion && u.fechaFin == null).ToList();
                                if (leasings.Count() != 0)
                                {
                                    hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                                    foreach (leasing leasing in leasings)
                                    {
                                        DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                        var detallesLeasings = db.detalle_leasing.Where(u => u.id_leasing.Value == leasing.id_leasing && u.fecha_pagado == null && u.fecha_pago < fechaFinPeriodo).ToList();
                                        if (hoja.subTotalNeto != 0)
                                        {
                                            foreach (detalle_leasing detalleLeasing in detallesLeasings)
                                            {
                                                tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();
                                                int valorPagar = Convert.ToInt32(Math.Round(detalleLeasing.valor_pagar.Value * hoja.valorUF, 0, MidpointRounding.AwayFromZero));
                                                if (detalleLeasing.fecha_pago.Value.Year < DateTime.Now.Year)
                                                {
                                                    descLeasing.concepto = "LEASING " + camion.patente;
                                                    descLeasing.cuotas = leasing.cuotas;
                                                    descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    descLeasing.cuota = detalleLeasing.cuota.Value;
                                                    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.neto = valorPagar.ToString("N0");
                                                    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                    descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                    descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                    hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                    hoja.tablaLeasing.Add(descLeasing);
                                                }
                                                if (detalleLeasing.fecha_pago.Value.Year == DateTime.Now.Year && detalleLeasing.fecha_pago.Value.Month <= DateTime.Now.Month)
                                                {
                                                    descLeasing.concepto = "LEASING " + camion.patente;
                                                    descLeasing.cuotas = leasing.cuotas;
                                                    descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    descLeasing.cuota = detalleLeasing.cuota.Value;
                                                    descLeasing.iva = Math.Round(valorPagar * 0.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.neto = valorPagar.ToString("N0");
                                                    descLeasing.periodo = detalleLeasing.fecha_pago.Value.ToShortDateString();
                                                    descLeasing.total = Math.Round(valorPagar * 1.19, 0, MidpointRounding.AwayFromZero).ToString("N0");
                                                    descLeasing.valorCuota = detalleLeasing.saldoCuota.Value.ToString("N2");
                                                    descLeasing.valorPagar = detalleLeasing.valor_pagar.Value.ToString("N2");
                                                    descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                    hoja.subTotalNeto = hoja.subTotalNeto - valorPagar;
                                                    hoja.leasingNeto = valorPagar + hoja.leasingNeto;
                                                    double temporalLeasingNeto = hoja.leasingNeto * 0.19;
                                                    double temporalLeasingTotal = hoja.leasingNeto * 1.19;
                                                    hoja.leasingIva = Convert.ToInt32(Math.Round(temporalLeasingNeto, 0, MidpointRounding.AwayFromZero));
                                                    hoja.leasingTotal = Convert.ToInt32(Math.Round(temporalLeasingTotal, 0, MidpointRounding.AwayFromZero));
                                                    hoja.tablaLeasing.Add(descLeasing);
                                                }
                                            }
                                        }
                                    }
                                }

                                #endregion

                                #region Recorrer Descuentos

                                var descuentos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento != 16).ToList();
                                if (descuentos.Count() > 0)
                                {
                                    hoja.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
                                    foreach (descuento descuentoEncabezado in descuentos)
                                    {
                                        DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                        var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == descuentoEncabezado.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                                        foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                                        {
                                            bool agregarDescuento = false;
                                            tablaDescuentoPrePagoExcel rowDescuento = new tablaDescuentoPrePagoExcel();
                                            int valorDesc = (descuentoEncabezado.en_uf == true) ? Convert.ToInt32(Math.Round(descuentoDetalle.valor_pagar.Value * uf.valor_uf, 0, MidpointRounding.AwayFromZero)) : Convert.ToInt32(descuentoDetalle.valor_pagar.Value);
                                            int valorPagar = 0;
                                            int valorIva = 0;

                                            if (descuentoDetalle.fecha.Value.Year < DateTime.Now.Year)
                                            {
                                                var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                                rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                                rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                                rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                                rowDescuento.oc = descuentoEncabezado.oc;
                                                rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                                rowDescuento.factura = descuentoEncabezado.numero_factura;
                                                rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                                if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                                else rowDescuento.llevaIVA = "NO";
                                                rowDescuento.neto = valorDesc.ToString("N0");
                                                rowDescuento.valorCuota = (descuentoEncabezado.en_uf == false) ? descuentoDetalle.valor_cuota.Value.ToString("N0") : descuentoDetalle.valor_cuota.Value.ToString("N2");
                                                rowDescuento.valorPagar = valorDesc.ToString("N0");
                                                if (descuentoEncabezado.lleva_iva == true)
                                                {
                                                    valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                    valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                    rowDescuento.total = valorPagar.ToString("N0");
                                                    rowDescuento.iva = valorIva.ToString("N0");
                                                    //hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                    //hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                                }
                                                else
                                                {
                                                    rowDescuento.iva = "0";
                                                    rowDescuento.total = rowDescuento.neto;
                                                    //hoja.descuentoTotal = Convert.ToInt32(valorDesc) + hoja.descuentoTotal;
                                                }
                                                //hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);
                                                //hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
                                                //hoja.tablaDescuentos.Add(rowDescuento);
                                                agregarDescuento = true;
                                            }
                                            if (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= DateTime.Now.Month)
                                            {
                                                var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == descuentoEncabezado.id_tipo_descuento.Value).FirstOrDefault();
                                                rowDescuento.idTipoDescuento = descuentoEncabezado.id_tipo_descuento.Value;
                                                rowDescuento.concepto = concepto.nombre + " " + camion.patente; //Agregada Patente para el mostrar en el Excel
                                                rowDescuento.cuotaActual = descuentoDetalle.cuota.Value;
                                                rowDescuento.oc = descuentoEncabezado.oc;
                                                rowDescuento.cuotaTotal = descuentoEncabezado.cuotas.Value.ToString();
                                                rowDescuento.factura = descuentoEncabezado.numero_factura;
                                                rowDescuento.fecha = descuentoDetalle.fecha.Value.ToShortDateString();
                                                if (descuentoEncabezado.lleva_iva == true) rowDescuento.llevaIVA = "SI";
                                                else rowDescuento.llevaIVA = "NO";
                                                rowDescuento.neto = valorDesc.ToString("N0");

                                                rowDescuento.valorCuota = (descuentoEncabezado.en_uf == false) ? descuentoDetalle.valor_cuota.Value.ToString("N0") : descuentoDetalle.valor_cuota.Value.ToString("N2");
                                                rowDescuento.valorPagar = valorDesc.ToString("N0");
                                                if (descuentoEncabezado.lleva_iva == true)
                                                {
                                                    valorPagar = Convert.ToInt32(Math.Round(valorDesc * 1.19, 0, MidpointRounding.AwayFromZero));
                                                    valorIva = Convert.ToInt32(Math.Round(valorDesc * 0.19, 0, MidpointRounding.AwayFromZero));
                                                    rowDescuento.total = valorPagar.ToString("N0");
                                                    rowDescuento.iva = valorIva.ToString("N0");
                                                    //hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                    //hoja.descuentoTotal = valorPagar + hoja.descuentoTotal;
                                                }
                                                else
                                                {
                                                    rowDescuento.iva = "0";
                                                    rowDescuento.total = rowDescuento.neto;
                                                    //hoja.descuentoTotal = Convert.ToInt32(valorDesc) + hoja.descuentoTotal;
                                                }
                                                //hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
                                                //hoja.descuentoNeto = valorDesc + hoja.descuentoNeto;
                                                //hoja.tablaDescuentos.Add(rowDescuento);
                                                agregarDescuento = true;
                                            }

                                            if (agregarDescuento)
                                            {
                                                if (descuentoEncabezado.en_uf)
                                                {

                                                    if (hoja.tablaLeasing == null)
                                                    {
                                                        hoja.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                                                    }

                                                    tablaLeasingPrePagoExcel descLeasing = new tablaLeasingPrePagoExcel();

                                                    descLeasing.concepto = rowDescuento.concepto;
                                                    descLeasing.cuotas = Convert.ToInt32(rowDescuento.cuotaTotal);
                                                    descLeasing.patente = camion.patente; //Agregada Patente para el mostrar en el Excel
                                                    descLeasing.cuota = rowDescuento.cuotaActual;
                                                    descLeasing.iva = rowDescuento.iva;
                                                    descLeasing.neto = rowDescuento.neto;
                                                    descLeasing.periodo = rowDescuento.fecha;
                                                    descLeasing.total = rowDescuento.total;
                                                    descLeasing.valorCuota = rowDescuento.valorCuota;
                                                    descLeasing.valorPagar = rowDescuento.valorPagar;
                                                    descLeasing.valorUF = hoja.valorUF.ToString("N2");
                                                    hoja.subTotalNeto = hoja.subTotalNeto - valorDesc;
                                                    hoja.leasingNeto = valorDesc + hoja.leasingNeto;
                                                    hoja.leasingIva = valorIva + hoja.leasingIva;
                                                    hoja.leasingTotal = valorDesc + valorIva + hoja.leasingTotal;
                                                    hoja.tablaLeasing.Add(descLeasing);
                                                }
                                                else
                                                {
                                                    hoja.subTotalNeto = hoja.subTotalNeto - Convert.ToInt32(valorDesc);

                                                    hoja.descuentoNeto = Convert.ToInt32(valorDesc) + hoja.descuentoNeto;
                                                    hoja.descuentoIva = hoja.descuentoIva + valorIva;
                                                    hoja.descuentoTotal = hoja.descuentoNeto + hoja.descuentoIva + hoja.descuentoTotal;

                                                    hoja.tablaDescuentos.Add(rowDescuento);
                                                }
                                            }

                                        }
                                    }
                                }
                                #endregion

                                hoja.totalDescuentosNeto = hoja.descuentoNeto + hoja.leasingNeto;
                                hoja.totalDescuentosIva = hoja.descuentoIva + hoja.leasingIva;
                                hoja.totalDescuentosTotal = hoja.descuentoTotal + hoja.leasingTotal;
                                hoja.subTotalIva = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                                hoja.subTotal = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionNeto = Convert.ToInt32(Math.Round(hoja.subTotalNeto * 0.05, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionIva = Convert.ToInt32(Math.Round(hoja.retencionNeto * 0.19, 0, MidpointRounding.AwayFromZero));
                                hoja.retencionTotal = Convert.ToInt32(Math.Round(hoja.retencionNeto * 1.19, 0, MidpointRounding.AwayFromZero));
                                hoja.total = hoja.subTotal - hoja.retencionTotal;
                                hoja.totalNeto = hoja.subTotalNeto - hoja.retencionNeto;
                                hoja.totalIva = hoja.subTotalIva - hoja.retencionIva;
                                model.hojaCamion.Add(hoja);
                                model.hojaDetalleCamion.Add(hojaDetalle);
                            }
                        }
                        #endregion

                        model.hojaTotal = new camionExcelFranquiciadoPago();
                        foreach (var item in model.hojaCamion)
                        {
                            model.hojaTotal.descuentoIva = item.descuentoIva + model.hojaTotal.descuentoIva;
                            model.hojaTotal.descuentoNeto = item.descuentoNeto + model.hojaTotal.descuentoNeto;
                            model.hojaTotal.descuentoTotal = item.descuentoTotal + model.hojaTotal.descuentoTotal;
                            model.hojaTotal.HRcantidadTotal = item.HRcantidadTotal + model.hojaTotal.HRcantidadTotal;
                            model.hojaTotal.HRIvaTotal = item.HRIvaTotal + model.hojaTotal.HRIvaTotal;
                            model.hojaTotal.HRNetoTotal = item.HRNetoTotal + model.hojaTotal.HRNetoTotal;
                            model.hojaTotal.HRTotal = item.HRTotal + model.hojaTotal.HRTotal;
                            model.hojaTotal.leasingIva = item.leasingIva + model.hojaTotal.leasingIva;
                            model.hojaTotal.leasingNeto = item.leasingNeto + model.hojaTotal.leasingNeto;
                            model.hojaTotal.leasingTotal = item.leasingTotal + model.hojaTotal.leasingTotal;
                            model.hojaTotal.patenteCamion = "Totales";
                            model.hojaTotal.periodo = item.periodo;
                            model.hojaTotal.retencionIva = item.retencionIva + model.hojaTotal.retencionIva;
                            model.hojaTotal.retencionNeto = item.retencionNeto + model.hojaTotal.retencionNeto;
                            model.hojaTotal.retencionTotal = item.retencionTotal + model.hojaTotal.retencionTotal;
                            model.hojaTotal.subTotal = item.subTotal + model.hojaTotal.subTotal;
                            model.hojaTotal.subTotalIva = item.subTotalIva + model.hojaTotal.subTotalIva;
                            model.hojaTotal.subTotalNeto = item.subTotalNeto + model.hojaTotal.subTotalNeto;
                            if (model.hojaTotal.tablaDescuentos == null) model.hojaTotal.tablaDescuentos = new List<tablaDescuentoPrePagoExcel>();
                            if (item.tablaDescuentos != null)
                            {
                                foreach (var desc in item.tablaDescuentos)
                                {
                                    model.hojaTotal.tablaDescuentos.Add(desc);
                                }
                            }
                            if (model.hojaTotal.tablaHR == null) model.hojaTotal.tablaHR = new List<tablaHojasDeRutaPorTipoExcel>();
                            foreach (var hr in item.tablaHR)
                            {
                                if (model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault() == null)
                                {
                                    tablaHojasDeRutaPorTipoExcel nuev = new tablaHojasDeRutaPorTipoExcel();
                                    nuev.tipo = hr.tipo;
                                    nuev.cantidad = hr.cantidad;
                                    nuev.iva = hr.iva;
                                    nuev.neto = hr.neto;
                                    nuev.total = hr.total;
                                    model.hojaTotal.tablaHR.Add(nuev);
                                }
                                else
                                {
                                    var row = model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault();
                                    int cant = hr.cantidad;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().cantidad = cant + row.cantidad;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().iva = hr.iva + row.iva;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().neto = hr.neto + row.neto;
                                    model.hojaTotal.tablaHR.Where(u => u.tipo.Equals(hr.tipo)).FirstOrDefault().total = hr.total + row.total;
                                }
                            }

                            if (model.hojaTotal.tablaLeasing == null) model.hojaTotal.tablaLeasing = new List<tablaLeasingPrePagoExcel>();
                            if (item.tablaLeasing != null)
                            {
                                foreach (var leasing in item.tablaLeasing)
                                {
                                    model.hojaTotal.tablaLeasing.Add(leasing);
                                }
                            }
                            model.hojaTotal.total = item.total + model.hojaTotal.total;
                            model.hojaTotal.totalDescuentosIva = item.totalDescuentosIva + model.hojaTotal.totalDescuentosIva;
                            model.hojaTotal.totalDescuentosNeto = item.totalDescuentosNeto + model.hojaTotal.totalDescuentosNeto;
                            model.hojaTotal.totalDescuentosTotal = item.totalDescuentosTotal + model.hojaTotal.totalDescuentosTotal;
                            model.hojaTotal.totalIva = item.totalIva + model.hojaTotal.totalIva;
                            model.hojaTotal.totalNeto = item.totalNeto + model.hojaTotal.totalNeto;
                            model.hojaTotal.valorUF = item.valorUF;
                        }

                        //*** Anticipos ***
                        foreach (var camion in camionesFranquiciado)
                        {
                            var anticipos = db.descuento.Where(u => u.id_camion.Value == camion.id_camion && u.estado_descuento.Value == 0 && u.id_tipo_descuento == 16).ToList();
                            foreach (var anticipo in anticipos)
                            {
                                DateTime fechaFinPeriodo = Convert.ToDateTime("01-" + fechaHasta.Month.ToString() + "-" + fechaHasta.Year.ToString()).AddMonths(1).AddMinutes(-1);
                                var descuentosDetalles = db.detalle_descuento.Where(u => u.id_descuento == anticipo.id_descuento && u.fecha < fechaFinPeriodo && u.estado.Value == 0).ToList();
                                foreach (detalle_descuento descuentoDetalle in descuentosDetalles)
                                {
                                    if ((descuentoDetalle.fecha.Value.Year < fechaFinPeriodo.Year) || (descuentoDetalle.fecha.Value.Year == DateTime.Now.Year && descuentoDetalle.fecha.Value.Month <= fechaFinPeriodo.Year))
                                    {
                                        if (anticipo.en_uf == false)
                                        {
                                            model.anticipo = model.anticipo + Convert.ToInt32(descuentoDetalle.valor_pagar);
                                        }
                                    }
                                }
                            }
                        }
                        if (model.anticipo != 0)
                        {
                            model.hojaTotal.totalNeto = model.hojaTotal.totalNeto - model.anticipo;
                            model.hojaTotal.totalIva = model.hojaTotal.totalIva - Convert.ToInt32(Math.Round(model.anticipo * 0.19, 0, MidpointRounding.AwayFromZero));
                            model.hojaTotal.total = model.hojaTotal.totalIva + model.hojaTotal.totalNeto;
                        }
                    }

                    if (model.hojaTotal.patenteCamion == null) model.hojaTotal.patenteCamion = "Totales";

                    if (model.hojaTotal != null)
                    {
                        if (idFranquiciado == -1)
                        {
                            // *** Se solicitó en 07/2018 (ticket 709) que el archivo Excel NO tenga celdas combinadas ***
                            generarHojaExcelPrePago(workbookConsolidado, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, model.hojaTotal, model.franquiciado.TrimEnd(), true, model.hojaCamion);
                            generarHojaExcelPagoDetalleConsolidado(workbookConsolidado, model);

                            filaResumen++;
                            contarResumen++;

                            worksheetResumen.Range("A" + filaResumen, "K" + filaResumen)
                                .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                                .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                            worksheetResumen.Cell("A" + filaResumen)
                                .SetValue(contarResumen)
                                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                                .NumberFormat.SetNumberFormatId(1);

                            worksheetResumen.Range("C" + filaResumen + ":K" + filaResumen)
                                .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                                .NumberFormat.SetFormat("$ #,##0");

                            worksheetResumen.Cell("A" + filaResumen).SetValue(contarResumen);
                            worksheetResumen.Cell("B" + filaResumen).SetValue(model.franquiciado.Trim());
                            worksheetResumen.Cell("C" + filaResumen).SetValue(model.hojaTotal.HRTotal);
                            worksheetResumen.Cell("D" + filaResumen).SetValue(model.hojaTotal.leasingTotal);
                            worksheetResumen.Cell("E" + filaResumen).SetValue(model.hojaTotal.descuentoTotal);
                            worksheetResumen.Cell("F" + filaResumen).SetValue(model.hojaTotal.totalDescuentosTotal);
                            worksheetResumen.Cell("G" + filaResumen).SetValue(model.hojaTotal.subTotal);
                            worksheetResumen.Cell("H" + filaResumen).SetValue(model.hojaTotal.retencionTotal);
                            worksheetResumen.Cell("I" + filaResumen).SetValue(model.hojaTotal.totalNeto + model.anticipo + model.hojaTotal.totalIva + model.anticipo * 0.19);
                            worksheetResumen.Cell("J" + filaResumen).SetValue(model.anticipo);
                            worksheetResumen.Cell("K" + filaResumen).SetValue(model.hojaTotal.total + (model.anticipo * 0.19));

                        }
                        else
                        {
                            var archivoPrePago = generarExcelPrePago(model);
                            return File(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\" + archivoPrePago, System.Net.Mime.MediaTypeNames.Application.Octet, archivoPrePago);
                        }

                    }

                }

            }


            if (workbookConsolidado != null)
            {
                //worksheetResumen.Columns().AdjustToContents();
                //worksheetResumen.Column("B").Width = 20;
                //worksheetResumen.Column("C").Width = 11;

                var archivoPrePagoConsolidado = "CONSOLIDADO_PrePago_" + fechaInicio.Replace("/", "-") + "_" + fechaFin.Replace("/", "-") + ".xlsx";

                workbookConsolidado.SaveAs(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\" + archivoPrePagoConsolidado);

                return File(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\" + archivoPrePagoConsolidado, System.Net.Mime.MediaTypeNames.Application.Octet, archivoPrePagoConsolidado);
            }

            //return View(model);
            return null;
        }

        private string generarExcelPrePago(excelFranquiciadoPrePago model)
        {
            var workbook = new XLWorkbook();

            generarHojaExcelPrePago(workbook, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, model.hojaTotal, model.hojaTotal.patenteCamion, true, model.hojaCamion);

            foreach (var hoja in model.hojaCamion)
            {
                generarHojaExcelPrePago(workbook, model.franquiciado.TrimEnd(), model.rutFranquiciado, model.fechaInicio, model.fechaFin, model.anticipo, hoja, hoja.patenteCamion);
            }

            foreach (var hoja in model.hojaDetalleCamion)
            {
                generarHojaExcelPrePagoDetalle(workbook, hoja);
            }

            var archivoPrePago = "prePago" + model.rutFranquiciado + "-" + model.fechaInicio.Replace("/", "_").Replace("-", "_") + "-" + model.fechaFin.Replace("/", "_").Replace("-", "_") + ".xlsx";
            workbook.SaveAs(HttpContext.Server.MapPath("~/HistorialPagosFranquiciados") + "\\" + archivoPrePago);

            return archivoPrePago;
        }
        private void generarHojaExcelPrePagoDetalle(XLWorkbook workbook, hojaDetalleCamionExcelPrePago hoja)
        {
            var worksheet = workbook.Worksheets.Add(formatoNombreHojaExcel(workbook, hoja.patenteCamion, " Detalle"));

            worksheet.ShowGridLines = false;

            int colMargen = 0;
            int fila = 1;

            worksheet.Style.Font.SetFontName("Calibri");
            worksheet.Style.Font.SetFontSize(11);

            //worksheet.Columns().Width = 10.71;
            //worksheet.Column(1 + colMargen).Width = 28;

            worksheet.Range(fila, 1 + colMargen, fila, 9 + colMargen)
                .Style.Alignment.SetVertical(XLAlignmentVerticalValues.Center)
                .Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                .Border.SetInsideBorder(XLBorderStyleValues.Thin)
                .Font.SetBold(true);

            worksheet.Cell(fila, 1 + colMargen).SetValue("N°");
            worksheet.Cell(fila, 2 + colMargen).SetValue("FECHA");
            worksheet.Cell(fila, 3 + colMargen).SetValue("CLIENTE");
            worksheet.Cell(fila, 4 + colMargen).SetValue("HDR");
            worksheet.Cell(fila, 5 + colMargen).SetValue("TRAMO");
            worksheet.Cell(fila, 6 + colMargen).SetValue("TARIFA");
            worksheet.Cell(fila, 7 + colMargen).SetValue("CONDUCTOR");
            worksheet.Cell(fila, 8 + colMargen).SetValue("ESTADO");
            worksheet.Cell(fila, 9 + colMargen).SetValue("TIPO");

            foreach (var row in hoja.tabla)
            {
                fila++;

                worksheet.Range(fila, 1 + colMargen, fila, 9 + colMargen)
                    .Style.Border.SetOutsideBorder(XLBorderStyleValues.Thin)
                    .Border.SetInsideBorder(XLBorderStyleValues.Thin);

                worksheet.Cell(fila, 1 + colMargen)
                    .SetValue(row.numero)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetNumberFormatId(1);

                worksheet.Cell(fila, 2 + colMargen).SetValue(row.fecha);
                worksheet.Cell(fila, 3 + colMargen).SetValue(row.cliente);

                worksheet.Cell(fila, 4 + colMargen)
                    .SetValue(row.id)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetNumberFormatId(1);

                worksheet.Cell(fila, 5 + colMargen).SetValue(row.trama);

                worksheet.Cell(fila, 6 + colMargen)
                    .SetValue(row.tarifa)
                    .Style.Alignment.SetHorizontal(XLAlignmentHorizontalValues.Right)
                    .NumberFormat.SetFormat("$ #,##0");

                worksheet.Cell(fila, 7 + colMargen).SetValue(row.conductor);
                worksheet.Cell(fila, 8 + colMargen).SetValue(row.estado);
                worksheet.Cell(fila, 9 + colMargen).SetValue(row.tipo);
            }

            worksheet.Columns().AdjustToContents();

        }
        public void calculoCantidadPuntos(DateTime fechaInicio, DateTime fechaFin, tipo_tarifa tarifa)
        {
            int cantidad = 0;
            using (var db = new sgsbdEntities())
            {
                var puntos = db.puntos_servicio.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).ToList();
                var lista = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value >= fechaInicio && u.fecha.Value <= fechaFin && u.estado == 3).ToList();
                foreach (var header in lista)
                {
                    var lines = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == header.id_hoja_ruta).ToList();
                    foreach (var line in lines)
                    {
                        if (puntos.Where(u => u.id_punto_servicio == line.id_punto_servicio).FirstOrDefault() != null && (line.estado == 2 || line.estado == 6)) cantidad++;
                    }
                }
                var nueva = db.detalle_calculo_tarifa_escalable.Create();
                nueva.id = db.detalle_calculo_tarifa_escalable.Count() + 1;
                nueva.id_tipo_tarifa = tarifa.id_tipo_tarifa;
                nueva.fecha_final = fechaFin;
                nueva.fecha_inicio = fechaInicio;
                nueva.cantidad = cantidad;
                nueva.fechaEjecucion = DateTime.Now;
                db.detalle_calculo_tarifa_escalable.Add(nueva);
                db.SaveChanges();

            }
        }
        public ActionResult excelPagadosFranquiciados(string fechaInicio, string fechaFin, int idFranquiciado)
        {
            DateTime fechaDesde = Convert.ToDateTime(fechaInicio);
            DateTime fechaHasta = Convert.ToDateTime(fechaFin).AddDays(1).AddMilliseconds(-1);
            excelFranquiciadoPrePago model = new excelFranquiciadoPrePago();
            using (var db = new sgsbdEntities())
            {
                int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var franquiciado = db.franquiciado.Where(u => u.id_franquiciado == idFranquiciado && u.id_unidad == unidad).FirstOrDefault();
                model.fechaFin = fechaFin;
                model.fechaInicio = fechaInicio;
                model.franquiciado = franquiciado.razon_social;
                model.rutFranquiciado = franquiciado.rut_franquiciado;

            }
            string format = "dd-MM-yyyy";
            model.fechaInicio = fechaDesde.ToString(format);
            model.fechaFin = fechaHasta.ToString(format);
            string mydocpath = HttpContext.Server.MapPath("~/HistorialPagosFranquiciados");
            return File(mydocpath + @"\PAGADO_" + model.rutFranquiciado + "_" + model.fechaInicio + "_" + model.fechaFin + ".xml", System.Net.Mime.MediaTypeNames.Application.Octet, "PAGADO_" + model.rutFranquiciado + "_" + model.fechaInicio + "_" + model.fechaFin + ".xml");
        }
        public ActionResult edit(int id)
        {

            FranquiciadosRepository rs = new FranquiciadosRepository(new sgsbdEntities());
            franquiciado c = rs.GetFranquiciadoByID(id);

            if (c.direccion != null && c.direccion != "")
            {
                string[] split = c.direccion.Split(null, 2);

                //para sacar la direccion y el numero de direccion
                StringBuilder sb = new StringBuilder();
                string[] split2 = split[1].Split(null);
                int _count = split2.Count();

                for (int i = 0; i < _count - 1; i++)
                {
                    sb.Append(" " + split2[i]);
                }

                ViewBag.tipo = split[0];
                ViewBag.tipoc = ViewBag.tipo;

                ViewBag.direccion = sb.ToString().Trim();
                ViewBag.numero_direccion = split2[_count - 1];

                string fechaval = "";
                string fechadata = "";

                if (String.IsNullOrEmpty(Convert.ToString(c.fecha_contrato)))
                {
                    fechaval = "00/00/0000";

                }
                else
                {
                    fechaval = Convert.ToString(c.fecha_contrato);
                    string mes = fechaval.Substring(0, 2);
                    string dia = fechaval.Substring(3, 2);
                    string ano = fechaval.Substring(6, 4);

                    fechadata = String.Format("{0:dd-mm-yyyy}", dia + "/" + mes + "/" + ano);


                }

                ViewBag.datepickerNoModel = String.Format("{0:dd-mm-yyyy}", fechadata);
            }

            this.buildViewBagList();
            return View("create", c);
        }
        //public ActionResult Delete(int id)
        //{

        //    this.rs = new FranquiciadosRepository(new sgsbdEntities());
        //    rs.DeleteFranquiciado(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "Franquiciados");

        //}
        public string calculoPeriodo(DateTime fechaInicio, DateTime fechaFin)
        {
            string periodo;
            if (fechaInicio.Date >= fechaFin.Date)
            {
                periodo = fechaFin.Month.ToString() + "-" + fechaFin.Year.ToString();
            }
            else
            {
                periodo = fechaFin.Month.ToString() + "-" + fechaFin.Year;
            }
            return periodo;
        }
        public void llenaConceptos(CrearDescuentoView model)
        {
            sgsbdEntities db = new sgsbdEntities();

            if (model.fechaPrimeraCuota == null)
            {
                model.fechaPrimeraCuota = new selectListCamiones();
            }
            if (model.conceptoSelectList == null)
            {
                model.conceptoSelectList = new selectListConceptosDescuentos();
            }
            model.conceptoSelectList.conceptos = db.tipo_descuento.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.nombre,
                Value = x.id_tipo_descuento.ToString()
            });

            string fecha1 = DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString();
            string fecha2 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string fecha3 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
            string fecha4 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
            SelectList periodos = new SelectList(
                     new List<Object>{ 
                   new { value = "1", text = fecha1  },
                   new { value = "2" , text = fecha2  },
                   new { value = "3" , text = fecha3 },
                   new { value = "4" , text = fecha4  }},
                "value",
                "text");
            model.fechaPrimeraCuota.camiones = new SelectList(periodos, "value", "text", model.fechaPrimeraCuota.camionSelect);
        }
        public void llenaConceptos(CrearLeasingView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            var guardarfecha = db.leasing.Where(u => u.id_leasing == model.idLeasing).FirstOrDefault();

            if (model.fechaPrimeraCuota == null)
            {
                model.fechaPrimeraCuota = new selectListLeasing();
            }
            string fecha0 = DateTime.Now.AddMonths(-1).Month.ToString() + "-" + DateTime.Now.AddMonths(-1).Year.ToString();
            string fecha1 = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string fecha2 = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
            string fecha3 = DateTime.Now.AddMonths(2).Month.ToString() + "-" + DateTime.Now.AddMonths(2).Year.ToString();
            if (guardarfecha != null)
            {
                var quitado = guardarfecha.fecha.Value.Month.ToString() + "-" + guardarfecha.fecha.Value.Year.ToString();
                if (quitado.ToString() == fecha0.ToString())
                {
                    SelectList periodos = new SelectList(
                       new List<Object>{
                   new { value = "0" , text = fecha0  },
                   new { value = "1" , text = fecha1  },
                   new { value = "2" , text = fecha2  },
                   new { value = "3" , text = fecha3 } },
                  "value",
                  "text");
                    model.fechaPrimeraCuota.leasing = new SelectList(periodos, "value", "text", model.fechaPrimeraCuota.leasingSelect);
                }
                else
                {
                    SelectList periodos = new SelectList(
                             new List<Object>{
                   new { value = "1" , text = fecha1  },
                   new { value = "2" , text = fecha2  },
                   new { value = "3" , text = fecha3 } },
                        "value",
                        "text");
                    model.fechaPrimeraCuota.leasing = new SelectList(periodos, "value", "text", model.fechaPrimeraCuota.leasingSelect);

                }
            }
            else
            {
                SelectList periodos = new SelectList(
                               new List<Object>{
                   new { value = "1" , text = fecha1  },
                   new { value = "2" , text = fecha2  },
                   new { value = "3" , text = fecha3 } },
                          "value",
                          "text");
                model.fechaPrimeraCuota.leasing = new SelectList(periodos, "value", "text", model.fechaPrimeraCuota.leasingSelect);
            }
        }
        public void llenaFranquiciados(CrearDescuentoView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.franquiciadosSelectList == null)
            {
                model.franquiciadosSelectList = new selectListFranquiciados();
                if (model.idFranquiciado != model.franquiciadosSelectList.franquiciadoSelect) model.franquiciadosSelectList.franquiciadoSelect = -1;
            }
            int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            model.franquiciadosSelectList.franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.razon_social,
                Value = x.id_franquiciado.ToString(),
            });
        }
        public void llenaFranquiciadosFiltro(DescuentosView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.selecListFranquiciados == null)
            {
                model.selecListFranquiciados = new selectListFranquiciadosFiltro();
                model.selecListFranquiciados.franquiciadoSelect = -1;
            }
            int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            model.selecListFranquiciados.franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.razon_social,
                Value = x.id_franquiciado.ToString(),
            });
        }
        public void llenaFranquiciadosFiltroLeasing(LeasingView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.selecListFranquiciados == null)
            {
                model.selecListFranquiciados = new selectListFranquiciadosFiltro();
                model.selecListFranquiciados.franquiciadoSelect = -1;
            }

            int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            model.selecListFranquiciados.franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.razon_social,
                Value = x.id_franquiciado.ToString(),
            });
        }
        public void llenaFranquiciadosFiltroPagados(franquiciadosPagadosView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.selecListFranquiciados == null)
            {
                model.selecListFranquiciados = new selectListFranquiciadosFiltro();
                model.selecListFranquiciados.franquiciadoSelect = -1;
            }
            int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            model.selecListFranquiciados.franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.razon_social,
                Value = x.id_franquiciado.ToString(),
            });
        }
        public void llenaPeriodosFiltroPagados(franquiciadosPagadosView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.selecListPeriodos == null)
            {
                model.selecListPeriodos = new selectListPeriodos();
                model.selecListPeriodos.periodoSelect = -1;
            }
            SelectList periodos = new SelectList(
                     new List<Object>{ 
                   new { value = "1" , text = "11-2014"  },
                   new { value = "2" , text = "12-2014"  },
                   new { value = "3" , text = "01-2015" },
                   new { value = "4" , text = "02-2015"  },
                   new { value = "5" , text = "03-2015" },
                   new { value = "6" , text = "04-2015"  },
                   new { value = "7" , text = "05-2015" },
                   new { value = "8" , text = "06-2015"  },
                   new { value = "9" , text = "07-2015" },
                   new { value = "10" , text = "08-2015"  },
                   new { value = "11" , text = "09-2015" },
                   new { value = "12" , text = "10-2015" },
                   new { value = "13" , text = "11-2015" },
                   new { value = "14" , text = "12-2015" }},
                "value",
                "text");
            model.selecListPeriodos.periodos = new SelectList(periodos, "value", "text", model.selecListPeriodos.periodos);
        }
        public void llenaFranquiciadosLeasing(CrearLeasingView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.franquiciadosSelectList == null)
            {
                model.franquiciadosSelectList = new selectListFranquiciados();
                if (model.idFranquiciado != model.franquiciadosSelectList.franquiciadoSelect) model.franquiciadosSelectList.franquiciadoSelect = -1;
            }
            int unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
            model.franquiciadosSelectList.franquiciados = db.franquiciado.Where(u => u.id_unidad == unidad).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.razon_social,
                Value = x.id_franquiciado.ToString(),
            });
        }
        public void llenaCamiones(CrearDescuentoView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.camionesSelectList == null)
            {
                model.camionesSelectList = new selectListCamiones();
            }
            model.camionesSelectList.camiones = db.camiones.Where(u => u.id_franquiciado == model.franquiciadosSelectList.franquiciadoSelect).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.patente,
                Value = x.id_camion.ToString()
            });
        }
        public void llenaCamionesLeasing(CrearLeasingView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.camionesSelectList == null)
            {
                model.camionesSelectList = new selectListCamiones();
            }
            model.camionesSelectList.camiones = db.camiones.Where(u => u.id_franquiciado == model.franquiciadosSelectList.franquiciadoSelect).AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.patente,
                Value = x.id_camion.ToString()
            });
        }
        public void llenaEstados(DescuentosView model)
        {
            SelectList estado = new SelectList(
                     new List<Object>{ 
                   new { value = "2" , text = "Todos"  },
                   new { value = "1" , text = "Cancelado" },
                   new { value = "0" , text = "Vigente" }},
                "value",
                "text");
            if (model.selectListEstados == null)
            {
                model.selectListEstados = new selectListEstados();
                model.selectListEstados.estadoSelect = 2;
            }
            model.selectListEstados.estados = new SelectList(estado, "value", "text", model.selectListEstados.estadoSelect);
        }
        public void llenaEstadosLeasing(LeasingView model)
        {
            SelectList estado = new SelectList(
                     new List<Object>{ 
                   new { value = "2" , text = "Todos"  },
                   new { value = "1" , text = "Cancelado" },
                   new { value = "0" , text = "Vigente" }},
                "value",
                "text");
            if (model.selectListEstados == null)
            {
                model.selectListEstados = new selectListEstados();
                model.selectListEstados.estadoSelect = 2;
            }
            model.selectListEstados.estados = new SelectList(estado, "value", "text", model.selectListEstados.estadoSelect);
        }
        public void llenaMesAño(FranquiciadosProcesosView model)
        {
            SelectList año = new SelectList(
                     new List<Object>{ 
                   new { value = "2014" , text = "2014"  },
                   new { value = "2015" , text = "2015" }},
                "value",
                "text");
            SelectList mes = new SelectList(
                     new List<Object>{ 
                   new { value = "1" , text = "Enero"  },
                   new { value = "2" , text = "Febrero" },
                   new { value = "3" , text = "Marzo"  },
                   new { value = "4" , text = "Abril" },
                   new { value = "5" , text = "Mayo"  },
                   new { value = "6" , text = "Junio" },
                   new { value = "7" , text = "Julio"  },
                   new { value = "8" , text = "Agosto" },
                   new { value = "9" , text = "Septiembre"  },
                   new { value = "10" , text = "Octubre" },
                   new { value = "11" , text = "Noviembre"  },
                   new { value = "12" , text = "Diciembre" }},
                "value",
                "text");
            if (model.año == null || model.mes == null)
            {
                model.año = new selectListAño();
                model.mes = new selectListMes();
                model.año.añoSelect = DateTime.Now.Year;
                model.mes.mesSelect = DateTime.Now.Month;
            }
            model.año.año = new SelectList(año, "value", "text", model.año.añoSelect);
            model.mes.mes = new SelectList(mes, "value", "text", model.mes.mesSelect);
        }
        private void buildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            IEnumerable<object> estado = new object[] {
             new { text="Operativo",value="Operativo"},
             new { text="Fuera de Servicio",value="Fuera de Servicio"},
            };


            IEnumerable<object> tipoc = new object[] {
             new { text="Avenida",value="Avenida"},
             new { text="Calle",value="Calle"},
             new { text="Pasaje",value="Pasaje"}
            };
            ViewBag.estado = new SelectList(estado, "value", "text", ViewBag.value_estado);
            ViewBag.tipoc = new SelectList(tipoc, "value", "text", ViewBag.tipoc);
        }
        private void buildViewBagList2()
        {

            sgsbdEntities ctx = new sgsbdEntities();
            TipoDescuentoRepository rs = new TipoDescuentoRepository(ctx);


            IEnumerable<object> estado = new object[] {
             new { text="Operativo",value="Operativo"},
             new { text="Fuera de Servicio",value="Fuera de Servicio"},
            };


            IEnumerable<object> tipoc = new object[] {
             new { text="Avenida",value="Avenida"},
             new { text="Calle",value="Calle"},
             new { text="Pasaje",value="Pasaje"}
            };
            ViewBag.estado = new SelectList(estado, "value", "text", ViewBag.value_estado);
            ViewBag.tipoc = new SelectList(tipoc, "value", "text", ViewBag.tipoc);

            ViewBag.tipod = new SelectList(rs.GetTipoDescuento(), "id_tipo_descuento", "nombre");
        }
        private double valorActualPetroleo()
        {
            double valor = 0;

            using (var db = new sgsbdEntities())
            {
                var petroleo = db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault();

                if (petroleo != null) valor = petroleo.valor;
            }

            return valor;
        }

    }
}
