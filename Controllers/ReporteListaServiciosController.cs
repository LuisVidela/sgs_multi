﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;


namespace SGS.Controllers
{
    public class ReporteListaServiciosController : Controller
    {
        /// <summary>
        /// Empresa: valentys
        /// Fecha : 13-08-2015
        /// Descripción: carga vista principal.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Empresa: Valentys
        /// Fecha: 13-08-2015
        /// Descripción: Buscar la lista de servicio
        /// </summary>
        /// <param name="desde">Fecha desde</param>
        /// <param name="hasta">Fecha hasta</param>
        /// <returns></returns>
        public ActionResult Buscar(String desde, String hasta)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_ListaServicio_Result> lstListaServicios = db.SP_SEL_ListaServicio(inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View("result", lstListaServicios);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="desde"></param>
        /// <param name="hasta"></param>
        /// <returns></returns>
        public ActionResult Excel(String desde, String hasta)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_ListaServicio_Result> lstListaServicios = db.SP_SEL_ListaServicio(inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            ViewBag.desde = desde;
            ViewBag.hasta = hasta;

            return View(lstListaServicios);
        }
    }
}
