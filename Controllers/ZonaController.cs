﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.modelViews;
using SGS.Models;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class ZonaController : Controller
    {
        //
        // GET: /Zona/

        public ActionResult Index()
        {
            ZonasRepository rs = new ZonasRepository(new sgsbdEntities());
            List<Zona> data = (List<Zona>)rs.GetZonas();

            return View(data);
        }

        public ActionResult create()
        {

            return View();
        }


        [HttpPost]
        public ActionResult create(Zona model)
        {

            if (ModelState.IsValid)
            {

                ZonasRepository rs = new ZonasRepository(new sgsbdEntities());

                if (model.id_zona == 0)
                {
                    rs.InsertZona(model);
                }
                else
                {
                    rs.UpdateZona(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Zona");

            }


            return View(model);
        }

        public ActionResult edit(int id)
        {

            ZonasRepository rs = new ZonasRepository(new sgsbdEntities());
            Zona c = rs.GetZonasByID(id);
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            ZonasRepository rs = new ZonasRepository(new sgsbdEntities());
            rs.DeleteZona(id);
            rs.Save();
            return this.RedirectToAction("Index", "Zona");

        }


    }
}
