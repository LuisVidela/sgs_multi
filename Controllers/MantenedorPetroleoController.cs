﻿using SGS.Models;
using SGS.Models.modelViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace SGS.Controllers
{
    public class MantenedorPetroleoController : Controller
    {
        //
        // GET: /MantenedorPetroleo/

        public ActionResult Index()
        {
            sgsbdEntities db = new sgsbdEntities();
            petroleoIndex model = new petroleoIndex();
            int mesPosterior = DateTime.Now.AddMonths(1).Month;
            int añoAnterior = DateTime.Now.AddMonths(1).Year;
            if (db.uf.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() != null && db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() != null)
            {
                model.puedeAgregar = false;
            }
            else
            {
                model.puedeAgregar = true;
            }
            List<petroleosRow> lista = new List<petroleosRow>();
            int id_unidad = Convert.ToInt32(Session["unidad_seleccionada"]);
            var petroleos = db.petroleo.Where(u => u.id_unidad_negocio == id_unidad);
            foreach (var item in petroleos)
            {
                petroleosRow row = new petroleosRow();
                row.id = item.id_petroleo;
                row.periodo = item.mes.ToString() + "-" + item.año.ToString();
                row.valor= item.valor.ToString("N2");
                if (item.año == DateTime.Now.Year && item.mes == DateTime.Now.Month) row.editable = true;
                if (item.año == DateTime.Now.AddMonths(1).Year && item.mes == DateTime.Now.AddMonths(1).Month) row.editable = true;
                lista.Add(row);
            }
            model.tabla = lista;
            return View(model);
        }

        public ActionResult crearPetroleo(int id)
        {
            crearPetroleoView model = new crearPetroleoView();
            model.idPetroleo = id;
            using (var db = new sgsbdEntities())
            {
                if (id != -1)
                {
                    var uf = db.petroleo.Where(u => u.id_petroleo == id).FirstOrDefault();
                    model.idPetroleo = uf.id_petroleo;
                    model.valorPetroleo = uf.valor.ToString("N2");
                }
            }
            llenaPeriodos(model);
            return View(model);
        }
        [HttpPost]
        public ActionResult crearPetroleo(crearPetroleoView model)
        {
            using (var db = new sgsbdEntities())
            {
                if (model.idPetroleo != -1)
                {
                    var modificar = db.petroleo.Where(u => u.id_petroleo == model.idPetroleo).FirstOrDefault();
                    modificar.valor = double.Parse(model.valorPetroleo.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                    db.SaveChanges();
                }
                else
                {
                    var nueva = db.petroleo.Create();
                    if (model.periodo != null)
                    {
                        if (model.periodo.select == 1)
                        {
                            nueva.año = DateTime.Now.Year;
                            nueva.mes = DateTime.Now.Month;
                        }
                        else
                        {
                            nueva.mes = DateTime.Now.AddMonths(1).Month;
                            nueva.año = DateTime.Now.AddMonths(1).Year;
                        }
                        var user = db.UserProfile.Where(u => u.UserId == WebSecurity.CurrentUserId).FirstOrDefault();
                        nueva.id_unidad_negocio = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                        nueva.valor = double.Parse(model.valorPetroleo.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                        db.petroleo.Add(nueva);
                        db.SaveChanges();
                    }
                }
            }
            return RedirectToAction("Index", "MantenedorPetroleo");
        }
        public void llenaPeriodos(crearPetroleoView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.periodo  == null)
            {
                model.periodo = new selectListCustom();
                model.periodo.select = 1;
            }
            string periodoActual = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string periodoPosterioro = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
            if (db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                         new List<Object>{ 
                   new { value = "1" , text = DateTime.Now.Month.ToString()+"-"+DateTime.Now.Year.ToString()  }},
                    "value",
                    "text");
                model.periodo.items = new SelectList(periodos, "value", "text", model.periodo.items);
            }
            int mesPosterior = DateTime.Now.AddMonths(1).Month;
            int añoAnterior = DateTime.Now.AddMonths(1).Year;
            int id_unidad = Convert.ToInt32(Session["unidad_seleccionada"]);
            if (db.petroleo.Where(u => u.año == añoAnterior && u.mes == mesPosterior && u.id_unidad_negocio == id_unidad).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                            new List<Object>{ 
                   new { value = "2" , text = DateTime.Now.AddMonths(1).Month.ToString()+"-"+DateTime.Now.AddMonths(1).Year.ToString() }},
                       "value",
                       "text");
                model.periodo.items = new SelectList(periodos, "value", "text", model.periodo.items);

            }
            if (db.petroleo.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month && u.id_unidad_negocio == id_unidad).FirstOrDefault() == null && db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                         new List<Object>{ 
                   new { value = "1" , text = DateTime.Now.Month.ToString()+"-"+DateTime.Now.Year.ToString()  },
                   new { value = "2" , text = DateTime.Now.AddMonths(1).Month.ToString()+"-"+DateTime.Now.AddMonths(1).Year.ToString() }},
                    "value",
                    "text");
                model.periodo.items = new SelectList(periodos, "value", "text", model.periodo.items);
            }

        }

    }
}
