﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class ComunasController : Controller
    {
        //
        // GET: /Comunas/ 

        public ActionResult Index()
        {
            ComunasRepository rs = new ComunasRepository(new sgsbdEntities());
            List<comunas> data = (List<comunas>)rs.GetComunas();
            return View(data.OrderBy(m=>m.id_comuna));
        }

        public ActionResult excel()
        {

            ComunasRepository rs = new ComunasRepository(new sgsbdEntities());
            List<comunas> data = (List<comunas>)rs.GetComunas();
            return View(data.OrderBy(m => m.id_comuna) );
        }
        public ActionResult create()
        {
            this.buildViewBagList();
            return View();
        }

        [HttpPost]
        public ActionResult create(comunas model)
        {

            if (ModelState.IsValid)
            {

                ComunasRepository rs = new ComunasRepository(new sgsbdEntities());

                if (model.id_comuna == 0)
                {
                    rs.InsertComuna(model);
                }
                else
                {
                    rs.UpdateComuna(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Comunas");

            }

            this.buildViewBagList();
            return View(model);
        }

        public ActionResult edit(int id)
        {

            ComunasRepository rs = new ComunasRepository(new sgsbdEntities());
            comunas c = rs.GetComunaByID(id);
            this.buildViewBagList();
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            ComunasRepository rs = new ComunasRepository(new sgsbdEntities());
            rs.DeleteComuna(id);
            rs.Save();
            return this.RedirectToAction("Index", "Comunas");

        }


        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();
            CiudadesRepository rs = new CiudadesRepository(ctx);

            ViewBag.ciudades = new SelectList(rs.GetCiudades().OrderBy(m=>m.ciudad),"id_ciudad","ciudad");

        }

    }
}
