﻿using SGS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Controllers
{
    public class InformeVertederosController : Controller
    {
        //
        // GET: /InformeVertederos/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult buscar(String desde, String hasta)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_informe_vertederos_Result> lstListaVertederos = db.SP_SEL_informe_vertederos(inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View("result", lstListaVertederos);
        }


        public ActionResult excel(String desde, String hasta)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_informe_vertederos_Result> lstListaVertederos = db.SP_SEL_informe_vertederos(inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            ViewBag.desde = desde;
            ViewBag.hasta = hasta;

            return View(lstListaVertederos);

        }

    }
}
