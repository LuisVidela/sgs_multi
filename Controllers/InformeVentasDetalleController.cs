﻿using SGS.Models;
using SGS.Models.repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.modelViews;


namespace SGS.Controllers
{
    public class InformeVentasDetalleController : Controller
    {
        //
        // GET: /InformeVentasDetalle/

        public ActionResult Index()
        {
            this.buildViewBagList();
            return View();
        }

        public ActionResult buscar(String desde, String hasta, Int32 cliente, Int32 sucursal)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            Int32 cli = Convert.ToInt32(cliente);
            Int32 sucu = Convert.ToInt32(sucursal);
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_informe_ventas_detalle1_Result> lstListaVentaDetalle = db.SP_SEL_informe_ventas_detalle1(cli, sucu, inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View("result", lstListaVentaDetalle);
        }

        public ActionResult excel(String desde, String hasta, Int32 cliente, Int32 sucursal)
        {
            DateTime inicio = Convert.ToDateTime(desde);
            DateTime fin = Convert.ToDateTime(hasta);
            Int32 cli = Convert.ToInt32(cliente);
            Int32 sucu = Convert.ToInt32(sucursal);
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_informe_ventas_detalle1_Result> lstListaVentaDetalle = db.SP_SEL_informe_ventas_detalle1(cli, sucu, inicio, fin, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            ViewBag.desde = desde;
            ViewBag.hasta = hasta;

            return View(lstListaVentaDetalle);

        }

        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            ClientesRepository rs1 = new ClientesRepository(ctx);
            SucursalesRepository rs2 = new SucursalesRepository(ctx);
            

            IEnumerable<object> sucursales = rs2.GetSucursalesForSelectByUnidad(Models.repository.UsuariosRepository.getUnidadSeleccionada());


            IEnumerable<object> clientes = rs1.GetClientesForSelectByUnidad(Models.repository.UsuariosRepository.getUnidadSeleccionada());
            
            
            //IEnumerable<object> franquiciados = rs_franquiciados.GetFranquiciadoForSelect();

            ViewBag.clientes = new SelectList(clientes, "Value", "Text", "0");
            ViewBag.sucursales = new SelectList(sucursales, "id_sucursal", "sucursal");
           
            
           
        }
    }
}
