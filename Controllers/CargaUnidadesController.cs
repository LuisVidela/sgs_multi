﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LinqToExcel;
using SGS.Models;
using System.Globalization;


namespace SGS.Controllers
{
    public class CargaUnidadesController : Controller
    {
        //
        // GET: /CargaUnidades/

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult CargaArchivo()
        {
            HttpPostedFileBase archivo = Request.Files.Get("xls");
            string[] nombres = Request.Files.AllKeys;
            string nombre = DateTime.Now.ToString("his") + "_" + archivo.FileName;
            string[] _ext = nombre.Split('.');
            string extension = _ext[_ext.Length - 1];
            string serverPath = Server.MapPath("~/Files/unidades/" + nombre);
            if (extension != "xls" && extension != "xlsx")
            {

                return Content("EXT_FAIL");
            }

            try
            {
                archivo.SaveAs(serverPath);
                var excel = new ExcelQueryFactory(serverPath);
                List<string> sheets = (List<string>)excel.GetWorksheetNames();
                Session["archivo_xls"] = nombre;
                return Json(sheets);
            }
            catch (Exception ex)
            {

                return Content("ERROR_SAVE");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="unidad"></param>
        /// <param name="hoja"></param>
        /// <returns></returns>
        public ActionResult Procesar(int unidad, string hoja)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            string nombre = (string)Session["archivo_xls"];
            var serverPath = Server.MapPath("~/Files/unidades/" + nombre);
            var excel = new ExcelQueryFactory(serverPath);
            var query = (from c in excel.Worksheet(hoja)
                         select new ExcelCarga()
                         {
                             numero_contrato = ((string)c["N° Contrato HDR"]).Trim(),
                             razon_social = ((string)c["Razón Social"]).Trim(),
                             rut = ((string)c["Rut"]).Trim(),
                             nombre_cliente = ((string)c["Nombre del Cliente - Servicio"]).Trim(),
                             sucursal_concatenar = ((string)c["Sucursal para concatenar"]).Trim(),
                             sucursal_oficial = ((string)c["Sucursal Oficial"]).Trim(),
                             unidad = ((string)c["Unidad"]).Trim(),
                             centro_responsabilidad = ((string)c["Centro de Responsabilidad"]).Trim(),
                             reajuste = ((string)c["Reajuste"]).Trim(),
                             tipo_industria = ((string)c["Tipo Industria"]).Trim(),
                             tipo_cobro = ((string)c["Tipo Cobro"]).Trim(),
                             sistema = ((string)c["Sistema"]).Trim(),
                             ruta = ((string)c["Ruta"]).Trim(),
                             numero_equipos = ((string)c["N° Equipos"]).Trim(),
                             nombre_equipo = ((string)c["Nombre Equipo"]).Trim(),
                             tipo_equipo = ((string)c["Tipo Equipo"]).Trim(),
                             capacidad = Convert.ToDouble(c["Capacidad"]),
                             vertedero = ((string)c["Vertedero"]).Trim(),
                             valor_fijo_mensual = ((string)c["Valor Fijo Mensual"]).Trim(),
                             valor_retiro = ((string)c["Valor Retiro o Volteo"]).Trim(),
                             arriendo = ((string)c["Arriendo"]).Trim(),
                             valor_m3 = ((string)c["Valor m3 / Kg"]).Trim(),
                             valor_rampa = ((string)c["Valor Rampa"]).Trim(),
                             tramo = ((string)c["Tramo"]).Trim(),
                             fecha_facturacion = ((string)c["Fecha Facturación"]).Trim(),
                             valor_pago_desde2_vuelta = ((string)c["Valor Pago Desde 2° Vuelta"]).Trim(),
                             valor_maximo_kg = ((string)c["Valor Máximo Kg"]).Trim(),
                             valor_minimo_kg = ((string)c["Valor Mínimo Kg"]).Trim(),
                             Tercero = ((string)c["Tercero"]).Trim(),
                             valor_lifter = ((string)c["Valor Lifter"]).Trim(),
                             valor_eliminador_olor = ((string)c["Valor Eliminador Olor"]).Trim(),
                             valor_retiro_carro = ((string)c["Valor Retiro con carro"]).Trim(),
                         }
                             ).ToList();

            var rubrosx = query.Where(x => !ctx.rubros.Select(r => r.rubro).ToList().Contains(x.tipo_industria) && x.tipo_industria != "").GroupBy(x => x.tipo_industria).Select(x => x.Key).ToList();
            var sistemasx = query.Where(x => !ctx.tipo_servicio.Select(r => r.tipo_servicio1).ToList().Contains(x.sistema) && x.sistema != "").GroupBy(x => x.sistema).Select(x => x.Key).ToList();
            var vertederosx = query.Where(x => !ctx.vertederos.Select(r => r.vertedero).ToList().Contains(x.vertedero) && x.vertedero != "").GroupBy(x => x.vertedero).Select(x => x.Key).ToList();
            var ciudadesx = query.Where(x => !ctx.ciudades.Select(r => r.ciudad).ToList().Contains(x.unidad) && x.vertedero != "").GroupBy(x => x.unidad).Select(x => x.Key).ToList();
            var tipo_contratosx = query.Where(x => !ctx.tipo_contrato.Select(r => r.tipo_contrato1).ToList().Contains(x.sistema) && x.sistema != "").GroupBy(x => x.sistema).Select(x => x.Key).ToList();
            var equiposx = query.Where(x => x.capacidad != 0 || x.tipo_equipo != "" || x.nombre_equipo != "" || x.numero_equipos != "").GroupBy(x => new { x.nombre_equipo, x.tipo_equipo, x.capacidad }).ToList();
            var centro_responsabilidadx = query.Where(x => !ctx.centro_responsabilidad.Select(c => c.centro_responsabilidad1).ToList().Contains(x.centro_responsabilidad) && x.centro_responsabilidad != "").GroupBy(x => x.centro_responsabilidad).Select(x => x.Key).ToList();

            foreach (var r in rubrosx)
            {
                rubros _rubro = new rubros();
                _rubro.rubro = r;
                ctx.rubros.Add(_rubro);
            }

            foreach (var r in equiposx)
            {
                equipos _eq = new equipos();
                if (r.Key.nombre_equipo.Length > 14) { _eq.nom_nemo = r.Key.nombre_equipo.Substring(0, 14); } else { _eq.nom_nemo = r.Key.nombre_equipo; }
                if (r.Key.capacidad > 0) { _eq.capacidad_equipo = r.Key.capacidad; } else { _eq.capacidad_equipo = 0; }
                if (r.Key.tipo_equipo != "") { _eq.tipo_equipo = r.Key.tipo_equipo; } else { _eq.tipo_equipo = "S/A"; }
                ctx.equipos.Add(_eq);
            }

            foreach (var r in tipo_contratosx)
            {
                tipo_contrato _tipo_contrato = new tipo_contrato();
                _tipo_contrato.tipo_contrato1 = r;
                ctx.tipo_contrato.Add(_tipo_contrato);
            }


            foreach (var s in sistemasx)
            {

                tipo_servicio _tipo_servicio = new tipo_servicio();
                _tipo_servicio.tipo_servicio1 = s;
                ctx.tipo_servicio.Add(_tipo_servicio);
            }

            foreach (var cr in centro_responsabilidadx)
            {

                centro_responsabilidad c = new centro_responsabilidad();
                c.centro_responsabilidad1 = cr;
                ctx.centro_responsabilidad.Add(c);
            }


            foreach (var c in ciudadesx)
            {
                ciudades _ciudad = new ciudades();
                _ciudad.ciudad = c;
                //pedir este campo a clientes
                _ciudad.id_region = 1;
                ////////////////////////////////
                ctx.ciudades.Add(_ciudad);

            }

            foreach (var v in vertederosx)
            {

                vertederos _vertedero = new vertederos();
                _vertedero.vertedero = v;
                //pedir campos a los clientes
                _vertedero.direccion_vertedero = "S/A";
                _vertedero.id_pais = 3;
                _vertedero.id_region = 13;
                _vertedero.id_ciudad = 47;
                _vertedero.id_region = 13;
                /////////////////////////////////////
                _vertedero.id_unidad = unidad;
                ctx.vertederos.Add(_vertedero);
            }
            ctx.SaveChanges();



            List<idx_nombre> rubros = ctx.rubros.OrderBy(x => x.rubro).Select(x => new idx_nombre() { id = x.id_rubro, nombre = x.rubro }).ToList();
            List<idx_nombre> sistemas = ctx.tipo_servicio.OrderBy(x => x.tipo_servicio1).Select(x => new idx_nombre() { id = x.id_tipo_servicio, nombre = x.tipo_servicio1 }).ToList();
            List<idx_nombre> vertederos = ctx.vertederos.OrderBy(x => x.vertedero).Select(x => new idx_nombre() { id = x.id_vertedero, nombre = x.vertedero }).ToList();
            List<idx_nombre> ciudades = ctx.ciudades.OrderBy(x => x.ciudad).Select(x => new idx_nombre() { id = x.id_ciudad, nombre = x.ciudad }).ToList();
            List<idx_nombre> tipos_contratos = ctx.tipo_contrato.OrderBy(x => x.tipo_contrato1).Select(x => new idx_nombre() { id = x.id_tipo_contrato, nombre = x.tipo_contrato1 }).ToList();
            List<equipos> equipos = ctx.equipos.OrderBy(x => x.nom_nemo).ToList();
            List<centro_responsabilidad> centro_responsabilidad = ctx.centro_responsabilidad.OrderBy(x => x.centro_responsabilidad1).ToList();

            var group_clientes = query.Where(x => !ctx.clientes.Select(c => new { razon_social = c.razon_social, rut = c.rut_cliente }).ToList().Contains(new { razon_social = x.razon_social, rut = x.rut }) && x.razon_social != "" && x.rut != "").GroupBy(x => new { x.razon_social, x.rut }).ToList();

            int idxx_residuo = ctx.residuos.Min(x => x.id_residuo);
            int idxx_tipo_tarifa = ctx.tipo_tarifa.Min(x => x.id_tipo_tarifa);
            var _min_c = ctx.contratos.Min(x => x.id_tipo_contrato);

            int indice_clientes = ctx.clientes.Max(x => x.id_cliente);
            int indice_sucursal = ctx.sucursales.Max(x => x.id_sucursal);
            int indice_contrato = ctx.contratos.Max(x => x.id_contrato);
            int indice_puntos_servicio = ctx.puntos_servicio.Max(x => x.id_punto_servicio);
            int indice_modelo_cobro = ctx.modelo_cobro.Max(x => x.id_modelo);
            int indice_save = 0;

            foreach (var gc in group_clientes)
            {


                indice_clientes++;
                clientes cliente = new clientes();
                cliente.id_cliente = indice_clientes;
                if (gc.Key.rut.Equals("")) { cliente.rut_cliente = "S/A"; } else { cliente.rut_cliente = removePuntos(gc.Key.rut); }
                if (gc.Key.razon_social.Equals("")) { cliente.razon_social = "S/A"; } else { cliente.razon_social = gc.Key.razon_social; };
                cliente.id_centro_costo = 1;
                cliente.id_unidad = unidad;
                ctx.clientes.Add(cliente);
                indice_save++;

                var cliente_sucursal = gc.GroupBy(x => new { x.sucursal_oficial, x.tipo_industria });
                List<sucursal_contrato> sc2 = new List<sucursal_contrato>();

                foreach (var _c in cliente_sucursal)
                {

                    indice_sucursal++;
                    sucursales _s1 = new sucursales();
                    _s1.id_sucursal = indice_sucursal;
                    if (_c.Key.sucursal_oficial == "") { _s1.sucursal = "S/A"; } else { _s1.sucursal = _c.Key.sucursal_oficial; }
                    _s1.descripcion = _s1.sucursal;
                    _s1.id_cliente = cliente.id_cliente;
                    _s1.id_pais = 3;
                    _s1.estado_sucursal = "Activa";
                    _s1.id_rubro = rubros.Where(x => x.nombre == _c.Key.tipo_industria).Select(x => x.id).FirstOrDefault();
                    ctx.sucursales.Add(_s1);

                    var _sis = _c.Select(y => y.sistema).FirstOrDefault();
                    var _tcx = tipos_contratos.Where(x => x.nombre == _sis).Select(x => new idx_nombre() { id = x.id, nombre = x.nombre });

                    indice_contrato++;
                    contratos _cnt = new contratos();
                    _cnt.id_contrato = indice_contrato;
                    if (_tcx.Count() == 0)
                    {
                        _cnt.id_tipo_contrato = _min_c;
                    }
                    else
                    {
                        _cnt.id_tipo_contrato = _tcx.Select(x => x.id).First();
                    }
                    _cnt.id_unidad = unidad;
                    _cnt.id_sucursal = _s1.id_sucursal;
                    _cnt.estado = "Activo";
                    _cnt.id_cliente = cliente.id_cliente;
                    _cnt.fecha_inicio = DateTime.Now;
                    _cnt.fecha_renovacion = DateTime.Now;
                    ctx.contratos.Add(_cnt);

                    sc2.Add(new sucursal_contrato()
                    {
                        id_contrato = _cnt.id_contrato,
                        id_sucursal = _s1.id_sucursal,
                        nombre_sucursal = _s1.sucursal,
                        tipo_industria = _c.Key.tipo_industria
                    });
                }


                foreach (var xx in gc)
                {
                    indice_puntos_servicio++;
                    if (xx.nombre_equipo == "") { xx.nombre_equipo = null; }
                    if (xx.capacidad == 0) { xx.capacidad = 0; }
                    if (xx.tipo_equipo == "") { xx.tipo_equipo = "S/A"; }

                    var _equipo = equipos.Where(x => x.nom_nemo == xx.nombre_equipo && x.tipo_equipo == xx.tipo_equipo && x.capacidad_equipo == xx.capacidad).Select(x => x.id_equipo);

                    puntos_servicio ps1 = new puntos_servicio();
                    ps1.id_punto_servicio = indice_puntos_servicio;
                    ps1.nombre_punto = xx.nombre_cliente;
                    ps1.id_contrato = sc2.Where(x => x.tipo_industria == xx.tipo_industria && x.nombre_sucursal == xx.sucursal_oficial).Select(x => x.id_contrato).First();
                    ps1.id_vertedero = vertederos.Where(x => x.nombre == xx.vertedero).Select(x => x.id).FirstOrDefault();
                    if (_equipo.Count() != 0) { ps1.id_equipo = _equipo.First(); }
                    ps1.id_residuo = idxx_residuo;
                    ps1.id_tipo_tarifa = idxx_tipo_tarifa;
                    ps1.lunes = false;
                    ps1.martes = false;
                    ps1.miercoles = false;
                    ps1.jueves = false;
                    ps1.viernes = false;
                    ps1.sabado = false;
                    ps1.domingo = false;
                    if (xx.numero_equipos != "") { ps1.cant_equipos = int.Parse(xx.numero_equipos); }
                    if (xx.valor_m3 == "" || xx.valor_m3 == "0") { ps1.pago_por_peso = false; } else { ps1.pago_por_peso = true; }
                    ps1.codigo_externo_asignado = false;
                    ps1.estado_punto = 1;
                    ctx.puntos_servicio.Add(ps1);


                    indice_modelo_cobro++;
                    modelo_cobro mc = new modelo_cobro();
                    mc.id_modelo = indice_modelo_cobro;
                    mc.modelo = ps1.nombre_punto;
                    mc.id_tipo_modelo = 1;
                    mc.id_punto_servicio = ps1.id_punto_servicio;
                    ctx.modelo_cobro.Add(mc);



                    if (xx.valor_fijo_mensual != "" && xx.valor_fijo_mensual != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 5;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_fijo_mensual), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_retiro != "" && xx.valor_retiro != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 6;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_retiro), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.arriendo != "" && xx.arriendo != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 7;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.arriendo), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_m3 != "" && xx.valor_m3 != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 13;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_m3), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_rampa != "" && xx.valor_rampa != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 12;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_rampa), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_lifter != "" && xx.valor_lifter != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 11;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_lifter), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_eliminador_olor != "" && xx.valor_eliminador_olor != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 10;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_eliminador_olor), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }

                    if (xx.valor_retiro_carro != "" && xx.valor_retiro_carro != "0")
                    {
                        modelo_cobro_detalle mcd = new modelo_cobro_detalle();
                        mcd.id_tipo_modalidad = 14;
                        if (xx.valor_maximo_kg != "" && xx.valor_maximo_kg != "0") { mcd.rango_maximo = int.Parse(xx.valor_maximo_kg); }
                        if (xx.valor_minimo_kg != "" && xx.valor_minimo_kg != "0") { mcd.rango_minimo = int.Parse(xx.valor_minimo_kg); }
                        mcd.valor = float.Parse(removeComasPuntos(xx.valor_retiro_carro), CultureInfo.InvariantCulture);
                        mcd.id_modelo_cobro = mc.id_modelo;
                        ctx.modelo_cobro_detalle.Add(mcd);

                    }



                }

                if (indice_save == 10)
                {

                    ctx.SaveChanges();
                    indice_save = 0;

                }

            }

            ctx.SaveChanges();
            return Content("SUCCESS");

        }
        public ActionResult Create()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            ViewBag.unidades = new SelectList((from u in ctx.unidades_negocio select new { Text = u.unidad, Value = u.id_unidad }).ToList(), "Value", "Text");
            return View();
        }





        private string removePuntos(string cadena)
        {

            string cadena_final = "";

            foreach (char s in cadena)
            {

                if (s.Equals('.'))
                {

                    continue;
                }

                cadena_final += s;
            }

            return cadena_final;
        }

        private string removeComasPuntos(string cadena)
        {

            string cadena_final = "";

            foreach (char s in cadena)
            {

                if (s.Equals(','))
                {
                    cadena_final += '.';
                    continue;
                }

                cadena_final += s;
            }

            return cadena_final;
        }




    }


    class sucursal_contrato
    {

        public int id_sucursal { get; set; }
        public int id_contrato { get; set; }
        public string nombre_sucursal { get; set; }
        public string tipo_industria { get; set; }


    }

    class ExcelCarga
    {

        public string numero_contrato { get; set; }
        public string razon_social { get; set; }
        public string avenida_calle_pasaje { get; set; }
        public string numero { get; set; }
        public string comuna { get; set; }
        public string region { get; set; }
        public string rut { get; set; }
        public string nombre_cliente { get; set; }
        public string sucursal_concatenar { get; set; }
        public string sucursal_oficial { get; set; }
        public string unidad { get; set; }
        public string centro_responsabilidad { get; set; }
        public string reajuste { get; set; }
        public string tipo_industria { get; set; }
        public string tipo_cobro { get; set; }
        public string sistema { get; set; }
        public string ruta { get; set; }
        public string numero_equipos { get; set; }
        public string nombre_equipo { get; set; }
        public string tipo_equipo { get; set; }
        public double capacidad { get; set; }
        public string vertedero { get; set; }
        public string valor_fijo_mensual { get; set; }
        public string valor_retiro { get; set; }
        public string arriendo { get; set; }
        public string valor_m3 { get; set; }
        public string valor_rampa { get; set; }
        public string tramo { get; set; }
        public string valor_pago_franquiciado { get; set; }
        public string fecha_facturacion { get; set; }
        public string valor_pago_desde2_vuelta { get; set; }
        public string valor_maximo_kg { get; set; }
        public string valor_minimo_kg { get; set; }
        public string Tercero { get; set; }
        public string valor_lifter { get; set; }
        public string valor_eliminador_olor { get; set; }
        public string valor_retiro_carro { get; set; }


    }


    class idx_nombre
    {

        public int id { get; set; }
        public string nombre { get; set; }

    }
}
