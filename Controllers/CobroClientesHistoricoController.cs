﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
//using RazorPDF;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.factories;
using Bytescout.PDFExtractor;


namespace SGS.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    public class CobroClientesHistoricoController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="table"></param>
        /// <param name="cellText"></param>
        private static void AddTableCell(PdfPTable table, string cellText)
        {
            Paragraph para770 = new Paragraph(cellText, FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell770 = new PdfPCell(para770)
            {
                HorizontalAlignment = 1
            };
            table.AddCell(theCell770);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="rs"></param>
        /// <returns></returns>
        private static PdfPCell GetClientReportData(int idCliente, CobroClientesHistoricoRepository rs)
        {
            string razonSocial = "";
            string rut = "";
            List<ClientesView> clientesViews = (List<ClientesView>)rs.getClienteData(idCliente);
            foreach (var clienteView in clientesViews)
            {
                razonSocial = clienteView.razon_social;
                rut = clienteView.rut_cliente;
            }

            string cliente = "Cliente: " + razonSocial.Trim() + " Rut: " + rut.Trim();
            Paragraph paragraph = new Paragraph(cliente, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell cell = new PdfPCell(paragraph)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            return cell;
        }

        //
        // GET: /CobroClientes/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            ViewBag.fechaHide = "000000";

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            items.Add(new SelectListItem { Text = "Enero", Value = "01" });
            items.Add(new SelectListItem { Text = "Febrero", Value = "02" });
            items.Add(new SelectListItem { Text = "Marzo", Value = "03" });
            items.Add(new SelectListItem { Text = "Abril", Value = "04" });
            items.Add(new SelectListItem { Text = "Mayo", Value = "05" });
            items.Add(new SelectListItem { Text = "Junio", Value = "06" });
            items.Add(new SelectListItem { Text = "Julio", Value = "07" });
            items.Add(new SelectListItem { Text = "Agosto", Value = "08" });
            items.Add(new SelectListItem { Text = "Septiembre", Value = "09" });
            items.Add(new SelectListItem { Text = "Octubre", Value = "10" });
            items.Add(new SelectListItem { Text = "Noviembre", Value = "11" });
            items.Add(new SelectListItem { Text = "Diciembre", Value = "12" });
            //Assign the value to ViewBag            
            ViewBag.Months = items;

            //Populate year data in controller
            //List<SelectListItem> drodwnitems = new List<SelectListItem>();
            //drodwnitems.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            //drodwnitems.Add(new SelectListItem { Text = "2014", Value = "1" });
            //drodwnitems.Add(new SelectListItem { Text = "2015", Value = "2" });
            //drodwnitems.Add(new SelectListItem { Text = "2016", Value = "3" });
            //drodwnitems.Add(new SelectListItem { Text = "2017", Value = "4" });
            //drodwnitems.Add(new SelectListItem { Text = "2018", Value = "5" });
            ////Assign the value to ViewBag
            //ViewBag.Year = drodwnitems;

            List<SelectListItem> drodwnitems = new List<SelectListItem>();
            using (var ctx = new sgsbdEntities())
            {
                var cobro = (from o in ctx.cobro_clientes_cerrado
                             where o.Yearis.Value <= DateTime.Now.Year + 1
                             select o.Yearis.Value).Distinct().ToList();
                var i = 0;
                foreach (int anios in cobro)
                {
                    i++;
                    drodwnitems.Add(new SelectListItem { Text = anios.ToString(), Value = i.ToString() });
                }

            }
            ViewBag.Year = drodwnitems;


            List<SelectListItem> OpenClose = new List<SelectListItem>();
            OpenClose.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            OpenClose.Add(new SelectListItem { Text = "Abierto", Value = "1" });
            OpenClose.Add(new SelectListItem { Text = "Cerrado", Value = "2" });
            ViewBag.Open = OpenClose;

            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            List<CobroClientesView> dot = new List<CobroClientesView>();//)rs.getCobroClientes("000000");

            return View(dot);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult index(FormCollection collection)
        {
            string fechaHideIs = collection.Get("fechaHide");
            string exito_o_fracaso = "-9";

            ViewBag.PeriodoIs = fechaHideIs;

            CobroClientesHistoricoRepository rs = new CobroClientesHistoricoRepository(new sgsbdEntities());

            ViewBag.fechaHide = fechaHideIs;

            string fechaCbx = fechaHideIs;

            string secalculaelcierre = collection.Get("Select");

            string secierra = "";
            string secierracalculo = "";
            ViewBag.hdnFlag = "SI";

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            items.Add(new SelectListItem { Text = "Enero", Value = "01" });
            items.Add(new SelectListItem { Text = "Febrero", Value = "02" });
            items.Add(new SelectListItem { Text = "Marzo", Value = "03" });
            items.Add(new SelectListItem { Text = "Abril", Value = "04" });
            items.Add(new SelectListItem { Text = "Mayo", Value = "05" });
            items.Add(new SelectListItem { Text = "Junio", Value = "06" });
            items.Add(new SelectListItem { Text = "Julio", Value = "07" });
            items.Add(new SelectListItem { Text = "Agosto", Value = "08" });
            items.Add(new SelectListItem { Text = "Septiembre", Value = "09" });
            items.Add(new SelectListItem { Text = "Octubre", Value = "10" });
            items.Add(new SelectListItem { Text = "Noviembre", Value = "11" });
            items.Add(new SelectListItem { Text = "Diciembre", Value = "12" });
            //Assign the value to ViewBag         

            string mesval = fechaCbx.Substring(0, 2);

            //Populate year data in controller
          List<SelectListItem> drodwnitems = new List<SelectListItem>();
          using (var ctx = new sgsbdEntities())
          {
              var cobro = (from o in ctx.cobro_clientes_cerrado
                           where o.Yearis.Value <= DateTime.Now.Year + 1
                           select o.Yearis.Value).Distinct().ToList();
              var i = 0;
              foreach (int anios in cobro)
              {
                  i++;
                  drodwnitems.Add(new SelectListItem { Text = anios.ToString(), Value = i.ToString() });
              }

          }

           //List<SelectListItem> drodwnitems = new List<SelectListItem>();
           //drodwnitems.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
           //drodwnitems.Add(new SelectListItem { Text = "2014", Value = "1" });
           //drodwnitems.Add(new SelectListItem { Text = "2015", Value = "2" });
           //drodwnitems.Add(new SelectListItem { Text = "2016", Value = "3" });
           //drodwnitems.Add(new SelectListItem { Text = "2017", Value = "4" });
           //drodwnitems.Add(new SelectListItem { Text = "2018", Value = "5" });

            string yearwhen = collection.Get("YearVal");

            ViewBag.Months = new SelectList(items, "Value", "Text", mesval);
            ViewBag.Year = new SelectList(drodwnitems, "Value", "Text", yearwhen);
            List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientesCerrado(fechaHideIs, SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            if (dot.Count == 0)
            {
                ViewBag.PeriodoIs = null;
            }
            return View(dot);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenIs"></param>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public ActionResult excelClientes(string whenIs, int idcliente)
        {
            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            int Cerrado = rs.getProcesoCerrado(whenIs);

            if (String.IsNullOrEmpty(Cerrado.ToString()))
            {
                List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientesExcel(whenIs, idcliente);
                return View(dot);
            }
            else
            {
                if (Cerrado == 1)
                {
                    List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientesCerradoExcel(whenIs, idcliente);
                    return View(dot);
                }
                else
                {
                    List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientesExcel(whenIs, idcliente);
                    return View(dot);
                }

            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public ActionResult excelClientesFull(string whenIs)
        {
            ViewBag.TitleIs = "Clientes_Cobro_Periodo" + whenIs + ".xls";

            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            int Cerrado = rs.getProcesoCerrado(whenIs);

            if (String.IsNullOrEmpty(Cerrado.ToString()))
            {
                List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientes(whenIs);
                return View(dot);
            }
            else
            {
                if (Cerrado == 1)
                {
                    List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientesCerrado(whenIs);
                    return View(dot);
                }
                else
                {
                    List<CobroClientesView> dot = (List<CobroClientesView>)rs.getCobroClientes(whenIs);
                    return View(dot);
                }

            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public ActionResult excelContrato(string rutcliente, string whenIs)
        {
            ViewBag.TitleIs = "Clientes_Cobro_Contrato_Periodo" + whenIs + ".xls";

            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            List<CobroClientesContratoView> dot = (List<CobroClientesContratoView>)rs.getCobroContratoCerrado(rutcliente, whenIs);
            return View(dot);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public ActionResult excelPuntos(string rutcliente, string whenIs)
        {
            ViewBag.TitleIs = "Clientes_Cobro_Puntos_Periodo" + whenIs + ".xls";

            CobroClientesHistoricoRepository rs = new CobroClientesHistoricoRepository(new sgsbdEntities());
            List<CobroPuntosView> dot = (List<CobroPuntosView>)rs.getCalculoPuntosCerrado(rutcliente, whenIs);
            return View(dot);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public ActionResult excelPuntosxls(string rutcliente, string whenIs)
        {
            ViewBag.TitleIs = "Clientes_Cobro_Puntos_Periodo" + whenIs + ".xls";

            CobroClientesHistoricoRepository rs = new CobroClientesHistoricoRepository(new sgsbdEntities());

            int Cerrado = rs.getProcesoCerrado(whenIs);

            if (String.IsNullOrEmpty(Cerrado.ToString()))
            {
                List<CobroPuntosView> dot = (List<CobroPuntosView>)rs.getCalculoPuntos(rutcliente, whenIs);
                return View(dot);
            }
            else
            {
                if (Cerrado == 1)
                {
                    List<CobroPuntosView> dot = (List<CobroPuntosView>)rs.getCalculoPuntosCerrado(rutcliente, whenIs);
                    return View(dot);
                }
                else
                {
                    List<CobroPuntosView> dot = (List<CobroPuntosView>)rs.getCalculoPuntos(rutcliente, whenIs);
                    return View(dot);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public ActionResult excelPuntosOut(string rutcliente, string whenIs)
        {
            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            int Cerrado = rs.getProcesoCerrado(whenIs);

            if (String.IsNullOrEmpty(Cerrado.ToString()))
            {
                List<CobroClientesPuntosView> dot = (List<CobroClientesPuntosView>)rs.getCobroPuntos(rutcliente, whenIs);
                return View(dot);
            }
            else
            {
                if (Cerrado == 1)
                {
                    List<CobroClientesPuntosView> dot = (List<CobroClientesPuntosView>)rs.getCobroPuntosCerrado(rutcliente, whenIs);
                    return View(dot);
                }
                else
                {
                    List<CobroClientesPuntosView> dot = (List<CobroClientesPuntosView>)rs.getCobroPuntos(rutcliente, whenIs);
                    return View(dot);
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="Select"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult pdfCheckBox(string[] Select)
        {
            string NAME_FILE;
            string RazonSocial = "";
            string Rut = "";
            decimal valor = 0;
            decimal iva = 0;
            int NumeroServicios = 0;
            int id_punto_servicio = 0;
            int idpuntoservicio = 0;
            string zona = "", ciudad = "", nombre_comuna = "", rubro = "";
            string descrip = "";
            string imagepath = Server.MapPath("Images");
            FileStreamResult result;
            MemoryStream stream;
            int EXISTE = 0;
            var message = "";

            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            //GENERANDO EL PDF

            if (Select != null && Select.Length > 0)
            {
                foreach (string s in Select)
                {
                    List<CobroClientesPuntosView> dot = null;

                    string[] parts;
                    parts = s.Split(new string[] { "X" }, StringSplitOptions.None);

                    int id_cliente = Convert.ToInt16(parts[0]);
                    id_punto_servicio = Convert.ToInt16(parts[1]);
                    string Fecha = (parts[2]);
                    RazonSocial = (parts[3]);
                    NAME_FILE = String.Concat("Cli", id_cliente, "Pto Serv", id_punto_servicio, "Perio", Fecha, ".pdf");
                    EXISTE = 1;

                    stream = new MemoryStream();

                    Document document = new Document(PageSize.A4.Rotate());
                    PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
                    pdfWriter.CloseStream = false;

                    // BUSCANDO LOS DATGOS DEL CLIENTE
                    List<ClientesView> cli = (List<ClientesView>)rs.getClienteData(id_cliente);
                    foreach (var Xvar in cli)
                    {
                        RazonSocial = Xvar.razon_social;
                        Rut = Xvar.rut_cliente;
                    }

                    int Cerrado = rs.getProcesoCerrado(Fecha);
                    if (String.IsNullOrEmpty(Cerrado.ToString()))
                    {
                        dot = (List<CobroClientesPuntosView>)rs.getCobroPdfPtoServ(id_cliente, Fecha, id_punto_servicio);
                    }
                    else
                    {
                        if (Cerrado == 1)
                        {
                            dot = (List<CobroClientesPuntosView>)rs.getCobroPdfCerradoPtoServ(id_cliente, Fecha, id_punto_servicio);
                        }
                        else
                        {
                            dot = (List<CobroClientesPuntosView>)rs.getCobroPdfPtoServ(id_cliente, Fecha, id_punto_servicio);
                        }
                    }

                    // TIPO DE SERTVICIO ASOCIADO AL PUNTO DE SERVICIO
                    Nullable<System.Int32> idtiposervicio = 0;
                    string servicio = "";
                    List<PuntoServicioDescView> tiposerv = (List<PuntoServicioDescView>)rs.getPuntoServicioDescripcion(id_punto_servicio);
                    foreach (var Xvar in tiposerv)
                    {
                        idtiposervicio = Xvar.id_tipo_servicio;
                        servicio = Xvar.tipo_servicio;
                    };

                    PdfPTable table = new PdfPTable(7);
                    float[] headers = { 15, 8, 10, 25, 15, 15, 12 };  //Header Widths
                    table.SetWidths(headers);        //Set the pdf headers
                    table.WidthPercentage = 80;       //Set the PDF File witdh percentage

                    Paragraph para770 = new Paragraph("Item", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell770 = new PdfPCell(para770);
                    theCell770.HorizontalAlignment = 1;
                    table.AddCell(theCell770);

                    Paragraph para771 = new Paragraph("Fecha", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell771 = new PdfPCell(para771);
                    theCell771.HorizontalAlignment = 1;
                    table.AddCell(theCell771);

                    Paragraph para772 = new Paragraph("Guía u Orden", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell772 = new PdfPCell(para772);
                    theCell772.HorizontalAlignment = 1;
                    table.AddCell(theCell772);

                    Paragraph para773 = new Paragraph("Contenedor", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell773 = new PdfPCell(para773);
                    theCell773.HorizontalAlignment = 1;
                    table.AddCell(theCell773);

                    Paragraph para774 = new Paragraph("Camión", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell774 = new PdfPCell(para774);
                    theCell774.HorizontalAlignment = 1;
                    table.AddCell(theCell774);

                    Paragraph para776 = new Paragraph("Chofer", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell776 = new PdfPCell(para776);
                    theCell776.HorizontalAlignment = 1;
                    table.AddCell(theCell776);

                    Paragraph para777 = new Paragraph("Valor", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell777 = new PdfPCell(para777);
                    theCell777.HorizontalAlignment = 1;
                    table.AddCell(theCell777);

                    // RECORRIENDO EL LIST RETORNADO POR EL METODO
                    foreach (var Item in dot)
                    {
                        string camion = "Sin datos";
                        string conductor = "Sin Datos";
                        string rut_conductor = "Sin Datos";

                        // con el campo id_hoja_ruta podemos obtener el camion y el chofer
                        int idhojaruta = Convert.ToInt16(Item.id_hoja_ruta);
                        List<ConductorCamionView> punto = (List<ConductorCamionView>)rs.getConductorCamion(idhojaruta);
                        foreach (var Xvar in punto)
                        {
                            camion = Xvar.camion;
                            conductor = Xvar.nombre_conductor;
                            rut_conductor = Xvar.rut_conductor;
                        }

                        if (Item.tipo_modalidad.ToString() != "Arriendo")
                        {
                            NumeroServicios = NumeroServicios + 1;
                        }

                        // CONSTRUCCION DE CADA LINEA DEL INFORME PDF
                        Paragraph para11 = new Paragraph(Item.tipo_modalidad.ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell = new PdfPCell(para11);
                        theCell.HorizontalAlignment = 1;
                        table.AddCell(theCell);

                        if (Item.glosa == "Retiro")
                        {
                            Paragraph para22 = new Paragraph(Item.fechaFull.ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                            PdfPCell theCell22 = new PdfPCell(para22);
                            theCell22.HorizontalAlignment = 1;
                            table.AddCell(theCell22);
                        }
                        else
                        {
                            Paragraph para22 = new Paragraph(Item.cobro_arriendo, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                            PdfPCell theCell22 = new PdfPCell(para22);
                            theCell22.HorizontalAlignment = 1;
                            table.AddCell(theCell22);
                        }

                        Paragraph para33 = new Paragraph(Item.Orden.ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell33 = new PdfPCell(para33);
                        theCell33.HorizontalAlignment = 1;
                        table.AddCell(theCell33);

                        string equipo_desc = "";
                        // BUSQUEDA TIPO DE EQUIPO
                        idpuntoservicio = id_punto_servicio;
                        List<EquipoView> equipo = (List<EquipoView>)rs.getEquipo(idpuntoservicio);
                        foreach (var Xvar in equipo)
                        {
                            equipo_desc = Xvar.Equipo;
                        }

                        Paragraph para44 = new Paragraph(equipo_desc, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell44 = new PdfPCell(para44);
                        theCell44.HorizontalAlignment = 1;
                        table.AddCell(theCell44);

                        Paragraph para55 = new Paragraph(camion.ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell55 = new PdfPCell(para55);
                        theCell55.HorizontalAlignment = 1;
                        table.AddCell(theCell55);

                        Paragraph para66 = new Paragraph(conductor.ToString(), FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell66 = new PdfPCell(para66);
                        theCell66.HorizontalAlignment = 1;
                        table.AddCell(theCell66);

                        string many = Item.Valor.Value.ToString("#,##0");
                        Paragraph para77 = new Paragraph(many, FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                        PdfPCell theCell77 = new PdfPCell(para77);
                        theCell77.HorizontalAlignment = 2;
                        table.AddCell(theCell77);

                        valor = valor + Item.Valor.Value;
                    }

                    document.SetMargins(20, 20, 20, 20);

                    document.Add(new Paragraph("\n"));

                    Image jpg = Image.GetInstance(imagepath.Replace("\\CobroClientes", "") + "\\resiterlogo.jpg");
                    document.Add(jpg);

                    Paragraph para = new Paragraph("Empresa de Residuos Resiter S.A.", FontFactory.GetFont("Arial", 14, iTextSharp.text.Font.BOLD, new iTextSharp.text.Color(0, 0, 0)));
                    para.Alignment = Element.ALIGN_CENTER;
                    para.Font.SetStyle(Font.UNDERLINE);
                    document.Add(para);

                    Paragraph para1 = new Paragraph("Manejo Integral de Residuos Sólidos - Transporte de Carga", FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    para1.Alignment = Element.ALIGN_CENTER;
                    document.Add(para1);

                    Paragraph para2 = new Paragraph("José Joaquín Prieto 9750", FontFactory.GetFont("Arial", 11, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    para2.Alignment = Element.ALIGN_CENTER;
                    document.Add(para2);

                    document.Add(new Paragraph("\n"));

                    Paragraph p = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new iTextSharp.text.Color(0, 0, 0), Element.ALIGN_LEFT, 1)));
                    document.Add(p);

                    // con el campo id_punto_servicio podemos obtener la zona sucursal region etc
                    List<PuntoServicioDireView> puntowhere = (List<PuntoServicioDireView>)rs.getPtoServicioDire(idpuntoservicio);
                    foreach (var Xvar in puntowhere)
                    {
                        zona = Xvar.zona;
                        ciudad = Xvar.ciudad;
                        nombre_comuna = Xvar.nombre_comuna;
                        rubro = Xvar.rubro;
                        descrip = string.Concat("Zona ", zona, " Ciudad ", ciudad, " Comuna ", nombre_comuna, " Rubro ", rubro);
                    }

                    string blancos = "";
                    int LenClie = Convert.ToInt32(descrip.Trim().Length);
                    blancos = "";
                    for (int i = 0; i < (100 - LenClie); i++)
                    {
                        blancos = blancos + " ";
                    }

                    Phrase zonawhere = new Phrase(descrip + blancos + "Fecha: " + DateTime.Today.ToString() + "\n", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    document.Add(zonawhere);

                    string cliente = RazonSocial + " Rut: " + Rut;
                    LenClie = Convert.ToInt32(cliente.Length) + 1;
                    blancos = "";
                    for (int i = 0; i < (99 - LenClie); i++)
                    {
                        blancos = blancos + " ";
                    }
                    int numero_servicios = 9;
                    Phrase lineacliente = new Phrase("Cliente: " + cliente + blancos + "N° Servicios: " + NumeroServicios.ToString() + "\n", FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    document.Add(lineacliente);

                    blancos = "";
                    for (int i = 0; i < (100); i++)
                    {
                        blancos = blancos + " ";
                    }
                    string tipo_servicios = servicio;
                    Phrase lineaservicio = new Phrase("Tipo de Servicios: " + tipo_servicios, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    document.Add(lineaservicio);

                    //*********************************************************

                    Paragraph p1 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new iTextSharp.text.Color(0, 0, 0), Element.ALIGN_LEFT, 1)));
                    document.Add(p1);

                    document.Add(new Paragraph("\n"));

                    // AÑADIENDO TABLA
                    document.Add(table);

                    document.Add(new Paragraph("\n"));

                    iva = ((valor * 19) / 100);
                    decimal total = (valor + iva);

                    // TABLA CON LOS TOTALES
                    PdfPTable table1 = new PdfPTable(7);
                    float[] headers1 = { 15, 8, 10, 25, 15, 15, 12 };  //Header Widths
                    table1.SetWidths(headers1);        //Set the pdf headers
                    table1.WidthPercentage = 100;       //Set the PDF File witdh percentage
                    table1.DefaultCell.Border = Rectangle.NO_BORDER;
                    table1.DefaultCell.BorderColor = new iTextSharp.text.Color(255, 255, 255);

                    Paragraph para11T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellT = new PdfPCell(para11T);
                    theCellT.HorizontalAlignment = 1;
                    theCellT.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellT);

                    Paragraph para22T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell22T = new PdfPCell(para22T);
                    theCell22T.HorizontalAlignment = 1;
                    theCell22T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell22T);

                    Paragraph para33T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell33T = new PdfPCell(para33T);
                    theCell33T.HorizontalAlignment = 0;
                    theCell33T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell33T);

                    Paragraph para44T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell44T = new PdfPCell(para44T);
                    theCell44T.HorizontalAlignment = 2;
                    theCell44T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell44T);

                    Paragraph para55T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell55T = new PdfPCell(para55T);
                    theCell55T.HorizontalAlignment = 1;
                    theCell55T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell55T);

                    Paragraph para66T = new Paragraph("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell66T = new PdfPCell(para66T);
                    theCell66T.HorizontalAlignment = 0;
                    theCell66T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell66T);

                    Paragraph para77T = new Paragraph(valor.ToString("#,##0"), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell77T = new PdfPCell(para77T);
                    theCell77T.HorizontalAlignment = 2;
                    theCell77T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell77T);

                    //****************************************************************************************
                    Paragraph para11x = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellx = new PdfPCell(para11x);
                    theCellx.HorizontalAlignment = 1;
                    theCellx.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellx);

                    Paragraph para22x = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell22x = new PdfPCell(para22x);
                    theCell22x.HorizontalAlignment = 1;
                    theCell22x.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell22x);

                    Paragraph para33x = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell33x = new PdfPCell(para33x);
                    theCell33x.HorizontalAlignment = 1;
                    theCell33x.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell33x);

                    Paragraph para44x = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell44x = new PdfPCell(para44x);
                    theCell44x.HorizontalAlignment = 1;
                    theCell44x.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell44x);

                    Paragraph para55x = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell55x = new PdfPCell(para55x);
                    theCell55x.HorizontalAlignment = 1;
                    theCell55x.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell55x);

                    Paragraph para66x = new Paragraph("IVA", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell66x = new PdfPCell(para66x);
                    theCell66x.HorizontalAlignment = 0;
                    theCell66x.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell66x);

                    Paragraph para77z = new Paragraph(iva.ToString("#,##0"), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCell77z = new PdfPCell(para77z);
                    theCell77z.HorizontalAlignment = 2;
                    theCell77z.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCell77z);

                    //****************************************************************************************

                    Paragraph para11W = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellW = new PdfPCell(para11W);
                    theCellW.HorizontalAlignment = 1;
                    theCellW.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellW);

                    Paragraph paraW2T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellW2T = new PdfPCell(paraW2T);
                    theCellW2T.HorizontalAlignment = 1;
                    theCellW2T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellW2T);

                    Paragraph paraW3T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellWT3 = new PdfPCell(paraW3T);
                    theCellWT3.HorizontalAlignment = 1;
                    theCellWT3.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellWT3);

                    Paragraph paraW4T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellW4T = new PdfPCell(paraW4T);
                    theCellW4T.HorizontalAlignment = 1;
                    theCellW4T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellW4T);

                    Paragraph paraW5T = new Paragraph("", FontFactory.GetFont("Arial", 8, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellW5T = new PdfPCell(paraW5T);
                    theCellW5T.HorizontalAlignment = 1;
                    theCellW5T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellW5T);

                    Paragraph paraW6T = new Paragraph("Total", FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellW6T = new PdfPCell(paraW6T);
                    theCellW6T.HorizontalAlignment = 0;
                    theCellW6T.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellW6T);

                    Paragraph paraWT = new Paragraph(total.ToString("#,##0"), FontFactory.GetFont("Arial", 9, iTextSharp.text.Font.NORMAL, new iTextSharp.text.Color(0, 0, 0)));
                    PdfPCell theCellWT = new PdfPCell(paraWT);
                    theCellWT.HorizontalAlignment = 2;
                    theCellWT.BorderColor = new iTextSharp.text.Color(255, 255, 255);
                    table1.AddCell(theCellWT);

                    //****************************************************************************************
                    document.Add(table1);

                    Paragraph p3 = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new iTextSharp.text.Color(0, 0, 0), Element.ALIGN_LEFT, 1)));
                    document.Add(p3);

                    document.Close();

                    stream.Flush();
                    stream.Position = 0;

                    result = new FileStreamResult(stream, "application/pdf");
                    result.FileDownloadName = NAME_FILE;
                    return result;
                }

                return new EmptyResult();
            }
            else
            {
                return new EmptyResult();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="id_punto_servicio"></param>
        /// <param name="Fecha"></param>
        /// <param name="id_contra"></param>
        /// <param name="id_sucu"></param>
        /// <returns></returns>
        public ActionResult pdfLineaPunto(int id_cliente, int id_punto_servicio, string Fecha, int id_contra, int id_sucu)
        {
            return GenerarInformeDetalladoPdf(id_cliente, id_punto_servicio, Fecha);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idPuntoServicio"></param>
        /// <param name="fecha"></param>
        /// <returns></returns>
        private ActionResult GenerarInformeDetalladoPdf(int idCliente, int idPuntoServicio, string fecha)
        {
            int numeroServicios = 0;
            int idpuntoservicio = 0;
            string zona = "", ciudad = "", nombre_comuna = "", rubro = "";
            Decimal DisposcionEs = 0;
            int existe_disposicion = 0;
            string periodoes = "";
            string Sucursal = "";
            string ValorTonelada = "";
            string NumeroEquipos = "";
            Double valor_tone = 0;

            CobroClientesHistoricoRepository rs = new CobroClientesHistoricoRepository(new sgsbdEntities());
            int Cerrado = 0;
            //GENERANDO EL PDF
            MemoryStream stream = new MemoryStream();
            Document document = new Document(PageSize.A4.Rotate());
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, stream);
            pdfWriter.CloseStream = false;

            // DETERMINACION DEL TIPO DE SERVICIO
            // 1 FRONT LOADER, 2 HOOK, 3 CAJA RECOLECTORA 
            string nombrePunto = "";
            string nombrePunto1 = "";
            string servicio = "";
            List<PuntoServicioDescView> tiposerv =
                (List<PuntoServicioDescView>)rs.getPuntoServicioDescripcion(idPuntoServicio);
            foreach (var Xvar in tiposerv)
            {
                servicio = Xvar.tipo_servicio;
                nombrePunto = Xvar.nombre_punto;
                nombrePunto1 = Xvar.nombre_punto1;
            }

            string name_file_type = servicio.ToLower().Replace(" ", "");

            // OBTENIENDO LA DISPOSICION
            List<CobroClienDisposicionView> exp =
                (List<CobroClienDisposicionView>)rs.getDisposicionCerrado(idCliente, fecha, idPuntoServicio);
            foreach (var Xvar in exp)
            {
                DisposcionEs = Decimal.Parse(Xvar.Disposicion.ToString());
            }

            if (idPuntoServicio == 355 || idPuntoServicio == 356 || idPuntoServicio == 358)
            {
                DisposcionEs = 0;
            }

            ValorTonelada = "";
            if (servicio.ToUpper() != "CAJA RECOLECTORA")
            {
                List<PuntoCobroToneView> tone1 =
                    (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(idPuntoServicio, 8);
                foreach (var Xvar in tone1)
                {
                    ValorTonelada = Xvar.valor.Value.ToString("#,##0");
                }

                if (idPuntoServicio == 17 || idPuntoServicio == 87 || idPuntoServicio == 382)
                {
                    List<CobroClienteDisposicionView> dispo =
                        (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(idPuntoServicio);
                    foreach (var Xvar1 in dispo)
                    {
                        ValorTonelada = (Xvar1.CobroTonelada * Xvar1.valoruf).ToString();
                    }
                }

                if (ValorTonelada == "0")
                {
                    ValorTonelada = "";
                }
            }

            List<PuntoNumEquiposView> equipo_cant = (List<PuntoNumEquiposView>)rs.getPuntoNumEquipos(idPuntoServicio);
            foreach (var Xvar in equipo_cant)
            {
                NumeroEquipos = Xvar.cant_equipos.ToString();
            }

            if (NumeroEquipos == "0")
            {
                NumeroEquipos = "";
            }

            List<CobroClientesPuntosView> dot = (List<CobroClientesPuntosView>)rs.getCobroPdfCerradoPtoServ(idCliente, fecha, idPuntoServicio);

            // OBTENIENDO LA DISPOSICION PROCESO CERRADO
            List<CobroClienDisposicionView> expC =
                (List<CobroClienDisposicionView>)rs.getDisposicionCerrado(idCliente, fecha, idPuntoServicio);
            foreach (var Xvar in expC)
            {
                DisposcionEs = Decimal.Parse(Xvar.Disposicion.ToString());
            }

            PdfPTable table = new PdfPTable(7);
            float[] headers = { 25, 10, 10, 10, 15, 20, 10 }; //Header Widths
            table.SetWidths(headers); //Set the pdf headers
            table.WidthPercentage = 100; //Set the PDF File witdh percentage

            AddTableCell(table, "Item");
            AddTableCell(table, "Fecha o Período");
            AddTableCell(table, "Codigo Externo");
            AddTableCell(table, "Pesaje, Volumen o N° Volteos");
            AddTableCell(table, "Patente camión");
            AddTableCell(table, "Conductor");
            AddTableCell(table, "Valor");

            Decimal valor = 0;
            // RECORRIENDO EL LIST RETORNADO POR EL METODO
            foreach (var Item in dot)
            {
                // INSERCION DE LA DISPOSICION
                // SOLO SE IMPRIME UNA SOLA VEZ SI EL SERVICIO ES HOOK
                if (((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "HOOK") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "DISPOSICIÓN") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "NO APLICA") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "FRONT LOADER"))
                {
                    if (existe_disposicion == 0 && DisposcionEs > 0)
                    {
                        #region  disposición

                        string meshere = DateTime.Today.Month.ToString();
                        if (meshere.Length == 1)
                        {
                            meshere = "0" + meshere;
                        }

                        Paragraph paraD = new Paragraph("Disposcion", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD = new PdfPCell(paraD) { HorizontalAlignment = 0 };
                        table.AddCell(theCellD);

                        Paragraph paraD1 = new Paragraph(FechaDisposi(Item.fechaFull), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD1 = new PdfPCell(paraD1) { HorizontalAlignment = 1 };
                        table.AddCell(theCellD1);

                        Paragraph paraD2 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD2 = new PdfPCell(paraD2) { HorizontalAlignment = 1 };
                        table.AddCell(theCellD2);

                        Paragraph paraD3 = new Paragraph(DisposcionEs.ToString("#,##0"), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD3 = new PdfPCell(paraD3) { HorizontalAlignment = 1 };
                        table.AddCell(theCellD3);

                        Paragraph paraD22 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD22 = new PdfPCell(paraD22) { HorizontalAlignment = 1 };
                        table.AddCell(theCellD22);

                        Paragraph paraD4 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD4 = new PdfPCell(paraD4) { HorizontalAlignment = 1 };
                        table.AddCell(theCellD4);

                        // PARA  CALCULAR  EL  VAOR TOTAL DE LA DISPOSICION ES NECESARIO
                        // OBTENER EL COBRO POR  TONELADA  ASOCIADO AL PUNTO DE SERVICIO                               

                        List<PuntoCobroToneView> tone =
                            (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(idPuntoServicio, 8);
                        foreach (var Xvar in tone)
                        {
                            valor_tone = Double.Parse(Xvar.valor.ToString());
                        }

                        List<CobroClienteDisposicionView> disposi =
                            (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(idPuntoServicio, "Retiro");

                        if (disposi.Count != null && disposi.Count > 0)
                        {
                            List<CobroClienteDisposicionView> dispo = (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(idPuntoServicio, "Retiro");
                            foreach (var xvar in dispo)
                            {
                                if (xvar.valoruf.HasValue && !xvar.valoruf.Equals(0.0))
                                {
                                    valor_tone = Convert.ToDouble(xvar.CobroTonelada * xvar.valoruf);
                                }
                            }
                        }

                        Decimal valor_total;
                        if (string.IsNullOrEmpty(valor_tone.ToString()))
                        {
                            valor_tone = 0;
                            valor_total = 0;
                        }
                        else
                        {
                            valor_total = Convert.ToDecimal(DisposcionEs) * Convert.ToDecimal(valor_tone);

                            if (Item.cobro_por_peso == true)
                                valor_total = valor_total / 1000;
                        }

                        valor_total = Math.Floor(valor_total);
                        valor = valor + valor_total;

                        Paragraph paraD5 = new Paragraph(valor_total.ToString("#,##0"), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD5 = new PdfPCell(paraD5) { HorizontalAlignment = 2 };
                        table.AddCell(theCellD5);

                        #endregion

                        // Revisa manejo dris, si existen puntos de servicio al que se le cobre. Caso contrario, se salta registro
                        tone = (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(idPuntoServicio, 17);
                        if (tone.Count != 0)
                        {
                            #region Manejo Dris

                            meshere = DateTime.Today.Month.ToString();
                            if (meshere.Length == 1)
                            {
                                meshere = "0" + meshere;
                            }

                            Paragraph paraMajejodris = new Paragraph("Manejo Dris", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris = new PdfPCell(paraMajejodris) { HorizontalAlignment = 0 };
                            table.AddCell(theCellMajejodris);

                            Paragraph paraMajejodris1 = new Paragraph(FechaDisposi(Item.fechaFull), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris1 = new PdfPCell(paraMajejodris1) { HorizontalAlignment = 1 };
                            table.AddCell(theCellMajejodris1);

                            Paragraph paraMajejodris2 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris2 = new PdfPCell(paraMajejodris2) { HorizontalAlignment = 1 };
                            table.AddCell(theCellMajejodris2);

                            Paragraph paraMajejodris3 = new Paragraph(DisposcionEs.ToString("#,##0"), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris3 = new PdfPCell(paraMajejodris3) { HorizontalAlignment = 1 };
                            table.AddCell(theCellMajejodris3);

                            Paragraph paraMajejodris22 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris22 = new PdfPCell(paraMajejodris22) { HorizontalAlignment = 1 };
                            table.AddCell(theCellMajejodris22);

                            Paragraph paraMajejodris4 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris4 = new PdfPCell(paraMajejodris4) { HorizontalAlignment = 1 };
                            table.AddCell(theCellMajejodris4);

                            // PARA  CALCULAR  EL  VAOR TOTAL DE LA DISPOSICION ES NECESARIO
                            // OBTENER EL COBRO POR  TONELADA  ASOCIADO AL PUNTO DE SERVICIO                               

                            foreach (var Xvar in tone)
                            {
                                valor_tone = Double.Parse(Xvar.valor.ToString());
                            }

                            disposi =
                                (List<CobroClienteDisposicionView>)
                                rs.getDisposicionExcepcion(idPuntoServicio, "Retiro Manejo Dris");

                            if (disposi.Count != null && disposi.Count > 0)
                            {
                                List<CobroClienteDisposicionView> dispo = (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(idPuntoServicio, "Retiro Manejo Dris");
                                foreach (var Xvar1 in dispo)
                                {
                                    if (Xvar1.valoruf.HasValue && !Xvar1.valoruf.Equals(0.0))
                                    {
                                        valor_tone = Convert.ToDouble(Xvar1.CobroTonelada * Xvar1.valoruf);
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(valor_tone.ToString()))
                            {
                                valor_tone = 0;
                                valor_total = 0;
                            }
                            else
                            {
                                valor_total = Convert.ToDecimal(DisposcionEs) * Convert.ToDecimal(valor_tone);

                                if (Item.cobro_por_peso == true)
                                    valor_total = valor_total / 1000;
                            }

                            valor_total = Math.Floor(valor_total);
                            valor = valor + valor_total;

                            Paragraph paraMajejodris5 = new Paragraph(valor_total.ToString("#,##0"), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris5 = new PdfPCell(paraMajejodris5) { HorizontalAlignment = 2 };
                            table.AddCell(theCellMajejodris5);

                            #endregion
                        }
                    }
                    existe_disposicion = 1;
                }

                if (Item.glosa == "Retiro" || Item.glosa == "Arriendo Contenedor")
                {
                    numeroServicios = numeroServicios + 1;

                    // CONSTRUCCION DE CADA LINEA DEL INFORME PDF
                    Paragraph para11 = new Paragraph(Item.glosa.Trim(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell = new PdfPCell(para11) { HorizontalAlignment = 0 };
                    table.AddCell(theCell);

                    Paragraph para22 = new Paragraph(FechaDia(Item.fechaFull), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell22 = new PdfPCell(para22) { HorizontalAlignment = 1 };
                    table.AddCell(theCell22);
                }
                else
                {
                    if (Item.glosa != "Retiro Manejo Dris")
                    {
                        Paragraph para22 = new Paragraph(FechaArriendo(Item.cobro_arriendo), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCell22 = new PdfPCell(para22) { HorizontalAlignment = 1 };
                        table.AddCell(theCell22);
                    }
                }

                string codigo_externo = "";

                if (Item.codigo_externo != null)
                {
                    codigo_externo = Item.codigo_externo.ToString();
                }

                if (Item.glosa != "Retiro Manejo Dris")
                {
                    Paragraph para33 = new Paragraph(codigo_externo, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell33 = new PdfPCell(para33) { HorizontalAlignment = 1 };
                    table.AddCell(theCell33);
                }
                string equipo_desc = "";
                // BUSQUEDA TIPO DE EQUIPO
                idpuntoservicio = idPuntoServicio;
                List<EquipoView> equipo = (List<EquipoView>)rs.getEquipo(idPuntoServicio);
                foreach (var Xvar in equipo)
                {
                    equipo_desc = Xvar.Equipo;
                }

                // -------------------------------------------------------------
                string data = "";
                string many = "";
                if (Item.glosa == "Retiro")
                {
                    if (Item.glosa == "Retiro" && servicio.ToUpper() == "HOOK" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "DISPOSICIÓN" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "NO APLICA" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "FRONT LOADER")
                    {
                        data = Item.Peso.Value.ToString("#,##0");
                        many = Item.Valor.Value.ToString("#,##0");
                        valor = valor + Item.Valor.Value;
                    }
                    else
                    {
                        data = Item.Volteos.ToString();
                        many = Item.Valor.Value.ToString("#,##0");
                        valor = valor + Item.Valor.Value;
                    }
                }
                else
                {
                    data = "";
                    many = Item.Valor.Value.ToString("#,##0");
                    valor = valor + Item.Valor.Value;
                }

                // -------------------------------------------------------------
                if (Item.como_se_calcula.Trim() == "PESO")
                {
                    if (Item.Peso % 1 == 0)
                    {
                        data = Item.Peso.Value.ToString("#,##"); // JPIZARRO si el valor es entero no agrega decimales
                    }
                    else
                    {
                        data = Item.Peso.Value.ToString();
                    }
                }

                if (data == "0")
                {
                    data = "";
                }

                if (Item.glosa != "Retiro Manejo Dris")
                {
                    Paragraph para44 = new Paragraph(data, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell44 = new PdfPCell(para44) { HorizontalAlignment = 1 };
                    table.AddCell(theCell44);

                    Paragraph para55 = new Paragraph(Item.camion, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell55 = new PdfPCell(para55) { HorizontalAlignment = 1 };
                    table.AddCell(theCell55);

                    Paragraph para66 = new Paragraph(Item.conductor, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell66 = new PdfPCell(para66) { HorizontalAlignment = 0 };
                    table.AddCell(theCell66);

                    if (many == "0")
                    {
                        many = "";
                    }

                    Paragraph para77 = new Paragraph(many, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell77 = new PdfPCell(para77) { HorizontalAlignment = 2 };
                    table.AddCell(theCell77);
                }
            }

            document.SetMargins(20, 20, 20, 20);
            document.Open();

            string imagepath = Server.MapPath("/Images");
            Image jpg = Image.GetInstance(imagepath.Replace("\\CobroClientes", "") + "\\resiter_logo_up.jpg");
            jpg.ScaleAbsolute(650f, 50f);
            document.Add(jpg);

            string FechaWhen = DateTime.Today.ToString().Substring(0, 10);

            string descrip = FechaDia(FechaWhen);

            PdfPTable dateIs = new PdfPTable(1);
            float[] encabez = { 100 };
            dateIs.SetWidths(encabez);
            dateIs.WidthPercentage = 100;
            dateIs.DefaultCell.Border = Rectangle.NO_BORDER;
            dateIs.DefaultCell.BorderColor = new Color(255, 255, 255);
            dateIs.SpacingAfter = 4;

            Paragraph Pd = new Paragraph(descrip, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ad = new PdfPCell(Pd)
            {
                HorizontalAlignment = 2,
                BorderColor = new Color(255, 255, 255)
            };
            dateIs.AddCell(Ad);

            document.Add(dateIs);

            Chunk linebreak = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0), Element.ALIGN_LEFT, 1));
            document.Add(linebreak);

            // con el campo id_punto_servicio podemos obtener la zona sucursal region etc
            List<PuntoServicioDireView> puntowhere = (List<PuntoServicioDireView>)rs.getPtoServicioDire(idpuntoservicio);
            foreach (var Xvar in puntowhere)
            {
                zona = Xvar.zona;
                ciudad = Xvar.ciudad;
                nombre_comuna = Xvar.nombre_comuna;
                rubro = Xvar.rubro;
                Sucursal = Xvar.sucursal;
                descrip = string.Concat("Unidad ");
                descrip = descrip.Replace("SIN DATOS", "");
            }

            PdfPTable tableHead1 = new PdfPTable(3);
            float[] headersa1 = { 50, 25, 25 }; //Header Widths
            tableHead1.SetWidths(headersa1); //Set the pdf headers
            tableHead1.WidthPercentage = 100; //Set the PDF File witdh percentage
            tableHead1.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead1.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead1.SpacingBefore = 5;
            tableHead1.SpacingAfter = 1;

            Paragraph Pxa = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axa = new PdfPCell(Pxa)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axa);

            Paragraph Pxb = new Paragraph("Resiter Industrial S.A.", FontFactory.GetFont("Arial", 13, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axb = new PdfPCell(Pxb)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axb);

            Paragraph Pxc = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axc = new PdfPCell(Pxc)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axc);

            Paragraph Pxaa = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axaa = new PdfPCell(Pxaa)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axaa);

            Paragraph Pxbb = new Paragraph("Manejo Integral de Residuos Sólidos", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axbb = new PdfPCell(Pxbb)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axbb);

            Paragraph Pxcc = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axcc = new PdfPCell(Pxcc)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Axcc);

            Paragraph Px11 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11 = new PdfPCell(Px11)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead1.AddCell(Ax11);

            Paragraph Px11a = new Paragraph("Transporte de Carga",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11a = new PdfPCell(Px11a);
            Ax11a.HorizontalAlignment = 0;
            Ax11a.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Ax11a);

            Paragraph Px11c = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11c = new PdfPCell(Px11c);
            Ax11c.HorizontalAlignment = 0;
            Ax11c.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Ax11c);

            Paragraph Pxw1 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1 = new PdfPCell(Pxw1);
            Axw1.HorizontalAlignment = 0;
            Axw1.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1);

            Paragraph Pxw1a = new Paragraph("Los Conquistadores 2752 Providencia",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1a = new PdfPCell(Pxw1a);
            Axw1a.HorizontalAlignment = 0;
            Axw1a.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1a);

            Paragraph Pxw1c = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1c = new PdfPCell(Pxw1c);
            Axw1c.HorizontalAlignment = 0;
            Axw1c.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1c);

            // -------------------------------------------
            PdfPTable tableHead = new PdfPTable(3);
            float[] headersa = { 50, 25, 25 }; //Header Widths
            tableHead.SetWidths(headersa); //Set the pdf headers
            tableHead.WidthPercentage = 100; //Set the PDF File witdh percentage
            tableHead.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead.SpacingBefore = 5;
            tableHead.SpacingAfter = 7;

            periodoes = MesNombre(fecha.Substring(0, 2)) + "-" + fecha.Substring(2, 4);

            Paragraph P2 = new Paragraph(descrip, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A2 = new PdfPCell(P2)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            tableHead.AddCell(A2);

            Paragraph P4 = new Paragraph("N° Servicios: " + numeroServicios.ToString(),
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A4 = new PdfPCell(P4);
            A4.HorizontalAlignment = 0;
            A4.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A4);

            Paragraph P5 = new Paragraph("Período: " + periodoes,
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A5 = new PdfPCell(P5);
            A5.HorizontalAlignment = 0;
            A5.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A5);

            ///---------------------------------------------------------------------------------------------------------

            // BUSCANDO LOS DATOS DEL CLIENTE
            tableHead.AddCell(GetClientReportData(idCliente, rs));

            Paragraph P44 = new Paragraph("N° Equipos: " + NumeroEquipos,
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A44 = new PdfPCell(P44);
            A44.HorizontalAlignment = 0;
            A44.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A44);

            Paragraph P66 = new Paragraph("Tipo de servicio: " + servicio,
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A66 = new PdfPCell(P66);
            A66.HorizontalAlignment = 0;
            A66.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A66);

            ///---------------------------------------------------------------------------------------------------------

            Paragraph P55 = new Paragraph("Nombre Punto: " + nombrePunto1,
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A55 = new PdfPCell(P55);
            A55.HorizontalAlignment = 0;
            A55.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A55);

            Double ValorTone;
            if (ValorTonelada != "")
            {
                double vTone = Convert.ToDouble(ValorTonelada);
                if (vTone < 5 && vTone > 0)
                    ValorTone = Math.Round(Convert.ToDouble(ValorTonelada), 4);
                else
                    ValorTone = Math.Round(Convert.ToDouble(ValorTonelada), 0);
            }
            else
            {
                ValorTone = -999;
            }
            Paragraph P45 = new Paragraph("Valor Disposición: " + ValorTone.ToString().Replace("-999", ""),
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A45 = new PdfPCell(P45);
            A45.HorizontalAlignment = 0;
            A45.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A45);

            Paragraph P6 = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A6 = new PdfPCell(P6);
            A6.HorizontalAlignment = 0;
            A6.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A6);

            document.Add(tableHead);

            PdfPTable tableHead2 = new PdfPTable(3);
            float[] headersa2 = { 50, 25, 25 }; //Header Widths
            tableHead2.SetWidths(headersa2); //Set the pdf headers
            tableHead2.WidthPercentage = 100; //Set the PDF File witdh percentage
            tableHead2.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead2.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead2.SpacingBefore = 5;
            tableHead2.SpacingAfter = 7;
            ///---------------------------------------------------------------------------------------------------------
            Paragraph P71 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A71 = new PdfPCell(P71);
            A71.HorizontalAlignment = 0;
            A71.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A71);

            Paragraph P72 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A72 = new PdfPCell(P72);
            A72.HorizontalAlignment = 0;
            A72.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A72);

            Paragraph P73 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A73 = new PdfPCell(P73);
            A73.HorizontalAlignment = 0;
            A73.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A73);

            Paragraph P7 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A7 = new PdfPCell(P7);
            A7.HorizontalAlignment = 0;
            A7.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A7);

            Paragraph P8 = new Paragraph("Informe de servicios mensuales",
                FontFactory.GetFont("Arial", 12, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell A8 = new PdfPCell(P8);
            A8.HorizontalAlignment = 2;
            A8.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A8);

            Paragraph P9 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A9 = new PdfPCell(P9);
            A9.HorizontalAlignment = 0;
            A9.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A9);

            Paragraph P74 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A74 = new PdfPCell(P74);
            A74.HorizontalAlignment = 0;
            A74.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A74);

            Paragraph P75 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A75 = new PdfPCell(P75);
            A75.HorizontalAlignment = 0;
            A75.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A75);

            Paragraph P76 = new Paragraph(" ",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A76 = new PdfPCell(P76);
            A76.HorizontalAlignment = 0;
            A76.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A76);

            Chunk linebreak1 =
                new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0),
                    Element.ALIGN_LEFT, 1));
            document.Add(linebreak1);

            Paragraph paraup = new Paragraph("linea en blanco",
                FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(255, 255, 255)));
            paraup.Alignment = Element.ALIGN_CENTER;
            document.Add(paraup);

            Paragraph parax = new Paragraph("Informe de servicios mensuales",
                FontFactory.GetFont("Arial", 12, Font.BOLD, new Color(0, 0, 0)));
            parax.Alignment = Element.ALIGN_CENTER;

            document.Add(parax);
            document.Add(new Paragraph("\n"));
            document.Add(table);
            valor = Math.Floor(valor);
            Decimal iva = (valor * 19) / 100;

            Decimal total = valor + iva;

            // TABLA CON LOS TOTALES
            PdfPTable table1 = new PdfPTable(7);
            float[] headers1 = { 25, 10, 10, 10, 15, 20, 10 }; //Header Widths
            table1.SetWidths(headers1); //Set the pdf headers
            table1.WidthPercentage = 100; //Set the PDF File witdh percentage
            table1.DefaultCell.Border = Rectangle.NO_BORDER;
            table1.DefaultCell.BorderColor = new Color(255, 255, 255);

            Paragraph para11T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellT = new PdfPCell(para11T);
            theCellT.HorizontalAlignment = 1;
            theCellT.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCellT);

            Paragraph para22T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell22T = new PdfPCell(para22T);
            theCell22T.HorizontalAlignment = 1;
            theCell22T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell22T);

            Paragraph para33T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell33T = new PdfPCell(para33T);
            theCell33T.HorizontalAlignment = 0;
            theCell33T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell33T);

            Paragraph para44T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell44T = new PdfPCell(para44T);
            theCell44T.HorizontalAlignment = 2;
            theCell44T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell44T);

            Paragraph para55T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell55T = new PdfPCell(para55T);
            theCell55T.HorizontalAlignment = 1;
            theCell55T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell55T);

            Paragraph para66T = new Paragraph("Total:",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell66T = new PdfPCell(para66T);
            theCell66T.HorizontalAlignment = 0;
            theCell66T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell66T);

            Paragraph para77T = new Paragraph(valor.ToString("#,##0"),
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell77T = new PdfPCell(para77T);
            theCell77T.HorizontalAlignment = 2;
            theCell77T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell77T);

            //****************************************************************************************
            Paragraph para11x = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellx = new PdfPCell(para11x);
            theCellx.HorizontalAlignment = 1;
            theCellx.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCellx);

            Paragraph para22x = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell22x = new PdfPCell(para22x);
            theCell22x.HorizontalAlignment = 1;
            theCell22x.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell22x);

            Paragraph para33x = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell33x = new PdfPCell(para33x);
            theCell33x.HorizontalAlignment = 1;
            theCell33x.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell33x);

            Paragraph para44x = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell44x = new PdfPCell(para44x);
            theCell44x.HorizontalAlignment = 1;
            theCell44x.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell44x);

            Paragraph para55x = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell55x = new PdfPCell(para55x);
            theCell55x.HorizontalAlignment = 1;
            theCell55x.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell55x);

            Paragraph para66x = new Paragraph("IVA:",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell66x = new PdfPCell(para66x);
            theCell66x.HorizontalAlignment = 0;
            theCell66x.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell66x);

            Paragraph para77z = new Paragraph(iva.ToString("#,##0"),
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell77z = new PdfPCell(para77z);
            theCell77z.HorizontalAlignment = 2;
            theCell77z.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCell77z);

            //****************************************************************************************

            Paragraph para11W = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW = new PdfPCell(para11W);
            theCellW.HorizontalAlignment = 1;
            theCellW.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCellW);

            Paragraph paraW2T = new Paragraph("",
                FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW2T = new PdfPCell(paraW2T);
            theCellW2T.HorizontalAlignment = 1;
            theCellW2T.BorderColor = new Color(255, 255, 255);
            table1.AddCell(theCellW2T);

            Paragraph paraW3T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellWT3 = new PdfPCell(paraW3T)
            {
                HorizontalAlignment = 1,
                BorderColor = new Color(255, 255, 255)
            };
            table1.AddCell(theCellWT3);

            Paragraph paraW4T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW4T = new PdfPCell(paraW4T)
            {
                HorizontalAlignment = 1,
                BorderColor = new Color(255, 255, 255)
            };
            table1.AddCell(theCellW4T);

            Paragraph paraW5T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW5T = new PdfPCell(paraW5T)
            {
                HorizontalAlignment = 1,
                BorderColor = new Color(255, 255, 255)
            };
            table1.AddCell(theCellW5T);

            Paragraph paraW6T = new Paragraph("Total más IVA:", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW6T = new PdfPCell(paraW6T)
            {
                HorizontalAlignment = 0,
                BorderColor = new Color(255, 255, 255)
            };
            table1.AddCell(theCellW6T);

            Paragraph paraWT = new Paragraph(total.ToString("#,##0"), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellWT = new PdfPCell(paraWT)
            {
                HorizontalAlignment = 2,
                BorderColor = new Color(255, 255, 255)
            };
            table1.AddCell(theCellWT);

            //****************************************************************************************
            document.Add(new Paragraph("\n"));
            document.Add(table1);

            Paragraph pxx = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0), Element.ALIGN_LEFT, 1)));
            document.Add(pxx);
            document.Close();

            stream.Flush();
            stream.Position = 0;

            FileStreamResult result = new FileStreamResult(stream, "application/pdf");
            result.FileDownloadName = String.Concat(nombrePunto, "_Periodo", fecha, "_", name_file_type, ".pdf");
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id_cliente"></param>
        /// <param name="id_punto_servicio"></param>
        /// <param name="Fecha"></param>
        /// <param name="id_contra"></param>
        /// <param name="id_sucu"></param>
        /// <returns></returns>
        public ActionResult XLSLineaPunto(int id_cliente, int id_punto_servicio, string Fecha, int id_contra, int id_sucu)
        {
            string RazonSocial = "";
            string Rut = "";
            Decimal valor = 0;
            Decimal iva = 0;
            int NumeroServicios = 0;
            string servicio = "";
            int idpuntoservicio = 0;
            Nullable<System.Int32> idtiposervicio = 0;
            string zona = "";
            string ciudad = "";
            string nombre_comuna = "";
            string rubro = "";
            string descrip = "";
            string imagepath = Server.MapPath("/Images");
            FileStreamResult result;
            MemoryStream stream;
            MemoryStream streamOutXls;
            Decimal DisposcionEs = 0;
            Decimal ValorEs = 0;
            int existe_disposicion = 0;
            string periodoes = "";
            string Sucursal = "";
            System.Decimal valor_total = 0;
            string ValorTonelada = "";
            string NumeroEquipos = "";
            System.Double valor_tone = 0;
            int Cerrado = 0;

            List<CobroClientesPuntosView> dot = null;

            CobroClientesHistoricoRepository rs = new CobroClientesHistoricoRepository(new sgsbdEntities());

            //GENERANDO EL PDF

            int id_suma = 0;

            stream = new MemoryStream();
            streamOutXls = new MemoryStream();

            Document document1 = new Document(new Rectangle(800f, 5500f));
            PdfWriter pdfWriter1 = PdfWriter.GetInstance(document1, stream);
            pdfWriter1.CloseStream = false;

            // DETERMINACION DEL TIPO DE SERVICIO
            // 1 FRONT LOADER, 2 HOOK, 3 CAJA RECOLECTORA 
            string nombrePunto = "";
            string nombrePunto1 = "";
            List<PuntoServicioDescView> tiposerv = (List<PuntoServicioDescView>)rs.getPuntoServicioDescripcion(id_punto_servicio);
            foreach (var Xvar in tiposerv)
            {
                idtiposervicio = Xvar.id_tipo_servicio;
                servicio = Xvar.tipo_servicio;
                nombrePunto = Xvar.nombre_punto;
                nombrePunto1 = Xvar.nombre_punto1;
            }

            string name_file_type = servicio.ToLower().Replace(" ", "");
            string NAME_FILE = String.Concat(nombrePunto, "_Periodo", Fecha, "_", name_file_type, ".pdf");

            // BUSCANDO LOS DATOS DEL CLIENTE
            List<ClientesView> cli = (List<ClientesView>)rs.getClienteData(id_cliente);
            foreach (var Xvar in cli)
            {
                RazonSocial = Xvar.razon_social;
                Rut = Xvar.rut_cliente;
            }

            // OBTENIENDO LA DISPOSICION
            List<CobroClienDisposicionView> exp = (List<CobroClienDisposicionView>)rs.getDisposicionCerrado(id_cliente, Fecha, id_punto_servicio);
            foreach (var Xvar in exp)
            {
                DisposcionEs = System.Decimal.Parse(Xvar.Disposicion.ToString());
                ValorEs = Xvar.Valor.GetValueOrDefault(0);
            }

            if (id_punto_servicio == 355 || id_punto_servicio == 356 || id_punto_servicio == 358)
            {
                DisposcionEs = 0;
            }

            ValorTonelada = "";
            if (servicio.ToUpper() != "CAJA RECOLECTORA")
            {
                List<PuntoCobroToneView> tone1 =
                    (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(id_punto_servicio, 8);
                foreach (var Xvar in tone1)
                {
                    ValorTonelada = Xvar.valor.Value.ToString("#,##0");
                }

                if (id_punto_servicio == 17 || id_punto_servicio == 87 || id_punto_servicio == 382)
                {
                    List<CobroClienteDisposicionView> dispo =
                        (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(id_punto_servicio);
                    foreach (var Xvar1 in dispo)
                    {
                        ValorTonelada = (Xvar1.CobroTonelada * Xvar1.valoruf).ToString();
                    }
                }

                if (ValorTonelada == "0")
                {
                    ValorTonelada = "";
                }
            }

            List<PuntoNumEquiposView> equipo_cant = (List<PuntoNumEquiposView>)rs.getPuntoNumEquipos(id_punto_servicio);
            foreach (var Xvar in equipo_cant)
            {
                NumeroEquipos = Xvar.cant_equipos.ToString();
            }

            if (NumeroEquipos == "0")
            {
                NumeroEquipos = "";
            }

            dot = (List<CobroClientesPuntosView>)rs.getCobroPdfCerradoPtoServ(id_cliente, Fecha, id_punto_servicio);

            // OBTENIENDO LA DISPOSICION PROCESO CERRADO
            List<CobroClienDisposicionView> expC = (List<CobroClienDisposicionView>)rs.getDisposicionCerrado(id_cliente, Fecha, id_punto_servicio);
            foreach (var Xvar in expC)
            {
                DisposcionEs = System.Decimal.Parse(Xvar.Disposicion.ToString());
            }

            PdfPTable tableXLS = new PdfPTable(7);
            float[] headersXLS = { 25, 10, 10, 10, 15, 20, 10 };  //Header Widths
            tableXLS.SetWidths(headersXLS);        //Set the pdf headers
            tableXLS.WidthPercentage = 100;       //Set the PDF File witdh percentage

            Paragraph para770 = new Paragraph("Item", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell770 = new PdfPCell(para770);
            theCell770.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell770);

            Paragraph para771 = new Paragraph("Fecha o Período", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell771 = new PdfPCell(para771);
            theCell771.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell771);

            Paragraph para772 = new Paragraph("Codigo Externo", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell772 = new PdfPCell(para772);
            theCell772.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell772);

            Paragraph para773 = new Paragraph("Pesaje, Volumen o N° Volteos", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell773 = new PdfPCell(para773);
            theCell773.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell773);

            Paragraph para774 = new Paragraph("Patente camión", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell774 = new PdfPCell(para774);
            theCell774.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell774);

            Paragraph para776 = new Paragraph("Conductor", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell776 = new PdfPCell(para776);
            theCell776.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell776);

            Paragraph para777 = new Paragraph("Valor", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell theCell777 = new PdfPCell(para777);
            theCell777.HorizontalAlignment = 1;
            tableXLS.AddCell(theCell777);

            valor = 0;
            // RECORRIENDO EL LIST RETORNADO POR EL METODO
            foreach (var Item in dot)
            {
                // INSERCION DE LA DISPOSICION
                // SOLO SE IMPRIME UNA SOLA VEZ SI EL SERVICIO ES HOOK
                if (((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "HOOK") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "DISPOSICIÓN") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "NO APLICA") ||
                    ((Item.glosa == "Retiro" || Item.glosa == "Retiro Manejo Dris") && servicio.ToUpper() == "FRONT LOADER"))
                {
                    if (existe_disposicion == 0 && DisposcionEs > 0)
                    {
                        #region  disposición

                        string meshere = DateTime.Today.Month.ToString();
                        if (meshere.Length == 1)
                        {
                            meshere = "0" + meshere;
                        }
                        string fechaPrint = "01" + "/" + meshere + "/" + DateTime.Today.Year.ToString();

                        Paragraph paraD = new Paragraph("Disposcion",
                            FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD = new PdfPCell(paraD);
                        theCellD.HorizontalAlignment = 0;
                        tableXLS.AddCell(theCellD);

                        Paragraph paraD1 = new Paragraph(FechaDisposi(Item.fechaFull), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD1 = new PdfPCell(paraD1);
                        theCellD1.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCellD1);

                        Paragraph paraD2 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD2 = new PdfPCell(paraD2);
                        theCellD2.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCellD2);

                        Paragraph paraD3X = new Paragraph(DisposcionEs.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD3X = new PdfPCell(paraD3X);
                        theCellD3X.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCellD3X);

                        Paragraph paraD22 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD22 = new PdfPCell(paraD22);
                        theCellD2.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCellD22);

                        Paragraph paraD4 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD4 = new PdfPCell(paraD4);
                        theCellD4.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCellD4);

                        // PARA  CALCULAR  EL  VAOR TOTAL DE LA DISPOSICION ES NECESARIO
                        // OBTENER EL COBRO POR  TONELADA  ASOCIADO AL PUNTO DE SERVICIO                               

                        List<PuntoCobroToneView> tone = (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(id_punto_servicio, 8);
                        foreach (var Xvar in tone)
                        {
                            valor_tone = System.Double.Parse(Xvar.valor.ToString());
                        }

                        List<CobroClienteDisposicionView> disposi = (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(id_punto_servicio, "Retiro");

                        if (disposi.Count != null && disposi.Count > 0)
                        {
                            List<CobroClienteDisposicionView> dispo = (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(id_punto_servicio, "Retiro");
                            foreach (var Xvar1 in dispo)
                            {
                                if (Xvar1.valoruf.HasValue && !Xvar1.valoruf.Equals(0.0))
                                {
                                    valor_tone = Convert.ToDouble(Xvar1.CobroTonelada * Xvar1.valoruf);
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(valor_tone.ToString()))
                        {
                            valor_tone = 0;
                            valor_total = 0;
                        }
                        else
                        {
                            valor_total = Convert.ToDecimal(DisposcionEs) * Convert.ToDecimal(valor_tone);
                            //JPIZARRO prueba 12-04-2016

                            if (Item.cobro_por_peso)
                                valor_total = valor_total / 1000;
                        }

                        List<CobroClienteDisposicionView> cccv = (List<CobroClienteDisposicionView>)rs.getDisposicionExcepcion(Item.id_punto_servicio);
                        foreach (var ptouf in cccv)
                        {
                            if (ptouf.valoruf != null && ptouf.valoruf > 0)
                            {
                                valor_total = valor_total * (Convert.ToDecimal(ptouf.valoruf.Value));
                            }
                        }
                        valor_total = Math.Floor(valor_total);
                        valor = valor + valor_total;

                        Paragraph paraD5X = new Paragraph(valor_total.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCellD5X = new PdfPCell(paraD5X);
                        theCellD5X.HorizontalAlignment = 2;
                        tableXLS.AddCell(theCellD5X);
                        #endregion

                        // Revisa manejo dris, si existen puntos de servicio al que se le cobre. Caso contrario, se salta registro
                        tone = (List<PuntoCobroToneView>)rs.getPuntoCobroTonelada(id_punto_servicio, 17);
                        if (tone.Count != 0)
                        {
                            #region Manejo Dris

                            meshere = DateTime.Today.Month.ToString();
                            if (meshere.Length == 1)
                            {
                                meshere = "0" + meshere;
                            }
                            fechaPrint = "01" + "/" + meshere + "/" + DateTime.Today.Year.ToString();

                            Paragraph paraMajejodris = new Paragraph("Manejo Dris",
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris = new PdfPCell(paraMajejodris);
                            theCellMajejodris.HorizontalAlignment = 0;
                            tableXLS.AddCell(theCellMajejodris);

                            Paragraph paraMajejodris1 = new Paragraph(FechaDisposi(Item.fechaFull),
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris1 = new PdfPCell(paraMajejodris1);
                            theCellMajejodris1.HorizontalAlignment = 1;
                            tableXLS.AddCell(theCellMajejodris1);

                            Paragraph paraMajejodris2 = new Paragraph("",
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris2 = new PdfPCell(paraMajejodris2);
                            theCellD2.HorizontalAlignment = 1;
                            tableXLS.AddCell(theCellMajejodris2);

                            Paragraph paraMajejodris3X = new Paragraph(DisposcionEs.ToString(),
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris3X = new PdfPCell(paraMajejodris3X);
                            theCellMajejodris3X.HorizontalAlignment = 1;
                            tableXLS.AddCell(theCellMajejodris3X);

                            Paragraph paraMajejodris22 = new Paragraph("",
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris22 = new PdfPCell(paraMajejodris22);
                            theCellMajejodris2.HorizontalAlignment = 1;
                            tableXLS.AddCell(theCellMajejodris22);

                            Paragraph paraMajejodris4 = new Paragraph("",
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris4 = new PdfPCell(paraMajejodris4);
                            theCellMajejodris4.HorizontalAlignment = 1;
                            tableXLS.AddCell(theCellMajejodris4);

                            // PARA  CALCULAR  EL  VAOR TOTAL DE LA DISPOSICION ES NECESARIO
                            // OBTENER EL COBRO POR  TONELADA  ASOCIADO AL PUNTO DE SERVICIO                               
                            foreach (var Xvar in tone)
                            {
                                valor_tone = System.Double.Parse(Xvar.valor.ToString());
                            }

                            disposi =
                                (List<CobroClienteDisposicionView>)
                                rs.getDisposicionExcepcion(id_punto_servicio, "Retiro Manejo Dris");

                            if (disposi.Count != null && disposi.Count > 0)
                            {
                                List<CobroClienteDisposicionView> dispo =
                                    (List<CobroClienteDisposicionView>)
                                    rs.getDisposicionExcepcion(id_punto_servicio, "Retiro Manejo Dris");
                                foreach (var Xvar1 in dispo)
                                {
                                    if (Xvar1.valoruf.HasValue && !Xvar1.valoruf.Equals(0.0))
                                    {
                                        valor_tone = Convert.ToDouble(Xvar1.CobroTonelada * Xvar1.valoruf);
                                    }
                                }
                            }

                            if (string.IsNullOrEmpty(valor_tone.ToString()))
                            {
                                valor_tone = 0;
                                valor_total = 0;
                            }
                            else
                            {
                                valor_total = Convert.ToDecimal(DisposcionEs) * Convert.ToDecimal(valor_tone);
                                //JPIZARRO prueba 12-04-2016
                                if (Item.cobro_por_peso)
                                    valor_total = valor_total / 1000;
                            }

                            valor_total = Math.Floor(valor_total);
                            valor = valor + valor_total;

                            Paragraph parMajejodris5X = new Paragraph(valor_total.ToString(),
                                FontFactory.GetFont("Arial", 10, Font.NORMAL,
                                    new Color(0, 0, 0)));
                            PdfPCell theCellMajejodris5X = new PdfPCell(parMajejodris5X);
                            theCellMajejodris5X.HorizontalAlignment = 2;
                            tableXLS.AddCell(theCellMajejodris5X);

                            #endregion
                        }
                    };
                    existe_disposicion = 1;
                }

                if (Item.glosa == "Retiro" || Item.glosa == "Arriendo Contenedor")
                {
                    NumeroServicios = NumeroServicios + 1;

                    // CONSTRUCCION DE CADA LINEA DEL INFORME PDF
                    Paragraph para11 = new Paragraph(Item.glosa.Trim(),
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell = new PdfPCell(para11);
                    theCell.HorizontalAlignment = 0;
                    tableXLS.AddCell(theCell);

                    Paragraph para22 = new Paragraph(FechaDia(Item.fechaFull.ToString()),
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell22 = new PdfPCell(para22);
                    theCell22.HorizontalAlignment = 1;
                    tableXLS.AddCell(theCell22);
                }
                else
                {
                    if (Item.glosa != "Retiro Manejo Dris")
                    {
                        Paragraph para22 = new Paragraph(FechaArriendo(Item.cobro_arriendo), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                        PdfPCell theCell22 = new PdfPCell(para22);
                        theCell22.HorizontalAlignment = 1;
                        tableXLS.AddCell(theCell22);
                    }

                }

                string codigo_externo = "";

                if (Item.codigo_externo != null)
                {
                    codigo_externo = Item.codigo_externo.ToString();
                }

                if (Item.glosa != "Retiro Manejo Dris")
                {
                    Paragraph para33 = new Paragraph(codigo_externo,
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell33 = new PdfPCell(para33);
                    theCell33.HorizontalAlignment = 1;
                    tableXLS.AddCell(theCell33);
                }
                string equipo_desc = "";
                // BUSQUEDA TIPO DE EQUIPO
                idpuntoservicio = id_punto_servicio;
                List<EquipoView> equipo = (List<EquipoView>)rs.getEquipo(id_punto_servicio);
                foreach (var Xvar in equipo)
                {
                    equipo_desc = Xvar.Equipo;
                }

                // -------------------------------------------------------------
                string dataXLS = "";
                string manyXLS = "";
                if (Item.glosa == "Retiro")
                {
                    if (Item.glosa == "Retiro" && servicio.ToUpper() == "HOOK" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "DISPOSICIÓN" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "NO APLICA" ||
                        Item.glosa == "Retiro" && servicio.ToUpper() == "FRONT LOADER")
                    {
                        valor = valor + Item.Valor.Value;
                        dataXLS = Item.Peso.Value.ToString();
                        manyXLS = Item.Valor.Value.ToString();
                    }
                    else
                    {
                        valor = valor + Item.Valor.Value;
                        dataXLS = Item.Volteos.ToString();
                        manyXLS = Item.Valor.Value.ToString();
                    }
                }
                else
                {
                    valor = valor + Item.Valor.Value;
                    dataXLS = "";
                    manyXLS = Item.Valor.Value.ToString();
                }

                // VALOR TOTAL DE LA DISPOSICION
                // -------------------------------------------------------------

                if (dataXLS == "0")
                {
                    dataXLS = "";
                }

                if (Item.glosa != "Retiro Manejo Dris")
                {
                    Paragraph para44z = new Paragraph(dataXLS,
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell44z = new PdfPCell(para44z);
                    theCell44z.HorizontalAlignment = 1;
                    tableXLS.AddCell(theCell44z);

                    Paragraph para55 = new Paragraph(Item.camion,
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell55 = new PdfPCell(para55);
                    theCell55.HorizontalAlignment = 1;
                    tableXLS.AddCell(theCell55);

                    Paragraph para66 = new Paragraph(Item.conductor,
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell66 = new PdfPCell(para66);
                    theCell66.HorizontalAlignment = 0;
                    tableXLS.AddCell(theCell66);

                    if (manyXLS == "0")
                    {
                        manyXLS = "";
                    }

                    Paragraph para77w = new Paragraph(manyXLS,
                        FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
                    PdfPCell theCell77w = new PdfPCell(para77w);
                    theCell77w.HorizontalAlignment = 2;
                    tableXLS.AddCell(theCell77w);
                }
            }

            document1.SetMargins(7, 7, 5, 5);
            document1.Open();

            Image jpg = Image.GetInstance(imagepath.Replace("\\CobroClientes", "") + "\\resiter_logo_up.jpg");
            jpg.ScaleAbsolute(650f, 50f);
            document1.Add(jpg);

            string FechaWhen = "";
            FechaWhen = DateTime.Today.ToString().Substring(0, 10);
            descrip = FechaDia(FechaWhen);

            PdfPTable dateIs = new PdfPTable(1);
            float[] encabez = { 100 };  //Header Widths
            dateIs.SetWidths(encabez);        //Set the pdf headers
            dateIs.WidthPercentage = 100;       //Set the PDF File witdh percentage
            dateIs.DefaultCell.Border = Rectangle.NO_BORDER;
            dateIs.DefaultCell.BorderColor = new Color(255, 255, 255);
            dateIs.SpacingAfter = 4;

            Paragraph Pd = new Paragraph(descrip, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ad = new PdfPCell(Pd);
            Ad.HorizontalAlignment = 2;
            Ad.BorderColor = new Color(255, 255, 255);
            dateIs.AddCell(Ad);

            Chunk linebreak = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0), Element.ALIGN_LEFT, 1));
            document1.Add(new Paragraph("\n"));

            // con el campo id_punto_servicio podemos obtener la zona sucursal region etc
            List<PuntoServicioDireView> puntowhere = (List<PuntoServicioDireView>)rs.getPtoServicioDire(idpuntoservicio);
            foreach (var Xvar in puntowhere)
            {
                zona = Xvar.zona;
                ciudad = Xvar.ciudad;
                nombre_comuna = Xvar.nombre_comuna;
                rubro = Xvar.rubro;
                Sucursal = Xvar.sucursal;
                descrip = string.Concat("Unidad ");
                descrip = descrip.Replace("SIN DATOS", "");
            };

            PdfPTable tableHead1 = new PdfPTable(3);
            float[] headersa1 = { 50, 25, 25 };  //Header Widths
            tableHead1.SetWidths(headersa1);        //Set the pdf headers
            tableHead1.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableHead1.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead1.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead1.SpacingBefore = 5;
            tableHead1.SpacingAfter = 1;

            Paragraph Pxa = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axa = new PdfPCell(Pxa);
            Axa.HorizontalAlignment = 0;
            Axa.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axa);

            Paragraph Pxb = new Paragraph("Resiter Industrial S.A.", FontFactory.GetFont("Arial", 13, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axb = new PdfPCell(Pxb);
            Axb.HorizontalAlignment = 0;
            Axb.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axb);

            Paragraph Pxc = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axc = new PdfPCell(Pxc);
            Axc.HorizontalAlignment = 0;
            Axc.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axc);

            Paragraph Pxaa = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axaa = new PdfPCell(Pxaa);
            Axaa.HorizontalAlignment = 0;
            Axaa.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axaa);

            Paragraph Pxbb = new Paragraph("Manejo Integral de Residuos Sólidos", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axbb = new PdfPCell(Pxbb);
            Axbb.HorizontalAlignment = 0;
            Axbb.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axbb);

            Paragraph Pxcc = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axcc = new PdfPCell(Pxcc);
            Axcc.HorizontalAlignment = 0;
            Axcc.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axcc);

            Paragraph Px11 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11 = new PdfPCell(Px11);
            Ax11.HorizontalAlignment = 0;
            Ax11.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Ax11);

            Paragraph Px11a = new Paragraph("Transporte de Carga", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11a = new PdfPCell(Px11a);
            Ax11a.HorizontalAlignment = 0;
            Ax11a.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Ax11a);

            Paragraph Px11c = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Ax11c = new PdfPCell(Px11c);
            Ax11c.HorizontalAlignment = 0;
            Ax11c.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Ax11c);

            Paragraph Pxw1 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1 = new PdfPCell(Pxw1);
            Axw1.HorizontalAlignment = 0;
            Axw1.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1);

            Paragraph Pxw1a = new Paragraph("Los Conquistadores 2752 Providencia", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1a = new PdfPCell(Pxw1a);
            Axw1a.HorizontalAlignment = 0;
            Axw1a.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1a);

            Paragraph Pxw1c = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell Axw1c = new PdfPCell(Pxw1c);
            Axw1c.HorizontalAlignment = 0;
            Axw1c.BorderColor = new Color(255, 255, 255);
            tableHead1.AddCell(Axw1c);
            document1.Add(tableHead1);

            // -------------------------------------------
            PdfPTable tableHead = new PdfPTable(3);
            float[] headersa = { 50, 25, 25 };  //Header Widths
            tableHead.SetWidths(headersa);        //Set the pdf headers
            tableHead.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableHead.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead.SpacingBefore = 5;
            tableHead.SpacingAfter = 7;

            periodoes = MesNombre(Fecha.Substring(0, 2)) + "-" + Fecha.Substring(2, 4);

            Paragraph P2 = new Paragraph(descrip, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A2 = new PdfPCell(P2);
            A2.HorizontalAlignment = 0;
            A2.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A2);

            Paragraph P4 = new Paragraph("N° Servicios: " + NumeroServicios.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A4 = new PdfPCell(P4);
            A4.HorizontalAlignment = 0;
            A4.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A4);

            Paragraph P5 = new Paragraph("Período: " + periodoes, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A5 = new PdfPCell(P5);
            A5.HorizontalAlignment = 0;
            A5.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A5);

            ///---------------------------------------------------------------------------------------------------------

            string cliente = "Cliente: " + RazonSocial.Trim() + " Rut: " + Rut.Trim();
            Paragraph P3 = new Paragraph(cliente, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A3 = new PdfPCell(P3);
            A3.HorizontalAlignment = 0;
            A3.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A3);

            Paragraph P44 = new Paragraph("N° Equipos: " + NumeroEquipos, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A44 = new PdfPCell(P44);
            A44.HorizontalAlignment = 0;
            A44.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A44);

            Paragraph P66 = new Paragraph("Tipo de servicio: " + servicio, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A66 = new PdfPCell(P66);
            A66.HorizontalAlignment = 0;
            A66.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A66);

            ///---------------------------------------------------------------------------------------------------------

            Paragraph P55 = new Paragraph("Nombre Punto: " + nombrePunto1, FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A55 = new PdfPCell(P55);
            A55.HorizontalAlignment = 0;
            A55.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A55);

            System.Double ValorTone;
            if (ValorTonelada != "")
            {
                double vTone = Convert.ToDouble(ValorTonelada);
                if (vTone < 5 && vTone > 0)
                    ValorTone = Math.Round(Convert.ToDouble(ValorTonelada), 4);
                else
                    ValorTone = Math.Round(Convert.ToDouble(ValorTonelada), 0);
            }
            else
            {
                ValorTone = -999;
            }
            Paragraph P45 = new Paragraph("Valor Disposición: " + ValorTone.ToString().Replace("-999", ""), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A45 = new PdfPCell(P45);
            A45.HorizontalAlignment = 0;
            A45.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A45);

            Paragraph P6 = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A6 = new PdfPCell(P6);
            A6.HorizontalAlignment = 0;
            A6.BorderColor = new Color(255, 255, 255);
            tableHead.AddCell(A6);

            document1.Add(tableHead);

            PdfPTable tableHead2 = new PdfPTable(3);
            float[] headersa2 = { 50, 25, 25 };  //Header Widths
            tableHead2.SetWidths(headersa2);        //Set the pdf headers
            tableHead2.WidthPercentage = 100;       //Set the PDF File witdh percentage
            tableHead2.DefaultCell.Border = Rectangle.NO_BORDER;
            tableHead2.DefaultCell.BorderColor = new Color(255, 255, 255);
            tableHead2.SpacingBefore = 5;
            tableHead2.SpacingAfter = 7;
            ///---------------------------------------------------------------------------------------------------------
            Paragraph P71 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A71 = new PdfPCell(P71);
            A71.HorizontalAlignment = 0;
            A71.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A71);

            Paragraph P72 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A72 = new PdfPCell(P72);
            A72.HorizontalAlignment = 0;
            A72.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A72);

            Paragraph P73 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A73 = new PdfPCell(P73);
            A73.HorizontalAlignment = 0;
            A73.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A73);

            Paragraph P7 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A7 = new PdfPCell(P7);
            A7.HorizontalAlignment = 0;
            A7.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A7);

            Paragraph P8 = new Paragraph("Informe de servicios mensuales", FontFactory.GetFont("Arial", 12, Font.BOLD, new Color(0, 0, 0)));
            PdfPCell A8 = new PdfPCell(P8);
            A8.HorizontalAlignment = 2;
            A8.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A8);

            Paragraph P9 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A9 = new PdfPCell(P9);
            A9.HorizontalAlignment = 0;
            A9.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A9);

            Paragraph P74 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A74 = new PdfPCell(P74);
            A74.HorizontalAlignment = 0;
            A74.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A74);

            Paragraph P75 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A75 = new PdfPCell(P75);
            A75.HorizontalAlignment = 0;
            A75.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A75);

            Paragraph P76 = new Paragraph(" ", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell A76 = new PdfPCell(P76);
            A76.HorizontalAlignment = 0;
            A76.BorderColor = new Color(255, 255, 255);
            tableHead2.AddCell(A76);

            document1.Add(dateIs);
            document1.Add(new Paragraph("\n"));
            document1.Add(tableHead2);

            Chunk linebreak1 = new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0), Element.ALIGN_LEFT, 1));
            document1.Add(new Paragraph("\n"));

            Paragraph paraup = new Paragraph("linea en blanco", FontFactory.GetFont("Arial", 9, Font.BOLD, new Color(255, 255, 255)));
            paraup.Alignment = Element.ALIGN_CENTER;

            Paragraph parax = new Paragraph("Informe de servicios mensuales", FontFactory.GetFont("Arial", 12, Font.BOLD, new Color(0, 0, 0)));
            parax.Alignment = Element.ALIGN_CENTER;

            document1.Add(new Paragraph("\n"));
            document1.Add(tableXLS);
            valor = Math.Floor(valor);
            iva = (valor * 19) / 100;
            iva = Math.Round(iva, 0);

            Decimal total = valor + iva;

            // TABLA CON LOS TOTALES
            PdfPTable table1x = new PdfPTable(7);
            float[] headers1x = { 25, 10, 10, 10, 15, 20, 10 };  //Header Widths
            table1x.SetWidths(headers1x);        //Set the pdf headers
            table1x.WidthPercentage = 100;       //Set the PDF File witdh percentage
            table1x.DefaultCell.Border = Rectangle.NO_BORDER;
            table1x.DefaultCell.BorderColor = new Color(255, 255, 255);

            Paragraph para11T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellT = new PdfPCell(para11T);
            theCellT.HorizontalAlignment = 1;
            theCellT.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellT);

            Paragraph para22T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell22T = new PdfPCell(para22T);
            theCell22T.HorizontalAlignment = 1;
            theCell22T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell22T);

            Paragraph para33T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell33T = new PdfPCell(para33T);
            theCell33T.HorizontalAlignment = 0;
            theCell33T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell33T);

            Paragraph para44T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell44T = new PdfPCell(para44T);
            theCell44T.HorizontalAlignment = 2;
            theCell44T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell44T);

            Paragraph para55T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell55T = new PdfPCell(para55T);
            theCell55T.HorizontalAlignment = 1;
            theCell55T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell55T);

            Paragraph para66T = new Paragraph("Total:", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell66T = new PdfPCell(para66T);
            theCell66T.HorizontalAlignment = 0;
            theCell66T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell66T);

            Paragraph para77Tx = new Paragraph(valor.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell77Tx = new PdfPCell(para77Tx);
            theCell77Tx.HorizontalAlignment = 2;
            theCell77Tx.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell77Tx);

            //****************************************************************************************
            Paragraph para11x = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellx = new PdfPCell(para11x);
            theCellx.HorizontalAlignment = 1;
            theCellx.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellx);

            Paragraph para22x = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell22x = new PdfPCell(para22x);
            theCell22x.HorizontalAlignment = 1;
            theCell22x.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell22x);

            Paragraph para33x = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell33x = new PdfPCell(para33x);
            theCell33x.HorizontalAlignment = 1;
            theCell33x.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell33x);

            Paragraph para44x = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell44x = new PdfPCell(para44x);
            theCell44x.HorizontalAlignment = 1;
            theCell44x.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell44x);

            Paragraph para55x = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell55x = new PdfPCell(para55x);
            theCell55x.HorizontalAlignment = 1;
            theCell55x.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell55x);

            Paragraph para66x = new Paragraph("IVA:", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell66x = new PdfPCell(para66x);
            theCell66x.HorizontalAlignment = 0;
            theCell66x.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell66x);

            Paragraph para77zx = new Paragraph(iva.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCell77zx = new PdfPCell(para77zx);
            theCell77zx.HorizontalAlignment = 2;
            theCell77zx.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCell77zx);

            //****************************************************************************************

            Paragraph para11W = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW = new PdfPCell(para11W);
            theCellW.HorizontalAlignment = 1;
            theCellW.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellW);

            Paragraph paraW2T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW2T = new PdfPCell(paraW2T);
            theCellW2T.HorizontalAlignment = 1;
            theCellW2T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellW2T);

            Paragraph paraW3T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellWT3 = new PdfPCell(paraW3T);
            theCellWT3.HorizontalAlignment = 1;
            theCellWT3.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellWT3);

            Paragraph paraW4T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW4T = new PdfPCell(paraW4T);
            theCellW4T.HorizontalAlignment = 1;
            theCellW4T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellW4T);

            Paragraph paraW5T = new Paragraph("", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW5T = new PdfPCell(paraW5T);
            theCellW5T.HorizontalAlignment = 1;
            theCellW5T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellW5T);

            Paragraph paraW6T = new Paragraph("Total más IVA:", FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellW6T = new PdfPCell(paraW6T);
            theCellW6T.HorizontalAlignment = 0;
            theCellW6T.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellW6T);

            Paragraph paraWTx = new Paragraph(total.ToString(), FontFactory.GetFont("Arial", 10, Font.NORMAL, new Color(0, 0, 0)));
            PdfPCell theCellWTx = new PdfPCell(paraWTx);
            theCellWTx.HorizontalAlignment = 2;
            theCellWTx.BorderColor = new Color(255, 255, 255);
            table1x.AddCell(theCellWTx);

            //****************************************************************************************
            document1.Add(new Paragraph("\n"));
            document1.Add(table1x);

            Paragraph pxx = new Paragraph(new Chunk(new iTextSharp.text.pdf.draw.LineSeparator(1.0F, 100.0F, new Color(0, 0, 0), Element.ALIGN_LEFT, 1)));
            document1.Add(pxx);
            document1.Close();

            Bytescout.PDFExtractor.XLSExtractor extractor = new Bytescout.PDFExtractor.XLSExtractor();
            stream.Flush();
            stream.Position = 0;
            extractor.LoadDocumentFromStream(stream);
            extractor.SaveToXLSStream(streamOutXls);

            streamOutXls.Flush();
            streamOutXls.Position = 0;

            result = new FileStreamResult(streamOutXls, "application/vnd.ms-excel");
            result.FileDownloadName = NAME_FILE.Replace("pdf", "xls");
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult CalculoCobroClientes()
        {

            CobroClientesRepository rs = new CobroClientesRepository(new sgsbdEntities());

            string exito_o_fracaso = rs.CalculoCobroClientes("no");

            return new EmptyResult();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="MesIs"></param>
        /// <returns></returns>
        private string MesNombre(string MesIs)
        {

            string MesOut = "";

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="When"></param>
        /// <returns></returns>
        private string FechaArriendo(string When)
        {

            string MesIs = "";
            string MesOut = "";
            string YearIs = "";

            MesIs = When.Substring(5, 2);
            YearIs = When.Substring(0, 4);

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut + "-" + YearIs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="When"></param>
        /// <returns></returns>
        private string FechaDisposi(string When)
        {
            string MesIs = "";
            string MesOut = "";
            string YearIs = "";
            //01-02-2015
            MesIs = When.Substring(3, 2);
            YearIs = When.Substring(6, 4);

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut + "-" + YearIs;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="When"></param>
        /// <returns></returns>
        private string FechaDia(string When)
        {
            string MesIs = "";
            string MesOut = "";
            string Dia = "";

            MesIs = When.Substring(3, 2);
            Dia = When.Substring(0, 2);

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut + " " + Dia;
        }

        ///////////////////////////////////////////////////////////////////////////////////
    }// SECCION NAMESPACE
}
