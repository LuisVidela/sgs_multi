﻿using SGS.Models;
using SGS.Models.repository;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using SGS.Reportes.Models;

namespace SGS.Controllers
{
    public class CertificadosController : Controller
    {
        //
        // GET: /Certificados/
        /// <summary>
        /// Action encargado de recuperar y mostrar la información de generación de certificados.
        /// </summary>
        /// <returns>invocación a View y model para llenarlo</returns>
        public ActionResult Index()
        {
            BuildViewBagList();
            if (UsuariosRepository.getUnidadSeleccionada() == 31) //corregir, no dejar dato en duro (31 = id_unidad de Pto Montt)
                ViewBag.disabled = false;
            else
                ViewBag.disabled = false;
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idSucursal"></param>
        /// <returns></returns>
        public ActionResult GetPuntoServicioPorSucursal(int idSucursal)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            PuntosServiciosRepository puntosServiciosRepository = new PuntosServiciosRepository(ctx);

            IEnumerable<object> puntosServicios = puntosServiciosRepository.GetPuntoServicioPorSucursal(idSucursal);

            JsonResult resultado = new JsonResult
            {
                Data = puntosServicios
            };


            return resultado;
        }


        /// <summary>
        /// Carga los objetos a usar en la vista
        /// </summary>
        private void BuildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            SucursalesRepository rs2 = new SucursalesRepository(ctx);
            VertederosRepository rs3 = new VertederosRepository(ctx);

            IEnumerable<object> sucursales = rs2.GetSucursalesForSelectByUnidad(UsuariosRepository.getUnidadSeleccionada());
            IEnumerable<object> vertederos = rs3.GetVertederosByUnidad(UsuariosRepository.getUnidadSeleccionada());
            IEnumerable<object> puntosServicio = new List<object>();

            ViewBag.sucursales = new SelectList(sucursales, "id_sucursal", "sucursal");
            ViewBag.vertederos = new SelectList(vertederos, "Value", "Text");
            ViewBag.puntosServicio = new SelectList(puntosServicio, "id_punto_servicio", "nombre_punto");
        }


        /// <summary>
        /// Generador principad de certificados
        /// </summary>
        /// <param name="fecha">Fecha inicial periodoreporte</param>
        /// <param name="fechaFin">Fecha final periodo reporte</param>
        /// <param name="sucursal">id sucursal seleccionada</param>
        /// <param name="vertedero">id vertedero seleccionado</param>
        /// <param name="puntoServicio">Vacio si se escogen todos, o el ID si se escoge 1</param>
        /// <returns>En caso de exito, devuelve el reporte, caso contrario un mensaje de error</returns>
        public ActionResult Generar(string fecha, string fechaFin, string sucursal, string vertedero, string puntoServicio)
        {
            DateTime endDate = CastearFecha(fechaFin);
            DateTime date = CastearFecha(fecha);

           //DataTable validacion = certBus.GetValidacionCertificado(rut, razon_social, nombre_sucursal, date, nombre_vertedero, Models.repository.UsuariosRepository.getUnidadSeleccionada());

            Report modeloReporte = new Report
            {
                nameReport = "CertificadoBase",
                tipo = "PDF",
                parameter = new List<Parameter>
                {
                   CrearNuevoParametro("codigo_unidad", UsuariosRepository.getUnidadSeleccionada().ToString()),
                    CrearNuevoParametro("fechaDesde", date.ToShortDateString()),
                    CrearNuevoParametro("fechaHasta", endDate.ToShortDateString()),
                    CrearNuevoParametro("IdSucursal", sucursal),
                    CrearNuevoParametro("IdVertedetro", vertedero),
                    CrearNuevoParametro("idPuntoServicio", String.IsNullOrEmpty(puntoServicio) ? null : puntoServicio)
                }
            };

            Byte[] reporteBytes = Reportes.Reporte.Get(modeloReporte);

            Response.AddHeader("Content-Disposition", "attachment; filename=Certificado.pdf");

            ViewBag.Message = null;
            return new FileContentResult(reporteBytes, "application/pdf");
        }


        /// <summary>
        /// Generación de parápetros para reporte
        /// </summary>
        /// <param name="nombre">Nombre del parámetro a ingresar</param>
        /// <param name="valor">Valor de parámetro según nombre</param>
        /// <returns>Nuevo parametro a adjuntar en listado.</returns>
        private Parameter CrearNuevoParametro(string nombre, string valor)
        {
            Parameter parametro = new Parameter
            {
                name = nombre,
                value = valor
            };

            return parametro;
        }


        /// <summary>
        /// Metodo encargado de validar el campo fecha. Si viene vacio o nulo, asigna la fecha de hoy -100 años, caso contrario castea la fecha a datatime 
        /// </summary>
        /// <param name="fecha">Fecha ingresada</param>
        /// <returns>fecha convertida</returns>
        private static DateTime CastearFecha(string fecha)
        {
            var date = string.IsNullOrEmpty(fecha) ? (DateTime.Now.AddYears(-100)) : Convert.ToDateTime(fecha);
            return date;
        }
    }
}