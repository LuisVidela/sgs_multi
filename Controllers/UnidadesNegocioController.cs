﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;

namespace SGS.Controllers
{
    public class UnidadesNegocioController : Controller
    {
        //
        // GET: /UnidadesNegocio/


        /// <summary>
        /// Action encargado de recuperar y mostrar la información de unidades de negocio para una unidad específica.
        /// </summary>
        /// <returns>invcación a View y model para llenarlo</returns>
        public ActionResult Index()
        {
            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(new sgsbdEntities());
            List<UnidadesNegocioView> data = (List <UnidadesNegocioView>)rs.GetunidadesNegocioView();

            return View(data.OrderBy(m=>m.id_unidad));
        }


        public ActionResult Excel()
        {
            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(new sgsbdEntities());
            List<UnidadesNegocioView> data = (List<UnidadesNegocioView>)rs.GetunidadesNegocioView();

            return View(data.OrderBy(m => m.id_unidad));
        }


        /// <summary>
        /// Invoca la vista de creación de nueva unidad de negocio
        /// </summary>
        /// <returns>Vista de creación unidad de negocio</returns>
        public ActionResult Create()
        {
            BuildViewBagList();
            return View();
        }


        /// <summary>
        /// Crea nueva unidad de negocio, o actualiza una existente basado en los parámetros ingresados.
        /// </summary>
        /// <param name="model">Modelo con la información de unidad de negocio</param>
        /// <returns>Vista Index en caso de update o index existoso y recarga de Create en caso que no</returns>
        [HttpPost]
        public ActionResult Create(unidades_negocio model)
        {
            if (ModelState.IsValid)
            {
                UnidadesNegocioRepository rs = new UnidadesNegocioRepository(new sgsbdEntities());

                if (model.id_unidad == 0)
                {
                    rs.InsertUnidadNegocio(model);
                }
                else 
                {
                    rs.UpdateUnidadNegocio(model);   
                }

                rs.Save();
                return RedirectToAction("Index", "UnidadesNegocio");
            }

            BuildViewBagList();
            return View(model);
        }


        /// <summary>
        /// Invoca a vista Create precargando los datos de unidad de negocio seleccionada
        /// </summary>
        /// <param name="id">id unidad de negocio</param>
        /// <returns>Carga vista create</returns>
        public ActionResult Edit(int id)
        {
            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(new sgsbdEntities());
            unidades_negocio c = rs.GetUnidadNegocioByID(id);
            BuildViewBagList();
            return View("create", c);
        }


        /// <summary>
        /// Elimina unidad de negocio actualmente seleccionada
        /// </summary>
        /// <param name="id">ID Unidad de negocio</param>
        /// <returns>Carga de vista Index de unidades de negocio</returns>
        public ActionResult Delete(int id)
        {
            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(new sgsbdEntities());
            rs.DeleteUnidadNegocio(id);
            rs.Save();
            return RedirectToAction("Index", "UnidadesNegocio");
        }


        public ActionResult DeleteCentro(int id) 
        {
            sgsbdEntities ctx = new sgsbdEntities();
            detalle_centro_responsabilidad cr= ctx.detalle_centro_responsabilidad.Where(x => x.id == id).FirstOrDefault();
            ctx.detalle_centro_responsabilidad.Remove(cr);
            ctx.SaveChanges();
            return RedirectToAction("addCentro", "UnidadesNegocio", new { id=cr.id_unidad_negocio });
        }


        public ActionResult addCentro(int id) 
        {
            ViewBag.id = id;

            sgsbdEntities ctx=new sgsbdEntities();
            var query = (from c in ctx.unidades_negocio.Where(u => u.id_unidad == id)
                         join cd in ctx.detalle_centro_responsabilidad on c.id_unidad equals cd.id_unidad_negocio
                         join cr in ctx.centro_responsabilidad on cd.id_centro_responsabildad equals cr.id_centro
                         select new CentrosBag
                         {
                             Id=cd.id,
                             IdCentro = cr.id_centro,
                             Nombre = cr.centro_responsabilidad1
                         }).ToList();

            int[] res = (from q in query select q.IdCentro).ToArray();
            var restantes = (from q in ctx.centro_responsabilidad
                                 where !res.Contains(q.id_centro)
                                 select new
                                 {
                                     id = q.id_centro,
                                     nombre = q.centro_responsabilidad1
                                 }).ToList();

            ViewBag.restantes = new SelectList(restantes, "id", "nombre");
            ViewBag.asignados = query;
            return View();
        }


        [HttpPost]
        public ActionResult addCentro(detalle_centro_responsabilidad model, int id) 
        {
            sgsbdEntities ctx = new sgsbdEntities();
            detalle_centro_responsabilidad p = new detalle_centro_responsabilidad
            {
                id_centro_responsabildad = model.id_centro_responsabildad,
                id_unidad_negocio = id,
                id = (ctx.detalle_centro_responsabilidad.Max(m => m.id)) + 1
            };
            ctx.detalle_centro_responsabilidad.Add(p);
            ctx.SaveChanges();
            return RedirectToAction("addCentro", "UnidadesNegocio", new { id=id});
        }


        /// <summary>
        /// Carga los objetos a usar en la vista
        /// </summary>
        private void BuildViewBagList() 
        {
            sgsbdEntities ctx = new sgsbdEntities();

            EmpresasRepository rs = new EmpresasRepository(ctx);
            CentroResponsabilidadRepository rs1 = new CentroResponsabilidadRepository(ctx);

            IEnumerable<object> estado = new object[] 
            {
             new { text="Activa",value="Activa"},
             new { text="Inactiva",value="Inactiva"}, 
            };

            ViewBag.empresas = new SelectList(rs.GetEmpresasForList(),"value","text");
            ViewBag.estado = new SelectList(estado,"value","text",ViewBag._estado);
            ViewBag.centro_responsabilidad = new SelectList(rs1.GetCentroResponsabilidadForList(), "value", "text");
        }
    }


    public class CentrosBag 
    {
        public int Id { get; set; }
        public int IdCentro { get; set; }
        public string Nombre { get; set; }
    }
}