﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models;
using System.Text;
using SGS.Models.ShadowBox;
using System.IO;
namespace SGS.Controllers
{
    public class MantenedorConceptosDescuentosController : Controller
    {
        //
        // GET: /MantenedorConceptosDescuentos/

        public ActionResult Index()
        {
            MantenedorConceptosDescuentosView model = new MantenedorConceptosDescuentosView();
            using(var db = new sgsbdEntities())
            {
                if(db.tipo_descuento.FirstOrDefault() != null)
                {
                    var conceptos = db.tipo_descuento.ToList();
                    model.tabla = new List<tablaConceptosdescuentos>();
                    foreach(var item in conceptos)
                    {
                        tablaConceptosdescuentos row = new tablaConceptosdescuentos();
                        row.id = item.id_tipo_descuento;
                        row.concepto = item.nombre;
                        model.tabla.Add(row);
                    }
                }
            }
            return View(model);
        }
        public ActionResult crear(int id)
        {
            crearConceptoDescuentoView model = new crearConceptoDescuentoView();
            if(id == -1)
            {
                model.id = -1;
            }
            else
            {
                using(var db = new sgsbdEntities())
                {
                    var concepto = db.tipo_descuento.Where(u => u.id_tipo_descuento == id).FirstOrDefault();
                    model.concepto = concepto.nombre;
                    model.id = concepto.id_tipo_descuento;
                }
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult crear(crearConceptoDescuentoView model)
        {
            using(var db = new sgsbdEntities())
            {
                if (model.id == -1)
                {
                    var nueva = db.tipo_descuento.Create();
                    if (db.tipo_descuento.Count() != 0) nueva.id_tipo_descuento = db.tipo_descuento.Max(u => u.id_tipo_descuento) + 1;
                    else nueva.id_tipo_descuento = 1;
                    nueva.nombre = model.concepto;
                    db.tipo_descuento.Add(nueva);
                    db.SaveChanges();
                }
                else
                {
                    var modificar = db.tipo_descuento.Where(u => u.id_tipo_descuento == model.id).FirstOrDefault();
                    modificar.nombre = model.concepto;
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index", "MantenedorConceptosDescuentos");
        }

    }
}
