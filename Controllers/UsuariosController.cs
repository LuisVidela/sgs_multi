﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Web.Security;
using System.Security.Principal;
using WebMatrix.WebData;
using SGS.Filters;
using SGS.Models.modelViews;
using System.Net.Mail;
using System.Data;

namespace SGS.Controllers
{
    [InitializeSimpleMembership]
    public class UsuariosController : Controller
    {

        private sgsbdEntities ctx = new sgsbdEntities();


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            viewUsuarios model = new viewUsuarios();
            model.row = new List<tablaUsuariosView>();
            using (var db = new sgsbdEntities())
            {
                var users = db.UserProfile.ToList();
                if (users.Count != 0)
                {
                    foreach (var item in users)
                    {
                        tablaUsuariosView row = new tablaUsuariosView();
                        var perfil = db.perfiles.Where(u => u.id_perfil == item.id_perfil).FirstOrDefault();
                        row.perfil = perfil.nombre;
                        row.estado = item.estado;
                        row.idUsuario = item.UserId;
                        row.nombre = item.nombre;
                        row.userName = item.UserName;
                        model.row.Add(row);
                    }
                }
            }
            return View(model);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public ActionResult Create(int idUsuario)
        {
            RegisterModel model = new RegisterModel();
            model.idUsuario = idUsuario;
            if (idUsuario != -1)
            {
                using (var db = new sgsbdEntities())
                {
                    var user = db.UserProfile.Where(u => u.UserId == idUsuario).FirstOrDefault();
                    model.UserName = user.UserName;
                    model.cr = new selectListCR();
                    model.cr.CRSelect = Convert.ToInt32(user.id_centro_responsabilidad);
                    model.un = new selectListUN();
                    model.un.UNSelect = Convert.ToInt32(user.id_unidad_negocio);
                    model.perfiles = new selectListPerfiles();
                    model.perfiles.perfilSelect = user.id_perfil;
                    model.estados = new selectListEstados();
                    model.nombre = user.nombre;
                    MembershipUser member = Membership.GetUser(model.UserName);
                    if (member.IsApproved == false) model.estados.estadoSelect = 2;
                    else model.estados.estadoSelect = 1;
                    model.email = user.email;
                }
            }
            llenaCRRegistro(model);
            llenaEstadosRegistro(model);
            llenaUNRegistro(model);
            llenaPerfilesRegistro(model);
            return View(model);
        }


        /// <summary>
        /// 
        /// </summary>
        private void buildViewBag()
        {
            ViewBag.Perfiles = new SelectList(ctx.perfiles.ToList().OrderBy(p => p.nombre).ToList(), "id_perfil", "nombre");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            UserProfile usuario = ctx.UserProfile.Where(x => x.UserId == id).FirstOrDefault();

            if (usuario.estado == 1) { ViewBag.Activo = true; } else { ViewBag.Activo = false; }
            buildViewBag();
            return View("Create", usuario);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(RegisterModel model)
        {

            if (model.Password == "" || model.Password == null)
            {
                ModelState.AddModelError("Password", "Campo Requerido");
            }
            else if (model.Password.Length < 3 || model.Password.Length > 20)
            {
                ModelState.AddModelError("Password", "Minimo 3 y Maximo 20 caracteres");
            }

            int activo;
            if (model.estados.estadoSelect == 1)
            {
                activo = 1;
            }
            else
            {
                activo = 0;
            }

            if (model.idUsuario == -1)
            {
                WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new
                {
                    estado = activo,
                    email = model.email,
                    nombre = model.nombre,
                    id_perfil = model.perfiles.perfilSelect,
                    id_unidad_negocio = model.UnN.tipos_Unidades.FirstOrDefault(),
                    id_centro_responsabilidad = model.cr.CRSelect
                });
                var tiposUnidades = model.UnN.tipos_Unidades;

                foreach (var item in tiposUnidades)
                {

                    usuariosxunidades usuario_unidad = new usuariosxunidades();
                    usuario_unidad.id_unidad = Convert.ToInt32(item);
                    //usuario_unidad.id_unidad = model.un.UNSelect;
                    usuario_unidad.id_usuario = ctx.UserProfile.Where(u => u.nombre.Equals(model.nombre)).FirstOrDefault().UserId;
                    ctx.usuariosxunidades.Add(usuario_unidad);
                    ctx.SaveChanges();
                }
            }
            else
            {
                UserProfile usuario = ctx.UserProfile.Where(u => u.UserId == model.idUsuario).FirstOrDefault();
                usuario.nombre = model.nombre;
                usuario.estado = activo;
                usuario.email = model.email;
                usuario.id_perfil = model.perfiles.perfilSelect;
                usuario.id_unidad_negocio = (model.UnN.tipos_Unidades.Where(u => u == usuario.id_unidad_negocio.ToString()).Count() > 0 ? usuario.id_unidad_negocio : Convert.ToInt32(model.UnN.tipos_Unidades.FirstOrDefault()));
                var entry = ctx.Entry(usuario);
                entry.State = EntityState.Modified;
                ctx.SaveChanges();

                List<usuariosxunidades> unidad = (from c in ctx.usuariosxunidades
                    where c.id_usuario == usuario.UserId
                    select c).ToList();
                foreach (usuariosxunidades item in unidad)
                {
                    ctx.usuariosxunidades.Remove(item);
                    ctx.SaveChanges();
                }
                //llenaUNRegistro(model);
                var seleccionados = model.UnN.tipos_Unidades;
                foreach (var item in seleccionados) {
                  
                        usuariosxunidades usuario_unidad = new usuariosxunidades();
                        //usuario_unidad.id_unidad = model.un.UNSelect;
                        usuario_unidad.id_unidad = Convert.ToInt32(item);
                        usuario_unidad.id_usuario = ctx.UserProfile.Where(u => u.nombre.Equals(model.nombre)).FirstOrDefault().UserId;
                        ctx.usuariosxunidades.Add(usuario_unidad);
                        ctx.SaveChanges();
                    //var unidades = ctx.unidades_negocio.Where(u => u.id_unidad.Equals(item));
                    //    Session.Add("unidades", new SelectList(unidades, "id_unidad", "unidad"));
                }
            }
            ActualizarUnidades(model);
            return RedirectToAction("Index", "Usuarios");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult crearPerfil(int idPerfil)
        {
            List<webpages_Roles> dbrole = ctx.webpages_Roles.ToList();

            ViewBag.modulos = ctx.modulos.ToList();//.OrderBy(u => u. );

            ViewBag.id = idPerfil;

            if (idPerfil != -1)
            {
                ViewBag.perfil = ctx.perfiles.First(o => o.id_perfil == idPerfil);
                ViewBag.permisos = ctx.webpages_UsersInRoles.Where(o => o.UserId == idPerfil).ToList();
            }

            return View(dbrole);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="modelo"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult crearPerfil(int id, FormCollection modelo)
        {
            List<webpages_UsersInRoles> roles;

            if (id == -1)
            {
                perfiles perfil = new perfiles();
                perfil.nombre = modelo["nombre"];
                ctx.perfiles.Add(perfil);
                ctx.SaveChanges();

                foreach (var item in modelo.AllKeys)
                {
                    if (item.Contains("chk"))
                    {
                        //insertar cada chk
                        webpages_UsersInRoles obj = new webpages_UsersInRoles();
                        obj.UserId = perfil.id_perfil;
                        obj.RoleId = int.Parse(item.Split('_')[1].Split('-')[0]);
                        obj.Create = true;
                        obj.Delete = true;
                        obj.Read = true;
                        obj.Update = true;
                        ctx.webpages_UsersInRoles.Add(obj);
                    }
                }
                ctx.SaveChanges();
            }
            else
            {
                //update Nombre
                perfiles perfil = ctx.perfiles.First(o => o.id_perfil == id);
                perfil.nombre = modelo["nombre"];
                ctx.SaveChanges();

                //borrar registros 
                List<webpages_UsersInRoles> list = ctx.webpages_UsersInRoles.Where(o => o.UserId == id).ToList();
                foreach (webpages_UsersInRoles obj in list)
                {
                    ctx.webpages_UsersInRoles.Remove(obj);
                    ctx.SaveChanges();
                }

                foreach (var item in modelo.AllKeys)
                {
                    if (item.Contains("chk"))
                    {
                        //insertar cada chk
                        webpages_UsersInRoles obj = new webpages_UsersInRoles();
                        obj.UserId = id;
                        //if(item == "chk_166-3")
                        //{
                        //    obj.RoleId = 136;
                        ////}
                        //else
                        //{
                            obj.RoleId = int.Parse(item.Split('_')[1].Split('-')[0]);

                        
                        obj.Create = true;
                        obj.Delete = true;
                        obj.Read = true;
                        obj.Update = true;

                        ctx.webpages_UsersInRoles.Add(obj);
                        ctx.SaveChanges();
                    }
                }

            }
            return RedirectToAction("index", "perfiles");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        public void cambiarRolesPorPerfil(int idPerfil)
        {
            using (var db = new sgsbdEntities())
            {
                var users = db.UserProfile.Where(u => u.id_perfil == idPerfil).ToList();
                foreach (var user in users)
                {
                    string[] temp = Roles.GetRolesForUser(user.UserName);
                    if (temp.Length != 0)
                    {
                        Roles.RemoveUserFromRoles(user.UserName, Roles.GetRolesForUser(user.UserName));
                    }

                    var detallePerfil = db.detalle_perfiles.Where(u => u.id_perfil == idPerfil).ToList();
                    foreach (var detalle in detallePerfil)
                    {
                        var rol = db.webpages_Roles.Where(u => u.RoleId == detalle.id_rol).FirstOrDefault();
                        Roles.AddUserToRole(user.UserName, rol.RoleName);
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult unit(usuariosxunidades model, int id)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            usuariosxunidades p = new usuariosxunidades();
            p.id_unidad = model.id_unidad;
            p.id_usuario = id;
            ctx.usuariosxunidades.Add(p);
            ctx.SaveChanges();

            return unit(id);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult unit(int id)
        {
           sgsbdEntities ctx = new sgsbdEntities();
           UnidadesNegocioRepository rs = new UnidadesNegocioRepository(ctx);
           List<UnidadesAsignadasView> _res = (from uxu in ctx.usuariosxunidades
               join u in ctx.unidades_negocio on uxu.id_unidad equals u.id_unidad
               where uxu.id_usuario == id
               select new UnidadesAsignadasView
               {
                   id_usuario = uxu.id_usuario,
                   predeterminada = uxu.predeterminada,
                   id_unidad = u.id_unidad,
                   nombre_unidad = u.unidad,
                   id_usuarioxunidad = uxu.id_usuarioxunidad
               }).OrderBy(x => x.nombre_unidad).ToList();

           int[] m = _res.Select(i => i.id_unidad).ToArray();

           var ux = ctx.unidades_negocio.Where(x => !m.
                   Contains(x.id_unidad))
               .Select(x => new
               {
                   Value = x.id_unidad,
                   Text = x.unidad
               }).ToList();

           ViewBag.asignadas = _res;
           ViewBag.unidades = new SelectList(ux, "Value", "Text");

           return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult logIn()
        {
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult deleteUnidad(int id)
        {

            sgsbdEntities ctx = new sgsbdEntities();
            usuariosxunidades unidad = (from c in ctx.usuariosxunidades
                where c.id_usuarioxunidad == id
                select c).FirstOrDefault();

            int idx = unidad.id_usuario;
            ctx.usuariosxunidades.Remove(unidad);
            ctx.SaveChanges();
            return RedirectToAction("Unit", "Usuarios", new { Id = idx });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            bool test = HttpContext.User.IsInRole("Administrador");
            return RedirectToAction("login", "usuarios");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Delete(int id)
        {
            using (var db = new sgsbdEntities())
            {
                var user = db.UserProfile.Where(u => u.UserId == id).FirstOrDefault();
                ((SimpleMembershipProvider)Membership.Provider).DeleteAccount(user.UserName); // deletes record from webpages_Membership table
                ((SimpleMembershipProvider)Membership.Provider).DeleteUser(user.UserName, true); // deletes record from UserProfile table
                db.SaveChanges();
            }
            return this.RedirectToAction("Index", "Usuarios");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Recupera(string email)
        {

            sgsbdEntities ctx = new sgsbdEntities();

            UserProfile usuario = (from e in ctx.UserProfile where e.email == email select e).FirstOrDefault();
            if (usuario == null)
            {

                return Content("INVALID");

            }

            MailMessage mm = new MailMessage();

            SmtpClient cliente = new SmtpClient("mail.datamatica.cl");
            cliente.Credentials = new System.Net.NetworkCredential("vlopez@datamatica.cl", "victor2015");
            cliente.Port = 25;

            string token = WebSecurity.GeneratePasswordResetToken(usuario.UserName);


            mm.From = new MailAddress("vlopez@datamatica.cl");
            mm.To.Add(usuario.email);
            mm.Subject = "Resiter - Recuperacion de Contrasena SGS";
            mm.Body = "el token de confirmacion es " + token + ", despues que confirme este condigo en login , su nuevo password sera Resiter2015";

            cliente.Send(mm);

            return Content("SUCCESS");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Confirma(string token)
        {
            bool pass = WebSecurity.ResetPassword(token, "Resiter2015");

            if (pass)
            {
                return Content("SUCCESS");
            }

            return Content("FAIL");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult changeUnidad()
        {
            int unidad = int.Parse(Request.Params.Get("unidades"));
            Session["unidad_seleccionada"] = unidad;
            SelectList list = (SelectList)Session["unidades"];
            Session["unidades"] = new SelectList(list.Items, "id_unidad", "unidad", SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            actualizarUnidadNegocio();
            return RedirectToAction("Index", "Clientes");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="idUser"></param>
        /// <returns></returns>
        public JsonResult nombreValido(string user, int idUser)
        {
            sgsbdEntities db = new sgsbdEntities();
            db.Configuration.ProxyCreationEnabled = false;
            if (idUser == -1)
            {
                if (db.UserProfile.Where(u => u.UserName.Equals(user)).Count() != 0)
                {
                    return Json(0, JsonRequestBehavior.AllowGet);

                }
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            return Json(1, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void llenaUNRegistro(RegisterModel model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.UnN == null)
            {
                model.UnN= new UnidadesNView();
                model.UnN.tiposUnidades = new List<SelectListItem>();
            }
            var tiposUnidades= db.unidades_negocio.OrderBy(u => u.id_unidad).ToList();
            foreach (var item in tiposUnidades) {
                var seleccionado = false;
                if (db.usuariosxunidades.Where(c => c.id_unidad == item.id_unidad && c.id_usuario == model.idUsuario).FirstOrDefault() != null)
                    seleccionado = true;

                SelectListItem temp = new SelectListItem()
                {
                    Text = item.unidad,
                    Value = item.id_unidad.ToString(),
                    Selected = seleccionado

                };
                model.UnN.tiposUnidades.Add(temp);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void llenaPerfilesRegistro(RegisterModel model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.perfiles == null)
            {
                model.perfiles = new selectListPerfiles();
            }
            model.perfiles.perfiles = db.perfiles.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.nombre,
                Value = x.id_perfil.ToString()
            });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void llenaCRRegistro(RegisterModel model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.cr == null)
            {
                model.cr = new selectListCR();
            }
            model.cr.CR = db.centro_responsabilidad.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.centro_responsabilidad1,
                Value = x.id_centro.ToString()
            });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        public void llenaEstadosRegistro(RegisterModel model)
        {
            SelectList estado = new SelectList(
                new List<Object>{ 
                    new { value = "1" , text = "Activa"  },
                    new { value = "2" , text = "Inactiva" }},
                "value",
                "text");
            if (model.estados == null)
            {
                model.estados = new selectListEstados();
                model.estados.estadoSelect = 1;
            }
            model.estados.estados = new SelectList(estado, "value", "text", model.estados.estadoSelect);
        }


        public  void ActualizarUnidades(RegisterModel model)
        {
            sgsbdEntities db = new sgsbdEntities();

            //var user = db.UserProfile.Where(u => u.UserName.Equals(model.UserName)).FirstOrDefault();

                var unidades = db.usuariosxunidades
                             .Join(db.unidades_negocio,
                                   uxu => uxu.id_unidad,
                                   u => u.id_unidad,
                                   (uxu, u) => new { unidades_negocio = u, usuariosxunidades = uxu })
                                   .Where(x => x.usuariosxunidades.id_usuario.Equals(model.idUsuario))
                                   .Select(x => x.unidades_negocio)
                                   .ToList();
                if (unidades.Count > 0)
                {


                    var all = (from w in db.webpages_UsersInRoles
                                   //join p in db.perfiles on w.UserId equals p.id_perfil
                               join m in db.webpages_Roles on w.RoleId equals m.RoleId
                               join s in db.modulos on m.modulo_id equals s.id
                               where w.UserId == model.perfiles.perfilSelect &&
                               (w.Read == true) && (w.Create == true || w.Delete == true || w.Update == true)
                               select new
                               {
                                   id_modulo = s.id,
                                   nombre_modulo = s.nombre,
                                   id = m.RoleId,
                                   nombre = m.RoleName,
                                   create = w.Create,
                                   update = w.Update,
                                   delete = w.Delete,
                                   read = w.Read
                               }
                                        ).ToList();

                    List<string> credenciales = all.GroupBy(x => x.nombre).Select(grp => grp.First()).Select(x => x.nombre).ToList();
                    var modulos = all.GroupBy(x => x.nombre_modulo).Select(grp => grp.First()).Select(s => s.nombre_modulo).ToList();
 
                    var seleccionado= db.UserProfile.Where(u => u.UserId.Equals(model.idUsuario)).Select(u => u.id_unidad_negocio).FirstOrDefault();
                    Session.Add("unidad_seleccionada", seleccionado); //unidades.First().id_unidad
                    Session.Add("unidades", new SelectList(unidades, "id_unidad", "unidad", seleccionado));
                    Session.Add("credenciales", credenciales);
                    Session.Add("modulos", modulos);
                }

        }
        public void actualizarUnidadNegocio()
        {
            using (var db = new sgsbdEntities())
            {
                var user = db.UserProfile.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
                user.id_unidad_negocio = Convert.ToInt32(Session["unidad_seleccionada"]);
                var entry = db.Entry(user);
                entry.State = EntityState.Modified;
                db.SaveChanges();
            }

        }
    }
}
