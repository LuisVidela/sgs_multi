﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using RazorPDF;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.factories;
using Bytescout.PDFExtractor;


namespace SGS.Controllers
{
    public class ReporteHistoricoReajusteController : Controller
    {

        /// <summary>
        /// Empresa: valentys
        /// Fecha : 09-07-2015
        /// Descripción: carga la vista principal con el historico.
        /// </summary>
        /// <returns></returns>
     
            public ActionResult index()
            {
                DateTime dateVerification = DateTime.Today;
                sgsbdEntities db = new sgsbdEntities();
                List<SP_SEL_HistorialPeriodo_Result> period = db.SP_SEL_HistorialPeriodo(Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList().OrderByDescending(o => o.HMCD_ANIO).ThenByDescending(o => o.HMCD_MES).ToList();
                //(from o in db.TB_HIS_MODELO_COBRO_DETALLE
                //                                  group o by new { o.HMCD_ANIO, o.HMCD_MES } into p
                //                                  select new periodosShadowBoxView
                //                                    {
                //                                        anio = p.Key.HMCD_ANIO,
                //                                        mes = p.Key.HMCD_MES
                //                                    }).OrderByDescending(o => o.anio).ThenByDescending(o => o.mes).ToList();
                List<SP_SEL_HistorialReajuste_Result> historia = new List<SP_SEL_HistorialReajuste_Result>();
                if (period.Count() > 0)
                {
                    historia = db.SP_SEL_HistorialReajuste(period[0].HMCD_MES, period[0].HMCD_ANIO, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();
                }
                ViewBag.period = period;
                return View("index", historia);
            }
 

        /// <summary>
        /// Empresa: valentys
        /// Fecha: 09-07-2015
        /// Descripción: simula un reajuste según los parametros enviados
        /// </summary>
        /// <returns></returns>
        public ActionResult buscar(Int32 mes, Int32 anio)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_HistorialReajuste_Result> historia = new List<SP_SEL_HistorialReajuste_Result>();
            if (mes == -1 && anio == -1)
            {
                historia = db.SP_SEL_HistorialReajuste(null, null, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();
            }
            else
            {
                historia = db.SP_SEL_HistorialReajuste(mes, anio, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();
            }
            return View("listHistorico", historia);
        }

        /// <summary>
        /// Empresa: valentys
        /// Fecha: 09-07-2015
        /// Descripción: Obtiene el historial de reajustes.
        /// </summary>
        /// <param name="codigo">codigo deldetalle de cobro</param>
        /// <returns></returns>
        public ActionResult historial(Int32 codigo)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<TB_HIS_MODELO_COBRO_DETALLE> historia = (from o in db.TB_HIS_MODELO_COBRO_DETALLE
                                                          where o.HMCD_ID_DETALLE_MODELO_COBRO == codigo
                                                          select o).ToList();
            return View(historia);
        }

        /// <summary>
        /// Empresas: Valentys
        /// Fecha: 09-07-2015
        /// Descripción: Obtiene html del mensaje de confirmación
        /// </summary>
        /// <param name="mensaje">Mensaje que se visualizará en la confirmación</param>
        /// <returns></returns>
        public ActionResult confirm(String mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View("confirm");
        }

        /// <summary>
        /// Empresa: Valentys
        /// Fecha: 09-07-2015
        /// Descripcíon: reversa un reajuste.
        /// </summary>
        /// <param name="mes"> mes de reverso</param>
        /// <param name="anio">año de reverso</param>
        /// <returns></returns>
        public JsonResult reversar(Int32 mes, Int32 anio)
        {
            sgsbdEntities db = new sgsbdEntities();
            Int32 result = db.SP_UPD_ReversarReajuste(mes, anio, Models.repository.UsuariosRepository.getUnidadSeleccionada());
            return Json(result);
        }

    }

}
