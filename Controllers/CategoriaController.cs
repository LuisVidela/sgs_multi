﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class CategoriaController : Controller
    {
        //
        // GET: /Categoria/

        public ActionResult Index()
        {
            CategoriaSucursalRepository rs = new CategoriaSucursalRepository(new sgsbdEntities());
            List<categoria_sucursal> data = (List<categoria_sucursal>)rs.GetCategoriaSucursal();
            return View(data);
        }



        public ActionResult create()
        {
            this.buildViewBagList();

            return View();
        }

        [HttpPost]
        public ActionResult create(categoria_sucursal model)
        {

            if (ModelState.IsValid)
            {

                CategoriaSucursalRepository rs = new CategoriaSucursalRepository(new sgsbdEntities());

                if (model.id_categoria == 0)
                {
                    rs.InsertCategoriaSucursal(model);
                }
                else
                {
                    rs.UpdateCategoriaSucursal(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Categoria");

            }

            //this.buildViewBagList();
            return View(model);
        }

        public ActionResult edit(int id)
        {


            CategoriaSucursalRepository rs = new CategoriaSucursalRepository(new sgsbdEntities());
            categoria_sucursal c = rs.GetCategoriaSucursalByID(id);

            this.buildViewBagList();
            return View("create", c);
        }

        //public ActionResult Delete(int id)
        //{
        //    ComunasRepository rs = new ComunasRepository(new sgsbdEntities());
        //    rs.DeleteComuna(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "Comunas");

        //}


       private void buildViewBagList()
       {

            IEnumerable<object> rsestado = new object[] {
             new { text="Activo",value="1"},
             new { text="Inactivo",value="2"}
            };

            ViewBag.estado = new SelectList(rsestado, "value", "text");
        }

    }
}
