﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.modelViews;
 
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class ProcesoPagosController : Controller
    {
        //
        // GET: /ProcesoCobros/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Datos()
        {
           
            using (var ctx = new sgsbdEntities())
            {
                //String sql = "SELECT distinct t0.razon_social,t0.id_franquiciado,t6.tipo_servicio,t1.camion,sum(t5.valor_tarifa) as 'Monto Calculado'"
                //      + "FROM franquiciado t0 inner join camiones t1 on t0.id_franquiciado = t1.id_franquiciado"
                //      +  "inner join hoja_ruta t2 on t2.id_camion = t1.id_camion"
                //      +  "inner join hoja_ruta_detalle t3 on t3.id_hoja_ruta = t2.id_hoja_ruta"
                //      +  "inner join puntos_servicio t4 on t4.id_punto_servicio = t3.id_punto_servicio"
                //      +  "inner join tipo_tarifa  t5 on t4.id_tipo_tarifa = t5.id_tipo_tarifa"
                //      +  "left join tipo_servicio t6 on t6.id_tipo_servicio = t3.id_tipo_servicio"
                //      + "group by t0.id_franquiciado,t0.razon_social,t1.camion,t6.tipo_servicio";
                List<ProcesoPagoView> procPagos = new List<ProcesoPagoView>();
                
                sgsbdEntities d = new sgsbdEntities();
                
                 var query = (from t0 in d.franquiciado
                 join t1 in d.camiones on t0.id_franquiciado equals t1.id_franquiciado
                              join t2 in d.hoja_ruta on new { Id_camion = t1.id_camion } equals new { Id_camion = (int)(t2.id_camion) }
                 join t3 in d.hoja_ruta_detalle on t2.id_hoja_ruta equals t3.id_hoja_ruta
                 join t4 in d.puntos_servicio on t3.id_punto_servicio equals t4.id_punto_servicio
                 join t7 in d.tipo_tarifa on t4.id_tipo_tarifa equals t7.id_tipo_tarifa

                              join t6 in d.tipo_servicio on new { Id_tipo_servicio = (int)(t3.id_tipo_servicio) } equals new { Id_tipo_servicio = t6.id_tipo_servicio } into t6_join
                 from t6 in t6_join.DefaultIfEmpty()
                 group new { t0, t1, t6 } by new
                 {
                     t0.id_franquiciado,
                     t0.razon_social,
                     t1.camion,
                     t6.tipo_servicio1
                 } into g
                 select new
                 {
                     
                    razon1 = g.Key.razon_social,
                     Id_franquiciado = (int?)g.Key.id_franquiciado,
                     Tipo_servicio1 = g.Key.tipo_servicio1,
                     g.Key.camion//,
                    // Monto_Calculado = (double?)g.Sum(p => p.t7.Tipo_tarifa.Valor_tarifa)
                 }).Distinct();

                 foreach (var m in query)
                 {
                     ProcesoPagoView pago = new ProcesoPagoView();
                     pago.razon_social = m.razon1;
                     pago.tipo_servicio = m.Tipo_servicio1;
                     pago.camion = m.camion;
                     procPagos.Add(pago);
                 }
                
               
                
                return View("index",procPagos);
            }
            
        }


    }
}
