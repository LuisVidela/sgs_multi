﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
//using RazorPDF;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.factories;
using Bytescout.PDFExtractor;


namespace SGS.Controllers
{
    public class ReajusteController : Controller
    {
        //
        // GET: /CobroClientes/
        /// <summary>
        /// Empresa: valentys
        /// Fecha : 08-07-2015
        /// Descripción: carga la vista principal con la simulación de resjuste del mes en curso.
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {

            DateTime dateVerification = DateTime.Today;
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_VerificarReajeste_Result> preReajuste = db.SP_SEL_VerificarReajeste(dateVerification.Month, dateVerification.Year, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();
            return View("index", preReajuste);
        }
        /// <summary>
        /// Empresa: valentys
        /// Fecha: 08-07-2015
        /// Descripción: simula un reajuste según los parametros enviados
        /// </summary>
        /// <returns></returns>
        public ActionResult simulate(Int32 mes, Int32 anio)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_VerificarReajeste_Result> preReajuste = db.SP_SEL_VerificarReajeste(mes, anio, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();
            return View("simulation", preReajuste);
        }

        public ActionResult excel(Int32 mes, Int32 anio)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<SP_SEL_VerificarReajeste_Result> excel = db.SP_SEL_VerificarReajeste(mes, anio, Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View(excel);
        }

        /// <summary>
        /// Empresa: valentys
        /// Fecha: 08-07-2015
        /// Descripción: Obtiene el historial de reajustes.
        /// </summary>
        /// <param name="codigo">codigo deldetalle de cobro</param>
        /// <returns></returns>
        public ActionResult historial(Int32 codigo)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<TB_HIS_MODELO_COBRO_DETALLE> historia = (from o in db.TB_HIS_MODELO_COBRO_DETALLE
                                                             where o.HMCD_ID_DETALLE_MODELO_COBRO == codigo
                                                             select o).OrderByDescending(o => o.HMCD_ANIO).ThenByDescending(o => o.HMCD_MES).ToList();
            return View(historia);
        }
        
        /// <summary>
        /// Empresas: Valentys
        /// Fecha: 08-07-2015
        /// Descripción: Obtiene html del mensaje de confirmación
        /// </summary>
        /// <param name="mensaje">Mensaje que se visualizará en la confirmación</param>
        /// <returns></returns>
        public ActionResult confirm(String mensaje)
        {
            ViewBag.Mensaje = mensaje;
            return View("confirm");
        }

        /// <summary>
        /// Empresa: Valentys
        /// Fecha: 08-07-2015
        /// Descripcíon: 
        /// </summary>
        /// <param name="mes"> mes de proceso</param>
        /// <param name="anio">año de proceso</param>
        /// <returns></returns>
        public JsonResult process(Int32 mes, Int32 anio)
        {
            sgsbdEntities db = new sgsbdEntities();
            Int32 result = db.SP_UPD_Reajeste(null, mes, anio, Models.repository.UsuariosRepository.getUnidadSeleccionada());
            return Json(result);
        }
        ///////////////////////////////////////////////////////////////////////////////////
    }// SECCION NAMESPACE

}
