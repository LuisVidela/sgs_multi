﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class UnidadesNView
    {
        public IEnumerable<string> tipos_Unidades{ get; set; }
        public List<SelectListItem> tiposUnidades { get; set; }
    }

}
