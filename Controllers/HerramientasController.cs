﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;

namespace SGS.Controllers
{
    public class HerramientasController : Controller
    {
        //
        // GET: /Herramientas/

        public ActionResult Index()
        {
            HerramientasRepository rs = new HerramientasRepository(new sgsbdEntities());
            List<HerramientasView> data = (List<HerramientasView>)rs.GetHerramientas2();

            return View(data.OrderBy(h=>h.herramienta));
        }
        public ActionResult excel()
        {

            HerramientasRepository rs = new HerramientasRepository(new sgsbdEntities());
            List<HerramientasView> data = (List<HerramientasView>)rs.GetHerramientas2();

            return View(data.OrderBy(h => h.herramienta));
        }
        public ActionResult create()
        {
            this.buildViewBagList();
            return View();
        }


        [HttpPost]
        public ActionResult create(herramientas model)
        {

            if (ModelState.IsValid)
            {

                HerramientasRepository rs = new HerramientasRepository(new sgsbdEntities());

                if (model.id_herramienta == 0)
                {
                    rs.InsertHerramienta(model);
                }
                else
                {
                    rs.UpdateHerramienta(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Herramientas");

            }

            this.buildViewBagList();
            return View(model);
        }


        public ActionResult edit(int id)
        {

            HerramientasRepository rs = new HerramientasRepository(new sgsbdEntities());
            herramientas c = rs.GetHerramientaByID(id);
            this.buildViewBagList();
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            HerramientasRepository rs = new HerramientasRepository(new sgsbdEntities());
            rs.DeleteHerramienta(id);
            rs.Save();
            return this.RedirectToAction("Index", "Herramientas");

        }


        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            CamionesRepository rs = new CamionesRepository(ctx);

            ViewBag.camiones = new SelectList(rs.getCamionesForList(),"value","text");

        }

    }
}
