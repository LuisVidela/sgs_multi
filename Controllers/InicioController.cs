﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Data.Objects.SqlClient;


namespace SGS.Controllers
{
    public class InicioController : Controller
    {
        //
        // GET: /Inicio/

        public ActionResult Index()
        {

            try
            {
                sgsbdEntities db = new sgsbdEntities();
                List<sp_dashboard_Result> data = db.sp_dashboard().ToList();
                var datosGraficos = db.sp_cantidad_por_categoria_clientes().ToArray();

                List<sp_cantidad_por_categoria_clientes_Result> MisDatos = db.sp_cantidad_por_categoria_clientes().ToList();
                ViewBag.MisDatos = MisDatos;

                return View(data);

            }
            catch (Exception ex)
            {
                throw ex;
            }



        }

        public ActionResult view()
        {

            return View();
        }

        [HttpPost]
        public string getDetalleCateg(string Categoria)
        {
            System.Text.Encoding utf_8 = System.Text.Encoding.UTF8;
            using (var db = new sgsbdEntities())
            {
                List<sp_cliente_numero_contrato_Result> MisDatos = db.sp_cliente_numero_contrato(Categoria).ToList();
                var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(MisDatos);

                return jsonString;
            }
        }



    }
}
