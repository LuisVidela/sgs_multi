﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;


namespace SGS.Controllers
{
    public class CamionesController : Controller
    {
        //
        // GET: /Documentos/



        public ActionResult Index()
        {
            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            //List<camiones> data = (List<camiones>)rs.getCamionesWithAll2();

            List<CamionesView> dot = (List<CamionesView>)rs.getCamionesWithAll32(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            //List<CamionesView> dot = (List<CamionesView>)rs.getCamionesWithAll32();


            return View(dot);
        }


        public ActionResult Create(int id)
        {
            //if (id == 0)
            //{
            //    string encabe = "Agregar Nuevo Camión";
            //    ViewBag.Encabezado = encabe;
            //}
            this.buildViewBagList();

            return View();
        }

        [HttpPost]
        public ActionResult Create(camiones model)
        {

            if (ModelState.IsValid)
            {
               
                CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
                if (model.id_camion == 0)
                {
                    rs.InsertCamion(model);
                }
                else
                {
                    rs.UpdateCamion(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Camiones"); ;
            }
            this.buildViewBagList();
            return View(model);

        }






        public ActionResult edit(int id)
        {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            camiones c = rs.GetCamionByID(id);
            ViewBag.estado_camion = c.estado;
            this.buildViewBagList();
            return View("create", c);
        }

        public ActionResult view(int id)
        {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            camionesShadowBoxView c = rs.GetCamionShadowBox(id);
           
            return View(c);
        }


        public ActionResult Delete(int id)
        {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            rs.DeleteCamion(id);
            rs.Save();
            return this.RedirectToAction("Index", "Camiones");

        }

        public ActionResult print(int id)
        {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            camionesShadowBoxView c = rs.GetCamionShadowBox(id);
            ViewBag.print = true;
            return View("view",c);
        }

        public ActionResult restrict(int id) {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            camionesShadowBoxView data = rs.GetCamionShadowBox(id);
            var restricciones = data.restricciones.ToList();
            List<restriccion_camion_cliente> temp= new List<restriccion_camion_cliente>();
            foreach(var item in restricciones)
            {
                using(var db = new sgsbdEntities())
                {
                    var restriccion = db.restriccion_camion_cliente.Where(u => u.id_restriccion == item.id_restriccion).FirstOrDefault();
                    var cliente = db.clientes.Where(u => u.id_cliente == restriccion.id_cliente).FirstOrDefault();
                    var camion = db.camiones.Where(u => u.id_camion == restriccion.id_camion).FirstOrDefault();
                    string nombre = cliente.razon_social;
                    restriccion.cliente = nombre;
                    temp.Add(restriccion);
                }
            }
            ViewBag.restricciones = temp;
            ViewBag.id_camion = id;

            CamionesRepository cam = new CamionesRepository(new sgsbdEntities());
            camiones what = cam.GetCamionByID(id);
            ViewBag.existe = "visible";
            ViewBag.nuevo = "hidden";
            ViewBag.TitleIs = "Camión Patente " + what.patente.ToString();

            this.buildViewBagList2();
            return View();

        }


        public ActionResult deleteRestriction(int id) {

            RestriccionClienteCamionRepository rs = new RestriccionClienteCamionRepository(new sgsbdEntities());
            rs.DeleteRestriccionClienteCamion(id);
            rs.Save();
            return this.RedirectToAction("Index","Camiones");
        
        }

        [HttpPost]
        public ActionResult restrict(restriccion_camion_cliente model,string id)
        {
            RestriccionClienteCamionRepository rs = new RestriccionClienteCamionRepository(new sgsbdEntities());
            model.id_camion = Int32.Parse(id);
            rs.InsertRestriccionClienteCamion(model);
            rs.Save();

            CamionesRepository cam = new CamionesRepository(new sgsbdEntities());
            camiones camion = cam.GetCamionByID(model.id_camion);
            ViewBag.TitleIs = "Camión Patente " + camion.patente.ToString();
            ViewBag.existe = "visible";
            ViewBag.nuevo = "hidden";
            ViewBag.TitleIs = "";

            return this.RedirectToAction("Restrict", "Camiones", new { id = Int32.Parse(id) }); 
        }

        public ActionResult excel() {

            CamionesRepository rs = new CamionesRepository(new sgsbdEntities());
            List<CamionesExcelView>data=(List<CamionesExcelView>)rs.getCamionesForExcel();

            return View(data);
        }
              


        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();
            CamionesMarcasRepository rs1 = new CamionesMarcasRepository(ctx);
            FranquiciadosRepository rs2 = new FranquiciadosRepository(ctx);
            TipoServicioRepository rs3 = new TipoServicioRepository(ctx);
            IEnumerable<object> estado = new object[] {
             new { text="Inactivo",value="Inactivo"},
             new { text="Activo",value="Activo"},
             new { text="En Mantencion",value="En Mantencion"}
            };

            ViewBag.Marcas = new SelectList(rs1.GetCamionesMarcas(), "id_camion_marca", "marca");
            ViewBag.Franquiciados = new SelectList(rs2.GetFranquiciadoForList2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "id_franquiciado", "razon_social");
            ViewBag.TiposServicios = new SelectList(rs3.GetTiposServicios(), "id_tipo_servicio", "tipo_servicio1");
            ViewBag.estado = new SelectList(estado, "value", "text",ViewBag.estado_camion);
        }



        private void buildViewBagList2() {

            sgsbdEntities ctx = new sgsbdEntities();
            ClientesRepository rs = new ClientesRepository(ctx);

            ViewBag.clientes2 = new SelectList(rs.GetClientesForList2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada())
                , "id_cliente", "razon_social");
        }
    }

}
