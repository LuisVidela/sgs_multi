﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models.modelViews;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.ShadowBox;
using System.Text;

namespace SGS.Controllers
{
    public class SucursalesController : Controller
    {
        //
        // GET: /Sucursales/
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
            List<SucursalesView> data = (List<SucursalesView>)rs.GetSucursalesView(UsuariosRepository.getUnidadSeleccionada());

            return View(data.OrderBy(m => m.id_sucursal).ToList());
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult view(int id)
        {
            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
            sucursalesShadowBoxView data = rs.getSucursalShadowBox(id);

            return View(data);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult create()
        {
            buildViewBagList();
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="numero_direccion"></param>
        /// <param name="direccion"></param>
        /// <param name="tipo"></param>
        /// <param name="desde"></param>
        /// <param name="hasta"></param>
        /// <param name="zonas"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult create(sucursales model, string numero_direccion, string direccion, string tipo, string desde, string hasta, string zonas, string estado)
        {
            if (numero_direccion.Equals(""))
            {
                ModelState.AddModelError("direccion", "Se requieren todos los campos de la direccion");
            }

            if (ModelState.IsValid)
            {
                SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());

                model.direccion = tipo.Trim() + " " + direccion.Trim() + " " + numero_direccion.Trim();
                model.periodo_desde = Int32.Parse(desde);
                model.periodo_hasta = Int32.Parse(hasta);
                model.zona = zonas;
                model.estado_sucursal = estado;
               

                int existe;
                if (model.id_sucursal == 0)
                {
                    rs.InsertSucursal(model);
                    existe = 1;
                }
                else
                {
                    rs.UpdateSucursal(model);
                    existe = 0;
                }

                try
                {
                    rs.Save();
                }
                catch (Exception e)
                {
                    if (existe == 1)
                    {
                        ViewBag.Error = "EXISTE";
                    }
                    else
                    {
                        ViewBag.Error = "OTRO";
                    }

                    buildViewBagList();
                    return View(model);
                }

                return RedirectToAction("Index", "Sucursales");
            }

            this.buildViewBagList();
            return View(model);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult createflow(int id)
        {
            this.buildViewBagList();

            ClientesRepository rscc = new ClientesRepository(new sgsbdEntities());
            clientes clie = rscc.GetClienteByID(id);

            ViewBag.ClieName = clie.razon_social.Trim();

            SucursalesRepository rsc = new SucursalesRepository(new sgsbdEntities());
            sucursales sucu = new sucursales
            {
                id_cliente = id
            };

            return View(sucu);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="numero_direccion"></param>
        /// <param name="direccion"></param>
        /// <param name="tipo"></param>
        /// <param name="desde"></param>
        /// <param name="hasta"></param>
        /// <param name="zonas"></param>
        /// <param name="estado"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult createflow(sucursales model, string numero_direccion, string direccion, string tipo, string desde, string hasta, string zonas, string estado)
        {
            if (numero_direccion.Equals(""))
            {
                ModelState.AddModelError("direccion", "Se requieren todos los campos de la direccion");
            }

            if (ModelState.IsValid)
            {
                SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());

                model.direccion = tipo.Trim() + " " + direccion.Trim() + " " + numero_direccion.Trim();
                model.periodo_desde = (double)Int32.Parse(desde);
                model.periodo_hasta = (double)Int32.Parse(hasta);
                model.zona = zonas;
                model.estado_sucursal = estado;
                
                int existe;
                if (model.id_sucursal == 0)
                {
                    rs.InsertSucursal(model);
                    existe = 1;
                }
                else
                {
                    rs.UpdateSucursal(model);
                    existe = 0;
                }

                try
                {
                    rs.Save();
                }
                catch (Exception e)
                {
                    if (existe == 1)
                    {
                        ViewBag.Error = "EXISTE";
                    }
                    else
                    {
                        ViewBag.Error = "OTRO";
                    }

                    this.buildViewBagList();
                    return View(model);
                }

                ViewBag.existe = "hidden";
                @ViewBag.nuevo = "visible";
                return RedirectToAction("createflow", "Contratos", new { id = model.id_sucursal });
            }

            this.buildViewBagList();
            return View(model);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult edit(int id)
        {
            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
            sucursales s = rs.GetSucursalByID(id);
            if (!string.IsNullOrEmpty(s.direccion))
            {
                string[] split = s.direccion.Split(null, 2);

                //para sacar la direccion y el numero de direccion
                StringBuilder sb = new StringBuilder();
                string[] split2 = split[1].Split(null);
                int count = split2.Count();

                for (int i = 0; i < count - 1; i++)
                {
                    sb.Append(" " + split2[i]);
                }

                ViewBag.tipo = split[0];
                ViewBag.direccion = sb.ToString().Trim();
                ViewBag.numero_direccion = split2[count - 1];
            }

            ViewBag._estado = s.estado_sucursal;
            ViewBag._zonas = s.zona;

            if (s.periodo_desde != null) { ViewBag._desde = (int)s.periodo_desde; } else { ViewBag._desde = 0; }
            if (s.periodo_hasta != null) { ViewBag._hasta = (int)s.periodo_hasta; } else { ViewBag._hasta = 0; }


            this.buildViewBagList();
            return View("create", s);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public ActionResult Delete(int id)
        //{
        //    SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
        //    rs.DeleteSucursal(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "Sucursales");
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult excel()
        {
            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
            List<SucursalesView> data = (List<SucursalesView>)rs.GetSucursalesView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }


        /// <summary>
        /// 
        /// </summary>
        private void buildViewBagList()
        {
            //MODELO cliente pais ciudad region comuna rubro 
            //MANO zona estado periodo_desde periodo_hasta

            sgsbdEntities ctx = new sgsbdEntities();
            ClientesRepository rs1 = new ClientesRepository(ctx);
            PaisesRepository rs2 = new PaisesRepository(ctx);
            CiudadesRepository rs3 = new CiudadesRepository(ctx);
            RegionesRepository rs4 = new RegionesRepository(ctx);
            ComunasRepository rs5 = new ComunasRepository(ctx);
            RubrosRepository rs6 = new RubrosRepository(ctx);
            CentroResponsabilidadRepository cr = new CentroResponsabilidadRepository(ctx);
            ZonasRepository zr = new ZonasRepository(ctx);
            CategoriaSucursalRepository ct = new CategoriaSucursalRepository(ctx);

            IEnumerable<object> rsestado = new object[] {
             new { text="Activa",value="Activa"},
             new { text="Suspendida",value="Suspendida"}
            };

            IEnumerable<object> rstipo = new object[] {
             new { text="Avenida",value="Avenida"},
             new { text="Calle",value="Calle"},
             new { text="Pasaje",value="Pasaje"}
            };

            IEnumerable<int> rsdesde = Enumerable.Range(1, 31);
            IEnumerable<int> rshasta = Enumerable.Range(1, 31);

            ViewBag.clientes = new SelectList(rs1.GetClientesForList2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "id_cliente", "razon_social");
            ViewBag.paises = new SelectList(rs2.GetPaises().OrderBy(x => x.pais), "id_pais", "pais");
            ViewBag.Ciudades = new SelectList(rs3.GetCiudades().OrderBy(m => m.ciudad), "id_ciudad", "ciudad");
            ViewBag.Regiones = new SelectList(rs4.GetRegiones().OrderBy(m => m.nombre_region), "id_region", "nombre_region");
            ViewBag.Comunas = new SelectList(rs5.GetComunas().OrderBy(m => m.nombre_comuna), "id_comuna", "nombre_comuna");
            ViewBag.Rubros = new SelectList(rs6.Getrubros().OrderBy(m => m.rubro), "id_rubro", "rubro");
            ViewBag.CentroResponsabilidad = new SelectList(cr.GetCentroResponsabilidadForList(), "value", "text");
            ViewBag.zonas = new SelectList(zr.GetZonas().OrderBy(m => m.nombre_zona), "id_zona", "nombre_zona");
            ViewBag.categoria = new SelectList(ct.GetCategoriaSucursal().OrderBy(m => m.categoria), "id_categoria", "categoria");

            ViewBag.estado = new SelectList(rsestado, "value", "text", ViewBag._estado);
            ViewBag.tipo = new SelectList(rstipo, "value", "text", ViewBag._tipo);

            ViewBag.desde = new SelectList(rsdesde, ViewBag._desde);
            ViewBag.hasta = new SelectList(rshasta, ViewBag._hasta);
        }
    }
}
