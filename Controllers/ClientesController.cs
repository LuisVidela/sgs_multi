﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models;
using System.Text;
using SGS.Models.ShadowBox;

namespace SGS.Controllers
{
    public class ClientesController : Controller
    {
        //
        // GET: /Clientes/

        private ClientesRepository rs;


        public ActionResult Index()
        {
            this.rs = new ClientesRepository(new sgsbdEntities());
            List<ClientesView> data = (List<ClientesView>)rs.GetClientesView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());


            return View(data);
        }



        public ActionResult Create()
        {
            this.buildViewBagList();

            return View();

        }

        [HttpPost]
        public ActionResult Create(clientes model, string numero_direccion, string direccion, string tipo)
        {
            int nuevo = 0;

            if (numero_direccion.Equals("") || direccion.Equals(""))
            {
                ModelState.AddModelError("direccion", "Se requieren todos los campos de la direccion");
            }


            if (ModelState.IsValid)
            {

                model.direccion_cliente = tipo.Trim() + " " + direccion.Trim() + " " + numero_direccion.Trim();
    

                ClientesRepository rs = new ClientesRepository(new sgsbdEntities());
                if (model.id_cliente == 0)
                {
                    model.id_centro_costo = 1;
                    rs.InsertCliente(model);
 
                }
                else
                {
                    model.id_centro_costo = 1;
                    rs.UpdateCliente(model);
                    nuevo = 0;
                }

                try
                {
                    rs.Save();

                }
                catch (Exception e)
                {
                 
                   ViewBag.Error = "EXISTE";
                   this.buildViewBagList();
                   return View(model);
                }

                if (nuevo == 0)
                {
                    return this.RedirectToAction("Index", "Clientes");
                }
                else
                {
                    return RedirectToAction("createflow", "Sucursales", new { id = model.id_cliente });
                }

            }
            this.buildViewBagList();
            return View(model);
        }



        public ActionResult edit(int id)
        {

            ClientesRepository rs = new ClientesRepository(new sgsbdEntities());
            clientes c = rs.GetClienteByID(id);
            if (c.direccion_cliente != null && c.direccion_cliente != "")
            {
                string[] split = c.direccion_cliente.Split(null, 2);

                //para sacar la direccion y el numero de direccion
                StringBuilder sb = new StringBuilder();
                string[] split2 = split[1].Split(null);
                int _count = split2.Count();

                for (int i = 0; i < _count - 1; i++)
                {
                    sb.Append(" " + split2[i]);
                }

                ViewBag.tipo = split[0];
                ViewBag.direccion = sb.ToString().Trim();
                ViewBag.numero_direccion = split2[_count - 1];

  
            }

            this.buildViewBagList();
            return View("create", c);
        }


        public ActionResult view(int id)
        {
            ClientesRepository rs = new ClientesRepository(new sgsbdEntities());

            clientesShadowBoxView c = rs.GetClienteShadowBox(id);
            return View(c);

        }

        
        public ActionResult print(int id) {

            ClientesRepository rs = new ClientesRepository(new sgsbdEntities());
            clientesShadowBoxView c = rs.GetClienteShadowBox(id);

            ViewBag.print=true;
            return View("view",c);
        
        }


        //public ActionResult Delete(int id)
        //{

        //    this.rs = new ClientesRepository(new sgsbdEntities());
        //    rs.DeleteCliente(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "Clientes");

        //}


        public ActionResult excel()
        {
            ClientesRepository rs = new ClientesRepository(new sgsbdEntities());


            return View(rs.GetClientesExcel());

        }

        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            PaisesRepository rs1 = new PaisesRepository(ctx);
            ComunasRepository rs2 = new ComunasRepository(ctx);
            CiudadesRepository rs3 = new CiudadesRepository(ctx);
            RegionesRepository rs4 = new RegionesRepository(ctx);
            UnidadesNegocioRepository rs5 = new UnidadesNegocioRepository(ctx);
            RubrosRepository rs6 = new RubrosRepository(ctx);
            CategoriaRepository rs7 = new CategoriaRepository(ctx);

            IEnumerable<object> tipo = new object[] {
             new { text="Avenida",value="Avenida"},
             new { text="Calle",value="Calle"},
             new { text="Pasaje",value="Pasaje"}
            };



            ViewBag.Paises = new SelectList(rs1.GetPaises().OrderBy(m => m.pais), "id_pais", "pais");
            ViewBag.Comunas = new SelectList(rs2.GetComunas().OrderBy(m => m.nombre_comuna), "id_comuna", "nombre_comuna");
            ViewBag.Ciudades = new SelectList(rs3.GetCiudades().OrderBy(m => m.ciudad), "id_ciudad", "ciudad");
            ViewBag.Regiones = new SelectList(rs4.GetRegiones().OrderBy(m => m.id_region), "id_region", "nombre_region");
            ViewBag.Rubros = new SelectList(rs6.Getrubros().OrderBy(m => m.id_rubro), "id_rubro", "rubro");
            ViewBag.tipo = new SelectList(tipo, "value", "text", ViewBag.tipo);
            ViewBag.categoria = new SelectList(rs7.GetCategoria().OrderBy(m => m.id_categoria_).DefaultIfEmpty(), "id_categoria_", "categoria_nombre");

        }

    }
}
