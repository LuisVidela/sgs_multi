﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class RubrosController : Controller
    {
        //
        // GET: /Rubros/

        public ActionResult Index()
        {
            RubrosRepository rs = new RubrosRepository(new sgsbdEntities());
            List<rubros> data = (List<rubros>)rs.Getrubros();

            return View(data);
        }

        public ActionResult create()
        {
            return View();
        }
        public ActionResult excel()
        {

            RubrosRepository rs = new RubrosRepository(new sgsbdEntities());
            List<rubros> data = (List<rubros>)rs.Getrubros();

            return View(data);
        }

        [HttpPost]
        public ActionResult create(rubros model)
        {

            if (ModelState.IsValid)
            {

                RubrosRepository rs = new RubrosRepository(new sgsbdEntities());

                if (model.id_rubro == 0)
                {
                    rs.InsertRubro(model);
                }
                else
                {
                    rs.UpdateRubro(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Rubros");

            }

            return View(model);
        }

        public ActionResult edit(int id)
        {

            RubrosRepository rs = new RubrosRepository(new sgsbdEntities());
            rubros c = rs.GetRubroByID(id);
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            RubrosRepository rs = new RubrosRepository(new sgsbdEntities());
            rs.DeleteRubro(id);
            rs.Save();
            return this.RedirectToAction("Index", "Rubros");

        }

    }
}
