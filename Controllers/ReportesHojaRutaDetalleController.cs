﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models;
using SGS.Models.connector;
using System.Data.Objects.SqlClient;

namespace SGS.Controllers
{
    public class ReportesHojaRutaDetalleController : Controller
    {
        //
        // GET: /ReportesCostosFranc2/

        public ActionResult Index()
        {

            this.buildingViewBag();
            return View();
        }


        public ActionResult List()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            var query = ctx.hoja_ruta_detalle.GroupJoin(ctx.hoja_ruta,
                                                        x => x.id_hoja_ruta,
                                                        hr => hr.id_hoja_ruta,
                                                        (x, hr) => new { x = x, hr = hr.DefaultIfEmpty() })
                                                        .SelectMany(b => b.hr.Select(r => new { hoja_ruta_detalle = b.x, hoja_ruta = r }))
                                                           .GroupJoin(ctx.conductores,
                                                                      a1 => a1.hoja_ruta.id_conductor,
                                                                      a2 => a2.id_conductor,
                                                                      (a1, a2) => new { a1 = a1, a2 = a2.DefaultIfEmpty() })
                                                                      .SelectMany(b => b.a2.Select(c => new { conductor = c, hojas_ruta = b.a1 }))
                                                                      .GroupJoin(ctx.tipo_servicio,
                                                                                 s => s.hojas_ruta.hoja_ruta_detalle.id_tipo_servicio,
                                                                                 ts => ts.id_tipo_servicio,
                                                                                 (s, ts) => new { s = s, ts = ts.DefaultIfEmpty() })
                                                                                 .SelectMany(sm => sm.ts.Select(c => new { tipo_serv = c, container = sm }))
                                                                                 .GroupJoin(ctx.puntos_servicio,
                                                                                             x => x.container.s.hojas_ruta.hoja_ruta_detalle.id_punto_servicio,
                                                                                             s => s.id_punto_servicio,
                                                                                             (x, s) => new { x = x, s = s.DefaultIfEmpty() })
                                                                                             .SelectMany(b => b.s.Select(c => new { punto_servicios = c, demas = b.x }))
                                                                                             .GroupJoin(ctx.contratos,
                                                                                                        x => x.punto_servicios.id_contrato,
                                                                                                        c => c.id_contrato,
                                                                                                        (x, c) => new { x = x, c = c.DefaultIfEmpty() })
                                                                                                        .SelectMany(sm => sm.c.Select(x => new { contratos=x,demas=sm }))
                                                                                             .GroupJoin(ctx.franquiciado,
                                                                                                         x => x.demas.x.demas.container.s.conductor.id_franquiciado,
                                                                                                         c => c.id_franquiciado,
                                                                                                         (x, c) => new { x = x, c = c.DefaultIfEmpty() }).SelectMany(b => b.c.Select(s => new { franqui = s, demas = b }));

            string fecha_inicial = Request.Params.Get("fecha_inicial");
            string fecha_final = Request.Params.Get("fecha_final");
            string punto_servicio = Request.Params.Get("punto_servicio");
            string sistema = Request.Params.Get("sistema");
            string estado = Request.Params.Get("estado");
            string franquiciado = Request.Params.Get("franquiciado");
            string excel = Request.Params.Get("excel");

            string SEcho = Request.Params.Get("sEcho");
            string IDisplayStart = Request.Params.Get("IdisplayStart");
            string IDisplayLength = Request.Params.Get("IDisplayLength");
            int sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            string sortDirection = Request["sSortDir_0"];

            int IDisplayStart_int = 0;
            int IDisplayLength_int = 0;
            int punto_servicio_int = int.Parse(punto_servicio);


            if (IDisplayLength != "" && IDisplayLength != null)
            {
                IDisplayStart_int = int.Parse(IDisplayStart);
                IDisplayLength_int = int.Parse(IDisplayLength);

            }


            if (fecha_inicial.Trim() != "" || fecha_final != "")
            {
                if (fecha_inicial != "")
                {
                    string[] fecha1 = fecha_inicial.Split('/');
                    DateTime t1 = new DateTime(int.Parse(fecha1[2]), int.Parse(fecha1[1]), int.Parse(fecha1[0]));
                    query = query.Where(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha >= t1);
                }

                if (fecha_final != "")
                {
                    string[] fecha2 = fecha_final.Split('/');
                    DateTime t2 = new DateTime(int.Parse(fecha2[2]), int.Parse(fecha2[1]), int.Parse(fecha2[0]));
                    query = query.Where(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha <= t2);

                }
            }


            if (estado.Trim() != "0")
            {
                int _e = int.Parse(estado);
                query = query.Where(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.estado == _e);
            }

            if (franquiciado.Trim() != "NaN")
            {
                query = query.Where(x => x.franqui.razon_social.Contains(franquiciado));
            }

            if (sistema.Trim() != "0")
            {
                int _s = int.Parse(sistema);
                query = query.Where(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.id_tipo_servicio == _s);
            }

            if (punto_servicio.Trim() != "0")
            {

                query = query.Where(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.id_punto_servicio.Equals(punto_servicio_int));

            }



            if (sortColumnIndex == 0) {


                if (sortDirection == "asc")
                {
                    query = query.OrderBy(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha);
                }
                else
                {
                    query = query.OrderByDescending(x => x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha);
                }
            
            }

            int unidad=SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();

            IQueryable<subData> pre_stage = query.Where(x=>x.demas.x.contratos.id_unidad==unidad).Select(x => new subData
            {
                chofer = x.demas.x.demas.x.demas.container.s.conductor.nombre_conductor.Trim(),
                estado_punto = (x.demas.x.demas.x.punto_servicios.estado_punto==1)?"ACTIVO" : "INACTIVO",
                sistema = x.demas.x.demas.x.demas.tipo_serv.tipo_servicio1,
                volteos = SqlFunctions.StringConvert((double)x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.cantidad).Trim(),
                franquiciado = x.franqui.razon_social,
                patente = (from ca in ctx.camiones where ca.id_franquiciado == x.franqui.id_franquiciado select ca.patente).FirstOrDefault(),
                hoja_ruta = SqlFunctions.StringConvert((double)x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.id_hoja_ruta).Trim(),
                punto = x.demas.x.demas.x.punto_servicios.nombre_punto,
                fecha = (SqlFunctions.DateName("day", x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha) + "/" + SqlFunctions.DateName("month", x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha) + "/" + SqlFunctions.DateName("year", x.demas.x.demas.x.demas.container.s.hojas_ruta.hoja_ruta_detalle.fecha)).Trim()
            });



            switch (sortColumnIndex)
            {/*
                case 0:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.fecha); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.fecha); }
                    break;
              * */
                case 1:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.hoja_ruta); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.hoja_ruta); }
                    break;
                case 2:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.punto); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.punto); }
                    break;
                case 3:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.sistema); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.sistema); }
                    break;
                case 4:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.patente); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.patente); }
                    break;
                case 5:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.franquiciado); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.franquiciado); }
                    break;
                case 6:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.chofer); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.chofer); }
                    break;
                case 7:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.estado_punto); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.estado_punto); }
                    break;
                case 8:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.volteos); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.volteos); }
                    break;
                case 9:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.cobro); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.cobro); }
                    break;
                case 10:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.pago); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.pago); }
                    break;
                case 11:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.margen); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.margen); }
                    break;
                case 12:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.margen_ciento); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.margen_ciento); }
                    break;

            }

            List<subData> final_data = pre_stage.ToList();

            if (excel.Equals("true"))
            {
                var final_data_excel = final_data;
                return View("Excel", final_data);
            }



            List<subData> data_pag = final_data.Skip(IDisplayStart_int).Take(IDisplayLength_int).ToList();
            int count = final_data.Count;



            json_dataTable2 data = new json_dataTable2();
            data.aaData = data_pag;
            data.iTotalDisplayRecords = count;
            data.iTotalRecords = count;
            data.sEcho = SEcho;


            return Json(data, JsonRequestBehavior.AllowGet);
        }



        public void buildingViewBag()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            IEnumerable<object> tipo_servicio = new object[]{
            new { Text="Seleccione un Valor",Value="0"},
            new { Text="Front Loader",Value="1"},
            new { Text="Hook",Value="2"},
            new { Text="Caja Recolectora",Value="3"}
        };

            PuntosServiciosRepository rs_puntos = new PuntosServiciosRepository(ctx);
            PuntoEstadoRepository rs_estado = new PuntoEstadoRepository(ctx);
            FranquiciadosRepository rs_franquiciados = new FranquiciadosRepository(ctx);

            IEnumerable<object> puntos = rs_puntos.getPuntosServicioForSelect();
            IEnumerable<object> estados = rs_estado.getPuntoEstadoForSelect2();
            IEnumerable<object> franquiciados = rs_franquiciados.GetFranquiciadoForSelect();


            ViewBag.tipo_servicio = new SelectList(tipo_servicio, "Value", "Text", "0");
            ViewBag.puntos = new SelectList(puntos, "Value", "Text", "0");
            ViewBag.estados = new SelectList(estados, "Value", "Text", "0");
            ViewBag.franquiciados = new SelectList(franquiciados, "Value", "Text", "NaN");

        }
    }
}
