﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models;
using System.Text;
using Microsoft.VisualBasic;
using SGS.Models.ShadowBox;


namespace SGS.Controllers
{
    public class MantenedorUFController : Controller
    {
        //
        // GET: /MantenedorUF/

        public ActionResult Index()
        {
            MantenedorUfView model = new MantenedorUfView();
            using (var db = new sgsbdEntities())
            {
                int mesPosterior = DateTime.Now.AddMonths(1).Month;
                int añoAnterior = DateTime.Now.AddMonths(1).Year;
                if (db.uf.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() != null && db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() != null)
                {
                    model.puedeAgregar = false;
                }
                else
                {
                    model.puedeAgregar = true;
                }
                var ufs = db.uf.ToList();
                if (ufs.Count != 0)
                {
                    model.tabla = new List<tablaUFView>();
                    foreach (var item in ufs)
                    {
                        tablaUFView row = new tablaUFView();
                        row.idUF = item.id;
                        row.periodo = item.mes.ToString() + "-" + item.año.ToString();
                        row.valorUF = item.valor_uf.ToString("N2");
                        model.tabla.Add(row);
                        if (item.año == DateTime.Now.Year && item.mes == DateTime.Now.Month) row.editable = true;
                        if (item.año == DateTime.Now.AddMonths(1).Year && item.mes == DateTime.Now.AddMonths(1).Month) row.editable = true;
                    }
                }
            }
            return View(model);
        }
        public ActionResult excel()
        {

            MantenedorUfView model = new MantenedorUfView();
            using (var db = new sgsbdEntities())
            {
                int mesPosterior = DateTime.Now.AddMonths(1).Month;
                int añoAnterior = DateTime.Now.AddMonths(1).Year;
                if (db.uf.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() != null && db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() != null)
                {
                    model.puedeAgregar = false;
                }
                else
                {
                    model.puedeAgregar = true;
                }
                var ufs = db.uf.ToList();
                if (ufs.Count != 0)
                {
                    model.tabla = new List<tablaUFView>();
                    foreach (var item in ufs)
                    {
                        tablaUFView row = new tablaUFView();
                        row.idUF = item.id;
                        row.periodo = item.mes.ToString() + "-" + item.año.ToString();
                        row.valorUF = item.valor_uf.ToString("N2");
                        model.tabla.Add(row);
                        if (item.año == DateTime.Now.Year && item.mes == DateTime.Now.Month) row.editable = true;
                        if (item.año == DateTime.Now.AddMonths(1).Year && item.mes == DateTime.Now.AddMonths(1).Month) row.editable = true;
                    }
                }
            }
            return View(model);
        }
        public ActionResult crearUF(int idUF)
        {
            crearUFView model = new crearUFView();
            model.idUF = idUF;
            using (var db = new sgsbdEntities())
            {
                if (idUF != -1)
                {
                    var uf = db.uf.Where(u => u.id == idUF).FirstOrDefault();
                    model.idUF = uf.id;
                    model.uf = uf.valor_uf.ToString("N2");
                }

            }
            llenaPeriodosUF(model);
            return View(model);
        }
        [HttpPost]
        public ActionResult crearUF(crearUFView model)
        {
            using (var db = new sgsbdEntities())
            {
                if (model.idUF != -1)
                {
                    var modificar = db.uf.Where(u => u.id == model.idUF).FirstOrDefault();
                    modificar.valor_uf = double.Parse(model.uf.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                    db.SaveChanges();
                }
                else
                {
                    var nueva = db.uf.Create();
                    if (db.uf.ToList().Count == 0) nueva.id = 1;
                    else nueva.id = db.uf.Max(u => u.id) + 1;
                    if (model.SelectListPeriodo.periodoSelect.Value == 1)
                    {
                        nueva.año = DateTime.Now.Year;
                        nueva.mes = DateTime.Now.Month;
                    }
                    else
                    {
                        nueva.mes = DateTime.Now.AddMonths(1).Month;
                        nueva.año = DateTime.Now.AddMonths(1).Year;
                    }
                    nueva.valor_uf = double.Parse(model.uf.Replace(".", "").Replace(",", "."), System.Globalization.CultureInfo.InvariantCulture);
                    db.uf.Add(nueva);
                    db.SaveChanges();
                }

            }
            return RedirectToAction("Index", "MantenedorUF");
        }
        public void llenaPeriodosUF(crearUFView model)
        {
            sgsbdEntities db = new sgsbdEntities();
            if (model.SelectListPeriodo == null)
            {
                model.SelectListPeriodo = new selectListPeriodoUF();
                model.SelectListPeriodo.periodoSelect = 1;
            }
            string periodoActual = DateTime.Now.Month.ToString() + "-" + DateTime.Now.Year.ToString();
            string periodoPosterioro = DateTime.Now.AddMonths(1).Month.ToString() + "-" + DateTime.Now.AddMonths(1).Year.ToString();
            if (db.uf.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                         new List<Object>{ 
                   new { value = "1" , text = DateTime.Now.Month.ToString()+"-"+DateTime.Now.Year.ToString()  }},
                    "value",
                    "text");
                model.SelectListPeriodo.periodos = new SelectList(periodos, "value", "text", model.SelectListPeriodo.periodos);
            }
            int mesPosterior = DateTime.Now.AddMonths(1).Month;
            int añoAnterior = DateTime.Now.AddMonths(1).Year;
            if (db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                            new List<Object>{ 
                   new { value = "2" , text = DateTime.Now.AddMonths(1).Month.ToString()+"-"+DateTime.Now.AddMonths(1).Year.ToString() }},
                       "value",
                       "text");
                model.SelectListPeriodo.periodos = new SelectList(periodos, "value", "text", model.SelectListPeriodo.periodos);

            }
            if (db.uf.Where(u => u.año == DateTime.Now.Year && u.mes == DateTime.Now.Month).FirstOrDefault() == null && db.uf.Where(u => u.año == añoAnterior && u.mes == mesPosterior).FirstOrDefault() == null)
            {
                SelectList periodos = new SelectList(
                         new List<Object>{ 
                   new { value = "1" , text = DateTime.Now.Month.ToString()+"-"+DateTime.Now.Year.ToString()  },
                   new { value = "2" , text = DateTime.Now.AddMonths(1).Month.ToString()+"-"+DateTime.Now.AddMonths(1).Year.ToString() }},
                    "value",
                    "text");
                model.SelectListPeriodo.periodos = new SelectList(periodos, "value", "text", model.SelectListPeriodo.periodos);
            }

        }
    }
}
