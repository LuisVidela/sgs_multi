﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.modelViews;
using Rotativa;
using System.IO;
using System.Configuration;


namespace SGS.Controllers
{
    public class MantenedorHDRDController : Controller
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Oso()
        {
            return View();
        }


        //
        // GET: /MantenedorHDR/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="imprimir"></param>
        /// <returns></returns>
        public ActionResult View(string id, int imprimir = 0)
        {
            int n = int.Parse(id);


            sgsbdEntities ctx = new sgsbdEntities();

            ViewData["editable"] = true;

            if (Request.QueryString["estado"] != null)
            {
                if (Int32.Parse(Request.QueryString["estado"]).Equals(3))
                {

                    ViewData["editable"] = false;
                }
            }

            var editable = ctx.hoja_ruta.Where(u => u.id_hoja_ruta == n).SingleOrDefault();
            if (editable.fecha_pagado != null)
            {
                ViewData["editable"] = false;
            }

            if (User.IsInRole("EditHDRPagada"))
            {
                ViewData["editable"] = true;
            }

            ViewData["administrador"] = false;
            var usuario = ctx.UserProfile.Where(u => u.UserName == User.Identity.Name).FirstOrDefault();
            if (usuario.id_perfil == 1)
            {
                ViewData["administrador"] = true;
            }




            int id_hoja = Convert.ToInt32(id);
            var modelo = new UnidadesNegocioView();
            modelo.id_unidad = (from hr in ctx.hoja_ruta
                                join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
                                join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
                                join con in ctx.contratos on ps.id_contrato equals con.id_contrato
                                where hr.id_hoja_ruta == id_hoja
                                select new UnidadesNegocioView { id_unidad = con.id_unidad.Value }).FirstOrDefault().id_unidad;

            int id_unidad = modelo.id_unidad;
            ViewBag.idunidad = id_unidad;

            Models.repository.UnidadesNegocioRepository rsd = new Models.repository.UnidadesNegocioRepository(new sgsbdEntities());
            unidades_negocio un = rsd.GetUnidadNegocioByID(id_unidad);
            ViewBag.unidad = un;
            



            var model = new ViewModelHDRD();
            model.Elements = (from hr in ctx.hoja_ruta
                              join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
                              join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
                              join rs in ctx.residuos on ps.id_residuo equals rs.id_residuo
                              join co in ctx.contratos on ps.id_contrato equals co.id_contrato
                              join tc in ctx.tipo_contrato on co.id_tipo_contrato equals tc.id_tipo_contrato
                              join eq in ctx.equipos on ps.id_equipo equals eq.id_equipo
                              join v in ctx.vertederos on ps.id_vertedero equals v.id_vertedero
                              join sucu in ctx.sucursales on co.id_sucursal equals sucu.id_sucursal
                              where hr.id_hoja_ruta == n
                              orderby hr.id_hoja_ruta, rd.Orden
                              select new MantenedorHDRDView
                              {
                                  id_hoja_ruta = hr.id_hoja_ruta,
                                  id_punto_servicio = rd.id_punto_servicio,
                                  orden = rd.Orden,
                                  cantidad = rd.cantidad,
                                  peso = rd.peso == null ? 0 : rd.peso,
                                  nombre_punto = ps.nombre_punto,
                                  estado = rd.estado,
                                  tipo_contrato1 = hr.id_tipo_servicio,
                                  id_tipo_contrato = hr.id_tipo_servicio,
                                  tipo_equipo = eq.tipo_equipo,
                                  equipoentrada = rd.equipoentrada,
                                  equiposalida = rd.equiposalida,
                                  horainicio = rd.horainicio,
                                  horatermino = rd.horatermino,
                                  relleno = rd.relleno,
                                  observacion = rd.observacion,
                                  vertedero = v.vertedero,
                                  cant_equipos = ps.cant_equipos,
                                  vertededordefecto = v.vertedero,
                                  codigo_externo_asignado = ps.codigo_externo_asignado,
                                  codigo_externo = rd.codigo_externo,
                                  PORL_CODIGO = rd.PORL_CODIGO,
                                  peso_calculado = rd.peso_calculado,
                                  densidad = rd.densidad,
                                  densidadporDefecto = 1,
                                  capacidad = eq.capacidad_equipo == null ? 0 : eq.capacidad_equipo,
                                  con_carro = sucu.permite_carro,
                                  sucursal = sucu.id_sucursal,
                                  peso_concarro =rd.peso_concarro == null ? 0 : rd.peso_concarro,
                                  guia_concarro = rd.guia_concarro,
                                  costos_por_cantidad = ps.cobro_por_cantidad,
                                  costos_por_m3 =ps.cobro_por_m3,
                                  costos_por_peso =ps.cobro_por_peso,
                                  costos_por_bano = ps.cobro_por_banos,
                                  cantidad_bano =rd.cantidad_bano,
                              }).ToList();
            ViewBag.model = model.Elements[0].costos_por_bano;
            var vertir = new ViewModelVertedero();
            vertir.Elements = (from v in ctx.vertederos
                               select new VertederosView { id_vertedero = v.id_vertedero, vertedero = v.vertedero }).ToList();

            var EncabezadoRuta = new ViewModelHeaderHDRD();
            EncabezadoRuta.Elements = (from hr in ctx.hoja_ruta
                                       join co in ctx.conductores on hr.id_conductor equals co.id_conductor
                                       join ca in ctx.camiones on hr.id_camion equals ca.id_camion
                                       join tc in ctx.tipo_servicio on hr.id_tipo_servicio equals tc.id_tipo_servicio
                                       where hr.id_hoja_ruta == n
                                       select new HeaderHDRDView { id_hoja_ruta = hr.id_hoja_ruta, franquiciado = co.nombre_conductor, rut_conductor = co.rut_conductor, patente = ca.patente, num_interno = ca.num_interno, tipo_servicio = tc.tipo_servicio1, fecha = hr.fecha.Value }).ToList();

            var hoy = DateTime.Today.ToLongDateString();


            var ValesRuta = new ViewModelValesHDRD();

            ValesRuta.Elements = (from hr in ctx.hoja_ruta
                                  join co in ctx.conductores on hr.id_conductor equals co.id_conductor
                                  join ca in ctx.camiones on hr.id_camion equals ca.id_camion
                                  join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
                                  join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
                                  join con in ctx.contratos on ps.id_contrato equals con.id_contrato
                                  join tc in ctx.tipo_contrato on con.id_tipo_contrato equals tc.id_tipo_contrato
                                  join eq in ctx.equipos on ps.id_equipo equals eq.id_equipo
                                  join v in ctx.vertederos on ps.id_vertedero equals v.id_vertedero
                                  where hr.id_hoja_ruta == n
                                  select new ValesHDRDView { id_hoja_ruta = hr.id_hoja_ruta, nombre_conductor = co.nombre_conductor, rut_conductor = co.rut_conductor, patente = ca.patente, num_interno = ca.num_interno, Vertedero = v.vertedero, punto_servicio = ps.nombre_punto, Tipo_Contenedor = eq.nom_nemo, Fecha = hr.fecha.Value }).ToList();

            List<TB_SGS_PORCENTAJELLENADO> lstProcentajeLLenado = ctx.TB_SGS_PORCENTAJELLENADO.ToList();

            ViewData["poncentajeLLenado"] = lstProcentajeLLenado.OrderBy(o => o.PORL_PORCERTAJELLENADO).ToList();
            ViewData["puntosHDRD"] = model.Elements;
            ViewData["Vertederos"] = vertir.Elements;
            ViewData["ValesRuta"] = ValesRuta.Elements;
            ViewData["EncabezadoRuta"] = EncabezadoRuta.Elements;
            ViewData["imprimir"] = imprimir;
           

            return View(model.Elements);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AnularHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            int IdHojaRuta = int.Parse(Request.Form["IdHojaRuta"].ToString());
            List<hoja_ruta> hoja2 = ctx.hoja_ruta.Where(i => i.id_hoja_ruta == IdHojaRuta).ToList();
            hoja2[0].estado = 4;
            ctx.hoja_ruta.Attach(hoja2[0]);
            ctx.Entry(hoja2[0]).State = System.Data.EntityState.Modified;

            var detalle = ctx.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == IdHojaRuta).DefaultIfEmpty();

            foreach (hoja_ruta_detalle items in detalle)
            {
                items.estado = 13; //Estado 13 de puntos de HDR = Anulado
            }

            

            ctx.SaveChanges();
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public object checkValue(object val)
        {
            if (((string)val).Equals("NN"))
            {
                return null;
            }
            else
            {
                return val;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ModificarHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            string pp = Request.Form["puntos"].ToString();
            string[] puntos = Request.Form["puntos"].ToString().Split('|');
            int Ruta;
            int Punto;
            int Orden;
            int? tipo_servicio_bano = null;
            double? Peso;
            int Estado;
            int? cantidad;
            int? codigo_externo;
            int tiposervicio;
            Int32 porcentajeLlenado;
            Nullable<double> densidad = null;
            double? peso_concarro;
            int? guia_concarro;
            int? cantidad_bano;


            string equipoentrada, equiposalida, horainicio, horatermino, observacion, vertedero;

            bool todos = true;
            bool cambio = false;
            int iidruta = 0;
            for (int j = 0; j < puntos.Length; j++)
            {
                String[] unpunto = puntos[j].Split('_');
                Ruta = int.Parse(unpunto[0]);
                iidruta = Ruta;
                Punto = int.Parse(unpunto[1]);
                Orden = int.Parse(unpunto[2]);
                horainicio = (string)checkValue(unpunto[3]);
                horatermino = (string)checkValue(unpunto[4]);
                if (unpunto[5] != "N/A" && unpunto[5] != "") { codigo_externo = int.Parse(unpunto[5]); } else { codigo_externo = null; }
                equipoentrada = (string)checkValue(unpunto[6]);
                equiposalida = (string)checkValue(unpunto[7]);
                vertedero = unpunto[8];
                cantidad = (unpunto[9] != "") ? int.Parse(unpunto[9]) : 0;
                //if (checkValue(unpunto[10]) != null) { Peso = int.Parse(unpunto[10]); } else { Peso = null; }
                if (checkValue(unpunto[10]) != null) { Peso = Convert.ToDouble(unpunto[10].Replace(".", ",")); } else { Peso = 0; }
                Estado = int.Parse(unpunto[11]);
                observacion = (string)checkValue(unpunto[12]);
                tiposervicio = int.Parse(unpunto[13]);
                if (tiposervicio == 285){
                    tipo_servicio_bano = int.Parse(unpunto[20]);
                }
                if (checkValue(unpunto[17]) != null) { peso_concarro = Convert.ToDouble(unpunto[17].Replace(".", ",")); } else { peso_concarro = 0; }
                if (unpunto[18] != "N/A" && unpunto[18] != "" && unpunto[18] != "undefined") { guia_concarro = int.Parse(unpunto[18]); } else { guia_concarro = null; }
                if (unpunto[19] != "" && unpunto[19] != "NN") { cantidad_bano = int.Parse(unpunto[19]); } else { cantidad_bano = 0; }
                if (String.IsNullOrEmpty(unpunto[14]) || unpunto[14] == "undefined")
                {
                    porcentajeLlenado = 1;
                }
                else { porcentajeLlenado = Convert.ToInt32(unpunto[14]); }
                if (String.IsNullOrEmpty(unpunto[16]) || unpunto[16] == "undefined")
                {
                    densidad = 0;
                }
                else { densidad = Convert.ToDouble(unpunto[16].Replace(".", ",")); }
                List<hoja_ruta_detalle> hoja = ctx.hoja_ruta_detalle.Where(i => i.id_hoja_ruta == Ruta && i.Orden == Orden).ToList();

                if (cantidad != -1)
                    hoja[0].cantidad = cantidad;
                else
                    hoja[0].cantidad = null;

                if (Peso != null)
                    hoja[0].peso = Peso;
                else
                    hoja[0].peso = 0;

                if (Estado == 1 && Peso != null)
                    hoja[0].estado = 2;
                else
                    hoja[0].estado = Estado;

                if (hoja[0].estado != 1)
                    cambio = true;

                if (hoja[0].estado == 1 || hoja[0].estado == 8)
                    todos = false;

               
                hoja[0].tipo_servicio_bano = tipo_servicio_bano;
                hoja[0].id_tipo_servicio = tiposervicio;
                hoja[0].equipoentrada = equipoentrada;
                hoja[0].equiposalida = equiposalida;
                hoja[0].horainicio = horainicio;
                hoja[0].horatermino = horatermino;
                hoja[0].observacion = observacion;
                hoja[0].relleno = vertedero;
                hoja[0].codigo_externo = codigo_externo;
                hoja[0].PORL_CODIGO = porcentajeLlenado;
                //hoja[0].peso_calculado = Convert.ToBoolean(unpunto[15]);
                hoja[0].densidad = densidad;
                hoja[0].peso_concarro = peso_concarro ;
                hoja[0].guia_concarro = guia_concarro;
                hoja[0].cantidad_bano = cantidad_bano;

                ctx.hoja_ruta_detalle.Attach(hoja[0]);
                ctx.Entry(hoja[0]).State = System.Data.EntityState.Modified;
                ctx.SaveChanges();
            }

            List<hoja_ruta> hoja2 = ctx.hoja_ruta.Where(i => i.id_hoja_ruta == iidruta).ToList();
            if (todos)
                hoja2[0].estado = 3;
            else if (cambio)
                hoja2[0].estado = 2;
            else
                hoja2[0].estado = 1;
            ctx.hoja_ruta.Attach(hoja2[0]);
            ctx.Entry(hoja2[0]).State = System.Data.EntityState.Modified;
            ctx.SaveChanges();

            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Imprimir(string id) //HttpResponseMessage
        {
            try
            {
                var patente = "";

                if (Request.QueryString["patente"] != null)
                {
                    patente = "_" + Request.QueryString["patente"];
                }

                var switches = @" --username " + ConfigurationManager.AppSettings["PdfUserName"] + " --password " + ConfigurationManager.AppSettings["PdfPassword"];

                var vista = new ActionAsPdf("View", new { id = id, imprimir = 1 })
                {
                    FileName = "HDR_" + id + ".pdf",
                    PageOrientation = Rotativa.Options.Orientation.Landscape,
                    PageSize = Rotativa.Options.Size.A4
                };

                return vista;
            }
            catch (Exception ex)
            {
                WriteLog(ex);
            }

            return null;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void WriteLog(Exception ex)
        {
            using (StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\logSGS_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", true))
            {
                sw.WriteLine("{0} - {1}:{2}:{3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message, ex.StackTrace, ex.Source);
                sw.Close();
            }
        }

        
        //protected override void OnException(ExceptionContext filterContext)
        //{
        //    base.OnException(filterContext);

        //    Exception ex = filterContext.Exception;
        //    filterContext.ExceptionHandled = true;
        //    WriteLog(ex);
        //}

    }
}
