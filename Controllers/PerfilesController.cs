using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;

using System.Data;


namespace SGS.Controllers
{
    public class PerfilesController : Controller
    {
        private sgsbdEntities ctx = new sgsbdEntities();

        public ActionResult Index() {


            List<perfiles> data = ctx.perfiles.OrderBy(x=>x.nombre).ToList();

            return View(data);
        }

        public ActionResult Create() {

            return View();
        }

        [HttpPost]
        public ActionResult Create(perfiles model)
        {

            if (ModelState.IsValid) {

                

                if (model.id_perfil == 0)
                {
                   
                    ctx.perfiles.Add(model);

                }
                else
                {
                    var entry=ctx.Entry(model);
                    entry.State=EntityState.Modified;
                }

                ctx.SaveChanges();
                return RedirectToAction("Index","Perfiles");
            }


            return View(model);

        }


        public ActionResult edit(int id) {
           perfiles model= ctx.perfiles.Where(p=>p.id_perfil==id).FirstOrDefault();
           return View("Create",model);
        
        }


        public ActionResult Delete(int id) {


            perfiles model = ctx.perfiles.Where(p => p.id_perfil == id).FirstOrDefault();
            ctx.perfiles.Remove(model);
            ctx.SaveChanges();
            return RedirectToAction("Index","Perfiles");
        
        }



        public ActionResult Roles(int id) {


            var roles=(from r in ctx.modulos orderby r.nombre select r).ToList();
            ViewBag.Roles = roles;
            ViewBag.id = id;
            List<int>Read=(from w in ctx.webpages_UsersInRoles where w.UserId==id
                            && w.Read==true
                            select w.webpages_Roles.RoleId).ToList();
            List<int> Create = (from w in ctx.webpages_UsersInRoles
                              where w.UserId == id
                                  && w.Create == true
                              select w.webpages_Roles.RoleId).ToList();
            List<int> Update = (from w in ctx.webpages_UsersInRoles
                              where w.UserId == id
                                  && w.Update == true
                              select w.webpages_Roles.RoleId).ToList();
            List<int> Delete = (from w in ctx.webpages_UsersInRoles
                              where w.UserId == id
                                  && w.Delete == true
                              select w.webpages_Roles.RoleId).ToList();
            
            ViewBag.Create = Create;
            ViewBag.Update = Update;
            ViewBag.Read = Read;
            ViewBag.Delete = Delete;
            return View(roles);

           
        }



        public ActionResult addRol(int rol,int perfil, string type) {

            int numero=ctx.webpages_UsersInRoles.Where(x => x.UserId == perfil && x.RoleId == rol).Count();
            webpages_UsersInRoles w;

            if (numero == 0)
            {
                w = new webpages_UsersInRoles();
                w.UserId=perfil;
                w.RoleId=rol;
                w = setterOperations(w, type, 1);
                ctx.webpages_UsersInRoles.Add(w);
            }
            else {
                w = ctx.webpages_UsersInRoles.Where(x => x.UserId == perfil && x.RoleId == rol).FirstOrDefault();
                w = setterOperations(w, type, 1);
                var entry=ctx.Entry(w);
                entry.State = EntityState.Modified;

            }

            ctx.SaveChanges();
            return Content("SUCCESS");
        }

        public ActionResult removeRol(int rol,int perfil, string type)
        {
            webpages_UsersInRoles w=ctx.webpages_UsersInRoles.Where(x => x.UserId == perfil && x.RoleId == rol).FirstOrDefault();
            w = setterOperations(w, type, 0);
            var entry = ctx.Entry(w);
            entry.State = EntityState.Modified;
            ctx.SaveChanges();
            return Content("SUCCESS");
        }


        private webpages_UsersInRoles setterOperations(webpages_UsersInRoles ent,string _type,int direction) { 
        
         switch(_type){
             case "read":
                 if (direction == 1) { ent.Read = true; } else { ent.Read = false; }
             break;
             case "create":
             if (direction == 1) { ent.Create = true; } else { ent.Create = false; }
                 break;   
             case "update":
                 if (direction == 1) { ent.Update = true; } else { ent.Update = false; }
              break;    
             case "delete":
              if (direction == 1) { ent.Delete = true; } else { ent.Delete = false; }
          break;
             default:
                 throw new Exception("valor invalido de tipo de operacion");
         }

         return ent;
        
        }

       
    }
}
