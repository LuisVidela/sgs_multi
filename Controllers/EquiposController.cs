﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class EquiposController : Controller
    {
        //
        // GET: /Equipos/

        public ActionResult Index()
        {
            EquiposRepository rs = new EquiposRepository(new sgsbdEntities());
            List<equipos> data = (List<equipos>)rs.GetEquipos();

            return View(data);
        }
        public ActionResult excel()
        {

            EquiposRepository rs = new EquiposRepository(new sgsbdEntities());
            List<equipos> data = (List<equipos>)rs.GetEquipos();

            return View(data);
        }

        public ActionResult create()
        {
           
            return View();
        }


        [HttpPost]
        public ActionResult create(equipos model)
        {

            if (ModelState.IsValid)
            {

                EquiposRepository rs = new EquiposRepository(new sgsbdEntities());

                if (model.id_equipo == 0)
                {
                    rs.InsertEquipo(model);
                }
                else
                {
                    rs.UpdateEquipo(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Equipos");

            }

            
            return View(model);
        }



        public ActionResult edit(int id)
        {

            EquiposRepository rs = new EquiposRepository(new sgsbdEntities());
            equipos c = rs.GetEquipoByID(id);
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            EquiposRepository rs = new EquiposRepository(new sgsbdEntities());
            rs.DeleteEquipo(id);
            rs.Save();
            return this.RedirectToAction("Index", "Equipos");

        }

    }
}
