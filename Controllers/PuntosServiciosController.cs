﻿using System;
using System.Collections;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Web.UI;

namespace SGS.Controllers
{
    public class PuntosServiciosController : Controller
    {

        private sgsbdEntities ctx = new sgsbdEntities();
        //
        // GET: /PuntosServicios/

        public ActionResult Index()
        {
            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            List<PuntosServiciosView> data = (List<PuntosServiciosView>)rs.GetPuntosServiciosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }
        public ActionResult Excel()
        {

            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            List<puntos_servicio> data = (List<puntos_servicio>)rs.GetPuntosServicios();

            return View(data);
        }

        public ActionResult view(int id)
        {
            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            PuntosServiciosView c = rs.GetPuntosServiciosView(id);
            buildViewBagList();
            return View(c);
        }


        public ActionResult create()
        {

            puntos_servicio model = new puntos_servicio();

            this.buildViewBagList();
            return View(model);
        }

        [HttpPost]
        public ActionResult create(puntos_servicio model,string dias)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());

            //#region sin usar
            var dato = ctx.puntos_servicio.Where(u => u.nombre_punto == model.nombre_punto).FirstOrDefault();
            //#endregion

            if (ctx.puntos_servicio.Where(u => u.nombre_punto == model.nombre_punto).FirstOrDefault() != null && model.id_punto_servicio == 0)
            {
                //Response.Write("<script>alert('Your text');</script>");
                ModelState.AddModelError("nombre_punto", "El nombre ya existe");
            }
            if (ModelState.IsValid)
            {
      


                if (model.id_punto_servicio == 0)
                {
                  

                     rs.InsertPuntoServicio(model); 
                    
                }
                else
                {
                   
                    rs.UpdatePuntoServicio(model);
                }

                rs.Save();
                return this.RedirectToAction("Index", "PuntosServicios");
            }

            this.buildViewBagList();
            return View(model);
        }

        //[HttpPost]
        //public string BuscarOrdenes(sgsbdEntities ctx)
        //{

        //    int contrato = int.Parse(Request.Form["puntos"].ToString());
        //    OrdenCompraRepository rs = new OrdenCompraRepository(new sgsbdEntities());
        //    List<OrdenCompraView> data = (List<OrdenCompraView>)rs.GetContrato(contrato);

        //    ViewBag.ordencompra =data;
        //    var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);

        //    return jsonString;
        //}


        public ActionResult createflow(int id, int suc) 	 	{ 	 	
	 	 	
	            puntos_servicio model = new puntos_servicio(); 	 	
                model.id_contrato = id; 	 	
	 	 	
	            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities()); 	 	
	            sucursales sucis = rs.GetSucursalByID(suc); 	 	
	 	 	
	            ViewBag.TitleIs = "Sucursal " + sucis.sucursal + " Contrato " + id.ToString(); 	 	
	 	 	
	            this.buildViewBagList(); 	 	
	            return View(model); 	 	
	 	 	
	        } 	 	
	 	 	
	        [HttpPost] 	 	
	        public ActionResult createflow(puntos_servicio model, string dias) { 	 	
 	 	
	 	 	
           if (ModelState.IsValid) 	 	
           { 	 	
 	 	
	            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());


                if (model.id_punto_servicio == 0) 	 	
                {
 
                    rs.InsertPuntoServicio(model);
                    //ctx.puntos_servicio.Add()
                } 	 	
                else 	 	
                { 	 	
	                    rs.UpdatePuntoServicio(model); 	 	
               } 	 	
	 	
                rs.Save(); 	 	
                return this.RedirectToAction("Index", "PuntosServicios"); 	 	
           } 	 	
 	 	
            this.buildViewBagList(); 	 	
            return View(model); 	 	
        } 
        public ActionResult edit(int id)
        {
            
            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            puntos_servicio c = rs.GetPuntosServiciosByID(id);
            ViewBag._estado = c.estado_punto;
            this.buildViewBagList();
            return View("create", c);
        }

        //public ActionResult Delete(int id)
        //{
        //    PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
        //    rs.DeletePuntoServicio(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "PuntosServicios");

        //}

        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();

            ContratosRepository rs1 = new ContratosRepository(ctx);
            RellenosRepository rs2 = new RellenosRepository(ctx);
            ResiduosRepository rs3 = new ResiduosRepository(ctx);
            EquiposRepository rs5 = new EquiposRepository(ctx);
            TipoTarifaRepository rs4 = new TipoTarifaRepository(ctx);
            OrdenCompraRepository rs6 = new OrdenCompraRepository(ctx);
        

          IEnumerable<object> rsestado = new object[] {
             new { text="Inactivo",value="2"},
             new { text="Activo",value="1"}
            };

            ViewBag.estado = new SelectList(rsestado, "value", "text");
            ViewBag.contratos = new SelectList(rs1.GetContratosForList((int?)Session["unidad_seleccionada"]??default(int)),"value","text");
            ViewBag.vertederos = new SelectList(rs2.GetRellenosForList(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "value", "text");
            ViewBag.residuos = new SelectList(rs3.GetResiduosForList(), "value", "text");
            ViewBag.tipotarifas = new SelectList(rs4.GetTipoTarifasForList(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "value", "text");
            //ViewBag.tipotarifas = new SelectList(rs4.GetTipoTarifasForList(), "value", "text");
            ViewBag.equipos = new SelectList(rs5.GetEquiposForList(), "value", "text");

        }
    }
}
