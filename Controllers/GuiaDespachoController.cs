﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using RazorPDF;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.factories;
using Bytescout.PDFExtractor;


namespace SGS.Controllers
{
    public class GuiaDespachoController : Controller
    {

        /// <summary>
        /// Empresa: valentys
        /// Fecha : 14-08-2015
        /// Descripción: Carga la página principal
        /// </summary>
        /// <returns></returns>
        public ActionResult index()
        {
            sgsbdEntities db = new sgsbdEntities();

            List<Models.TB_GIA_ESTADOGUIA> estado = db.TB_GIA_ESTADOGUIA.ToList();
            ViewBag.Estados = estado;
            return View();
        }
        /// <summary>
        /// Empresa : Valentys
        /// Fecha: 17-08-2015
        /// Descripción: obtine las guias de despacho
        /// </summary>
        /// <param name="identificador">identificador de la guia</param>
        /// <param name="fecha">fecha de la guia</param>
        /// <param name="patente">patente del camion</param>
        /// <param name="codigoEstado">Estado de la guia</param>
        /// <returns></returns>
        public ActionResult buscar(String identificador, String fecha, String patente, String codigoEstado)
        {

            sgsbdEntities db = new sgsbdEntities();
            Nullable<Int32> i = null;
            if (!String.IsNullOrEmpty(identificador))
            {
                i = Convert.ToInt32(identificador);
            }
            Nullable<DateTime> d = null;
            if (!String.IsNullOrEmpty(fecha))
            {
                d = Convert.ToDateTime(fecha);
            }
            Nullable<int> e = null;
            if (!String.IsNullOrEmpty(codigoEstado))
            {
                e = db.TB_GIA_ESTADOGUIA.Where(o => o.EGIA_TOKEN == new Guid(codigoEstado)).FirstOrDefault().EGIA_CODIGO;
            }
            List<Models.SP_SEL_GuiaDespacho_Result> lstGuias = db.SP_SEL_GuiaDespacho(String.IsNullOrEmpty(patente) ? null : patente,
                    i,
                    d,
                    e,
                    Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View("result", lstGuias);
        }
        /// <summary>
        /// Empresa : Valentys
        /// Fecha: 17-08-2015
        /// Descripción: obtine las guias de despacho para exportarlas
        /// </summary>
        /// <param name="identificador">identificador de la guia</param>
        /// <param name="fecha">fecha de la guia</param>
        /// <param name="patente">patente del camion</param>
        /// <param name="codigoEstado">Estado de la guia</param>
        /// <returns></returns>
        public ActionResult excel(String identificador, String fecha, String patente, String codigoEstado)
        {
            sgsbdEntities db = new sgsbdEntities();
            Nullable<Int32> i = null;
            if (!String.IsNullOrEmpty(identificador))
            {
                i = Convert.ToInt32(identificador);
            }
            Nullable<DateTime> d = null;
            if (!String.IsNullOrEmpty(fecha))
            {
                d = Convert.ToDateTime(fecha);
            }
            Nullable<int> e = null;
            if (!String.IsNullOrEmpty(codigoEstado))
            {
                e = db.TB_GIA_ESTADOGUIA.Where(o => o.EGIA_TOKEN == new Guid(codigoEstado)).FirstOrDefault().EGIA_CODIGO;
            }
            List<Models.SP_SEL_GuiaDespacho_Result> lstGuias = db.SP_SEL_GuiaDespacho(String.IsNullOrEmpty(patente) ? null : patente,
                    i,
                    d,
                    e,
                    Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();


            return View(lstGuias);

        }
        public ActionResult create()
        {
            sgsbdEntities db = new sgsbdEntities();
            
            ViewData["vertederos"] = db.vertederos.ToList();
            ViewData["Estados"] = db.TB_GIA_ESTADOGUIA.ToList();

            return View("createGuia", new Models.TB_GIA_Guia());
        }
        public ActionResult editar(Guid token)
        {
            sgsbdEntities db = new sgsbdEntities();
            TB_GIA_Guia gia = (from s in db.TB_GIA_Guia
                               where s.GUIA_TOKEN == token
                               select s).FirstOrDefault();

            ViewData["vertederos"] = db.vertederos.ToList();
            ViewData["Estados"] = db.TB_GIA_ESTADOGUIA.ToList();

            return View("createGuia", gia);
        }
        [HttpPost]
        public JsonResult guardarGuia(String identificador, String nombre, String patente, String vertedero, String fechaLlegada, String estado, String codigo)
        {

            sgsbdEntities ctx = new sgsbdEntities();
            GuiaDespachoRepository guia = new GuiaDespachoRepository();
            TB_GIA_Guia indi = new TB_GIA_Guia();
            indi.GUIA_EGIA_CODIGO = (from o in ctx.TB_GIA_ESTADOGUIA where o.EGIA_TOKEN == new Guid(estado) select o).FirstOrDefault().EGIA_CODIGO;
            if (!String.IsNullOrEmpty(fechaLlegada))
            {
                indi.GUIA_FECHALLEGADA = Convert.ToDateTime(fechaLlegada);
            }
            indi.GUIA_ID_VERTEDERO = Convert.ToInt32(vertedero);
            indi.GUIA_IDENTIFICADOR = Convert.ToInt32(identificador);
            indi.GUIA_PATENTE = patente;
            indi.GUIA_NOMBRE = nombre;
            
            if (!String.IsNullOrEmpty(codigo) && !codigo.Equals("0"))
            {
                indi.GUIA_CODIGO = Convert.ToInt32(codigo);
                guia.Update(indi);
                return Json(indi.GUIA_CODIGO);
            }
            guia.Insert(indi);
            return Json(1);
        }
        public ActionResult detalle(Guid token)
        {
            return View("indexDetalle", token);
        }
        public JsonResult eliminar(Int32 codigo)
        {
            sgsbdEntities db = new sgsbdEntities();
            TB_GIA_GUIADETALLE detalle = db.TB_GIA_GUIADETALLE.Where(o => o.DGIA_CODIGO == codigo).FirstOrDefault();
            db.TB_GIA_GUIADETALLE.Remove(detalle);

            db.SaveChanges();
            return Json(1);
        }
        public ActionResult buscarDetalle(Guid token)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_DetalleGuia_Result> lstResult = db.SP_SEL_DetalleGuia(token).ToList();
            return View("resultDetalle", lstResult);
        }
        public ActionResult crearDetalle()
        {
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_DisposicionTemporalDisponible_Result> disposicion = db.SP_SEL_DisposicionTemporalDisponible(Models.repository.UsuariosRepository.getUnidadSeleccionada()).ToList();

            return View("createDetalle", disposicion);
        }
        public JsonResult guardarDetalle(Int32 codigohoja, Int32 orden, Guid token)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            TB_GIA_GUIADETALLE detalle = new TB_GIA_GUIADETALLE();

            detalle.DGIA_ID_HOJA_RUTA = codigohoja;
            detalle.DGIA_ORDEN = orden;
            detalle.DGIA_GUIA_CODIGO = ctx.TB_GIA_Guia.Where(o => o.GUIA_TOKEN == token).FirstOrDefault().GUIA_CODIGO;
            ctx.TB_GIA_GUIADETALLE.Add(detalle);
            ctx.SaveChanges();

            return Json(1);
        }
        public ActionResult verDetalle(Guid token)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_DetalleGuia_Result> lstResult = db.SP_SEL_DetalleGuia(token).ToList();

            return View("verDetalle", lstResult);
        }
        public ActionResult imprimirGuia(Guid token)
        {
            sgsbdEntities db = new sgsbdEntities();
            List<Models.SP_SEL_DetalleGuia_Result> lstResult = db.SP_SEL_DetalleGuia(token).ToList();
            ViewData["guia"] = db.TB_GIA_Guia.Where(o => o.GUIA_TOKEN == token).FirstOrDefault();



            return View(lstResult);
        }

    }

}
