﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data;
using System.Globalization;

namespace SGS.Controllers
{

    public class IngresarHDRBañoController : Controller
    {
        //
        // GET: /MantenedorHDR/

        public ActionResult Index()
        {
            return View();
        }



        public ActionResult create(int id)
        {
            Session["bano"] = id;
            buildViewBagList();
            return View();
        }

        [HttpPost]
        public ActionResult InsertarHoja(sgsbdEntities context)
        {
            int camion = int.Parse(Request.Form["camion"].ToString());
            int chofer = int.Parse(Request.Form["chofer"]);
            int tiposervicio = int.Parse(Request.Form["tiposervicio"]);
            string puntos = Request.Form["puntos"];
            DateTime Fecha = DateTime.Parse(Request.Form["Fecha"]);
            string rutamodelo = Request.Form["rutamodelo"];

            sgsbdEntities ctx = new sgsbdEntities();

            hoja_ruta hoja = new hoja_ruta
            {
                id_hoja_ruta = 0,
                id_conductor = chofer,
                id_camion = camion,
                fecha = Fecha,
                id_tipo_servicio = tiposervicio,
                estado = 1
            };

            context.hoja_ruta.Add(hoja);
            context.SaveChanges();

            string[] data = puntos.Split('_');

            for (int i = 0; i < (int)(data.Length / 3); i++)
            {
                hoja_ruta_detalle hojadetalle = new hoja_ruta_detalle
                {
                    id_hoja_ruta = hoja.id_hoja_ruta,
                    Orden = int.Parse(data[i * 3 + 1]),
                    id_punto_servicio = int.Parse(data[i * 3]),
                    estado = 1,
                    fecha = (data[i * 3 + 2] == "") ? DateTime.Today : DateTime.Parse(data[i * 3 + 2])

                };
                context.hoja_ruta_detalle.Add(hojadetalle);
                context.SaveChanges();
            }

            //rs.Save();

            return Content("SUCCESS_" + hoja.id_hoja_ruta);
        }



        [HttpPost]
        public ActionResult ActualizarHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            int camion = int.Parse(Request.Form["camion"].ToString());
            int chofer = int.Parse(Request.Form["chofer"]);
            int tiposervicio = int.Parse(Request.Form["tiposervicio"]);
            int idruta = int.Parse(Request.Form["ruta"]);
            string puntos = Request.Form["puntos"];
            string puntoseliminados = Request.Form["puntoseliminados"];

            if (puntoseliminados != "" && puntoseliminados != null)
            {
                string[] dataeliminada = puntoseliminados.Split('_');
                for (int j = 0; j < (int)(dataeliminada.Length) - 1; j++)
                {
                    var puntoaeliminar = int.Parse(dataeliminada[j]);
                    List<hoja_ruta_detalle> hojaeliminar = ctx.hoja_ruta_detalle.Where(i => i.id_hoja_ruta == idruta && i.id_punto_servicio == puntoaeliminar).ToList();
                    ctx.hoja_ruta_detalle.Add(hojaeliminar[0]);
                    ctx.Entry(hojaeliminar[0]).State = System.Data.EntityState.Deleted;
                    ctx.SaveChanges();
                }
            }


            string[] data = puntos.Split('_');
            for (int i = 0; i < (int)(data.Length / 3); i++)
            {
                //List<hoja_ruta_detalle> hoja = ctx.hoja_ruta_detalle.Where(i => i.id_hoja_ruta == idruta && i.id_punto_servicio == Punto).ToList();
                hoja_ruta_detalle hojadetalle = new hoja_ruta_detalle
                {
                    id_hoja_ruta = idruta,
                    Orden = int.Parse(data[i * 3 + 1]),
                    id_punto_servicio = int.Parse(data[i * 3]),
                    estado = 1,
                    fecha = DateTime.Parse(data[i * 3 + 2])
                };
                ctx.hoja_ruta_detalle.Add(hojadetalle);
                ctx.SaveChanges();
            }

            //rs.Save();

            return View();
        }



        [HttpPost]
        public string getRutas(sgsbdEntities ctx)
        {
            int TipoServicio = int.Parse(Request.Form["TipoServicio"].ToString());
            DateTime Fecha = DateTime.Parse(Request.Form["Fecha"]);

            string id = Request.Form["ID"];
            if (id == "")
            {
                var q =
        from hr in ctx.hoja_ruta
        join ca in ctx.camiones on hr.id_camion equals ca.id_camion
        join co in ctx.conductores on hr.id_conductor equals co.id_conductor
        join tc in ctx.tipo_servicio on hr.id_tipo_servicio equals tc.id_tipo_servicio
        join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
        join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
        where tc.id_tipo_servicio == TipoServicio && hr.fecha == Fecha
        //((ps.lunes == true && Dia == "Lunes") || (ps.martes == true && Dia == "Martes") || (ps.miercoles == true && Dia == "Miercoles") || (ps.jueves == true && Dia == "Jueves") || (ps.viernes == true && Dia == "Viernes") || (ps.sabado == true && Dia == "Sabado") || (ps.domingo == true && Dia == "Domingo"))
        orderby hr.id_hoja_ruta, rd.Orden
        select new { hr.estado, hr.id_hoja_ruta, ca.patente, ca.id_camion, co.nombre_conductor, co.id_conductor, rd.id_punto_servicio, rd.Orden };

                //string html = "<tr><td style='padding:2px;background-color:/*00*/' title='/*011*/'><a href='/MantenedorHDRD/view//*100*/_" + 0 + "'>/*100*/</a></td><td class='ui-droppable getCamion'>/*200*/</td><td class='ui-droppable getConductor'>/*300*/</td><td id='c_1' class='ui-droppable getPuntos' style='width:5%'>/*1*/</td><td  id='c_2'  class='ui-droppable getPuntos'  style='width:5%'>/*2*/</td><td   id='c_3' class='ui-droppable getPuntos'  style='width:5%'>/*3*/</td><td   id='c_4' class='ui-droppable getPuntos'  style='width:5%'>/*4*/</td><td   id='c_5' class='ui-droppable getPuntos'  style='width:5%'>/*5*/</td><td   id='c_6' class='ui-droppable getPuntos'  style='width:5%'>/*6*/</td><td    id='c_7' class='ui-droppable getPuntos'  style='width:5%'>/*7*/</td><td    id='c_8' class='ui-droppable getPuntos'  style='width:5%'>/*8*/</td><td    id='c_9' class='ui-droppable getPuntos'  style='width:5%'>/*9*/</td><td    id='c_10' class='ui-droppable getPuntos'  style='width:5%'>/*10*/</td><td    id='c_11' class='ui-droppable getPuntos'  style='width:5%'>/*11*/</td><td    id='c_12' class='ui-droppable getPuntos'  style='width:5%'>/*12*/</td><td><div id='' class='btn btn-lg btn-primary pull-right GrabarFila' style='margin-top:-1px;font-size:12px;'><img src='/ico/save.png' style='width:25px;height:25px' /></div></td></tr>";
                string html = "<tr><td style='padding:2px;background-color:/*00*/;color:/*01*/' title='/*011*/'><a href='#' onclick='javascript:MostrarDetalle(\"/*100*/\");'>/*100*/</a></td><td class='ui-droppable getCamion'>/*200*/</td><td class='ui-droppable getConductor'>/*300*/</td><td id='c_1' class='ui-droppable getPuntos' style='width:5%'>/*1*/</td><td  id='c_2'  class='ui-droppable getPuntos'  style='width:5%'>/*2*/</td><td   id='c_3' class='ui-droppable getPuntos'  style='width:5%'>/*3*/</td><td   id='c_4' class='ui-droppable getPuntos'  style='width:5%'>/*4*/</td><td   id='c_5' class='ui-droppable getPuntos'  style='width:5%'>/*5*/</td><td   id='c_6' class='ui-droppable getPuntos'  style='width:5%'>/*6*/</td><td    id='c_7' class='ui-droppable getPuntos'  style='width:5%'>/*7*/</td><td    id='c_8' class='ui-droppable getPuntos'  style='width:5%'>/*8*/</td><td    id='c_9' class='ui-droppable getPuntos'  style='width:5%'>/*9*/</td><td    id='c_10' class='ui-droppable getPuntos'  style='width:5%'>/*10*/</td><td    id='c_11' class='ui-droppable getPuntos'  style='width:5%'>/*11*/</td><td    id='c_12' class='ui-droppable getPuntos'  style='width:5%'>/*12*/</td><td><div class='btn btn-lg btn-primary pull-right GrabarFila' style='margin-top:-1px;font-size:12px;'><img src='/ico/save.png' style='width:25px;height:25px' /></div></td></tr>";
                string htmloriginal = html;
                string htmlall = "<tr><th style='width:20px;padding:2px;'>Id</th><th style='width: 25%;'>Camion</th><th style='width: 25%;'>Chofer</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th></th></tr>";
                int inicia = 0;
                int rutaant = -1;

                foreach (var v in q)
                {
                    if (inicia == 0 || v.id_hoja_ruta != rutaant)
                    {
                        if (inicia == 1)
                        {
                            for (int i = 1; i <= 12; i++)
                                html = html.Replace("/*" + i + "*/", "");
                            htmlall += html;
                            html = htmloriginal;
                        }
                        switch (v.estado.ToString())
                        {
                            case "1":
                                html = html.Replace("/*00*/", "Red");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "3":
                                /// html = html.Replace("javascript:MostrarDetalle(\"/*100*/\");", "");
                                html = html.Replace("/*00*/", "#00FF00");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "2":
                                html = html.Replace("/*00*/", "Yellow");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "4":
                                html = html.Replace("/*00*/", "#0000AA");
                                html = html.Replace("/*01*/", "#FFFFFF' class='linkblanco");
                                break;
                            default:
                                html = html.Replace("/*00*/", "Red");
                                html = html.Replace("/*01*/", "Black");
                                break;
                        }
                        //html = html.Replace("/*00*/", (v.estado.ToString() == "1") ? "Red" : "#00FF00");
                        switch (v.estado.ToString())
                        {
                            case "1":
                                html = html.Replace("/*011*/", "PENDIENTE");
                                break;
                            case "2":
                                html = html.Replace("/*011*/", "EN PROCESO");
                                break;
                            case "3":
                                html = html.Replace("/*011*/", "CERRADA");
                                break;
                            case "4":
                                html = html.Replace("/*011*/", "ANULADA");
                                break;
                            default:
                                html = html.Replace("/*011*/", "EN PROCESO");
                                break;
                        }

                        html = html.Replace("/*100*/", v.id_hoja_ruta.ToString());
                        html = html.Replace("/*200*/", v.patente + "<input type='hidden' value='" + v.id_camion + "'>");
                        html = html.Replace("/*300*/", v.nombre_conductor + "<input type='hidden' value='" + v.id_conductor + "'>");
                    }

                    html = html.Replace("/*" + Convert.ToInt32(v.Orden / 1000).ToString() + "*/", "<img id='" + v.id_punto_servicio + "' src='/ico/Yellow.jpg'>" + "/*" + Convert.ToInt32(v.Orden / 1000).ToString() + "*/");

                    rutaant = Convert.ToInt32(v.id_hoja_ruta);
                    inicia = 1;
                }
                if (inicia == 1)
                {
                    for (int i = 1; i <= 12; i++)
                        html = html.Replace("/*" + i + "*/", "");
                    htmlall += html;
                }

                //return View();
                return htmlall;
            }
            else
            {
                int nuevaID = int.Parse(id);
                var q =
        from hr in ctx.hoja_ruta
        join ca in ctx.camiones on hr.id_camion equals ca.id_camion
        join co in ctx.conductores on hr.id_conductor equals co.id_conductor
        join tc in ctx.tipo_servicio on hr.id_tipo_servicio equals tc.id_tipo_servicio
        join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
        join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
        where hr.id_hoja_ruta == nuevaID
        //((ps.lunes == true && Dia == "Lunes") || (ps.martes == true && Dia == "Martes") || (ps.miercoles == true && Dia == "Miercoles") || (ps.jueves == true && Dia == "Jueves") || (ps.viernes == true && Dia == "Viernes") || (ps.sabado == true && Dia == "Sabado") || (ps.domingo == true && Dia == "Domingo"))
        orderby hr.id_hoja_ruta, rd.Orden
        select new { hr.estado, hr.id_hoja_ruta, ca.patente, ca.id_camion, co.nombre_conductor, co.id_conductor, rd.id_punto_servicio, rd.Orden };

                //string html = "<tr><td style='padding:2px;background-color:/*00*/' title='/*011*/'><a href='/MantenedorHDRD/view//*100*/_" + 0 + "'>/*100*/</a></td><td class='ui-droppable getCamion'>/*200*/</td><td class='ui-droppable getConductor'>/*300*/</td><td id='c_1' class='ui-droppable getPuntos' style='width:5%'>/*1*/</td><td  id='c_2'  class='ui-droppable getPuntos'  style='width:5%'>/*2*/</td><td   id='c_3' class='ui-droppable getPuntos'  style='width:5%'>/*3*/</td><td   id='c_4' class='ui-droppable getPuntos'  style='width:5%'>/*4*/</td><td   id='c_5' class='ui-droppable getPuntos'  style='width:5%'>/*5*/</td><td   id='c_6' class='ui-droppable getPuntos'  style='width:5%'>/*6*/</td><td    id='c_7' class='ui-droppable getPuntos'  style='width:5%'>/*7*/</td><td    id='c_8' class='ui-droppable getPuntos'  style='width:5%'>/*8*/</td><td    id='c_9' class='ui-droppable getPuntos'  style='width:5%'>/*9*/</td><td    id='c_10' class='ui-droppable getPuntos'  style='width:5%'>/*10*/</td><td    id='c_11' class='ui-droppable getPuntos'  style='width:5%'>/*11*/</td><td    id='c_12' class='ui-droppable getPuntos'  style='width:5%'>/*12*/</td><td><div id='' class='btn btn-lg btn-primary pull-right GrabarFila' style='margin-top:-1px;font-size:12px;'><img src='/ico/save.png' style='width:25px;height:25px' /></div></td></tr>";
                string html = "<tr><td style='padding:2px;background-color:/*00*/;color:/*01*/' title='/*011*/'><a href='#' onclick='javascript:MostrarDetalle(\"/*100*/\");'>/*100*/</a></td><td class='ui-droppable getCamion'>/*200*/</td><td class='ui-droppable getConductor'>/*300*/</td><td id='c_1' class='ui-droppable getPuntos' style='width:5%'>/*1*/</td><td  id='c_2'  class='ui-droppable getPuntos'  style='width:5%'>/*2*/</td><td   id='c_3' class='ui-droppable getPuntos'  style='width:5%'>/*3*/</td><td   id='c_4' class='ui-droppable getPuntos'  style='width:5%'>/*4*/</td><td   id='c_5' class='ui-droppable getPuntos'  style='width:5%'>/*5*/</td><td   id='c_6' class='ui-droppable getPuntos'  style='width:5%'>/*6*/</td><td    id='c_7' class='ui-droppable getPuntos'  style='width:5%'>/*7*/</td><td    id='c_8' class='ui-droppable getPuntos'  style='width:5%'>/*8*/</td><td    id='c_9' class='ui-droppable getPuntos'  style='width:5%'>/*9*/</td><td    id='c_10' class='ui-droppable getPuntos'  style='width:5%'>/*10*/</td><td    id='c_11' class='ui-droppable getPuntos'  style='width:5%'>/*11*/</td><td    id='c_12' class='ui-droppable getPuntos'  style='width:5%'>/*12*/</td><td><div class='btn btn-lg btn-primary pull-right GrabarFila' style='margin-top:-1px;font-size:12px;'><img src='/ico/save.png' style='width:25px;height:25px' /></div></td></tr>";
                string htmloriginal = html;
                string htmlall = "<tr><th style='width:20px;padding:2px;'>Id</th><th style='width: 25%;'>Camion</th><th style='width: 25%;'>Chofer</th><th>6</th><th>7</th><th>8</th><th>9</th><th>10</th><th>11</th><th>12</th><th>13</th><th>14</th><th>15</th><th>16</th><th>17</th><th></th></tr>";
                int inicia = 0;
                int rutaant = -1;

                foreach (var v in q)
                {
                    if (inicia == 0 || v.id_hoja_ruta != rutaant)
                    {
                        if (inicia == 1)
                        {
                            for (int i = 1; i <= 12; i++)
                                html = html.Replace("/*" + i + "*/", "");
                            htmlall += html;
                            html = htmloriginal;
                        }
                        switch (v.estado.ToString())
                        {
                            case "1":
                                html = html.Replace("/*00*/", "Red");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "3":
                                //  html = html.Replace("javascript:MostrarDetalle(\"/*100*/\");", "");
                                html = html.Replace("/*00*/", "#00FF00");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "2":
                                html = html.Replace("/*00*/", "Yellow");
                                html = html.Replace("/*01*/", "Black");
                                break;
                            case "4":
                                html = html.Replace("/*00*/", "#0000AA");
                                html = html.Replace("/*01*/", "#FFFFFF' class='linkblanco");
                                break;
                            default:
                                html = html.Replace("/*00*/", "Red");
                                html = html.Replace("/*01*/", "Black");
                                break;
                        }
                        //html = html.Replace("/*00*/", (v.estado.ToString() == "1") ? "Red" : "#00FF00");
                        switch (v.estado.ToString())
                        {
                            case "1":
                                html = html.Replace("/*011*/", "PENDIENTE");
                                break;
                            case "2":
                                html = html.Replace("/*011*/", "EN PROCESO");
                                break;
                            case "3":
                                //   html = html.Replace("javascript:MostrarDetalle(\"/*100*/\");", "");
                                html = html.Replace("/*011*/", "CERRADA");
                                break;
                            case "4":
                                html = html.Replace("/*011*/", "ANULADA");
                                break;
                            default:
                                html = html.Replace("/*011*/", "EN PROCESO");
                                break;
                        }

                        html = html.Replace("/*100*/", v.id_hoja_ruta.ToString());
                        html = html.Replace("/*200*/", v.patente + "<input type='hidden' value='" + v.id_camion + "'>");
                        html = html.Replace("/*300*/", v.nombre_conductor + "<input type='hidden' value='" + v.id_conductor + "'>");
                    }

                    html = html.Replace("/*" + Convert.ToInt32(v.Orden / 1000).ToString() + "*/", "<img id='" + v.id_punto_servicio + "' src='/ico/Yellow.jpg'>" + "/*" + Convert.ToInt32(v.Orden / 1000).ToString() + "*/");

                    rutaant = Convert.ToInt32(v.id_hoja_ruta);
                    inicia = 1;
                }
                if (inicia == 1)
                {
                    for (int i = 1; i <= 12; i++)
                        html = html.Replace("/*" + i + "*/", "");
                    htmlall += html;
                }

                //return View();
                return htmlall;
            }

        }



        [HttpPost]
        public string getRutaModelo(sgsbdEntities ctx)
        {
            int IdRutaModelo = int.Parse(Request.Form["IdRutaModelo"].ToString());

            var q =
        from rp in ctx.ruta_punto
        where rp.id_ruta == IdRutaModelo
        orderby rp.Orden
        select new { rp.Orden, rp.id_punto_servicio, rp.puntos_servicio };

            string html = "";



            foreach (var v in q)
            {
                html = html + v.puntos_servicio.nombre_punto + "|" + v.Orden + "_" + v.id_punto_servicio + "_";
            }

            if (html != "")
                html = html.Substring(0, html.Length - 1);
            return html;
        }



        [HttpPost]
        public string getPuntos(sgsbdEntities ctx)
        {
            int TipoServicio = int.Parse(Request.Form["TipoServicio"].ToString());
            CultureInfo provider = CultureInfo.InvariantCulture;
            string Fecha = DateTime.ParseExact(Request.Form["Fecha"].ToString(), "dd/MM/yyyy", provider).ToString();
            BD Conexion = new BD();
            DataTable Datos;
            Datos = Conexion.ExecQry(string.Format("Select P.*,ps.nombre_punto from (select distinct IndividualDate,id_punto_servicio from fechas f,contratos c,puntos_servicio p where f.IndividualDate>=c.fecha_inicio and f.IndividualDate<=convert(datetime,'{0}',103) and c.id_contrato=p.id_contrato and c.id_tipo_contrato={1} and ((p.lunes=1 and datepart(weekday,f.IndividualDate)=2) or (p.martes=1 and datepart(weekday,f.IndividualDate)=3) or (p.miercoles=1 and datepart(weekday,f.IndividualDate)=4) or (p.jueves=1 and datepart(weekday,f.IndividualDate)=5) or (p.viernes=1 and datepart(weekday,f.IndividualDate)=6) or (p.sabado=1 and datepart(weekday,f.IndividualDate)=7) or (p.domingo=1 and datepart(weekday,f.IndividualDate)=1)) except Select fecha,id_punto_servicio from hoja_ruta_detalle hrd except Select fecha,id_punto_servicio from hoja_ruta_atrasados) p,puntos_servicio ps Where p.id_punto_servicio=ps.id_punto_servicio", Fecha, TipoServicio));
            String HTML = "";
            if (Datos != null)
            {
                for (int i = 0; i < Datos.Rows.Count; i++)
                {
                    if (DateTime.Parse(Datos.Rows[i].ItemArray[0].ToString()) == DateTime.Parse(Fecha))
                        HTML = HTML + "<li class='lipuntos Amarillo' style='padding:10px 10px 10px 10px;'>";
                    else
                        HTML = HTML + "<li class='lipuntos Rojo' style='padding:10px 10px 10px 10px;'>";
                    HTML = HTML + "<div>";
                    if (DateTime.Parse(Datos.Rows[i].ItemArray[0].ToString()) == DateTime.Parse(Fecha))
                        HTML = HTML + string.Format("<div class='punto' id='punto_{0}'  style='float:left;margin-top:-3px;'><img class='imagenx' id='{0}' title='{1}' src='/ico/Yellow.jpg'/></div>", Datos.Rows[i].ItemArray[1].ToString(), Datos.Rows[i].ItemArray[0].ToString());
                    else
                        HTML = HTML + string.Format("<div class='punto' id='punto_{0}'  style='float:left;margin-top:-3px;'><img class='imagenx' id='{0}' title='{1}' src='/ico/Red.jpg'/></div>", Datos.Rows[i].ItemArray[1].ToString(), Datos.Rows[i].ItemArray[0].ToString());
                    HTML = HTML + string.Format("<span id='quitarpunto' class='desc nompun'>{0}</span>", Datos.Rows[i].ItemArray[2].ToString());
                    if (DateTime.Parse(Datos.Rows[i].ItemArray[0].ToString()) == DateTime.Parse(Fecha))
                        //HTML = HTML + String.Format("<a href='javascript:filtrarEstado(\"Amarillo\")'><span  id='coloreado_{0}'  class='EstadoPunto label label-today'>Pendiente</span></a>", Datos.Rows[i].ItemArray[1].ToString());
                        HTML = HTML + String.Format("<span onclick='javascript:filtrarEstado(\"Amarillo\")' ondblclick='javascript:EliminarPuntoAtrasado(this)' id='coloreado_{0}'  class='EstadoPunto label label-today'>Pendiente</span>", Datos.Rows[i].ItemArray[1].ToString());
                    else
                        //HTML = HTML + String.Format("<a href='javascript:filtrarEstado(\"Rojo\")'><span  id='coloreado_{0}'  class='EstadoPunto label label-important'>Atrasado</span></a>", Datos.Rows[i].ItemArray[1].ToString());
                        HTML = HTML + String.Format("<span onclick='javascript:filtrarEstado(\"Rojo\")' ondblclick='javascript:EliminarPuntoAtrasado(this)' id='coloreado_{0}'  class='EstadoPunto label label-important'>Atrasado</span>", Datos.Rows[i].ItemArray[1].ToString());
                    HTML = HTML + "</li></div>";
                    HTML = HTML + "</li>";

                }
            }
            return HTML;
        }


        [HttpPost]
        public string getPuntosTodos(sgsbdEntities ctx)
        {
            var q = from p in ctx.puntos_servicio
                    orderby p.nombre_punto
                    select new { p.nombre_punto, p.id_punto_servicio };

            string html = "";

            foreach (var v in q)
            {
                html = html + "<li class='lipuntos Amarillo' style='padding:10px 10px 10px 10px;'>";
                html = html + string.Format("<div class='punto' id='punto_{0}'  style='float:left;margin-top:-3px;'><img id='{0}' title='{1}' src='/ico/Green.jpg'/></div>", v.id_punto_servicio, "");
                html = html + string.Format("<span class='desc nompun'>{0}</span>", v.nombre_punto);
                // html = html + String.Format("<a href='javascript:filtrarEstado(\"Amarillo\")'><span  id='coloreado_{0}'  class='EstadoPunto label label-today'>Pendiente</span></a>", v.id_punto_servicio);
                html = html + "</li>";
            }
            return html;
        }

        [HttpPost]
        public string EliminarPuntoAtrasado(sgsbdEntities ctx)
        {

            int punto = int.Parse(Request.Form["punto"].ToString());
            DateTime fechax = DateTime.Parse(Request.Form["fecha"].ToString());

            hoja_ruta_atrasados puntosatrasados = new hoja_ruta_atrasados
            {
                id_punto_servicio = punto,
                fecha = fechax
            };
            ctx.hoja_ruta_atrasados.Add(puntosatrasados);
            ctx.SaveChanges();
            return "";
        }

        [HttpPost]
        public string getRutasModelo(sgsbdEntities ctx)
        {
            int TipoServicio = int.Parse(Request.Form["TipoServicio"].ToString());
            RutasModeloRepository rs6 = new RutasModeloRepository(ctx);
            ViewData["rutasmodelo"] = rs6.GetRutasModeloXTipoServicio(TipoServicio);
            return "";
        }

        private void buildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            //TipoLicenciaRepository rs = new TipoLicenciaRepository(ctx);
            //FranquiciadosRepository rs1 = new FranquiciadosRepository(ctx);
            PuntosServiciosRepository rs2 = new PuntosServiciosRepository(ctx);
            CamionesRepository rs3 = new CamionesRepository(ctx);
            ConductoresRepository rs4 = new ConductoresRepository(ctx);
            PuntosServiciosRepository rs5 = new PuntosServiciosRepository(ctx);
            RutasModeloRepository rs6 = new RutasModeloRepository(ctx);
            TipoServicioRepository ts = new TipoServicioRepository(ctx);


            IEnumerable<object> estado = new object[] {
             new { text="Activo",value="Activo"},
             new { text="Inactivo",value="Inactivo"},

            };

            //ViewBag.franquiciado = new SelectList(rs1.GetFranquiciados().OrderBy(m => m.id_franquiciado), "id_franquiciado", "razon_social");
            //ViewBag.licencia = new SelectList(rs.GetTipoLicencias().OrderBy(m => m.tipo_licencia1), "id_tipo_licencia", "tipo_licencia1");
            //ViewBag.estado = new SelectList(estado, "value", "text", ViewBag.value_estado);

            ViewBag.tipo_servicio = new SelectList(rs2.GetPuntosServicios().OrderBy(m => m.nombre_punto).Where(m => m.cobro_por_banos == true), "id_punto_servicio", "nombre_punto");
            if (Session["bano"].ToString() == "1")
            {
                ViewBag.tipo_servicio_real = new SelectList(ts.GetTiposServicios().OrderBy(m => m.tipo_servicio1).Where(m => m.tipo_servicio1 == "Baño"), "id_tipo_servicio", "tipo_servicio1");
            }
            else
            {
                ViewBag.tipo_servicio_real = new SelectList(ts.GetTiposServicios().OrderBy(m => m.tipo_servicio1), "id_tipo_servicio", "tipo_servicio1");
            }
            //ViewBag.camiones = new SelectList(rs3.GetCamiones().OrderBy(m => m.id_camion), "id_camion", "camion");
            //ViewData["camiones"] = rs3.GetCamiones();
            ViewData["camiones"] = rs3.getCamionesWithAll2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            //ViewData["camiones"] = rs3.getCamionesWithAll2();
            ViewData["conductores"] = rs4.GetConductores(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            //ViewData["conductores"] = rs4.GetConductores();
            ViewData["puntos"] = rs5.GetPuntosServicios_TipoServicio_View(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            ViewData["rutasmodelo"] = rs6.GetRutasModelo(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
        }



        [HttpPost]
        public ActionResult ModificarHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            string[] puntos = Request.Form["puntos"].ToString().Split('|');
            int Ruta, Punto, Orden, Cantidad, Peso, Estado, tiposervicio;
            string equipoentrada, equiposalida, horainicio, horatermino, observacion, vertedero;

            bool cambio = false;
            bool EnProceso = true;
            bool Cerrada = true;
            int iidruta = 0;
            for (int j = 0; j < puntos.Length; j++)
            {
                String[] unpunto = puntos[j].Split('_');
                Ruta = int.Parse(unpunto[0]);
                iidruta = Ruta;
                Punto = int.Parse(unpunto[1]);
                Orden = int.Parse(unpunto[2]);
                Cantidad = (unpunto[3] != "") ? int.Parse(unpunto[3]) : -1;
                Peso = (unpunto[4] != "") ? int.Parse(unpunto[4]) : -1;
                tiposervicio = int.Parse((unpunto[6] == "") ? "-1" : unpunto[6]);
                equipoentrada = unpunto[7];
                equiposalida = unpunto[8];
                horainicio = unpunto[9];
                horatermino = unpunto[10];
                observacion = unpunto[9];
                vertedero = unpunto[10];
                Estado = int.Parse((unpunto[5] == "") ? "1" : unpunto[5]);
                List<hoja_ruta_detalle> hoja = ctx.hoja_ruta_detalle.Where(i => i.id_hoja_ruta == Ruta && i.Orden == Orden).ToList();
                if (Cantidad != -1)
                    hoja[0].cantidad = Cantidad;
                else
                    hoja[0].cantidad = null;
                if (Peso != -1)
                    hoja[0].peso = Peso;
                else
                    hoja[0].peso = null;
                if (Estado == 1 && (Cantidad != -1 || Peso != -1))
                    hoja[0].estado = 2;
                else
                    hoja[0].estado = Estado;
                if (hoja[0].estado != 1) cambio = true;
                if (hoja[0].estado != 1) EnProceso = false;
                if (hoja[0].estado == 1) Cerrada = false;
                if (tiposervicio != -1)
                    hoja[0].id_tipo_servicio = tiposervicio;
                hoja[0].equipoentrada = equipoentrada;
                hoja[0].equiposalida = equiposalida;
                //hoja[0].horainicio = horainicio;
                //hoja[0].horatermino = horatermino;
                hoja[0].observacion = observacion;
                hoja[0].relleno = vertedero;

                ctx.hoja_ruta_detalle.Attach(hoja[0]);
                ctx.Entry(hoja[0]).State = System.Data.EntityState.Modified;
                ctx.SaveChanges();
            }

            List<hoja_ruta> hoja2 = ctx.hoja_ruta.Where(i => i.id_hoja_ruta == iidruta).ToList();
            if (EnProceso)
                hoja2[0].estado = 1;
            else if (Cerrada)
                hoja2[0].estado = 3;
            else
                hoja2[0].estado = 2;
            ctx.hoja_ruta.Attach(hoja2[0]);
            ctx.Entry(hoja2[0]).State = System.Data.EntityState.Modified;
            ctx.SaveChanges();

            return View();
        }


    }
}