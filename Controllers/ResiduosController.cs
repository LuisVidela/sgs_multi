﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;


namespace SGS.Controllers
{
    public class ResiduosController : Controller
    {
        //
        // GET: /Documentos/

        public ActionResult Index()
        {
           ResiduosRepository rs = new ResiduosRepository(new sgsbdEntities());
           List<ResiduosView> data = (List<ResiduosView>)rs.GetResiduosView();

            return View(data);
               
           
        }

        public ActionResult Create()
        {
                      return View();
        }

        [HttpPost]
        public ActionResult Create(residuos model)
        {
            if (ModelState.IsValid)
            {
                ResiduosRepository rs = new ResiduosRepository(new sgsbdEntities());
                model.id_unidad = Models.repository.UsuariosRepository.getUnidadSeleccionada();
                if (model.id_residuo == 0)
                {   
                    rs.InsertResiduo(model);
                }
                else
                {
                    rs.UpdateResiduo(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Residuos");
            }
            return View(model);
        }

        public ActionResult edit(int id)
        {

            ResiduosRepository rs = new ResiduosRepository(new sgsbdEntities());
            residuos c = rs.GetResiduoByID(id);
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {

            ResiduosRepository rs = new ResiduosRepository(new sgsbdEntities());
            rs.DeleteResiduo(id);
            rs.Save();
            return this.RedirectToAction("Index", "Residuos");

        }

        public ActionResult print(int id)
        {

            ResiduosRepository rs = new ResiduosRepository(new sgsbdEntities());
          
            return View("view",rs);
        }

   

}
}

 