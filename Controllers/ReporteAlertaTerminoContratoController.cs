﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.modelViews;
using SGS.Models.repository;
using System.Data.Objects.SqlClient;
using WebMatrix.WebData;

namespace SGS.Controllers
{
    public class ReporteAlertaTerminoContratoController : Controller
    {
        //
        // GET: /ReporteAlertaTerminoContrato/

        public ActionResult Index()
        {
            sgsbdEntities db = new sgsbdEntities();
            //ContratosRepository rs = new ContratosRepository(new SGS.Models.sgsbdEntities());
            //List<ContratosView> data = (List<ContratosView>)rs.GetContratosView2();
            List<sp_calcular_vencimiento_Result1> data = db.sp_calcular_vencimiento().OrderBy(m => m.dias_termino).ToList();


            return View(data);
        }
        public ActionResult Excel()
        {

            ContratosRepository rs = new ContratosRepository(new SGS.Models.sgsbdEntities());
            List<ContratosView> data = (List<ContratosView>)rs.GetContratosView2();

            return View(data);
        }
      
    }
}
