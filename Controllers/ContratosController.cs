﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.modelViews;
using SGS.Models.repository;
using WebMatrix.WebData;

namespace SGS.Controllers
{
    public class ContratosController : Controller
    {
        
        public ActionResult Index()
        {
            ContratosRepository rs = new ContratosRepository(new SGS.Models.sgsbdEntities());
            List<ContratosView> data = (List<ContratosView>)rs.GetContratosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            return View(data);
        }
        public ActionResult Excel()
        {

            ContratosRepository rs = new ContratosRepository(new SGS.Models.sgsbdEntities());
            List<ContratosView> data = (List<ContratosView>)rs.GetContratosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }
        public ActionResult create()
        {
            @ViewBag.existe = "hidden";
            @ViewBag.nuevo = "visible";

            this.buildViewBagList();
            return View();
        }
        


        [HttpPost]
        public ActionResult create(FormCollection form, contratos model, string estado)
        {

            var fecha_inicio = form.Get("fecha_inicio");
            var fecha_renovacion = form.Get("fecha_renovacion");

            ModelState.Remove("fecha_renovacion");
            ModelState.Remove("fecha_inicio");

            //string[]  fi = null;
            //string[] fr = null;

            if (fecha_inicio == "")
            {
                ModelState.AddModelError("fecha_inicio", "requerido");   
            }
            //else {
            //    fi = fecha_inicio.ToString().Replace("-", "/").Split('/');
            //}
            if (fecha_renovacion == "")
            {
                ModelState.AddModelError("fecha_renovacion", "requerido");
            }
            //else {
            //    fr = fecha_renovacion.ToString().Replace("-", "/").Split('/');
            //}
            @ViewBag.fecha_inicio = model.fecha_inicio;

            @ViewBag.fecha_renovacion = model.fecha_renovacion;

            @ViewBag.existe = "hidden";
            @ViewBag.nuevo = "visible";

            @ViewBag.Begin = @ViewBag.fecha_inicio;
            @ViewBag.End = @ViewBag.fecha_renovacion;

            if (ModelState.IsValid)
            {

                model.fecha_renovacion = Convert.ToDateTime(fecha_renovacion);
                model.fecha_inicio = Convert.ToDateTime(fecha_inicio) ;
                model.estado = estado;
                ContratosRepository rs = new ContratosRepository(new sgsbdEntities());

                List<Int32> month = new List<int>();
                List<Int32> indicator = new List<int>();
                for (int i = 0; i < form.Keys.Count; i++)
                {
                    var key = form.Keys[i];
                    if (key.Contains("mes"))
                    {
                        month.Add(Convert.ToInt32(key.Split('-')[1]));
                    }
                    else if (key.Contains("indicador"))
                    {
                        indicator.Add(Convert.ToInt32(key.Split('-')[1]));
                    }

                }
                sgsbdEntities db = new sgsbdEntities();
                model.id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var sucursal = Convert.ToInt32(form.Get("id_sucursal"));
                var id_cliente = db.sucursales.Where(x => x.id_sucursal == sucursal).Select(x => x.id_cliente).FirstOrDefault().ToString();
                model.id_cliente = Convert.ToInt32(id_cliente);
                if (model.id_contrato == 0)
                {
                        model.estado = "Activo";
                        rs.InsertContrato(model);
                        rs.Save();
                        //-----------------------------------------------------------------------
                        //---------inserción reajuste--------------------------------------------
                        //-----------------------------------------------------------------------
                        #region inserta reajuste
                        TB_SGS_REAJUSTE reajuste = db.TB_SGS_REAJUSTE.Create();
                        reajuste.REAJ_ID_CONTRATO = model.id_contrato;
                        reajuste.REAJ_FECHA_MODIFICACION = DateTime.Now;
                        reajuste.REAJ_USERID = WebSecurity.GetUserId(User.Identity.Name);
                        db.TB_SGS_REAJUSTE.Add(reajuste);
                        db.SaveChanges();

                        foreach (Int32 item in month)
                        {
                            TB_SGS_REAJUSTE_DETALLE detalle = db.TB_SGS_REAJUSTE_DETALLE.Create();
                            detalle.READ_MES = item;
                            detalle.READ_REAJ_CODIGO = reajuste.REAJ_CODIGO;
                            db.TB_SGS_REAJUSTE_DETALLE.Add(detalle);
                            db.SaveChanges();
                        }

                        foreach (Int32 item in indicator)
                        {
                            TB_SGS_REAJUSTE_INDICADOR indi = db.TB_SGS_REAJUSTE_INDICADOR.Create();
                            indi.REIN_INDI_CODIGO = item;
                            indi.REIN_REAJ_CODIGO = reajuste.REAJ_CODIGO;
                            db.TB_SGS_REAJUSTE_INDICADOR.Add(indi);
                            db.SaveChanges();
                        }
                        #endregion
                        //-----------------------------------------------------------------------
                        //-----------------------------------------------------------------------
                        //-----------------------------------------------------------------------

                        @ViewBag.existe = "visible";
                        @ViewBag.nuevo = "hidden"; 
                }
                else
                {
                    model.estado = "Activo";
                    
                    rs.UpdateContrato(model);

                    //-----------------------------------------------------------------------
                    //---------inserción reajuste--------------------------------------------
                    //-----------------------------------------------------------------------
                    //Busca el reajuste si no tiene lo crea
                    TB_SGS_REAJUSTE reajuste = db.TB_SGS_REAJUSTE.FirstOrDefault(o => o.REAJ_ID_CONTRATO == model.id_contrato);
                    if (reajuste == null)
                    {
                        reajuste = db.TB_SGS_REAJUSTE.Create();
                        reajuste.REAJ_ID_CONTRATO = model.id_contrato;
                        reajuste.REAJ_USERID = WebSecurity.GetUserId(User.Identity.Name);
                        reajuste.REAJ_FECHA_MODIFICACION = DateTime.Now;
                        db.TB_SGS_REAJUSTE.Add(reajuste);
                        db.SaveChanges();
                    }

                    #region configuracion de los indicadores
                    //busca los indicadores del reajuste configurados
                    List<TB_SGS_REAJUSTE_INDICADOR> delIndicator = (from o in db.TB_SGS_REAJUSTE_INDICADOR
                                                                     where o.REIN_REAJ_CODIGO == reajuste.REAJ_CODIGO
                                                                     select o).ToList();
                    foreach (TB_SGS_REAJUSTE_INDICADOR item in delIndicator)
                    {
                        //busca si esta dentro de los que hay que dejar configurado si no lo elimina de la lista para crear el resto.
                        Int32 result = indicator.FirstOrDefault(o => o == item.REIN_INDI_CODIGO);
                        if (result == null || result == 0)
                        {
                            //emimina el indicador que no debe estar configurado.
                            db.TB_SGS_REAJUSTE_INDICADOR.Remove(item);
                            db.SaveChanges();
                        }
                        else
                        {
                            //lo elimina de la lista para no crearlo mas abajo.
                            indicator.Remove(result);
                        }
                    }
                    //crea los indicadores que no estaban configurados
                    foreach (Int32 item in indicator)
                    {
                        TB_SGS_REAJUSTE_INDICADOR indi = db.TB_SGS_REAJUSTE_INDICADOR.Create();
                        indi.REIN_INDI_CODIGO = item;
                        indi.REIN_REAJ_CODIGO = reajuste.REAJ_CODIGO;
                        db.TB_SGS_REAJUSTE_INDICADOR.Add(indi);
                        db.SaveChanges();
                    }
                    #endregion

                    #region configuracion de meses de reajuste
                    List<TB_SGS_REAJUSTE_DETALLE> delReajusteDetalle = (from o in db.TB_SGS_REAJUSTE_DETALLE
                                                                        where o.READ_REAJ_CODIGO == reajuste.REAJ_CODIGO
                                                                        select o).ToList();

                    foreach (TB_SGS_REAJUSTE_DETALLE item in delReajusteDetalle)
                    {
                        //busca si esta dentro de los que hay que dejar configurado si no lo elimina de la lista para crear el resto.
                        Int32 result = month.FirstOrDefault(o => o == item.READ_MES);
                        if (result == null || result == 0)
                        {
                            //emimina el indicador que no debe estar configurado.
                            db.TB_SGS_REAJUSTE_DETALLE.Remove(item);
                            db.SaveChanges();
                        }
                        else
                        {
                            //lo elimina de la lista para no crearlo mas abajo.
                            month.Remove(result);
                        }
                    }
                    //crea los indicadores que no estaban configurados
                    foreach (Int32 item in month)
                    {
                        TB_SGS_REAJUSTE_DETALLE detalle = db.TB_SGS_REAJUSTE_DETALLE.Create();
                        detalle.READ_MES = item;
                        detalle.READ_REAJ_CODIGO = reajuste.REAJ_CODIGO;
                        db.TB_SGS_REAJUSTE_DETALLE.Add(detalle);
                        db.SaveChanges();
                    }
                    #endregion

                    //-----------------------------------------------------------------------
                    //-----------------------------------------------------------------------
                    //-----------------------------------------------------------------------

                        @ViewBag.existe = "visible";
                        @ViewBag.nuevo = "hidden"; 

                    
                }
                rs.Save();
                return RedirectToAction("Index", "Contratos");
            }

            ViewBag.edit = false;
            this.buildViewBagList();
            return View(model);
        }
        
        public ActionResult createflow(int id)
        {

            @ViewBag.existe = "hidden";
            @ViewBag.nuevo = "visible";

            SucursalesRepository sucu = new SucursalesRepository(new SGS.Models.sgsbdEntities());
            sucursales sucis = sucu.GetSucursalByID(id);
            ViewBag.Sucuname = sucis.sucursal.Trim();

            contratos contra = new contratos();
            contra.id_cliente = Convert.ToInt16(sucis.id_cliente);
            contra.id_sucursal = id;

            this.buildViewBagList();
            return View(contra);

        }

        [HttpPost]
        public ActionResult createflow(FormCollection form, contratos model, string estado)
        {

            //var fecha_inicio = form.Get("fecha_inicio");
            //var fecha_renovacion = form.Get("fecha_fin");

            var fecha_inicio = form.Get("Begin");
            var fecha_renovacion = form.Get("End");

            //var fecha_inicio = model.fecha_inicio.ToString("dd/MM/yy");
            //var fecha_renovacion = model.fecha_renovacion.ToString();

            ModelState.Remove("fecha_renovacion");
            ModelState.Remove("fecha_inicio");

            if (fecha_inicio == "") { ModelState.AddModelError("fecha_inicio","requerido"); }
            if (fecha_renovacion == "" || fecha_renovacion == null) { 
                
                ModelState.AddModelError("fecha_renovacion", "requerido"); 
            
            }
            @ViewBag.fecha_inicio = model.fecha_inicio;
            //@ViewBag.fecha_inicio = fecha_inicio;

            var fi=fecha_inicio.Split('/');
            var fr=fecha_renovacion.Split('/');
            

            
            @ViewBag.fecha_renovacion = model.fecha_renovacion;

            @ViewBag.existe = "hidden";
            @ViewBag.nuevo = "visible";

            @ViewBag.Begin = @ViewBag.fecha_inicio;
            @ViewBag.End = @ViewBag.fecha_renovacion;

            

            if (ModelState.IsValid)
            {
                model.fecha_renovacion = new DateTime(int.Parse(fr[2]), int.Parse(fr[0]), int.Parse(fr[1]));
                model.fecha_inicio = new DateTime(int.Parse(fi[2]), int.Parse(fi[0]), int.Parse(fi[1]));

                model.estado = estado;
                ContratosRepository rs = new ContratosRepository(new sgsbdEntities());
                ClientesRepository cli = new ClientesRepository(new sgsbdEntities());

                clientes cliente = new clientes();
                cliente = cli.GetClienteByID(model.id_cliente);
                ViewBag.ClienteIs = cliente.razon_social.Trim();

                if (model.id_contrato == 0)
                {
                    using (var db = new sgsbdEntities())
                    {
                        var contrato = db.contratos.Create();
                        contrato.fecha_inicio = model.fecha_inicio;
                        contrato.fecha_renovacion = model.fecha_renovacion;
                        contrato.id_tipo_contrato = model.id_tipo_contrato;
                        contrato.id_cliente = model.id_cliente;
                        contrato.id_sucursal = model.id_sucursal;
                        contrato.id_unidad = model.id_unidad;
                        contrato.estado = "Activo";
                        contrato.id_centro_responsabilidad = model.id_centro_responsabilidad;
                        db.contratos.Add(contrato);
                        db.SaveChanges();

                        model.id_contrato = contrato.id_contrato;

                        @ViewBag.existe = "visible";
                        @ViewBag.nuevo = "hidden";

                    }
                }
                else
                {
                    using (var db = new sgsbdEntities())
                    {
                        var contrato = db.contratos.Where(u => u.id_contrato == model.id_contrato).FirstOrDefault();
                        contrato.fecha_inicio = model.fecha_inicio;
                        contrato.fecha_renovacion = model.fecha_renovacion;
                        contrato.id_tipo_contrato = model.id_tipo_contrato;
                        contrato.id_cliente = model.id_cliente;
                        contrato.id_sucursal = model.id_sucursal;
                        contrato.id_unidad = model.id_unidad;
                        contrato.id_centro_responsabilidad = model.id_centro_responsabilidad;
                        db.SaveChanges();

                        @ViewBag.existe = "visible";
                        @ViewBag.nuevo = "hidden";

                    }
                }

                this.buildViewBagList();
                return RedirectToAction("createflow", "PuntosServicios", new { id = model.id_contrato, suc = model.id_sucursal });
            }

            this.buildViewBagList();
            return View();

        }

        public ActionResult edit(int id)
        {
            @ViewBag.existe = "visible";
            @ViewBag.nuevo = "hidden";

            sgsbdEntities ctx = new sgsbdEntities();
            ContratosRepository rs = new ContratosRepository(new sgsbdEntities());
            contratos c = rs.GetContratoByID(id);
            ViewBag._estado= c.estado;
            this.buildViewBagList();
            ViewBag.sucursales = ctx.sucursales.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.sucursal,
                Value = x.id_sucursal.ToString(),
            }).OrderBy(x => x.Text);

            @ViewBag.fecha_inicio = c.fecha_inicio;
            @ViewBag.fecha_renovacion = c.fecha_renovacion;

            @ViewBag.Begin = @ViewBag.fecha_inicio;
            @ViewBag.End = @ViewBag.fecha_renovacion;
            ViewBag.edit = true;
            ClientesRepository cli = new ClientesRepository(new sgsbdEntities());

            clientes cliente = new clientes();
            cliente = cli.GetClienteByID(c.id_cliente);
            ViewBag.ClienteIs = cliente.razon_social.Trim();

            TB_SGS_REAJUSTE reajuste = (from cto in ctx.TB_SGS_REAJUSTE
                                        where cto.REAJ_ID_CONTRATO == id
                                        select cto).FirstOrDefault();
            ViewBag.ReajusteContrato = reajuste;
          

            return View("create", c);
        }


        public ActionResult view(int id)
        {
            ContratosRepository rs = new ContratosRepository(new sgsbdEntities());
            ContratosView c = rs.GetContratosView(id);
            buildViewBagList();
            return View(c);

        }

        //public ActionResult Delete(int id)
        //{

        //    ContratosRepository rs = new ContratosRepository(new sgsbdEntities());
        //    rs.DeleteContrato(id);
        //    rs.Save();
        //    return this.RedirectToAction("Index", "Contratos");

        //}



        private void buildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            ClientesRepository rs1 = new ClientesRepository(ctx);
            TipoContratoRepository rs2 = new TipoContratoRepository(ctx);
            ModeloCobrosRepository rs3 = new ModeloCobrosRepository(ctx);
            IndicadoresRepository rs4 = new IndicadoresRepository(ctx);
            SucursalesRepository rs5 = new SucursalesRepository(ctx);


            IEnumerable<object> rsestado = new object[] {
             new { text="Activa",value="Activa"},
             new { text="Inactiva",value="Inactiva"}
            };

            ViewBag.centros = ctx.centro_responsabilidad.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.centro_responsabilidad1,
                Value = x.id_centro.ToString(),
            }).OrderBy(x => x.Text);



            //ViewBag.sucursales = ctx.sucursales.AsEnumerable().Select(x => new SelectListItem
            //{
            //    Text = x.sucursal,
            //    Value = x.id_sucursal.ToString(),
            //}).OrderBy(x => x.Text);

            ViewBag.sucursales = new SelectList(rs5.GetSucursalesForSelectByUnidad(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()),"id_sucursal", "sucursal" );
            ViewBag.clientes = new SelectList(rs1.GetClientesForListByUN(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "id_cliente", "razon_social");
            ViewBag.tipo_contrato = new SelectList(rs2.GetTipoContratos(), "id_tipo_contrato", "tipo_contrato1");
            ViewBag.estado = new SelectList(rsestado, "value", "text", ViewBag._estado);
            ViewBag.modelos = new SelectList(rs3.GetModeloCobros(), "id_modelo", "modelo");
            ViewBag.indicadores = rs4.Get().ToList();
        }

    }
}
