﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using System.Data;

namespace SFS.Controllers
{
    public class CredencialesController : Controller
    {

        sgsbdEntities ctx = new sgsbdEntities();
        public ActionResult Index()
        {


            List<webpages_Roles> data = ctx.webpages_Roles.OrderBy(x => x.RoleName).ToList();
            return View(data);
        }

        private void buildViewBag()
        {
            ViewBag.Modulos = new SelectList(ctx.modulos.OrderBy(m=>m.nombre).ToList(),"id","nombre");
        }

        public ActionResult Create()
        {
            buildViewBag();
            return View();
        }

        [HttpPost]
        public ActionResult Create(webpages_Roles model)
        {

            if (ModelState.IsValid)
            {



                if (model.RoleId == 0)
                {

                    ctx.webpages_Roles.Add(model);

                }
                else
                {
                    var entry = ctx.Entry(model);
                    entry.State = EntityState.Modified;
                }

                ctx.SaveChanges();
                return RedirectToAction("Index", "Credenciales");
            }

            buildViewBag();
            return View(model);

        }


        public ActionResult edit(int id)
        {
            webpages_Roles model = ctx.webpages_Roles.Where(p => p.RoleId == id).FirstOrDefault();
            buildViewBag();
            return View("Create", model);

        }


        public ActionResult Delete(int id)
        {


            webpages_Roles model = ctx.webpages_Roles.Where(p => p.RoleId == id).FirstOrDefault();
            ctx.webpages_Roles.Remove(model);
            ctx.SaveChanges();
            return RedirectToAction("Index", "Credenciales");

        }



    }
}
