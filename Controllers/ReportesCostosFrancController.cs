﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using RazorPDF;
using iTextSharp;
using iTextSharp.text.pdf;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using System.IO;
using iTextSharp.text.factories;
using System.Globalization;

namespace SGS.Controllers
{
    public class ReportesCostosFrancController : Controller
    {

        public ActionResult Index()
        {

            CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());
            List<CobroClientesView> dot = (List<CobroClientesView>)rs4.getCobroClientes("000000");
            return View(dot);

        }
           
        public ActionResult excel()
        {
            CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());

            List<CobroClientesFranquiciaView> dot = (List<CobroClientesFranquiciaView>)rs4.getListadoCobrosClientesFranquiciados();

            return View(dot);
        }


        [HttpPost]
        public ActionResult Index(FormCollection collection)
        {

                if (String.IsNullOrEmpty(collection.Get("Begin").ToString().Trim()))
                {
                    ViewBag.Error = "INICIO";
                    ViewBag.When = "";
                    return View();
                }

                if (String.IsNullOrEmpty(collection.Get("End").ToString().Trim()))
                {
                    ViewBag.Error = "FIN";
                    ViewBag.When = "";
                    return View();
                }

                string fecha1 = String.Format("{0:yyyy-mm-dd HH:mm:ss}", collection.Get("Begin"));
                string fecha2 = String.Format("{0:yyyy-mm-dd HH:mm:ss}", collection.Get("End"));

                DateTime dateTime;
                try
                {
                    DateTime.TryParse(fecha1, out dateTime);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "INICIONO";
                    ViewBag.When = "";
                    return View();
                }

                try
                {
                    DateTime.TryParse(fecha2, out dateTime);
                }
                catch (Exception ex)
                {
                    ViewBag.Error = "FINNO";
                    ViewBag.When = "";
                    return View();
                }

                string mes = fecha1.Substring(0, 2);
                string dia = fecha1.Substring(3, 2);
                string ano = fecha1.Substring(6, 4);

                System.DateTime inicio = new DateTime(Convert.ToInt16(ano), Convert.ToInt16(mes), Convert.ToInt16(dia));
                ViewBag.fecha_inicio = inicio;

                string mes2 = fecha2.Substring(0, 2);
                string dia2 = fecha2.Substring(3, 2);
                string ano2 = fecha2.Substring(6, 4);

                System.DateTime fin = new DateTime(Convert.ToInt16(ano2), Convert.ToInt16(mes2), Convert.ToInt16(dia2));
                ViewBag.fecha_fin = fin;

                string fechinist = String.Format("{0:yyyy-mm-dd}", ano + "-" + mes + "-" + dia);
                string fechfinst = String.Format("{0:yyyy-mm-dd}", ano2 + "-" + mes2 + "-" + dia2);

                ViewBag.Inicio = mes + "-" + dia + "-" + ano;
                ViewBag.fin = mes2 + "-" + dia2 + "-" + ano2;

                if (inicio > fin)
                {
                    ViewBag.Error = "ERRORFECHA";
                    ViewBag.When = "";
                    return View();
                }

                if (((fin - inicio).TotalDays) > 30)
                {
                    ViewBag.Error = "MES30";
                    ViewBag.When = "";
                    return View();
                }

                CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());

                // CALCULO DE COBRO VERSUS FRANQUICIAS
                rs4.CalculoCobroClientesFranquiciados(fechinist, fechfinst);

                List<CobroClientesFranquiciaView> dot = (List<CobroClientesFranquiciaView>)rs4.getListadoCobrosClientesFranquiciados();

                if (dot.Count == 0)
                {
                    string MesError = MesNombre(mes) + " " + ano;
                    ViewBag.Error = "ERROR";
                    ViewBag.When = " " + MesError;
                }
                else
                {
                    ViewBag.Error = "";
                    ViewBag.When = "";
                }
                return View(dot);

        }

        public ActionResult Puntos()
        {

            @ViewBag.Visible = null;

            DataCalculosFullView Modelo = new DataCalculosFullView();
            Modelo = null;

            //--------------------------------------------------------------------------------------
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });

            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());

            List<PuntosServiciosView> dot = (List<PuntosServiciosView>)rs.GetPuntosServiciosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            foreach (var Xvar in dot)
            {
                string idPunto = Xvar.id_punto_servicio.ToString();
                string nombrePunto = Xvar.nombre_punto.Trim();
                items.Add(new SelectListItem { Text = nombrePunto, Value = idPunto });
            };
            ViewBag.PuntosServicio1 = items;
            //--------------------------------------------------------------------------------------

            List<SelectListItem> items1 = new List<SelectListItem>();
            items1.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            items1.Add(new SelectListItem { Text = "Front Loader", Value = "1" });
            items1.Add(new SelectListItem { Text = "Hook", Value = "2" });
            items1.Add(new SelectListItem { Text = "Caja Recolectora", Value = "3" });
            ViewBag.SistemaIt = items1;
            //--------------------------------------------------------------------------------------
            List<SelectListItem> items2 = new List<SelectListItem>();
            CobroClientesRepository rs6 = new CobroClientesRepository(new sgsbdEntities());

            items2.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            List<PuntoEstadosView> dot1 = (List<PuntoEstadosView>)rs6.getPuntoEstados();
            foreach (var Xvar in dot1)
            {
                items2.Add(new SelectListItem { Text = Xvar.descripcion.Trim(), Value = Xvar.estado.ToString() });
            };
            ViewBag.PuntoEstadoEs = items2;
            //----------------------------------------------------------------------------------------------------
            List<SelectListItem> items3 = new List<SelectListItem>();
            items3.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });

            FranquiciadosRepository rs7 = new FranquiciadosRepository(new sgsbdEntities());

            List<FranquiciadosView> dot2 = (List<FranquiciadosView>)rs7.GetFranquiciadosView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            foreach (var Xvar in dot2)
            {
                items3.Add(new SelectListItem { Text = Xvar.razon_social.Trim(), Value = Xvar.id_franquiciado.ToString() });
            };
            ViewBag.FranqPersons = items3;
            //--------------------------------------------------------------------------------------

            return View(Modelo);

        }

        [HttpPost]
        public ActionResult Puntos(FormCollection collection, string button)
        {

            @ViewBag.Visible = null;
            DataCalculosFullView Modelo = new DataCalculosFullView();
            Modelo = null;

            string puntois = collection.Get("ptoHide");
            string idPunto = "";
            if (puntois == "")
            {
                idPunto = "0";
                puntois = "0";
            }
            else
            {
                idPunto = puntois;
            }

            if (button == "Ver reporte")
            {
                string iniD = collection.Get("Begin");
                string finD = collection.Get("End");
                string Sistema = collection.Get("SistemaIs");
                string PuntoEstado = collection.Get("PuntoEstado");
                string PuntoEstadoPosi = collection.Get("PuntoEstadoPosi");
                string Franquic = collection.Get("Who");
                string FranquicPosi = collection.Get("WhoPosi");

                if (string.IsNullOrEmpty(PuntoEstado) == true) { PuntoEstado = "0"; }
                if (string.IsNullOrEmpty(Sistema) == true) { Sistema = "0"; }
                if (string.IsNullOrEmpty(PuntoEstado) == true) { PuntoEstado = "0"; }
                if (string.IsNullOrEmpty(Franquic) == true) { Franquic = "0"; }

                if (PuntoEstado == "0")
                {
                    PuntoEstadoPosi = "0";
                }
                if (Franquic == "0")
                {
                    FranquicPosi = "00";
                }
                return RedirectToAction("VerReporte", "ReportesCostosFranc", new { idPunto = puntois, fecha1 = iniD, fecha2 = finD, SistemaVer = Sistema, Estado = PuntoEstado, Posi = PuntoEstadoPosi, WhoFranqui = Franquic, WhoFranquiPosi = FranquicPosi });
            }

            List<SelectListItem> items1 = new List<SelectListItem>();
            items1.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            items1.Add(new SelectListItem { Text = "Front Loader", Value = "1" });
            items1.Add(new SelectListItem { Text = "Hook", Value = "2" });
            items1.Add(new SelectListItem { Text = "Caja Recolectora", Value = "3" });
            ViewBag.SistemaIt = items1;

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });

            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            List<PuntosServiciosView> dot1 = (List<PuntosServiciosView>)rs.GetPuntosServiciosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            foreach (var Xvar in dot1)
            {
                string idPunto1 = Xvar.id_punto_servicio.ToString();
                string nombrePunto1 = Xvar.nombre_punto.Trim();
                items.Add(new SelectListItem { Text = nombrePunto1, Value = idPunto1 });
            };
            //ViewBag.PuntosServicio1 = items;
            ViewBag.PuntosServicio1 = new SelectList(items, "Value", "Text", 0);

            //--------------------------------------------------------------------------------------
            List<SelectListItem> items2 = new List<SelectListItem>();
            CobroClientesRepository rs6 = new CobroClientesRepository(new sgsbdEntities());

            items2.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            List<PuntoEstadosView> dot11 = (List<PuntoEstadosView>)rs6.getPuntoEstados();
            foreach (var Xvar in dot11)
            {
                items2.Add(new SelectListItem { Text = Xvar.descripcion.Trim(), Value = Xvar.estado.ToString() });
            };
            ViewBag.PuntoEstadoEs = items2;
            //--------------------------------------------------------------------------------------
            List<SelectListItem> items3 = new List<SelectListItem>();
            items3.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "00" });

            FranquiciadosRepository rs7 = new FranquiciadosRepository(new sgsbdEntities());

            List<FranquiciadosView> dot2 = (List<FranquiciadosView>)rs7.GetFranquiciadosView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            foreach (var Xvar in dot2)
            {
                items3.Add(new SelectListItem { Text = Xvar.razon_social.Trim(), Value = Xvar.id_franquiciado.ToString() });
            };
            ViewBag.FranqPersons = items3;
            //--------------------------------------------------------------------------------------

            if (String.IsNullOrEmpty(collection.Get("Begin").ToString().Trim()))
            {
                ViewBag.Error = "INICIO";
                ViewBag.When = "";
                return View(Modelo);
            }

            if (String.IsNullOrEmpty(collection.Get("End").ToString().Trim()))
            {
                ViewBag.Error = "FIN";
                ViewBag.When = "";
                return View(Modelo);
            }

            string fecha1 = String.Format("{0:yyyy-mm-dd HH:mm:ss}", collection.Get("Begin"));
            string fecha2 = String.Format("{0:yyyy-mm-dd HH:mm:ss}", collection.Get("End"));

            DateTime dateTime;
            try
            {
                DateTime.TryParse(fecha1, out dateTime);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "INICIONO";
                ViewBag.When = "";
                return View(Modelo);
            }

            try
            {
                DateTime.TryParse(fecha2, out dateTime);
            }
            catch (Exception ex)
            {
                ViewBag.Error = "FINNO";
                ViewBag.When = "";
                return View(Modelo);
            }

            string mes = fecha1.Substring(0, 2);
            string dia = fecha1.Substring(3, 2);
            string ano = fecha1.Substring(6, 4);

            System.DateTime inicio = new DateTime(Convert.ToInt16(ano), Convert.ToInt16(mes), Convert.ToInt16(dia));
            ViewBag.fecha_inicio = inicio;

            string mes2 = fecha2.Substring(0, 2);
            string dia2 = fecha2.Substring(3, 2);
            string ano2 = fecha2.Substring(6, 4);

            System.DateTime fin = new DateTime(Convert.ToInt16(ano2), Convert.ToInt16(mes2), Convert.ToInt16(dia2));
            ViewBag.fecha_fin = fin;

            string fechinist = String.Format("{0:yyyy-mm-dd}", ano + "-" + mes + "-" + dia);
            string fechfinst = String.Format("{0:yyyy-mm-dd}", ano2 + "-" + mes2 + "-" + dia2);

            ViewBag.Inicio = mes + "-" + dia + "-" + ano;
            ViewBag.fin = mes2 + "-" + dia2 + "-" + ano2;

            if (inicio > fin)
            {
                ViewBag.Error = "ERRORFECHA";
                ViewBag.When = "";
                return View(Modelo);
            }

            if (((fin - inicio).TotalDays) > 32)
            {
                ViewBag.Error = "MES30";
                ViewBag.When = "";
                return View(Modelo);
            }

            CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());

            // CALCULO DE COBRO VERSUS FRANQUICIAS
            rs4.CalculoPuntosFechas(fechinist, fechfinst);

            if (rs4.erorris != null)
            {
                string MesError = MesNombre(mes) + " " + ano;
                ViewBag.Error = "ERROR";
                ViewBag.When = " " + MesError;
                @ViewBag.Visible = "hidden";
            }
            else
            {
                ViewBag.Error = "ISOK";
                ViewBag.When = "";
                @ViewBag.Visible = "visible";
            }
            return View(Modelo);
        }

        public ActionResult VerReporte(string idPunto, string fecha1, string fecha2, string SistemaVer, string Estado, string Posi, string WhoFranqui, string WhoFranquiPosi)
        {

            @ViewBag.Visible = null;
            List<DataCalculosFullView> dot = new List<DataCalculosFullView>();

            DataCalculosFullView Modelo = new DataCalculosFullView();

            CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());

            Int16 ptois = 0;
            Int16 Sisid = 0;
            if (idPunto.Trim() == "")
            {
                ptois = 0;
                ViewBag.Punto = "0";
            }
            else
            {
                ptois = Convert.ToInt16(idPunto.Trim());
                ViewBag.Punto = ptois.ToString();
            }

            if (ptois == 0 && SistemaVer == "0" && Estado == "0" && WhoFranqui == "0")
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosAll();
            }
            if (ptois > 0)
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosFechas(ptois);
            }

            ViewBag.Sistema = "0";
            if (SistemaVer.Length > 0 && SistemaVer != "0")
            {
                ViewBag.Sistema = SistemaVer;
                if (SistemaVer == "Front Loader") { Sisid = 1; };
                if (SistemaVer == "Hook") { Sisid = 2; };
                if (SistemaVer == "Caja Recolectora") { Sisid = 3; };
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosSistema(SistemaVer);
            }

            ViewBag.EstadoPuntoSel = "0";
            if (Estado.Length > 0 && Estado != "0")
            {
                ViewBag.EstadoPuntoSel = Estado;
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosEstado(Estado);
            }
            if (Posi == "") { Posi = "0"; };

            ViewBag.FranquiciadoSel = "00";
            if (WhoFranqui.Length > 0 && WhoFranquiPosi != "00")
            {
                ViewBag.FranquiciadoSel = WhoFranqui;
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosFranquiciado(WhoFranqui);
            }
            if (WhoFranquiPosi == "") { WhoFranquiPosi = "00"; };
            ////--------------------------------------------------------------------------------------
            List<SelectListItem> items1 = new List<SelectListItem>();
            items1.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            items1.Add(new SelectListItem { Text = "Front Loader", Value = "1" });
            items1.Add(new SelectListItem { Text = "Hook", Value = "2" });
            items1.Add(new SelectListItem { Text = "Caja Recolectora", Value = "3" });
            ViewBag.SistemaIt = new SelectList(items1, "Value", "Text", Sisid);

            ////--------------------------------------------------------------------------------------
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });

            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());

            List<PuntosServiciosView> dot1 = (List<PuntosServiciosView>)rs.GetPuntosServiciosView2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            foreach (var Xvar in dot1)
            {
                string idPunto1 = Xvar.id_punto_servicio.ToString();
                string nombrePunto1 = Xvar.nombre_punto.Trim();
                items.Add(new SelectListItem { Text = nombrePunto1, Value = idPunto1 });
            };
            //ViewBag.PuntosServicio = items;
            ViewBag.PuntosServicio1 = new SelectList(items, "Value", "Text", idPunto);
            //--------------------------------------------------------------------------------------
            List<SelectListItem> items2 = new List<SelectListItem>();
            CobroClientesRepository rs6 = new CobroClientesRepository(new sgsbdEntities());

            items2.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "0" });
            List<PuntoEstadosView> dot11 = (List<PuntoEstadosView>)rs6.getPuntoEstados();
            foreach (var Xvar in dot11)
            {
                items2.Add(new SelectListItem { Text = Xvar.descripcion.Trim(), Value = Xvar.estado.ToString() });
            };
            ViewBag.PuntoEstadoEs = new SelectList(items2, "Value", "Text", Posi);
            //----------------------------------------------------------------------------------------------------
            List<SelectListItem> items3 = new List<SelectListItem>();
            items3.Add(new SelectListItem { Text = "Seleccione un Valor", Value = "00" });

            FranquiciadosRepository rs7 = new FranquiciadosRepository(new sgsbdEntities());

            List<FranquiciadosView> dot2 = (List<FranquiciadosView>)rs7.GetFranquiciadosView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            foreach (var Xvar in dot2)
            {
                items3.Add(new SelectListItem { Text = Xvar.razon_social.Trim(), Value = Xvar.id_franquiciado.ToString() });
            };
            ViewBag.FranqPersons = new SelectList(items3, "Value", "Text", WhoFranquiPosi);
            //--------------------------------------------------------------------------------------

            string mes = fecha1.Substring(0, 2);
            string dia = fecha1.Substring(3, 2);
            string ano = fecha1.Substring(6, 4);

            System.DateTime inicio = new DateTime(Convert.ToInt16(ano), Convert.ToInt16(mes), Convert.ToInt16(dia));
            ViewBag.fecha_inicio = inicio;

            string mes2 = fecha2.Substring(0, 2);
            string dia2 = fecha2.Substring(3, 2);
            string ano2 = fecha2.Substring(6, 4);

            System.DateTime fin = new DateTime(Convert.ToInt16(ano2), Convert.ToInt16(mes2), Convert.ToInt16(dia2));
            ViewBag.fecha_fin = fin;

            ViewBag.Inicio = mes + "-" + dia + "-" + ano;
            ViewBag.fin = mes2 + "-" + dia2 + "-" + ano2;

            if (dot.Count == 0)
            {
                Modelo = null;
                ViewBag.Error = "SINDATOS";
                ViewBag.When = "SINDATOS";
                @ViewBag.Visible = "hidden";
                return View("Puntos", Modelo);
            }
            else
            {
                ViewBag.Error = "";
                ViewBag.When = "";
                @ViewBag.Visible = "visible";
                return View("Puntos", dot);
            }

        }

        public ActionResult excelPuntos(string fecha1, string fecha2, string PuntoServ, string Sistema, string PuntoEstadoOut, string FranquiciadoOut)
        {
            List<DataCalculosFullView> dot = new List<DataCalculosFullView>();
            CobroClientesRepository rs4 = new CobroClientesRepository(new sgsbdEntities());
            Int16 Point;

            string mes = fecha1.Substring(0, 2);
            string dia = fecha1.Substring(3, 2);
            string ano = fecha1.Substring(6, 4);

            System.DateTime inicio = new DateTime(Convert.ToInt16(ano), Convert.ToInt16(mes), Convert.ToInt16(dia));
            ViewBag.fecha_inicio = inicio;

            string mes2 = fecha2.Substring(0, 2);
            string dia2 = fecha2.Substring(3, 2);
            string ano2 = fecha2.Substring(6, 4);

            System.DateTime fin = new DateTime(Convert.ToInt16(ano2), Convert.ToInt16(mes2), Convert.ToInt16(dia2));
            ViewBag.fecha_fin = fin;

            ViewBag.Inicio = mes + "-" + dia + "-" + ano;
            ViewBag.fin = mes2 + "-" + dia2 + "-" + ano2;
            
            if (string.IsNullOrEmpty(PuntoServ) == true) { PuntoServ = "0"; }
            if (string.IsNullOrEmpty(Sistema) == true) { Sistema = "0"; }
            if (string.IsNullOrEmpty(PuntoEstadoOut) == true) { PuntoEstadoOut = "0"; }
            if (string.IsNullOrEmpty(FranquiciadoOut) == true) { FranquiciadoOut = "0"; }

            @ViewBag.Visible = null;
            if (PuntoServ.Trim() == "")
            {
                PuntoServ = "0";
            }
            Point = Convert.ToInt16(PuntoServ);
            if (Point == 0 && Sistema == "0" && PuntoEstadoOut == "0" && FranquiciadoOut == "0")
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosAll();
            }
            if (Point > 0)
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosFechas(Point);
            }
            if (Sistema.Length > 0 && Sistema != "0")
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosSistema(Sistema);
            }
            if (PuntoEstadoOut.Length > 0 && PuntoEstadoOut != "0")
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosEstado(PuntoEstadoOut);
            }
            if (FranquiciadoOut.Length > 0 && FranquiciadoOut != "00")
            {
                dot = (List<DataCalculosFullView>)rs4.getCalculoPuntosFranquiciado(FranquiciadoOut);
            }

            if (dot.Count == 0)
            {
                ViewBag.Error = "ERROR";
                ViewBag.When = "anyway";
            }
            else
            {
                ViewBag.Error = "";
                ViewBag.When = "";
                @ViewBag.Visible = "visible";
            }
            return View(dot);

        }


        private string MesNombre(string MesIs)
        {

            string MesOut = "";

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut;
        }

        private string FechaArriendo(string When)
        {

            string MesIs = "";
            string MesOut = "";
            string YearIs = "";

            MesIs = When.Substring(3, 2);
            YearIs = When.Substring(6, 4);

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut + "-" + YearIs;
        }

        private string FechaDia(string When)
        {

            string MesIs = "";
            string MesOut = "";
            string Dia = "";

            MesIs = When.Substring(3, 2);
            Dia = When.Substring(0, 2);

            if (MesIs == "01")
            {
                MesOut = "Ene";
            }

            if (MesIs == "02")
            {
                MesOut = "Feb";
            }

            if (MesIs == "03")
            {
                MesOut = "Mar";
            }
            if (MesIs == "04")
            {
                MesOut = "Abr";
            }
            if (MesIs == "05")
            {
                MesOut = "May";
            }
            if (MesIs == "06")
            {
                MesOut = "Jun";
            }
            if (MesIs == "07")
            {
                MesOut = "Jul";
            }
            if (MesIs == "08")
            {
                MesOut = "Ago";
            }
            if (MesIs == "09")
            {
                MesOut = "Sep";
            }
            if (MesIs == "10")
            {
                MesOut = "Oct";
            }
            if (MesIs == "11")
            {
                MesOut = "Nov";
            }
            if (MesIs == "12")
            {
                MesOut = "Dic";
            }

            return MesOut + " " + Dia;
        }

    }
}
