﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.File;
using System.Data.Entity.Validation;
using System.Data.Entity.Infrastructure;
using Newtonsoft.Json;
using System.Collections;
using SGS.lib;
using SGS.lib.linq;
using System.Data.Objects.SqlClient;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using System.Configuration;
using System.Net;

namespace SGS.Controllers
{
    public class OrdenCompraController : Controller
    {
        //
        // GET: /OrdenCompra/
        String rutaGlobal;
        public ActionResult Index()
        {
            OrdenCompraRepository rs = new OrdenCompraRepository(new sgsbdEntities());
            List<orden_de_compra> data = (List<orden_de_compra>)rs.GetOrdenCompra();
            return View(data);
        }

        public ActionResult Create()
        {
            this.buildViewBagList();

            return View();
        }

        [HttpPost]
        public string BuscarSucursales(sgsbdEntities ctx)
        {
            if (Request.Form["puntos"].ToString() == "") return null;

            int cliente = int.Parse(Request.Form["puntos"].ToString());
            SucursalesRepository rs = new SucursalesRepository(new sgsbdEntities());
            List<SucursalesView> data = (List<SucursalesView>)rs.GetSucursalesClientes(cliente);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            return jsonString;
        }

        [HttpPost]
        public string BuscarPuntoServicio(sgsbdEntities ctx)
        {

            int sucursal = int.Parse(Request.Form["puntos"].ToString());
            PuntosServiciosRepository rs = new PuntosServiciosRepository(new sgsbdEntities());
            List<PuntosServiciosView> data = (List<PuntosServiciosView>)rs.GetPuntosSucursal(sucursal);

            var jsonString = Newtonsoft.Json.JsonConvert.SerializeObject(data);

            return jsonString;
        }


        [HttpPost]
        public ActionResult Create(FormCollection form, orden_de_compra model)
        {
            Session["id"] = model.id_orden_de_compra;
            var fecha_instalacion = form.Get("fecha_instalacion");
            var fecha_retiro= form.Get("fecha_retiro");

            ModelState.Remove("fecha_instalacion");
            ModelState.Remove("fecha_retiro");
            
            @ViewBag.fecha_instalacion = model.fecha_instalacion;

            @ViewBag.fecha_renovacion = model.fecha_retiro;

            if (ModelState.IsValid)
            {
                model.ruta = Session["ruta"].ToString();
                model.fecha_instalacion =Convert.ToDateTime (fecha_instalacion);
                model.fecha_retiro = Convert.ToDateTime(fecha_retiro);
                model.fecha_creacion = DateTime.Now;

                OrdenCompraRepository rs = new OrdenCompraRepository(new sgsbdEntities());
                if (model.id_orden_de_compra == 0)
                {
                    rs.InsertOrdenCompra(model);
                }
                else
                {
                    rs.UpdateOrdenCompra(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "OrdenCompra"); ;
            }

            this.buildViewBagList();
            return View(model);

        }



        public ActionResult edit(int id)
        {

            OrdenCompraRepository ctx = new OrdenCompraRepository(new sgsbdEntities());
            orden_de_compra c = ctx.GetOrdenCompraByID(id);
            this.buildViewBagList();
            
            @ViewBag.fecha_instalacion = c.fecha_instalacion;
            @ViewBag.fecha_retiro = c.fecha_retiro;
            @ViewBag.id_sucursal = c.id_sucursal;
            @ViewBag.id_puntoservicio = c.id_puntoservicio;

            return View("create", c);
        }

        private void buildViewBagList()
        {

            sgsbdEntities ctx = new sgsbdEntities();
            ClientesRepository rs1 = new ClientesRepository(ctx);
            ViewBag.clientes = new SelectList(rs1.GetClientesForListByUN(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "id_cliente", "razon_social");

        }

       public ActionResult subirArchivo()
       {
           return View();
       }

        //public ActionResult DescargarArchivo(int id)
        //{
        //    String archivo = ConfigurationManager.AppSettings.Get("ODC");
        //    String ruta = Server.MapPath(archivo);
        //    string nombre = id.ToString();
        //    System.Net.WebClient c = new System.Net.WebClient();
        //    {
        //        c.DownloadFile(ruta, nombre);
        //    }
        //    return View();

        //}

        [HttpPost]
       public ActionResult subirArchivo(HttpPostedFileBase file, orden_de_compra c)
       {
            // orden_de_compra model;
            OrdenCompraRepository ctx = new OrdenCompraRepository(new sgsbdEntities());
           // orden_de_compra c = ctx.GetOrdenCompraByID(id);
            SubirArchivoModelo modelo = new SubirArchivoModelo();

            if (file != null)
            {
                String archivo = ConfigurationManager.AppSettings.Get("ODC");
                String ruta = Server.MapPath(archivo);
                //Session["ruta"] = ruta;
                //rutaGlobal = ruta;
                if (!System.IO.Directory.Exists(ruta))
                {
                    System.IO.Directory.CreateDirectory(ruta); //Crea el repositorio si no existe
                }
                string punto = ".";
                char[] anyOf = punto.ToCharArray();
                //int ext = 0;
                //char[] extension = { ' ', ' ', ' ', ' ' };
                var posicionExtension = file.FileName.LastIndexOfAny(anyOf);
                //int j = 0;
                //for (int i = nombreArchivo; i < file.FileName.Length; i++)
                //{

                //    extension[j] = file.FileName[i];
                //    j++;
                //}
                var archivo_original = file.FileName;
                var extension = file.FileName.Substring(posicionExtension);
                var id_ruta = c.id_orden_de_compra.ToString().PadLeft(10, '0') + extension;
                ruta += "\\" + id_ruta;
                Session["ruta"] = archivo_original;
                modelo.SubirArchivo(ruta, file);
                //c.nombre_original = archivo_original;
                ViewBag.error = modelo.error;
                ViewBag.correcto = modelo.confirmacion;

                return this.RedirectToAction("Index", "OrdenCompra");
            }

            else {
                return this.RedirectToAction("Create", "OrdenCompra");
            }
          
       }
        public ActionResult DescargarArchivo(int id)
        {
            String archivo = ConfigurationManager.AppSettings.Get("ODC");
            String ruta = Server.MapPath(archivo);
            OrdenCompraRepository ctx = new OrdenCompraRepository(new sgsbdEntities());
            orden_de_compra c = ctx.GetOrdenCompraByID(id);
            this.buildViewBagList();
           
            var client = new WebClient();
            var descarga = c.ruta;
            var ruta_descarga = ruta +"\\"+ c.ruta ;
            ViewBag.descarga = descarga;
            //var dibujo = "https://previews.123rf.com/images/hermandesign2015/hermandesign20151608/hermandesign2015160800067/61077118-dibujo-animado-lindo-del-ping%C3%BCino-que-agita.jpg";
            //String prueba = "C:\\Users\\LuisVidela\\Desktop\\Proyects\\sgs_muti\\sgs-multi-20-11-2019\\OrdendeCompra\\prueba.txt";
            //client.DownloadFile(dibujo, "a.");//(c.ruta, c.id_cliente.ToString());
            //ViewBag
            return this.RedirectToAction("Index", "OrdenCompra");
        }

    }
}
