﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.Enums;
using SGS.Models.modelViews;
using SGS.Models.repository;
using SGS.Reportes;
using SGS.Reportes.Models;

namespace SGS.Controllers
{
    public class ReporteUnificadoController : Controller
    {
        //
        // GET: /ReporteUnificado/

        public ActionResult Index()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            int idUnidad = UsuariosRepository.getUnidadSeleccionada();
            ViewBag.clientes = new SelectList((from cl in ctx.clientes
                                               where cl.id_unidad == idUnidad
                                               orderby cl.razon_social
                                               select new
                                               {
                                                   Text = cl.razon_social,
                                                   Value = cl.id_cliente
                                               }).ToList(), "Value", "Text");

            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="periodo"></param>
        /// <param name="idCliente"></param>
        /// <param name="idFormatoExportacion"></param>
        /// <returns></returns>
        public ActionResult Generar(string periodo, string idCliente, int idFormatoExportacion)
        {
            string fechaPeriodo = CastearFecha(periodo);

            List<Parameter> listaParametros = new List<Parameter>
            {
                Reporte.CrearNuevoParametro("Periodo", fechaPeriodo),
                Reporte.CrearNuevoParametro("IdCliente", String.IsNullOrEmpty(idCliente) ? null : idCliente)
            };

            string nombreReporte = "ReporteUnificado";
            FormatoExportacionView formatoExportacion = Reporte.GetFormatoExportacion((FormatoExportacion) idFormatoExportacion);

            Response.AddHeader("Content-Disposition", "attachment; filename=ReporteUnificado" + formatoExportacion.Extencion);

            return Reporte.GenerarReporte(formatoExportacion, nombreReporte, listaParametros);
        }


        /// <summary>
        /// Metodo encargado de validar el campo fecha. Si viene vacio o nulo, asigna la fecha de hoy -100 años, caso contrario castea la fecha a datatime 
        /// </summary>
        /// <param name="fecha">Fecha ingresada</param>
        /// <returns>fecha convertida</returns>
        private static string CastearFecha(string fecha)
        {
            var date = string.IsNullOrEmpty(fecha) ? (DateTime.Now.AddYears(-100)) : Convert.ToDateTime(fecha);
            string formatedDate = date.ToString("MM") + date.Year;
            return formatedDate;
        }
    }
}
