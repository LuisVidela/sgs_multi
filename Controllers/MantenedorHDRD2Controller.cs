﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;
using Rotativa;
using System.IO;


namespace SGS.Controllers
{
    public class MantenedorHDRD2Controller : Controller
    {
        //
        // GET: /MantenedorHDR/

        
        public ActionResult view(string id,int imprimir=0)
        {
           // if (id == "") id = "0_Martes";
            //id = "5";
            int n = int.Parse(id);
            //int n = int.Parse(id.Split('_')[0]);
            //string Dia = id.Split('_')[1];



            ViewData["editable"] = true;

            if (Request.QueryString["estado"] != null)
            {
                if (Int32.Parse(Request.QueryString["estado"]).Equals(0))
                {

                    ViewData["editable"] = false;
                }
            }
           
            


            sgsbdEntities ctx = new sgsbdEntities();
            var model = new ViewModelHDRD();
            model.Elements = (from hr in ctx.hoja_ruta
                              join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
                              join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
                              join co in ctx.contratos on ps.id_contrato equals co.id_contrato
                              join tc in ctx.tipo_contrato on co.id_tipo_contrato equals tc.id_tipo_contrato
                              join eq in ctx.equipos on ps.id_equipo equals eq.id_equipo
                              join v in ctx.vertederos on ps.id_vertedero equals v.id_vertedero
                              
                              where hr.id_hoja_ruta == n
                              //&& ((ps.lunes==true && Dia=="Lunes") || (ps.martes==true && Dia=="Martes") || (ps.miercoles==true && Dia=="Miercoles") || (ps.jueves==true && Dia=="Jueves") || (ps.viernes==true && Dia=="Viernes") || (ps.sabado==true && Dia=="Sabado") || (ps.domingo==true && Dia=="Domingo"))
                              orderby hr.id_hoja_ruta, rd.Orden
                              select new MantenedorHDRDView { id_hoja_ruta = hr.id_hoja_ruta, id_punto_servicio = rd.id_punto_servicio, orden = rd.Orden, cantidad = rd.cantidad, peso = rd.peso, nombre_punto = ps.nombre_punto, estado = rd.estado, tipo_contrato1 = hr.id_tipo_servicio, id_tipo_contrato = hr.id_tipo_servicio, tipo_equipo = eq.tipo_equipo, equipoentrada = rd.equipoentrada, equiposalida = rd.equiposalida, horainicio = rd.horainicio, horatermino = rd.horatermino, relleno = rd.relleno, observacion = rd.observacion, vertedero = v.vertedero, cant_equipos = ps.cant_equipos, vertededordefecto = v.vertedero }).ToList();
            var vertir = new ViewModelVertedero();
            vertir.Elements=(from v in ctx.vertederos
                             select new VertederosView { id_vertedero = v.id_vertedero, vertedero = v.vertedero }).ToList();

            var EncabezadoRuta = new ViewModelHeaderHDRD();
            EncabezadoRuta.Elements = (from hr in ctx.hoja_ruta
                                       join co in ctx.conductores on hr.id_conductor equals co.id_conductor
                                       join ca in ctx.camiones on hr.id_camion equals ca.id_camion
                                       join tc in ctx.tipo_servicio on hr.id_tipo_servicio equals tc.id_tipo_servicio
                                       
                                       where hr.id_hoja_ruta == n
                                       select new HeaderHDRDView { id_hoja_ruta = hr.id_hoja_ruta, franquiciado = co.nombre_conductor, rut_conductor = co.rut_conductor, patente = ca.patente, num_interno = ca.num_interno, tipo_servicio = tc.tipo_servicio1, fecha = hr.fecha.Value}).ToList();

            var hoy = DateTime.Today.ToLongDateString().ToString();
            //var dia = (from hr in ctx.hoja_ruta
            //           where hr.id_hoja_ruta == n
            //           select hr.fecha.ToString()).Single();

            //dia = dia.ToString();



            var ValesRuta = new ViewModelValesHDRD();

            ValesRuta.Elements = (from hr in ctx.hoja_ruta
                                  join co in ctx.conductores on hr.id_conductor equals co.id_conductor
                                  join ca in ctx.camiones on hr.id_camion equals ca.id_camion
                                  join rd in ctx.hoja_ruta_detalle on hr.id_hoja_ruta equals rd.id_hoja_ruta
                                  join ps in ctx.puntos_servicio on rd.id_punto_servicio equals ps.id_punto_servicio
                                  join con in ctx.contratos on ps.id_contrato equals con.id_contrato
                                  join tc in ctx.tipo_contrato on con.id_tipo_contrato equals tc.id_tipo_contrato
                                  join eq in ctx.equipos on ps.id_equipo equals eq.id_equipo
                                  join v in ctx.vertederos on ps.id_vertedero equals v.id_vertedero
                                  where hr.id_hoja_ruta == n
                                  select new ValesHDRDView { id_hoja_ruta = hr.id_hoja_ruta, nombre_conductor = co.nombre_conductor, rut_conductor = co.rut_conductor, patente = ca.patente, num_interno = ca.num_interno, Vertedero = v.vertedero,punto_servicio=ps.nombre_punto,Tipo_Contenedor=eq.nom_nemo,Fecha=hr.fecha.Value, }).ToList();



            sgsbdEntities ctx2 = new sgsbdEntities();
            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(ctx2);

            ViewBag.unidad_negocio = rs.GetunidadesNegocio();

  


            ViewData["puntosHDRD"] = model.Elements;
            ViewData["Vertederos"] = vertir.Elements;
            ViewData["ValesRuta"] = ValesRuta.Elements;
            ViewData["EncabezadoRuta"] = EncabezadoRuta.Elements;
            ViewData["imprimir"] = imprimir;

            //CamionesRepository rs3 = new CamionesRepository(ctx);
            // ViewData["puntos"] = rs3.GetCamiones();
            
            return View(model.Elements);
        }

        [HttpPost]
        public ActionResult AnularHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            int IdHojaRuta = int.Parse(Request.Form["IdHojaRuta"].ToString());
            List<hoja_ruta> hoja2 = ctx.hoja_ruta.Where(i => i.id_hoja_ruta == IdHojaRuta).ToList();
            hoja2[0].estado = 4;
            ctx.hoja_ruta.Attach(hoja2[0]);
            ctx.Entry(hoja2[0]).State = System.Data.EntityState.Modified;
            ctx.SaveChanges();
            return View();
        }


        [HttpPost]
        public ActionResult ModificarHoja()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            string[] puntos = Request.Form["puntos"].ToString().Split('|');
            int Ruta, Punto, Orden, Cantidad, Peso, Estado,tiposervicio;
            string equipoentrada, equiposalida, horainicio, horatermino, observacion,vertedero;

            bool todos = true;
            bool cambio = false;
            int iidruta = 0;
            for (int j = 0; j < puntos.Length; j++)
            {
                String[] unpunto = puntos[j].Split('_');
                Ruta = int.Parse(unpunto[0]);
                iidruta = Ruta;
                Punto = int.Parse(unpunto[1]);
                Orden = int.Parse(unpunto[2]);
                Cantidad = (unpunto[3] != "") ? int.Parse(unpunto[3]) : -1;
                Peso = (unpunto[4] != "") ? int.Parse(unpunto[4]) : -1;
                tiposervicio= int.Parse(unpunto[5]);
                Estado = int.Parse((unpunto[6] == "") ? "1" : unpunto[6]);
                equipoentrada = unpunto[7];
                equiposalida=unpunto[8];
                horainicio=unpunto[8];
                horatermino = unpunto[9];
                observacion = unpunto[9];
                vertedero = unpunto[10];
                List<hoja_ruta_detalle> hoja = ctx.hoja_ruta_detalle.Where(i => i.id_hoja_ruta == Ruta && i.Orden == Orden).ToList();
                if (Cantidad != -1)
                    hoja[0].cantidad = Cantidad;
                else
                    hoja[0].cantidad = null;
                if (Peso != -1)
                    hoja[0].peso = Peso;
                else
                    hoja[0].peso = null;
                if (Estado == 1 && (Cantidad != -1 || Peso != -1))
                    hoja[0].estado = 2;
                else
                    hoja[0].estado = Estado;
                if (hoja[0].estado != 1) 
                    cambio = true;
                if (hoja[0].estado == 1 || hoja[0].estado == 8) 
                    todos = false;
                hoja[0].id_tipo_servicio = tiposervicio;
                hoja[0].equipoentrada = equipoentrada;
                hoja[0].equiposalida = equiposalida;
                //hoja[0].horainicio = horainicio;
                //hoja[0].horatermino = horatermino;
                hoja[0].observacion = observacion;
                hoja[0].relleno = vertedero;

                ctx.hoja_ruta_detalle.Attach(hoja[0]);
                ctx.Entry(hoja[0]).State = System.Data.EntityState.Modified;
                ctx.SaveChanges();
            }

            List<hoja_ruta> hoja2 = ctx.hoja_ruta.Where(i => i.id_hoja_ruta == iidruta).ToList();
            if (todos)
                hoja2[0].estado = 3;
            else if (cambio)
                hoja2[0].estado = 2;
            else
                hoja2[0].estado = 1;
            ctx.hoja_ruta.Attach(hoja2[0]);
            ctx.Entry(hoja2[0]).State = System.Data.EntityState.Modified;
            ctx.SaveChanges();

            return View();
        }

        public ActionResult Imprimir(string id)
        {
            var patente = "";

            if (Request.QueryString["patente"] != null) {

                patente = "_" + Request.QueryString["patente"];
            
            }


            
            var vista = new ActionAsPdf("View", new { id = id, imprimir = 1 }) { 
                         FileName = "HDR_"+id+patente+".pdf", 
                         PageOrientation = Rotativa.Options.Orientation.Landscape, 
                         PageSize = Rotativa.Options.Size.A4};
          
            return vista;
        }


    }
}
