﻿using System;
using System.Collections.Generic;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.Forms;
using SGS.Models.modelViews;
using SGS.Models.repository;

namespace SGS.Controllers
{
    public class ServiciosReporteController : Controller
    {
        //
        // GET: /ServiciosReporte/
        /// <summary>
        /// Action encargado de recuperar y mostrar la información de mantenedor de reporte unificado.
        /// </summary>
        /// <returns>invocación a View y model para llenarlo</returns>
        public ActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// Action encargado de recuperar y mostrar la interfaz de creación de nuevo Intem de cobro
        /// </summary>
        /// <returns>invocación a View y model para llenarlo</returns>
        [HttpPost]
        public ActionResult Create(int id)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            List<UmbView> lista = (from umbList in ctx.umb
                                   select new UmbView
                                   {
                                       IdUmb = umbList.id_umb,
                                       NombreUmb = umbList.nombre
                                   }).ToList();
            ViewBag.listaUmb = lista;

            ViewBag.itemReporte = (from item in ctx.item_reporte_unificado
                                   where item.id_item == id
                                   select new ItemReporteForm
                               {
                                   IdUmb = item.id_UMB,
                                   NombreItem = item.nombre_item,
                                   OrdenItem = item.orden_item,
                                   IdItemReporte = item.id_item,
                                   MuestraManejoDris = item.agrupar_manejo_dris
                               }).FirstOrDefault();

            return View("ModelReportItem");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>invocación a View y model para llenarlo</returns>
        public ActionResult Filter()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            int idUnidad = UsuariosRepository.getUnidadSeleccionada();
            ViewBag.clientes = new SelectList((from cl in ctx.clientes
                                               where cl.id_unidad == idUnidad
                                               orderby cl.razon_social
                                               select new
                                               {
                                                   Text = cl.razon_social,
                                                   Value = cl.id_cliente
                                               }).ToList(), "Value", "Text");
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns>invocación a View y model para llenarlo</returns>
        public ActionResult ReportItem(int idCliente)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            List<ItemReporteUnificadoView> itemReporteList = (from item in ctx.item_reporte_unificado
                                                              join umbTable in ctx.umb on item.id_UMB equals umbTable.id_umb
                                                              where item.id_cliente == idCliente
                                                              select new ItemReporteUnificadoView
                                                                         {
                                                                             IdItemPeriodo = item.id_item,
                                                                             OrdenItem = item.orden_item,
                                                                             NombreItem = item.nombre_item,
                                                                             NombreUmb = umbTable.nombre,
                                                                             MuestraManejoDris = item.agrupar_manejo_dris
                                                                         }).ToList();

            return View(itemReporteList);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        public ActionResult PointServices(int idCliente)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            List<PuntoServicioReporteUnificadoView> pointOfServiceList = (from pos in ctx.puntos_servicio
                                                                          join con in ctx.contratos on pos.id_contrato equals con.id_contrato
                                                                          where con.id_cliente == idCliente
                                                                          select new PuntoServicioReporteUnificadoView
                                                                          {
                                                                              IdPuntoServicio = pos.id_punto_servicio,
                                                                              NombrePuntoServicio = pos.nombre_punto,
                                                                              IdItemPeriodo = pos.id_item
                                                                          }).ToList();

            ViewBag.itemReporteList = (from item in ctx.item_reporte_unificado
                                       join umbTable in ctx.umb on item.id_UMB equals umbTable.id_umb
                                       where (item.id_cliente == idCliente)
                                       select new ItemReporteUnificadoView
                                       {
                                           IdItemPeriodo = item.id_item,
                                           OrdenItem = item.orden_item,
                                           NombreItem = SqlFunctions.StringConvert((double)item.orden_item) + "- " + item.nombre_item,
                                           NombreUmb = umbTable.nombre
                                       }).ToList();

            return View(pointOfServiceList);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idItemPeriodo"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteCurrentReportItem(int idItemPeriodo)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            // Actualiza tabla puntos de servicio y anula relación
            List<puntos_servicio> pos = ctx.puntos_servicio.Where(p => p.id_item == idItemPeriodo).ToList();
            foreach (var puntosServicio in pos)
            {
                puntosServicio.id_item = null;
            }

            // Ejecuta delete de registro
            item_reporte_unificado item = ctx.item_reporte_unificado.Find(idItemPeriodo);
            if (item != null) ctx.item_reporte_unificado.Remove(item);

            ctx.SaveChanges();

            return Json("1");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idCliente"></param>
        /// <param name="idReportItem"></param>
        /// <param name="orden"></param>
        /// <param name="nombre"></param>
        /// <param name="idUmb"></param>
        /// <param name="varShowsManejoDris"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveCurrentReportItem(int idCliente, int idReportItem, int orden, string nombre, int idUmb, bool varShowsManejoDris)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            item_reporte_unificado item;

            if (idReportItem == 0)
            {
                item = ctx.item_reporte_unificado.Create();
                item.id_cliente = idCliente;
                item.id_UMB = idUmb;
                item.nombre_item = nombre;
                item.orden_item = orden;
                item.agrupar_manejo_dris = varShowsManejoDris;
                ctx.item_reporte_unificado.Add(item);
            }
            else
            {
                item = ctx.item_reporte_unificado.Find(idReportItem);
                if (item != null)
                {
                    item.id_cliente = idCliente;
                    item.id_UMB = idUmb;
                    item.nombre_item = nombre;
                    item.orden_item = orden;
                    item.agrupar_manejo_dris = varShowsManejoDris;
                }
            }

            ctx.SaveChanges();
            return Json("1");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <param name="idCliente"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddCurrentReportItem(int idpuntoservicio, int idCliente)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            puntos_servicio pos = ctx.puntos_servicio.First(p => p.id_punto_servicio == idpuntoservicio);
            int ordenActual = (from currentItem in ctx.item_reporte_unificado
                               where currentItem.id_cliente == idCliente
                               select currentItem.orden_item).DefaultIfEmpty(0).Max();

            item_reporte_unificado item = ctx.item_reporte_unificado.Create();
            item.id_cliente = idCliente;
            item.id_UMB = 1;
            item.nombre_item = pos.nombre_punto;
            item.orden_item = ordenActual + 1;
            ctx.item_reporte_unificado.Add(item);
            ctx.SaveChanges();

            pos.id_item = item.id_item;
            ctx.SaveChanges();
            return Json("1");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoServicio"></param>
        /// <param name="idItem"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ChangeCurrentReportItem(int idpuntoServicio, string idItem)
        {
            sgsbdEntities ctx = new sgsbdEntities();

            puntos_servicio pos = ctx.puntos_servicio.First(p => p.id_punto_servicio == idpuntoServicio);

            if (string.IsNullOrEmpty(idItem))
            {
                pos.id_item = null;
            }
            else
            {
                pos.id_item = Convert.ToInt32(idItem);
            }
            ctx.SaveChanges();
            return Json("1");
        }
    }
}
