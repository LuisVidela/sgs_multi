﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models;
using SGS.Models.connector;
using System.Data.Objects.SqlClient;

namespace SGS.Controllers
{
    public class ReportesCostosFranc2Controller : Controller
    {
        //
        // GET: /ReportesCostosFranc2/

        public ActionResult Index()
        {

            this.buildingViewBag();
            return View();
        }


        public ActionResult List() {

            sgsbdEntities ctx=new sgsbdEntities();

            IQueryable<V_DATA_CALCULOS_FULL> query = ctx.V_DATA_CALCULOS_FULL;
            
            string fecha_inicial = Request.Params.Get("fecha_inicial");
            string fecha_final = Request.Params.Get("fecha_final");
            string punto_servicio = Request.Params.Get("punto_servicio");
            string sistema = Request.Params.Get("sistema");
            string estado = Request.Params.Get("estado");
            string franquiciado = Request.Params.Get("franquiciado");
            string excel = Request.Params.Get("excel");

            string SEcho = Request.Params.Get("sEcho");
            string IDisplayStart = Request.Params.Get("IdisplayStart");
            string IDisplayLength = Request.Params.Get("IDisplayLength");
            int sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
            string sortDirection = Request["sSortDir_0"];

            int IDisplayStart_int = 0;
            int IDisplayLength_int = 0;
            int punto_servicio_int = int.Parse(punto_servicio);

            if (IDisplayLength != "" && IDisplayLength!=null)
            {
                IDisplayStart_int = int.Parse(IDisplayStart);
                IDisplayLength_int = int.Parse(IDisplayLength);
                
            }


            if (fecha_inicial.Trim() != "" || fecha_final!="")
            {
                if (fecha_inicial != "") {
                    string[] fecha1 = fecha_inicial.Split('/');
                    DateTime t1 = new DateTime(int.Parse(fecha1[2]), int.Parse(fecha1[1]), int.Parse(fecha1[0]));
                    query = query.Where(x=>x.fechaFull >=t1);
                }
                
                if (fecha_final != "")
                {
                    string[] fecha2 = fecha_final.Split('/');
                    DateTime t2 = new DateTime(int.Parse(fecha2[2]), int.Parse(fecha2[1]), int.Parse(fecha2[0]));
                    query = query.Where(x=>x.fechaFull<=t2);

                }
            }


            if (estado.Trim() != "0")
            {
                query = query.Where(x => x.estadopunto.Equals(estado));
            }

            if (franquiciado.Trim() != "NaN")
            {
                query = query.Where(x=>x.franquiciado.Contains(franquiciado));
            }

            if (sistema.Trim() != "0")
            {
                query = query.Where(x=>x.sistema.Contains(sistema));
            }

            if (punto_servicio.Trim() != "0")
            {

                query = query.Where(x => x.id_punto_servicio.Equals(punto_servicio_int));

            }


            if (excel.Equals("true"))
            {
                var final_data_excel = query.ToList().OrderBy(m => m.id_hoja_ruta);
                return View("Excel", final_data_excel);
            }


            IQueryable<subData> pre_stage = query.Select(x => new subData { 
             chofer=x.conductor.Trim(),
              cobro=SqlFunctions.StringConvert((double)x.bruto).Trim(),
               estado_punto=x.estadopunto.Trim(),
                sistema=x.sistema.Trim(),
                 volteos=SqlFunctions.StringConvert((double)x.cantidad).Trim(),
                  pago=SqlFunctions.StringConvert((double)x.Pago).Trim(),
                   margen=SqlFunctions.StringConvert((double)x.Margen).Trim(),
                    margen_ciento=SqlFunctions.StringConvert((double)x.MargenPorc).Trim(),
                     franquiciado=x.franquiciado.Trim(),
                      patente=x.camion.Trim(),
                       hoja_ruta=SqlFunctions.StringConvert((double)x.id_hoja_ruta).Trim(),
                        punto=x.nombre_punto.Trim(),
                         fecha=x.fechacorta.Trim()
            });


            switch (sortColumnIndex)
            {
                case 0:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.fecha); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x =>x.fecha); }
                    break;
                case 1:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.hoja_ruta); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.hoja_ruta); }
                    break;
                case 2:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.punto); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.punto); }
                    break;
                case 3:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.sistema); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.sistema); }
                    break;
                case 4:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.patente); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.patente); }
                    break;
                case 5:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.franquiciado); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.franquiciado); }
                    break;
                case 6:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.chofer); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.chofer); }
                    break;
                case 7:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.estado_punto); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.estado_punto); }
                    break;
                case 8:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.volteos); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.volteos); }
                    break;
                case 9:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.cobro); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.cobro); }
                    break;
                case 10:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.pago); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.pago); }
                    break;
                case 11:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.margen); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.margen); }
                    break;
                case 12:
                    if (sortDirection == "asc")
                    { pre_stage = pre_stage.OrderBy(x => x.margen_ciento); }
                    else
                    { pre_stage = pre_stage.OrderByDescending(x => x.margen_ciento); }
                    break;

            }

            List<subData> final_data = pre_stage.ToList();
            using (var db = new sgsbdEntities())
            {
                int idUnidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                var listExpt = new List<subData>();
                foreach (var item in final_data)
                {
                    var punto = db.puntos_servicio.Where(u => u.nombre_punto == item.punto && u.contratos.clientes.id_unidad == idUnidad).FirstOrDefault();
                    if (punto == null)
                    {
                        listExpt.Add(item);
                    }
                }
                foreach (var item in listExpt)
                {
                    final_data.Remove(item);
                }
            }
            List<subData> data_pag = final_data.Skip(IDisplayStart_int).Take(IDisplayLength_int).ToList();
            int count = final_data.Count;



            json_dataTable2 data = new json_dataTable2();
            data.aaData = data_pag;
            data.iTotalDisplayRecords = count;
            data.iTotalRecords = count;
            data.sEcho = SEcho;


            return Json(data,JsonRequestBehavior.AllowGet);
        }
        


        public void buildingViewBag(){

            sgsbdEntities ctx = new sgsbdEntities();

            IEnumerable<object> tipo_servicio = new object[]{
            new { Text="Seleccione un Valor",Value="0"},
            new { Text="Front Loader",Value="Front Loader"},
            new { Text="Hook",Value="Hook"},
            new { Text="Caja Recolectora",Value="Caja Recolectora"}
        };

            PuntosServiciosRepository rs_puntos = new PuntosServiciosRepository(ctx);
            PuntoEstadoRepository rs_estado = new PuntoEstadoRepository(ctx);
            FranquiciadosRepository rs_franquiciados = new FranquiciadosRepository(ctx);

            IEnumerable<object> puntos = rs_puntos.getPuntosServicioForSelect();
            IEnumerable<object> estados = rs_estado.getPuntoEstadoForSelect();
            IEnumerable<object> franquiciados = rs_franquiciados.GetFranquiciadoForSelect();


            ViewBag.tipo_servicio = new SelectList(tipo_servicio, "Value", "Text","0");
            ViewBag.puntos = new SelectList(puntos, "Value", "Text", "0");
            ViewBag.estados = new SelectList(estados, "Value", "Text", "0");
            ViewBag.franquiciados = new SelectList(franquiciados, "Value", "Text", "NaN");
        
        }
    }
}
