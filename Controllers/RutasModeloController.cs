﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.ajax;
using SGS.lib;
using Newtonsoft.Json;

namespace SGS.Controllers
{
    public class RutasModeloController : Controller
    {
        //
        // GET: /RutasModelo/

        public ActionResult Index()
        {

            RutasModeloRepository rs = new RutasModeloRepository(new sgsbdEntities());
            List<ruta_modelo> data = (List<ruta_modelo>)rs.GetRutasModelo(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }
        public ActionResult excel()
        {

            RutasModeloRepository rs = new RutasModeloRepository(new sgsbdEntities());
            List<ruta_modelo> data = (List<ruta_modelo>)rs.GetRutasModelo(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }
        public ActionResult create()
        {
            buildViewBagList();
            return View();
        }


        [HttpPost]
        public ActionResult create(ruta_modelo model)
        {
           

            return View();
        }

        public ActionResult edit(int id)
        {

            RutasModeloRepository rs = new RutasModeloRepository(new sgsbdEntities());
            ruta_modelo c = rs.GetRutaModeloByID(id);
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            RutasModeloRepository rs = new RutasModeloRepository(new sgsbdEntities());
            rs.DeleteRutaModelo(id);
            rs.Save();
            return this.RedirectToAction("Index", "RutasModelo");

        }


        public ActionResult tableData(string dia,int tipo_servicio) {

            RutasModeloRepository rs = new RutasModeloRepository(new sgsbdEntities());
            List<tableDataView>data=(List<tableDataView>)rs.GetRutaModelForTableData(dia,tipo_servicio,SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data.OrderByDescending(m=>m.id_ruta));
        
        }



        public ActionResult editDetailsPuntosServicios(int ruta,int col) {


            ViewBag.delete = false;
            sgsbdEntities ctx = new sgsbdEntities();
            RutaPuntoRepository rs=new RutaPuntoRepository(ctx);
            List<tableDataDetailsAjax> data = (List<tableDataDetailsAjax>)rs.getRutasPuntosDetails(ruta, col);
           

            return View(data);
        }


        public ActionResult allPuntosServicios(int ruta) {


            ViewBag.delete = true;
            ViewBag.id_ruta = ruta;
            sgsbdEntities ctx = new sgsbdEntities();
            RutaPuntoRepository rs=new RutaPuntoRepository(ctx);
            List<tableDataDetailsAjax> data = (List<tableDataDetailsAjax>)rs.getRutasPuntosDetailsAll(ruta);

            return View("editDetailsPuntosServicios", data);
          
        }

        public ActionResult puntosServiciosData(int dia)
        {

            string d = Utils.getStringOfDay(dia);
            sgsbdEntities ctx = new sgsbdEntities();
            PuntosServiciosRepository rs = new PuntosServiciosRepository(ctx);
            List<puntos_servicio> data=(List<puntos_servicio>)rs.getPuntosServiciosForAjax(d,SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data.OrderBy(m=>m.nombre_punto));

        }


        public ActionResult removeRutaModelo(int ruta) {

            sgsbdEntities ctx = new sgsbdEntities();
            RutasModeloRepository rs = new RutasModeloRepository(ctx);
            rs.DeleteRutaModelo(ruta);
            rs.Save();
            return Content("SUCCESS");
        }


        public ActionResult removePuntoServicioAjax(int ids)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            RutaPuntoRepository rs = new RutaPuntoRepository(ctx);
            rs.DeleteRutaPunto(ids);
            rs.Save();




            return Content("SUCCESS");

        }


        public ActionResult UpdatepuntosServiciosSaveDataAjax(int id_ruta,int id_punto,int col,int index){

            sgsbdEntities ctx = new sgsbdEntities();
            RutaPuntoRepository rs = new RutaPuntoRepository(ctx);

            ruta_punto rp = new ruta_punto();
            rp.id_ruta = id_ruta;
            rp.id_punto_servicio = id_punto;
            rp.Orden=Int32.Parse(String.Format("{0}{1}{2}",col,"00",index));
            rs.InsertRutaPunto(rp);
            rs.Save();
            return Content("fin");
        }

        public ActionResult puntosServiciosSaveDataAjax(string dia,
                                                        string nombre_ruta,
                                                        int servicio,
                                                        string ruta,
                                                        string data)
        {


            List<string> list =data.Split(',').ToList();

            sgsbdEntities ctx = new sgsbdEntities();
            RutasModeloRepository rs = new RutasModeloRepository(ctx);

            ruta_modelo _data = null;

           
                _data = new ruta_modelo();
            

            _data.nombre_ruta = nombre_ruta;
            _data.id_tipo_servicio = servicio;
            _data.dia_ruta = dia;
            _data.fecha_ruta = DateTime.Now;
            _data.id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
             rs.InsertRutaModelo(_data);
             rs.Save();

            if (list.Count() > 0 && list[0]!="") {

                ruta_punto rp = new ruta_punto();
                RutaPuntoRepository rs2 = new RutaPuntoRepository(ctx);
               

                foreach (string v in list) {

                    string _t2=v.Substring(1, v.Length - 2);
                    List<string> _temp = _t2.Split('-').ToList();
                    List<string> _puntos = _temp[1].Split('|').ToList();

                    int _c2 = 0;
                    foreach (string v2 in _puntos)
                    {
                        String _correlativo=String.Format("{0}{1}{2}",_temp[0],"00",(_c2+1));
                        rp.Orden = Int32.Parse(_correlativo);
                        rp.id_punto_servicio = Int32.Parse(v2);
                        rp.id_ruta = _data.id_ruta;
                        rs2.InsertRutaPunto(rp);
                        rs2.Save();
                        _c2++;
                    }
                }
            }
                return Content("SUCCESS");

        }




        public void buildViewBagList()
        {

            sgsbdEntities ctx=new sgsbdEntities();
            TipoServicioRepository rs = new TipoServicioRepository(ctx);

            IEnumerable<object> dias = new object[] {
             new { text="Lunes",value="1"},
             new { text="Martes",value="2"},
             new { text="Miercoles",value="3"},
             new { text="Jueves",value="4"},
             new { text="Viernes",value="5"},
             new { text="Sabado",value="6"},
             new { text="Domingo",value="7"},
             
            };


            ViewData["servicios"] = new SelectList(rs.GetTiposServicios(), "id_tipo_servicio", "tipo_servicio1");
            ViewData["dias"] = new SelectList(dias, "value", "text");
            
        }

    }
}
