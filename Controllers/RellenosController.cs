﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;

namespace SGS.Controllers
{
    public class RellenosController : Controller
    {
        //
        // GET: /Rellenos/
        /// <summary>
        /// Action encargado de recuperar y mostrar la información de vertederos para una unidad específica.
        /// </summary>
        /// <returns>invcación a View y model para llenarlo</returns>
        public ActionResult Index()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            int idUnidad = UsuariosRepository.getUnidadSeleccionada();

            var query = (from v in ctx.vertederos
                         join c in ctx.ciudades on v.id_ciudad equals c.id_ciudad into ciudadx
                         from cx in ciudadx.DefaultIfEmpty()
                         join co in ctx.comunas on v.id_comuna equals co.id_comuna into comunasx
                         from cox in comunasx.DefaultIfEmpty()
                         join r in ctx.regiones on v.id_region equals r.id_region into regionesx
                         from re in regionesx.DefaultIfEmpty()
                         join p in ctx.paises on v.id_pais equals p.id_pais into paisesx
                         from pa in paisesx
                         where v.id_unidad == idUnidad
                         select new vertederosView2()
                         {
                             id_vertedero = v.id_vertedero,
                             vertedero = v.vertedero,
                             pais = pa.pais,
                             region = re.nombre_region,
                             ciudad = cx.ciudad,
                             comuna = cox.nombre_comuna,
                             direccion = v.direccion_vertedero,
                             disposicion_final = v.disposicion_final,
                             pertenece_resiter = v.pertenece_resiter
                         }
                           ).ToList();

            return View(query);
        }


        /// <summary>
        /// Construye la vista de nuevo relleno u edicion de uno existente
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            BuildViewBagList();

            return View();
        }


        /// <summary>
        /// Crea nuevo relleno, o actualiza uno existente basado en los parámetros ingresados.
        /// </summary>
        /// <param name="model">Model con la información de relleno</param>
        /// <param name="numeroDireccion">Numero de la dirección</param>
        /// <param name="direccion">Dirección</param>
        /// <param name="tipo">Tipo</param>
        /// <returns>Vista Index en caso de update o index existoso y recarga de Create en caso que no</returns>
        [HttpPost]
        public ActionResult Create(vertederos model, string numeroDireccion, string direccion, string tipo)
        {
            if (numeroDireccion.Equals("") || direccion.Equals(""))
            {
                ModelState.AddModelError("direccion", "Se requieren todos los campos de la direccion");
            }

            if (ModelState.IsValid)
            {
                model.direccion_vertedero = tipo.Trim() + " " + direccion.Trim() + " " + numeroDireccion.Trim();
                model.id_unidad = UsuariosRepository.getUnidadSeleccionada();
                RellenosRepository rs = new RellenosRepository(new sgsbdEntities());
                if (model.id_vertedero == 0)
                {
                    rs.InsertRelleno(model);
                }
                else
                {
                    rs.UpdateRelleno(model);
                }

                rs.Save();
                return RedirectToAction("Index", "Rellenos");
            }
            BuildViewBagList();
            return View(model);
        }


        /// <summary>
        /// Invoca a vista Create precargando los datos del relleno seleccionado
        /// </summary>
        /// <param name="id">id relleno</param>
        /// <returns>Carga vista create</returns>
        public ActionResult Edit(int id)
        {
            RellenosRepository rs = new RellenosRepository(new sgsbdEntities());
            vertederos c = rs.GetRellenoByID(id);
            if (!string.IsNullOrEmpty(c.direccion_vertedero))
            {
                string[] split = c.direccion_vertedero.Split(' ');

                //para sacar la direccion y el numero de direccion

                ViewBag.tipo = split[0];
                if (split.Length - 1 >= 1) ViewBag.direccion = split[1];
                else ViewBag.direccion = "";
                if (split.Length - 1 >= 2) ViewBag.numero_direccion = split[2];
                else ViewBag.numero_direccion = "";
            }

            BuildViewBagList();
            return View("create", c);
        }


        /// <summary>
        /// Elimina un relleno bajo un ID definido
        /// </summary>
        /// <param name="id">id relleno</param>
        /// <returns>Recarga vista Index de relleno</returns>
        public ActionResult Delete(int id)
        {
            RellenosRepository rs = new RellenosRepository(new sgsbdEntities());
            rs.DeleteRelleno(id);
            rs.Save();
            return RedirectToAction("Index", "Rellenos");
        }


        /// <summary>
        /// Carga los objetos a usar en combobox por la vista
        /// </summary>
        private void BuildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            PaisesRepository rs1 = new PaisesRepository(ctx);
            ComunasRepository rs2 = new ComunasRepository(ctx);
            CiudadesRepository rs3 = new CiudadesRepository(ctx);
            RegionesRepository rs4 = new RegionesRepository(ctx);
            IEnumerable<object> tipo = new object[] {
             new { text="Avenida",value="Avenida"},
             new { text="Calle",value="Calle"},
             new { text="Pasaje",value="Pasaje"}
            };

            ViewBag.Paises = new SelectList(rs1.GetPaises().OrderBy(m => m.pais), "id_pais", "pais");
            ViewBag.Comunas = new SelectList(rs2.GetComunas().OrderBy(m => m.nombre_comuna), "id_comuna", "nombre_comuna");
            ViewBag.Ciudades = new SelectList(rs3.GetCiudades().OrderBy(m => m.ciudad), "id_ciudad", "ciudad");
            ViewBag.Regiones = new SelectList(rs4.GetRegiones().OrderBy(m => m.nombre_region), "id_region", "nombre_region");
            ViewBag.tipo = new SelectList(tipo, "value", "text", ViewBag.tipo);
        }
    }
}