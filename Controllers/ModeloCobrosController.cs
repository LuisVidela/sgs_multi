﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using Newtonsoft.Json;
using SGS.Models.modelViews;

namespace SGS.Controllers
{
    public class ModeloCobrosController : Controller
    {
        private ModeloCobrosDetalleRepository _rsmc;


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ModeloCobrosRepository rs = new ModeloCobrosRepository(new sgsbdEntities());
            ModeloCobrosView data = rs.GetModeloCobrosTabla(UsuariosRepository.getUnidadSeleccionada());
            return View(data);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            sgsbdEntities ctx = new sgsbdEntities();
            BuildViewBagList();
            ViewBag.punto_servicio_value = "";
            int idUnidad = UsuariosRepository.getUnidadSeleccionada();
            ViewBag.punto_servicio = ctx.puntos_servicio.Where(u => u.contratos.id_unidad == idUnidad).AsEnumerable().Select(x => new SelectListItem

            {
                Text = x.nombre_punto,
                Value = x.id_punto_servicio.ToString(),
            }).OrderBy(x => x.Text);
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(modelo_cobro model)
        {
            ModeloCobrosRepository rs = new ModeloCobrosRepository(new sgsbdEntities());

            if (model.id_modelo == 0)
            {
                rs.InsertModeloCobro(model);
            }
            else
            {
                rs.UpdateModeloCobro(model);
            }
            rs.Save();
            return RedirectToAction("Index", "ModeloCobros");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddNewModel(modelo_cobro model)
        {
            ModeloCobrosRepository rs = new ModeloCobrosRepository(new sgsbdEntities());

            ViewBag.Pto = model.id_punto_servicio;

            PuntosServiciosRepository rsx = new PuntosServiciosRepository(new sgsbdEntities());

            puntos_servicio m = rsx.GetPuntosServiciosByID(Convert.ToInt16(model.id_punto_servicio));

            string modelo = m.nombre_punto.Trim();

            if (model.id_modelo == 0)
            {
                Int32 idOut = 0;
                using (var db = new sgsbdEntities())
                {
                    var item = db.modelo_cobro.FirstOrDefault(u => u.id_punto_servicio == model.id_punto_servicio);
                    if (item != null)
                    {
                        idOut = item.id_modelo;
                    }
                }
                if (rs.ERROR == "")
                {
                    ViewBag.hdnFlag = "15834";
                    ViewBag.modeloId = idOut;
                    ViewBag.data = modelo;
                    ViewBag.ModeloIdJob = idOut;
                    model.modelo = modelo;
                    model.id_modelo = idOut;
                    ViewBag.id_modelo_cobro = idOut;

                }
                else
                {
                    ViewBag.modeloId = "";
                    ViewBag.hdnFlag = idOut;
                    model.modelo = "";
                    model.id_modelo = 0;
                    ViewBag.id_modelo_cobro = 0;
                }
            }

            BuildViewBagList();
            sgsbdEntities ctx = new sgsbdEntities();
            ViewBag.punto_servicio_value = "";
            ViewBag.punto_servicio = ctx.puntos_servicio.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.nombre_punto,
                Value = x.id_punto_servicio.ToString(),
            }).OrderBy(x => x.Text);
            int idModelo = 0;
            using (var db = new sgsbdEntities())
            {
                var item = db.modelo_cobro.FirstOrDefault(u => u.id_punto_servicio == model.id_punto_servicio);
                if (item != null)
                {
                    idModelo = item.id_modelo;
                }
            }
            if (idModelo > 0)
            {
                BuildViewBagList2(idModelo);
            }

            if (ViewBag.ModeloIdJob != 0)
            {
                if (_rsmc == null)
                {
                    _rsmc = new ModeloCobrosDetalleRepository(ctx);
                }
                List<ModeloCobrosDetallesView> data = (List<ModeloCobrosDetallesView>)_rsmc.GetModeloCobroDetallesForDetails(idModelo);
                ViewBag.mcobrosdetalles = data;
            }

            List<SelectListItem> drodwnitems = new List<SelectListItem>
            {
                new SelectListItem {Value = "0", Text = "Seleccione un Valor"}
            };

            var item2 = ctx.puntos_servicio.FirstOrDefault(u => u.id_punto_servicio == model.id_punto_servicio).id_punto_servicio;

            ModeloCobrosRepository rsz = new ModeloCobrosRepository(new sgsbdEntities());
            List<TipoModalidadView> run = (List<TipoModalidadView>)rsz.getTipoModalidad1(item2);
            foreach (var xvar in run)
            {
                drodwnitems.Add(new SelectListItem { Value = xvar.id_tipo_modalidad.ToString(), Text = xvar.tipo_modalidad.Trim() });
            }
            ViewBag.tipo_modalidad = new SelectList(drodwnitems, "value", "text");

            ViewBag.id_modelo_cobro = ViewBag.ModeloIdJob;

            ModeloCobrosRepository rs2 = new ModeloCobrosRepository(new sgsbdEntities());

            if (model.id_modelo == 0)
            {
                rs2.InsertModeloCobro(model);
            }
            else
            {
                rs2.UpdateModeloCobro(model);
            }
            rs2.Save();
            return RedirectToAction("Index", "ModeloCobros");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Edit(int id)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            ModeloCobrosRepository rs = new ModeloCobrosRepository(ctx);
            modelo_cobro c = rs.GetModeloCobroByID(id);

            ViewBag.ModeloIdJob = c.id_modelo;
            ViewBag.Data = c.modelo;

            BuildViewBagList();
            ViewBag.punto_servicio = ctx.puntos_servicio.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.nombre_punto,
                Value = x.id_punto_servicio.ToString(),
            }).OrderBy(x => x.Text);
            return View("edit", c);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult Details(int id)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            ViewBag.id_modelo_cobro = id;
            modelo_cobro modelo = ctx.modelo_cobro.Find(id);
            ViewBag.TitleIs = modelo.modelo;
            BuildViewBagList2(id);
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult DetailsModel(int id, modelo_cobro model)
        {
            ViewBag.TitleIs = model.modelo.Trim();
            ViewBag.id_modelo_cobro = id;
            BuildViewBagList2(id);
            return View("DetailsModel");
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="collection"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DetailsModel(FormCollection collection)
        {
            ViewBag.Error = null;

            int id = Convert.ToInt16(collection.Get("idModCob"));
            string tipoModalidadIs;
            if (collection.Get("id_tipo_modalidad") != null)
            {
                tipoModalidadIs = collection.Get("id_tipo_modalidad").Trim();
            }
            else
            {
                tipoModalidadIs = collection.Get("tipo_modalidad").Trim();
            }
            string valoris = (collection.Get("valor")).Trim();

            if (tipoModalidadIs == "")
            {
                ViewBag.Error = "-987";
                tipoModalidadIs = "0";
            }

            int idMoneda = Convert.ToInt32(collection.Get("id_moneda"));

            Double valor;

            if (!Double.TryParse(valoris, out valor))
            {
                valor = 0;
            }

            modelo_cobro_detalle model = new modelo_cobro_detalle
            {
                id_tipo_modalidad = Convert.ToInt16(tipoModalidadIs),
                valor = valor,
                id_tipo_moneda = idMoneda
            };

            ModeloCobrosDetalleRepository rsmc = new ModeloCobrosDetalleRepository(new sgsbdEntities());

            if (ModelState.IsValid && ViewBag.Error == null)
            {
                model.id_modelo_cobro = id;
                model.id_tipo_modalidad = Convert.ToInt32(tipoModalidadIs);
                using (var db = new sgsbdEntities())
                {
                    var edit = db.modelo_cobro_detalle.FirstOrDefault(u => u.id_modelo_cobro == model.id_modelo_cobro && u.id_tipo_modalidad == model.id_tipo_modalidad);
                    if (edit != null)
                    {
                        edit.valor = model.valor;
                        edit.id_tipo_moneda = model.id_tipo_moneda;
                        db.SaveChanges();
                    }
                    else
                    {
                        rsmc.InsertModeloCobroDetalle(model);
                        rsmc.Save();
                    }
                }

            }
            ViewBag.id_modelo_cobro = id;

            // modelo de cobro
            sgsbdEntities ctx = new sgsbdEntities();

            ModeloCobrosRepository modeloCobrosRepository = new ModeloCobrosRepository(ctx);
            modelo_cobro m = modeloCobrosRepository.GetModeloCobroByID(id);
            GetPointOfServices(m, id, ctx);

            BuildViewBagList();
            BuildViewBagList2(id);
            return View("create", m);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="id"></param>
        /// <param name="ctx"></param>
        private void GetPointOfServices(modelo_cobro m, int id, sgsbdEntities ctx)
        {
            ViewBag.TitleIs = m.modelo;
            ViewBag.id_modelo_cobro = id;

            PuntosServiciosRepository pto = new PuntosServiciosRepository(ctx);
            puntos_servicio es = pto.GetPuntosServiciosByID(Convert.ToInt16(m.id_punto_servicio));
            ViewBag.data = es.nombre_punto;

            ViewBag.punto_servicio_value = m.id_punto_servicio.ToString();
            ViewBag.punto_servicio = ctx.puntos_servicio.AsEnumerable().Select(x => new SelectListItem
            {
                Text = x.nombre_punto,
                Value = x.id_punto_servicio.ToString(),
            }).OrderBy(x => x.Text);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <param name="idMoneda"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Details(modelo_cobro_detalle model, int id, int idMoneda)
        {
            ModeloCobrosDetalleRepository rsmc = new ModeloCobrosDetalleRepository(new sgsbdEntities());

            if (ModelState.IsValid)
            {
                model.id_modelo_cobro = id;
                using (var db = new sgsbdEntities())
                {
                    var edit = db.modelo_cobro_detalle.FirstOrDefault(u => u.id_modelo_cobro == model.id_modelo_cobro && u.id_tipo_modalidad == model.id_tipo_modalidad);
                    if (edit != null)
                    {
                        edit.valor = model.valor;
                        edit.id_tipo_moneda = idMoneda;
                        db.SaveChanges();
                    }
                    else
                    {
                        rsmc.InsertModeloCobroDetalle(model);
                        rsmc.Save();
                    }
                }
                return RedirectToAction("Details", "ModeloCobros", new { id });
            }
            ViewBag.id_modelo_cobro = id;
            BuildViewBagList2(id);
            return View();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="id2"></param>
        /// <returns></returns>
        public ActionResult DeleteModeloDetails(int id, int id2)
        {
            ModeloCobrosDetalleRepository rs = new ModeloCobrosDetalleRepository(new sgsbdEntities());
            rs.DeleteModeloCobroDetalle(id);
            rs.Save();

            return RedirectToAction("Details", "ModeloCobros", new { id = id2 });
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="id2"></param>
        /// <returns></returns>
        public ActionResult DeleteModeloTipoId(int id, int id2)
        {
            ModeloCobrosDetalleRepository modeloCobrosDetalleRepositoryDelete = new ModeloCobrosDetalleRepository(new sgsbdEntities());
            modeloCobrosDetalleRepositoryDelete.DeleteModeloCobroDetalle(id);
            modeloCobrosDetalleRepositoryDelete.Save();

            sgsbdEntities ctx = new sgsbdEntities();

            ModeloCobrosRepository modeloCobrosRepository = new ModeloCobrosRepository(ctx);
            modelo_cobro m = modeloCobrosRepository.GetModeloCobroByID(id2);
            GetPointOfServices(m,id2,ctx);
            
            BuildViewBagList();
            BuildViewBagList2(id);

            return View("create", m);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //public ActionResult Delete(int id)
        //{
        //    ModeloCobrosRepository rs = new ModeloCobrosRepository(new sgsbdEntities());
        //    rs.DeleteModeloCobro(id);
        //    rs.Save();
        //    return RedirectToAction("Index", "ModeloCobros");
        //}



        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        private void BuildViewBagList2(int id)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            TipoModalidadRepository rs = new TipoModalidadRepository(ctx);

            if (_rsmc == null)
            {
                _rsmc = new ModeloCobrosDetalleRepository(ctx);
            }
            List<ModeloCobrosDetallesView> data = (List<ModeloCobrosDetallesView>)_rsmc.GetModeloCobroDetallesForDetails(id);
            TipoMonedaRepository rs3 = new TipoMonedaRepository(ctx);

            ViewBag.tipo_moneda = new SelectList(rs3.getTipoMonedaForList(), "Value", "Text");

            ViewBag.mcobrosdetalles = data;

            ViewBag.tipo_modalidad = new SelectList(rs.getTipoModalidad1(id), "value", "text");
        }


        /// <summary>
        /// 
        /// </summary>
        private void BuildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            TipoServicioRepository rs = new TipoServicioRepository(ctx);
            ModeloCobrosRepository rs2 = new ModeloCobrosRepository(ctx);

            ViewBag.tipo_modelo = new SelectList(rs2.getTipoModeloForList(), "value", "text");
            ViewBag.servicios = new SelectList(rs.GetTiposServicios().OrderBy(m => m.tipo_servicio1), "id_tipo_servicio", "tipo_servicio1");
        }

        public ActionResult excel()
        {
            ModeloCobrosView model = new ModeloCobrosView();
            model.tabla = new List<tablaModeloCobrosView>();
            using (var db = new sgsbdEntities())
            {
                var datos = db.modelo_cobro.ToList();
                if (datos.Count != 0)
                {
                    foreach (var item in datos)
                    {
                        tablaModeloCobrosView row = new tablaModeloCobrosView();

                        row.idModelo = item.id_modelo;
                        row.nombreModelo = item.modelo;
                        row.tipoModelo = item.tipo_modelo.tipo_modelo1;
        
                        var detallecobro = db.modelo_cobro_detalle.Where(d => d.id_modelo_cobro == item.id_modelo).ToList();

                        if (detallecobro.Count > 0)
                        {
                            foreach (var item2 in detallecobro)
                            {

                                var tipomodalidad = db.tipo_modalidad.Where(y => y.id_tipo_modalidad == item2.id_tipo_modalidad).ToList();
                                foreach (var item3 in tipomodalidad)
                                {

                                    tablaModeloCobrosView row2 = new tablaModeloCobrosView();

                                    row2.tipoModalidad = item3.tipo_modalidad1;
                                    row2.valor = item2.valor;
                                    row2.nombreModelo = item.modelo;
                                    row2.tipoModelo = item.tipo_modelo.tipo_modelo1;

                                    var lstServicio = db.puntos_servicio.Where(x => x.id_punto_servicio == item.id_punto_servicio).ToList();
                                    if (lstServicio.Count > 0)
                                    {
                                        row2.nombrePuntoServicio = lstServicio.FirstOrDefault().nombre_punto;
                                        int idcontrato = lstServicio.FirstOrDefault().id_contrato;
                                        var contrato = db.contratos.Where(x => x.id_contrato == idcontrato).FirstOrDefault();
                                        int idcliente = contrato.id_cliente;
                                        var cliente = db.clientes.Where(x => x.id_cliente == idcliente).FirstOrDefault();
                                        row2.cliente = cliente.razon_social;
                                        var id_tipo_contrato = contrato.id_tipo_contrato;
                                        var tipo_contrato = db.tipo_contrato.Where(x => x.id_tipo_contrato == id_tipo_contrato);
                                        row2.tiposervicio = tipo_contrato.FirstOrDefault().tipo_contrato1;
                                        var tipopago = db.tipo_moneda.Where(x => x.id_moneda == item2.id_tipo_moneda).FirstOrDefault();
                                        if (tipopago != null)
                                        {
                                            row2.tipopago = tipopago.moneda;
                                        }

                                    }


                                    model.tabla.Add(row2);

                                }
                            }
                        }
                    }
                }
            }
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="search"></param>
        /// <param name="tipoServicio"></param>
        /// <returns></returns>
        public ActionResult ListForAutocomplete(string search, int tipoServicio)
        {
            sgsbdEntities ctx = new sgsbdEntities();
            ModeloCobrosRepository rs = new ModeloCobrosRepository(ctx);
            var result = rs.getForAutocomplete(search, tipoServicio);

            string data = JsonConvert.SerializeObject(result);

            return Content(data);
        }
    }
}
