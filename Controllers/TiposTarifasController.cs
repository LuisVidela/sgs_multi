﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.repository;
using SGS.Models;

namespace SGS.Controllers
{
    public class TiposTarifasController : Controller
    {
        //
        // GET: /TiposTarifas/

        private sgsbdEntities ctx = new sgsbdEntities();

        public ActionResult Index()
        {
            TipoTarifaRepository rs = new TipoTarifaRepository(new sgsbdEntities());
            List<tipo_tarifa> data = (List<tipo_tarifa>)rs.GetTipoTarifas(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());

            return View(data);
        }
        public ActionResult Create()
        {

            this.buildViewBagList();

           IEnumerable<object> estado = new object[] {
            new { text="Activo",value=1},
            new { text="Inactivo",value=2}
           };
           ViewBag.estado = new SelectList(estado, "value", "text", ViewBag.estado_tarifa);

            return View();
        }

        private void buildViewBagList()
        {

            UnidadesNegocioRepository rs = new UnidadesNegocioRepository(ctx);
            TipoMonedaRepository rs1 = new TipoMonedaRepository(ctx);

            ViewBag.tipo_moneda = new SelectList(rs1.getTipoMonedaForList(), "Value", "Text");
          
        }

        [HttpPost]
        public ActionResult create(SGS.Models.tipo_tarifa model)
        {

            if (ModelState.IsValid)
            {
                TipoTarifaRepository rs = new TipoTarifaRepository(new sgsbdEntities());

                if (model.id_tipo_tarifa == 0)
                {
                    rs.InsertTipoTarifa(model);
                }
                else
                {
                    rs.UpdateTipoTarifa(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "TiposTarifas");

            }
            this.buildViewBagList();
              return View(model);
        }

        //*********************************************

        public ActionResult Create2()
        {
            //this.buildViewBagList();

            return View();
        }

        [HttpPost]
        public ActionResult create2(SGS.Models.tipo_tarifa model, int id)
        {

            if (ModelState.IsValid)
            {
                TipoTarifaRepository rs = new TipoTarifaRepository(new sgsbdEntities());
                model.id_unidad = SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada();
                if (model.id_tipo_tarifa == 0)
                {
                    rs.InsertTipoTarifa(model);
                }
                else
                {
                    rs.UpdateTipoTarifa(model);
                }
                rs.Save();

                return this.RedirectToAction("edit", "PuntosServicios", new { id }); //{ id = Int32.Parse(id) });

            }
            return View(model);
        }

        //*********************************************

       //public ActionResult edit(int id)
       //{
       //    TipoTarifaRepository rs = new TipoTarifaRepository(new sgsbdEntities());
       //    tipo_tarifa t = rs.GetTipoTarifaByID(id);

       //    this.buildViewBagList();
       //    return View("create", t);
       //}
        public ActionResult edit(int id)
        {
            TipoTarifaRepository rs = new TipoTarifaRepository(new sgsbdEntities());
            tipo_tarifa t = rs.GetTipoTarifaByID(id);
        
            IEnumerable<object> rsestado = new object[] {
             new { text="Activo",value="1"},
             new { text="Inactivo",value="2"}
            };
            ViewBag.estadoTarifa = new SelectList(rsestado, "value", "text");

            this.buildViewBagList();
            return View("create", t);
        }


        //public ActionResult Delete(int id) {


        //    sgsbdEntities ctx = new sgsbdEntities();
        //    TipoTarifaRepository r = new TipoTarifaRepository(ctx);
        //    r.DeleteTipoTarifa(id);
        //    r.Save();

        //    return RedirectToAction("Index","TiposTarifas");

        //}

    }
}
