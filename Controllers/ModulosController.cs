﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using System.Data;

namespace SFS.Controllers
{
    public class ModulosController : Controller
    {

        sgsbdEntities ctx = new sgsbdEntities();
        public ActionResult Index()
        {


            List<modulos> data = ctx.modulos.OrderBy(x => x.nombre).ToList();

            return View(data);
        }

        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(modulos model)
        {

            if (ModelState.IsValid)
            {



                if (model.id == 0)
                {

                    ctx.modulos.Add(model);

                }
                else
                {
                    var entry = ctx.Entry(model);
                    entry.State = EntityState.Modified;
                }

                ctx.SaveChanges();
                return RedirectToAction("Index", "Modulos");
            }


            return View(model);

        }


        public ActionResult edit(int id)
        {
            modulos model = ctx.modulos.Where(p => p.id == id).FirstOrDefault();
            return View("Create", model);

        }


        public ActionResult Delete(int id)
        {


            modulos model = ctx.modulos.Where(p => p.id == id).FirstOrDefault();
            ctx.modulos.Remove(model);
            ctx.SaveChanges();
            return RedirectToAction("Index", "Modulos");

        }



    }
}
