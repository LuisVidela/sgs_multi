﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;

namespace SGS.Controllers
{
    public class ConductoresController : Controller
    {
        //
        // GET: /Conductores/
        

        public ActionResult Index()
        {
           
            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
           // List<conductores>data=(List<conductores>)rs.GetConductores();

            List<ConductoresView> data = (List<ConductoresView>)rs.getConductoresView(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada());
            return View(data);
        }


        public ActionResult create()
        {
            this.buildViewBagList();
            return View();
        }

        [HttpPost]
        public ActionResult create(conductores model)
        {
            using (var db = new sgsbdEntities())
            {
                if (db.conductores.Where(u => u.rut_conductor.Equals(model.rut_conductor)).FirstOrDefault() != null)
                {
                     ModelState.AddModelError("rut", "Ya hay un coductor con el RUT ingresado");
                }
            }
            if (ModelState.IsValid) {

                ConductoresRepository rs=new ConductoresRepository(new sgsbdEntities());

                if (model.id_conductor == 0)
                {
                    rs.InsertConductor(model);
                }
                else
                {
                    rs.UpdateConductor(model);
                }
                rs.Save();
                return this.RedirectToAction("Index", "Conductores");

            }
            
            this.buildViewBagList();
            return View(model);
        }

        public ActionResult edit(int id)
        {

            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
            conductores c = rs.GetConductorByID(id);
            ViewBag.value_estado = c.estado;
            this.buildViewBagList();
            return View("create", c);
        }

        public ActionResult Delete(int id)
        {
            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
            rs.DeleteConductor(id);
            rs.Save();
            return this.RedirectToAction("Index", "Conductores");
           
        }

        public ActionResult view(int id)
        {
            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
            conductoresShadowBoxView c = rs.getConductorShadowBox(id);
            return View(c);
        }

        public ActionResult restrict(int id)
        {

            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
            conductoresShadowBoxView data = rs.getConductorShadowBox(id);
            ViewBag.restricciones = data;
            ViewBag.id_conductor = id;

            this.buildViewBagList2();
            return View();

        }


        public ActionResult deleteRestriction(int id)
        {


            RestriccionClienteConductorRepository rs = new RestriccionClienteConductorRepository(new sgsbdEntities());
            rs.DeleteRestriccionClienteConductor(id);
            rs.Save();
            return this.RedirectToAction("Index", "Conductores");



        }


        public ActionResult excel()
        {
            ConductoresRepository rs = new ConductoresRepository(new sgsbdEntities());
            return View(rs.GetConductoresExcel());
        }

        [HttpPost]
        public ActionResult restrict(restriccion_conductor_cliente model, string id)
        {
            RestriccionClienteConductorRepository rs = new RestriccionClienteConductorRepository(new sgsbdEntities());
            model.id_conductor = Int32.Parse(id);
            rs.InsertRestriccionClienteConductor(model);
            rs.Save();
            return this.RedirectToAction("Restrict", "Conductores", new { id = Int32.Parse(id) });



        }

        private void buildViewBagList()
        {
            sgsbdEntities ctx = new sgsbdEntities();

            TipoLicenciaRepository rs = new TipoLicenciaRepository(ctx);
            FranquiciadosRepository rs1 = new FranquiciadosRepository(ctx);
            IEnumerable<object> estado = new object[] {
             new { text="Activo",value="Activo"},
             new { text="Inactivo",value="Inactivo"},
             
            };

            ViewBag.franquiciado = new SelectList(rs1.GetFranquiciados(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()).OrderBy(m => m.id_franquiciado), "id_franquiciado", "razon_social");
            ViewBag.licencia = new SelectList(rs.GetTipoLicencias().OrderBy(m=>m.tipo_licencia1),"id_tipo_licencia","tipo_licencia1");
            ViewBag.estado = new SelectList(estado, "value", "text", ViewBag.value_estado);

      
        }

        private void buildViewBagList2()
        {

            sgsbdEntities ctx = new sgsbdEntities();
            ClientesRepository rs = new ClientesRepository(ctx);

            ViewBag.clientes2 = new SelectList(rs.GetClientesForList2(SGS.Models.repository.UsuariosRepository.getUnidadSeleccionada()), "id_cliente", "razon_social");
        }

        

    }
}
