USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_CALCULO_PUNTO_2]    Script Date: 25-07-2017 16:49:21 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_CALCULO_PUNTO_2]
AS
    SELECT   id_calculo ,
             Id_cliente ,
             rut_cliente ,
             razon_social ,
             nombre_fantasia ,
             ISNULL(id_punto_servicio, 0) idpunto_servicio ,
             nombre_punto ,
             Numero_Retiros ,
             SUM(ValorTotal) AS ValorTotal ,
             Fecha ,
             Peso ,
             id_contrato ,
             id_sucursal ,
             id_tipo_modalidad ,
             id_unidad
    FROM     V_CALCULO_PUNTO
    GROUP BY id_calculo ,
             Id_cliente ,
             rut_cliente ,
             razon_social ,
             nombre_fantasia ,
             id_punto_servicio ,
             nombre_punto ,
             Numero_Retiros ,
             Fecha ,
             Peso ,
             id_contrato ,
             id_sucursal ,
             id_tipo_modalidad ,
             id_unidad;
GO
