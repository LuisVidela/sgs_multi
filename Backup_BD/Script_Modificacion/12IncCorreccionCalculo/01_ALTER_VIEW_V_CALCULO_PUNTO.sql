USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_CALCULO_PUNTO]    Script Date: 25-07-2017 17:15:35 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_CALCULO_PUNTO]
AS
    SELECT 0 AS id_calculo ,
           CASE WHEN vpt.Id_cliente IS NULL THEN cobr.id_cliente
                ELSE vpt.Id_cliente
           END AS Id_cliente ,
           CASE WHEN vpt.rut_cliente IS NULL THEN cobr.rut_cliente
                ELSE vpt.rut_cliente
           END AS rut_cliente ,
           CASE WHEN vpt.id_contrato IS NULL THEN cobr.id_contrato
                ELSE vpt.id_contrato
           END AS id_contrato ,
           CASE WHEN vpt.razon_social IS NULL THEN cli.razon_social
                ELSE vpt.razon_social
           END AS razon_social ,
           CASE WHEN vpt.nombre_fantasia IS NULL THEN cli.nombre_fantasia
                ELSE vpt.nombre_fantasia
           END AS nombre_fantasia ,
           CASE WHEN vpt.Sucursal IS NULL THEN suc.sucursal
                ELSE vpt.Sucursal
           END AS Sucursal ,
           CASE WHEN vpt.id_punto_servicio IS NULL THEN
                    cobr.id_punto_servicio
                ELSE vpt.id_punto_servicio
           END AS id_punto_servicio ,
           CASE WHEN vpt.nombre_punto IS NULL THEN ps.nombre_punto
                ELSE vpt.nombre_punto
           END AS nombre_punto ,
           ISNULL(nrs.nro_retiros, 0) AS Numero_Retiros ,
           CASE WHEN vpt.Valor IS NULL THEN cobr.Valor
                ELSE vpt.Valor
           END AS Valor ,
           CASE WHEN vpt.IVA IS NULL THEN 0
                ELSE vpt.IVA
           END AS IVA ,
           CASE WHEN vpt.NETO IS NULL THEN 0
                ELSE vpt.NETO
           END AS NETO ,
           CASE WHEN vpt.Peso IS NULL THEN cobr.Disposicion
                ELSE vpt.Peso
           END AS Peso ,
           CASE WHEN vpt.Fecha IS NULL THEN cobr.Fecha
                ELSE vpt.Fecha
           END AS Fecha ,
           CASE WHEN vpt.Monthis IS NULL THEN SUBSTRING(cobr.Fecha, 1, 2)
                ELSE vpt.Monthis
           END AS Monthis ,
           CASE WHEN vpt.Yearis IS NULL THEN SUBSTRING(cobr.Fecha, 3, 6)
                ELSE vpt.Yearis
           END AS Yearis ,
           CASE WHEN vpt.id_sucursal IS NULL THEN con.id_sucursal
                ELSE vpt.id_sucursal
           END AS id_sucursal ,
           ISNULL(cobr.Valor, 0) AS ValorTotalDisposicion ,
           CASE WHEN vpt.Valor IS NULL
                     OR CONVERT(INT, vpt.Valor) = cobr.Valor THEN
                    ISNULL(cobr.Valor, 0)
                ELSE
                    ISNULL(vpt.Valor, 0) + ISNULL(cobr.Valor, 0)
                    + ISNULL(cfp.ValorTotal, 0)
           END AS ValorTotal ,
           ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros ,
           con.id_unidad ,
           cfp.id_tipo_modalidad
    FROM   dbo.calculo_cobro_disposicion AS cobr
           LEFT OUTER JOIN dbo.V_PUNTO_SIN_DISPOCICION AS vpt ON vpt.id_punto_servicio = cobr.id_punto_servicio
                                                                 AND vpt.Fecha = cobr.Fecha
           LEFT OUTER JOIN dbo.V_CONTRATO_NO_RETIROS AS nore ON vpt.Id_cliente = nore.id_cliente
                                                                AND vpt.id_contrato = nore.id_contrato
                                                                AND vpt.id_punto_servicio = nore.id_punto_servicio
                                                                AND vpt.Fecha = nore.Fecha
           INNER JOIN dbo.clientes AS cli ON cobr.id_cliente = cli.id_cliente
                                             AND cli.id_unidad = cobr.id_unidad
           INNER JOIN dbo.contratos AS con ON cobr.id_contrato = con.id_contrato
                                              AND con.id_unidad = cli.id_unidad
                                              AND con.id_unidad = cobr.id_unidad
           INNER JOIN dbo.puntos_servicio AS ps ON cobr.id_punto_servicio = ps.id_punto_servicio
           INNER JOIN dbo.sucursales AS suc ON con.id_sucursal = suc.id_sucursal
           LEFT JOIN V_COBROS_FIJOS_PUNTOS cfp ON cfp.id_punto_servicio = ps.id_punto_servicio
                                                  AND cfp.Fecha = cobr.Fecha
           INNER JOIN V_NRO_RETIROS_SERVICIOS nrs ON nrs.id_punto_servicio = ps.id_punto_servicio
                                                     AND nrs.Fecha = cobr.Fecha
    UNION
    SELECT id_calculo ,
           fss.id_cliente ,
           rut_cliente ,
           fss.id_contrato ,
           razon_social ,
           nombre_fantasia ,
           sucursal ,
           id_punto_servicio ,
           nombre_punto ,
           0 AS Numero_Retiros ,
           valor ,
           0 AS IVA ,
           0 AS NETO ,
           0 AS Peso ,
           Fecha ,
           Monthis ,
           Yearis ,
           fss.id_sucursal ,
           0 AS ValorTotalDisposicion ,
           ValorTotal ,
           0 AS Numero_No_Retiros ,
           con.id_unidad ,
           fss.id_tipo_modalidad
    FROM   V_COBROS_FIJOS_SIN_SERVICIOS fss
           INNER JOIN contratos con ON con.id_cliente = fss.id_cliente;
GO


