--****************************************************************************
--Vincula los menu con los submenu
-------------------------Importante-------------------------------------------
--Antes de ejecutar verificar el numero del id en la tabla modulos del registro procesos y administración
--id 7 es de procesos
--id 8 administración
--Modificar si cambiaron
------------------------------------------------------------------------------
--****************************************************************************

delete from [dbo].[webpages_UsersInRoles];
delete from [dbo].[webpages_Roles];


INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Clientes',1)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Sucursales',1)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Contratos',1)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ModeloCobro',1)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('PuntosServicio',1)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Franquiciados',2)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Leasing',2)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('HistorialPagos',2)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Descuentos',2) 
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Camiones',6) 
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('CentroResponsabilidad',6) 
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Conductores',6) 
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('UnidadNegocio',6) 
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Comunas',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Equipos',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Herramientas',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('test',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Rellenos',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Residuos',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Rubros',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('UF',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ConceptosDescuentos',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Tarifas',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ValorPetroleo',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ProcesoPagos',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ProcesoCobros',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('RutasModelo',3)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('IngresarHDR',3)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('HojasRuta',3)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('InformesPuntos',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('CostosPuntos',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ServiciosPuntos',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('PagoFranquiciados',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Usuarios',8)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Perfiles',8)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('Modulos',8)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('SubModulos',8)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franProcPrepagar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franProcPagar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franProcExportar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franDescEditar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franDescEliminar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franLeaEditar',2)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('franLeaEliminar',2)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('manIndicadores',6)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('conCon',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('conConAgregar',7)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ResporteReajusteHistorico',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ResporteReajusteHistoricoReversar',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('ReporteListaServicios',4)
INSERT INTO [dbo].[webpages_Roles]([RoleName],[modulo_id])VALUES('GuiaDespacho',4)




delete from [dbo].[webpages_Roles] where RoleName = 'test';

SELECT * FROM [dbo].[webpages_Roles]