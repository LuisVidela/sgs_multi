
-- =============================================
-- Author:		valentys
-- Create date: 10-06-2015
-- Description:	realiza el proceso de reajuste
-- =============================================
-- exec SP_UPD_Reajeste null, 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_Reajeste](
	@CODIGOS_CLIENTES VARCHAR(MAX) = NULL,
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL
)AS
BEGIN
	SET NOCOUNT ON;

	  -- DECLARE @CODIGOS_CLIENTES VARCHAR(MAX) = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'
	   DECLARE @ID_UNIDAD INT = 2
	         , @ID_CLIENTE INT
	         , @VALOR float
			 , @ID_MODELO_COBRO INT
			 , @ID_DETALLE_MODELO_COBRO INT
			 , @ID_CONTRATO INT
	         , @MES_ULTIMO_REAJUSTE INT = 1
			 , @ANIO_ULTIMO_REAJUSTE INT
			 , @REAJ_CODIGO int 
			 , @CANTIDAD_CONFIGURACION int 
			 , @VALOR_REAJUSTE float

	   IF (@CODIGOS_CLIENTES IS NOT NULL AND @CODIGOS_CLIENTES != '')
		   BEGIN
			   --Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar.
			   --QUERY QUE REALIZA EL CALCULO SOLO A LOS CLIENTES QUE SE ENVIAN POR PARAMETRO
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO  = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO  = REAJ.REAJ_CODIGO
				 WHERE --CONT.id_unidad  = @ID_UNIDAD
				   --AND 
					   CONT.id_cliente IN (SELECT * FROM split(@CODIGOS_CLIENTES,','))
				   AND REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0;
			END
		ELSE
			BEGIN
				--Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar
				--BUSCA SOLO LOS CLIENTES A QUIEN SE DEBE APLICAR EL REAJUSTE
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
				 WHERE /*CONT.id_unidad  = @ID_UNIDAD
				   AND */REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0;
			END


		  --Abre el cursor y recorro a los clientes.
		  OPEN CR_id_clientes
		 FETCH NEXT FROM CR_id_clientes
		  INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
		 WHILE @@FETCH_STATUS = 0
			 BEGIN

					IF (@CANTIDAD_CONFIGURACION = 1)
						BEGIN
							--SI LA CONFIGURACI�N ES UNA SOLA ES PORQUE ES ANUAL Y TOMA DESDE EL MISMO MES PERO DEL A�O PASADO
							SELECT @MES_ULTIMO_REAJUSTE  = REDE.READ_MES 
								 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1 
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
						END
					ELSE
						BEGIN
							
							SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
							     , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
							   AND REDE.READ_MES < ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))

							   IF (@MES_ULTIMO_REAJUSTE IS NULL)
								   BEGIN
										SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
											 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1
										  FROM contratos			   CONT with(nolock)
									INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
									INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
										 WHERE CONT.id_contrato = @ID_CONTRATO
								   END

						END


					--CALCULA EL VALOR DE REAJUSTE

					SET @VALOR_REAJUSTE = (	SELECT SUM(INDV.INDV_VALOR)
											  FROM TB_SGS_INDICADOR          INDI with(nolock) 
										INNER JOIN TB_SGS_INDICADOR_VALOR    INDV with(nolock) ON INDI.INDI_CODIGO = INDV.INDV_INDI_CODIGO
										INNER JOIN TB_SGS_REAJUSTE_INDICADOR REIN with(nolock) ON REIN.REIN_INDI_CODIGO = INDI.INDI_CODIGO
											 WHERE Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') >= Convert(varchar(4), @ANIO_ULTIMO_REAJUSTE) + REPLACE(STR(@MES_ULTIMO_REAJUSTE, 2), SPACE(1), '0')
											   AND Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') <  Convert(varchar(4), ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))) +  Convert(varchar(2), REPLACE(STR(@MES_REAJUSTE, 2), SPACE(1), '0'))
											   AND REIN.REIN_REAJ_CODIGO = @REAJ_CODIGO)
					
					--print('ultimo mes : ' + convert(varchar(100) , @MES_ULTIMO_REAJUSTE))
					--print('ultimo a�o : ' + convert(varchar(100) , @ANIO_ULTIMO_REAJUSTE))
					--print('mes reajuste : ' + convert(varchar(100) , @MES_REAJUSTE))
					--print('a�o reajuste : ' + convert(varchar(100) , @ANIO_REAJUSTE))
					--print('codigo reajuste : ' + convert(varchar(100) , @REAJ_CODIGO))
					--print('valor reajuste: ' + convert(varchar(100) , @VALOR_REAJUSTE))
					IF (@VALOR_REAJUSTE > 0)
					BEGIN
						--INSERTO EL VALOR DEL DETALLE DE MODELO ACTUAL A LA TABLA HISTORICOS.
						INSERT INTO [TB_HIS_MODELO_COBRO_DETALLE]
								   ([HMCD_ID_DETALLE_MODELO_COBRO]
								   ,[HMCD_VALOR]
								   ,[HMCD_FECHA]
								   ,[HMCD_REAJUSTE])
							 VALUES
								   (@ID_DETALLE_MODELO_COBRO
								   ,@VALOR
								   ,GETDATE()
								   ,@VALOR_REAJUSTE)

						--ACTUALIZA EL VALOR DEL MODELO DE COBRO
						UPDATE [modelo_cobro_detalle]
						   SET [valor] = round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100), 0)
						 WHERE [id_detalle_modelo_cobro] = @ID_DETALLE_MODELO_COBRO
					

						--print('ultimo mes : ' + convert(varchar(100) , @MES_ULTIMO_REAJUSTE))
						--print('ultimo a�o : ' + convert(varchar(100) , @ANIO_ULTIMO_REAJUSTE))
						--print('mes reajuste : ' + convert(varchar(100) , @MES_REAJUSTE))
						--print('a�o reajuste : ' + convert(varchar(100) , @ANIO_REAJUSTE))
						--print('codigo reajuste : ' + convert(varchar(100) , @REAJ_CODIGO))
						
						--print('id_contrato = ' + convert(varchar(10) , @ID_CONTRATO) + 
						--	  ' ultimo mes reajuste = ' + convert(varchar(10) ,@MES_ULTIMO_REAJUSTE) + '-' + convert(varchar(10) , @ANIO_ULTIMO_REAJUSTE) +
						--	  ' VALOR REAJUSTE = ' + convert(varchar(10) , @VALOR_REAJUSTE) +
						--	  ' ID_MODELO DE COBRO DETALLE = ' + convert(varchar(10) , @ID_DETALLE_MODELO_COBRO) +
						--	  ' VALOR ACTUAL = ' + convert(varchar(100) , @VALOR) + 
						--	  ' VALOR ACTUALIZADO = ' + convert(varchar(100) , round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100),0)))
					
					END
					--Pr�ximo cliente
					FETCH NEXT FROM CR_id_clientes
					INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
			   END 
		 CLOSE CR_id_clientes;
	DEALLOCATE CR_id_clientes;
    
END

go


CREATE VIEW [dbo].[V_Comprobacion_Reajuste]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT CONT.id_cliente, CONT.id_contrato, SUCU.sucursal, TMOD.tipo_modalidad, PUNT.nombre_punto, HIST.HMCD_FECHA AS FechaReajuste, HIST.HMCD_REAJUSTE AS ValorReajuste, 
                         HIST.HMCD_VALOR AS ValorAnterior, MODD.valor
FROM            dbo.contratos AS CONT WITH (nolock) INNER JOIN
                         dbo.sucursales AS SUCU WITH (nolock) ON SUCU.id_cliente = CONT.id_cliente INNER JOIN
                         dbo.puntos_servicio AS PUNT WITH (nolock) ON PUNT.id_contrato = CONT.id_contrato INNER JOIN
                         dbo.modelo_cobro AS MODE WITH (nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio INNER JOIN
                         dbo.modelo_cobro_detalle AS MODD WITH (nolock) ON MODE.id_modelo = MODD.id_modelo_cobro INNER JOIN
                         dbo.tipo_modalidad AS TMOD WITH (nolock) ON TMOD.id_tipo_modalidad = MODD.id_tipo_modalidad INNER JOIN
                         dbo.TB_HIS_MODELO_COBRO_DETALLE AS HIST WITH (nolock) ON HIST.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro INNER JOIN
                         dbo.TB_SGS_REAJUSTE AS REAJ WITH (nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato INNER JOIN
                         dbo.TB_SGS_REAJUSTE_DETALLE AS REDE WITH (nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
ORDER BY CONT.id_contrato

GO
