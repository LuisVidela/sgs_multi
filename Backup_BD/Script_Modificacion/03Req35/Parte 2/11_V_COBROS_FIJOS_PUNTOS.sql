USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_COBROS_FIJOS_PUNTOS]    Script Date: 06-01-2017 12:35:57 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_COBROS_FIJOS_PUNTOS]
AS
    SELECT  id_calculo ,
            id_hoja_ruta ,
            id_punto_servicio ,
            cantidad ,
            peso ,
            estado ,
            fechaFull ,
            Fecha ,
            Monthis ,
            Yearis ,
            nombre_punto ,
            estado_punto ,
            id_contrato ,
            id_cliente ,
            id_sucursal ,
            estado_contrato ,
            id_modelo_cobro ,
            rut_cliente ,
            razon_social ,
            giro_comercial ,
            nombre_fantasia ,
            sucursal ,
            descripcion ,
            rut_sucursal ,
            id_tipo_modalidad ,
            valor ,
            tipo_modalidad ,
            como_se_calcula ,
            bruto ,
            proceso_cerrado ,
            fechaCalculo ,
            Orden ,
            id_detalle_modelo_cobro ,
            sorting ,
            glosa ,
            cobro_arriendo ,
            id_camion ,
            camion ,
            id_conductor ,
            conductor ,
            fechacorta ,
            sistema ,
            franquiciado ,
            estadopunto ,
            relleno ,
            pagofranquiciado ,
            margen ,
            margenporc ,
            valorenuf ,
            valoruf ,
            bruto AS ValorTotal
    FROM    dbo.calculo_cobro_clientes
    WHERE   ( id_tipo_modalidad NOT IN ( 6, 8, 17 ) )
            AND ( valor > 0 )
            AND ( valor IS NOT NULL );
GO


