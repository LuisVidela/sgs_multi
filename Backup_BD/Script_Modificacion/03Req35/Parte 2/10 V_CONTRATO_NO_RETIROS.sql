USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_CONTRATO_NO_RETIROS]    Script Date: 29-12-2016 15:47:16 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_CONTRATO_NO_RETIROS]
AS
    SELECT  id_cliente ,
            Fecha ,
            id_contrato ,
            id_punto_servicio ,
            COUNT(id_punto_servicio) AS Numero_No_Retiros
    FROM    calculo_cobro_clientes
    WHERE   glosa NOT IN  ('Retiro', 'Retiro Manejo Dris')
    GROUP BY id_cliente ,
            Fecha ,
            id_contrato ,
            id_punto_servicio;














GO


