USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_DISPOSICION]    Script Date: 29-12-2016 12:45:31 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_DISPOSICION]
AS
    SELECT  0 AS id_dispo ,
            id_punto_servicio ,
            Fecha ,
            id_cliente ,
            glosa ,
            SUM(ISNULL(CAST(peso AS DECIMAL(18, 2)), 0)) AS Disposicion ,
            0.00 AS Valor
    FROM    dbo.V_CALCULO_COBRO_PUNTO AS cal
    GROUP BY id_cliente ,
            Fecha ,
            id_punto_servicio ,
            glosa
    HAVING  ( glosa = 'Retiro'
              OR glosa = 'Retiro Manejo Dris'
            )
            AND ( SUM(ISNULL(peso, 0)) > 0 );
GO


