USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]    Script Date: 28-12-2016 10:19:11 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]
AS
    SELECT  cob.id_punto_servicio ,
            cob.id_modelo ,
            deta.valor ,
            deta.id_tipo_modalidad ,
            0 AS id_cliente ,
            0 AS id_contrato ,
            0 AS id_sucursal ,
            0 AS id_tipo_contrato
    FROM    dbo.modelo_cobro AS cob
            INNER JOIN dbo.modelo_cobro_detalle AS deta ON cob.id_modelo = deta.id_modelo_cobro
    WHERE   ( cob.id_punto_servicio IS NOT NULL )
            AND ( deta.id_tipo_modalidad IN ( 8, 17 ) )
            AND ( deta.valor > 0 );

GO


