USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_NRO_RETIROS_SERVICIOS]    Script Date: 28-12-2016 10:20:08 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_NRO_RETIROS_SERVICIOS]
AS
    SELECT  COUNT(glosa) AS nro_retiros ,
            id_punto_servicio
    FROM    dbo.calculo_cobro_clientes
    WHERE   ( glosa LIKE 'Retiro' )
            OR ( glosa LIKE 'Retiro Manejo Dris' )
    GROUP BY id_punto_servicio;

GO


