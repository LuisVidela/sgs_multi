-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE SP_CALCULO_COBRO_CLIENTES_unidad 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT        id_pago, id_cliente, rut_cliente, razon_social, nombre_fantasia, Numero_retiros, Valor, IVA, NETO, Peso, Fecha, Monthis, Yearis, ValorTotalDisposicion, ValorTotal, Numero_No_Retiros, ccc.id_unidad
FROM            dbo.V_CALCULO_COBRO_CLIENTES AS ccc
END
GO
