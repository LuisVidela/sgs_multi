USE [RS_SGS_ARAUCO];
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_REPORTE_UNIFICADO]    Script Date: 04-01-2017 9:35:21 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =============================================
-- Author:		<Intellego,José Joaquín Sepúlveda>
-- Create date: <06/12/2016>
-- Update date: <29/12/2016>
-- Description:	<Generación de procedimiento almacenado para recuperación de reporte unificado.>
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_REPORTE_UNIFICADO] 
	-- Add the parameters for the stored procedure here
    @Periodo VARCHAR(6) = NULL -- fecha comienzo
    ,
    @IdCliente INT = NULL
AS
    BEGIN
        SELECT  iru.nombre_item ,
                iru.orden_item ,
                umb.nombre ,
                ( SELECT    SUM(mcd2.valor) AS Fijo
                  FROM      modelo_cobro AS mc2
                            INNER JOIN modelo_cobro_detalle AS mcd2 ON mc2.id_modelo = mcd2.id_modelo_cobro
                            INNER JOIN puntos_servicio AS pc2 ON pc2.id_punto_servicio = mc2.id_punto_servicio
                            INNER JOIN V_CALCULO_PUNTO AS vcp2 ON vcp2.id_punto_servicio = pc2.id_punto_servicio
                            INNER JOIN item_reporte_unificado AS iru2 ON iru2.id_item = pc2.id_item
                  WHERE     vcp2.Fecha LIKE @Periodo
                            AND mcd2.id_tipo_modalidad IN ( 5, 7, 9, 10, 11,
                                                            12 )
                            AND iru2.id_item = iru.id_item
                ) AS Fijo ,
                ( SELECT    SUM(vcp2.ValorTotal)
                  FROM      modelo_cobro AS mc2
                            INNER JOIN modelo_cobro_detalle AS mcd2 ON mc2.id_modelo = mcd2.id_modelo_cobro
                            INNER JOIN puntos_servicio AS pc2 ON pc2.id_punto_servicio = mc2.id_punto_servicio
                            INNER JOIN V_CALCULO_PUNTO_2 AS vcp2 ON vcp2.idpunto_servicio = pc2.id_punto_servicio
                                                              AND ( vcp2.id_tipo_modalidad IS NULL
                                                              OR vcp2.id_tipo_modalidad <> 55
                                                              )
                            INNER JOIN item_reporte_unificado AS iru2 ON iru2.id_item = pc2.id_item
                  WHERE     vcp2.Fecha LIKE @Periodo
                            AND mcd2.id_tipo_modalidad IN ( 8, 6, 17 )
                            AND iru2.id_item = iru.id_item
                            AND ( mcd2.id_tipo_modalidad = vcp2.id_tipo_modalidad
                                  OR vcp2.id_tipo_modalidad IS NULL
                                )
                ) AS Variable ,
                ( SELECT    SUM(CASE WHEN iru2.id_UMB = 3
                                     THEN vcp2.Numero_Retiros
                                     ELSE vcp2.Peso
                                END)
                  FROM      modelo_cobro AS mc2
                            INNER JOIN modelo_cobro_detalle AS mcd2 ON mc2.id_modelo = mcd2.id_modelo_cobro
                            INNER JOIN puntos_servicio AS pc2 ON pc2.id_punto_servicio = mc2.id_punto_servicio
                            INNER JOIN V_CALCULO_PUNTO_2 AS vcp2 ON vcp2.idpunto_servicio = pc2.id_punto_servicio
                                                              AND ( vcp2.id_tipo_modalidad IS NULL
                                                              OR vcp2.id_tipo_modalidad <> 55
                                                              )
                            INNER JOIN item_reporte_unificado AS iru2 ON iru2.id_item = pc2.id_item
                  WHERE     vcp2.Fecha LIKE @Periodo
                            AND mcd2.id_tipo_modalidad IN ( 8, 6, 17 )
                            AND iru2.id_item = iru.id_item
                            AND ( mcd2.id_tipo_modalidad = vcp2.id_tipo_modalidad
                                  OR vcp2.id_tipo_modalidad IS NULL
                                )
                ) AS CantidadVariable
        FROM    item_reporte_unificado AS iru
                INNER JOIN umb ON iru.id_UMB = umb.id_umb
        WHERE   iru.id_cliente = @IdCliente
        ORDER BY iru.orden_item;
    END;


