CREATE TABLE dbo.item_reporte_unificado
	(
	id_item int NOT NULL IDENTITY (1, 1),
	orden_item int NOT NULL,
	nombre_item varchar(100) NOT NULL,
	id_UMB int NOT NULL,
	id_cliente int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.item_reporte_unificado ADD CONSTRAINT
	PK_item_reporte_unificado PRIMARY KEY CLUSTERED 
	(
	id_item
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.item_reporte_unificado ADD CONSTRAINT
	FK_item_reporte_unificado_clientes FOREIGN KEY
	(
	id_cliente
	) REFERENCES dbo.clientes
	(
	id_cliente
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
GO
ALTER TABLE dbo.item_reporte_unificado ADD CONSTRAINT
	FK_item_reporte_unificado_umb FOREIGN KEY
	(
	id_UMB
	) REFERENCES dbo.umb
	(
	id_umb
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO