ALTER TABLE dbo.puntos_servicio ADD
	id_item int NULL
GO
ALTER TABLE dbo.puntos_servicio ADD CONSTRAINT
	FK_puntos_servicio_item_reporte_unificado FOREIGN KEY
	(
	id_item
	) REFERENCES dbo.item_reporte_unificado
	(
	id_item
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.puntos_servicio SET (LOCK_ESCALATION = TABLE)
GO