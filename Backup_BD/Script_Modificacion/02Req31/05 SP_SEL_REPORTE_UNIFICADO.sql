USE [RS_SGS_ARAUCO]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_REPORTE_UNIFICADO]    Script Date: 16-12-2016 10:58:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Intellego,José Joaquín Sepúlveda>
-- Create date: <06/12/2016>
-- Update date: <06/12/2016>
-- Description:	<Generación de procedimiento almacenado para recuperación de reporte unificado.>
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_REPORTE_UNIFICADO] 
	-- Add the parameters for the stored procedure here
	@Periodo Varchar(6) = NULL -- fecha comienzo
	, @IdCliente INT = NULL
AS
BEGIN
	SELECT	iru.nombre_item
			, iru.orden_item
			, umb.nombre
			, (SELECT	SUM(mcd2.valor) as totalFijo
				FROM		modelo_cobro AS mc2 
				INNER JOIN	modelo_cobro_detalle AS mcd2 
					ON mc2.id_modelo = mcd2.id_modelo_cobro
				INNER JOIN	puntos_servicio AS pc2
					ON pc2.id_punto_servicio = mc2.id_punto_servicio 
				INNER JOIN	V_CALCULO_PUNTO AS vcp2 
					ON vcp2.id_punto_servicio = pc2.id_punto_servicio 
				INNER JOIN	item_reporte_unificado AS iru2 
					ON iru2.id_item = pc2.id_item 
				INNER JOIN	umb as umb2 ON iru2.id_UMB = umb2.id_umb
				WHERE		vcp2.Fecha LIKE @Periodo 
						AND iru2.id_cliente = iru.id_cliente
						and mcd2.id_tipo_modalidad in (5,7,9,10,11,12)
						and iru2.id_item = iru.id_item) as totalFijo
			, SUM(mcd.valor * vcp.Numero_Retiros) AS Variable
			, SUM(vcp.Numero_Retiros) AS CantidadVariable
	FROM		modelo_cobro AS mc 
	INNER JOIN	modelo_cobro_detalle AS mcd 
		ON mc.id_modelo = mcd.id_modelo_cobro
	INNER JOIN	puntos_servicio AS pc
		ON pc.id_punto_servicio = mc.id_punto_servicio 
	INNER JOIN	V_CALCULO_PUNTO AS vcp 
		ON vcp.id_punto_servicio = pc.id_punto_servicio 
	INNER JOIN	item_reporte_unificado AS iru 
		ON iru.id_item = pc.id_item 
	INNER JOIN	umb ON iru.id_UMB = umb.id_umb
	WHERE		vcp.Fecha LIKE @Periodo 
			AND iru.id_cliente = @IdCliente
			and mcd.id_tipo_modalidad in (8,6)
	group by iru.nombre_item
			, iru.orden_item
			, umb.nombre
			, iru.id_cliente
			, iru.id_item
END
