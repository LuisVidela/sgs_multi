	SELECT DISTINCT
		   CONT.id_cliente
	     , CONT.id_contrato
		 , SUCU.sucursal
		 , TMOD.tipo_modalidad
		 , PUNT.nombre_punto
		 , HIST.HMCD_FECHA FechaReajuste
		 , HIST.HMCD_REAJUSTE ValorReajuste
		 , HIST.HMCD_VALOR ValorAnterior
		 , MODD.valor
	  FROM contratos					CONT with(nolock)
INNER JOIN sucursales					SUCU with(nolock) ON SUCU.id_cliente				   = CONT.id_cliente
INNER JOIN puntos_servicio				PUNT with(nolock) ON PUNT.id_contrato				   = CONT.id_contrato
INNER JOIN modelo_cobro					MODE with(nolock) ON PUNT.id_punto_servicio			   = MODE.id_punto_servicio
INNER JOIN modelo_cobro_detalle			MODD with(nolock) ON MODE.id_modelo					   = MODD.id_modelo_cobro
INNER JOIN tipo_modalidad				TMOD with(nolock) ON TMOD.id_tipo_modalidad			   = MODD.id_tipo_modalidad
INNER JOIN TB_HIS_MODELO_COBRO_DETALLE	HIST with(nolock) ON HIST.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro
INNER JOIN TB_SGS_REAJUSTE				REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO			   = CONT.id_contrato
INNER JOIN TB_SGS_REAJUSTE_DETALLE		REDE with(nolock) ON REDE.READ_REAJ_CODIGO			   = REAJ.REAJ_CODIGO
  ORDER BY id_contrato
