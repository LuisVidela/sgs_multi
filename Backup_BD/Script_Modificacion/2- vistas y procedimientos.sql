
/****** Object:  StoredProcedure [dbo].[SP_UPD_ReversarReajuste]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_UPD_ReversarReajuste]
GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_Reajeste]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_UPD_Reajeste]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_VerificarReajeste]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_VerificarReajeste]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_UltimoReajustePtoServicio]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_UltimoReajustePtoServicio]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_ListaServicio]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_ListaServicio]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_HistorialReajuste]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_HistorialReajuste]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_HistorialPeriodo]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_HistorialPeriodo]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_GuiaDespacho]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_GuiaDespacho]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_DisposicionTemporalDisponible]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_DisposicionTemporalDisponible]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_DetalleGuia]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[SP_SEL_DetalleGuia]
GO
/****** Object:  StoredProcedure [dbo].[sp_reversa_cobro_periodo]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_reversa_cobro_periodo]
GO
/****** Object:  StoredProcedure [dbo].[sp_modelo_cobro_crear]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_modelo_cobro_crear]
GO
/****** Object:  StoredProcedure [dbo].[sp_log_calculo_cobro]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_log_calculo_cobro]
GO
/****** Object:  StoredProcedure [dbo].[sp_listar_cobroclientes_franquiciados_tabla]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_listar_cobroclientes_franquiciados_tabla]
GO
/****** Object:  StoredProcedure [dbo].[sp_franquiciado_detalle]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_franquiciado_detalle]
GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_empresas_periodo]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_cobro_empresas_periodo]
GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_clientes_periodo1]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_cobro_clientes_periodo1]
GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_clientes_periodo]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_cobro_clientes_periodo]
GO
/****** Object:  StoredProcedure [dbo].[sp_clientes_escalables]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_clientes_escalables]
GO
/****** Object:  StoredProcedure [dbo].[sp_calculo_cobro_empresas]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_calculo_cobro_empresas]
GO
/****** Object:  StoredProcedure [dbo].[sp_calculo_cobro_clientes]    Script Date: 24-08-2015 16:24:39 ******/
DROP PROCEDURE [dbo].[sp_calculo_cobro_clientes]
GO
/****** Object:  View [dbo].[V_VALOR_POR_RETIRO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VALOR_POR_RETIRO]
GO
/****** Object:  View [dbo].[V_TIPO_MODALIDAD]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_TIPO_MODALIDAD]
GO
/****** Object:  View [dbo].[V_PUNTOS_UNICOS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_UNICOS]
GO
/****** Object:  View [dbo].[V_PUNTOS_PAGO_PESO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_PAGO_PESO]
GO
/****** Object:  View [dbo].[V_PUNTOS_HOOK_CALCULADOS1]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_HOOK_CALCULADOS1]
GO
/****** Object:  View [dbo].[V_PUNTOS_HOOK_CALCULADOS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_HOOK_CALCULADOS]
GO
/****** Object:  View [dbo].[V_PUNTOS_HOJA_RUTA_ESTADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_HOJA_RUTA_ESTADO]
GO
/****** Object:  View [dbo].[V_PUNTOS_FRONT_LOADER]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTOS_FRONT_LOADER]
GO
/****** Object:  View [dbo].[V_PUNTO_VERTEDERO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_VERTEDERO]
GO
/****** Object:  View [dbo].[V_PUNTO_TARIFA]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_TARIFA]
GO
/****** Object:  View [dbo].[V_PUNTO_SUCURSAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_SUCURSAL]
GO
/****** Object:  View [dbo].[V_PUNTO_SERVICIO_DIRE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_SERVICIO_DIRE]
GO
/****** Object:  View [dbo].[V_PUNTO_SERVICIO_DESCRIPCION]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_SERVICIO_DESCRIPCION]
GO
/****** Object:  View [dbo].[V_PUNTO_NUM_EQUIPOS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_NUM_EQUIPOS]
GO
/****** Object:  View [dbo].[V_PUNTO_HOOK]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_HOOK]
GO
/****** Object:  View [dbo].[V_PUNTO_FRONT_LOADER]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_FRONT_LOADER]
GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL1]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_VALOR_FIJO_MENSUAL1]
GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_VALOR_FIJO_MENSUAL_CERRADO]
GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_VALOR_FIJO_MENSUAL]
GO
/****** Object:  View [dbo].[V_PTO_SERVICIO_MODALIDAD]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_SERVICIO_MODALIDAD]
GO
/****** Object:  View [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]
GO
/****** Object:  View [dbo].[V_PTO_FIJO_MENSUAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_FIJO_MENSUAL]
GO
/****** Object:  View [dbo].[V_PTO_CAJA_RECOLECTORA3]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_CAJA_RECOLECTORA3]
GO
/****** Object:  View [dbo].[V_PTO_CAJA_RECOLECTORA]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PTO_CAJA_RECOLECTORA]
GO
/****** Object:  View [dbo].[V_PERIODO_COBRO_POR_EMPRESA2]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PERIODO_COBRO_POR_EMPRESA2]
GO
/****** Object:  View [dbo].[V_PERIODO_COBRO_POR_EMPRESA]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PERIODO_COBRO_POR_EMPRESA]
GO
/****** Object:  View [dbo].[V_listar_valor_tonelada]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_listar_valor_tonelada]
GO
/****** Object:  View [dbo].[V_listado_CobrosCli_fran]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_listado_CobrosCli_fran]
GO
/****** Object:  View [dbo].[V_HOJA_RUTA_FRANQUICIADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_HOJA_RUTA_FRANQUICIADO]
GO
/****** Object:  View [dbo].[V_EQUIPO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_EQUIPO]
GO
/****** Object:  View [dbo].[V_DATA_CALCULOS_FULL1]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_DATA_CALCULOS_FULL1]
GO
/****** Object:  View [dbo].[V_DATA_CALCULOS_FULL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_DATA_CALCULOS_FULL]
GO
/****** Object:  View [dbo].[V_CONTRATO_PUNTO_SERVICIO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_PUNTO_SERVICIO]
GO
/****** Object:  View [dbo].[V_CONDUCTOR_CAMION]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONDUCTOR_CAMION]
GO
/****** Object:  View [dbo].[V_Comprobacion_Reajuste]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_Comprobacion_Reajuste]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_v2]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_PUNTO_v2]
GO
/****** Object:  View [dbo].[V_ARRIENDO_COSTO_FIJO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_ARRIENDO_COSTO_FIJO]
GO
/****** Object:  View [dbo].[HOJA_RUTA_REALIZADAS_ENE2015]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[HOJA_RUTA_REALIZADAS_ENE2015]
GO
/****** Object:  View [dbo].[V_COBRO_CLIENTES_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_COBRO_CLIENTES_TOTAL]
GO
/****** Object:  View [dbo].[V_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_COBRO_CLIENTES]
GO
/****** Object:  View [dbo].[V_CALCULO_PUNTO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_PUNTO_CERRADO]
GO
/****** Object:  View [dbo].[V_CONTRATO_CERRADO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_CERRADO_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]
GO
/****** Object:  View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]
GO
/****** Object:  View [dbo].[V_FRANQUICIADOS_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_FRANQUICIADOS_ESCALABLES]
GO
/****** Object:  View [dbo].[V_PUNTO_TARIFA_ESCALABLE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_TARIFA_ESCALABLE]
GO
/****** Object:  View [dbo].[V_CLIENTES_CONTRATOS_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_CONTRATOS_ESCALABLES]
GO
/****** Object:  View [dbo].[V_CLIENTES_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_ESCALABLES]
GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_ESCALABLE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTE_COBRO_ESCALABLE]
GO
/****** Object:  View [dbo].[V_NUMERO_SERVICOS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_NUMERO_SERVICOS]
GO
/****** Object:  View [dbo].[V_CALCULO_PUNTO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_PUNTO]
GO
/****** Object:  View [dbo].[V_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_PUNTO_SIN_DISPOCICION]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_PUNTO_SIN_DISPOCICION]
GO
/****** Object:  View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_PUNTOS_CLIENTES]
GO
/****** Object:  View [dbo].[V_DISPOSICION]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_DISPOSICION]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_PUNTO]
GO
/****** Object:  View [dbo].[V_DISPOSICION_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_DISPOSICION_CERRADO]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_CLIENTES_CERRADO]
GO
/****** Object:  View [dbo].[V_CLIENTES_CERRADOS_NO_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_CERRADOS_NO_CONTRATO_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_CLIENTE_DISPOSICION_TOTAL_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTE_DISPOSICION_TOTAL_CLOSE]
GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE]
GO
/****** Object:  View [dbo].[V_VOLTEOS_COBRO_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_COBRO_CLIENTES_CLOSE]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CONTRATO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_CONTRATO]
GO
/****** Object:  View [dbo].[V_CONTRATO_DISPOSICION_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_DISPOSICION_TOTAL]
GO
/****** Object:  View [dbo].[V_CLIENTES_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_CONTRATO_SIN_DIS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_SIN_DIS]
GO
/****** Object:  View [dbo].[V_VOLTEOS_CONTRATO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_CONTRATO_CLIENTES]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CONTRATO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_CONTRATO_CERRADO]
GO
/****** Object:  View [dbo].[V_CONTRATO_DISPOSICION_TOTAL_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_DISPOSICION_TOTAL_CLOSE]
GO
/****** Object:  View [dbo].[V_CLIENTES_CERRADOS_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_CERRADOS_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_CONTRATO_SIN_DIS_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CONTRATO_SIN_DIS_CLOSE]
GO
/****** Object:  View [dbo].[V_VOLTEOS_CONTRATO_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_CONTRATO_CLIENTES_CLOSE]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES_unidad]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_CLIENTES_unidad]
GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CALCULO_COBRO_CLIENTES]
GO
/****** Object:  View [dbo].[V_CLIENTES_NO_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTES_NO_CONTRATO_NO_RETIROS]
GO
/****** Object:  View [dbo].[V_CLIENTE_DISPOSICION_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTE_DISPOSICION_TOTAL]
GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION]
GO
/****** Object:  View [dbo].[V_VOLTEOS_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
DROP VIEW [dbo].[V_VOLTEOS_COBRO_CLIENTES]
GO
/****** Object:  View [dbo].[V_VOLTEOS_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_VOLTEOS_COBRO_CLIENTES]
AS
SELECT        cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Sum(cantidad) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha AS Fecha, Monthis AS Monthis, 
                         Yearis AS Yearis, Sistema, id_unidad
FROM            calculo_cobro_clientes cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, Sistema, id_unidad
HAVING        Sistema = 'Front Loader'
UNION ALL
SELECT        cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Count(id_punto_servicio) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha AS Fecha, Monthis AS Monthis, 
                         Yearis AS Yearis, Sistema, id_unidad
FROM            calculo_cobro_clientes cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, Sistema, id_unidad
HAVING        Sistema <> 'Front Loader'






GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION]
AS
SELECT        id_cliente, rut_cliente, razon_social, nombre_fantasia, SUM(Numero_Retiros) AS Numero_Retiros, SUM(BRUTO) AS Valor, 0 AS IVA, 0 AS NETO, SUM(Peso) AS Peso, Fecha, Monthis, Yearis, id_unidad
FROM            dbo.V_VOLTEOS_COBRO_CLIENTES
GROUP BY id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, id_unidad






GO
/****** Object:  View [dbo].[V_CLIENTE_DISPOSICION_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CLIENTE_DISPOSICION_TOTAL]
As
Select  id_cliente,
		rut_cliente,
		Sum(Valor) As ValorTotalDisposicion
   From calculo_cobro_disposicion
   Group by id_cliente,
		    rut_cliente










GO
/****** Object:  View [dbo].[V_CLIENTES_NO_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CLIENTES_NO_CONTRATO_NO_RETIROS]
As
Select  id_cliente,
		Fecha,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From calculo_cobro_clientes
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 Fecha










GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CALCULO_COBRO_CLIENTES]
AS
SELECT        0 AS id_pago, sindis.id_cliente, sindis.rut_cliente, sindis.razon_social, sindis.nombre_fantasia, sindis.Numero_Retiros - ISNULL(nore.Numero_No_Retiros, 0) AS Numero_Retiros, sindis.Valor, sindis.IVA, 
                         sindis.NETO, sindis.Peso, sindis.Fecha, sindis.Monthis, sindis.Yearis, ISNULL(condis.ValorTotalDisposicion, 0) AS ValorTotalDisposicion, ISNULL(sindis.Valor, 0) + ISNULL(condis.ValorTotalDisposicion, 0) 
                         AS ValorTotal, ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros, sindis.id_unidad
FROM            dbo.V_CLIENTE_COBRO_SIN_DISPOSICION AS sindis LEFT OUTER JOIN
                         dbo.V_CLIENTE_DISPOSICION_TOTAL AS condis ON sindis.id_cliente = condis.id_cliente LEFT OUTER JOIN
                         dbo.V_CLIENTES_NO_CONTRATO_NO_RETIROS AS nore ON sindis.id_cliente = nore.id_cliente AND sindis.Fecha = nore.Fecha






GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES_unidad]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CALCULO_COBRO_CLIENTES_unidad]
AS
SELECT        ccc.id_pago, ccc.id_cliente, ccc.rut_cliente, ccc.razon_social, ccc.nombre_fantasia, ccc.Numero_Retiros, ccc.Valor, ccc.IVA, ccc.NETO, ccc.Peso, ccc.Fecha, ccc.Monthis, ccc.Yearis, ccc.ValorTotalDisposicion, 
                         ccc.ValorTotal, ccc.Numero_No_Retiros, ccc.id_unidad
FROM            dbo.V_CALCULO_COBRO_CLIENTES AS ccc INNER JOIN
                         dbo.clientes AS c ON ccc.id_cliente = c.id_cliente






GO
/****** Object:  View [dbo].[V_VOLTEOS_CONTRATO_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_VOLTEOS_CONTRATO_CLIENTES_CLOSE]
AS
SELECT        cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, Sistema, Sum(cantidad) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha, Monthis, 
                         Yearis, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, Sistema, Fecha, Monthis, Yearis, id_unidad
HAVING        Sistema = 'Front Loader'
UNION ALL
SELECT        cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, Sistema, Count(id_punto_servicio) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha, 
                         Monthis, Yearis, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, Sistema, Fecha, Monthis, Yearis, id_unidad
HAVING        Sistema <> 'Front Loader'






GO
/****** Object:  View [dbo].[V_CONTRATO_SIN_DIS_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CONTRATO_SIN_DIS_CLOSE]
AS
SELECT        id_cliente, rut_cliente, id_contrato, razon_social, nombre_fantasia, Sucursal, SUM(Numero_Retiros) AS Numero_Retiros, SUM(BRUTO) AS Valor, 0 AS IVA, 0 AS NETO, SUM(Peso) AS Peso, Fecha, Monthis, Yearis, 
                         id_unidad
FROM            dbo.V_VOLTEOS_CONTRATO_CLIENTES_CLOSE
GROUP BY id_cliente, rut_cliente, id_contrato, razon_social, nombre_fantasia, Sucursal, Fecha, Monthis, Yearis, id_unidad






GO
/****** Object:  View [dbo].[V_CLIENTES_CERRADOS_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_CLIENTES_CERRADOS_NO_RETIROS]
As
Select  id_cliente,
		id_contrato,
		Fecha,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From cobro_clientes_cerrado
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 id_contrato,
				 Fecha










GO
/****** Object:  View [dbo].[V_CONTRATO_DISPOSICION_TOTAL_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CONTRATO_DISPOSICION_TOTAL_CLOSE]
As
Select  id_cliente,
		rut_cliente,
		id_contrato,
		Fecha,
		Sum(Valor) As ValorTotalDisposicion
   From cobro_disposicion_cerrado
   Group by id_cliente,
		    rut_cliente,
			id_contrato,
			Fecha










GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CONTRATO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CALCULO_COBRO_CONTRATO_CERRADO]
AS
SELECT        0 AS id_calculo, csin.id_cliente, csin.rut_cliente, csin.id_contrato, csin.razon_social, csin.nombre_fantasia, csin.Sucursal, csin.Numero_Retiros - ISNULL(nore.Numero_No_Retiros, 0) AS Numero_Retiros, 
                         csin.Valor, csin.IVA, csin.NETO, csin.Peso, csin.Fecha, csin.Monthis, csin.Yearis, ISNULL(ccc.ValorTotalDisposicion, 0) AS ValorTotalDisposicion, ISNULL(csin.Valor, 0) + ISNULL(ccc.ValorTotalDisposicion, 0) 
                         AS ValorTotal, ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros, csin.id_unidad
FROM            dbo.V_CONTRATO_SIN_DIS_CLOSE AS csin LEFT OUTER JOIN
                         dbo.V_CONTRATO_DISPOSICION_TOTAL_CLOSE AS ccc ON csin.id_cliente = ccc.id_cliente AND csin.id_contrato = ccc.id_contrato AND csin.Fecha = ccc.Fecha LEFT OUTER JOIN
                         dbo.V_CLIENTES_CERRADOS_NO_RETIROS AS nore ON csin.id_cliente = nore.id_cliente AND csin.id_contrato = nore.id_contrato AND csin.Fecha = nore.Fecha






GO
/****** Object:  View [dbo].[V_VOLTEOS_CONTRATO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_VOLTEOS_CONTRATO_CLIENTES]
As
Select  id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		Sistema,
	    Sum(cantidad) As Numero_Retiros,
		Sum(BRUTO) As BRUTO,
		0 As IVA,
		0 As NETO,
	    Sum(peso) As Peso,
	    Fecha,
		Monthis,
		Yearis
		From calculo_cobro_clientes
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 Sucursal,
				 Sistema,
				 Fecha,
				 Monthis,
				 Yearis
		Having Sistema = 'Front Loader'
        Union All
Select  id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		Sistema,
	    Count(id_punto_servicio) As Numero_Retiros,
		Sum(BRUTO) As BRUTO,
		0 As IVA,
		0 As NETO,
	    Sum(peso) As Peso,
	    Fecha,
		Monthis,
		Yearis
		From calculo_cobro_clientes
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 Sucursal,
				 Sistema,
				 Fecha,
				 Monthis,
				 Yearis
		Having Sistema <> 'Front Loader'







GO
/****** Object:  View [dbo].[V_CONTRATO_SIN_DIS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CONTRATO_SIN_DIS]
As
Select  id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
	    Sum(Numero_Retiros) As Numero_Retiros,
		Sum(BRUTO) As Valor,
		0 As IVA,
		0 As NETO,
	    Sum(peso) As Peso,
	    Fecha,
		Monthis,
		Yearis
		From V_VOLTEOS_CONTRATO_CLIENTES
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 sucursal,
				 Fecha,
				 Monthis,
				 Yearis







GO
/****** Object:  View [dbo].[V_CLIENTES_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_CLIENTES_NO_RETIROS]
As
Select  id_cliente,
		id_contrato,
		Fecha,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From calculo_cobro_clientes
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 id_contrato,
				 Fecha










GO
/****** Object:  View [dbo].[V_CONTRATO_DISPOSICION_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CONTRATO_DISPOSICION_TOTAL]
As
Select  id_cliente,
		rut_cliente,
		id_contrato,
		Sum(Valor) As ValorTotalDisposicion
   From calculo_cobro_disposicion
   Group by id_cliente,
		    rut_cliente,
			id_contrato










GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CONTRATO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CALCULO_COBRO_CONTRATO]
As
Select  0 As id_calculo,
		csin.id_cliente,
		csin.rut_cliente,
		csin.id_contrato,
		csin.razon_social,
		csin.nombre_fantasia,
		csin.Sucursal,
	    (csin.Numero_Retiros - Isnull(nore.Numero_No_Retiros,0)) As Numero_Retiros,
		csin.Valor,
		csin.IVA,
		csin.NETO,
	    csin.Peso,
	    csin.Fecha,
		csin.Monthis,
		csin.Yearis,
		Isnull(ccc.ValorTotalDisposicion,0) As ValorTotalDisposicion,
		(Isnull(csin.Valor,0) + Isnull(ccc.ValorTotalDisposicion,0)) As ValorTotal,
		Isnull(nore.Numero_No_Retiros,0) As Numero_No_Retiros
		From V_CONTRATO_SIN_DIS csin
		Left join V_CONTRATO_DISPOSICION_TOTAL ccc
		On csin.id_cliente = ccc.id_cliente
		And csin.id_contrato = ccc.id_contrato
		Left join V_CLIENTES_NO_RETIROS nore
		On csin.id_cliente = nore.id_cliente
		And csin.id_contrato = nore.id_contrato 
		And csin.Fecha = nore.Fecha















GO
/****** Object:  View [dbo].[V_VOLTEOS_COBRO_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_VOLTEOS_COBRO_CLIENTES_CLOSE]
AS
SELECT        cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Sum(cantidad) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha AS Fecha, Monthis AS Monthis, 
                         Yearis AS Yearis, Sistema, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, Sistema, id_unidad
HAVING        Sistema = 'Front Loader'
UNION ALL
SELECT        cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Count(id_punto_servicio) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) AS Peso, Fecha AS Fecha, Monthis AS Monthis, 
                         Yearis AS Yearis, Sistema, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, Sistema, id_unidad
HAVING        Sistema <> 'Front Loader'






GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE]
AS
SELECT        id_cliente, rut_cliente, razon_social, nombre_fantasia, SUM(Numero_Retiros) AS Numero_Retiros, SUM(BRUTO) AS Valor, 0 AS IVA, 0 AS NETO, SUM(Peso) AS Peso, Fecha, Monthis, Yearis, id_unidad
FROM            dbo.V_VOLTEOS_COBRO_CLIENTES_CLOSE
GROUP BY id_cliente, rut_cliente, razon_social, nombre_fantasia, Fecha, Monthis, Yearis, id_unidad






GO
/****** Object:  View [dbo].[V_CLIENTE_DISPOSICION_TOTAL_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CLIENTE_DISPOSICION_TOTAL_CLOSE]
As
Select  id_cliente,
		rut_cliente,
		Fecha,
		Sum(Valor) As ValorTotalDisposicion
   From cobro_disposicion_cerrado
   Group by id_cliente,
		    rut_cliente,
		    Fecha










GO
/****** Object:  View [dbo].[V_CLIENTES_CERRADOS_NO_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_CLIENTES_CERRADOS_NO_CONTRATO_NO_RETIROS]
As
Select  id_cliente,
		Fecha,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From cobro_clientes_cerrado
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 Fecha










GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CALCULO_COBRO_CLIENTES_CERRADO]
AS
SELECT        0 AS id_pago, sindis.id_cliente, sindis.rut_cliente, sindis.razon_social, sindis.nombre_fantasia, sindis.Numero_Retiros - ISNULL(nore.Numero_No_Retiros, 0) AS Numero_Retiros, ISNULL(sindis.Valor, 0) 
                         AS Valor, ISNULL(sindis.IVA, 0) AS IVA, ISNULL(sindis.NETO, 0) AS NETO, ISNULL(sindis.Peso, 0) AS Peso, sindis.Fecha, sindis.Monthis, sindis.Yearis, ISNULL(condis.ValorTotalDisposicion, 0) 
                         AS ValorTotalDisposicion, ISNULL(sindis.Valor + condis.ValorTotalDisposicion, 0) AS ValorTotal, ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros, sindis.id_unidad
FROM            dbo.V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE AS sindis LEFT OUTER JOIN
                         dbo.V_CLIENTE_DISPOSICION_TOTAL_CLOSE AS condis ON sindis.id_cliente = condis.id_cliente AND sindis.Fecha = condis.Fecha LEFT OUTER JOIN
                         dbo.V_CLIENTES_CERRADOS_NO_CONTRATO_NO_RETIROS AS nore ON sindis.id_cliente = nore.id_cliente AND sindis.Fecha = nore.Fecha






GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]
As
Select  0 As id_calculo,
		id_hoja_ruta,
		id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		Convert(Varchar,Isnull(fechaFull,'01/01/1945'),103) As fechaFull,
		id_punto_servicio,
		nombre_punto,
		0 As Numero_Retiros,
		BRUTO As Valor,
		0 As IVA,
		0 As NETO,
	    peso As Peso,
		cantidad AS Volteos,		
	    Fecha As Fecha,
		Monthis as Monthis,
		Yearis aS Yearis,
		Orden,
		tipo_modalidad,
		sorting,
		glosa,
		cobro_arriendo,
		conductor,
		camion,
		como_se_calcula
		From cobro_clientes_cerrado












GO
/****** Object:  View [dbo].[V_DISPOSICION_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[V_DISPOSICION_CERRADO]
As
Select  0 As id_dispo,
		cal.id_punto_servicio,
		cal.Fecha,
		cal.id_cliente,
		glosa,
	    Sum(Isnull(cal.Peso,0)) As Disposicion,
		0 As Valor				
		From V_CALCULO_COBRO_PUNTO_CERRADO cal
		Group By cal.id_cliente,
				 cal.Fecha,
				 cal.id_punto_servicio,
				 glosa
	    Having glosa = 'Retiro'
		And Sum(Isnull(cal.Peso,0)) > 0











GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CALCULO_COBRO_PUNTO]
As
Select  0 As id_calculo,
		id_hoja_ruta,
		id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		Convert(Varchar,Isnull(fechaFull,'01/01/1945'),103) As fechaFull,
		id_punto_servicio,
		nombre_punto,
		0 As Numero_Retiros,
		BRUTO As Valor,
		0 As IVA,
		0 As NETO,
	    peso As Peso,
		cantidad AS Volteos,		
	    Fecha As Fecha,
		Monthis as Monthis,
		Yearis aS Yearis,
		Orden,
		tipo_modalidad,
		sorting,
		glosa,
		cobro_arriendo,
		conductor,
		camion,
		como_se_calcula
		From calculo_cobro_clientes










GO
/****** Object:  View [dbo].[V_DISPOSICION]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_DISPOSICION]
As
Select  0 As id_dispo,
		cal.id_punto_servicio,
		cal.Fecha,
		cal.id_cliente,
		glosa,
	    Sum(Isnull(cal.Peso,0)) As Disposicion,
		0 As Valor				
		From V_CALCULO_COBRO_PUNTO cal
		Group By cal.id_cliente,
				 cal.Fecha,
				 cal.id_punto_servicio,
				 glosa
	    Having glosa = 'Retiro'
		And Sum(Isnull(cal.Peso,0)) > 0










GO
/****** Object:  View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES]
As
Select  Id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		id_punto_servicio,
		nombre_punto,
		Sum(cantidad) As Numero_Retiros,
		Sum(BRUTO) As BRUTO,
		0 As IVA,
		0 As NETO,
	    Sum(peso) As Peso,
	    Fecha As Fecha,
		Monthis as Monthis,
		Yearis aS Yearis,
		id_sucursal,
		Sistema
		From calculo_cobro_clientes
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 sucursal,
				 id_punto_servicio,
				 nombre_punto,
				 Fecha,
				 Monthis,
				 Yearis,
				 id_sucursal,
				 Sistema
	   Having Sistema = 'Front Loader'
Union All
        Select  Id_cliente,
		rut_cliente,
		id_contrato,
		razon_social,
		nombre_fantasia,
		Sucursal,
		id_punto_servicio,
		nombre_punto,
		Count(id_punto_servicio) As Numero_Retiros,
		Sum(BRUTO) As BRUTO,
		0 As IVA,
		0 As NETO,
	    Sum(peso) As Peso,
	    Fecha As Fecha,
		Monthis as Monthis,
		Yearis aS Yearis,
		id_sucursal,
		Sistema
		From calculo_cobro_clientes
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 sucursal,
				 id_punto_servicio,
				 nombre_punto,
				 Fecha,
				 Monthis,
				 Yearis,
				 id_sucursal,
				 Sistema
	   Having Sistema <> 'Front Loader'







GO
/****** Object:  View [dbo].[V_PUNTO_SIN_DISPOCICION]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_PUNTO_SIN_DISPOCICION]
As
        Select  Id_cliente,
				rut_cliente,
				id_contrato,
				razon_social,
				nombre_fantasia,
				Sucursal,
				id_punto_servicio,
				nombre_punto,
				Sum(Numero_Retiros) As Numero_Retiros,
				Sum(BRUTO) As Valor,
				0 As IVA,
				0 As NETO,
				Sum(peso) As Peso,
				Fecha As Fecha,
				Monthis as Monthis,
				Yearis aS Yearis,
				id_sucursal
		From V_VOLTEOS_PUNTOS_CLIENTES
		Group By id_cliente,
				 rut_cliente,
				 id_contrato,
				 razon_social,
				 nombre_fantasia,
				 sucursal,
				 id_punto_servicio,
				 nombre_punto,
				 Fecha,
				 Monthis,
				 Yearis,
				 id_sucursal







GO
/****** Object:  View [dbo].[V_CONTRATO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create View [dbo].[V_CONTRATO_NO_RETIROS]
As
Select  id_cliente,
		Fecha,
		id_contrato,
		id_punto_servicio,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From calculo_cobro_clientes
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 Fecha,
				 id_contrato,
				 id_punto_servicio













GO
/****** Object:  View [dbo].[V_CALCULO_PUNTO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CALCULO_PUNTO]
As
Select  0 As id_calculo,
		vpt.id_cliente,
		vpt.rut_cliente,
		vpt.id_contrato,
		vpt.razon_social,
		vpt.nombre_fantasia,
		vpt.Sucursal,
		vpt.id_punto_servicio,
		vpt.nombre_punto,
		(vpt.Numero_Retiros - IsNull(nore.Numero_No_Retiros,0)) As Numero_Retiros,
		vpt.Valor,
		vpt.IVA,
		vpt.NETO,
	    vpt.Peso,
	    vpt.Fecha,
		vpt.Monthis,
		vpt.Yearis,
		vpt.id_sucursal,
		Isnull(cobr.Valor,0) As ValorTotalDisposicion,
		(Isnull(vpt.Valor,0) + Isnull(cobr.Valor,0)) As ValorTotal,
		Isnull(nore.Numero_No_Retiros,0) As Numero_No_Retiros
		From V_PUNTO_SIN_DISPOCICION vpt
		Left Join calculo_cobro_disposicion cobr
		On vpt.id_punto_servicio = cobr.id_punto_servicio
		Left join V_CONTRATO_NO_RETIROS nore
		On vpt.id_cliente = nore.id_cliente
		And vpt.id_contrato = nore.id_contrato
		And vpt.id_punto_servicio = nore.id_punto_servicio  
		And vpt.Fecha = nore.Fecha















GO
/****** Object:  View [dbo].[V_NUMERO_SERVICOS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_NUMERO_SERVICOS]
As
Select  0 As id_calculo,
		id_cliente,
		rut_cliente,
		Fecha,
		Sucursal,
		Count(id_punto_servicio) As Numero_Servicios
		From V_CALCULO_PUNTO
				Group By id_calculo,
				 id_cliente,
				 rut_cliente,
				 Sucursal,
				 Fecha










GO
/****** Object:  View [dbo].[V_CLIENTE_COBRO_ESCALABLE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[V_CLIENTE_COBRO_ESCALABLE]
As
Select Distinct pto.id_punto_servicio
	From cobro_cliente_escalable pto
	Where pto.activo = 1










GO
/****** Object:  View [dbo].[V_CLIENTES_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[V_CLIENTES_ESCALABLES]
As
Select cal.id_contrato,
	   glosa,
       count(cal.id_punto_servicio) As Veces
	From calculo_cobro_clientes cal
	Inner Join V_CLIENTE_COBRO_ESCALABLE esca
	On cal.id_punto_servicio = esca.id_punto_servicio
	Group By cal.id_contrato,
			 glosa
    Having glosa = 'Retiro'







GO
/****** Object:  View [dbo].[V_CLIENTES_CONTRATOS_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create View [dbo].[V_CLIENTES_CONTRATOS_ESCALABLES]
As
Select pto.id_punto_servicio,
       escx.id_contrato,
       escx.veces
		FRom puntos_servicio pto
		Inner join V_CLIENTES_ESCALABLES escx
		On pto.id_contrato = escx.id_contrato











GO
/****** Object:  View [dbo].[V_PUNTO_TARIFA_ESCALABLE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_PUNTO_TARIFA_ESCALABLE]
As
Select pto.id_punto_servicio,
       pto.id_tipo_tarifa,
	   tar.valor_tarifa
	From puntos_servicio pto
	Inner Join tipo_tarifa tar
	On pto.id_tipo_tarifa = tar.id_tipo_tarifa
	And tar.tarifa_escalable = 1







GO
/****** Object:  View [dbo].[V_FRANQUICIADOS_ESCALABLES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_FRANQUICIADOS_ESCALABLES]
As
Select cal.id_punto_servicio,
       cal.franquiciado,
	   glosa,
	   sistema,
	   esca.id_tipo_tarifa,
       count(cal.id_punto_servicio) As Veces
	From calculo_cobro_clientes cal
	Inner join V_PUNTO_TARIFA_ESCALABLE esca
	On cal.id_punto_servicio = esca.id_punto_servicio 
	Group By cal.id_punto_servicio,
             cal.franquiciado,
			 glosa,
			 sistema,
			 esca.id_tipo_tarifa
    Having  glosa = 'Retiro'
	And sistema = 'Hook' 
	And cal.franquiciado is not null







GO
/****** Object:  View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]
AS
SELECT       cli.Id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, id_punto_servicio, nombre_punto, Sum(cantidad) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, Sum(peso) 
                         AS Peso, Fecha AS Fecha, Monthis AS Monthis, Yearis AS Yearis, cli.id_sucursal, Sistema, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, sucursal, id_punto_servicio, nombre_punto, Fecha, Monthis, Yearis, cli.id_sucursal, Sistema, id_unidad
HAVING        Sistema = 'Front Loader'
UNION ALL
SELECT        cli.Id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, Sucursal, id_punto_servicio, nombre_punto, Count(id_punto_servicio) AS Numero_Retiros, Sum(BRUTO) AS BRUTO, 0 AS IVA, 0 AS NETO, 
                         Sum(peso) AS Peso, Fecha AS Fecha, Monthis AS Monthis, Yearis AS Yearis, cli.id_sucursal, Sistema, id_unidad
FROM            cobro_clientes_cerrado cli
inner join contratos ctr on ctr.id_contrato = cli.id_contrato
GROUP BY cli.id_cliente, rut_cliente, cli.id_contrato, razon_social, nombre_fantasia, sucursal, id_punto_servicio, nombre_punto, Fecha, Monthis, Yearis, cli.id_sucursal, Sistema, id_unidad
HAVING        Sistema <> 'Front Loader'






GO
/****** Object:  View [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]
AS
SELECT        Id_cliente, rut_cliente, id_contrato, razon_social, nombre_fantasia, Sucursal, id_punto_servicio, nombre_punto, SUM(Numero_Retiros) AS Numero_Retiros, SUM(BRUTO) AS Valor, 0 AS IVA, 0 AS NETO, 
                         SUM(Peso) AS Peso, Fecha, Monthis, Yearis, id_sucursal, id_unidad
FROM            dbo.V_VOLTEOS_PUNTOS_CLIENTES_CLOSE
GROUP BY Id_cliente, rut_cliente, id_contrato, razon_social, nombre_fantasia, Sucursal, id_punto_servicio, nombre_punto, Fecha, Monthis, Yearis, id_sucursal, id_unidad






GO
/****** Object:  View [dbo].[V_CONTRATO_CERRADO_NO_RETIROS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create View [dbo].[V_CONTRATO_CERRADO_NO_RETIROS]
As
Select  id_cliente,
		Fecha,
		id_contrato,
		id_punto_servicio,
	    Count(id_punto_servicio) As Numero_No_Retiros
		From cobro_clientes_cerrado
		Where glosa <> 'Retiro'
		Group By id_cliente,
				 Fecha,
				 id_contrato,
				 id_punto_servicio













GO
/****** Object:  View [dbo].[V_CALCULO_PUNTO_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CALCULO_PUNTO_CERRADO]
AS
SELECT        0 AS id_calculo, vpt.Id_cliente, vpt.rut_cliente, vpt.id_contrato, vpt.razon_social, vpt.nombre_fantasia, vpt.Sucursal, vpt.id_punto_servicio, vpt.nombre_punto, 
                         vpt.Numero_Retiros - ISNULL(nore.Numero_No_Retiros, 0) AS Numero_Retiros, vpt.Valor, vpt.IVA, vpt.NETO, vpt.Peso, vpt.Fecha, vpt.Monthis, vpt.Yearis, vpt.id_sucursal, ISNULL(cobr.valor, 0) 
                         AS ValorTotalDisposicion, ISNULL(vpt.Valor, 0) + ISNULL(cobr.valor, 0) AS ValorTotal, ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros, vpt.id_unidad
FROM            dbo.V_PUNTO_SIN_DISPOCICION_CLOSE AS vpt LEFT OUTER JOIN
                         dbo.cobro_clientes_cerrado AS cobr ON vpt.id_punto_servicio = cobr.id_punto_servicio AND vpt.Fecha = cobr.Fecha LEFT OUTER JOIN
                         dbo.V_CONTRATO_CERRADO_NO_RETIROS AS nore ON vpt.Id_cliente = nore.id_cliente AND vpt.id_contrato = nore.id_contrato AND vpt.id_punto_servicio = nore.id_punto_servicio AND vpt.Fecha = nore.Fecha






GO
/****** Object:  View [dbo].[V_COBRO_CLIENTES]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[V_COBRO_CLIENTES]
As
Select id_punto_servicio,
	   Fecha,
	   id_cliente,
	   rut_cliente,
	   Sum(Bruto) AS Cobro
 From calculo_cobro_clientes
 Group By id_punto_servicio,
	      Fecha,
	      id_cliente,
	      rut_cliente










GO
/****** Object:  View [dbo].[V_COBRO_CLIENTES_TOTAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_COBRO_CLIENTES_TOTAL]
As
Select cob.id_cliente,
	   cob.rut_cliente,
	   cob.Fecha,
	   cob.id_punto_servicio,
	   cob.Cobro,
	   disp.Valor,
	   (cob.Cobro + disp.Valor) As TotalCliente,
	   (((cob.Cobro + disp.Valor) * 19) / 100) As IVA,
	   ((((cob.Cobro + disp.Valor) * 19) / 100) + (cob.Cobro + disp.Valor)) As TotalClienteMasIVA
 From V_COBRO_CLIENTES cob
 Inner Join calculo_cobro_disposicion disp
 On cob.id_punto_servicio = disp.id_punto_servicio
 And cob.id_cliente = disp.id_cliente











GO
/****** Object:  View [dbo].[HOJA_RUTA_REALIZADAS_ENE2015]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[HOJA_RUTA_REALIZADAS_ENE2015]
As
Select det.id_punto_servicio,
       det.id_hoja_ruta,
       web.fecha,
       web.id_conductor
	From hoja_ruta_detalle det
	inner join hoja_ruta web
	On det.id_hoja_ruta = web.id_hoja_ruta
	And web.estado = 3			---- hoja de ruta cerrada
	And det.estado = 2			---- punto de servicio realizado
	Where web.fecha > '2015-01-01'












GO
/****** Object:  View [dbo].[V_ARRIENDO_COSTO_FIJO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_ARRIENDO_COSTO_FIJO]
As
 Select det.id_detalle_modelo_cobro,
		det.id_modelo_cobro,
		det.id_tipo_modalidad,
		det.valor,
		mode.id_punto_servicio,
		tipo.tipo_modalidad
		from modelo_cobro_detalle det
		Inner join modelo_cobro mode
		On det.id_modelo_cobro = mode.id_modelo
		Left outer join tipo_modalidad tipo
		On det.id_tipo_modalidad = tipo.id_tipo_modalidad
		Where det.id_tipo_modalidad In (7,9,10,11,12)
		And det.valor > 0












GO
/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_v2]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CALCULO_COBRO_PUNTO_v2]
As
Select  0 As id_calculo,
		hr.id_hoja_ruta,
		cc.id_cliente,
		cc.rut_cliente,
		cc.razon_social,
		cc.nombre_fantasia,
		cc.Sucursal,
		Convert(Varchar,Isnull(cc.fechaFull,'01/01/1945'),103) As fechaFull,
		cc.id_punto_servicio,
		cc.nombre_punto,
		0 As Numero_Retiros,
	    cc.peso As Peso,
		cc.cantidad AS Volteos,		
		cc.BRUTO As Valor,
	    cc.Fecha As Fecha,
		cc.Orden,
		cc.tipo_modalidad,
		cc.sorting,
		cc.glosa,
		cc.cobro_arriendo,
		cc.conductor,
		cc.camion,
		cc.como_se_calcula,hr.codigo_externo
		From calculo_cobro_clientes cc
		inner join puntos_servicio ps on cc.id_punto_servicio=ps.id_punto_servicio
  left join hoja_ruta_detalle hr
  on cc.id_hoja_ruta=hr.id_hoja_ruta and hr.Orden=SUBSTRING(convert(varchar,cc.orden),5,100)










GO
/****** Object:  View [dbo].[V_Comprobacion_Reajuste]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_Comprobacion_Reajuste]
AS
SELECT DISTINCT 
                         TOP (100) PERCENT CONT.id_cliente, CONT.id_contrato, SUCU.sucursal, TMOD.tipo_modalidad, PUNT.nombre_punto, HIST.HMCD_FECHA AS FechaReajuste, HIST.HMCD_REAJUSTE AS ValorReajuste, 
                         HIST.HMCD_VALOR AS ValorAnterior, MODD.valor
FROM            dbo.contratos AS CONT WITH (nolock) INNER JOIN
                         dbo.sucursales AS SUCU WITH (nolock) ON SUCU.id_cliente = CONT.id_cliente INNER JOIN
                         dbo.puntos_servicio AS PUNT WITH (nolock) ON PUNT.id_contrato = CONT.id_contrato INNER JOIN
                         dbo.modelo_cobro AS MODE WITH (nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio INNER JOIN
                         dbo.modelo_cobro_detalle AS MODD WITH (nolock) ON MODE.id_modelo = MODD.id_modelo_cobro INNER JOIN
                         dbo.tipo_modalidad AS TMOD WITH (nolock) ON TMOD.id_tipo_modalidad = MODD.id_tipo_modalidad INNER JOIN
                         dbo.TB_HIS_MODELO_COBRO_DETALLE AS HIST WITH (nolock) ON HIST.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro INNER JOIN
                         dbo.TB_SGS_REAJUSTE AS REAJ WITH (nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato INNER JOIN
                         dbo.TB_SGS_REAJUSTE_DETALLE AS REDE WITH (nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
ORDER BY CONT.id_contrato



GO
/****** Object:  View [dbo].[V_CONDUCTOR_CAMION]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_CONDUCTOR_CAMION]
As
Select  hr.id_hoja_ruta,
		hr.id_conductor,
		hr.id_camion,
		con.nombre_conductor,
		con.rut_conductor,
		cam.camion,
		cam.tipo_camion,
		cam.modelo_camion,
		cam.patente
   From hoja_ruta hr
   Inner Join conductores con
   On  hr.id_conductor = con.id_conductor
   Inner Join camiones cam
   On hr.id_camion = cam.id_camion










GO
/****** Object:  View [dbo].[V_CONTRATO_PUNTO_SERVICIO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_CONTRATO_PUNTO_SERVICIO]
AS
SELECT        con.id_contrato, pto.id_punto_servicio, perio.periodo, perio.inicio, perio.fin, con.estado, con.id_unidad
FROM            dbo.contratos AS con INNER JOIN
                         dbo.puntos_servicio AS pto ON con.id_contrato = pto.id_contrato INNER JOIN
                         dbo.calculo_cobro_empresas AS perio ON con.id_contrato = perio.id_contrato
WHERE        (con.estado = 'ACTIVO')






GO
/****** Object:  View [dbo].[V_DATA_CALCULOS_FULL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_DATA_CALCULOS_FULL]
As
Select  0 As idData,
		cal.fechacorta,
		cal.nombre_punto,
		cal.sistema,
		cal.camion,
		cal.franquiciado,
		cal.conductor,
		cal.estadopunto,
		cal.relleno,
		cal.peso As cantidad,
		cal.bruto,
		cal.pagofranquiciado As Pago,
		cal.margen As Margen,
		cal.margenporc As MargenPorc,
		cal.id_hoja_ruta,
		cal.id_punto_servicio,
		cal.fechaFull
   From	calculo_cobro_clientes cal
   Where cal.glosa = 'Retiro'
   And cal.sistema in ('Hook')
   And cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0
   Union All
   Select  0 As idData,
			cal.fechacorta,
			cal.nombre_punto,
			cal.sistema,
			cal.camion,
			cal.franquiciado,
			cal.conductor,
			cal.estadopunto,
			cal.relleno,
			cal.cantidad,
			0 As Cobro,
			0 As Pago,
			0 As Margfen,
			0 As MargenPorc,
			cal.id_hoja_ruta,
			cal.id_punto_servicio,
			cal.fechaFull
   From	calculo_cobro_clientes cal
   Where cal.glosa = 'Retiro'
   And cal.sistema in ('Caja Recolectora','Front Loader')
   And cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0
Union All
   Select   0 As idData,
			cal.fechacorta,
			cal.nombre_punto,
			cal.sistema,
			cal.camion,
			cal.franquiciado,
			cal.conductor,
			cal.estadopunto,
			Null As relleno,
			0 As cantidad,
			0 As Cobro,
			0 As Pago,
			0 As Margfen,
			0 As MargenPorc,
			cal.id_hoja_ruta,
			cal.id_punto_servicio,
			cal.fechaFull
   From	ccc_puntos_no_realizados cal
   Where cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0











GO
/****** Object:  View [dbo].[V_DATA_CALCULOS_FULL1]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_DATA_CALCULOS_FULL1]
As
Select  0 As idData,
		cal.fechacorta,
		cal.nombre_punto,
		cal.sistema,
		cal.camion,
		cal.franquiciado,
		cal.conductor,
		cal.estadopunto,
		cal.relleno,
		cal.peso As cantidad,
		cal.bruto,
		cal.pagofranquiciado As Pago,
		cal.margen As Margen,
		cal.margenporc As MargenPorc,
		cal.id_hoja_ruta,
		cal.id_punto_servicio,
		cal.fechaFull
   From	calculo_cobro_clientes1 cal
   Where cal.glosa = 'Retiro'
   And cal.sistema in ('Hook')
   And cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0
   Union All
   Select  0 As idData,
			cal.fechacorta,
			cal.nombre_punto,
			cal.sistema,
			cal.camion,
			cal.franquiciado,
			cal.conductor,
			cal.estadopunto,
			cal.relleno,
			cal.cantidad,
			0 As Cobro,
			0 As Pago,
			0 As Margfen,
			0 As MargenPorc,
			cal.id_hoja_ruta,
			cal.id_punto_servicio,
			cal.fechaFull
   From	calculo_cobro_clientes1 cal
   Where cal.glosa = 'Retiro'
   And cal.sistema in ('Caja Recolectora','Front Loader')
   And cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0
Union All
   Select   0 As idData,
			cal.fechacorta,
			cal.nombre_punto,
			cal.sistema,
			cal.camion,
			cal.franquiciado,
			cal.conductor,
			cal.estadopunto,
			Null As relleno,
			0 As cantidad,
			0 As Cobro,
			0 As Pago,
			0 As Margfen,
			0 As MargenPorc,
			cal.id_hoja_ruta,
			cal.id_punto_servicio,
			cal.fechaFull
   From	ccc_puntos_no_realizados1 cal
   Where cal.id_hoja_ruta is not null
   And cal.id_hoja_ruta > 0








GO
/****** Object:  View [dbo].[V_EQUIPO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[V_EQUIPO]
As
SELECT pto.id_punto_servicio,
       pto.id_equipo,
	   equip.tipo_equipo + ' ' + capacidad_equipo As Equipo,
	   nom_nemo
  FROM puntos_servicio pto
  Inner join equipos equip
  On pto.id_equipo = equip.id_equipo










GO
/****** Object:  View [dbo].[V_HOJA_RUTA_FRANQUICIADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_HOJA_RUTA_FRANQUICIADO]
As
 Select rut.id_hoja_ruta,
        rut.id_conductor,
		cond.id_franquiciado,
		fran.razon_social,
		fran.rut_franquiciado
		From hoja_ruta rut
		Inner join conductores cond
		On rut.id_conductor = cond.id_conductor
		Inner Join franquiciado fran
		On cond.id_franquiciado = fran.id_franquiciado








GO
/****** Object:  View [dbo].[V_listado_CobrosCli_fran]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[V_listado_CobrosCli_fran]
As
Select 0 as id_calculo
      ,id_cliente
      ,rut_cliente
      ,razon_social
      ,sum(cantidad) as cantidad
      ,Sum(valor) as valor
      ,Sum(pagofranquiciado) as pagofranquiciado
      ,0 as ValorTotalDisposicion
      ,Sum(ValorTotal) as ValorTotal
      ,(Sum(ValorTotal)-Sum(pagofranquiciado)) as margen
	  ,((Sum(Convert(decimal,ValorTotal))-Sum(Convert(decimal,pagofranquiciado))) / Sum(Convert(decimal,Isnull(ValorTotal,0)))) * 100 as margenporcer
  From dbo.calculo_cobro_clientes_franquiciado
  Group By  id_cliente,
			rut_cliente,
			razon_social
  Having Sum(ValorTotal) > 0










GO
/****** Object:  View [dbo].[V_listar_valor_tonelada]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE view [dbo].[V_listar_valor_tonelada]
as
		select 
		C.id_cliente,
		P.id_punto_servicio,
		P.id_tipo_tarifa,
		T.valor_tarifa 
		from dbo.puntos_servicio P
		inner join dbo.tipo_tarifa  T on T.id_tipo_tarifa =P.id_tipo_tarifa
		inner join dbo.contratos C on C.id_contrato=P.id_contrato
	









GO
/****** Object:  View [dbo].[V_PERIODO_COBRO_POR_EMPRESA]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[V_PERIODO_COBRO_POR_EMPRESA]
As
Select cli.id_cliente,
	   suc.id_sucursal,
	   cont.id_contrato,
	   suc.periodo_desde,
	   suc.periodo_hasta
	From clientes cli
	Inner join sucursales suc
	On cli.id_cliente = suc.id_cliente
	Inner join contratos cont
	On suc.id_sucursal = cont.id_sucursal










GO
/****** Object:  View [dbo].[V_PERIODO_COBRO_POR_EMPRESA2]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[V_PERIODO_COBRO_POR_EMPRESA2]
As
Select cli.id_cliente,
	   suc.id_sucursal,
	   cont.id_contrato,
	   suc.periodo_desde,
	   suc.periodo_hasta
	From clientes cli
	Inner join sucursales suc
	On cli.id_cliente = suc.id_cliente
	Inner join contratos cont
	On suc.id_sucursal = cont.id_sucursal











GO
/****** Object:  View [dbo].[V_PTO_CAJA_RECOLECTORA]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PTO_CAJA_RECOLECTORA]
As
	select mode.id_punto_servicio,
		   deta.valor,
		   deta.id_tipo_modalidad
		from modelo_cobro mode
		Inner join modelo_cobro_detalle deta
		On mode.id_modelo = deta.id_modelo_cobro
		Where deta.id_tipo_modalidad = 5
		And deta.valor > 0








GO
/****** Object:  View [dbo].[V_PTO_CAJA_RECOLECTORA3]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create View [dbo].[V_PTO_CAJA_RECOLECTORA3]
As
Select Distinct punto.id_punto_servicio,
	   cont.id_tipo_contrato
	   From Contratos cont 
	   Inner join puntos_servicio punto
	   On cont.id_contrato = punto.id_contrato
	   Where cont.id_tipo_contrato = 3








GO
/****** Object:  View [dbo].[V_PTO_FIJO_MENSUAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PTO_FIJO_MENSUAL]
As
Select mode.id_punto_servicio,
       mode.id_modelo,
	   deta.valor
	 From modelo_cobro mode
	 Inner Join modelo_cobro_detalle deta
	 On mode.id_modelo = deta.id_modelo_cobro
	 Where deta.id_tipo_modalidad = 5
	 And deta.valor > 0
	 And deta.valor Is Not Null








GO
/****** Object:  View [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PTO_SERVICIO_COBRO_TONELADA]
As
		Select  cob.id_punto_servicio,
				cob.id_modelo,
				deta.valor,
				deta.id_tipo_modalidad,
				0 id_cliente,
				0 id_contrato,
				0 id_sucursal,
				0 id_tipo_contrato
			From modelo_cobro cob
			Inner Join modelo_cobro_detalle deta
			On cob.id_modelo = deta.id_modelo_cobro
			Where cob.id_punto_servicio Is Not Null
			And deta.id_tipo_modalidad = 8
			And deta.valor > 0










GO
/****** Object:  View [dbo].[V_PTO_SERVICIO_MODALIDAD]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PTO_SERVICIO_MODALIDAD]
As
Select Distinct mc.id_modelo,
			    mcd.id_modelo_cobro,
			    mc.id_punto_servicio,
			    mcd.id_tipo_modalidad,
			    moda.tipo_modalidad,
			    mcd.id_detalle_modelo_cobro,
				mcd.valor
	from modelo_cobro mc
	Inner join modelo_cobro_detalle mcd
	On  mc.id_modelo = mcd.id_modelo_cobro
	Inner Join tipo_modalidad moda
	On mcd.id_tipo_modalidad = moda.id_tipo_modalidad
	Where  mc.id_punto_servicio Is Not Null










GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[V_PTO_VALOR_FIJO_MENSUAL]
As
	Select  id_punto_servicio,
			id_tipo_modalidad
	   From calculo_cobro_clientes
	   Where id_tipo_modalidad = 5










GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL_CERRADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PTO_VALOR_FIJO_MENSUAL_CERRADO]
As
	Select  id_punto_servicio,
			id_tipo_modalidad
	   From cobro_clientes_cerrado
	   Where id_tipo_modalidad = 5










GO
/****** Object:  View [dbo].[V_PTO_VALOR_FIJO_MENSUAL1]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PTO_VALOR_FIJO_MENSUAL1]
As
	Select  id_punto_servicio,
			id_tipo_modalidad
	   From calculo_cobro_clientes1
	   Where id_tipo_modalidad = 5







GO
/****** Object:  View [dbo].[V_PUNTO_FRONT_LOADER]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[V_PUNTO_FRONT_LOADER]
As
Select Distinct punto.id_punto_servicio,
	   cont.id_tipo_contrato
	   From Contratos cont 
	   Inner join puntos_servicio punto
	   On cont.id_contrato = punto.id_contrato
	   Where cont.id_tipo_contrato = 1








GO
/****** Object:  View [dbo].[V_PUNTO_HOOK]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View [dbo].[V_PUNTO_HOOK]
As
Select Distinct punto.id_punto_servicio,
	   cont.id_tipo_contrato
	   From Contratos cont 
	   Inner join puntos_servicio punto
	   On cont.id_contrato = punto.id_contrato
	   Where cont.id_tipo_contrato = 2







GO
/****** Object:  View [dbo].[V_PUNTO_NUM_EQUIPOS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_PUNTO_NUM_EQUIPOS]
As
	Select  id_punto_servicio,
			cant_equipos 
			From puntos_servicio
			Where cant_equipos is not null
			and cant_equipos > 0










GO
/****** Object:  View [dbo].[V_PUNTO_SERVICIO_DESCRIPCION]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[V_PUNTO_SERVICIO_DESCRIPCION]
AS
SELECT DISTINCT 
                         pto.id_punto_servicio, cont.id_tipo_contrato AS id_tipo_servicio, REPLACE(REPLACE(REPLACE(CONVERT(varchar, cont.id_tipo_contrato), '1', 'Front Loader'), '2', 'Hook'), '3', 'Caja Recolectora') AS tipo_servicio, 
                         REPLACE(REPLACE(pto.nombre_punto, ' ', ''), '.', '') AS nombre_punto, RTRIM(LTRIM(pto.nombre_punto)) AS nombre_punto1, cont.id_unidad
FROM            dbo.puntos_servicio AS pto INNER JOIN
                         dbo.contratos AS cont ON pto.id_contrato = cont.id_contrato






GO
/****** Object:  View [dbo].[V_PUNTO_SERVICIO_DIRE]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE View  [dbo].[V_PUNTO_SERVICIO_DIRE]
As
Select Distinct con.id_contrato,
			    pto.id_punto_servicio,
			    con.estado,
			    suc.id_sucursal,
			    suc.sucursal,
			    suc.id_ciudad,
			    suc.id_comuna,
			    suc.id_pais,
			    suc.id_region,
			    suc.id_rubro,
			    Isnull(suc.zona,'SIN DATOS') As zona,
				Isnull(ciu.ciudad,'SIN DATOS') As ciudad,
				Isnull(comu.nombre_comuna,'SIN DATOS') As nombre_comuna,
				Isnull(pai.pais,'SIN DATOS') As pais,
				Isnull(rub.rubro,'SIN DATOS') As rubro
			    from contratos con
			    Inner Join puntos_servicio pto
			    On con.id_contrato = pto.id_contrato
			    Inner Join calculo_cobro_empresas perio
			    On con.id_contrato = perio.id_contrato
			    Inner Join sucursales suc
			    On con.id_sucursal = suc.id_sucursal
				inner join ciudades ciu
				On suc.id_ciudad = ciu.id_ciudad
				inner join comunas comu
				On suc.id_comuna = comu.id_comuna
				Inner join paises pai
				On suc.id_pais = pai.id_pais
				Inner join rubros rub
				On suc.id_rubro = rub.id_rubro
			    Where con.estado = 'ACTIVO'










GO
/****** Object:  View [dbo].[V_PUNTO_SUCURSAL]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create View [dbo].[V_PUNTO_SUCURSAL]
As
Select  pto.id_punto_servicio, 
		pto.id_contrato,
		clie.razon_social,
		sucu.sucursal	
		From puntos_servicio pto
		Inner join contratos ctt
		On pto.id_contrato = ctt.id_contrato
		Inner Join clientes clie
		On ctt.id_cliente = clie.id_cliente
		inner join sucursales sucu
		On ctt.id_sucursal = sucu.id_sucursal 
			









GO
/****** Object:  View [dbo].[V_PUNTO_TARIFA]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PUNTO_TARIFA]
As
Select pto.id_punto_servicio,
       pto.id_tipo_tarifa,
	   tar.valor_tarifa
	From puntos_servicio pto
	Inner Join tipo_tarifa tar
	On pto.id_tipo_tarifa = tar.id_tipo_tarifa








GO
/****** Object:  View [dbo].[V_PUNTO_VERTEDERO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PUNTO_VERTEDERO]
As
Select pto.id_punto_servicio,
       ver.vertedero
	   From puntos_servicio pto
	   Inner join vertederos ver
	   On pto.id_vertedero = ver.id_vertedero







GO
/****** Object:  View [dbo].[V_PUNTOS_FRONT_LOADER]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE View [dbo].[V_PUNTOS_FRONT_LOADER]
As
Select xxx.id_punto_servicio,
       xxx.nombre_punto, 
	   xxx.id_contrato,
	   kkk.id_tipo_contrato,
	   xxx.id_tipo_tarifa
  From puntos_servicio xxx
  Inner Join contratos kkk
  on xxx.id_contrato = kkk.id_contrato
  Where kkk.id_tipo_contrato = 1










GO
/****** Object:  View [dbo].[V_PUNTOS_HOJA_RUTA_ESTADO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PUNTOS_HOJA_RUTA_ESTADO]
As
Select  web.fecha As FechaPunto,
        det.id_punto_servicio As PuntoServicio,
        det.estado As EstadoPunto,
		pto.descripcion As Descripcion
		From hoja_ruta_detalle det
		inner join hoja_ruta web
		On det.id_hoja_ruta = web.id_hoja_ruta
		Inner join punto_estados pto
		On det.estado = pto.estado
		And web.estado = 3
		And pto.estado <> 2







GO
/****** Object:  View [dbo].[V_PUNTOS_HOOK_CALCULADOS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PUNTOS_HOOK_CALCULADOS]
As
 Select Distinct id_punto_servicio 
	From calculo_cobro_clientes
	Where peso > 0 







GO
/****** Object:  View [dbo].[V_PUNTOS_HOOK_CALCULADOS1]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_PUNTOS_HOOK_CALCULADOS1]
As
 Select Distinct id_punto_servicio 
	From calculo_cobro_clientes1
	Where peso > 0 










GO
/****** Object:  View [dbo].[V_PUNTOS_PAGO_PESO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create view [dbo].[V_PUNTOS_PAGO_PESO]
As
Select id_punto_servicio 
	From puntos_servicio 
	Where pago_por_peso <> 0







GO
/****** Object:  View [dbo].[V_PUNTOS_UNICOS]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[V_PUNTOS_UNICOS]
As
Select Distinct det.id_punto_servicio,
       web.fecha
	From hoja_ruta_detalle det
	inner join hoja_ruta web
	On det.id_hoja_ruta = web.id_hoja_ruta
	And web.estado = 3			---- hoja de ruta cerrada
	And det.estado = 2			---- punto de servicio realizado












GO
/****** Object:  View [dbo].[V_TIPO_MODALIDAD]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create View [dbo].[V_TIPO_MODALIDAD]
As
Select id_tipo_modalidad,
	   tipo_modalidad
  From tipo_modalidad










GO
/****** Object:  View [dbo].[V_VALOR_POR_RETIRO]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[V_VALOR_POR_RETIRO]
As
Select  mode.id_punto_servicio,
		mode.id_modelo,
		deta.id_tipo_modalidad,
		deta.valor
		from modelo_cobro mode
		Inner Join modelo_cobro_detalle deta
		On mode.id_modelo = deta.id_modelo_cobro
		Where id_tipo_modalidad = 6
		And valor > 0
		And  mode.id_punto_servicio Is Not Null










GO
/****** Object:  StoredProcedure [dbo].[sp_calculo_cobro_clientes]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_calculo_cobro_clientes] (

@Cerrar Varchar(20)

)

AS	

declare @Hoy varchar(30)
declare @existe int

declare @id_contrato int
declare @id_punto_servicio int
declare @periodo varchar(30)
declare @inicio datetime
declare @fin datetime
declare @existen_datos int
declare @estado_cursor int
declare @mesarriendo varchar(50)
declare @cobro_arriendo varchar(50)

declare @periodo_cobro varchar(20)

declare @Erase VARCHAR(20)
declare @PuntoServicio VARCHAR(20)
declare @DatosAnexos VARCHAR(20)

declare @cerrar_proceso int
declare @proceso_fue_cerrado int
declare @mescobro varchar(20)
declare @yearcobro varchar(20)

declare @firstday datetime

	set nocount on

	If @Cerrar = 'SI'
	Begin 
		Set @cerrar_proceso = 1
	End
	Else
	Begin
		If @Cerrar = 'NO'
		Begin
			Set @cerrar_proceso = 0
		End
		Else
		Begin
			RAISERROR ('Debe indicar si desea CERRAR el proceso. Ingrese SI o NO', 16, 1)
			exec sp_log_calculo_cobro 'ERROR','Debe indicar si desea CERRAR el proceso. Ingrese SI o NOs'
			Return
		End
	End

	Begin transaction

					Delete from calculo_cobro_clientes

				    Update T1
					Set T1.cantidad = 0
					from hoja_ruta_detalle T1 
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On T1.id_punto_servicio = T2.id_punto_servicio
					And T1.fecha between T2.inicio and T2.fin
					Where T1.peso > 0
					
					Update T1
					Set T1.peso = 0
					from hoja_ruta_detalle T1 
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On T1.id_punto_servicio = T2.id_punto_servicio
					And T1.fecha between T2.inicio and T2.fin
					Where T1.cantidad > 0
															  
	Commit transaction

	SEt @mesarriendo = (Convert(Varchar,(Month(Getdate())-1)))
	Set @mescobro =  (Convert(Varchar,(Month(Getdate())-1)))
	Set @yearcobro =  Convert(Varchar,(Year(Getdate())))

	If @mescobro = '12'
	Begin
		Set @yearcobro =  Convert(Varchar,(Year(Getdate())-1))
	End

	If len(@mescobro) = 1
	Begin
		Set @mescobro = '0' + @mescobro
	End
	If len(@mesarriendo) = 1
	Begin
		Set @mesarriendo = '0' + @mesarriendo
	End
	Set @periodo_cobro = (@mescobro + @yearcobro)
	Set @cobro_arriendo = ('01/' + @mesarriendo + '/' + @yearcobro)

	---- VALIDAR SI EL PROCESO PARA EL PERIODO DE COBRO SE ENCUENTRA CERRADO
	Set @proceso_fue_cerrado = (Select Isnull(Count(*),0) From calculo_realizado
										Where Fecha = @periodo_cobro
										And Cerrado = 1)

	If @proceso_fue_cerrado > 0
	Begin
			RAISERROR ('El proceso de Calculo se encuentra CERRADO', 16, 1)
			exec sp_log_calculo_cobro 'ERROR','El proceso de Calculo se encuentra CERRADO'
			Return
	End
	----Else
	----Begin

		---- Delete From calculo_realizado Where Fecha = @periodo_cobro

	----End

	Set @existe = (Select Isnull(Count(*),0) From V_CONTRATO_PUNTO_SERVICIO)

	If @existe = 0
	Begin
	    RAISERROR ('Error Calculo Cobro Clientes - calculo_cobro_empresas', 16, 1)
		exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes - calculo_cobro_empresas'
		Return
	End

	Select @Erase = 'BorraCobroClientes';
	SELECT @PuntoServicio = 'PuntoServicio';
	Select @DatosAnexos = 'DatosAnexos'
		
	---- PRIMERO SE DEBE VALIDAR QUE EXISTEN PUNTOS DE SERVICIOS EN HOJA_RUTA_DETALLE
	---- PARA EL PERIODO DE FACTURACION EN PROCESO	
    ---- CADA CONTRATO TIENE SU PROPIO PERIODO DE BUSQUEDA VER TABLA  calculo_cobro_empresas	       

            Begin Transaction @PuntoServicio

			BEGIN
				  
				  Set @firstday = (DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0))
				  			
				  ---- SE PROCESA LA INFORMACION PUES EXISTEN DATOS
				  ---- SE INSERTAN EN LA TABLA calculo_cobro_clientes 
									
				    ---- SE TOMAN LOS DATOS DE LA TABLA hoja_ruta_detalle
					---- EXCLUYENDO LOS id_tipo_modalidad not in (5,7,9,10,11,12)
					---- QUE SE COBRAN UNA VEZ AL MES ... CANTIDAD = 1

					Insert	into calculo_cobro_clientes
						(
							id_hoja_ruta
						   ,id_punto_servicio
						   ,cantidad
						   ,peso
						   ,estado
						   ,fechaFull
						   ,Fecha
						   ,Monthis
						   ,Yearis
						   ,id_tipo_modalidad
						   ,valor
						   ,proceso_cerrado
						   ,id_detalle_modelo_cobro
						)
					Select	Distinct hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							hoja.fecha,
							@periodo_cobro,
							Convert(int,Month(@firstday)) As Monthis,
							Convert(int,Year(@firstday)) As Yearis,
		 				--	Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							--Null, --- tipo_modalidad
							--Null,
							--Null,
							0,
							--Null,
							--Null,
							deta.id_detalle_modelo_cobro
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
							On cob.id_punto_servicio = pto.id_punto_servicio
							Inner join modelo_cobro_detalle deta
							On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
							On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
							On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner Join hoja_ruta ruta
							On hoja.id_hoja_ruta = ruta.id_hoja_ruta
							And hoja.fecha between perio.inicio and perio.fin
							And deta.valor is not Null
							And deta.valor > 0
							And deta.id_tipo_modalidad not in (5,7,9,10,11,12,8)
							And hoja.cantidad > 0
							And ruta.estado = 3

					Insert	into calculo_cobro_clientes
					(
							id_hoja_ruta
						   ,id_punto_servicio
						   ,cantidad
						   ,peso
						   ,estado
						   ,fechaFull
						   ,Fecha
						   ,Monthis
						   ,Yearis
						   ,id_tipo_modalidad
						   ,valor
						   ,proceso_cerrado
						   ,id_detalle_modelo_cobro
						)
					Select	Distinct hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							hoja.fecha,
							@periodo_cobro,
							Convert(int,Month(@firstday)) As Monthis,
							Convert(int,Year(@firstday)) As Yearis,
		 				--	Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							--Null, --- tipo_modalidad
							--Null,
							--Null,
							0,
							--Null,
							--Null,
							deta.id_detalle_modelo_cobro
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
							On cob.id_punto_servicio = pto.id_punto_servicio
							Inner join modelo_cobro_detalle deta
							On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
							On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
							On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner Join hoja_ruta ruta
							On hoja.id_hoja_ruta = ruta.id_hoja_ruta
							And hoja.fecha between perio.inicio and perio.fin
							And deta.valor is not Null
							And deta.valor > 0
							And deta.id_tipo_modalidad not in (5,7,9,10,11,12)
							And hoja.peso > 0
							And deta.id_tipo_modalidad = 8
							And ruta.estado = 3	

			        ---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					Insert	into calculo_cobro_clientes
					(
							id_hoja_ruta
						   ,id_punto_servicio
						   ,cantidad
						   ,peso
						   ,estado
						   ,fechaFull
						   ,Fecha
						   ,Monthis
						   ,Yearis
						   ,id_tipo_modalidad
						   ,valor
						   ,proceso_cerrado
						   ,id_detalle_modelo_cobro
						   ,cobro_arriendo
						)
					Select	0, 
							cal.id_punto_servicio,
							1, ---- cantidad
							0, ---- peso
							2, ---- estado
							@firstday, ---- primer dia mes anterior --- dato ficticio
							@periodo_cobro,
							Convert(int,Month(@firstday)) As Monthis,
							Convert(int,Year(@firstday)) As Yearis,
					 	--	Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							--Null,
							fijo.id_tipo_modalidad, --- id_tipo_modalidad
							fijo.valor, -- VALOR desde modelo_detalle_cobro
							--Null, --- tipo_modalidad
							--Null,
							--Null,
							0,
							--Null,
							--Null,
							--Null,
							--Null,
							fijo.id_detalle_modelo_cobro, --- id_detalle_modelo_cobro
							@cobro_arriendo
							--Null,
							--Null,
							--Null,
							--Null
						    From V_PUNTOS_UNICOS cal
							Inner Join V_ARRIENDO_COSTO_FIJO fijo
							On cal.id_punto_servicio = fijo.id_punto_servicio

							
						  If @@error <> 0 
						  Begin					
							Rollback Transaction @PuntoServicio
							exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes'
							RAISERROR ('Error Calculo Cobro Clientes', 16, 1)
							Return
						  End	

		END

		Commit Transaction @PuntoServicio

		---- SE CARGAN LOS DATOS ANEXOS A LOS PUNTOS DE SERVICIO

		Begin Transaction @DatosAnexos

		    Update T1
			Set T1.tipo_modalidad = T2.tipo_modalidad	
			from calculo_cobro_clientes T1 
			inner join tipo_modalidad T2
			On T1.id_tipo_modalidad = T2.id_tipo_modalidad

		--- DESCRIPCION DEL PUNTO DE SERVICIO
			Update T1
			Set T1.nombre_punto = T2.nombre_punto,
				T1.id_contrato = T2.id_contrato
			from calculo_cobro_clientes T1 
			inner join puntos_servicio T2
			On T1.id_punto_servicio = T2.id_punto_servicio		

		--- DESCRIPCION DEL PUNTO DE SERVICIO
		    Update T1
			Set T1.id_cliente = T2.id_cliente,
				 T1.id_sucursal = T2.id_sucursal,
				 T1.estado_contrato = T2.estado,
				 T1.id_modelo_cobro = T2.id_modelo_cobro
			from calculo_cobro_clientes T1 
			inner join contratos T2
			On T1.id_contrato = T2.id_contrato		
		 
		--- DESCRIPCION CLIENTE
		    Update T1
			Set T1.rut_cliente = T2.rut_cliente,
				 T1.giro_comercial = T2.giro_comercial,
				 T1.razon_social = T2.razon_social,
				 T1.nombre_fantasia = T2.nombre_fantasia
			from calculo_cobro_clientes T1 
			inner join clientes T2
			On T1.id_cliente = T2.id_cliente		

		--- DESCRIPCION SUCURSAL
		    Update T1
			Set T1.sucursal = T2.sucursal,
				 T1.descripcion = T2.descripcion,
				 T1.rut_sucursal = T2.rut_sucursal
			from calculo_cobro_clientes T1 
			inner join sucursales T2
			On T1.id_sucursal = T2.id_sucursal

		--- COMO SE CALCULA EL VALOR DEPENDIENDO DE tipo_modalidad
			Update calculo_cobro_clientes
			Set como_se_calcula = 'CANTIDAD'
			Where id_tipo_modalidad in (5,6,7,9,10,11,12)

		---- LOS COSTOS FIJOS SE COBRAN UNA SOLA VEZ AL MES POR LO
		---- CUAL LA CANTIDAD SE SETEA A 1
			 Update calculo_cobro_clientes
			 Set cantidad = 1,
				 peso = 0
			 Where id_tipo_modalidad in (5,7,9,10,11,12)		

			 Update calculo_cobro_clientes
			 Set como_se_calcula = 'PESO',
			 cantidad = 0
		     Where id_tipo_modalidad in (8)

		-------------------------------------------------------------------
		-------------------------------------------------------------------
		----- CALCULO DEL VALOR BRUTO DEPENDIENDO DEL CAMPO como_se_calcula
			Update calculo_cobro_clientes
			Set bruto = 0

			Update calculo_cobro_clientes
			Set bruto = (isnull(valor,0) * isnull(cantidad,0))
			Where como_se_calcula = 'CANTIDAD'
			
			Update calculo_cobro_clientes
			Set bruto = (isnull(valor,0))
			Where como_se_calcula = 'PESO'
			And id_tipo_modalidad = 8

		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 8

		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 6

			Update calculo_cobro_clientes
			Set bruto = (isnull(cantidad,0) * isnull(valor,0))
			Where como_se_calcula = 'CANTIDAD'
			And id_tipo_modalidad = 6

		----- FECHA QUE SE REALIZA EL CALCULO
			Update calculo_cobro_clientes
			Set fechaCalculo = Getdate()

	     ---- ORDEN
		      Update T1
			  Set T1.Orden = T2.Orden
			  from calculo_cobro_clientes T1 
			  inner join hoja_ruta_detalle T2
			  On T1.id_hoja_ruta = T2.id_hoja_ruta
			  And T1.id_punto_servicio = T2.id_punto_servicio

			--- UPDATE CAMION CONDUCTOR
		    Update T1
			 Set T1.id_camion = T2.id_camion,
				 T1.camion = T2.patente,
				 T1.id_conductor = T2.id_conductor,
				 T1.conductor = T2.nombre_conductor
			from calculo_cobro_clientes T1 
			inner join V_CONDUCTOR_CAMION T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

			---Update calculo_cobro_clientes
			---Set bruto = 0
			---Where como_se_calcula = 'PESO'
			---And id_tipo_modalidad = 8

	     ---- ORDENAMIENTO PARA LA SALIDA DEL ARCHIVO PDF
		 Update calculo_cobro_clientes Set sorting = 1, 
										   glosa = tipo_modalidad Where id_tipo_modalidad = 7

		 Update calculo_cobro_clientes Set sorting = 2,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 10

		 Update calculo_cobro_clientes Set sorting = 3,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 12

         Update calculo_cobro_clientes Set sorting = 4,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 11

		 Update calculo_cobro_clientes Set sorting = 5,
									       glosa = tipo_modalidad Where id_tipo_modalidad = 9

		 Update calculo_cobro_clientes Set sorting = 6,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 5

		 Update calculo_cobro_clientes Set sorting = 8,
										   glosa = 'Retiro' Where id_tipo_modalidad = 8

		 Update calculo_cobro_clientes Set sorting = 9,
										   glosa = 'Retiro' Where id_tipo_modalidad = 6


		----- SE CIERRA EL PROYECTO SI FUE REQUERIDO DE ESA MANERA
		    Insert into calculo_realizado (Fecha,
										   Cerrado)
								   Values (@periodo_cobro,
										   @cerrar_proceso)
			iF @cerrar_proceso = 1
			Begin
			---- YA QUE EL PROCESO FUE CERRADO SE COPIAN TODOS LOS DATOS
			---- DE LA TABLA calculo_cobro_clientes
			---- A LA TABLA cobro_clientes_cerrado

			    Delete From cobro_clientes_cerrado Where Fecha = @periodo_cobro

				Insert into cobro_clientes_cerrado
				Select * From calculo_cobro_clientes
				
			End

			If @@error <> 0 
			Begin					
				Rollback Transaction @DatosAnexos
				exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes - Datos Anexos'
				RAISERROR ('Error Calculo Cobro Clientes - Datos Anexos', 16, 1)
				Return
			End

		   Commit Transaction @DatosAnexos
		   exec sp_log_calculo_cobro 'EXITO', 'Calculo Cobro Clientes Finalizado'









GO
/****** Object:  StoredProcedure [dbo].[sp_calculo_cobro_empresas]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_calculo_cobro_empresas]

AS	

declare @id_cliente int
declare @id_sucursal int
declare @id_contrato int
declare	@sucursal varchar(250)
declare @periodo_desde float
declare @periodo_hasta float
declare @inicio datetime
declare @fin datetime
declare @valor_mes varchar(3)
declare @year varchar(6)
declare @dia varchar(3)
declare @key varchar(60)

Declare periodo_cobro_sucursal 
	CURSOR FOR SELECT id_cliente,
					  id_sucursal,
					  id_contrato,
					  periodo_desde,
					  periodo_hasta
				 From V_PERIODO_COBRO_POR_EMPRESA;

set nocount on

	----- DETERMINACION DEL PERIODO DE COBRO POR SUCURSAL-CONTRATO
	----- EL PERIODO ES VARIABLE
	OPEN periodo_cobro_sucursal
		fetch next from periodo_cobro_sucursal 
		into @id_cliente, @id_sucursal, @id_contrato,@periodo_desde,@periodo_hasta
			while @@fetch_status = 0 
				begin 
			       If @periodo_desde = 1
				   Begin
						Set @inicio = (SELECT CONVERT(DATE,DATEADD(D,-datepart(d,getdate())+1,DATEADD(M,-1,GETDATE())))) 
				   End
				   Else
				   Begin
						
						Set @valor_mes = (Convert(varchar,month(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))))
						If @valor_mes = '12'
						Begin
							Set @year = (Convert(varchar,(year(getdate()) - 1)))
						End
						Else
						Begin
							Set @year = (Convert(varchar,(year(getdate()))))
						End

						If len(@valor_mes) = 1
						Begin
							Set @valor_mes = ('0' + @valor_mes)
						End

						Set @dia = (Convert(Varchar,@periodo_desde))
						If len(@dia) = 1
						Begin
							Set @dia = ('0' + @dia)
						End
						Set @inicio = (Convert(datetime,(@dia + '/' + @valor_mes + '/' + @year),103))

				   End
				   Set @Key = Replace(Replace(Convert(Varchar,@inicio,103),'/',''),'-','')

				   If @periodo_hasta = 1
				   Begin
						Set @fin = (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))  
				   End
				   Else
				   Begin
						Set @valor_mes = (Convert(varchar,month(DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()),0)))))
						If @valor_mes = '12'
						Begin
							Set @fin = (Convert(varchar,dateadd(DD, -1, dateadd(YY,datediff(yy,0,getdate()),0)),103))
						End
						Else
						Begin

							Set @year = (Convert(varchar,(year(getdate()))))
							If len(@valor_mes) = 1
							Begin
								Set @valor_mes = ('0' + @valor_mes)
							End

							Set @dia = (Convert(varchar,@periodo_hasta))
							If len(@dia) = 1
							Begin
								Set @dia = ('0' + @dia)
							End

							Set @fin = Convert(datetime,(@dia + '/' +  @valor_mes + '/' +  @year),103)

						End
				   End

				   Set @Key = @Key + Replace(Replace(Convert(Varchar,@fin,103),'/',''),'-','')				   
				   Set @Key = @Key + '-' +  'CLIE' + Convert(Varchar,@id_cliente)
				   Set @Key = @Key + '-' +  'SUCU' + Convert(Varchar,@id_sucursal)
				   Set @Key = @Key + '-' +  'CONT' + Convert(Varchar,@id_contrato)
				    				   
				   ---- SE INSERTAN LOS PERIODOS DE COBRO EN LA TABLA calculo_cobro_empresas
				   Insert into calculo_cobro_empresas (id_cliente,
													   id_sucursal,
													   id_contrato,
													   periodo,
													   inicio,
													   fin)
											   Values (@id_cliente,
													   @id_sucursal,
													   @id_contrato,
													   @Key,
													   @inicio,
													   @fin)

	               FETCH NEXT FROM periodo_cobro_sucursal into @id_cliente, 
															   @id_sucursal, 
															   @id_contrato,
															   @periodo_desde,
															   @periodo_hasta

				End 
		
		close periodo_cobro_sucursal 
		deallocate periodo_cobro_sucursal












GO
/****** Object:  StoredProcedure [dbo].[sp_clientes_escalables]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[sp_clientes_escalables]

As

declare @existe int
declare @veces int
declare @Tarifa int
declare @id_contratox int
declare @id_punto_serviciox int

Declare cobro_escalable_running
	CURSOR FOR Select id_punto_servicio,
	                  id_contrato,
	                  veces
					  From V_CLIENTES_CONTRATOS_ESCALABLES;

	set nocount on
	
				---- **************************************************************************
			---- COBRO A CLIENTES  CON  TARIFA ESCALABLE
			---- PUNTOS DE SERVICIOS 179 443 AMBOS CASOS
			---- CON EL ID CONTRATO 179
				OPEN cobro_escalable_running
				fetch next from cobro_escalable_running 
								into @id_punto_serviciox,
								     @id_contratox,
									 @veces

				 while @@fetch_status = 0 
				 begin

					Set @Tarifa = - 9
					---- SE BUSCA LA TARIFA A PAGAR
					Set @existe = (Select Isnull(Count(*),0) From cobro_cliente_escalable
										  Where id_punto_servicio = @id_punto_serviciox
										  And rango_min <= @veces
										  And rango_max >= @veces)
					If @existe > 0
					Begin
					
					   Set @existe = (Select id From cobro_cliente_escalable
										  Where id_punto_servicio = @id_punto_serviciox
										  And rango_min <= @veces
										  And rango_max >= @veces)

						Set @Tarifa = (Select Isnull(valor,0) From cobro_cliente_escalable
													  Where id = @existe)

                        Update T1
						Set  T1.valor = @Tarifa,
							 T1.bruto = @Tarifa
						from calculo_cobro_clientes T1 
						Where T1.id_punto_servicio = @id_punto_serviciox
						And T1.id_contrato = @id_contratox
						And T1.glosa = 'Retiro'
											
					End														
								  
				    fetch next from cobro_escalable_running 
								    into @id_punto_serviciox,
										 @id_contratox,
										 @veces
									 			 
				 end
				 		 
				 close cobro_escalable_running 
				 deallocate cobro_escalable_running		
			
		 ---- **************************************************************************
			






GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_clientes_periodo]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_cobro_clientes_periodo] (

@Cerrar Varchar(20),
@PeriodoSP Varchar(30),
@id_unidad int

)

AS	

declare @Hoy varchar(30)
declare @existe int

declare @id_contrato int
declare @id_punto_servicio int
declare @periodo varchar(30)
declare @inicio datetime
declare @fin datetime
declare @existen_datos int
declare @estado_cursor int
declare @mesarriendo varchar(50)
declare @cobro_arriendo varchar(50)
declare @arriendo varchar(50)

declare @periodo_cobro varchar(20)

declare @Erase VARCHAR(20)
declare @PuntoServicio VARCHAR(20)
declare @DatosAnexos VARCHAR(20)

declare @cerrar_proceso int
declare @proceso_fue_cerrado int
declare @mescobro varchar(20)
declare @yearcobro varchar(20)

declare @firstday datetime
declare @Cierre datetime
declare @valoruf float
declare @mesuf int
declare @yearuf int

declare @veces int
declare @Tarifa int
declare @id_contratox int
declare @id_punto_serviciox int

	set nocount on

	If @Cerrar = 'SI'
	Begin 
		Set @cerrar_proceso = 1
	End
	Else
	Begin
		If @Cerrar = 'NO'
		Begin
			Set @cerrar_proceso = 0
		End
		Else
		Begin
			RAISERROR ('Debe indicar si desea CERRAR el proceso. Ingrese SI o NO', 16, 1)
			exec sp_log_calculo_cobro 'ERROR','Debe indicar si desea CERRAR el proceso. Ingrese SI o NOs'
			Return
		End
	End

	---- DETERMINACION DEL PERIODO DE COBRO
    Set @mesarriendo = substring(@PeriodoSP,1,2)
	Set @mescobro =  substring(@PeriodoSP,1,2)
	Set @yearcobro =  substring(@PeriodoSP,3,4)

	If len(@mescobro) = 1
	Begin
		Set @mescobro = '0' + @mescobro
	End
	If len(@mesarriendo) = 1
	Begin
		Set @mesarriendo = '0' + @mesarriendo
	End
	Set @periodo_cobro = (@mescobro + @yearcobro)
	Set @cobro_arriendo = ('01/' + @mesarriendo + '/' + @yearcobro)
	---- Set @arriendo = ('01-' + @mesarriendo + '-' + @yearcobro)
	Set @arriendo = (@yearcobro + '-' + @mesarriendo + '-' + '01')
	
	--- DETERMINACION DE LAS FECHAS DE COBRO
	execute sp_cobro_empresas_periodo  @PeriodoSP

	--- *****************************************************************************
	Begin transaction

					Delete from calculo_cobro_clientes
					Delete from calculo_cobro_disposicion
					Delete from cobro_cliente_total

				    Update T1
					Set T1.cantidad = 0
					from hoja_ruta_detalle T1 
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On T1.id_punto_servicio = T2.id_punto_servicio
					Inner join V_PUNTO_HOOK hook
					On T1.id_punto_servicio = hook.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between T2.inicio and T2.fin
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
					And T1.peso > 0
					
					Update T1
					Set T1.peso = 0
					from hoja_ruta_detalle T1 
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On T1.id_punto_servicio = T2.id_punto_servicio
					Inner join V_PUNTO_FRONT_LOADER loader
					On T1.id_punto_servicio = loader.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between T2.inicio and T2.fin
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
					And T1.cantidad > 0
					
					--- CAJA RECOLECTORA
					--- UPDATE PARA QUE TOME LOS REGISTROS
					--- ASIGNANDOLE UNA CANTIDAD = 1										
					Update T1
					Set T1.cantidad = 1,
						T1.peso = 0
					from hoja_ruta_detalle T1 
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On T1.id_punto_servicio = T2.id_punto_servicio
					Inner join V_PTO_CAJA_RECOLECTORA3 loader
					On T1.id_punto_servicio = loader.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between T2.inicio and T2.fin
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
															  
	Commit transaction
	--- *****************************************************************************

	---- VALIDAR SI EL PROCESO PARA EL PERIODO DE COBRO SE ENCUENTRA CERRADO
	Set @proceso_fue_cerrado = (Select Isnull(Count(*),0) From calculo_realizado
										Where Fecha = @periodo_cobro
										And Cerrado = 1)

	If @proceso_fue_cerrado > 0
	Begin
			RAISERROR ('El proceso de Calculo se encuentra CERRADO', 16, 1)
			exec sp_log_calculo_cobro 'ERROR','El proceso de Calculo se encuentra CERRADO'
			Return
	End

	Set @existe = (Select Isnull(Count(*),0) From V_CONTRATO_PUNTO_SERVICIO)

	If @existe = 0
	Begin
	    RAISERROR ('Error Calculo Cobro Clientes - calculo_cobro_empresas', 16, 1)
		exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes - calculo_cobro_empresas'
		Return
	End

	Select @Erase = 'BorraCobroClientes';
	SELECT @PuntoServicio = 'PuntoServicio';
	Select @DatosAnexos = 'DatosAnexos'
		
	---- PRIMERO SE DEBE VALIDAR QUE EXISTEN PUNTOS DE SERVICIOS EN HOJA_RUTA_DETALLE
	---- PARA EL PERIODO DE FACTURACION EN PROCESO	
    ---- CADA CONTRATO TIENE SU PROPIO PERIODO DE BUSQUEDA VER TABLA  calculo_cobro_empresas	       

            Begin Transaction @PuntoServicio

			BEGIN
				  
				  Set @firstday = (DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0))
				  			
				  ---- SE PROCESA LA INFORMACION PUES EXISTEN DATOS
				  ---- SE INSERTAN EN LA TABLA calculo_cobro_clientes 

					Insert	into calculo_cobro_clientes
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
								On cob.id_punto_servicio = pto.id_punto_servicio
							INNER JOIN contratos CTR ON PTO.id_contrato = CTR.id_contrato AND CTR.id_unidad = @id_unidad
							Inner join modelo_cobro_detalle deta
								On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
								On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
								On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner Join hoja_ruta ruta
								On hoja.id_hoja_ruta = ruta.id_hoja_ruta
								And ruta.fecha between perio.inicio and perio.fin
								And deta.valor is not Null
								And deta.valor > 0
								And deta.id_tipo_modalidad not in (7,9,10,11,12,8)
								And hoja.cantidad > 0
								And ruta.estado = 3			---- hoja de ruta cerrada
								And hoja.estado = 2			---- punto de servicio realizado

					Insert	into calculo_cobro_clientes
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
								On cob.id_punto_servicio = pto.id_punto_servicio
							INNER JOIN contratos CTR ON PTO.id_contrato = CTR.id_contrato AND CTR.id_unidad = @id_unidad
							Inner join modelo_cobro_detalle deta
								On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
								On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
								On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner Join hoja_ruta ruta
								On hoja.id_hoja_ruta = ruta.id_hoja_ruta
								And ruta.fecha between perio.inicio and perio.fin
								And deta.valor is not Null
								And deta.valor > 0
								And deta.id_tipo_modalidad not in (7,9,10,11,12)
								And hoja.peso > 0
								And deta.id_tipo_modalidad = 8
								And ruta.estado = 3			---- hoja de ruta cerrada
								And hoja.estado = 2			---- punto de servicio realizado

						  If @@error <> 0 
						  Begin					
							Rollback Transaction @PuntoServicio
							exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes'
							RAISERROR ('Error Calculo Cobro Clientes', 16, 1)
							Return
						  End	

		END

		Commit Transaction @PuntoServicio

		---- SE CARGAN LOS DATOS ANEXOS A LOS PUNTOS DE SERVICIO

		Begin Transaction @DatosAnexos

					---- SERVICIOS HOOK QUE SOLO TIENEN ASOCIADO MODALIDAD = 8
					---- PARA ESTOS CASOS MODALIDAD = 6 NO EXISTE
					Insert	into calculo_cobro_clientes
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
								On cob.id_punto_servicio = pto.id_punto_servicio
							INNER JOIN contratos CTR ON PTO.id_contrato = CTR.id_contrato AND CTR.id_unidad = @id_unidad
							Inner join modelo_cobro_detalle deta
								On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
								On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
								On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner Join hoja_ruta ruta
								On hoja.id_hoja_ruta = ruta.id_hoja_ruta
								And ruta.fecha between perio.inicio and perio.fin
								And deta.valor is not Null
								And deta.valor > 0
								And hoja.peso > 0
								And deta.id_tipo_modalidad = 8
								And ruta.estado = 3			---- hoja de ruta cerrada
								And hoja.estado = 2			---- punto de servicio realizado
								And hoja.id_punto_servicio not in
							(Select id_punto_servicio From V_PUNTOS_HOOK_CALCULADOS)
							
					Insert	into calculo_cobro_clientes
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							8, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
								On cob.id_punto_servicio = pto.id_punto_servicio
							INNER JOIN contratos CTR ON PTO.id_contrato = CTR.id_contrato AND CTR.id_unidad = @id_unidad
							Inner join modelo_cobro_detalle deta
								On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
								On cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio
								On hoja.id_punto_servicio = perio.id_punto_servicio
							Inner join V_PUNTO_HOOK hook
								On hoja.id_punto_servicio = hook.id_punto_servicio
							Inner Join hoja_ruta ruta
								On hoja.id_hoja_ruta = ruta.id_hoja_ruta
								And ruta.fecha between perio.inicio and perio.fin
								And deta.valor is not Null
								And deta.valor > 0
								And hoja.peso > 0
								And deta.id_tipo_modalidad not in (8,7)
								And ruta.estado = 3			---- hoja de ruta cerrada
								And hoja.estado = 2			---- punto de servicio realizado
								And hoja.id_punto_servicio not in
							(Select id_punto_servicio From V_PUNTOS_HOOK_CALCULADOS)
												
			        ---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					Insert	into calculo_cobro_clientes
					Select	Distinct 0, 
							cal.id_punto_servicio,
							1, ---- cantidad
							0, ---- peso
							2, ---- estado
							@firstday, ---- primer dia mes anterior --- dato ficticio
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
					 		Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							fijo.id_tipo_modalidad, --- id_tipo_modalidad
							fijo.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							Null,
							Null,
							fijo.id_detalle_modelo_cobro, --- id_detalle_modelo_cobro
							@arriendo,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
						    From V_PUNTOS_UNICOS cal
							Inner Join V_ARRIENDO_COSTO_FIJO fijo On cal.id_punto_servicio = fijo.id_punto_servicio
							Inner Join V_CONTRATO_PUNTO_SERVICIO perio On perio.id_punto_servicio = fijo.id_punto_servicio
							Where cal.fecha between perio.inicio and perio.fin
							  and perio.id_unidad = @id_unidad
			
			Update T1
			Set T1.Sistema = 'Hook'	
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_HOOK T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
			Update T1
			Set T1.Sistema = 'Front Loader'	
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_FRONT_LOADER T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
			Update T1
			Set T1.Sistema = 'Caja Recolectora'	
			from calculo_cobro_clientes T1 
			inner join V_PTO_CAJA_RECOLECTORA3 T2
			On T1.id_punto_servicio = T2.id_punto_servicio						
				
		    Update T1
			Set T1.tipo_modalidad = T2.tipo_modalidad	
			from calculo_cobro_clientes T1 
			inner join tipo_modalidad T2
			On T1.id_tipo_modalidad = T2.id_tipo_modalidad

		--- DESCRIPCION DEL PUNTO DE SERVICIO
			Update T1
			Set T1.nombre_punto = T2.nombre_punto,
				T1.id_contrato = T2.id_contrato
			from calculo_cobro_clientes T1 
			inner join puntos_servicio T2
			On T1.id_punto_servicio = T2.id_punto_servicio		

		--- DESCRIPCION DEL PUNTO DE SERVICIO
		    Update T1
			Set T1.id_cliente = T2.id_cliente,
				 T1.id_sucursal = T2.id_sucursal,
				 T1.estado_contrato = T2.estado,
				 T1.id_modelo_cobro = T2.id_modelo_cobro
			from calculo_cobro_clientes T1 
			inner join contratos T2
			On T1.id_contrato = T2.id_contrato		
		 
		--- DESCRIPCION CLIENTE
		    Update T1
			Set T1.rut_cliente = T2.rut_cliente,
				 T1.giro_comercial = T2.giro_comercial,
				 T1.razon_social = T2.razon_social,
				 T1.nombre_fantasia = T2.nombre_fantasia
			from calculo_cobro_clientes T1 
			inner join clientes T2
			On T1.id_cliente = T2.id_cliente		

		--- DESCRIPCION SUCURSAL
		    Update T1
			Set T1.sucursal = T2.sucursal,
				 T1.descripcion = T2.descripcion,
				 T1.rut_sucursal = T2.rut_sucursal
			from calculo_cobro_clientes T1 
			inner join sucursales T2
			On T1.id_sucursal = T2.id_sucursal

		--- COMO SE CALCULA EL VALOR DEPENDIENDO DE tipo_modalidad
			Update calculo_cobro_clientes
			Set como_se_calcula = 'CANTIDAD'
			Where id_tipo_modalidad in (5,6,7,9,10,11,12)

		---- LOS COSTOS FIJOS SE COBRAN UNA SOLA VEZ AL MES POR LO
		---- CUAL LA CANTIDAD SE SETEA A 1
			 Update calculo_cobro_clientes
			 Set cantidad = 1,
				 peso = 0
			 Where id_tipo_modalidad in (5,7,9,10,11,12)		

			 Update calculo_cobro_clientes
			 Set como_se_calcula = 'PESO',
			 cantidad = 0
		     Where id_tipo_modalidad in (8)

		-------------------------------------------------------------------
		-------------------------------------------------------------------
		----- CALCULO DEL VALOR BRUTO DEPENDIENDO DEL CAMPO como_se_calcula
			Update calculo_cobro_clientes
			Set bruto = 0

			Update calculo_cobro_clientes
			Set bruto = (isnull(valor,0) * isnull(cantidad,0))
			Where como_se_calcula = 'CANTIDAD'
			
			Update calculo_cobro_clientes
			Set bruto = (isnull(valor,0))
			Where como_se_calcula = 'PESO'
			And id_tipo_modalidad = 8
			
		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 8
			
			Update T1
			Set  T1.valorenuf = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 8
			And T1.id_punto_servicio in (17,87,382)
			
		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 6

			Update calculo_cobro_clientes
			Set bruto = (isnull(cantidad,0) * isnull(valor,0))
			Where como_se_calcula = 'CANTIDAD'
			And id_tipo_modalidad = 6
			
	        ---- EXCEPCION PUNTO DE SERVICIOO 17 EDAM COT 20 M3
	        ---- SE COBRA EN UF
	        ---- BUSQUEDA UF MES ANTERIOR
	        Set @mesuf = Convert(int,@mescobro)
	        Set @yearuf = Convert(int,@yearcobro)
	        If @mesuf = 1
	        Begin
				Set @mesuf = 12
				Set @yearuf = @yearuf - 1
	        End
	        
	        Set @valoruf = (Select Isnull(valor_uf,0) From Uf Where mes = @mesuf And año = @yearuf)
	        
	        ---- EXCEPCION PUNTO DE SERVICIOO 17 EDAM COT 20 M3
	        ---- SE COBRA EN UF
	        ---- BUSQUEDA UF MES ANTERIOR
			Update calculo_cobro_clientes
			Set bruto = (isnull(valorenuf,0)) * (isnull(@valoruf,0)),
			    valoruf = @valoruf
			Where como_se_calcula = 'PESO'
			And id_tipo_modalidad = 8
			And id_punto_servicio in (17,87,382)
		
		----- FECHA QUE SE REALIZA EL CALCULO
			Update calculo_cobro_clientes
			Set fechaCalculo = Getdate()

	     ---- ORDEN
		      Update T1
			  Set T1.Orden = T2.Orden
			  from calculo_cobro_clientes T1 
			  inner join hoja_ruta_detalle T2
			  On T1.id_hoja_ruta = T2.id_hoja_ruta
			  And T1.id_punto_servicio = T2.id_punto_servicio

			--- UPDATE CAMION CONDUCTOR
		    Update T1
			 Set T1.id_camion = T2.id_camion,
				 T1.camion = T2.patente,
				 T1.id_conductor = T2.id_conductor,
				 T1.conductor = T2.nombre_conductor
			from calculo_cobro_clientes T1 
			inner join V_CONDUCTOR_CAMION T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

			---- UPDATE VALOR ARRIENDO DEPENDIENDO DE LA CANTIDAD
			---- DE  EQUIPOS  EN  ARRIENDO  POR PUNTO DE SERVICIO
		    Update T1
			Set T1.bruto = (T2.cant_equipos * T1.valor)
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_NUM_EQUIPOS T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			Where T1.id_tipo_modalidad = 7

         ---- ********************************************************************
		 ---- CALCULO DE COBRO CASOS ESPECIALES
		 ---- EMPRESA MOLYMET, PUNTOS 355, 356 , 358
		 Update calculo_cobro_clientes
		 Set bruto = (isnull(valor,0) * isnull(peso,0))/1000
		 Where id_punto_servicio in (355, 356, 358)

		 ---- ********************************************************************

		 ---- UPDATE DEL CAMPO ORDEN CONCATENAR ID_HOJA_RUTA
		 ---- Y ORDEN
		 Update calculo_cobro_clientes
		 Set Orden = Convert(int,Convert(Varchar,id_hoja_ruta) + Convert(Varchar,Orden))
		 Where Orden Is Not Null
		 
		 Insert into calculo_cobro_clientes
		 Select Distinct 0, 
		                 id_punto_servicio,
						 1, --- cantidad
						 0, --- peso
						 2, --- estado
						 @arriendo,
						 Fecha,
						 Monthis,
						 Yearis,
						 nombre_punto,
						 Null,
						 id_contrato,
						 id_cliente,
						 id_sucursal,
						 'Activo', ---- estado contrato
						 1,
						 rut_cliente,
						 razon_social,
						 Null,
						 Null,
						 sucursal,
						 descripcion,
						 Null,
						 55, ---- id_tipo_modalidad
						 0, ---- valor
						 'Fijo Mensual', ---- tipo_modalidad
						 'CANTIDAD',
						 0,
						 0,
						 Getdate(),
						 NULL,
						 1,
						 0,
						 'Fijo Mensual',
						 @arriendo,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null, ---- NUEVOS CAMPOS HACIA ABAJO
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null
						 From calculo_cobro_clientes Where id_tipo_modalidad = 5

            Update T1
			Set T1.valor = T2.valor,
			    T1.bruto = T2.valor
			from calculo_cobro_clientes T1 
			inner join V_PTO_FIJO_MENSUAL T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			Where id_tipo_modalidad = 55
			
	     --- EXCEPCIONES   AL   COBRO  CAJA RECOLECTORA
	     --- ID_PUNTOS_SERVICIO 144,246,283,285,369,278
	     --- NO SE LES COBRA NADA A LA FECHA 01-01-2015
	     --- SIN  EMBARGO  DEBEN APARECER EN EL CALCULO
	     Update calculo_cobro_clientes
			Set Valor = 0,
				Bruto = 0
			Where id_punto_servicio in (144,246,283,285,369,278)
		
	     ---- ORDENAMIENTO PARA LA SALIDA DEL ARCHIVO PDF
		 Update calculo_cobro_clientes Set sorting = 1, 
										   glosa = tipo_modalidad Where id_tipo_modalidad = 7

		 Update calculo_cobro_clientes Set sorting = 2,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 10

		 Update calculo_cobro_clientes Set sorting = 3,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 12

         Update calculo_cobro_clientes Set sorting = 4,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 11

		 Update calculo_cobro_clientes Set sorting = 5,
									       glosa = 'Retiro' Where id_tipo_modalidad = 9

		 Update calculo_cobro_clientes Set sorting = 6,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 55

		 Update calculo_cobro_clientes Set sorting = 12,
										   glosa = 'Retiro' Where id_tipo_modalidad = 5

		 Update calculo_cobro_clientes Set sorting = 13,
										   glosa = 'Retiro' Where id_tipo_modalidad = 8

		 Update calculo_cobro_clientes Set sorting = 14,
										   glosa = 'Retiro' Where id_tipo_modalidad = 6

        ---- CALCULO DE LA DISPOSICION E INSERCION EN LA TABLA calculo_cobro_disposicion

		Insert into calculo_cobro_disposicion
		Select  cal.id_cliente,
				cal.rut_cliente,
				cal.Fecha,
				cal.id_contrato,
				cal.id_punto_servicio,
				glosa,
				Sum(Isnull(cal.Peso,0)) As Disposicion,
				0 CobroTonelada,
				0 As Valor,
				0				
				From V_CALCULO_COBRO_PUNTO cal
				Group By cal.id_cliente,
						 cal.rut_cliente,
						 cal.Fecha,
						 cal.id_contrato,
						 cal.id_punto_servicio,
						 glosa
				Having glosa = 'Retiro'

	   ----- ACTUALIZANDO EL COBRO POR TONELADA Y EL VALOR EN LA TABLA calculo_cobro_disposicion
	  		    Update T1
				Set T1.CobroTonelada = T2.valor,
			    T1.valor = (T1.Disposicion * T2.valor) /1000
				from calculo_cobro_disposicion T1 
				inner join V_PTO_SERVICIO_COBRO_TONELADA T2
				On T1.id_punto_servicio = T2.id_punto_servicio
				
	   ---- EXECPCIO EDAM COT 20 M3 SE COBRA POR UF
	   	        Update T1
				Set T1.CobroTonelada = T2.valor,
				    T1.valoruf = @valoruf,
			    T1.valor = Round((T1.Disposicion * T2.valor * @valoruf) /1000,0)
				from calculo_cobro_disposicion T1 
				inner join V_PTO_SERVICIO_COBRO_TONELADA T2
				On T1.id_punto_servicio = T2.id_punto_servicio
				aND T1.id_punto_servicio in (17,87,382)

	   ----- INSERCION DE LOS TOTALES DE COBROS EN LA TABLA cobro_cliente_total
			 Insert into cobro_cliente_total 
			 Select id_cliente,
				    rut_cliente,
				    Fecha,
				    id_punto_servicio,
				    Cobro,
				    TotalCliente,
				    IVA,
				    TotalClienteMasIVA
			   From V_COBRO_CLIENTES_TOTAL
			   Where Fecha = @periodo_cobro

			   ----  EXCEPTUANDO LOS PUNTOS 355 356 358 DE MOLYMET
		       ----  NO SE LES COBRA DISPOSICION
			   Update calculo_cobro_disposicion
			   Set valor = 0,
				   Disposicion = 0,
				   CobroTonelada = 0
				   Where id_punto_servicio in (355, 356, 358, 324)

		----- SE CIERRA EL PROYECTO SI FUE REQUERIDO DE ESA MANERA
		    Insert into calculo_realizado (Fecha,
										   Cerrado)
								   Values (@periodo_cobro,
										   @cerrar_proceso)
		
			---- UPDDATE DE VALOR Y BRUTO PARA PUNTOS CAJA RECOLECTORA
			---- ID_TIPO_MODALIDAD = 5
			---- NO SE COBRAN LOS RETIROS SOLO SE COBRA UN VALOR  FIJO
			---- MENSUAL								   
			Update T1
			Set T1.valor = 0,
			    T1.bruto = 0
			from calculo_cobro_clientes T1 
			inner join V_PTO_CAJA_RECOLECTORA3 T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And glosa = 'Retiro'

			If @cerrar_proceso = 1
			Begin

			---- YA QUE EL PROCESO FUE CERRADO SE COPIAN TODOS LOS DATOS
			---- DE LA TABLA calculo_cobro_clientes
			---- A LA TABLA cobro_clientes_cerrado

			    Set @Cierre = (Select Getdate())

				--- DETALLE DE CADA PUNTO POR PERIODO
			    --- Delete From cobro_clientes_cerrado Where Fecha = @periodo_cobro

				--- TOTAL DE COBRO DISPOSICION POR PUNTO Y PERIODO
				--- Delete From cobro_disposicion_cerrado Where Fecha = @periodo_cobro

				--- TOTAL DE COBRO A CADA CLIENTE POR PERIODO
				--- Delete From cliente_total_cerrado Where Fecha = @periodo_cobro

				Insert into cobro_clientes_cerrado
				Select * From calculo_cobro_clientes Where Fecha = @periodo_cobro

				Insert into cobro_disposicion_cerrado
				Select id_cliente,
						rut_cliente,
						Fecha,
						id_contrato,
						id_punto_servicio,
						glosa,
						Disposicion,
						CobroTonelada,
						Valor,
						valoruf			
						From calculo_cobro_disposicion
						Where Fecha = @periodo_cobro

				 ---- INSERCION DE LOS TOTALES A COBRAR
				 Insert into cliente_total_cerrado 
				 Select id_cliente,
						rut_cliente,
						Fecha,
						id_punto_servicio,
						TotalClienteSinDisp,
						TotalClienteMasDisp,
						IVA,
						TotalClienteMasIVA,
						@Cierre
				 From cobro_cliente_total
				 Where Fecha = @periodo_cobro

				 Delete From calculo_cobro_clientes
				 Delete From calculo_cobro_disposicion
				 Delete From cobro_cliente_total

			End

			If @@error <> 0 
			Begin					
				Rollback Transaction @DatosAnexos
				exec sp_log_calculo_cobro 'ERROR','Error Calculo Cobro Clientes - Datos Anexos'
				RAISERROR ('Error Calculo Cobro Clientes - Datos Anexos', 16, 1)
				Return
			End

		    Commit Transaction @DatosAnexos
		    exec sp_clientes_escalables
		    exec sp_log_calculo_cobro 'EXITO', 'Calculo Cobro Clientes Finalizado'










GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_clientes_periodo1]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[sp_cobro_clientes_periodo1] (

@fechaini varchar(20),
@fechafinal varchar(20)

)

AS	

declare @Hoy varchar(30)
declare @existe int

declare @id_contrato int
declare @id_punto_servicio int
declare @periodo varchar(30)
declare @inicio datetime
declare @fin datetime
declare @existen_datos int
declare @estado_cursor int
declare @mesarriendo varchar(50)
declare @cobro_arriendo varchar(50)
declare @arriendo varchar(50)

declare @periodo_cobro varchar(20)

declare @Erase VARCHAR(20)
declare @PuntoServicio VARCHAR(20)
declare @DatosAnexos VARCHAR(20)
declare @PuntosNoRealizados VARCHAR(20)

declare @cerrar_proceso int
declare @proceso_fue_cerrado int
declare @mescobro varchar(20)
declare @yearcobro varchar(20)

declare @firstday datetime

Declare @franquiciado varchar(100)
Declare @id_tipo_tarifa int
Declare @Veces int
Declare @Tarifa int
Declare @TarifaA int

Declare franquiciados_pago_tramo
	CURSOR FOR Select id_punto_servicio,
				 	  franquiciado,
					  id_tipo_tarifa,
				      Veces
					  From V_FRANQUICIADOS_ESCALABLES;

	set nocount on

	---- DETERMINACION DEL PERIODO DE COBRO
    Set @mesarriendo = Month(Getdate());
	Set @mescobro =  Month(Getdate());
	Set @yearcobro =  Year(Getdate());

	If len(@mescobro) = 1
	Begin
		Set @mescobro = '0' + @mescobro
	End
	If len(@mesarriendo) = 1
	Begin
		Set @mesarriendo = '0' + @mesarriendo
	End
	Set @periodo_cobro = (@mescobro + @yearcobro)
	Set @cobro_arriendo = ('01/' + @mesarriendo + '/' + @yearcobro)
	Set @arriendo = (@yearcobro + '-' + @mesarriendo + '-' + '01')
	

	--- *****************************************************************************
	Begin transaction

					Delete from calculo_cobro_clientes1
					Delete From ccc_puntos_no_realizados1

				    Update T1
					Set T1.cantidad = 0
					from hoja_ruta_detalle T1 
					Inner join V_PUNTO_HOOK hook
					On T1.id_punto_servicio = hook.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between @fechaini and @fechafinal
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
					And T1.peso > 0
					
					Update T1
					Set T1.peso = 0
					from hoja_ruta_detalle T1 
					Inner join V_PUNTO_FRONT_LOADER loader
					On T1.id_punto_servicio = loader.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between @fechaini and @fechafinal
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
					And T1.cantidad > 0
					
					--- CAJA RECOLECTORA
					--- UPDATE PARA QUE TOME LOS REGISTROS
					--- ASIGNANDOLE UNA CANTIDAD = 1										
					Update T1
					Set T1.cantidad = 1,
						T1.peso = 0
					from hoja_ruta_detalle T1 
					Inner join V_PTO_CAJA_RECOLECTORA3 loader
					On T1.id_punto_servicio = loader.id_punto_servicio
					Inner Join hoja_ruta T3
					On T1.id_hoja_ruta = T3.id_hoja_ruta
					And T3.fecha between @fechaini and @fechafinal
					Where T3.estado = 3			---- hoja de ruta cerrada
					And T1.estado = 2			---- punto de servicio realizado
															  
	Commit transaction
	--- *****************************************************************************

	Select @Erase = 'BorraCobroClientes';
	SELECT @PuntoServicio = 'PuntoServicio';
	Select @DatosAnexos = 'DatosAnexos'
	Select @PuntosNoRealizados = 'PuntosNoRealizados'
		
	---- PRIMERO SE DEBE VALIDAR QUE EXISTEN PUNTOS DE SERVICIOS EN HOJA_RUTA_DETALLE
	---- PARA EL PERIODO DE FACTURACION EN PROCESO	
    ---- CADA CONTRATO TIENE SU PROPIO PERIODO DE BUSQUEDA VER TABLA  calculo_cobro_empresas	       

            Begin Transaction @PuntoServicio

			BEGIN
				  
				  Set @firstday = (DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE())-1, 0))
				  			
				  ---- SE PROCESA LA INFORMACION PUES EXISTEN DATOS
				  ---- SE INSERTAN EN LA TABLA calculo_cobro_clientes1 
									
				    ---- SE TOMAN LOS DATOS DE LA TABLA hoja_ruta_detalle
					---- EXCLUYENDO LOS id_tipo_modalidad not in (5,7,9,10,11,12)
					---- QUE SE COBRAN UNA VEZ AL MES ... CANTIDAD = 1

					Insert	into calculo_cobro_clientes1
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
							On cob.id_punto_servicio = pto.id_punto_servicio
							Inner join modelo_cobro_detalle deta
							On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
							On cob.id_punto_servicio = hoja.id_punto_servicio
							Inner Join hoja_ruta ruta
							On hoja.id_hoja_ruta = ruta.id_hoja_ruta
							And ruta.fecha between @fechaini and @fechafinal
							And deta.valor is not Null
							And deta.valor > 0
							And deta.id_tipo_modalidad not in (7,9,10,11,12,8)
							And hoja.cantidad > 0
							And ruta.estado = 3			---- hoja de ruta cerrada
							And hoja.estado = 2			---- punto de servicio realizado

					Insert	into calculo_cobro_clientes1
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
							On cob.id_punto_servicio = pto.id_punto_servicio
							Inner join modelo_cobro_detalle deta
							On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
							On cob.id_punto_servicio = hoja.id_punto_servicio
							Inner Join hoja_ruta ruta
							On hoja.id_hoja_ruta = ruta.id_hoja_ruta
							And ruta.fecha between @fechaini and @fechafinal
							And deta.valor is not Null
							And deta.valor > 0
							And deta.id_tipo_modalidad not in (7,9,10,11,12)
							And hoja.peso > 0
							And deta.id_tipo_modalidad = 8
							And ruta.estado = 3			---- hoja de ruta cerrada
							And hoja.estado = 2			---- punto de servicio realizado
						
			        ---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					Insert	into calculo_cobro_clientes1
					Select	Distinct 0, 
							cal.id_punto_servicio,
							1, ---- cantidad
							0, ---- peso
							2, ---- estado
							@firstday, ---- primer dia mes anterior --- dato ficticio
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
					 		Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							fijo.id_tipo_modalidad, --- id_tipo_modalidad
							fijo.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							Null,
							Null,
							fijo.id_detalle_modelo_cobro, --- id_detalle_modelo_cobro
							@arriendo,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
						    From V_PUNTOS_UNICOS cal
							Inner Join V_ARRIENDO_COSTO_FIJO fijo
							On cal.id_punto_servicio = fijo.id_punto_servicio
							Where cal.fecha between @fechaini and @fechafinal

						  If @@error <> 0 
						  Begin					
							Rollback Transaction @PuntoServicio
							exec sp_log_calculo_cobro 'ERROR','Error Reporte Costos - Pago'
							RAISERROR ('Error Reporte Costos - Pago', 16, 1)
							Return
						  End	

		END

		Commit Transaction @PuntoServicio

		---- SE CARGAN LOS DATOS ANEXOS A LOS PUNTOS DE SERVICIO

		Begin Transaction @DatosAnexos

					---- SERVICIOS HOOK QUE SOLO TIENEN ASOCIADO MODALIDAD = 8
					---- PARA ESTOS CASOS MODALIDAD = 6 NO EXISTE
					Insert	into calculo_cobro_clientes1
					Select	hoja.id_hoja_ruta,
							cob.id_punto_servicio,
							hoja.cantidad,
							hoja.peso,
							hoja.estado,
							ruta.fecha,
							@periodo_cobro,
							Convert(int,@mescobro) As Monthis,
							Convert(int,@yearcobro) As Yearis,
		 					Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							deta.id_tipo_modalidad, --- id_tipo_modalidad
							deta.valor, -- VALOR desde modelo_detalle_cobro
							Null, --- tipo_modalidad
							Null,
							Null,
							0,
							Null,
							Null,
							deta.id_detalle_modelo_cobro,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null, ---- NUEVOS CAMPOS HACIA ABAJO
							Null,
							Null,
							Null,
							Null,
							Null,
							Null,
							Null
							From modelo_cobro cob
							Inner join puntos_servicio pto
							On cob.id_punto_servicio = pto.id_punto_servicio
							Inner join modelo_cobro_detalle deta
							On cob.id_modelo = deta.id_modelo_cobro
							Inner join hoja_ruta_detalle hoja
							On cob.id_punto_servicio = hoja.id_punto_servicio
							Inner Join hoja_ruta ruta
							On hoja.id_hoja_ruta = ruta.id_hoja_ruta
							And ruta.fecha between @fechaini and @fechafinal
							And deta.valor is not Null
							And deta.valor > 0
							And hoja.peso > 0
							And deta.id_tipo_modalidad = 8
							And ruta.estado = 3			---- hoja de ruta cerrada
							And hoja.estado = 2			---- punto de servicio realizado
							And hoja.id_punto_servicio not in
							(Select id_punto_servicio From V_PUNTOS_HOOK_CALCULADOS1)
							
		    Update T1
			Set T1.tipo_modalidad = T2.tipo_modalidad	
			from calculo_cobro_clientes1 T1 
			inner join tipo_modalidad T2
			On T1.id_tipo_modalidad = T2.id_tipo_modalidad

		--- DESCRIPCION DEL PUNTO DE SERVICIO
			Update T1
			Set T1.nombre_punto = T2.nombre_punto,
				T1.id_contrato = T2.id_contrato
			from calculo_cobro_clientes1 T1 
			inner join puntos_servicio T2
			On T1.id_punto_servicio = T2.id_punto_servicio		

		--- DESCRIPCION DEL PUNTO DE SERVICIO
		    Update T1
			Set T1.id_cliente = T2.id_cliente,
				 T1.id_sucursal = T2.id_sucursal,
				 T1.estado_contrato = T2.estado,
				 T1.id_modelo_cobro = T2.id_modelo_cobro
			from calculo_cobro_clientes1 T1 
			inner join contratos T2
			On T1.id_contrato = T2.id_contrato		
		 
		--- DESCRIPCION CLIENTE
		    Update T1
			Set T1.rut_cliente = T2.rut_cliente,
				 T1.giro_comercial = T2.giro_comercial,
				 T1.razon_social = T2.razon_social,
				 T1.nombre_fantasia = T2.nombre_fantasia
			from calculo_cobro_clientes1 T1 
			inner join clientes T2
			On T1.id_cliente = T2.id_cliente		

		--- DESCRIPCION SUCURSAL
		    Update T1
			Set T1.sucursal = T2.sucursal,
				 T1.descripcion = T2.descripcion,
				 T1.rut_sucursal = T2.rut_sucursal
			from calculo_cobro_clientes1 T1 
			inner join sucursales T2
			On T1.id_sucursal = T2.id_sucursal

		--- COMO SE CALCULA EL VALOR DEPENDIENDO DE tipo_modalidad
			Update calculo_cobro_clientes1
			Set como_se_calcula = 'CANTIDAD'
			Where id_tipo_modalidad in (5,6,7,9,10,11,12)

		---- LOS COSTOS FIJOS SE COBRAN UNA SOLA VEZ AL MES POR LO
		---- CUAL LA CANTIDAD SE SETEA A 1
			 Update calculo_cobro_clientes1
			 Set cantidad = 1,
				 peso = 0
			 Where id_tipo_modalidad in (5,7,9,10,11,12)		

			 Update calculo_cobro_clientes1
			 Set como_se_calcula = 'PESO',
			 cantidad = 0
		     Where id_tipo_modalidad in (8)

		-------------------------------------------------------------------
		-------------------------------------------------------------------
		----- CALCULO DEL VALOR BRUTO DEPENDIENDO DEL CAMPO como_se_calcula
			Update calculo_cobro_clientes1
			Set bruto = 0

			Update calculo_cobro_clientes1
			Set bruto = (isnull(valor,0) * isnull(cantidad,0))
			Where como_se_calcula = 'CANTIDAD'
			
			Update calculo_cobro_clientes1
			Set bruto = (isnull(valor,0))
			Where como_se_calcula = 'PESO'
			And id_tipo_modalidad = 8

		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes1 T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 8

		    Update T1
			Set  T1.valor = T2.valor,
				 T1.bruto = T2.valor
			from calculo_cobro_clientes1 T1 
			inner join V_VALOR_POR_RETIRO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.id_tipo_modalidad = 6

			Update calculo_cobro_clientes1
			Set bruto = (isnull(cantidad,0) * isnull(valor,0))
			Where como_se_calcula = 'CANTIDAD'
			And id_tipo_modalidad = 6

		----- FECHA QUE SE REALIZA EL CALCULO
			Update calculo_cobro_clientes1
			Set fechaCalculo = Getdate()

	     ---- ORDEN
		      Update T1
			  Set T1.Orden = T2.Orden
			  from calculo_cobro_clientes1 T1 
			  inner join hoja_ruta_detalle T2
			  On T1.id_hoja_ruta = T2.id_hoja_ruta
			  And T1.id_punto_servicio = T2.id_punto_servicio

			--- UPDATE CAMION CONDUCTOR
		    Update T1
			 Set T1.id_camion = T2.id_camion,
				 T1.camion = T2.patente,
				 T1.id_conductor = T2.id_conductor,
				 T1.conductor = T2.nombre_conductor
			from calculo_cobro_clientes1 T1 
			inner join V_CONDUCTOR_CAMION T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

			---- UPDATE VALOR ARRIENDO DEPENDIENDO DE LA CANTIDAD
			---- DE  EQUIPOS  EN  ARRIENDO  POR PUNTO DE SERVICIO
		    Update T1
			Set T1.bruto = (T2.cant_equipos * T1.valor)
			from calculo_cobro_clientes1 T1 
			inner join V_PUNTO_NUM_EQUIPOS T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			Where T1.id_tipo_modalidad = 7

         ---- ********************************************************************
		 ---- CALCULO DE COBRO CASOS ESPECIALES
		 ---- EMPRESA MOLYMET, PUNTOS 355, 356 , 358
		 Update calculo_cobro_clientes1
		 Set bruto = (isnull(valor,0) * isnull(peso,0))/1000
		 Where id_punto_servicio in (355, 356, 358)

		 ---- ********************************************************************

		 ---- UPDATE DEL CAMPO ORDEN CONCATENAR ID_HOJA_RUTA
		 ---- Y ORDEN
		 Update calculo_cobro_clientes1
		 Set Orden = Convert(int,Convert(Varchar,id_hoja_ruta) + Convert(Varchar,Orden))
		 Where Orden Is Not Null

		 Insert into calculo_cobro_clientes1
		 Select Distinct 0, 
		                 id_punto_servicio,
						 1, --- cantidad
						 0, --- peso
						 2, --- estado
						 @arriendo,
						 Fecha,
						 Monthis,
						 Yearis,
						 nombre_punto,
						 Null,
						 id_contrato,
						 id_cliente,
						 id_sucursal,
						 'Activo', ---- estado contrato
						 1,
						 rut_cliente,
						 razon_social,
						 Null,
						 Null,
						 sucursal,
						 descripcion,
						 Null,
						 55, ---- id_tipo_modalidad
						 0, ---- valor
						 'Fijo Mensual', ---- tipo_modalidad
						 'CANTIDAD',
						 0,
						 0,
						 Getdate(),
						 NULL,
						 1,
						 0,
						 'Fijo Mensual',
						 @arriendo,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null, ---- NUEVOS CAMPOS HACIA ABAJO
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null,
						 Null
						 From calculo_cobro_clientes1 Where id_tipo_modalidad = 5

            Update T1
			Set T1.valor = T2.valor,
			    T1.bruto = T2.valor
			from calculo_cobro_clientes1 T1 
			inner join V_PTO_FIJO_MENSUAL T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			Where id_tipo_modalidad = 55
			
	     --- EXCEPCIONES AL COBRO CAJA RECOLECTORA
	     --- ID_PUNTOS_SERVICIO 369,144,283,285
	     --- NO SE LES COBRA NADA A LA FECHA 01-01-2015
	     --- SIN EMBARGO DEBEN APARECER EN EL CALCULO
	     Update calculo_cobro_clientes1
			Set Valor = 0,
				Bruto = 0
			Where id_punto_servicio in (144,283,285,369)
			
	     ---- ORDENAMIENTO PARA LA SALIDA DEL ARCHIVO PDF
		 Update calculo_cobro_clientes1 Set sorting = 1, 
										   glosa = tipo_modalidad Where id_tipo_modalidad = 7

		 Update calculo_cobro_clientes1 Set sorting = 2,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 10

		 Update calculo_cobro_clientes1 Set sorting = 3,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 12

         Update calculo_cobro_clientes1 Set sorting = 4,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 11

		 Update calculo_cobro_clientes1 Set sorting = 5,
									       glosa = 'Retiro' Where id_tipo_modalidad = 9

		 Update calculo_cobro_clientes Set sorting = 6,
										   glosa = tipo_modalidad Where id_tipo_modalidad = 55

		 Update calculo_cobro_clientes1 Set sorting = 12,
										   glosa = 'Retiro' Where id_tipo_modalidad = 5

		 Update calculo_cobro_clientes1 Set sorting = 13,
										   glosa = 'Retiro' Where id_tipo_modalidad = 8

		 Update calculo_cobro_clientes1 Set sorting = 14,
										   glosa = 'Retiro' Where id_tipo_modalidad = 6

			---- UPDDATE DE VALOR Y BRUTO PARA PUNTOS CAJA RECOLECTORA
			---- ID_TIPO_MODALIDAD = 5
			---- NO SE COBRAN LOS RETIROS SOLO SE COBRA UN VALOR  FIJO
			---- MENSUAL								   
			Update T1
			Set T1.valor = 0,
			    T1.bruto = 0
			from calculo_cobro_clientes1 T1 
			inner join V_PTO_CAJA_RECOLECTORA3 T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And glosa = 'Retiro'

			If @@error <> 0 
			Begin					
				Rollback Transaction @DatosAnexos
				exec sp_log_calculo_cobro 'ERROR','Error Reporte Costos - Pago - Datos Anexos'
				RAISERROR ('Error Reporte Costos - Pago - Datos Anexos', 16, 1)
				Return
			End

		    Commit Transaction @DatosAnexos

			---- CARGA DE DATOS DE PUNTOS NO REALIZADOS

			Begin Transaction @PuntosNoRealizados

				---- ccc SIGNIFICA CALCULO COBRO CLIENTES

				---- PRIMERO DEBE HACERSE UN UPDATE DE LA TABLA calculo_cobro_clientes1
				---- SE  DEBEN  AGREGAR LOS VALORES DE LOS CAMPOS REFERIDOS AL REPORTE
				---- DE  MARGEN  DE  COBRO - FRANQUICIAS SOLO PARA AQUELLOS CUYA GLOSA
				---- ES RETIRO
				Update calculo_cobro_clientes1 Set fechacorta =  Convert(Varchar,day(fechaFull))
				Update calculo_cobro_clientes1 Set fechacorta =  '0' + fechacorta Where Len(fechacorta) = 1
				Update calculo_cobro_clientes1 Set fechacorta = fechacorta + '-' + dbo.MesNombre(Month(fechaFull))

				Update T1
				Set T1.sistema = 'Caja Recolectora'
				from calculo_cobro_clientes1 T1 
				inner join V_PTO_CAJA_RECOLECTORA3 T2
				On T1.id_punto_servicio = T2.id_punto_servicio
			
				Update T1
				Set T1.sistema = 'Hook'
				from calculo_cobro_clientes1 T1 
				inner join V_PUNTO_HOOK T2
				On T1.id_punto_servicio = T2.id_punto_servicio

				Update T1
				Set T1.sistema = 'Front Loader'
				from calculo_cobro_clientes1 T1 
				inner join V_PUNTO_FRONT_LOADER T2
				On T1.id_punto_servicio = T2.id_punto_servicio
			
				Update T1
				Set T1.franquiciado = T2.razon_social
				from calculo_cobro_clientes1 T1 
				inner join V_HOJA_RUTA_FRANQUICIADO T2
				On T1.id_hoja_ruta = T2.id_hoja_ruta
			
				Update calculo_cobro_clientes1 Set estadopunto = 'Realizado'

				Update T1
				Set T1.Relleno = T2.vertedero
				from calculo_cobro_clientes1 T1 
				inner join V_PUNTO_VERTEDERO T2
				On T1.id_punto_servicio = T2.id_punto_servicio

				Update calculo_cobro_clientes1
					Set pagofranquiciado = 0,
						margen = 0,
						margenporc = 0

				---- **************************************************************************
				---- CALCULO DE PAGO A FRANQUICIADO

				Update T1
				Set T1.pagofranquiciado = 1 * (T2.valor_tarifa)
				from calculo_cobro_clientes1 T1 
				inner join V_PUNTO_TARIFA T2
				On T1.id_punto_servicio = T2.id_punto_servicio
				And T1.glosa = 'Retiro' And  T1.sistema = 'Hook'
			
				---- PAGO A FRANQUICIADOS CASOS ESPECIALES
				---- PUNTOS DE SERVICIO 355, 356, 358
				---- SE LES PAGA PESO * TARIFA
				Update T1
				Set T1.pagofranquiciado = (Isnull(T1.peso,0) * (T1.pagofranquiciado))/1000
				from calculo_cobro_clientes1 T1 
				inner join V_PUNTOS_PAGO_PESO T2
				On T1.id_punto_servicio = T2.id_punto_servicio
			
				---- PAGO A FRANQUICIADOS CON TARIFA ESCALABLE
				OPEN franquiciados_pago_tramo
				fetch next from franquiciados_pago_tramo 
						into @id_punto_servicio,
				 			 @franquiciado,
							 @id_tipo_tarifa,
							 @Veces

				 while @@fetch_status = 0 
				 begin

					Set @Tarifa = - 9
					---- SE BUSCA LA TARIFA A PAGAR
					Set @Existe = (Select Isnull(Count(*),0) From tipo_tarifa_detalle
										  Where idTarifa = @id_tipo_tarifa
										  And rango_min <= @Veces
										  And rango_max >= @Veces)
					If @Existe > 0
					Begin

						Set @Tarifa = (Select Isnull(valor,0) From tipo_tarifa_detalle
													  Where idTarifa = @id_tipo_tarifa
													  And rango_min <= @Veces
													  And rango_max >= @Veces)

					End
														
						Update calculo_cobro_clientes1
						Set pagofranquiciado = 1 * @Tarifa
						Where id_punto_servicio = @id_punto_servicio
						And franquiciado = @franquiciado
						And glosa = 'Retiro'

					fetch next from franquiciados_pago_tramo 
							into @id_punto_servicio,
				 				 @franquiciado,
								 @id_tipo_tarifa,
								 @Veces
			 
				 end
			 
				 close franquiciados_pago_tramo 
				 deallocate franquiciados_pago_tramo

				---- **************************************************************************
						
				Update calculo_cobro_clientes1
					Set pagofranquiciado = 0
					Where sistema = 'Caja Recolectora' or sistema = 'Front Loader'

				Update calculo_cobro_clientes1
					Set margen = (Convert(decimal,bruto)- Convert(decimal,pagofranquiciado)),
						margenporc = ((Convert(decimal,bruto)- Convert(decimal,pagofranquiciado))/ Convert(decimal,bruto)) * 100
					Where glosa = 'Retiro' And sistema = 'Hook'
			
				---- DETERMINACION DE PUNTOS NO REALIZADOS EN HOJAS DE RUTA CERRADAS
				---- LLENADO DE LA TABLA ccc_puntos_no_realizados1
					Insert into ccc_puntos_no_realizados1
					Select det.id_hoja_ruta,
						   det.id_punto_servicio,
						   0,
						   0,
						   web.estado,
						   web.fecha,
						   Null As Fecha,
						   Month(web.fecha),
						   Year(web.fecha),
						   Null as nombre_punto,
						   det.estado,
						   Null as estado_punto,
						   0 As Valor,
						   0 As Bruto,
						   Getdate(),
						   0,
						   Null,
						   0,
						   Null,
						   Null,
						   Null,
						   Null,
						   Null,
						   null
						From hoja_ruta_detalle det
						inner join hoja_ruta web
						On det.id_hoja_ruta = web.id_hoja_ruta
						Inner Join V_CONTRATO_PUNTO_SERVICIO T2
						On det.id_punto_servicio = T2.id_punto_servicio
						And web.estado = 3			---- hoja de ruta cerrada
						And det.estado <> 2			---- 2 = punto de servicio realizado
						And web.fecha between @fechaini and @fechafinal
			
				Update ccc_puntos_no_realizados1 Set fechacorta =  Convert(Varchar,day(fechaFull))
				Update ccc_puntos_no_realizados1 Set fechacorta =  '0' + fechacorta Where Len(fechacorta) = 1
				Update ccc_puntos_no_realizados1 Set fechacorta = fechacorta + '-' + dbo.MesNombre(Month(fechaFull))

				Update T1
				Set T1.nombre_punto = T2.nombre_punto
				from ccc_puntos_no_realizados1 T1 
				inner join puntos_servicio T2
				On T1.id_punto_servicio = T2.id_punto_servicio

				Update T1
				Set T1.sistema = 'Caja Recolectora'
				from ccc_puntos_no_realizados1 T1 
				inner join V_PTO_CAJA_RECOLECTORA3 T2
				On T1.id_punto_servicio = T2.id_punto_servicio
			
				Update T1
				Set T1.sistema = 'Hook'
				from ccc_puntos_no_realizados1 T1 
				inner join V_PUNTO_HOOK T2
				On T1.id_punto_servicio = T2.id_punto_servicio

				Update T1
				Set T1.sistema = 'Front Loader'
				from ccc_puntos_no_realizados1 T1 
				inner join V_PUNTO_FRONT_LOADER T2
				On T1.id_punto_servicio = T2.id_punto_servicio
			
				Update T1
				Set T1.franquiciado = T2.razon_social
				from ccc_puntos_no_realizados1 T1 
				inner join V_HOJA_RUTA_FRANQUICIADO T2
				On T1.id_hoja_ruta = T2.id_hoja_ruta

				--- UPDATE CAMION CONDUCTOR
				Update T1
				Set T1.id_camion = T2.id_camion,
					T1.camion = T2.patente,
					T1.id_conductor = T2.id_conductor,
					T1.conductor = T2.nombre_conductor
				from ccc_puntos_no_realizados1 T1 
				inner join V_CONDUCTOR_CAMION T2
				On T1.id_hoja_ruta = T2.id_hoja_ruta

				Update T1
				Set T1.franquiciado = T2.razon_social
				from ccc_puntos_no_realizados1 T1 
				inner join V_HOJA_RUTA_FRANQUICIADO T2
				On T1.id_hoja_ruta = T2.id_hoja_ruta

				Update T1
				Set T1.estadopunto = T2.descripcion
				from ccc_puntos_no_realizados1 T1 
				inner join punto_estados T2
				On T1.puntoestado = T2.id_estado_punto
			 
				---- PAGO   A  FRANQUICIADOS  PUNTOS  NO  REALIZADOS
				---- LOS PUNTOS CUYO ESTADO = BLOQUEADO, NO ATENDIDO
				---- , MAL ESTIBADO SE PAGA CON EL VALOR DE  TRAMO A

				Set @TarifaA = (Select Isnull(valor_tarifa,0) from tipo_tarifa where tipo_tarifa = 'A') 

				Update ccc_puntos_no_realizados1 Set pagofranquiciado = 0

				Update T1
				Set T1.pagofranquiciado = @TarifaA
				from ccc_puntos_no_realizados1 T1 
				inner join punto_estados T2
				On T1.puntoestado = T2.estado
				And T2.estado = 5 ---	bloqueado

				Update T1
				Set T1.pagofranquiciado = @TarifaA
				from ccc_puntos_no_realizados1 T1 
				inner join punto_estados T2
				On T1.puntoestado = T2.estado
				And T2.estado = 6 ---	no atendido

				Update T1
				Set T1.pagofranquiciado = @TarifaA
				from ccc_puntos_no_realizados1 T1 
				inner join punto_estados T2
				On T1.puntoestado = T2.estado
				And T2.estado = 7 ---	mal estibado

			If @@error <> 0 
			Begin					
				Rollback Transaction @PuntosNoRealizados
				exec sp_log_calculo_cobro 'ERROR','Error Reporte Costos - Pago - Puntos No Realizados'
				RAISERROR ('Error Reporte Costos - Pago - Puntos No Realizados', 16, 1)
				Return
			End

			Commit Transaction @PuntosNoRealizados






GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_empresas_periodo]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_cobro_empresas_periodo] (

@Periodo Varchar(30)

)

AS	

declare @id_cliente int
declare @id_sucursal int
declare @id_contrato int
declare	@sucursal varchar(250)
declare @periodo_desde float
declare @periodo_hasta float
declare @inicio datetime
declare @inicio_periodo datetime
declare @inicio2 datetime
declare @fin datetime
declare @valor_mes varchar(3)
declare @year varchar(6)
declare @dia varchar(3)
declare @key varchar(60)
declare @mesarriendo varchar(30)
declare @mescobro varchar(30)
declare @yearcobro varchar(30)
declare @mes_anterior varchar(20)
declare @mes varchar(20)
declare @ini varchar(20)

Declare periodo_cobro_sucursal 
	CURSOR FOR SELECT id_cliente,
					  id_sucursal,
					  id_contrato,
					  periodo_desde,
					  periodo_hasta
				 From V_PERIODO_COBRO_POR_EMPRESA;

set nocount on

	----- DETERMINACION DEL PERIODO DE COBRO POR SUCURSAL-CONTRATO
	----- EL PERIODO ES VARIABLE
		---- DETERMINACION DEL PERIODO DE COBRO
    Set @mesarriendo = substring(@Periodo,1,2)
	Set @mescobro =  substring(@Periodo,1,2)
	Set @yearcobro =  substring(@Periodo,3,4)

	Delete From calculo_cobro_empresas

	OPEN periodo_cobro_sucursal
		fetch next from periodo_cobro_sucursal 
		into @id_cliente, @id_sucursal, @id_contrato,@periodo_desde,@periodo_hasta
			while @@fetch_status = 0 
				begin 
			       If @periodo_desde = 1
				   Begin
							If Len(@mescobro) = 1
							Begin
								Set @mescobro = '0' + @mescobro
							End
							Set @inicio = (@mescobro + '/' +  '01' + '/' + @yearcobro)
				   End
				   Else
				   Begin
						
						Set @valor_mes = (@mescobro)
						Set @year = @yearcobro

						If len(@valor_mes) = 1
						Begin
							Set @valor_mes = '0' + @valor_mes
						End

						Set @dia = (Convert(Varchar,@periodo_desde))
						If len(@dia) = 1
						Begin
							Set @dia = ('0' + @dia)
						End

						If @valor_mes = '01'
						Begin
							Set @valor_mes = '12'
							Set @year = Convert(Varchar,(Convert(int,@year) -1))
							Set @inicio = (Convert(datetime,(@dia + '/' + @valor_mes + '/' + @year),103))
						End
						Else
						Begin
							Set @mes = (Convert(int,@valor_mes) - 1)
							If len(@mes) = 1
							Begin
								Set @mes = '0' + @mes
							End
							Set @inicio = (Convert(datetime,(@dia + '/' + @mes + '/' + @yearcobro),103))
						End
				   End

				   Set @mesarriendo = substring(@Periodo,1,2)
				   Set @mescobro =  substring(@Periodo,1,2)
				   Set @yearcobro =  substring(@Periodo,3,4)

				   If @periodo_hasta = 1
				   Begin
				        ---- ULTIMO DIA DEL MES QUE SE BUSCA LA INFORMACION
						Set @inicio = (Convert(datetime,('01' + '/' + @mescobro + '/' + @yearcobro),103))
						Set @fin = (SELECT DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,@inicio)+1,0)))  
				   End
				   Else
				   Begin
							Set @valor_mes = @mescobro
							Set @year = @yearcobro
							Set @dia = (Convert(varchar,@periodo_hasta))
							If len(@dia) = 1
							Begin
								Set @dia = ('0' + @dia)
							End

								If len(@valor_mes) = 1
								Begin
									Set @valor_mes = ('0' + @valor_mes)
								End
								Set @fin = (Convert(datetime,(@dia + '/' + @valor_mes + '/' + @yearcobro),103))
				   End
				   ---- print convert(Varchar,@inicio) + ' ---- ' +  convert(Varchar,@fin)

				   				   				   Insert into calculo_cobro_empresas (id_cliente,
																	   id_sucursal,
																	   id_contrato,
																	   periodo,
																	   inicio,
																	   fin)
															   Values (@id_cliente,
																	   @id_sucursal,
																	   @id_contrato,
																	   '',
																	   @inicio,
																	   @fin)

	               FETCH NEXT FROM periodo_cobro_sucursal into @id_cliente, 
															   @id_sucursal, 
															   @id_contrato,
															   @periodo_desde,
															   @periodo_hasta

				End 
		
		close periodo_cobro_sucursal 
		deallocate periodo_cobro_sucursal












GO
/****** Object:  StoredProcedure [dbo].[sp_franquiciado_detalle]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_franquiciado_detalle] 

As	

Declare @id_punto_servicio int
Declare @franquiciado varchar(100)
Declare @id_tipo_tarifa int
Declare @Veces int
Declare @Existe int
Declare @Tarifa int
Declare @TarifaA int

Declare franquiciados_pago_tramo
	CURSOR FOR Select id_punto_servicio,
				 	  franquiciado,
					  id_tipo_tarifa,
				      Veces
					  From V_FRANQUICIADOS_ESCALABLES;

Set nocount on

Begin

            ---- ccc SIGNIFICA CALCULO COBRO CLIENTES
            Delete From ccc_puntos_no_realizados

			---- PRIMERO DEBE HACERSE UN UPDATE DE LA TABLA calculo_cobro_clientes
			---- SE  DEBEN  AGREGAR LOS VALORES DE LOS CAMPOS REFERIDOS AL REPORTE
			---- DE  MARGEN  DE  COBRO - FRANQUICIAS SOLO PARA AQUELLOS CUYA GLOSA
			---- ES RETIRO
			Update calculo_cobro_clientes Set fechacorta =  Convert(Varchar,day(fechaFull))
			Update calculo_cobro_clientes Set fechacorta =  '0' + fechacorta Where Len(fechacorta) = 1
			Update calculo_cobro_clientes Set fechacorta = fechacorta + '-' + dbo.MesNombre(Month(fechaFull))

		    Update T1
			Set T1.sistema = 'Caja Recolectora'
			from calculo_cobro_clientes T1 
			inner join V_PTO_CAJA_RECOLECTORA3 T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
		    Update T1
			Set T1.sistema = 'Hook'
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_HOOK T2
			On T1.id_punto_servicio = T2.id_punto_servicio

			Update T1
			Set T1.sistema = 'Front Loader'
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_FRONT_LOADER T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
			Update T1
			Set T1.franquiciado = T2.razon_social
			from calculo_cobro_clientes T1 
			inner join V_HOJA_RUTA_FRANQUICIADO T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta
			
			Update calculo_cobro_clientes Set estadopunto = 'Realizado'

		    Update T1
			Set T1.Relleno = T2.vertedero
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_VERTEDERO T2
			On T1.id_punto_servicio = T2.id_punto_servicio

			Update calculo_cobro_clientes
				Set pagofranquiciado = 0,
					margen = 0,
					margenporc = 0

			---- **************************************************************************
			---- CALCULO DE PAGO A FRANQUICIADO

			Update T1
			Set T1.pagofranquiciado = 1 * (T2.valor_tarifa)
			from calculo_cobro_clientes T1 
			inner join V_PUNTO_TARIFA T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			And T1.glosa = 'Retiro' And  T1.sistema = 'Hook'
			
			---- PAGO A FRANQUICIADOS CASOS ESPECIALES
			---- PUNTOS DE SERVICIO 355, 356, 358
			---- SE LES PAGA PESO * TARIFA
			Update T1
			Set T1.pagofranquiciado = (Isnull(T1.peso,0) * (T1.pagofranquiciado))/1000
			from calculo_cobro_clientes T1 
			inner join V_PUNTOS_PAGO_PESO T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
			---- PAGO A FRANQUICIADOS CON TARIFA ESCALABLE
			OPEN franquiciados_pago_tramo
			fetch next from franquiciados_pago_tramo 
					into @id_punto_servicio,
				 	     @franquiciado,
					     @id_tipo_tarifa,
				         @Veces

			 while @@fetch_status = 0 
			 begin

			    Set @Tarifa = - 9
			    ---- SE BUSCA LA TARIFA A PAGAR
				Set @Existe = (Select Isnull(Count(*),0) From tipo_tarifa_detalle
									  Where idTarifa = @id_tipo_tarifa
									  And rango_min <= @Veces
									  And rango_max >= @Veces)
				If @Existe > 0
				Begin

					Set @Tarifa = (Select Isnull(valor,0) From tipo_tarifa_detalle
												  Where idTarifa = @id_tipo_tarifa
												  And rango_min <= @Veces
												  And rango_max >= @Veces)

				End
									
					Update calculo_cobro_clientes
					Set pagofranquiciado = 1 * @Tarifa
                    Where id_punto_servicio = @id_punto_servicio
					And franquiciado = @franquiciado
					And glosa = 'Retiro'

				fetch next from franquiciados_pago_tramo 
						into @id_punto_servicio,
				 			 @franquiciado,
							 @id_tipo_tarifa,
							 @Veces
			 
			 end
			 
			 close franquiciados_pago_tramo 
			 deallocate franquiciados_pago_tramo

			---- **************************************************************************
						
			Update calculo_cobro_clientes
				Set pagofranquiciado = 0
				Where sistema = 'Caja Recolectora' or sistema = 'Front Loader'

			Update calculo_cobro_clientes
				Set margen = (Convert(decimal,bruto)- Convert(decimal,pagofranquiciado)),
				    margenporc = ((Convert(decimal,bruto)- Convert(decimal,pagofranquiciado))/ Convert(decimal,bruto)) * 100
				Where glosa = 'Retiro' And sistema = 'Hook'
			
			---- DETERMINACION DE PUNTOS NO REALIZADOS EN HOJAS DE RUTA CERRADAS
			---- LLENADO DE LA TABLA ccc_puntos_no_realizados
			    Insert into ccc_puntos_no_realizados
				Select det.id_hoja_ruta,
					   det.id_punto_servicio,
					   0,
					   0,
					   web.estado,
					   web.fecha,
					   Null As Fecha,
					   Month(web.fecha),
					   Year(web.fecha),
					   Null as nombre_punto,
					   det.estado,
					   Null as estado_punto,
					   0 As Valor,
					   0 As Bruto,
					   Getdate(),
					   0,
					   Null,
					   0,
					   Null,
					   Null,
					   Null,
					   Null,
					   Null,
					   Null
					From hoja_ruta_detalle det
					inner join hoja_ruta web
					On det.id_hoja_ruta = web.id_hoja_ruta
					Inner Join V_CONTRATO_PUNTO_SERVICIO T2
					On det.id_punto_servicio = T2.id_punto_servicio
					And web.estado = 3			---- hoja de ruta cerrada
					And det.estado <> 2			---- 2 = punto de servicio realizado
					And web.fecha between T2.inicio and T2.fin
			
			Update ccc_puntos_no_realizados Set fechacorta =  Convert(Varchar,day(fechaFull))
			Update ccc_puntos_no_realizados Set fechacorta =  '0' + fechacorta Where Len(fechacorta) = 1
			Update ccc_puntos_no_realizados Set fechacorta = fechacorta + '-' + dbo.MesNombre(Month(fechaFull))

		    Update T1
			Set T1.nombre_punto = T2.nombre_punto
			from ccc_puntos_no_realizados T1 
			inner join puntos_servicio T2
			On T1.id_punto_servicio = T2.id_punto_servicio

		    Update T1
			Set T1.sistema = 'Caja Recolectora'
			from ccc_puntos_no_realizados T1 
			inner join V_PTO_CAJA_RECOLECTORA3 T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
		    Update T1
			Set T1.sistema = 'Hook'
			from ccc_puntos_no_realizados T1 
			inner join V_PUNTO_HOOK T2
			On T1.id_punto_servicio = T2.id_punto_servicio

			Update T1
			Set T1.sistema = 'Front Loader'
			from ccc_puntos_no_realizados T1 
			inner join V_PUNTO_FRONT_LOADER T2
			On T1.id_punto_servicio = T2.id_punto_servicio
			
			Update T1
			Set T1.franquiciado = T2.razon_social
			from ccc_puntos_no_realizados T1 
			inner join V_HOJA_RUTA_FRANQUICIADO T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

		    --- UPDATE CAMION CONDUCTOR
		    Update T1
			Set T1.id_camion = T2.id_camion,
				T1.camion = T2.patente,
				T1.id_conductor = T2.id_conductor,
				T1.conductor = T2.nombre_conductor
			from ccc_puntos_no_realizados T1 
			inner join V_CONDUCTOR_CAMION T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

			Update T1
			Set T1.franquiciado = T2.razon_social
			from ccc_puntos_no_realizados T1 
			inner join V_HOJA_RUTA_FRANQUICIADO T2
			On T1.id_hoja_ruta = T2.id_hoja_ruta

			Update T1
			Set T1.estadopunto = T2.descripcion
			from ccc_puntos_no_realizados T1 
			inner join punto_estados T2
			On T1.puntoestado = T2.id_estado_punto
			
			---- PAGO   A  FRANQUICIADOS  PUNTOS  NO  REALIZADOS
			---- LOS PUNTOS CUYO ESTADO = BLOQUEADO, NO ATENDIDO
			---- , MAL ESTIBADO SE PAGA CON EL VALOR DE  TRAMO A

			Set @TarifaA = (Select Isnull(valor_tarifa,0) from tipo_tarifa where tipo_tarifa = 'A') 

			Update ccc_puntos_no_realizados Set pagofranquiciado = 0

			Update T1
			Set T1.pagofranquiciado = @TarifaA
			from ccc_puntos_no_realizados T1 
			inner join punto_estados T2
			On T1.puntoestado = T2.estado
			And T2.estado = 5 ---	bloqueado

			Update T1
			Set T1.pagofranquiciado = @TarifaA
			from ccc_puntos_no_realizados T1 
			inner join punto_estados T2
			On T1.puntoestado = T2.estado
			And T2.estado = 6 ---	no atendido

			Update T1
			Set T1.pagofranquiciado = @TarifaA
			from ccc_puntos_no_realizados T1 
			inner join punto_estados T2
			On T1.puntoestado = T2.estado
			And T2.estado = 7 ---	mal estibado
							
End






GO
/****** Object:  StoredProcedure [dbo].[sp_listar_cobroclientes_franquiciados_tabla]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROCEDURE [dbo].[sp_listar_cobroclientes_franquiciados_tabla](
@fechaini varchar(20),
@fechafinal varchar(20)
)
AS
/**
set @fechaini='2014-12-01'
set @fechafinal='2014-12-31'
**/
BEGIN

Delete  from calculo_cobro_disposicion_franquiciado
Delete  from calculo_cobro_clientes_franquiciado

Insert into calculo_cobro_clientes_franquiciado
Select    C.id_cliente,
          C.rut_cliente,
          C.razon_social,
          COUNT( C.id_punto_servicio),   --- CANTIDAD
          Sum(C.BRUTO) As Valor,
          0, 
          0,
          0,
          0,
          C.id_punto_servicio
          From calculo_cobro_clientes  C
          inner join dbo.contratos CO on CO.id_contrato=C.id_contrato
          Where C.fechaFull between @fechaini and @fechafinal
		  Group By  C.id_cliente,
                    C.rut_cliente,
                    C.razon_social,
                    C.id_sucursal,
                    C.sucursal,
                    C.id_punto_servicio

Update T1 Set T1.cantidad=T1.cantidad-T2.Numero_No_Retiros
FROM calculo_cobro_clientes_franquiciado T1
 inner join (Select C.id_cliente,
				    C.id_punto_servicio,
				    Count(id_punto_servicio) As Numero_No_Retiros
				    From calculo_cobro_clientes C
				    Where glosa <> 'Retiro'
				    AND C.fechaFull  between @fechaini and @fechafinal
				    Group By id_cliente,
						     id_punto_servicio) T2
 On T1.id_cliente = T2.id_cliente

 Update T1 
 set T1.pagofranquiciado = T2.Sumar
 from dbo.calculo_cobro_clientes_franquiciado  T1
 inner join (select V.id_cliente, SUM(ISNULL(MD.valor,0) * ISNULL(D.cantidad,0))as Sumar ,D.id_punto_servicio
		from dbo.franquiciado F 
		inner join camiones C  on F.id_franquiciado =C.id_franquiciado
		inner join hoja_ruta H on H.id_camion=C.id_camion
		inner join hoja_ruta_detalle D on D.id_hoja_ruta =H.id_hoja_ruta
		inner join puntos_servicio P on P.id_punto_servicio=D.id_punto_servicio
		inner join contratos T on T.id_contrato =P.id_contrato
		inner join clientes V  on V.id_cliente=T.id_cliente
		inner join tipo_servicio S on S.id_tipo_servicio = D.id_tipo_servicio
		inner join modelo_cobro M  on M.id_punto_servicio =P.id_punto_servicio
		inner join modelo_cobro_detalle MD on MD.id_modelo_cobro=M.id_modelo
		Where   H.fecha  between @fechaini and @fechafinal
		and  H.fecha  is not null
		and  V.id_cliente  is not null
		Group by V.id_cliente,
				 T.id_sucursal,
				 D.id_punto_servicio) T2
 on T1.id_cliente = T2.id_cliente

 Update T1 
 set T1.ValorPagarApunto = T2.valor_tarifa 
 from dbo.calculo_cobro_clientes_franquiciado  T1
 inner join V_listar_valor_tonelada T2
 on T1.id_cliente = T2.id_cliente
 
 Update dbo.calculo_cobro_clientes_franquiciado  
 Set pagofranquiciado = (cantidad * ValorPagarApunto), 
     ValorTotal = 0
  
 Insert into calculo_cobro_disposicion_franquiciado
             Select  cal.id_cliente,
                     cal.rut_cliente,
                     Sum(Isnull(cal.Peso,0)) As Disposicion,
                     0,
                     0 As Valor,
                     0,
                     cal.id_punto_servicio             
                     From dbo.calculo_cobro_clientes cal
                     inner join dbo.contratos con 
                     on con.id_contrato = cal.id_contrato
                     Where cal.fechaFull between @fechaini and @fechafinal                     
                     Group By cal.id_cliente,
                               cal.rut_cliente,
                               cal.Fecha,
                               cal.id_contrato,
                               cal.id_punto_servicio,
                               glosa
                     Having glosa = 'Retiro'
                      
 
 Update T1 
 set T1.CobroTonelada = T2.valor 
 from dbo.calculo_cobro_disposicion_franquiciado  T1
 inner join V_PTO_SERVICIO_COBRO_TONELADA T2
 on T1.id_punto_servicio = T2.id_punto_servicio
 
Update dbo.calculo_cobro_disposicion_franquiciado 
Set valor = (Disposicion * CobroTonelada) /1000,
    ValorTotal = 0

Update dbo.calculo_cobro_disposicion_franquiciado 
Set ValorTotal = valor 

Update T1 
Set T1.ValorTotal = (T1.valor + Isnull(T2.Valor,0))
From dbo.calculo_cobro_clientes_franquiciado  T1
Left join calculo_cobro_disposicion_franquiciado T2
On T1.id_punto_servicio = T2.id_punto_servicio

END







GO
/****** Object:  StoredProcedure [dbo].[sp_log_calculo_cobro]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_log_calculo_cobro] (

 @exito varchar(20),
 @descripcion varchar(200))

As
	set nocount on

	Insert into log_calculo_cobro_clientes 
             (exito,descripcion,fecha) Values
			 (@exito,@descripcion,Getdate())












GO
/****** Object:  StoredProcedure [dbo].[sp_modelo_cobro_crear]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[sp_modelo_cobro_crear] (
 @id_modelo int,
 @modelo varchar(200),
 @id_tipo_modelo int,
 @id_punto_servicio int,
 @id_new_created int out
)
As

declare @existe int

	set nocount on

	If @id_modelo = 0 
	Begin

	    Set @existe = (Select Isnull(count(*),0) From modelo_cobro Where modelo = @modelo)

		If @existe > 0
		Begin
			Select @id_new_created = -777		
			RAISERROR ('Modelo existente', 16, 1)
			Return
		End
		Else
		Begin
			--- INSERT NUEVO MODELO DE COBRO
			Insert into modelo_cobro (modelo,
									  id_tipo_modelo,
									  id_punto_servicio)
							  Values (@modelo,
									  @id_tipo_modelo,
									  @id_punto_servicio);

						If @@error <> 0 
						Begin
							Select @id_new_created = -999		
							RAISERROR ('Error Modelo de Cobro Crear', 16, 1)
							Return
						End
						Select @id_new_created = SCOPE_IDENTITY();
		End

	End
	Else
	Begin

	    --- UPDATE MODELO DE COBRO EXISTENTE
		Update modelo_cobro Set id_tipo_modelo = @id_tipo_modelo
				Where id_modelo = @id_modelo;

				If @@error <> 0 
				Begin
					Select @id_new_created = -999			
					RAISERROR ('Error Modelo de Cobro Modificación', 16, 1)
					Return
				End

				Select @id_new_created = @id_modelo;
	End







GO
/****** Object:  StoredProcedure [dbo].[sp_reversa_cobro_periodo]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[sp_reversa_cobro_periodo]
	@periodo VARCHAR (6)
AS
BEGIN
		delete from calculo_cobro_empresas
		delete from calculo_cobro_clientes
		delete from calculo_cobro_disposicion
		delete from cobro_cliente_total

		delete from calculo_realizado Where Fecha = @periodo And Cerrado = 1
		delete from cobro_clientes_cerrado where Fecha = @periodo
		delete from cobro_disposicion_cerrado where Fecha = @periodo
		delete from cliente_total_cerrado where Fecha = @periodo
END





GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_DetalleGuia]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Valentys
-- Create date: 19-08-2015
-- Description:	obtiene el detalle de una guia
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_DetalleGuia]
	@GUIA_TOKEN UNIQUEIDENTIFIER
AS
BEGIN	  
	
	  SELECT PUNT.nombre_punto
		   , DGIA.DGIA_CODIGO
		   , RESI.residuo
		   , VERT.vertedero
	  FROM TB_GIA_Guia			GUIA
INNER JOIN TB_GIA_GUIADETALLE   DGIA ON DGIA.DGIA_GUIA_CODIGO	= GUIA.GUIA_CODIGO
INNER JOIN hoja_ruta_detalle	HDET ON HDET.id_hoja_ruta		= DGIA.DGIA_ID_HOJA_RUTA
									AND HDET.Orden				= DGIA.DGIA_ORDEN
INNER JOIN puntos_servicio		PUNT ON HDET.id_punto_servicio	= PUNT.id_punto_servicio
INNER JOIN vertederos			VERT ON VERT.id_vertedero		= PUNT.id_vertedero
INNER JOIN residuos				resi ON resi.id_residuo			= punt.id_residuo
	 WHERE GUIA.GUIA_TOKEN = @GUIA_TOKEN

END
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_DisposicionTemporalDisponible]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Valentys
-- Create date: 19-08-2015
-- Description:	obtiene los residuos que estan en disposición temporal y no tienen una guia de despacho asociada.
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_DisposicionTemporalDisponible]
	@unidad int
AS
BEGIN	  
	

	  SELECT PUNT.nombre_punto
		   , HDET.id_hoja_ruta
		   , HDET.Orden
		   , RESI.residuo
		   , VERT.vertedero
		FROM hoja_ruta_detalle HDET
  INNER JOIN puntos_servicio   PUNT ON HDET.id_punto_servicio	= PUNT.id_punto_servicio
  INNER JOIN contratos		   CONT ON CONT.id_contrato			= PUNT.id_contrato
  INNER JOIN vertederos		   VERT ON VERT.id_vertedero		= HDET.relleno
  INNER JOIN residuos		   resi ON resi.id_residuo = punt.id_residuo
	   WHERE VERT.disposicion_final = 0
		 AND HDET.estado = 2
		 AND CONT.id_unidad = @unidad
		 AND HDET.id_hoja_ruta NOT IN (SELECT DET.DGIA_ID_HOJA_RUTA
										 FROM TB_GIA_GUIADETALLE DET
										WHERE DET.DGIA_ORDEN = HDET.Orden)

END
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_GuiaDespacho]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Valentys
-- Create date: 17-08-2015
-- Description:	obtiene las guias de despacho
-- =============================================
-- exec SP_SEL_GuiaDespacho null,null,null,null
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_GuiaDespacho]
	@patente VARCHAR(10) = null,
	@identificador INT = null,
	@fecha DATETIME = null,
	@CodigoEstado INT = null,
	@idunidad int
AS
BEGIN	
	SET NOCOUNT ON;

	SELECT GUIA.GUIA_TOKEN
		 , ESTA.EGIA_NOMBRE
		 , GUIA.GUIA_FECHA
		 , GUIA.GUIA_FECHALLEGADA
		 , GUIA.GUIA_IDENTIFICADOR
		 , GUIA.GUIA_NOMBRE
		 , GUIA.GUIA_PATENTE
		 , VERT.vertedero
	  FROM TB_GIA_GUIA		 GUIA
INNER JOIN TB_GIA_ESTADOGUIA ESTA ON ESTA.EGIA_CODIGO = GUIA.GUIA_EGIA_CODIGO
INNER JOIN vertederos        VERT ON VERT.id_vertedero = GUIA.GUIA_ID_VERTEDERO
	 WHERE GUIA.GUIA_ID_UNIDAD = @idunidad
	   AND GUIA.GUIA_PATENTE = ISNULL(@patente, GUIA.GUIA_PATENTE)
	   AND GUIA.GUIA_IDENTIFICADOR = ISNULL(@identificador, GUIA.GUIA_IDENTIFICADOR)
	   AND GUIA.GUIA_FECHA = ISNULL(@fecha, GUIA.GUIA_FECHA)
	   AND GUIA.GUIA_EGIA_CODIGO = ISNULL(@CodigoEstado, GUIA.GUIA_EGIA_CODIGO)
    
END

GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_HistorialPeriodo]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		valentys
-- Create date: 08-07-2015
-- Description:	obtiene el historial de reajuste.
-- =============================================
-- exec [SP_SEL_VerificarReajeste] 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_HistorialPeriodo](
	@id_unidad INT
)AS
BEGIN
	SET NOCOUNT ON;
	 
	 SELECT RESU.HMCD_MES
		  , RESU.HMCD_ANIO
	   FROM TB_HIS_MODELO_COBRO_DETALLE RESU with(nolock)
 INNER JOIN modelo_cobro_detalle		MODD with(nolock) ON RESU.HMCD_ID_DETALLE_MODELO_COBRO	= MODD.id_detalle_modelo_cobro
 INNER JOIN modelo_cobro				MODE with(nolock) ON MODD.id_modelo_cobro				= MODE.id_modelo
 INNER JOIN puntos_servicio				PUNT with(nolock) ON PUNT.id_punto_servicio				= MODE.id_punto_servicio
 INNER JOIN contratos					CONT with(nolock) ON PUNT.id_contrato					= CONT.id_contrato
      WHERE CONT.id_unidad = @id_unidad
  GROUP BY RESU.HMCD_MES
		 , RESU.HMCD_ANIO

END



GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_HistorialReajuste]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		valentys
-- Create date: 08-07-2015
-- Description:	obtiene el historial de reajuste.
-- =============================================
-- exec [SP_SEL_VerificarReajeste] 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_HistorialReajuste](
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL,
	@id_unidad INT
)AS
BEGIN
	SET NOCOUNT ON;
	 
	 SELECT MODD.id_modelo_cobro
		  , TMOD.tipo_modelo
		  , MODD.id_detalle_modelo_cobro
		  , MODA.tipo_modalidad
		  , CONT.id_cliente
		  , CLIE.razon_social
		  , CONT.id_contrato
		  , SUCU.descripcion
		  , RESU.HMCD_VALOR
		  , RESU.HMCD_REAJUSTE 		  
		  , RESU.HMCD_MES
		  , RESU.HMCD_ANIO
		  , RESU.HMCD_FECHA
	   FROM TB_HIS_MODELO_COBRO_DETALLE RESU with(nolock)
 INNER JOIN modelo_cobro_detalle		MODD with(nolock) ON RESU.HMCD_ID_DETALLE_MODELO_COBRO	= MODD.id_detalle_modelo_cobro
 INNER JOIN modelo_cobro				MODE with(nolock) ON MODD.id_modelo_cobro				= MODE.id_modelo
 INNER JOIN puntos_servicio				PUNT with(nolock) ON PUNT.id_punto_servicio				= MODE.id_punto_servicio
 INNER JOIN contratos					CONT with(nolock) ON PUNT.id_contrato					= CONT.id_contrato
 INNER JOIN clientes					CLIE with(nolock) ON CONT.id_cliente					= CLIE.id_cliente
 INNER JOIN tipo_modelo					TMOD with(nolock) ON MODE.id_tipo_modelo				= TMOD.id_tipo_modelo
 INNER JOIN tipo_modalidad				MODA with(nolock) ON MODD.id_tipo_modalidad				= MODA.id_tipo_modalidad
 INNER JOIN sucursales					SUCU with(nolock) ON SUCU.id_sucursal					= CONT.id_sucursal
      WHERE RESU.HMCD_MES = isnull(@MES_REAJUSTE, RESU.HMCD_MES)
	    and RESU.HMCD_ANIO = isnull(@ANIO_REAJUSTE, RESU.HMCD_ANIO)
		and CONT.id_unidad = @id_unidad

END



GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_ListaServicio]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jonathan Ancán
-- Create date: 13-08-2015
-- Description:	Obtiene Lista de Servicios
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_ListaServicio]
	@FechaDesde DATETIME
   ,@fechaHasta DATETIME
   ,@codigo_unidad int
AS
BEGIN
	SET NOCOUNT ON;

        select hoja.fecha
			 , hoja.id_hoja_ruta folio
			 , tmod.tipo_modelo servicio
			 , cond.nombre_conductor conductor
			 , cami.patente 
			 , equi.tipo_equipo tipoContenedor
			 , Cast(porc.PORL_PORCERTAJELLENADO as varchar(4)) + ' %' nivelLlenado
			 , equi.capacidad_equipo volumen
			 , resi.residuo residuo
			 , sucu.sucursal origen
			 , vert.vertedero destino
			 , CASE WHEN deta.peso_calculado = 1
					THEN 'Calculo por Densidad'
					ELSE 'Pesaje Calculado'
			   END tipoPesaje
			 , CASE WHEN deta.peso_calculado = 1
					THEN null
					ELSE CASE WHEN porc.PORL_PORCERTAJELLENADO > 0
							  THEN deta.peso / ((CONVERT(float, porc.PORL_PORCERTAJELLENADO) / 100) * equi.capacidad_equipo)
							  ELSE 0
						 END
			   END pesajeDirecto
			 , CASE WHEN deta.peso_calculado = 1
					THEN resi.densidad
					ELSE deta.peso / equi.capacidad_equipo
			   END densidad
			 , CONVERT(float, isnull(deta.peso, 0.0)) peso
		  from hoja_ruta				hoja
	inner join hoja_ruta_detalle		deta on hoja.id_hoja_ruta = deta.id_hoja_ruta
	inner join puntos_servicio			punt on punt.id_punto_servicio = deta.id_punto_servicio
	inner join contratos				cont on cont.id_contrato = punt.id_contrato
	inner join sucursales				sucu on sucu.id_sucursal = cont.id_sucursal
	 left join modelo_cobro				mode on mode.id_punto_servicio = punt.id_punto_servicio
	 left join tipo_modelo				tmod on tmod.id_tipo_modelo = mode.id_tipo_modelo
	inner join residuos					resi on resi.id_residuo = punt.id_residuo
	inner join camiones					cami on cami.id_camion = hoja.id_camion
	inner join conductores				cond on cond.id_conductor = hoja.id_conductor
	inner join equipos					equi on equi.id_equipo = punt.id_equipo
	 left join TB_SGS_PORCENTAJELLENADO porc on porc.PORL_CODIGO = deta.PORL_CODIGO
	inner join vertederos				vert on vert.id_vertedero = deta.relleno
		 WHERE deta.estado = 2
		   and cont.id_unidad = @codigo_unidad
		   and (hoja.fecha >= @FechaDesde
		   and hoja.fecha <= @fechaHasta)
END

GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_UltimoReajustePtoServicio]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		valentys
-- Create date: 14-07-2015
-- Description:	Obtiene el ultimo reajuste de un puento de servicio.
-- =============================================
-- SP_SEL_UltimoReajustePtoServicio 7, 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_UltimoReajustePtoServicio](
	@ID_PUNTO_SERVICIO INT,
	@MES INT,
	@ANIO INT
)AS
BEGIN
	SET NOCOUNT ON;
	 
	 SELECT RESU.HMCD_REAJUSTE	      
		  , RESU.HMCD_MES
		  , RESU.HMCD_ANIO
	   FROM TB_HIS_MODELO_COBRO_DETALLE RESU with(nolock)
 INNER JOIN modelo_cobro_detalle		MODD with(nolock) ON RESU.HMCD_ID_DETALLE_MODELO_COBRO	= MODD.id_detalle_modelo_cobro
														 AND (convert(varchar(4), RESU.HMCD_ANIO) + right('0'+convert(varchar(2), RESU.HMCD_MES), 2)) <= (convert(varchar(4), @ANIO) + right('0'+convert(varchar(2), @MES), 2))
 INNER JOIN modelo_cobro				MODE with(nolock) ON MODD.id_modelo_cobro				= MODE.id_modelo
 INNER JOIN puntos_servicio				PUNT with(nolock) ON PUNT.id_punto_servicio				= MODE.id_punto_servicio
      WHERE PUNT.id_punto_servicio = @ID_PUNTO_SERVICIO
	    and (convert(varchar(4), RESU.HMCD_ANIO) + right('0'+convert(varchar(2), RESU.HMCD_MES), 2)) = (SELECT MAX((convert(varchar(4), HMCD_ANIO)+ right('0'+convert(varchar(2), HMCD_MES), 2)))
																											   FROM TB_HIS_MODELO_COBRO_DETALLE
																											  WHERE HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro)
   group by RESU.HMCD_REAJUSTE	      
		  , RESU.HMCD_MES
		  , RESU.HMCD_ANIO

END



GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_VerificarReajeste]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		valentys
-- Create date: 07-07-2015
-- Description:	obtiene los modelos de cobros que deben ser reajustados.
-- =============================================
-- exec [SP_SEL_VerificarReajeste] 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_SEL_VerificarReajeste](
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL,
	@ID_UNIDAD INT NULL
)AS
BEGIN
	SET NOCOUNT ON;

	  -- DECLARE @CODIGOS_CLIENTES VARCHAR(MAX) = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'
	   DECLARE --@ID_UNIDAD INT = 2
	           @ID_CLIENTE INT
	         , @VALOR float
			 , @ID_MODELO_COBRO INT
			 , @ID_DETALLE_MODELO_COBRO INT
			 , @ID_CONTRATO INT
	         , @MES_ULTIMO_REAJUSTE INT = 1
			 , @ANIO_ULTIMO_REAJUSTE INT
			 , @REAJ_CODIGO int 
			 , @CANTIDAD_CONFIGURACION int 
			 , @VALOR_REAJUSTE float
			 , @TOKEN_PROCESO UNIQUEIDENTIFIER = NEWID()

	   				--Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar
				--BUSCA SOLO LOS CLIENTES A QUIEN SE DEBE APLICAR EL REAJUSTE
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
				 WHERE CONT.id_unidad  = @ID_UNIDAD
				   AND REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0
				   AND MODD.id_detalle_modelo_cobro NOT IN (SELECT HIST.HMCD_ID_DETALLE_MODELO_COBRO
													  FROM TB_HIS_MODELO_COBRO_DETALLE HIST
													 WHERE HIST.HMCD_MES = @MES_REAJUSTE
													   AND HIST.HMCD_ANIO = @ANIO_REAJUSTE);
			


		  --Abre el cursor y recorro a los clientes.
		  OPEN CR_id_clientes
		 FETCH NEXT FROM CR_id_clientes
		  INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
		 WHILE @@FETCH_STATUS = 0
			 BEGIN

					IF (@CANTIDAD_CONFIGURACION = 1)
						BEGIN
							--SI LA CONFIGURACIÓN ES UNA SOLA ES PORQUE ES ANUAL Y TOMA DESDE EL MISMO MES PERO DEL AÑO PASADO
							SELECT @MES_ULTIMO_REAJUSTE  = REDE.READ_MES 
								 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1 
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
						END
					ELSE
						BEGIN
							
							SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
							     , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
							   AND REDE.READ_MES < ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))

							   IF (@MES_ULTIMO_REAJUSTE IS NULL)
								   BEGIN
										SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
											 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1
										  FROM contratos			   CONT with(nolock)
									INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
									INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
										 WHERE CONT.id_contrato = @ID_CONTRATO
								   END

						END

--------------------------------------------------------------------------------------------------------------------------------------------
--codigo comentado a raiz que el ipc utilizado en abril fue de 0.6 y la forma del procedimiento da 0.7
--------------------------------------------------------------------------------------------------------------------------------------------
					--CALCULA EL VALOR DE REAJUSTE
					SET @VALOR_REAJUSTE = (	SELECT SUM(INDV.INDV_VALOR)
											  FROM TB_SGS_INDICADOR          INDI with(nolock) 
										INNER JOIN TB_SGS_INDICADOR_VALOR    INDV with(nolock) ON INDI.INDI_CODIGO = INDV.INDV_INDI_CODIGO
										INNER JOIN TB_SGS_REAJUSTE_INDICADOR REIN with(nolock) ON REIN.REIN_INDI_CODIGO = INDI.INDI_CODIGO
											 WHERE Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') >= Convert(varchar(4), @ANIO_ULTIMO_REAJUSTE) + REPLACE(STR(@MES_ULTIMO_REAJUSTE, 2), SPACE(1), '0')
											   AND Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') <  Convert(varchar(4), ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))) +  Convert(varchar(2), REPLACE(STR(@MES_REAJUSTE, 2), SPACE(1), '0'))
											   AND REIN.REIN_REAJ_CODIGO = @REAJ_CODIGO)
--------------------------------------------------------------------------------------------------------------------------------------------
					--SET @VALOR_REAJUSTE = 0.6
--------------------------------------------------------------------------------------------------------------------------------------------
					IF (@VALOR_REAJUSTE > 0)
					BEGIN
						--INSERTO EL VALOR DEL DETALLE DE MODELO ACTUAL A LA TABLA HISTORICOS.
						INSERT TB_AUX_REAJUSTE(	[AUXI_ID_CONTRATO]	,
												[AUXI_ULTIMO_MES_REAJUSTE],
												[AUXI_FACTOR_REAJUSTE],
												[AUXI_ID_DETALLE_MODELO_COBRO],
												[AUXI_VALOR_ACTUAL],
												[AUXI_VALOR_NUEVO],
												[AUXI_TOKEN_PROCESO],
												[AUXI_MES],
												[AUXI_ANIO])
								   values(	@ID_CONTRATO
										 ,  convert(varchar(10) ,@MES_ULTIMO_REAJUSTE) + '-' + convert(varchar(10) , @ANIO_ULTIMO_REAJUSTE)
										 ,  @VALOR_REAJUSTE
										 ,	@ID_DETALLE_MODELO_COBRO
										 ,	@VALOR
										 ,	round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100),0)
										 ,  @TOKEN_PROCESO
										 ,  @MES_REAJUSTE
										 ,  @ANIO_REAJUSTE)
					
					END
					--Próximo cliente
					FETCH NEXT FROM CR_id_clientes
					INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
			   END 
		 CLOSE CR_id_clientes;
	DEALLOCATE CR_id_clientes;
    



	 SELECT MODD.id_modelo_cobro
		  , TMOD.tipo_modelo
		  , MODD.id_detalle_modelo_cobro
		  , MODA.tipo_modalidad
		  , CONT.id_cliente
		  , CLIE.razon_social
		  , CONT.id_contrato
		  , SUCU.descripcion
		  , RESU.AUXI_VALOR_ACTUAL
		  , RESU.AUXI_VALOR_NUEVO
		  , RESU.AUXI_FACTOR_REAJUSTE 
		  , (SELECT MAX(HMOD.HMCD_FECHA)
		       FROM TB_HIS_MODELO_COBRO_DETALLE HMOD
			  WHERE HMOD.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro) Fecha_Ultimo_Reajuste
		  , RESU.AUXI_MES
		  , RESU.AUXI_ANIO
	   FROM TB_AUX_REAJUSTE         RESU with(nolock)
 INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON RESU.AUXI_ID_DETALLE_MODELO_COBRO	= MODD.id_detalle_modelo_cobro
 INNER JOIN modelo_cobro			MODE with(nolock) ON MODD.id_modelo_cobro				= MODE.id_modelo
 INNER JOIN puntos_servicio		    PUNT with(nolock) ON PUNT.id_punto_servicio				= MODE.id_punto_servicio
 INNER JOIN contratos			    CONT with(nolock) ON PUNT.id_contrato					= CONT.id_contrato
 INNER JOIN clientes				CLIE with(nolock) ON CONT.id_cliente					= CLIE.id_cliente
 INNER JOIN tipo_modelo			    TMOD with(nolock) ON MODE.id_tipo_modelo				= TMOD.id_tipo_modelo
 INNER JOIN tipo_modalidad		    MODA with(nolock) ON MODD.id_tipo_modalidad				= MODA.id_tipo_modalidad
 INNER JOIN sucursales			    SUCU with(nolock) ON SUCU.id_sucursal					= CONT.id_sucursal
      WHERE CONT.id_unidad = @ID_UNIDAD
	

	DELETE FROM TB_AUX_REAJUSTE WHERE AUXI_TOKEN_PROCESO = @TOKEN_PROCESO
	

END


GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_Reajeste]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		valentys
-- Create date: 10-06-2015
-- Description:	realiza el proceso de reajuste
-- =============================================
-- exec SP_UPD_Reajeste null, 4, 2015
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_Reajeste](
	@CODIGOS_CLIENTES VARCHAR(MAX) = NULL,
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL,
	@ID_UNIDAD INT

)AS
BEGIN
	SET NOCOUNT ON;

	  -- DECLARE @CODIGOS_CLIENTES VARCHAR(MAX) = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'
	   DECLARE --@ID_UNIDAD INT = 2
	          @ID_CLIENTE INT
	         , @VALOR float
			 , @ID_MODELO_COBRO INT
			 , @ID_DETALLE_MODELO_COBRO INT
			 , @ID_CONTRATO INT
	         , @MES_ULTIMO_REAJUSTE INT = 1
			 , @ANIO_ULTIMO_REAJUSTE INT
			 , @REAJ_CODIGO int 
			 , @CANTIDAD_CONFIGURACION int 
			 , @VALOR_REAJUSTE float

	   IF (@CODIGOS_CLIENTES IS NOT NULL AND @CODIGOS_CLIENTES != '')
		   BEGIN
			   --Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar.
			   --QUERY QUE REALIZA EL CALCULO SOLO A LOS CLIENTES QUE SE ENVIAN POR PARAMETRO
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO  = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO  = REAJ.REAJ_CODIGO
				 WHERE CONT.id_unidad  = @ID_UNIDAD
				   AND CONT.id_cliente IN (SELECT * FROM split(@CODIGOS_CLIENTES,','))
				   AND REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0
				   AND MODD.id_detalle_modelo_cobro NOT IN (SELECT HIST.HMCD_ID_DETALLE_MODELO_COBRO
															  FROM TB_HIS_MODELO_COBRO_DETALLE HIST
															 WHERE HIST.HMCD_MES = @MES_REAJUSTE
															   AND HIST.HMCD_ANIO = @ANIO_REAJUSTE
															   AND HIST.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro);
			END
		ELSE
			BEGIN
				--Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar
				--BUSCA SOLO LOS CLIENTES A QUIEN SE DEBE APLICAR EL REAJUSTE
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
				 WHERE CONT.id_unidad  = @ID_UNIDAD
				   AND REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0
				   AND MODD.id_detalle_modelo_cobro NOT IN (SELECT HIST.HMCD_ID_DETALLE_MODELO_COBRO
															  FROM TB_HIS_MODELO_COBRO_DETALLE HIST
															 WHERE HIST.HMCD_MES = @MES_REAJUSTE
															   AND HIST.HMCD_ANIO = @ANIO_REAJUSTE
															   AND HIST.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro);
			END


		  --Abre el cursor y recorro a los clientes.
		  OPEN CR_id_clientes
		 FETCH NEXT FROM CR_id_clientes
		  INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
		 WHILE @@FETCH_STATUS = 0
			 BEGIN

					IF (@CANTIDAD_CONFIGURACION = 1)
						BEGIN
							--SI LA CONFIGURACIÓN ES UNA SOLA ES PORQUE ES ANUAL Y TOMA DESDE EL MISMO MES PERO DEL AÑO PASADO
							SELECT @MES_ULTIMO_REAJUSTE  = REDE.READ_MES 
								 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1 
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
						END
					ELSE
						BEGIN
							
							SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
							     , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
							   AND REDE.READ_MES < ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))

							   IF (@MES_ULTIMO_REAJUSTE IS NULL)
								   BEGIN
										SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
											 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1
										  FROM contratos			   CONT with(nolock)
									INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
									INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
										 WHERE CONT.id_contrato = @ID_CONTRATO
								   END

						END

--------------------------------------------------------------------------------------------------------------------------------------------
--codigo comentado a raiz que el ipc utilizado en abril fue de 0.6 y la forma del procedimiento da 0.7
--------------------------------------------------------------------------------------------------------------------------------------------
					--CALCULA EL VALOR DE REAJUSTE
					SET @VALOR_REAJUSTE = (	SELECT SUM(INDV.INDV_VALOR)
											  FROM TB_SGS_INDICADOR          INDI with(nolock) 
										INNER JOIN TB_SGS_INDICADOR_VALOR    INDV with(nolock) ON INDI.INDI_CODIGO = INDV.INDV_INDI_CODIGO
										INNER JOIN TB_SGS_REAJUSTE_INDICADOR REIN with(nolock) ON REIN.REIN_INDI_CODIGO = INDI.INDI_CODIGO
											 WHERE Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') >= Convert(varchar(4), @ANIO_ULTIMO_REAJUSTE) + REPLACE(STR(@MES_ULTIMO_REAJUSTE, 2), SPACE(1), '0')
											   AND Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') <  Convert(varchar(4), ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))) +  Convert(varchar(2), REPLACE(STR(@MES_REAJUSTE, 2), SPACE(1), '0'))
											   AND REIN.REIN_REAJ_CODIGO = @REAJ_CODIGO)
--------------------------------------------------------------------------------------------------------------------------------------------
					--SET @VALOR_REAJUSTE = 0.6
--------------------------------------------------------------------------------------------------------------------------------------------
					--print('ultimo mes : ' + convert(varchar(100) , @MES_ULTIMO_REAJUSTE))
					--print('ultimo año : ' + convert(varchar(100) , @ANIO_ULTIMO_REAJUSTE))
					--print('mes reajuste : ' + convert(varchar(100) , @MES_REAJUSTE))
					--print('año reajuste : ' + convert(varchar(100) , @ANIO_REAJUSTE))
					--print('codigo reajuste : ' + convert(varchar(100) , @REAJ_CODIGO))
					--print('valor reajuste: ' + convert(varchar(100) , @VALOR_REAJUSTE))
					IF (@VALOR_REAJUSTE > 0)
					BEGIN
						--INSERTO EL VALOR DEL DETALLE DE MODELO ACTUAL A LA TABLA HISTORICOS.
						INSERT INTO [TB_HIS_MODELO_COBRO_DETALLE]
								   ([HMCD_ID_DETALLE_MODELO_COBRO]
								   ,[HMCD_VALOR]
								   ,[HMCD_FECHA]
								   ,[HMCD_REAJUSTE]
								   ,[HMCD_MES]
								   ,[HMCD_ANIO])
							 VALUES
								   (@ID_DETALLE_MODELO_COBRO
								   ,@VALOR
								   ,GETDATE()
								   ,@VALOR_REAJUSTE
								   ,@MES_REAJUSTE
								   ,@ANIO_REAJUSTE)

						--ACTUALIZA EL VALOR DEL MODELO DE COBRO
						UPDATE [modelo_cobro_detalle]
						   SET [valor] = round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100), 0)
						 WHERE [id_detalle_modelo_cobro] = @ID_DETALLE_MODELO_COBRO
					

						--print('ultimo mes : ' + convert(varchar(100) , @MES_ULTIMO_REAJUSTE))
						--print('ultimo año : ' + convert(varchar(100) , @ANIO_ULTIMO_REAJUSTE))
						--print('mes reajuste : ' + convert(varchar(100) , @MES_REAJUSTE))
						--print('año reajuste : ' + convert(varchar(100) , @ANIO_REAJUSTE))
						--print('codigo reajuste : ' + convert(varchar(100) , @REAJ_CODIGO))
						
						--print('id_contrato = ' + convert(varchar(10) , @ID_CONTRATO) + 
						--	  ' ultimo mes reajuste = ' + convert(varchar(10) ,@MES_ULTIMO_REAJUSTE) + '-' + convert(varchar(10) , @ANIO_ULTIMO_REAJUSTE) +
						--	  ' VALOR REAJUSTE = ' + convert(varchar(10) , @VALOR_REAJUSTE) +
						--	  ' ID_MODELO DE COBRO DETALLE = ' + convert(varchar(10) , @ID_DETALLE_MODELO_COBRO) +
						--	  ' VALOR ACTUAL = ' + convert(varchar(100) , @VALOR) + 
						--	  ' VALOR ACTUALIZADO = ' + convert(varchar(100) , round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100),0)))
					
					END
					--Próximo cliente
					FETCH NEXT FROM CR_id_clientes
					INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
			   END 
		 CLOSE CR_id_clientes;
	DEALLOCATE CR_id_clientes;
    
END



GO
/****** Object:  StoredProcedure [dbo].[SP_UPD_ReversarReajuste]    Script Date: 24-08-2015 16:24:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		valentys
-- Create date: 08-07-2015
-- Description:	Reversa un reajuste.
-- =============================================
-- 
-- =============================================
CREATE PROCEDURE [dbo].[SP_UPD_ReversarReajuste](
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL,
	@ID_UNIDAD INT
)AS
BEGIN
	SET NOCOUNT ON;
	
	IF(@MES_REAJUSTE IS NOT NULL AND @ANIO_REAJUSTE IS NOT NULL)
	BEGIN
		--actualiza los valores al mes enviado
		UPDATE MODD
		   SET MODD.valor = HIST.HMCD_VALOR
		  FROM modelo_cobro_detalle		   MODD
	INNER JOIN TB_HIS_MODELO_COBRO_DETALLE HIST ON MODD.id_detalle_modelo_cobro = HIST.HMCD_ID_DETALLE_MODELO_COBRO
	INNER JOIN modelo_cobro			   MODE with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
	INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
	INNER JOIN contratos			   CONT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
		 WHERE HIST.HMCD_MES  = @MES_REAJUSTE
		   AND HIST.HMCD_ANIO = @ANIO_REAJUSTE
		   AND CONT.id_unidad = @ID_UNIDAD;
	
		--Eliminar desde el mes de reverso hacia adelante
		DELETE FROM TB_HIS_MODELO_COBRO_DETALLE
		 WHERE (convert(varchar(4), HMCD_ANIO) + right('0'+convert(varchar(2), HMCD_MES), 2)) >=  (convert(varchar(4), @ANIO_REAJUSTE) + right('0'+convert(varchar(2), @MES_REAJUSTE), 2));
	END
END



GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sindis"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "condis"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 119
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nore"
            Begin Extent = 
               Top = 120
               Left = 253
               Bottom = 233
               Right = 448
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "sindis"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 9
         End
         Begin Table = "condis"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 136
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nore"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 251
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "ccc"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 242
            End
            DisplayFlags = 280
            TopColumn = 13
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 6
               Left = 280
               Bottom = 136
               Right = 463
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 19
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES_unidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CLIENTES_unidad'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "csin"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 11
         End
         Begin Table = "ccc"
            Begin Extent = 
               Top = 6
               Left = 253
               Bottom = 136
               Right = 457
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nore"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 233
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CONTRATO_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_COBRO_CONTRATO_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "vpt"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 219
            End
            DisplayFlags = 280
            TopColumn = 14
         End
         Begin Table = "cobr"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nore"
            Begin Extent = 
               Top = 6
               Left = 257
               Bottom = 136
               Right = 452
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_PUNTO_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_PUNTO_CERRADO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_VOLTEOS_COBRO_CLIENTES"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 10
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CLIENTE_COBRO_SIN_DISPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CLIENTE_COBRO_SIN_DISPOSICION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_VOLTEOS_COBRO_CLIENTES_CLOSE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 10
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CLIENTE_COBRO_SIN_DISPOSICION_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "con"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "pto"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "perio"
            Begin Extent = 
               Top = 6
               Left = 302
               Bottom = 136
               Right = 472
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CONTRATO_PUNTO_SERVICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CONTRATO_PUNTO_SERVICIO'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_VOLTEOS_CONTRATO_CLIENTES_CLOSE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 231
            End
            DisplayFlags = 280
            TopColumn = 12
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CONTRATO_SIN_DIS_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CONTRATO_SIN_DIS_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "pto"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 261
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cont"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 6
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PUNTO_SERVICIO_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PUNTO_SERVICIO_DESCRIPCION'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "V_VOLTEOS_PUNTOS_CLIENTES_CLOSE"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 235
            End
            DisplayFlags = 280
            TopColumn = 15
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 12
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PUNTO_SIN_DISPOCICION_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_PUNTO_SIN_DISPOCICION_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_COBRO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_COBRO_CLIENTES'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_COBRO_CLIENTES_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_COBRO_CLIENTES_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_CONTRATO_CLIENTES_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_CONTRATO_CLIENTES_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_PUNTOS_CLIENTES_CLOSE'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_VOLTEOS_PUNTOS_CLIENTES_CLOSE'
GO
