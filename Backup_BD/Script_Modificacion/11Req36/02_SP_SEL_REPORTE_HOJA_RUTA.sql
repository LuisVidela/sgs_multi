USE [RS_SGS_ARAUCO]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_REPORTE_HOJA_RUTA]    Script Date: 28-06-2017 16:51:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Intellego,José Joaquín Sepúlveda>
-- Create date: <09/06/2017>
-- Update date: <28/06/2017>
-- Description:	<Generación de procedimiento almacenado para recuperación de hojas de ruta.>
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_REPORTE_HOJA_RUTA]
    @fechaDesde DATE = NULL , -- fecha comienzo
    @fechaHasta DATE = NULL , -- fecha fin periodo
    @IdEstado VARCHAR(20) = NULL ,
    @IdTipoServicio VARCHAR(100) = NULL ,
    @idHojaRuta INT = NULL ,
    @idUnidad INT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT   CASE WHEN hr.estado IS NULL THEN 0
                      WHEN hr.estado = 2 THEN 0
                      WHEN hr.estado = 1 THEN 4
                      ELSE hr.estado
                 END estado ,
                 hr.id_hoja_ruta ,
                 ca.id_camion ,
                 ca.patente ,
                 co.id_conductor ,
                 co.nombre_conductor ,
                 hr.id_tipo_servicio ,
                 tc.tipo_contrato ,
                 hr.fecha ,
                 cn.id_unidad
        FROM     hoja_ruta hr
                 INNER JOIN dbo.camiones ca ON ca.id_camion = hr.id_camion
                 INNER JOIN dbo.conductores co ON co.id_conductor = hr.id_conductor
                 INNER JOIN dbo.hoja_ruta_detalle rd ON hr.id_hoja_ruta = rd.id_hoja_ruta
                 INNER JOIN dbo.puntos_servicio ps ON rd.id_punto_servicio = ps.id_punto_servicio
                 INNER JOIN dbo.contratos cn ON ps.id_contrato = cn.id_contrato
				 INNER JOIN dbo.tipo_contrato tc ON cn.id_tipo_contrato = tc.id_tipo_contrato
        WHERE    hr.fecha >= @fechaDesde
                 AND hr.fecha <= @fechaHasta
                 AND ( CASE WHEN hr.estado IS NULL THEN 0
                            ELSE hr.estado
                       END IN (   SELECT number
                                  FROM   intlist_to_tbl(@IdEstado)
                              )
                     )
                 AND ( hr.id_tipo_servicio IN (   SELECT number
                                                  FROM   intlist_to_tbl(@IdTipoServicio)
											  )
                    )
                 AND hr.id_hoja_ruta = ISNULL(@idHojaRuta, hr.id_hoja_ruta)
				 
                 AND cn.id_unidad = @idUnidad
        GROUP BY hr.estado ,
                 hr.id_hoja_ruta ,
                 ca.id_camion ,
                 ca.patente ,
                 co.id_conductor ,
                 co.nombre_conductor ,
                 hr.id_tipo_servicio ,
                 tc.tipo_contrato ,
                 hr.fecha ,
                 cn.id_unidad
        ORDER BY hr.id_hoja_ruta;

		
    END;
