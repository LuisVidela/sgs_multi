USE RS_SGS_ARAUCO;
GO
/*
   miércoles, 24 de mayo de 201715:24:28
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.calculo_cobro_clientes ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.calculo_cobro_clientes SET (LOCK_ESCALATION = TABLE)
GO
COMMIT;

DELETE FROM dbo.calculo_cobro_clientes;
go
