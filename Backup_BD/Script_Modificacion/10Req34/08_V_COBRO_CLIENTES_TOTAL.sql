USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_COBRO_CLIENTES_TOTAL]    Script Date: 25-05-2017 18:15:22 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_COBRO_CLIENTES_TOTAL]
AS
    SELECT  cob.id_cliente ,
            cob.rut_cliente ,
            cob.Fecha ,
            cob.id_punto_servicio ,
            cob.Cobro ,
            disp.Valor ,
            ( cob.Cobro + disp.Valor ) AS TotalCliente ,
            ( ( ( cob.Cobro + disp.Valor ) * 19 ) / 100 ) AS IVA ,
            ( ( ( ( cob.Cobro + disp.Valor ) * 19 ) / 100 ) + ( cob.Cobro
                                                              + disp.Valor ) ) AS TotalClienteMasIVA ,
            disp.id_unidad
    FROM    V_COBRO_CLIENTES cob
            INNER JOIN calculo_cobro_disposicion disp ON cob.id_punto_servicio = disp.id_punto_servicio
                                                         AND disp.id_unidad = cob.id_unidad
                                                         AND cob.id_cliente = disp.id_cliente;

GO
