USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_PUNTOS_HOOK_CALCULADOS]    Script Date: 24-05-2017 17:39:45 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_PUNTOS_HOOK_CALCULADOS]
AS
    SELECT DISTINCT
            id_punto_servicio ,
            Fecha
    FROM    dbo.calculo_cobro_clientes
    WHERE   ( peso > 0 );


GO


