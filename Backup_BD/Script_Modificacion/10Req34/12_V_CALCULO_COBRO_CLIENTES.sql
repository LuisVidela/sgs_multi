USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_CALCULO_COBRO_CLIENTES]    Script Date: 26-05-2017 18:11:07 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_CALCULO_COBRO_CLIENTES]
AS
    SELECT  0 AS id_pago ,
            ccc.id_cliente ,
            ccc.rut_cliente ,
            ccc.razon_social ,
            ccc.nombre_fantasia ,
            ISNULL(nrc.nro_retiros, 0) AS Numero_retiros ,
            SUM(ccc.valor) AS Valor ,
            0 AS IVA ,
            0 AS NETO ,
            SUM(ccc.peso) AS Peso ,
            ccc.Fecha ,
            ccc.Monthis ,
            ccc.Yearis ,
            0 AS ValorTotalDisposicion ,
            vtc.valor AS ValorTotal ,
            1 AS Numero_No_Retiros ,
            ccc.id_unidad
    FROM    dbo.calculo_cobro_clientes AS ccc
            LEFT OUTER JOIN dbo.V_NRO_RETIROS_CLIENTE AS nrc ON nrc.id_cliente = ccc.id_cliente
                                                              AND nrc.Fecha = ccc.Fecha
            LEFT OUTER JOIN dbo.V_VALOR_TOTAL_CLIENTE AS vtc ON vtc.id_cliente = ccc.id_cliente
                                                              AND vtc.Fecha = ccc.Fecha
    GROUP BY ccc.id_cliente ,
            ccc.rut_cliente ,
            ccc.razon_social ,
            ccc.nombre_fantasia ,
            nrc.nro_retiros ,
            vtc.valor ,
            ccc.Fecha ,
            ccc.Monthis ,
            ccc.Yearis ,
            ccc.id_unidad;


GO


