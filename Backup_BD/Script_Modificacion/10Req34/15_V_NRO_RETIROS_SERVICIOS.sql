USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_NRO_RETIROS_SERVICIOS]    Script Date: 29-05-2017 14:31:48 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO




ALTER VIEW [dbo].[V_NRO_RETIROS_SERVICIOS]
AS
    SELECT  COUNT(glosa) AS nro_retiros ,
            id_punto_servicio ,
            Fecha
    FROM    dbo.calculo_cobro_clientes
    WHERE   ( glosa LIKE 'Retiro' )
    GROUP BY id_punto_servicio ,
            Fecha;





GO


