USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_NRO_RETIROS_CLIENTE]    Script Date: 26-05-2017 18:17:41 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_NRO_RETIROS_CLIENTE]
AS
    SELECT  COUNT(glosa) AS nro_retiros ,
            id_cliente ,
            Fecha
    FROM    dbo.calculo_cobro_clientes
    WHERE   ( glosa LIKE 'Retiro' )
    GROUP BY id_cliente ,
            Fecha;


GO


