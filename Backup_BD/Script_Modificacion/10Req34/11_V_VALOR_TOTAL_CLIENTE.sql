USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_VALOR_TOTAL_CLIENTE]    Script Date: 29-05-2017 11:29:33 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_VALOR_TOTAL_CLIENTE]
AS
    SELECT  SUM(ValorTotal) AS valor ,
            Id_cliente ,
            Fecha
    FROM    dbo.V_CALCULO_PUNTO
    GROUP BY Id_cliente ,
            Fecha;


GO


