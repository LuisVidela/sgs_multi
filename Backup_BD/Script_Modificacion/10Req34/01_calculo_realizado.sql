USE RS_SGS_ARAUCO;
GO
/*
   miércoles, 24 de mayo de 201712:49:36
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION;
SET QUOTED_IDENTIFIER ON;
SET ARITHABORT ON;
SET NUMERIC_ROUNDABORT OFF;
SET CONCAT_NULL_YIELDS_NULL ON;
SET ANSI_NULLS ON;
SET ANSI_PADDING ON;
SET ANSI_WARNINGS ON;
COMMIT;
BEGIN TRANSACTION;
GO
ALTER TABLE dbo.calculo_realizado ADD
id_unidad INT NULL;
GO
ALTER TABLE dbo.calculo_realizado SET (LOCK_ESCALATION = TABLE);
GO
COMMIT;

DELETE  FROM dbo.calculo_realizado;
GO

INSERT  INTO dbo.calculo_realizado
        SELECT DISTINCT
                [Fecha] ,
                1 ,
                id_unidad
        FROM    [RS_SGS_ARAUCO].[dbo].[cobro_clientes_cerrado];