USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_COBRO_CLIENTES]    Script Date: 25-05-2017 19:24:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[V_COBRO_CLIENTES]
AS
SELECT id_punto_servicio,
	   Fecha,
	   id_cliente,
	   rut_cliente,
	   SUM(Bruto) AS Cobro,
	   id_unidad
 FROM calculo_cobro_clientes
 GROUP BY id_punto_servicio,
	      Fecha,
	      id_cliente,
	      rut_cliente,
		  id_unidad













GO


