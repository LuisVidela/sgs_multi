USE [RS_SGS_ARAUCO];
GO
/****** Object:  StoredProcedure [dbo].[sp_reversa_cobro_periodo]    Script Date: 30-05-2017 16:14:42 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO


ALTER PROCEDURE [dbo].[sp_reversa_cobro_periodo]
    @periodo VARCHAR(6) ,
    @id_unidad INT
AS
    BEGIN
		--delete from calculo_cobro_empresas
        DELETE  FROM calculo_cobro_clientes
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;

        DELETE  FROM calculo_cobro_disposicion
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;
        
		DELETE  FROM cobro_cliente_total
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;


        DELETE  FROM calculo_realizado
        WHERE   Fecha = @periodo
                AND Cerrado = 1
                AND id_unidad = @id_unidad;
        
		DELETE  FROM cobro_clientes_cerrado
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;
        
		DELETE  FROM cobro_disposicion_cerrado
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;
        
		DELETE  FROM cliente_total_cerrado
        WHERE   Fecha = @periodo
                AND id_unidad = @id_unidad;
    END;





