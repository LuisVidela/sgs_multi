USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_COBROS_FIJOS_SIN_SERVICIOS]    Script Date: 29-05-2017 14:21:05 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_COBROS_FIJOS_SIN_SERVICIOS]
AS
    SELECT  A.id_calculo ,
            A.id_hoja_ruta ,
            A.id_punto_servicio ,
            A.cantidad ,
            A.peso ,
            A.estado ,
            A.fechaFull ,
            A.Fecha ,
            A.Monthis ,
            A.Yearis ,
            A.nombre_punto ,
            A.estado_punto ,
            A.id_contrato ,
            A.id_cliente ,
            A.id_sucursal ,
            A.estado_contrato ,
            A.id_modelo_cobro ,
            A.rut_cliente ,
            A.razon_social ,
            A.giro_comercial ,
            A.nombre_fantasia ,
            A.sucursal ,
            A.descripcion ,
            A.rut_sucursal ,
            A.id_tipo_modalidad ,
            A.valor ,
            A.tipo_modalidad ,
            A.como_se_calcula ,
            A.bruto ,
            A.proceso_cerrado ,
            A.fechaCalculo ,
            A.Orden ,
            A.id_detalle_modelo_cobro ,
            A.sorting ,
            A.glosa ,
            A.cobro_arriendo ,
            A.id_camion ,
            A.camion ,
            A.id_conductor ,
            A.conductor ,
            A.fechacorta ,
            A.sistema ,
            A.franquiciado ,
            A.estadopunto ,
            A.relleno ,
            A.pagofranquiciado ,
            A.margen ,
            A.margenporc ,
            A.valorenuf ,
            A.valoruf ,
            A.ValorTotal
    FROM    dbo.V_COBROS_FIJOS_PUNTOS AS A
            LEFT OUTER JOIN dbo.V_CALCULO_PUNTO_OLD AS B ON A.id_punto_servicio = B.id_punto_servicio
                                                            AND B.Fecha = A.Fecha
    WHERE   ( B.id_punto_servicio IS NULL );


GO


