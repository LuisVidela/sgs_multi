--********************************************
--Agrega la unidad a la tabla contratos
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.contratos ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.contratos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--********************************************
--Agrega la id_punto_servicio a la tabla detalle_calculo_tarifa_escalable
-------------------------------------------------------
--Observaci�n: debe ser not null pero como hay registros en la base de datos
-- de deja como que permita nulos.
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.detalle_calculo_tarifa_escalable ADD
	id_punto_servicio int NULL
GO
ALTER TABLE dbo.detalle_calculo_tarifa_escalable SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--Agrega la id_unidad a la tabla franquiciado
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.franquiciado ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.franquiciado SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--renombra los campos y elimna el icono de la tabla modulos
--********************************************
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.modulos.id_modulo', N'id', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.modulos.modulo', N'nombre', 'COLUMN' 
GO
ALTER TABLE dbo.modulos
	DROP COLUMN icono
GO
ALTER TABLE dbo.modulos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--inserta el campo id_unidad a la tabla pago_franquiciados
-------------------------------------------------------
--Observaci�n: debe ser not null pero como hay registros en la base de datos
-- de deja como que permita nulos.
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.pago_franquiciados ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.pago_franquiciados SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--renombra el campo perfi por nombre de la tabla perfiles
--********************************************
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.perfiles.perfil', N'nombre', 'COLUMN' 
GO
ALTER TABLE dbo.perfiles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--agrega el campo id_unidad a la ruta_modelo
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.ruta_modelo ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.ruta_modelo SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--agrega el campo id_unidad al tipo_tarifa
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.tipo_tarifa ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.tipo_tarifa SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--Modifica el campo id_unidad_negocio y id_centro_responsabilidad para que permita nulos
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.UserProfile
	DROP COLUMN id_unidad_negocio, id_centro_responsabilidad
GO
ALTER TABLE dbo.UserProfile SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.UserProfile ADD
	id_unidad_negocio int NULL,
	id_centro_responsabilidad int NULL
GO
ALTER TABLE dbo.UserProfile SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.perfiles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.UserProfile ADD CONSTRAINT
	FK_UserProfile_perfiles FOREIGN KEY
	(
	id_perfil
	) REFERENCES dbo.perfiles
	(
	id_perfil
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.UserProfile SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--modifica los campos id_unidad_negocio y id_centro_responsabilidad para que permitan nulos
-- en la tabla usuarios
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.usuarios
	DROP COLUMN id_unidad_negocio, id_centro_responsabilidad
GO
ALTER TABLE dbo.usuarios SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.usuarios ADD
	id_unidad_negocio int NULL,
	id_centro_responsabilidad int NULL
GO
ALTER TABLE dbo.usuarios SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--agrega el campo id_unidad en la tabla vertederos
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.vertederos ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.vertederos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--agrega el campo modulo_id a la tabla webpages_roles
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_Roles ADD
	modulo_id int NULL
GO
ALTER TABLE dbo.webpages_Roles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_Roles ADD CONSTRAINT
	FK_webpages_Roles_modulos1 FOREIGN KEY
	(
	modulo_id
	) REFERENCES dbo.modulos
	(
	id
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.webpages_Roles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
--********************************************
--agrega campos create, read, update y dalete a la tabla webpages_UsersInRoles
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD
	[Create] bit NULL,
	[Read] bit NULL,
	[Update] bit NULL,
	[Delete] bit NULL
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	DF_webpages_UsersInRoles_Create DEFAULT ((0)) FOR [Create]
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	DF_webpages_UsersInRoles_Read DEFAULT ((0)) FOR [Read]
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	DF_webpages_UsersInRoles_Update DEFAULT ((0)) FOR [Update]
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	DF_webpages_UsersInRoles_Delete DEFAULT ((0)) FOR [Delete]
GO
ALTER TABLE dbo.webpages_UsersInRoles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--********************************************
-- crea la tabla usuariosxunidades
--********************************************

CREATE TABLE [dbo].[usuariosxunidades](
	[id_usuarioxunidad] [int] IDENTITY(1,1) NOT NULL,
	[id_usuario] [int] NOT NULL,
	[id_unidad] [int] NOT NULL,
	[predeterminada] [bit] NOT NULL,
 CONSTRAINT [PK__usuarios__3213E83F728B9A9A] PRIMARY KEY CLUSTERED 
(
	[id_usuarioxunidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[usuariosxunidades] ADD  DEFAULT ((0)) FOR [predeterminada]
GO

--********************************************
-- actualiza vinculos entre las tablas webpages_UsersInRoles, UserProfile y perfiles
--********************************************
/*delete from [dbo].[webpages_UsersInRoles];


BEGIN TRANSACTION
GO
ALTER TABLE dbo.perfiles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	FK_webpages_UsersInRoles_perfiles FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.perfiles
	(
	id_perfil
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	
GO
ALTER TABLE dbo.webpages_UsersInRoles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles
	DROP CONSTRAINT fk_UserId
GO
ALTER TABLE dbo.UserProfile SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT*/


--********************************************
-- tabla petroleo autoincremental
--********************************************

BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_petroleo
	(
	id_petroleo int NOT NULL IDENTITY (1, 1),
	mes int NOT NULL,
	a�o int NOT NULL,
	valor float(53) NOT NULL,
	id_unidad_negocio int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_petroleo SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_petroleo ON
GO
IF EXISTS(SELECT * FROM dbo.petroleo)
	 EXEC('INSERT INTO dbo.Tmp_petroleo (id_petroleo, mes, a�o, valor, id_unidad_negocio)
		SELECT id_petroleo, mes, a�o, valor, id_unidad_negocio FROM dbo.petroleo WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_petroleo OFF
GO
DROP TABLE dbo.petroleo
GO
EXECUTE sp_rename N'dbo.Tmp_petroleo', N'petroleo', 'OBJECT' 
GO
ALTER TABLE dbo.petroleo ADD CONSTRAINT
	PK_petroleo PRIMARY KEY CLUSTERED 
	(
	id_petroleo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT


--********************************************
-- Agregar densidad a la tabla residuos
--********************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.residuos ADD
	densidad float(53) NOT NULL CONSTRAINT DF_residuos_densidad DEFAULT 0
GO
ALTER TABLE dbo.residuos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


--********************************************
-- CREAR NUEVA TABLA DE Porcentaje de llenado
--********************************************
BEGIN TRANSACTION
GO
CREATE TABLE dbo.TB_SGS_PORCENTAJELLENADO
	(
	PORL_CODIGO int NOT NULL IDENTITY (1, 1),
	PORL_PORCERTAJELLENADO int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.TB_SGS_PORCENTAJELLENADO ADD CONSTRAINT
	PK_TB_SGS_PORCENTAJELLENADO PRIMARY KEY CLUSTERED 
	(
	PORL_CODIGO
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.TB_SGS_PORCENTAJELLENADO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--********************************************
-- Vinculo con tabla hoja de ruta dellate
--********************************************

BEGIN TRANSACTION
GO
ALTER TABLE dbo.TB_SGS_PORCENTAJELLENADO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.hoja_ruta_detalle ADD
	PORL_CODIGO int NULL
GO
ALTER TABLE dbo.hoja_ruta_detalle ADD CONSTRAINT
	FK_hoja_ruta_detalle_TB_SGS_PORCENTAJELLENADO FOREIGN KEY
	(
	PORL_CODIGO
	) REFERENCES dbo.TB_SGS_PORCENTAJELLENADO
	(
	PORL_CODIGO
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.hoja_ruta_detalle SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--********************************************
-- nuevo campo peso colculado en la tabla hoja_ruta_detalle
--********************************************


BEGIN TRANSACTION
GO
ALTER TABLE dbo.hoja_ruta_detalle ADD
	peso_calculado bit NULL
GO
ALTER TABLE dbo.hoja_ruta_detalle ADD CONSTRAINT
	DF_hoja_ruta_detalle_peso_calculado DEFAULT 0 FOR peso_calculado
GO
ALTER TABLE dbo.hoja_ruta_detalle SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update hoja_ruta_detalle
   set peso_calculado = 0
go

--****************************************************************************************
-- elimina relaci�n entre reajuste y webpages_Membership
--****************************************************************************************

BEGIN TRANSACTION
GO
ALTER TABLE dbo.TB_SGS_REAJUSTE
	DROP CONSTRAINT FK_TB_SGS_REAJUSTE_webpages_Membership
GO
ALTER TABLE dbo.webpages_Membership SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TB_SGS_REAJUSTE SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--****************************************************************************************
-- cambio tipo de datos de varchar a float del campo capacidad_equipo en la tabla equipos
--****************************************************************************************
update equipos
   set capacidad_equipo = LTRIM(RTRIM(REPLACE(capacidad_equipo, ' M3', '')))

update equipos
   set capacidad_equipo = LTRIM(RTRIM(REPLACE(capacidad_equipo, 'Otros', '0')))

update equipos
   set capacidad_equipo = LTRIM(RTRIM(REPLACE(capacidad_equipo, '4,6', '4.6')))

go
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_equipos
	(
	id_equipo int NOT NULL IDENTITY (1, 1),
	tipo_equipo varchar(250) NOT NULL,
	capacidad_equipo float(53) NOT NULL,
	nom_abreviado varchar(5) NULL,
	nom_nemo varchar(15) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_equipos SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_equipos ON
GO
IF EXISTS(SELECT * FROM dbo.equipos)
	 EXEC('INSERT INTO dbo.Tmp_equipos (id_equipo, tipo_equipo, capacidad_equipo, nom_abreviado, nom_nemo)
		SELECT id_equipo, tipo_equipo, CONVERT(float(53), capacidad_equipo), nom_abreviado, nom_nemo FROM dbo.equipos WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_equipos OFF
GO
ALTER TABLE dbo.puntos_servicio
	DROP CONSTRAINT fk_equipos
GO
DROP TABLE dbo.equipos
GO
EXECUTE sp_rename N'dbo.Tmp_equipos', N'equipos', 'OBJECT' 
GO
ALTER TABLE dbo.equipos ADD CONSTRAINT
	PK__equipos__EE01F88A49C3F6B7 PRIMARY KEY CLUSTERED 
	(
	id_equipo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idx_capacidad_equipo ON dbo.equipos
	(
	capacidad_equipo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idx_tipo_equipo ON dbo.equipos
	(
	tipo_equipo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.puntos_servicio WITH NOCHECK ADD CONSTRAINT
	fk_equipos FOREIGN KEY
	(
	id_equipo
	) REFERENCES dbo.equipos
	(
	id_equipo
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.puntos_servicio SET (LOCK_ESCALATION = TABLE)
GO
COMMIT