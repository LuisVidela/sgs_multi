/*
   miércoles, 1 de febrero de 201715:50:37
   Usuario: sa
   Servidor: JOSEPULVEDA-NTB
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.item_reporte_unificado ADD
	agrupar_manejo_dris bit NOT NULL CONSTRAINT DF_item_reporte_unificado_agrupar_manejo_dris DEFAULT (0)
GO
ALTER TABLE dbo.item_reporte_unificado SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
