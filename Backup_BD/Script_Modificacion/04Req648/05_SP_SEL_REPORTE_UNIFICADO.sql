USE [RS_SGS_ARAUCO]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_REPORTE_UNIFICADO]    Script Date: 03-02-2017 11:13:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Intellego,José Joaquín Sepúlveda>
-- Create date: <06/12/2016>
-- Update date: <07/02/2016>
-- Description:	<Generación de procedimiento almacenado para recuperación de reporte unificado.>
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_REPORTE_UNIFICADO]
-- Add the parameters for the stored procedure here
@Periodo VARCHAR(6) = NULL -- fecha comienzo
,
@IdCliente INT = NULL
AS
BEGIN

	-- Paso 1 Alimentar tabla temporal de reporte unificado para conservar valores de Manejo Dris
	DELETE TB_SGS_TMP_MANEJO_DRIS_POR_UNIDAD;

	INSERT INTO TB_SGS_TMP_MANEJO_DRIS_POR_UNIDAD
			SELECT
				vpsct.id_punto_servicio
				--,ps.nombre_punto
				--,vpsct.valor
				--,ccd.Disposicion
				--,ps.cobro_por_peso
				,CASE
					WHEN ps.cobro_por_peso = 1 THEN ccd.Disposicion * vpsct.valor / 1000
					ELSE ccd.Disposicion * vpsct.valor
				END AS total_manejo_dris
			FROM calculo_cobro_disposicion ccd
			INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA vpsct
				ON ccd.id_punto_servicio = vpsct.id_punto_servicio
			INNER JOIN puntos_servicio ps
				ON ccd.id_punto_servicio = ps.id_punto_servicio
			WHERE (vpsct.id_tipo_modalidad = 17)
			AND Fecha LIKE @Periodo;


	-- Paso 2 Alimentar la información de reporte unificado en una tabla temporal en vez de traer los datos directamente
	DELETE TB_SGS_TMP_REPORTE_UNIFICADO

	INSERT INTO TB_SGS_TMP_REPORTE_UNIFICADO
			SELECT
				iru.id_item
				,iru.nombre_item
				,iru.orden_item
				,umb.nombre
				,(SELECT
						SUM(mcd2.valor) AS Fijo
					FROM modelo_cobro AS mc2
					INNER JOIN modelo_cobro_detalle AS mcd2
						ON mc2.id_modelo = mcd2.id_modelo_cobro
					INNER JOIN puntos_servicio AS pc2
						ON pc2.id_punto_servicio = mc2.id_punto_servicio
					INNER JOIN V_CALCULO_PUNTO AS vcp2
						ON vcp2.id_punto_servicio = pc2.id_punto_servicio
					INNER JOIN item_reporte_unificado AS iru2
						ON iru2.id_item = pc2.id_item
					WHERE vcp2.Fecha LIKE @Periodo
					AND mcd2.id_tipo_modalidad IN (5, 7, 9, 10, 11,
					12)
					AND iru2.id_item = iru.id_item)
				AS Fijo
				,(SELECT
						SUM(vcp2.ValorTotal)
					FROM modelo_cobro AS mc2
					INNER JOIN modelo_cobro_detalle AS mcd2
						ON mc2.id_modelo = mcd2.id_modelo_cobro
					INNER JOIN puntos_servicio AS pc2
						ON pc2.id_punto_servicio = mc2.id_punto_servicio
					INNER JOIN V_CALCULO_PUNTO_2 AS vcp2
						ON vcp2.idpunto_servicio = pc2.id_punto_servicio
						AND (vcp2.id_tipo_modalidad IS NULL
						OR vcp2.id_tipo_modalidad <> 55
						)
					INNER JOIN item_reporte_unificado AS iru2
						ON iru2.id_item = pc2.id_item
					WHERE vcp2.Fecha LIKE @Periodo
					AND mcd2.id_tipo_modalidad IN (8, 6)
					AND iru2.id_item = iru.id_item
					AND (mcd2.id_tipo_modalidad = vcp2.id_tipo_modalidad
					OR vcp2.id_tipo_modalidad IS NULL
					))
				AS Variable
				,(SELECT
						SUM(CASE
							WHEN iru2.id_umb = 3 THEN vcp2.Numero_Retiros
							ELSE vcp2.Peso
						END)
					FROM modelo_cobro AS mc2
					INNER JOIN modelo_cobro_detalle AS mcd2
						ON mc2.id_modelo = mcd2.id_modelo_cobro
					INNER JOIN puntos_servicio AS pc2
						ON pc2.id_punto_servicio = mc2.id_punto_servicio
					INNER JOIN V_CALCULO_PUNTO_2 AS vcp2
						ON vcp2.idpunto_servicio = pc2.id_punto_servicio
						AND (vcp2.id_tipo_modalidad IS NULL
						OR vcp2.id_tipo_modalidad <> 55
						)
					INNER JOIN item_reporte_unificado AS iru2
						ON iru2.id_item = pc2.id_item
					WHERE vcp2.Fecha LIKE @Periodo
					AND mcd2.id_tipo_modalidad IN (8, 6)
					AND iru2.id_item = iru.id_item
					AND (mcd2.id_tipo_modalidad = vcp2.id_tipo_modalidad
					OR vcp2.id_tipo_modalidad IS NULL
					))
				AS CantidadVariable
				,iru.agrupar_manejo_dris
			FROM item_reporte_unificado AS iru
			INNER JOIN umb
				ON iru.id_umb = umb.id_umb
			WHERE iru.Id_cliente = @IdCliente;


	-- Paso 3 Descontar de tabla los totales de manejo Dris para cada punto de servicio afectado. Descontar solo valor, no cantidad
	UPDATE tstru
	SET Variable = tstru.Variable - manejoDris.variableDris
	FROM TB_SGS_TMP_REPORTE_UNIFICADO tstru
	INNER JOIN (SELECT
			SUM(tstmdpu.total_manejo_dris) AS variableDris
			,iru.id_item
		FROM TB_SGS_TMP_MANEJO_DRIS_POR_UNIDAD tstmdpu
		INNER JOIN puntos_servicio ps
			ON tstmdpu.id_punto_servicio = ps.id_punto_servicio
		INNER JOIN item_reporte_unificado iru
			ON ps.id_item = iru.id_item
		GROUP BY iru.id_item) AS manejoDris
		ON manejoDris.id_item = tstru.id_item;

	-- Paso 4 Descontar de tabla los totales de manejo Dris para cada punto de servicio afectado. Descontar solo cantidad, no valor
	UPDATE tstru
	SET CantidadVariable = tstru.CantidadVariable - manejoDris.CantidadDris
	FROM TB_SGS_TMP_REPORTE_UNIFICADO tstru
	INNER JOIN (SELECT
			SUM(vd.Disposicion) AS CantidadDris
			,iru.id_item
		FROM V_DISPOSICION vd
		INNER JOIN puntos_servicio ps
			ON vd.id_punto_servicio = ps.id_punto_servicio
		INNER JOIN item_reporte_unificado iru
			ON ps.id_item = iru.id_item
		WHERE vd.glosa LIKE '%Retiro Manejo Dris%'
		GROUP BY iru.id_item) AS manejoDris
		ON manejoDris.id_item = tstru.id_item;

	-- Paso 5 Agregar valor consolidado de manejo Dris en item especial y el total de cantidades para cada punto de servicio asociado
	UPDATE TB_SGS_TMP_REPORTE_UNIFICADO
	SET Variable =
	CASE
		WHEN Variable IS NULL THEN (SELECT
					SUM(tstmdpu.total_manejo_dris) AS variableDris
				FROM TB_SGS_TMP_MANEJO_DRIS_POR_UNIDAD tstmdpu)
		ELSE Variable + (SELECT
					SUM(tstmdpu.total_manejo_dris) AS variableDris
				FROM TB_SGS_TMP_MANEJO_DRIS_POR_UNIDAD tstmdpu)
	END
	WHERE agrupar_manejo_dris = 1;

	UPDATE TB_SGS_TMP_REPORTE_UNIFICADO
	SET CantidadVariable =
	CASE
		WHEN CantidadVariable IS NULL THEN (SELECT
					SUM(Disposicion)
				FROM V_DISPOSICION
				WHERE glosa LIKE 'Retiro Manejo Dris')
		ELSE CantidadVariable + (SELECT
					SUM(Disposicion)
				FROM V_DISPOSICION
				WHERE glosa LIKE 'Retiro Manejo Dris')
	END
	WHERE agrupar_manejo_dris = 1;

	-- Paso 6 Recuperar información para reporte unificado desde tabla temporal
	SELECT
		tstru.nombre_item
		,tstru.orden_item
		,tstru.nombre
		,tstru.Fijo
		,tstru.Variable
		,tstru.CantidadVariable
	FROM TB_SGS_TMP_REPORTE_UNIFICADO tstru
	ORDER BY tstru.orden_item;
END;