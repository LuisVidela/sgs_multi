/*
   viernes, 3 de febrero de 201711:51:44
   Usuario: sa
   Servidor: JOSEPULVEDA-NTB
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.TB_SGS_TMP_REPORTE_UNIFICADO
	(
	id_item int NOT NULL,
	nombre_item varchar(100) NOT NULL,
	orden_item int NOT NULL,
	nombre varchar(50) NOT NULL,
	Fijo float(53) NULL,
	Variable decimal(38, 4) NULL,
	CantidadVariable float(53) NULL,
	agrupar_manejo_dris BIT NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.TB_SGS_TMP_REPORTE_UNIFICADO ADD CONSTRAINT
	PK_TB_SGS_TMP_REPORTE_UNIFICADO PRIMARY KEY CLUSTERED 
	(
	id_item
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.TB_SGS_TMP_REPORTE_UNIFICADO SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
