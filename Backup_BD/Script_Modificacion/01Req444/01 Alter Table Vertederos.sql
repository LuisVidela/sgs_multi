ALTER TABLE RS_SGS_ARAUCO.[dbo].[vertederos]
	ADD  pertenece_resiter BIT NOT NULL DEFAULT 0;
GO
DECLARE @v sql_variant 
SET @v = N'Indica si el vertedero pertenece o no a Resiter. 0 no pertenece. 1 pertenece. Por defecto el valor es 0'
EXECUTE sp_addextendedproperty N'MS_Description', @v, N'SCHEMA', N'dbo', N'TABLE', N'vertederos', N'COLUMN', N'pertenece_resiter'
GO