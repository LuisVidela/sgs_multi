-- =============================================
-- Author:		valentys
-- Create date: 07-07-2015
-- Description:	obtiene los modelos de cobros que deben ser reajustados.
-- =============================================
-- exec [SP_SEL_VerificarReajeste] 4
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_VerificarReajeste](
	@MES_REAJUSTE INT = NULL,
	@ANIO_REAJUSTE INT = NULL
)AS
BEGIN
	SET NOCOUNT ON;

	  -- DECLARE @CODIGOS_CLIENTES VARCHAR(MAX) = '1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'
	   DECLARE @ID_UNIDAD INT = 2
	         , @ID_CLIENTE INT
	         , @VALOR float
			 , @ID_MODELO_COBRO INT
			 , @ID_DETALLE_MODELO_COBRO INT
			 , @ID_CONTRATO INT
	         , @MES_ULTIMO_REAJUSTE INT = 1
			 , @ANIO_ULTIMO_REAJUSTE INT
			 , @REAJ_CODIGO int 
			 , @CANTIDAD_CONFIGURACION int 
			 , @VALOR_REAJUSTE float
			 , @TOKEN_PROCESO UNIQUEIDENTIFIER = NEWID()

	   				--Creo el cursor con los datos de los detalles de modelo de cobro que se debe modificar
				--BUSCA SOLO LOS CLIENTES A QUIEN SE DEBE APLICAR EL REAJUSTE
			   DECLARE CR_id_clientes CURSOR FOR
				SELECT MODD.valor
					 , MODD.id_modelo_cobro
					 , MODD.id_detalle_modelo_cobro
					 , CONT.id_cliente
					 , CONT.id_contrato
					 , REAJ.REAJ_CODIGO
					 , (SELECT COUNT(REDE2.READ_REAJ_CODIGO) FROM TB_SGS_REAJUSTE_DETALLE REDE2 WHERE REDE2.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO) CANTIDAD_CONFIGURACION
				  FROM contratos			   CONT with(nolock)
			INNER JOIN puntos_servicio		   PUNT with(nolock) ON PUNT.id_contrato	   = CONT.id_contrato
			INNER JOIN modelo_cobro			   MODE with(nolock) ON PUNT.id_punto_servicio = MODE.id_punto_servicio
			INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON MODE.id_modelo		   = MODD.id_modelo_cobro
			INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
			INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
				 WHERE /*CONT.id_unidad  = @ID_UNIDAD
				   AND */REDE.READ_MES = ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))
				   AND MODD.valor > 0;
			


		  --Abre el cursor y recorro a los clientes.
		  OPEN CR_id_clientes
		 FETCH NEXT FROM CR_id_clientes
		  INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
		 WHILE @@FETCH_STATUS = 0
			 BEGIN

					IF (@CANTIDAD_CONFIGURACION = 1)
						BEGIN
							--SI LA CONFIGURACIÓN ES UNA SOLA ES PORQUE ES ANUAL Y TOMA DESDE EL MISMO MES PERO DEL AÑO PASADO
							SELECT @MES_ULTIMO_REAJUSTE  = REDE.READ_MES 
								 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1 
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
						END
					ELSE
						BEGIN
							
							SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
							     , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))
							  FROM contratos			   CONT with(nolock)
						INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
						INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
							 WHERE CONT.id_contrato = @ID_CONTRATO
							   AND REDE.READ_MES < ISNULL(@MES_REAJUSTE, DATEPART(MM,GETDATE()))

							   IF (@MES_ULTIMO_REAJUSTE IS NULL)
								   BEGIN
										SELECT @MES_ULTIMO_REAJUSTE  = MAX(REDE.READ_MES)
											 , @ANIO_ULTIMO_REAJUSTE = ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE())) - 1
										  FROM contratos			   CONT with(nolock)
									INNER JOIN TB_SGS_REAJUSTE		   REAJ with(nolock) ON REAJ.REAJ_ID_CONTRATO = CONT.id_contrato
									INNER JOIN TB_SGS_REAJUSTE_DETALLE REDE with(nolock) ON REDE.READ_REAJ_CODIGO = REAJ.REAJ_CODIGO
										 WHERE CONT.id_contrato = @ID_CONTRATO
								   END

						END

--------------------------------------------------------------------------------------------------------------------------------------------
--codigo comentado a raiz que el ipc utilizado en abril fue de 0.6 y la forma del procedimiento da 0.7
--------------------------------------------------------------------------------------------------------------------------------------------
					--CALCULA EL VALOR DE REAJUSTE
					SET @VALOR_REAJUSTE = (	SELECT SUM(INDV.INDV_VALOR)
											  FROM TB_SGS_INDICADOR          INDI with(nolock) 
										INNER JOIN TB_SGS_INDICADOR_VALOR    INDV with(nolock) ON INDI.INDI_CODIGO = INDV.INDV_INDI_CODIGO
										INNER JOIN TB_SGS_REAJUSTE_INDICADOR REIN with(nolock) ON REIN.REIN_INDI_CODIGO = INDI.INDI_CODIGO
											 WHERE Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') >= Convert(varchar(4), @ANIO_ULTIMO_REAJUSTE) + REPLACE(STR(@MES_ULTIMO_REAJUSTE, 2), SPACE(1), '0')
											   AND Convert(varchar(4), INDV.INDV_ANIO) + REPLACE(STR(INDV.INDV_MES, 2), SPACE(1), '0') <  Convert(varchar(4), ISNULL(@ANIO_REAJUSTE, DATEPART(YYYY,GETDATE()))) +  Convert(varchar(2), REPLACE(STR(@MES_REAJUSTE, 2), SPACE(1), '0'))
											   AND REIN.REIN_REAJ_CODIGO = @REAJ_CODIGO)
--------------------------------------------------------------------------------------------------------------------------------------------
					--SET @VALOR_REAJUSTE = 0.6
--------------------------------------------------------------------------------------------------------------------------------------------
					IF (@VALOR_REAJUSTE > 0)
					BEGIN
						--INSERTO EL VALOR DEL DETALLE DE MODELO ACTUAL A LA TABLA HISTORICOS.
						INSERT TB_AUX_REAJUSTE(	[AUXI_ID_CONTRATO]	,
												[AUXI_ULTIMO_MES_REAJUSTE],
												[AUXI_FACTOR_REAJUSTE],
												[AUXI_ID_DETALLE_MODELO_COBRO],
												[AUXI_VALOR_ACTUAL],
												[AUXI_VALOR_NUEVO],
												[AUXI_TOKEN_PROCESO],
												[AUXI_MES],
												[AUXI_ANIO])
								   values(	@ID_CONTRATO
										 ,  convert(varchar(10) ,@MES_ULTIMO_REAJUSTE) + '-' + convert(varchar(10) , @ANIO_ULTIMO_REAJUSTE)
										 ,  @VALOR_REAJUSTE
										 ,	@ID_DETALLE_MODELO_COBRO
										 ,	@VALOR
										 ,	round(@VALOR + ((@VALOR * @VALOR_REAJUSTE)/100),0)
										 ,  @TOKEN_PROCESO
										 ,  @MES_REAJUSTE
										 ,  @ANIO_REAJUSTE)
					
					END
					--Próximo cliente
					FETCH NEXT FROM CR_id_clientes
					INTO @VALOR, @ID_MODELO_COBRO, @ID_DETALLE_MODELO_COBRO, @ID_CLIENTE, @ID_CONTRATO, @REAJ_CODIGO, @CANTIDAD_CONFIGURACION
			   END 
		 CLOSE CR_id_clientes;
	DEALLOCATE CR_id_clientes;
    



	 SELECT MODD.id_modelo_cobro
		  , TMOD.tipo_modelo
		  , MODD.id_detalle_modelo_cobro
		  , MODA.tipo_modalidad
		  , CONT.id_cliente
		  , CLIE.razon_social
		  , CONT.id_contrato
		  , SUCU.descripcion
		  , RESU.AUXI_VALOR_ACTUAL
		  , RESU.AUXI_VALOR_NUEVO
		  , RESU.AUXI_FACTOR_REAJUSTE 
		  , (SELECT MAX(HMOD.HMCD_FECHA)
		       FROM TB_HIS_MODELO_COBRO_DETALLE HMOD
			  WHERE HMOD.HMCD_ID_DETALLE_MODELO_COBRO = MODD.id_detalle_modelo_cobro) Fecha_Ultimo_Reajuste
		  , RESU.AUXI_MES
		  , RESU.AUXI_ANIO
	   FROM TB_AUX_REAJUSTE         RESU with(nolock)
 INNER JOIN modelo_cobro_detalle    MODD with(nolock) ON RESU.AUXI_ID_DETALLE_MODELO_COBRO	= MODD.id_detalle_modelo_cobro
 INNER JOIN modelo_cobro			MODE with(nolock) ON MODD.id_modelo_cobro				= MODE.id_modelo
 INNER JOIN puntos_servicio		    PUNT with(nolock) ON PUNT.id_punto_servicio				= MODE.id_punto_servicio
 INNER JOIN contratos			    CONT with(nolock) ON PUNT.id_contrato					= CONT.id_contrato
 INNER JOIN clientes				CLIE with(nolock) ON CONT.id_cliente					= CLIE.id_cliente
 INNER JOIN tipo_modelo			    TMOD with(nolock) ON MODE.id_tipo_modelo				= TMOD.id_tipo_modelo
 INNER JOIN tipo_modalidad		    MODA with(nolock) ON MODD.id_tipo_modalidad				= MODA.id_tipo_modalidad
 INNER JOIN sucursales			    SUCU with(nolock) ON SUCU.id_sucursal					= CONT.id_sucursal

	

	DELETE FROM TB_AUX_REAJUSTE WHERE AUXI_TOKEN_PROCESO = @TOKEN_PROCESO
	

END
