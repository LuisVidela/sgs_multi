USE [RS_SGS_ARAUCO]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_CERTIFICADO_REPORTE]    Script Date: 10-04-2017 11:07:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Intellego,José Joaquín Sepúlveda>
-- Create date: <02/11/2016>
-- Update date: <10/04/2017>
-- Description:	<Generación de procedimiento almacenado para recuperación de certificados.>
--				<Se agregan Observaciones y separa información de M3 y peso en columnas independientes>
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_CERTIFICADO_REPORTE]
    @codigo_unidad INT = NULL ,
    @fechaDesde DATETIME = NULL -- fecha comienzo
    ,
    @fechaHasta DATETIME = NULL -- fecha fin periodo
    ,
    @IdSucursal INT = NULL ,
    @IdVertedetro INT = NULL ,
    @idPuntoServicio INT NULL
AS
    BEGIN
        DECLARE @per1 VARCHAR(4); 
        DECLARE @per2 VARCHAR(2);
        SET @per1 = DATEPART(YEAR, @fechaHasta);
        SET @per2 = RIGHT('0' + RTRIM(DATEPART(MONTH, @fechaHasta)), 2);

        INSERT  INTO tb_certificados
                SELECT TOP ( 1 )
                        GETDATE() ,
                        con.id_contrato ,
                        suc.id_sucursal ,
                        ver.id_vertedero ,
                        ( CONCAT(@per1, '-', @per2) )
                FROM    contratos con
                        INNER JOIN sucursales suc ON suc.id_sucursal = con.id_sucursal
                        INNER JOIN puntos_servicio pos ON con.id_contrato = pos.id_contrato
                        INNER JOIN clientes AS cli ON con.id_cliente = cli.id_cliente
                        INNER JOIN vertederos AS ver ON ver.id_vertedero = pos.id_vertedero
                WHERE   suc.id_sucursal = @IdSucursal
                        AND ver.id_vertedero = @IdVertedetro
                        AND ( @idPuntoServicio IS NULL
                              OR ( pos.id_punto_servicio = @idPuntoServicio )
                            ); 

        SELECT  MAX(certi.id_certificado) AS 'NroCertificado' ,
                vert.pertenece_resiter ,
                REPLACE(REPLACE(REPLACE(vert.direccion_vertedero, 'Avenida ',
                                        ''), 'Calle ', ''), 'Pasaje ', '') AS 'direccion_vertedero' ,
                vert.vertedero ,
                com.nombre_comuna ,
                uni.numero_resolucion ,
                uni.represenante_legal ,
                uni.rut_rep_legal ,
                cli.razon_social AS 'RazonSocial' ,
                cli.rut_cliente AS 'Rut' ,
                @fechaHasta AS 'Periodo' ,
                GETDATE() AS 'fechacertificado' ,
				case when pos.cobro_por_m3 = 1 then SUM(hdetalle.peso) else 0 end AS 'm3' ,
				case when pos.cobro_por_peso = 1 then SUM(hdetalle.peso/1000) else 0 end AS 'peso' ,
                hoja.fecha AS 'FechaGuiaDespacho' ,
                'Disposición de Residuos Sólidos ' AS 'GlosaServicio' ,
                case when pos.cobro_por_m3 = 1 then hdetalle.peso else 0 end AS 'm3Body' ,
				case when pos.cobro_por_peso = 1 then hdetalle.peso/1000 else 0 end AS 'pesoBody' ,
                hdetalle.codigo_externo AS 'GuiaDespacho' ,
                pos.nombre_punto ,
                res.residuo AS 'TipoResiduo' ,
                equip.tipo_equipo AS 'TipoContenedor',
				hdetalle.observacion AS Observaciones
        FROM    hoja_ruta AS hoja
                INNER JOIN hoja_ruta_detalle AS hdetalle ON hdetalle.id_hoja_ruta = hoja.id_hoja_ruta
                INNER JOIN puntos_servicio AS pos ON pos.id_punto_servicio = hdetalle.id_punto_servicio
                INNER JOIN residuos AS res ON res.id_residuo = pos.id_residuo
                INNER JOIN equipos AS equip ON equip.id_equipo = pos.id_equipo
                INNER JOIN contratos AS cont ON cont.id_contrato = pos.id_contrato
                INNER JOIN clientes AS cli ON cont.id_cliente = cli.id_cliente
                INNER JOIN tb_certificados AS certi ON certi.id_sucursal = cont.id_sucursal
                INNER JOIN vertederos AS vert ON certi.id_vertedero = vert.id_vertedero
                INNER JOIN unidades_negocio AS uni ON uni.id_unidad = cont.id_unidad
                INNER JOIN comunas AS com ON com.id_comuna = vert.id_comuna
        WHERE   hoja.estado = 3
                AND hdetalle.estado = 2
                AND hoja.fecha >= @fechaDesde
                AND hoja.fecha <= @fechaHasta
                AND cont.id_unidad = @codigo_unidad
                AND ( @idPuntoServicio IS NULL
                      OR ( pos.id_punto_servicio = @idPuntoServicio )
                    )
                AND cont.id_sucursal = @IdSucursal
                AND certi.id_vertedero = @IdVertedetro
        GROUP BY cli.razon_social ,
                cli.rut_cliente ,
                hoja.fecha ,
                hdetalle.peso ,
                hdetalle.codigo_externo ,
                pos.nombre_punto ,
                res.residuo ,
                equip.tipo_equipo ,
                vert.pertenece_resiter ,
                vert.vertedero ,
                uni.numero_resolucion ,
                uni.represenante_legal ,
                uni.rut_rep_legal ,
                vert.direccion_vertedero ,
                com.nombre_comuna,
				hdetalle.observacion,
				pos.cobro_por_peso,
				pos.cobro_por_m3
        ORDER BY hoja.fecha ASC;
    END;
