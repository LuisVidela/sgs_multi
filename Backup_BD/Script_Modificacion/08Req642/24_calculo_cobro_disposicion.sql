/*
   viernes, 21 de abril de 201712:06:11
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO_QA
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_calculo_cobro_disposicion
	(
	id_calculo_disposicion int NOT NULL IDENTITY (1, 1),
	id_cliente int NULL,
	rut_cliente varchar(10) NULL,
	Fecha varchar(30) NULL,
	id_contrato int NULL,
	id_punto_servicio int NULL,
	glosa varchar(50) NULL,
	Disposicion decimal(18, 2) NULL,
	CobroTonelada float(53) NULL,
	Valor decimal(18, 2) NULL,
	valoruf float(53) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_calculo_cobro_disposicion SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_calculo_cobro_disposicion ON
GO
IF EXISTS(SELECT * FROM dbo.calculo_cobro_disposicion)
	 EXEC('INSERT INTO dbo.Tmp_calculo_cobro_disposicion (id_calculo_disposicion, id_cliente, rut_cliente, Fecha, id_contrato, id_punto_servicio, glosa, Disposicion, CobroTonelada, Valor, valoruf)
		SELECT id_calculo_disposicion, id_cliente, rut_cliente, Fecha, id_contrato, id_punto_servicio, glosa, Disposicion, CobroTonelada, CONVERT(decimal(18, 2), Valor), valoruf FROM dbo.calculo_cobro_disposicion WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_calculo_cobro_disposicion OFF
GO
DROP TABLE dbo.calculo_cobro_disposicion
GO
EXECUTE sp_rename N'dbo.Tmp_calculo_cobro_disposicion', N'calculo_cobro_disposicion', 'OBJECT' 
GO
ALTER TABLE dbo.calculo_cobro_disposicion ADD CONSTRAINT
	PK_calculo_cobro_disposicion PRIMARY KEY CLUSTERED 
	(
	id_calculo_disposicion
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
