USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_CALCULO_PUNTO_OLD_CLOSED]    Script Date: 30-03-2017 11:52:09 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_CALCULO_PUNTO_OLD_CLOSED]
AS
SELECT  0 AS id_calculo ,
        CASE WHEN vpt.Id_cliente IS NULL THEN cobr.id_cliente
             ELSE vpt.Id_cliente
        END AS Id_cliente ,
        CASE WHEN vpt.rut_cliente IS NULL THEN cobr.rut_cliente
             ELSE vpt.rut_cliente
        END AS rut_cliente ,
        CASE WHEN vpt.id_contrato IS NULL THEN cobr.id_contrato
             ELSE vpt.id_contrato
        END AS id_contrato ,
        CASE WHEN vpt.razon_social IS NULL THEN cli.razon_social
             ELSE vpt.razon_social
        END AS razon_social ,
        CASE WHEN vpt.nombre_fantasia IS NULL THEN cli.nombre_fantasia
             ELSE vpt.nombre_fantasia
        END AS nombre_fantasia ,
        CASE WHEN vpt.Sucursal IS NULL THEN suc.sucursal
             ELSE vpt.Sucursal
        END AS Sucursal ,
        CASE WHEN vpt.id_punto_servicio IS NULL THEN cobr.id_punto_servicio
             ELSE vpt.id_punto_servicio
        END AS id_punto_servicio ,
        CASE WHEN vpt.nombre_punto IS NULL THEN ps.nombre_punto
             ELSE vpt.nombre_punto
        END AS nombre_punto ,
        ISNULL(vpt.Numero_Retiros, 0) AS Numero_Retiros ,
        CASE WHEN vpt.Valor IS NULL THEN cobr.Valor
             ELSE vpt.Valor
        END AS Valor ,
        CASE WHEN vpt.IVA IS NULL THEN 0
             ELSE vpt.IVA
        END AS IVA ,
        CASE WHEN vpt.NETO IS NULL THEN 0
             ELSE vpt.NETO
        END AS NETO ,
        CASE WHEN vpt.Peso IS NULL THEN cobr.Disposicion
             ELSE vpt.Peso
        END AS Peso ,
        CASE WHEN vpt.Fecha IS NULL THEN cobr.Fecha
             ELSE vpt.Fecha
        END AS Fecha ,
        CASE WHEN vpt.Monthis IS NULL THEN SUBSTRING(cobr.Fecha, 1, 2)
             ELSE vpt.Monthis
        END AS Monthis ,
        CASE WHEN vpt.Yearis IS NULL THEN SUBSTRING(cobr.Fecha, 3, 6)
             ELSE vpt.Yearis
        END AS Yearis ,
        CASE WHEN vpt.id_sucursal IS NULL THEN con.id_sucursal
             ELSE vpt.id_sucursal
        END AS id_sucursal ,
        ISNULL(cobr.Valor, 0) AS ValorTotalDisposicion ,
        CASE WHEN vpt.Valor IS NULL
                  OR CONVERT(INT, vpt.Valor) = cobr.Valor
             THEN ISNULL(cobr.Valor, 0)
             ELSE ISNULL(vpt.Valor, 0) + ISNULL(cobr.Valor, 0)
        END AS ValorTotal ,
        ISNULL(nore.Numero_No_Retiros, 0) AS Numero_No_Retiros
FROM    dbo.cobro_disposicion_cerrado AS cobr
        LEFT OUTER JOIN dbo.V_PUNTO_SIN_DISPOCICION_CLOSE AS vpt ON vpt.id_punto_servicio = cobr.id_punto_servicio
		AND vpt.Fecha = cobr.Fecha
        LEFT OUTER JOIN dbo.V_CONTRATO_NO_RETIROS_CLOSED AS nore ON vpt.Id_cliente = nore.id_cliente
                                                             AND vpt.id_contrato = nore.id_contrato
                                                             AND vpt.id_punto_servicio = nore.id_punto_servicio
                                                             AND vpt.Fecha = nore.Fecha
        INNER JOIN dbo.clientes AS cli ON cobr.id_cliente = cli.id_cliente
        INNER JOIN dbo.contratos AS con ON cobr.id_contrato = con.id_contrato
        INNER JOIN dbo.puntos_servicio AS ps ON cobr.id_punto_servicio = ps.id_punto_servicio
        INNER JOIN dbo.sucursales AS suc ON con.id_sucursal = suc.id_sucursal;

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[15] 2[26] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "cobr"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 245
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "vpt"
            Begin Extent = 
               Top = 6
               Left = 283
               Bottom = 136
               Right = 464
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "nore"
            Begin Extent = 
               Top = 6
               Left = 502
               Bottom = 136
               Right = 697
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "cli"
            Begin Extent = 
               Top = 6
               Left = 735
               Bottom = 136
               Right = 918
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "con"
            Begin Extent = 
               Top = 138
               Left = 38
               Bottom = 268
               Right = 264
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "ps"
            Begin Extent = 
               Top = 138
               Left = 302
               Bottom = 268
               Right = 525
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "suc"
            Begin Extent = 
               Top = 138
               Left = 563
               Bottom = 268
               Right = 747
            End
            DisplayFlags = 280
            TopColumn = 0
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_PUNTO_OLD_CLOSED'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_PUNTO_OLD_CLOSED'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'V_CALCULO_PUNTO_OLD_CLOSED'
GO


