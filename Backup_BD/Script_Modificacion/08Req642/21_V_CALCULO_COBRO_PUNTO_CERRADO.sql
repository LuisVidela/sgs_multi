USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]    Script Date: 03-04-2017 11:29:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[V_CALCULO_COBRO_PUNTO_CERRADO]
AS
SELECT  0 AS id_calculo ,
        id_hoja_ruta ,
        id_cliente ,
        rut_cliente ,
        id_contrato ,
        razon_social ,
        nombre_fantasia ,
        sucursal ,
        CONVERT(VARCHAR, ISNULL(fechaFull, '01/01/1945'), 103) AS fechaFull ,
        id_punto_servicio ,
        nombre_punto ,
        0 AS Numero_Retiros ,
        bruto AS Valor ,
        0 AS IVA ,
        0 AS NETO ,
        CAST(peso AS DECIMAL(18, 2)) AS peso ,
        cantidad AS Volteos ,
        Fecha ,
        Monthis ,
        Yearis ,
        Orden ,
        tipo_modalidad ,
        sorting ,
        glosa ,
        cobro_arriendo ,
        conductor ,
        camion ,
        como_se_calcula
FROM    dbo.cobro_clientes_cerrado;
GO
