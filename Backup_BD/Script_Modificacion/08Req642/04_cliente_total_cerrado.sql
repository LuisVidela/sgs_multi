/*
   martes, 28 de marzo de 201711:28:21
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.cliente_total_cerrado ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.cliente_total_cerrado SET (LOCK_ESCALATION = TABLE)
GO
delete cliente_total_cerrado WHERE id_unidad IS NULL
GO
COMMIT
