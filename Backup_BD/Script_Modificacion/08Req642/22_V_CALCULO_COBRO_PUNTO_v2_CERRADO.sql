USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_CALCULO_COBRO_PUNTO_v2]    Script Date: 03-04-2017 12:01:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[V_CALCULO_COBRO_PUNTO_v2_CERRADO]
AS
SELECT  0 AS id_calculo,
		hr.id_hoja_ruta,
		cc.id_cliente,
		cc.rut_cliente,
		cc.razon_social,
		cc.nombre_fantasia,
		cc.Sucursal,
		CONVERT(VARCHAR,ISNULL(cc.fechaFull,'01/01/1945'),103) AS fechaFull,
		cc.id_punto_servicio,
		cc.nombre_punto,
		0 AS Numero_Retiros,
	    cc.peso AS Peso,
		cc.cantidad AS Volteos,		
		cc.BRUTO AS Valor,
	    cc.Fecha AS Fecha,
		cc.Orden,
		cc.tipo_modalidad,
		cc.sorting,
		cc.glosa,
		cc.cobro_arriendo,
		cc.conductor,
		cc.camion,
		cc.como_se_calcula,hr.codigo_externo
		FROM dbo.cobro_clientes_cerrado cc
		INNER JOIN puntos_servicio ps ON cc.id_punto_servicio=ps.id_punto_servicio
  LEFT JOIN hoja_ruta_detalle hr
  ON cc.id_hoja_ruta=hr.id_hoja_ruta AND hr.Orden=SUBSTRING(CONVERT(VARCHAR,cc.orden),5,100)
GO