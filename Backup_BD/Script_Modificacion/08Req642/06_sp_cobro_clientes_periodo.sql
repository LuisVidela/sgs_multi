USE [RS_SGS_ARAUCO];
GO
/****** Object:  StoredProcedure [dbo].[sp_cobro_clientes_periodo]    Script Date: 27-03-2017 12:31:03 ******/
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
ALTER PROCEDURE [dbo].[sp_cobro_clientes_periodo]
    (
      @Cerrar VARCHAR(20) ,
      @PeriodoSP VARCHAR(30) ,
      @id_unidad INT

    )
AS
    DECLARE @Hoy VARCHAR(30);
    DECLARE @existe INT;

    DECLARE @id_contrato INT;
    DECLARE @id_punto_servicio INT;
    DECLARE @periodo VARCHAR(30);
    DECLARE @inicio DATETIME;
    DECLARE @fin DATETIME;
    DECLARE @existen_datos INT;
    DECLARE @estado_cursor INT;
    DECLARE @mesarriendo VARCHAR(50);
    DECLARE @cobro_arriendo VARCHAR(50);
    DECLARE @arriendo VARCHAR(50);

    DECLARE @periodo_cobro VARCHAR(20);

    DECLARE @Erase VARCHAR(20);
    DECLARE @PuntoServicio VARCHAR(20);
    DECLARE @DatosAnexos VARCHAR(20);

    DECLARE @cerrar_proceso INT;
    DECLARE @proceso_fue_cerrado INT;
    DECLARE @mescobro VARCHAR(20);
    DECLARE @yearcobro VARCHAR(20);

    DECLARE @firstday DATETIME;
    DECLARE @Cierre DATETIME;
    DECLARE @valoruf FLOAT;
    DECLARE @mesuf INT;
    DECLARE @yearuf INT;

    DECLARE @veces INT;
    DECLARE @Tarifa INT;
    DECLARE @id_contratox INT;
    DECLARE @id_punto_serviciox INT;

    SET NOCOUNT ON;

    IF @Cerrar = 'SI'
        BEGIN 
            SET @cerrar_proceso = 1;
        END;
    ELSE
        BEGIN
            IF @Cerrar = 'NO'
                BEGIN
                    SET @cerrar_proceso = 0;
                END;
            ELSE
                BEGIN
                    RAISERROR ('Debe indicar si desea CERRAR el proceso. Ingrese SI o NO', 16, 1);
                    EXEC sp_log_calculo_cobro 'ERROR',
                        'Debe indicar si desea CERRAR el proceso. Ingrese SI o NOs';
                    RETURN;
                END;
        END;

	---- DETERMINACION DEL PERIODO DE COBRO
    SET @mesarriendo = SUBSTRING(@PeriodoSP, 1, 2);
    SET @mescobro = SUBSTRING(@PeriodoSP, 1, 2);
    SET @yearcobro = SUBSTRING(@PeriodoSP, 3, 4);

    IF LEN(@mescobro) = 1
        BEGIN
            SET @mescobro = '0' + @mescobro;
        END;
    IF LEN(@mesarriendo) = 1
        BEGIN
            SET @mesarriendo = '0' + @mesarriendo;
        END;
    SET @periodo_cobro = ( @mescobro + @yearcobro );
    SET @cobro_arriendo = ( '01/' + @mesarriendo + '/' + @yearcobro );
	---- Set @arriendo = ('01-' + @mesarriendo + '-' + @yearcobro)
    SET @arriendo = ( @yearcobro + '-' + @mesarriendo + '-' + '01' );
	
	--- DETERMINACION DE LAS FECHAS DE COBRO
    EXECUTE sp_cobro_empresas_periodo @PeriodoSP;

	--- *****************************************************************************
    BEGIN TRANSACTION;

    DELETE  FROM calculo_cobro_clientes;
    DELETE  FROM calculo_cobro_disposicion;
    DELETE  FROM cobro_cliente_total;

										--JPIZARRO 09-05-2016 *** Borra cualquier registro de cobro dentro del periodo seleccionado
    DELETE  FROM [calculo_realizado]
    WHERE   Fecha LIKE @PeriodoSP;
    DELETE  FROM cliente_total_cerrado
    WHERE   Fecha LIKE @PeriodoSP
            AND id_unidad = @id_unidad;
    DELETE  FROM cobro_clientes_cerrado
    WHERE   Fecha LIKE @PeriodoSP
            AND id_unidad = @id_unidad;
    DELETE  FROM cobro_disposicion_cerrado
    WHERE   Fecha LIKE @PeriodoSP
            AND id_unidad = @id_unidad;

    UPDATE  T1
    SET     T1.cantidad = 0
    FROM    hoja_ruta_detalle T1
            INNER JOIN V_CONTRATO_PUNTO_SERVICIO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN V_PUNTO_HOOK hook ON T1.id_punto_servicio = hook.id_punto_servicio
            INNER JOIN hoja_ruta T3 ON T1.id_hoja_ruta = T3.id_hoja_ruta
                                       AND T3.fecha BETWEEN T2.inicio AND T2.fin
    WHERE   T3.estado = 3			---- hoja de ruta cerrada
            AND T1.estado = 2			---- punto de servicio realizado
            AND T1.peso > 0;

    UPDATE  T1
    SET     T1.cantidad = 1
    FROM    hoja_ruta_detalle T1
            INNER JOIN V_CONTRATO_PUNTO_SERVICIO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN V_PUNTO_HOOK hook ON T1.id_punto_servicio = hook.id_punto_servicio
            INNER JOIN hoja_ruta T3 ON T1.id_hoja_ruta = T3.id_hoja_ruta
                                       AND T3.fecha BETWEEN T2.inicio AND T2.fin
    WHERE   T3.estado = 3			---- hoja de ruta cerrada
            AND T1.estado = 2			---- punto de servicio realizado
            AND T1.peso = 0
					--update JPIZARRO
            AND ( T1.cantidad = 0
                  OR T1.cantidad IS NULL
                );
					
    UPDATE  T1
    SET     T1.peso = 0
    FROM    hoja_ruta_detalle T1
            INNER JOIN V_CONTRATO_PUNTO_SERVICIO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN V_PUNTO_FRONT_LOADER loader ON T1.id_punto_servicio = loader.id_punto_servicio
            INNER JOIN hoja_ruta T3 ON T1.id_hoja_ruta = T3.id_hoja_ruta
                                       AND T3.fecha BETWEEN T2.inicio AND T2.fin
    WHERE   T3.estado = 3			---- hoja de ruta cerrada
            AND T1.estado = 2			---- punto de servicio realizado
            AND T1.cantidad > 0;
					
					--- CAJA RECOLECTORA
					--- UPDATE PARA QUE TOME LOS REGISTROS
					--- ASIGNANDOLE UNA CANTIDAD = 1										
    UPDATE  T1
    SET     T1.cantidad = 1 ,
            T1.peso = 0
    FROM    hoja_ruta_detalle T1
            INNER JOIN V_CONTRATO_PUNTO_SERVICIO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN V_PTO_CAJA_RECOLECTORA3 loader ON T1.id_punto_servicio = loader.id_punto_servicio
            INNER JOIN hoja_ruta T3 ON T1.id_hoja_ruta = T3.id_hoja_ruta
                                       AND T3.fecha BETWEEN T2.inicio AND T2.fin
    WHERE   T3.estado = 3			---- hoja de ruta cerrada
            AND T1.estado = 2;			---- punto de servicio realizado
															  
    COMMIT TRANSACTION;
	--- *****************************************************************************

	---- VALIDAR SI EL PROCESO PARA EL PERIODO DE COBRO SE ENCUENTRA CERRADO
    SET @proceso_fue_cerrado = ( SELECT ISNULL(COUNT(*), 0)
                                 FROM   calculo_realizado
                                 WHERE  Fecha = @periodo_cobro
                                        AND Cerrado = 1
                               );

    IF @proceso_fue_cerrado > 0
        BEGIN
            RAISERROR ('El proceso de Calculo se encuentra CERRADO', 16, 1);
            EXEC sp_log_calculo_cobro 'ERROR',
                'El proceso de Calculo se encuentra CERRADO';
            RETURN;
        END;

    SET @existe = ( SELECT  ISNULL(COUNT(*), 0)
                    FROM    V_CONTRATO_PUNTO_SERVICIO
                  );

    IF @existe = 0
        BEGIN
            RAISERROR ('Error Calculo Cobro Clientes - calculo_cobro_empresas', 16, 1);
            EXEC sp_log_calculo_cobro 'ERROR',
                'Error Calculo Cobro Clientes - calculo_cobro_empresas';
            RETURN;
        END;

    SELECT  @Erase = 'BorraCobroClientes';
    SELECT  @PuntoServicio = 'PuntoServicio';
    SELECT  @DatosAnexos = 'DatosAnexos';
		
	---- PRIMERO SE DEBE VALIDAR QUE EXISTEN PUNTOS DE SERVICIOS EN HOJA_RUTA_DETALLE
	---- PARA EL PERIODO DE FACTURACION EN PROCESO	
    ---- CADA CONTRATO TIENE SU PROPIO PERIODO DE BUSQUEDA VER TABLA  calculo_cobro_empresas	       

    BEGIN TRANSACTION @PuntoServicio;

    BEGIN
				  
        SET @firstday = ( DATEADD(MONTH, DATEDIFF(MONTH, 0, GETDATE()) - 1, 0) );
				  			
				  ---- SE PROCESA LA INFORMACION PUES EXISTEN DATOS
				  ---- SE INSERTAN EN LA TABLA calculo_cobro_clientes 

        INSERT  INTO calculo_cobro_clientes
                SELECT  hoja.id_hoja_ruta ,
                        cob.id_punto_servicio ,
                        hoja.cantidad ,
                        hoja.peso ,
                        hoja.estado ,
                        ruta.fecha ,
                        @periodo_cobro ,
                        CONVERT(INT, @mescobro) AS Monthis ,
                        CONVERT(INT, @yearcobro) AS Yearis ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        deta.id_tipo_modalidad , --- id_tipo_modalidad
                        deta.valor , -- VALOR desde modelo_detalle_cobro
                        NULL , --- tipo_modalidad
                        NULL ,
                        NULL ,
                        0 ,
                        NULL ,
                        NULL ,
                        deta.id_detalle_modelo_cobro ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL
                FROM    modelo_cobro cob
                        INNER JOIN puntos_servicio pto ON cob.id_punto_servicio = pto.id_punto_servicio
                        INNER JOIN contratos CTR ON pto.id_contrato = CTR.id_contrato
                                                    AND CTR.id_unidad = @id_unidad
                        INNER JOIN modelo_cobro_detalle deta ON cob.id_modelo = deta.id_modelo_cobro
                        INNER JOIN hoja_ruta_detalle hoja ON cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
                        INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON hoja.id_punto_servicio = perio.id_punto_servicio
                        INNER JOIN hoja_ruta ruta ON hoja.id_hoja_ruta = ruta.id_hoja_ruta
                                                     AND ruta.fecha BETWEEN perio.inicio AND perio.fin
                                                     AND deta.valor IS NOT NULL
                                                     AND deta.valor > 0
                                                     AND deta.id_tipo_modalidad NOT IN (
                                                     5, 7, 9, 10, 11, 12, 8,
                                                     17 )
                                                     AND hoja.cantidad > 0
                                                     AND ruta.estado = 3			---- hoja de ruta cerrada
                                                     AND hoja.estado = 2;			---- punto de servicio realizado

        INSERT  INTO calculo_cobro_clientes
                SELECT  hoja.id_hoja_ruta ,
                        cob.id_punto_servicio ,
                        hoja.cantidad ,
                        hoja.peso ,
                        hoja.estado ,
                        ruta.fecha ,
                        @periodo_cobro ,
                        CONVERT(INT, @mescobro) AS Monthis ,
                        CONVERT(INT, @yearcobro) AS Yearis ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        deta.id_tipo_modalidad , --- id_tipo_modalidad
                        deta.valor , -- VALOR desde modelo_detalle_cobro
                        NULL , --- tipo_modalidad
                        NULL ,
                        NULL ,
                        0 ,
                        NULL ,
                        NULL ,
                        deta.id_detalle_modelo_cobro ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL ,
                        NULL
                FROM    modelo_cobro cob
                        INNER JOIN puntos_servicio pto ON cob.id_punto_servicio = pto.id_punto_servicio
                        INNER JOIN contratos CTR ON pto.id_contrato = CTR.id_contrato
                                                    AND CTR.id_unidad = @id_unidad
                        INNER JOIN modelo_cobro_detalle deta ON cob.id_modelo = deta.id_modelo_cobro
                        INNER JOIN hoja_ruta_detalle hoja ON cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
                        INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON hoja.id_punto_servicio = perio.id_punto_servicio
                        INNER JOIN hoja_ruta ruta ON hoja.id_hoja_ruta = ruta.id_hoja_ruta
                                                     AND ruta.fecha BETWEEN perio.inicio AND perio.fin
                                                     AND deta.valor IS NOT NULL
                                                     AND deta.valor > 0
                                                     AND deta.id_tipo_modalidad NOT IN (
                                                     7, 9, 10, 11, 12 )
								--And hoja.peso > 0
                                                     AND deta.id_tipo_modalidad IN (
                                                     8, 17 )
                                                     AND ruta.estado = 3			---- hoja de ruta cerrada
                                                     AND hoja.estado = 2;			---- punto de servicio realizado

        IF @@error <> 0
            BEGIN					
                ROLLBACK TRANSACTION @PuntoServicio;
                EXEC sp_log_calculo_cobro 'ERROR',
                    'Error Calculo Cobro Clientes';
                RAISERROR ('Error Calculo Cobro Clientes', 16, 1);
                RETURN;
            END;	

    END;

    COMMIT TRANSACTION @PuntoServicio;

		---- SE CARGAN LOS DATOS ANEXOS A LOS PUNTOS DE SERVICIO

    BEGIN TRANSACTION @DatosAnexos;

					---- SERVICIOS HOOK QUE SOLO TIENEN ASOCIADO MODALIDAD = 8
					---- PARA ESTOS CASOS MODALIDAD = 6 NO EXISTE
    INSERT  INTO calculo_cobro_clientes
            SELECT  hoja.id_hoja_ruta ,
                    cob.id_punto_servicio ,
                    hoja.cantidad ,
                    hoja.peso ,
                    hoja.estado ,
                    ruta.fecha ,
                    @periodo_cobro ,
                    CONVERT(INT, @mescobro) AS Monthis ,
                    CONVERT(INT, @yearcobro) AS Yearis ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    deta.id_tipo_modalidad , --- id_tipo_modalidad
                    deta.valor , -- VALOR desde modelo_detalle_cobro
                    NULL , --- tipo_modalidad
                    NULL ,
                    NULL ,
                    0 ,
                    NULL ,
                    NULL ,
                    deta.id_detalle_modelo_cobro ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL
            FROM    modelo_cobro cob
                    INNER JOIN puntos_servicio pto ON cob.id_punto_servicio = pto.id_punto_servicio
                    INNER JOIN contratos CTR ON pto.id_contrato = CTR.id_contrato
                                                AND CTR.id_unidad = @id_unidad
                    INNER JOIN modelo_cobro_detalle deta ON cob.id_modelo = deta.id_modelo_cobro
                    INNER JOIN hoja_ruta_detalle hoja ON cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
                    INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON hoja.id_punto_servicio = perio.id_punto_servicio
                    INNER JOIN hoja_ruta ruta ON hoja.id_hoja_ruta = ruta.id_hoja_ruta
                                                 AND ruta.fecha BETWEEN perio.inicio AND perio.fin
                                                 AND deta.valor IS NOT NULL
                                                 AND deta.valor > 0
                                                 AND hoja.peso > 0
                                                 AND deta.id_tipo_modalidad IN (
                                                 8, 17 )
                                                 AND ruta.estado = 3			---- hoja de ruta cerrada
                                                 AND hoja.estado = 2			---- punto de servicio realizado
                                                 AND hoja.id_punto_servicio NOT IN (
                                                 SELECT id_punto_servicio
                                                 FROM   V_PUNTOS_HOOK_CALCULADOS );
							
						

    INSERT  INTO calculo_cobro_clientes
            SELECT  hoja.id_hoja_ruta ,
                    cob.id_punto_servicio ,
                    hoja.cantidad ,
                    hoja.peso ,
                    hoja.estado ,
                    ruta.fecha ,
                    @periodo_cobro ,
                    CONVERT(INT, @mescobro) AS Monthis ,
                    CONVERT(INT, @yearcobro) AS Yearis ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    8 , --- id_tipo_modalidad
                    deta.valor , -- VALOR desde modelo_detalle_cobro
                    NULL , --- tipo_modalidad
                    NULL ,
                    NULL ,
                    0 ,
                    NULL ,
                    NULL ,
                    deta.id_detalle_modelo_cobro ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL
            FROM    modelo_cobro cob
                    INNER JOIN puntos_servicio pto ON cob.id_punto_servicio = pto.id_punto_servicio
                    INNER JOIN contratos CTR ON pto.id_contrato = CTR.id_contrato
                                                AND CTR.id_unidad = @id_unidad
                    INNER JOIN modelo_cobro_detalle deta ON cob.id_modelo = deta.id_modelo_cobro
                    INNER JOIN hoja_ruta_detalle hoja ON cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
                    INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON hoja.id_punto_servicio = perio.id_punto_servicio
                    INNER JOIN V_PUNTO_HOOK hook ON hoja.id_punto_servicio = hook.id_punto_servicio
                    INNER JOIN hoja_ruta ruta ON hoja.id_hoja_ruta = ruta.id_hoja_ruta
                                                 AND ruta.fecha BETWEEN perio.inicio AND perio.fin
                                                 AND deta.valor IS NOT NULL
                                                 AND deta.valor > 0
                                                 AND hoja.peso > 0
                                                 AND deta.id_tipo_modalidad NOT IN (
                                                 8, 7 )
                                                 AND ruta.estado = 3			---- hoja de ruta cerrada
                                                 AND hoja.estado = 2			---- punto de servicio realizado
                                                 AND hoja.id_punto_servicio NOT IN (
                                                 SELECT id_punto_servicio
                                                 FROM   V_PUNTOS_HOOK_CALCULADOS );
    INSERT  INTO calculo_cobro_clientes
            SELECT  hoja.id_hoja_ruta ,
                    cob.id_punto_servicio ,
                    hoja.cantidad ,
                    hoja.peso ,
                    hoja.estado ,
                    ruta.fecha ,
                    @periodo_cobro ,
                    CONVERT(INT, @mescobro) AS Monthis ,
                    CONVERT(INT, @yearcobro) AS Yearis ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    17 , --- id_tipo_modalidad
                    deta.valor , -- VALOR desde modelo_detalle_cobro
                    NULL , --- tipo_modalidad
                    NULL ,
                    NULL ,
                    0 ,
                    NULL ,
                    NULL ,
                    deta.id_detalle_modelo_cobro ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL
            FROM    modelo_cobro cob
                    INNER JOIN puntos_servicio pto ON cob.id_punto_servicio = pto.id_punto_servicio
                    INNER JOIN contratos CTR ON pto.id_contrato = CTR.id_contrato
                                                AND CTR.id_unidad = @id_unidad
                    INNER JOIN modelo_cobro_detalle deta ON cob.id_modelo = deta.id_modelo_cobro
                    INNER JOIN hoja_ruta_detalle hoja ON cob.id_punto_servicio = hoja.id_punto_servicio
							---- SE SELECCIONAN LOS DATOS DEL CLIENTE DEPENDIENDO
							---- DE  LOS  PERIODOS  DE CIERRE DE CADA CLIENTE QUE
							---- PUEDE SER VARIABLE
                    INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON hoja.id_punto_servicio = perio.id_punto_servicio
                    INNER JOIN V_PUNTO_HOOK hook ON hoja.id_punto_servicio = hook.id_punto_servicio
                    INNER JOIN hoja_ruta ruta ON hoja.id_hoja_ruta = ruta.id_hoja_ruta
                                                 AND ruta.fecha BETWEEN perio.inicio AND perio.fin
                                                 AND deta.valor IS NOT NULL
                                                 AND deta.valor > 0
                                                 AND hoja.peso > 0
                                                 AND deta.id_tipo_modalidad NOT IN (
                                                 17 )
                                                 AND ruta.estado = 3			---- hoja de ruta cerrada
                                                 AND hoja.estado = 2			---- punto de servicio realizado
                                                 AND hoja.id_punto_servicio NOT IN (
                                                 SELECT id_punto_servicio
                                                 FROM   V_PUNTOS_HOOK_CALCULADOS );
									
								
			        ---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					---- SE TOMAN LOS DATOS DE LA TABLA modelo_cobro_detalle
					---- QUE SOLO INCLUYEN ARRIENDO Y CARGO FIJO (NO APARECEN EN HOJA_RUT)
					--UPDATE JPIZARRO inclusión de todos los cobros de arriendos sin importar si tiene servicios dentro del mes
    INSERT  INTO calculo_cobro_clientes
            SELECT	DISTINCT
                    0 ,
                    fijo.id_punto_servicio ,
                    1 , ---- cantidad
                    0 , ---- peso
                    2 , ---- estado
                    @firstday , ---- primer dia mes anterior --- dato ficticio
                    @periodo_cobro ,
                    CONVERT(INT, @mescobro) AS Monthis ,
                    CONVERT(INT, @yearcobro) AS Yearis ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    fijo.id_tipo_modalidad , --- id_tipo_modalidad
                    fijo.valor , -- VALOR desde modelo_detalle_cobro
                    NULL , --- tipo_modalidad
                    NULL ,
                    NULL ,
                    0 ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    fijo.id_detalle_modelo_cobro , --- id_detalle_modelo_cobro
                    @arriendo ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL
						 --   From V_PUNTOS_UNICOS cal
							--Inner Join V_ARRIENDO_COSTO_FIJO fijo On cal.id_punto_servicio = fijo.id_punto_servicio
							--Inner Join V_CONTRATO_PUNTO_SERVICIO perio On perio.id_punto_servicio = fijo.id_punto_servicio
							--Where cal.fecha between perio.inicio and perio.fin
							--  and 
            FROM    V_ARRIENDO_COSTO_FIJO fijo
                    INNER JOIN V_CONTRATO_PUNTO_SERVICIO perio ON perio.id_punto_servicio = fijo.id_punto_servicio
							--Where cal.fecha between perio.inicio and perio.fin
                                                              AND perio.id_unidad = @id_unidad;
			
    UPDATE  T1
    SET     T1.sistema = 'Hook'
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PUNTO_HOOK T2 ON T1.id_punto_servicio = T2.id_punto_servicio;

			----UPDATE JPIZARRO 29-02-16, MULTIBUCKET, SE DEBEN AGREGAR DE A UNO LOS TIPOS DE SERVICIO
		
    UPDATE  T1
    SET     T1.sistema = 'Multibucket'
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PUNTO_MULTIBUCKET T2 ON T1.id_punto_servicio = T2.id_punto_servicio;
			
    UPDATE  T1
    SET     T1.sistema = 'Front Loader'
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PUNTO_FRONT_LOADER T2 ON T1.id_punto_servicio = T2.id_punto_servicio;
			
    UPDATE  T1
    SET     T1.sistema = 'Caja Recolectora'
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PTO_CAJA_RECOLECTORA3 T2 ON T1.id_punto_servicio = T2.id_punto_servicio;						
				

			---update JPIZARRO

    UPDATE  ccc
    SET     ccc.sistema = tc.tipo_contrato
    FROM    calculo_cobro_clientes ccc
            INNER JOIN contratos con ON ccc.id_contrato = con.id_contrato
            INNER JOIN tipo_contrato tc ON tc.id_tipo_contrato = con.id_tipo_contrato;

			

    UPDATE  T1
    SET     T1.tipo_modalidad = T2.tipo_modalidad
    FROM    calculo_cobro_clientes T1
            INNER JOIN tipo_modalidad T2 ON T1.id_tipo_modalidad = T2.id_tipo_modalidad;

		--- DESCRIPCION DEL PUNTO DE SERVICIO
    UPDATE  T1
    SET     T1.nombre_punto = T2.nombre_punto ,
            T1.id_contrato = T2.id_contrato
    FROM    calculo_cobro_clientes T1
            INNER JOIN puntos_servicio T2 ON T1.id_punto_servicio = T2.id_punto_servicio;		

		--- DESCRIPCION DEL PUNTO DE SERVICIO
    UPDATE  T1
    SET     T1.id_cliente = T2.id_cliente ,
            T1.id_sucursal = T2.id_sucursal ,
            T1.estado_contrato = T2.estado ,
            T1.id_modelo_cobro = T2.id_modelo_cobro
    FROM    calculo_cobro_clientes T1
            INNER JOIN contratos T2 ON T1.id_contrato = T2.id_contrato;		
		 
		--- DESCRIPCION CLIENTE
    UPDATE  T1
    SET     T1.rut_cliente = T2.rut_cliente ,
            T1.giro_comercial = T2.giro_comercial ,
            T1.razon_social = T2.razon_social ,
            T1.nombre_fantasia = T2.nombre_fantasia
    FROM    calculo_cobro_clientes T1
            INNER JOIN clientes T2 ON T1.id_cliente = T2.id_cliente;		

		--- DESCRIPCION SUCURSAL
    UPDATE  T1
    SET     T1.sucursal = T2.sucursal ,
            T1.descripcion = T2.descripcion ,
            T1.rut_sucursal = T2.rut_sucursal
    FROM    calculo_cobro_clientes T1
            INNER JOIN sucursales T2 ON T1.id_sucursal = T2.id_sucursal;

		--- COMO SE CALCULA EL VALOR DEPENDIENDO DE tipo_modalidad
    UPDATE  calculo_cobro_clientes
    SET     como_se_calcula = 'CANTIDAD'
    WHERE   id_tipo_modalidad IN ( 5, 6, 7, 9, 10, 11, 12 );

		---- LOS COSTOS FIJOS SE COBRAN UNA SOLA VEZ AL MES POR LO
		---- CUAL LA CANTIDAD SE SETEA A 1
    UPDATE  calculo_cobro_clientes
    SET     cantidad = 1 ,
            peso = 0
    WHERE   id_tipo_modalidad IN ( 5, 7, 9, 10, 11, 12 );		

    UPDATE  calculo_cobro_clientes
    SET     como_se_calcula = 'PESO' ,
            cantidad = 0
    WHERE   id_tipo_modalidad IN ( 8, 17 );


		-------------------------------------------------------------------
		-------------------------------------------------------------------
		----- CALCULO DEL VALOR BRUTO DEPENDIENDO DEL CAMPO como_se_calcula
    UPDATE  calculo_cobro_clientes
    SET     bruto = 0;

    UPDATE  calculo_cobro_clientes
    SET     bruto = ( ISNULL(valor, 0) * ISNULL(cantidad, 0) )
    WHERE   como_se_calcula = 'CANTIDAD';
			
    UPDATE  calculo_cobro_clientes
    SET     bruto = ( ISNULL(valor, 0) )
    WHERE   como_se_calcula = 'PESO'
            AND id_tipo_modalidad IN ( 8, 17 );
			
			
		

					
    UPDATE  T1
    SET     T1.valor = T2.valor ,
            T1.bruto = T2.valor
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_VALOR_POR_RETIRO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
                                                AND T1.id_tipo_modalidad IN (
                                                8, 17 );
			
    UPDATE  T1
    SET     T1.valorenuf = T2.valor ,
            T1.bruto = T2.valor
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_VALOR_POR_RETIRO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
                                                AND T1.id_tipo_modalidad IN (
                                                8, 17 )
                                                AND T1.id_punto_servicio IN (
                                                17, 87, 382 );
			
    UPDATE  T1
    SET     T1.valor = T2.valor ,
            T1.bruto = T2.valor
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_VALOR_POR_RETIRO T2 ON T1.id_punto_servicio = T2.id_punto_servicio
                                                AND T1.id_tipo_modalidad = 6;

    UPDATE  calculo_cobro_clientes
    SET     bruto = ( ISNULL(cantidad, 0) * ISNULL(valor, 0) )
    WHERE   como_se_calcula = 'CANTIDAD'
            AND id_tipo_modalidad = 6;
			
	        ---- EXCEPCION PUNTO DE SERVICIOO 17 EDAM COT 20 M3//UPDATE JPIZARRO, NO CUENTA
	        ---- SE COBRA EN UF
	        ---- BUSQUEDA UF MES ANTERIOR
    SET @mesuf = CONVERT(INT, @mescobro);
    SET @yearuf = CONVERT(INT, @yearcobro);
    IF @mesuf = 1
        BEGIN
            SET @mesuf = 12;
            SET @yearuf = @yearuf - 1;
        END;
	        
    SET @valoruf = ( SELECT ISNULL(valor_uf, 0)
                     FROM   uf
                     WHERE  mes = @mesuf
                            AND año = @yearuf
                   );
	        
	        ---- EXCEPCION PUNTO DE SERVICIOO 17 EDAM COT 20 M3//UPDATE JPIZARRO, FUNCIONA CON TODOS PESO X UF
	        ---- SE COBRA EN UF
	        ---- BUSQUEDA UF MES ANTERIOR
    UPDATE  calculo_cobro_clientes
    SET     bruto = ( ISNULL(valorenuf, 0) ) * ( ISNULL(@valoruf, 0) ) ,
            valoruf = @valoruf
    WHERE   como_se_calcula = 'PESO'
            AND id_tipo_modalidad IN ( 8, 17 )
            AND id_punto_servicio IN ( 17, 87, 382 );
		
		----- FECHA QUE SE REALIZA EL CALCULO
    UPDATE  calculo_cobro_clientes
    SET     fechaCalculo = GETDATE();

	     ---- ORDEN
    UPDATE  T1
    SET     T1.Orden = T2.Orden
    FROM    calculo_cobro_clientes T1
            INNER JOIN hoja_ruta_detalle T2 ON T1.id_hoja_ruta = T2.id_hoja_ruta
                                               AND T1.id_punto_servicio = T2.id_punto_servicio;

			--- UPDATE CAMION CONDUCTOR
    UPDATE  T1
    SET     T1.id_camion = T2.id_camion ,
            T1.camion = T2.patente ,
            T1.id_conductor = T2.id_conductor ,
            T1.conductor = T2.nombre_conductor
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_CONDUCTOR_CAMION T2 ON T1.id_hoja_ruta = T2.id_hoja_ruta;

			---- UPDATE VALOR ARRIENDO DEPENDIENDO DE LA CANTIDAD
			---- DE  EQUIPOS  EN  ARRIENDO  POR PUNTO DE SERVICIO
    UPDATE  T1
    SET     T1.bruto = ( T2.cant_equipos * T1.valor )
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PUNTO_NUM_EQUIPOS T2 ON T1.id_punto_servicio = T2.id_punto_servicio
    WHERE   T1.id_tipo_modalidad = 7;

         ---- ********************************************************************
		 ---- CALCULO DE COBRO CASOS ESPECIALES
		 ---- EMPRESA MOLYMET, PUNTOS 355, 356 , 358
    UPDATE  calculo_cobro_clientes
    SET     bruto = ( ISNULL(valor, 0) * ISNULL(peso, 0) ) / 1000
    WHERE   id_punto_servicio IN ( 355, 356, 358 );

		 ---- ********************************************************************

		 ---- UPDATE DEL CAMPO ORDEN CONCATENAR ID_HOJA_RUTA
		 ---- Y ORDEN
    UPDATE  calculo_cobro_clientes
    SET     Orden = CONVERT(BIGINT, CONVERT(VARCHAR, id_hoja_ruta)
            + CONVERT(VARCHAR, Orden))
    WHERE   Orden IS NOT NULL;
		 
    INSERT  INTO calculo_cobro_clientes
            SELECT DISTINCT
                    0 ,
                    vpfm.id_punto_servicio ,
                    1 , --- cantidad
                    0 , --- peso
                    2 , --- estado
                    @arriendo ,
                    @periodo_cobro ,
                    CONVERT(INT, @mescobro) AS Monthis ,
                    CONVERT(INT, @yearcobro) AS Yearis ,
                    nombre_punto ,
                    NULL ,
                    ps.id_contrato ,
                    con.id_cliente ,
                    con.id_sucursal ,
                    'Activo' , ---- estado contrato
                    1 ,
                    rut_cliente ,
                    razon_social ,
                    NULL ,
                    NULL ,
                    sucursal ,
                    descripcion ,
                    NULL ,
                    55 , ---- id_tipo_modalidad
                    0 , ---- valor
                    'Fijo Mensual' , ---- tipo_modalidad
                    'CANTIDAD' ,
                    0 ,
                    0 ,
                    GETDATE() ,
                    NULL ,
                    1 ,
                    0 ,
                    'Fijo Mensual' ,
                    @arriendo ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL , ---- NUEVOS CAMPOS HACIA ABAJO
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL ,
                    NULL
						 --From calculo_cobro_clientes Where id_tipo_modalidad = 5
            FROM    V_PTO_FIJO_MENSUAL vpfm
                    INNER JOIN puntos_servicio ps ON ps.id_punto_servicio = vpfm.id_punto_servicio
                    INNER JOIN contratos con ON con.id_contrato = ps.id_contrato
                    INNER JOIN clientes cl ON cl.id_cliente = con.id_cliente
                    INNER JOIN sucursales suc ON suc.id_sucursal = con.id_sucursal
            WHERE   con.id_unidad = @id_unidad
                    AND ps.estado_punto = 1;
						--corregido para puntos sin servicios mensuales JPIZARRO
    UPDATE  T1
    SET     T1.valor = T2.valor ,
            T1.bruto = T2.valor
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PTO_FIJO_MENSUAL T2 ON T1.id_punto_servicio = T2.id_punto_servicio
    WHERE   id_tipo_modalidad = 55;
			




	---- JPIZARRO COBRO RETIRO Y DISPOSICION







	     --- EXCEPCIONES   AL   COBRO  CAJA RECOLECTORA
	     --- ID_PUNTOS_SERVICIO 144,246,283,285,369,278
	     --- NO SE LES COBRA NADA A LA FECHA 01-01-2015
	     --- SIN  EMBARGO  DEBEN APARECER EN EL CALCULO
    UPDATE  calculo_cobro_clientes
    SET     valor = 0 ,
            bruto = 0
    WHERE   id_punto_servicio IN ( 144, 246, 283, 285, 369, 278 );
		
	     ---- ORDENAMIENTO PARA LA SALIDA DEL ARCHIVO PDF
    UPDATE  calculo_cobro_clientes
    SET     sorting = 1 ,
            glosa = tipo_modalidad
    WHERE   id_tipo_modalidad = 7;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 2 ,
            glosa = tipo_modalidad
    WHERE   id_tipo_modalidad = 10;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 3 ,
            glosa = tipo_modalidad
    WHERE   id_tipo_modalidad = 12;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 4 ,
            glosa = tipo_modalidad
    WHERE   id_tipo_modalidad = 11;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 5 ,
            glosa = 'Retiro'
    WHERE   id_tipo_modalidad = 9;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 6 ,
            glosa = tipo_modalidad
    WHERE   id_tipo_modalidad = 55;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 12 ,
            glosa = 'Retiro'
    WHERE   id_tipo_modalidad = 5;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 13 ,
            glosa = 'Retiro'
    WHERE   id_tipo_modalidad = 8;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 14 ,
            glosa = 'Retiro'
    WHERE   id_tipo_modalidad = 6;

    UPDATE  calculo_cobro_clientes
    SET     sorting = 15 ,
            glosa = 'Retiro Manejo Dris'
    WHERE   id_tipo_modalidad = 17;

        ---- CALCULO DE LA DISPOSICION E INSERCION EN LA TABLA calculo_cobro_disposicion
    DECLARE @valorDec DECIMAL(18, 4);
    SET @valorDec = 0.0000;

    INSERT  INTO calculo_cobro_disposicion
            SELECT  cal.id_cliente ,
                    cal.rut_cliente ,
                    cal.Fecha ,
                    cal.id_contrato ,
                    cal.id_punto_servicio ,
                    glosa ,
                    SUM(ISNULL(cal.peso, 0)) AS Disposicion ,
                    0 CobroTonelada ,
                    @valorDec AS Valor ,
                    0
            FROM    V_CALCULO_COBRO_PUNTO cal
            GROUP BY cal.id_cliente ,
                    cal.rut_cliente ,
                    cal.Fecha ,
                    cal.id_contrato ,
                    cal.id_punto_servicio ,
                    glosa
            HAVING  glosa = 'Retiro';


				--TEST
	   ----- ACTUALIZANDO EL COBRO POR TONELADA Y EL VALOR EN LA TABLA calculo_cobro_disposicion
	-- INTELLEGO: El siguient update considera solo modalidad de cobro Disposición
    UPDATE  T1
    SET     T1.CobroTonelada = T2.valor ,
            T1.Valor = ( T1.Disposicion * T2.valor ) / 1000
    FROM    calculo_cobro_disposicion T1
            INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN modelo_cobro T3 ON T1.id_punto_servicio = T3.id_punto_servicio
            INNER JOIN puntos_servicio T5 ON T2.id_punto_servicio = T5.id_punto_servicio
    WHERE   T5.cobro_por_peso = 1
            AND T2.id_tipo_modalidad = 8;

	-- INTELLEGO: El siguient update considera solo modalidad de manejo dris
    UPDATE  T1
    SET     T1.Valor = T1.Valor + ( ( T1.Disposicion * T2.valor ) / 1000 )
    FROM    calculo_cobro_disposicion T1
            INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN modelo_cobro T3 ON T1.id_punto_servicio = T3.id_punto_servicio
            INNER JOIN puntos_servicio T5 ON T2.id_punto_servicio = T5.id_punto_servicio
    WHERE   T5.cobro_por_peso = 1
            AND T2.id_tipo_modalidad = 17;

    UPDATE  T1
    SET     T1.CobroTonelada = T2.valor ,
            T1.Valor = ( T1.Disposicion * T2.valor )
    FROM    calculo_cobro_disposicion T1
            INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN modelo_cobro T3 ON T1.id_punto_servicio = T3.id_punto_servicio
            INNER JOIN puntos_servicio T5 ON T2.id_punto_servicio = T5.id_punto_servicio
    WHERE   T5.cobro_por_m3 = 1;

				
	   ---- EXECPCIO EDAM COT 20 M3 SE COBRA POR UF // Cualquiera con cobro por uf por peso o m3 JPIZARRO29-02-16
    UPDATE  T1
    SET     T1.CobroTonelada = T2.valor ,
            T1.valoruf = @valoruf ,
            T1.Valor = ( ( ( T1.Disposicion * T2.valor ) / 1000 ) * @valoruf )
    FROM    calculo_cobro_disposicion T1
            INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN modelo_cobro T3 ON T1.id_punto_servicio = T3.id_punto_servicio
            INNER JOIN modelo_cobro_detalle T4 ON T3.id_modelo = T4.id_modelo_cobro
            INNER JOIN puntos_servicio T5 ON T2.id_punto_servicio = T5.id_punto_servicio
    WHERE   T4.id_tipo_moneda = 2
            AND T5.cobro_por_peso = 1;
				

		 ---- EXECPCIO EDAM COT 20 M3 SE COBRA POR UF // Cualquiera con cobro por uf por peso o m3 JPIZARRO 29-02-16
    UPDATE  T1
    SET     T1.CobroTonelada = T2.valor ,
            T1.valoruf = @valoruf ,
            T1.Valor = ( T1.Disposicion * T2.valor ) * @valoruf
    FROM    calculo_cobro_disposicion T1
            INNER JOIN V_PTO_SERVICIO_COBRO_TONELADA T2 ON T1.id_punto_servicio = T2.id_punto_servicio
            INNER JOIN modelo_cobro T3 ON T1.id_punto_servicio = T3.id_punto_servicio
            INNER JOIN modelo_cobro_detalle T4 ON T3.id_modelo = T4.id_modelo_cobro
            INNER JOIN puntos_servicio T5 ON T2.id_punto_servicio = T5.id_punto_servicio
    WHERE   T4.id_tipo_moneda = 2
            AND T5.cobro_por_m3 = 1;






		----- update JPIZARRO. Se setean los valores brutos de las disposiciones que no tienen retiro

    UPDATE  calculo_cobro_clientes
    SET     bruto = 0
    FROM    calculo_cobro_clientes ccc
            INNER JOIN calculo_cobro_disposicion cd ON cd.id_punto_servicio = ccc.id_punto_servicio
			--select ps.id_punto_servicio, ps.nombre_punto, det.id_tipo_modalidad from puntos_servicio ps 
            INNER JOIN modelo_cobro mc ON mc.id_punto_servicio = cd.id_punto_servicio
            INNER JOIN modelo_cobro_detalle det ON det.id_modelo_cobro = mc.id_modelo
    WHERE   ccc.id_punto_servicio NOT IN ( SELECT   id_punto_servicio
                                           FROM     V_DISPOSICION_Y_RETIRO )
            AND cd.Valor > 0
            AND ccc.id_tipo_modalidad NOT IN ( 7, 9, 10, 11, 12, 5 );

		----


    UPDATE  calculo_cobro_clientes
    SET     bruto = bruto * @valoruf
    FROM    calculo_cobro_clientes ccc
            INNER JOIN modelo_cobro_detalle mcd ON mcd.id_detalle_modelo_cobro = ccc.id_detalle_modelo_cobro
    WHERE   mcd.id_tipo_modalidad = 6
            AND mcd.id_tipo_moneda = 2;



	   ----- INSERCION DE LOS TOTALES DE COBROS EN LA TABLA cobro_cliente_total
    INSERT  INTO cobro_cliente_total
            SELECT  id_cliente ,
                    rut_cliente ,
                    Fecha ,
                    id_punto_servicio ,
                    Cobro ,
                    TotalCliente ,
                    IVA ,
                    TotalClienteMasIVA
            FROM    V_COBRO_CLIENTES_TOTAL
            WHERE   Fecha = @periodo_cobro;

			   ----  EXCEPTUANDO LOS PUNTOS 355 356 358 DE MOLYMET
		       ----  NO SE LES COBRA DISPOSICION
    UPDATE  calculo_cobro_disposicion
    SET     Valor = 0 ,
            Disposicion = 0 ,
            CobroTonelada = 0
    WHERE   id_punto_servicio IN ( 355, 356, 358, 324 );

		----- SE CIERRA EL PROYECTO SI FUE REQUERIDO DE ESA MANERA
    INSERT  INTO calculo_realizado
            ( Fecha, Cerrado )
    VALUES  ( @periodo_cobro, @cerrar_proceso );
		
			---- UPDDATE DE VALOR Y BRUTO PARA PUNTOS CAJA RECOLECTORA
			---- ID_TIPO_MODALIDAD = 5
			---- NO SE COBRAN LOS RETIROS SOLO SE COBRA UN VALOR  FIJO
			---- MENSUAL								   
    UPDATE  T1
    SET     T1.valor = 0 ,
            T1.bruto = 0
    FROM    calculo_cobro_clientes T1
            INNER JOIN V_PTO_CAJA_RECOLECTORA3 T2 ON T1.id_punto_servicio = T2.id_punto_servicio
                                                     AND glosa = 'Retiro';

    IF @cerrar_proceso = 1
        BEGIN

			---- YA QUE EL PROCESO FUE CERRADO SE COPIAN TODOS LOS DATOS
			---- DE LA TABLA calculo_cobro_clientes
			---- A LA TABLA cobro_clientes_cerrado

            SET @Cierre = ( SELECT  GETDATE()
                          );

				--- DETALLE DE CADA PUNTO POR PERIODO
			    --- Delete From cobro_clientes_cerrado Where Fecha = @periodo_cobro

				--- TOTAL DE COBRO DISPOSICION POR PUNTO Y PERIODO
				--- Delete From cobro_disposicion_cerrado Where Fecha = @periodo_cobro

				--- TOTAL DE COBRO A CADA CLIENTE POR PERIODO
				--- Delete From cliente_total_cerrado Where Fecha = @periodo_cobro

            INSERT  INTO cobro_clientes_cerrado
                    SELECT  [id_calculo] ,
                            [id_hoja_ruta] ,
                            [id_punto_servicio] ,
                            [cantidad] ,
                            [peso] ,
                            [estado] ,
                            [fechaFull] ,
                            [Fecha] ,
                            [Monthis] ,
                            [Yearis] ,
                            [nombre_punto] ,
                            [estado_punto] ,
                            [id_contrato] ,
                            [id_cliente] ,
                            [id_sucursal] ,
                            [estado_contrato] ,
                            [id_modelo_cobro] ,
                            [rut_cliente] ,
                            [razon_social] ,
                            [giro_comercial] ,
                            [nombre_fantasia] ,
                            [sucursal] ,
                            [descripcion] ,
                            [rut_sucursal] ,
                            [id_tipo_modalidad] ,
                            [valor] ,
                            [tipo_modalidad] ,
                            [como_se_calcula] ,
                            [bruto] ,
                            [proceso_cerrado] ,
                            [fechaCalculo] ,
                            [Orden] ,
                            [id_detalle_modelo_cobro] ,
                            [sorting] ,
                            [glosa] ,
                            [cobro_arriendo] ,
                            [id_camion] ,
                            [camion] ,
                            [id_conductor] ,
                            [conductor] ,
                            [fechacorta] ,
                            [sistema] ,
                            [franquiciado] ,
                            [estadopunto] ,
                            [relleno] ,
                            [pagofranquiciado] ,
                            [margen] ,
                            [margenporc] ,
                            [valorenuf] ,
                            [valoruf] ,
                            @id_unidad
                    FROM    calculo_cobro_clientes
                    WHERE   Fecha = @periodo_cobro;

            INSERT  INTO cobro_disposicion_cerrado
                    SELECT  id_cliente ,
                            rut_cliente ,
                            Fecha ,
                            id_contrato ,
                            id_punto_servicio ,
                            glosa ,
                            Disposicion ,
                            CobroTonelada ,
                            Valor ,
                            valoruf,
                            @id_unidad
                    FROM    calculo_cobro_disposicion
                    WHERE   Fecha = @periodo_cobro;

				 ------ INSERCION DE LOS TOTALES A COBRAR
            INSERT  INTO cliente_total_cerrado
                    SELECT  id_cliente ,
                            rut_cliente ,
                            Fecha ,
                            id_punto_servicio ,
                            TotalClienteSinDisp ,
                            TotalClienteMasDisp ,
                            IVA ,
                            TotalClienteMasIVA ,
                            @Cierre,
                            @id_unidad
                    FROM    cobro_cliente_total
                    WHERE   Fecha = @periodo_cobro;

            DELETE  FROM calculo_cobro_clientes;
            DELETE  FROM calculo_cobro_disposicion;
            DELETE  FROM cobro_cliente_total;

        END;

    IF @@error <> 0
        BEGIN					
            ROLLBACK TRANSACTION @DatosAnexos;
            EXEC sp_log_calculo_cobro 'ERROR',
                'Error Calculo Cobro Clientes - Datos Anexos';
            RAISERROR ('Error Calculo Cobro Clientes - Datos Anexos', 16, 1);
            RETURN;
        END;

    COMMIT TRANSACTION @DatosAnexos;
    EXEC sp_clientes_escalables;
    EXEC sp_log_calculo_cobro 'EXITO', 'Calculo Cobro Clientes Finalizado';