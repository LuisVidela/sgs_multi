/*
   martes, 28 de marzo de 201711:28:21
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_cobro_disposicion_cerrado
	(
	id_disposicion_cerrado int NOT NULL IDENTITY (1, 1),
	id_cliente int NULL,
	rut_cliente varchar(10) NULL,
	Fecha varchar(30) NULL,
	id_contrato int NULL,
	id_punto_servicio int NULL,
	glosa varchar(50) NULL,
	Disposicion decimal(18, 2) NULL,
	CobroTonelada float(53) NULL,
	Valor int NULL,
	valoruf float(53) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_cobro_disposicion_cerrado SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_cobro_disposicion_cerrado ON
GO
IF EXISTS(SELECT * FROM dbo.cobro_disposicion_cerrado)
	 EXEC('INSERT INTO dbo.Tmp_cobro_disposicion_cerrado (id_disposicion_cerrado, id_cliente, rut_cliente, Fecha, id_contrato, id_punto_servicio, glosa, Disposicion, CobroTonelada, Valor, valoruf)
		SELECT id_disposicion_cerrado, id_cliente, rut_cliente, Fecha, id_contrato, id_punto_servicio, glosa, CONVERT(decimal(18, 2), Disposicion), CobroTonelada, Valor, valoruf FROM dbo.cobro_disposicion_cerrado WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_cobro_disposicion_cerrado OFF
GO
DROP TABLE dbo.cobro_disposicion_cerrado
GO
EXECUTE sp_rename N'dbo.Tmp_cobro_disposicion_cerrado', N'cobro_disposicion_cerrado', 'OBJECT' 
GO
ALTER TABLE dbo.cobro_disposicion_cerrado ADD CONSTRAINT
	PK_cobro_disposicion_cerrado PRIMARY KEY CLUSTERED 
	(
	id_disposicion_cerrado
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.cobro_disposicion_cerrado ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.cobro_disposicion_cerrado SET (LOCK_ESCALATION = TABLE)
GO
delete cobro_disposicion_cerrado WHERE id_unidad IS NULL
GO
COMMIT
