USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]    Script Date: 30-03-2017 11:33:56 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

ALTER VIEW [dbo].[V_PUNTO_SIN_DISPOCICION_CLOSE]
AS
    SELECT  Id_cliente ,
            rut_cliente ,
            id_contrato ,
            razon_social ,
            nombre_fantasia ,
            Sucursal ,
            id_punto_servicio ,
            nombre_punto ,
            SUM(Numero_Retiros) AS Numero_Retiros ,
            SUM(BRUTO) AS Valor ,
            0 AS IVA ,
            0 AS NETO ,
            SUM(Peso) AS Peso ,
            Fecha ,
            Monthis ,
            Yearis ,
            id_sucursal
    FROM    dbo.V_VOLTEOS_PUNTOS_CLIENTES_CLOSE
    GROUP BY Id_cliente ,
            rut_cliente ,
            id_contrato ,
            razon_social ,
            nombre_fantasia ,
            Sucursal ,
            id_punto_servicio ,
            nombre_punto ,
            Fecha ,
            Monthis ,
            Yearis ,
            id_sucursal;

GO


