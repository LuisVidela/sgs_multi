USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]    Script Date: 29-03-2017 11:45:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER VIEW [dbo].[V_VOLTEOS_PUNTOS_CLIENTES_CLOSE]
AS
SELECT  Id_cliente ,
        rut_cliente ,
        id_contrato ,
        razon_social ,
        nombre_fantasia ,
        Sucursal ,
        id_punto_servicio ,
        nombre_punto ,
        SUM(cantidad) AS Numero_Retiros ,
        SUM(bruto) AS BRUTO ,
        0 AS IVA ,
        0 AS NETO ,
        SUM(peso) AS Peso ,
        Fecha AS Fecha ,
        Monthis AS Monthis ,
        Yearis AS Yearis ,
        id_sucursal ,
        Sistema
FROM    cobro_clientes_cerrado
WHERE   id_tipo_modalidad NOT IN ( 5, 7, 9, 10, 11, 12 )
GROUP BY Id_cliente ,
        rut_cliente ,
        id_contrato ,
        razon_social ,
        nombre_fantasia ,
        Sucursal ,
        id_punto_servicio ,
        nombre_punto ,
        Fecha ,
        Monthis ,
        Yearis ,
        id_sucursal ,
        Sistema
HAVING  Sistema = 'Front Loader'
UNION ALL
SELECT  Id_cliente ,
        rut_cliente ,
        id_contrato ,
        razon_social ,
        nombre_fantasia ,
        Sucursal ,
        id_punto_servicio ,
        nombre_punto ,
        COUNT(id_punto_servicio) AS Numero_Retiros ,
        SUM(bruto) AS BRUTO ,
        0 AS IVA ,
        0 AS NETO ,
        SUM(peso) AS Peso ,
        Fecha AS Fecha ,
        Monthis AS Monthis ,
        Yearis AS Yearis ,
        id_sucursal ,
        Sistema
FROM    cobro_clientes_cerrado
WHERE   id_tipo_modalidad NOT IN ( 5, 7, 9, 10, 11, 12 )
GROUP BY Id_cliente ,
        rut_cliente ,
        id_contrato ,
        razon_social ,
        nombre_fantasia ,
        Sucursal ,
        id_punto_servicio ,
        nombre_punto ,
        Fecha ,
        Monthis ,
        Yearis ,
        id_sucursal ,
        Sistema
HAVING  Sistema <> 'Front Loader';

GO


