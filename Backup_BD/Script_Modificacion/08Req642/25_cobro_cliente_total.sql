/*
   viernes, 21 de abril de 201712:50:08
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO_QA
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_cobro_cliente_total
	(
	id_total int NOT NULL IDENTITY (1, 1),
	id_cliente int NULL,
	rut_cliente varchar(10) NULL,
	Fecha varchar(30) NULL,
	id_punto_servicio int NULL,
	TotalClienteSinDisp bigint NULL,
	TotalClienteMasDisp bigint NULL,
	IVA int NULL,
	TotalClienteMasIVA bigint NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_cobro_cliente_total SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_cobro_cliente_total ON
GO
IF EXISTS(SELECT * FROM dbo.cobro_cliente_total)
	 EXEC('INSERT INTO dbo.Tmp_cobro_cliente_total (id_total, id_cliente, rut_cliente, Fecha, id_punto_servicio, TotalClienteSinDisp, TotalClienteMasDisp, IVA, TotalClienteMasIVA)
		SELECT id_total, id_cliente, rut_cliente, Fecha, id_punto_servicio, CONVERT(bigint, TotalClienteSinDisp), CONVERT(bigint, TotalClienteMasDisp), IVA, CONVERT(bigint, TotalClienteMasIVA) FROM dbo.cobro_cliente_total WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_cobro_cliente_total OFF
GO
DROP TABLE dbo.cobro_cliente_total
GO
EXECUTE sp_rename N'dbo.Tmp_cobro_cliente_total', N'cobro_cliente_total', 'OBJECT' 
GO
ALTER TABLE dbo.cobro_cliente_total ADD CONSTRAINT
	PK_cobro_total PRIMARY KEY CLUSTERED 
	(
	id_total
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
