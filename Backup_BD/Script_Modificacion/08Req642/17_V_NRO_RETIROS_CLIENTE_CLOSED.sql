USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_NRO_RETIROS_CLIENTE_CLOSED]    Script Date: 31-03-2017 15:00:25 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


CREATE VIEW [dbo].[V_NRO_RETIROS_CLIENTE_CLOSED]
AS
    SELECT  COUNT(glosa) AS nro_retiros ,
            id_cliente ,
            Fecha
    FROM    dbo.cobro_clientes_cerrado
    WHERE   ( glosa LIKE 'Retiro' )
    GROUP BY id_cliente ,
            Fecha;


GO


