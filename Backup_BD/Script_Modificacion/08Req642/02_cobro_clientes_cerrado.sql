/*
   lunes, 27 de marzo de 201718:07:53
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_cobro_clientes_cerrado
	(
	id_calculo int NOT NULL,
	id_hoja_ruta int NULL,
	id_punto_servicio int NOT NULL,
	cantidad int NULL,
	peso float NULL,
	estado int NULL,
	fechaFull date NULL,
	Fecha varchar(30) NULL,
	Monthis int NULL,
	Yearis int NULL,
	nombre_punto varchar(100) NULL,
	estado_punto varchar(10) NULL,
	id_contrato int NULL,
	id_cliente int NULL,
	id_sucursal int NULL,
	estado_contrato varchar(100) NULL,
	id_modelo_cobro int NULL,
	rut_cliente varchar(10) NULL,
	razon_social varchar(200) NULL,
	giro_comercial varchar(200) NULL,
	nombre_fantasia varchar(200) NULL,
	sucursal varchar(250) NULL,
	descripcion varchar(MAX) NULL,
	rut_sucursal varchar(10) NULL,
	id_tipo_modalidad int NULL,
	valor decimal(18, 4) NULL,
	tipo_modalidad varchar(50) NULL,
	como_se_calcula varchar(50) NULL,
	bruto decimal(18, 4) NULL,
	proceso_cerrado int NULL,
	fechaCalculo date NULL,
	Orden bigint NULL,
	id_detalle_modelo_cobro int NULL,
	sorting int NULL,
	glosa varchar(50) NULL,
	cobro_arriendo varchar(50) NULL,
	id_camion int NULL,
	camion varchar(300) NULL,
	id_conductor int NULL,
	conductor varchar(300) NULL,
	fechacorta varchar(50) NULL,
	sistema varchar(50) NULL,
	franquiciado varchar(100) NULL,
	estadopunto varchar(50) NULL,
	relleno varchar(100) NULL,
	pagofranquiciado int NULL,
	margen int NULL,
	margenporc int NULL,
	valorenuf float(53) NULL,
	valoruf float(53) NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_cobro_clientes_cerrado SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.cobro_clientes_cerrado)
	 EXEC('INSERT INTO dbo.Tmp_cobro_clientes_cerrado (id_calculo, id_hoja_ruta, id_punto_servicio, cantidad, peso, estado, fechaFull, Fecha, Monthis, Yearis, nombre_punto, estado_punto, id_contrato, id_cliente, id_sucursal, estado_contrato, id_modelo_cobro, rut_cliente, razon_social, giro_comercial, nombre_fantasia, sucursal, descripcion, rut_sucursal, id_tipo_modalidad, valor, tipo_modalidad, como_se_calcula, bruto, proceso_cerrado, fechaCalculo, Orden, id_detalle_modelo_cobro, sorting, glosa, cobro_arriendo, id_camion, camion, id_conductor, conductor, fechacorta, sistema, franquiciado, estadopunto, relleno, pagofranquiciado, margen, margenporc, valorenuf, valoruf)
		SELECT id_calculo, id_hoja_ruta, id_punto_servicio, cantidad, peso, estado, fechaFull, Fecha, Monthis, Yearis, nombre_punto, estado_punto, id_contrato, id_cliente, id_sucursal, estado_contrato, id_modelo_cobro, rut_cliente, razon_social, giro_comercial, nombre_fantasia, sucursal, descripcion, rut_sucursal, id_tipo_modalidad, CONVERT(decimal(18, 4), valor), tipo_modalidad, como_se_calcula, bruto, proceso_cerrado, fechaCalculo, Orden, id_detalle_modelo_cobro, sorting, glosa, cobro_arriendo, id_camion, camion, id_conductor, conductor, fechacorta, sistema, franquiciado, estadopunto, relleno, pagofranquiciado, margen, margenporc, valorenuf, valoruf FROM dbo.cobro_clientes_cerrado WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.cobro_clientes_cerrado
GO
EXECUTE sp_rename N'dbo.Tmp_cobro_clientes_cerrado', N'cobro_clientes_cerrado', 'OBJECT' 
GO
ALTER TABLE dbo.cobro_clientes_cerrado ADD CONSTRAINT
	PK_cobro_clientes_cerrado PRIMARY KEY CLUSTERED 
	(
	id_calculo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX IDX_FRANQUICIADO ON dbo.cobro_clientes_cerrado
	(
	id_punto_servicio,
	Fecha,
	franquiciado
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.cobro_clientes_cerrado ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.cobro_clientes_cerrado SET (LOCK_ESCALATION = TABLE)
GO
delete cobro_clientes_cerrado WHERE id_unidad IS NULL
GO
COMMIT
