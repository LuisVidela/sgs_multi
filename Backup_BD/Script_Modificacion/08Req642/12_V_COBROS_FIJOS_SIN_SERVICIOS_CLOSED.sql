USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_COBROS_FIJOS_SIN_SERVICIOS_CLOSED]    Script Date: 30-03-2017 16:52:46 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE VIEW [dbo].[V_COBROS_FIJOS_SIN_SERVICIOS_CLOSED]
AS
    SELECT  A.id_calculo ,
            A.id_hoja_ruta ,
            A.id_punto_servicio ,
            A.cantidad ,
            A.peso ,
            A.estado ,
            A.fechaFull ,
            A.Fecha ,
            A.Monthis ,
            A.Yearis ,
            A.nombre_punto ,
            A.estado_punto ,
            A.id_contrato ,
            A.id_cliente ,
            A.id_sucursal ,
            A.estado_contrato ,
            A.id_modelo_cobro ,
            A.rut_cliente ,
            A.razon_social ,
            A.giro_comercial ,
            A.nombre_fantasia ,
            A.sucursal ,
            A.descripcion ,
            A.rut_sucursal ,
            A.id_tipo_modalidad ,
            A.valor ,
            A.tipo_modalidad ,
            A.como_se_calcula ,
            A.bruto ,
            A.proceso_cerrado ,
            A.fechaCalculo ,
            A.Orden ,
            A.id_detalle_modelo_cobro ,
            A.sorting ,
            A.glosa ,
            A.cobro_arriendo ,
            A.id_camion ,
            A.camion ,
            A.id_conductor ,
            A.conductor ,
            A.fechacorta ,
            A.sistema ,
            A.franquiciado ,
            A.estadopunto ,
            A.relleno ,
            A.pagofranquiciado ,
            A.margen ,
            A.margenporc ,
            A.valorenuf ,
            A.valoruf ,
            A.ValorTotal
    FROM    dbo.V_COBROS_FIJOS_PUNTOS_CLOSED AS A
            LEFT OUTER JOIN dbo.V_CALCULO_PUNTO_OLD_CLOSED AS B ON A.id_punto_servicio = B.id_punto_servicio
                                                              AND B.Fecha = A.Fecha
    WHERE   ( B.id_punto_servicio IS NULL );

GO

EXEC sys.sp_addextendedproperty @name = N'MS_DiagramPane1',
    @value = N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[21] 2[24] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "A"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 259
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "B"
            Begin Extent = 
               Top = 6
               Left = 297
               Bottom = 136
               Right = 501
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 52
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         ', @level0type = N'SCHEMA', @level0name = N'dbo',
    @level1type = N'VIEW',
    @level1name = N'V_COBROS_FIJOS_SIN_SERVICIOS_CLOSED';
GO

EXEC sys.sp_addextendedproperty @name = N'MS_DiagramPane2',
    @value = N'Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW',
    @level1name = N'V_COBROS_FIJOS_SIN_SERVICIOS_CLOSED';
GO

EXEC sys.sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 2,
    @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'VIEW',
    @level1name = N'V_COBROS_FIJOS_SIN_SERVICIOS_CLOSED';
GO


