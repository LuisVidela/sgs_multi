USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_NRO_RETIROS_SERVICIOS]    Script Date: 30-03-2017 17:53:33 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO



CREATE VIEW [dbo].[V_NRO_RETIROS_SERVICIOS_CLOSED]
AS
    SELECT  COUNT(glosa) AS nro_retiros ,
            id_punto_servicio ,
            Fecha
    FROM    dbo.cobro_clientes_cerrado
    WHERE   ( glosa LIKE 'Retiro' )
    GROUP BY id_punto_servicio ,
            Fecha;




GO


