USE [RS_SGS_ARAUCO];
GO

/****** Object:  View [dbo].[V_VALOR_TOTAL_CLIENTE_CLOSED]    Script Date: 31-03-2017 12:40:38 ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE VIEW [dbo].[V_VALOR_TOTAL_CLIENTE_CLOSED]
AS
    SELECT  SUM(ValorTotal) AS valor ,
            Id_cliente ,
            Fecha
    FROM    dbo.V_CALCULO_PUNTO_CERRADO
    GROUP BY Id_cliente ,
            Fecha;

GO
