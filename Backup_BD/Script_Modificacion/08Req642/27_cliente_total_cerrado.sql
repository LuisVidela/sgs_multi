/*
   viernes, 21 de abril de 201714:50:45
   Usuario: sa
   Servidor: localhost
   Base de datos: RS_SGS_ARAUCO_QA
   Aplicación: 
*/

/* Para evitar posibles problemas de pérdida de datos, debe revisar este script detalladamente antes de ejecutarlo fuera del contexto del diseñador de base de datos.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_cliente_total_cerrado
	(
	id_total_cerra int NOT NULL IDENTITY (1, 1),
	id_cliente int NULL,
	rut_cliente varchar(10) NULL,
	Fecha varchar(30) NULL,
	id_punto_servicio int NULL,
	TotalClienteSinDisp bigint NULL,
	TotalClienteMasDisp bigint NULL,
	IVA int NULL,
	TotalClienteMasIVA bigint NULL,
	FechaCierre datetime NULL,
	id_unidad int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_cliente_total_cerrado SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_cliente_total_cerrado ON
GO
IF EXISTS(SELECT * FROM dbo.cliente_total_cerrado)
	 EXEC('INSERT INTO dbo.Tmp_cliente_total_cerrado (id_total_cerra, id_cliente, rut_cliente, Fecha, id_punto_servicio, TotalClienteSinDisp, TotalClienteMasDisp, IVA, TotalClienteMasIVA, FechaCierre, id_unidad)
		SELECT id_total_cerra, id_cliente, rut_cliente, Fecha, id_punto_servicio, CONVERT(bigint, TotalClienteSinDisp), CONVERT(bigint, TotalClienteMasDisp), IVA, CONVERT(bigint, TotalClienteMasIVA), FechaCierre, id_unidad FROM dbo.cliente_total_cerrado WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_cliente_total_cerrado OFF
GO
DROP TABLE dbo.cliente_total_cerrado
GO
EXECUTE sp_rename N'dbo.Tmp_cliente_total_cerrado', N'cliente_total_cerrado', 'OBJECT' 
GO
ALTER TABLE dbo.cliente_total_cerrado ADD CONSTRAINT
	PK_total_cerrado PRIMARY KEY CLUSTERED 
	(
	id_total_cerra
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
