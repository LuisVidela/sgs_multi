USE [RS_SGS_ARAUCO]
GO

/****** Object:  View [dbo].[V_CALCULO_PUNTO_2_CLOSED]    Script Date: 31-03-2017 16:20:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




	
CREATE VIEW [dbo].[V_CALCULO_PUNTO_2_CLOSED]
AS
    SELECT  id_calculo ,
            Id_cliente ,
            rut_cliente ,
            razon_social ,
            nombre_fantasia ,
            ISNULL(id_punto_servicio, 0) idpunto_servicio ,
            nombre_punto ,
            Numero_Retiros ,
            SUM(ValorTotal) AS ValorTotal ,
            Fecha ,
            Peso ,
            id_contrato ,
            id_sucursal ,
            id_tipo_modalidad
    FROM    dbo.V_CALCULO_PUNTO_CERRADO
    GROUP BY id_calculo ,
            Id_cliente ,
            rut_cliente ,
            razon_social ,
            nombre_fantasia ,
            id_punto_servicio ,
            nombre_punto ,
            Numero_Retiros ,
            Fecha ,
            Peso ,
            id_contrato ,
            id_sucursal ,
            id_tipo_modalidad;





GO


