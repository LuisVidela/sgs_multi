USE [RS_SGS_ARAUCO]
GO
/****** Object:  StoredProcedure [dbo].[SP_SEL_ListaServicio]    Script Date: 15-03-2017 16:11:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[SP_SEL_ListaServicio] '21-02-2016', '21-03-2016', 32
ALTER PROCEDURE [dbo].[SP_SEL_ListaServicio]
    @FechaDesde DATETIME ,
    @fechaHasta DATETIME ,
    @codigo_unidad INT
	
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  hoja.fecha ,
                hoja.id_hoja_ruta folio ,
				--tmod.tipo_modelo servicio ,
				deta.codigo_externo ,
				CASE WHEN deta.estado = 1
				THEN 'ASIGNADO'
				WHEN deta.estado = 2
				THEN 'REALIZADO'
				WHEN deta.estado = 3
				THEN 'SUSPENDIDO'
				WHEN deta.estado = 4
				THEN 'NO REALIZADO'
				WHEN deta.estado = 5
				THEN 'BLOQUEADO'
				WHEN deta.estado = 6
				THEN 'NO ATENDIDO'
				WHEN deta.estado = 7
				THEN 'MAL ESTIBADO'
				WHEN deta.estado = 8
				THEN 'SIN PESAJE'
				ELSE 'ASIGNADA'
				END as estado_hoja,
				deta.equipoentrada ,
				ta.tipo_servicio servicio ,
                cond.nombre_conductor conductor ,
                cami.patente ,
                equi.tipo_equipo tipoContenedor ,
				left(deta.observacion,10) detalle_observacion,
                CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) + ' %' nivelLlenado ,
                ISNULL(( ( equi.capacidad_equipo
                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) ) / 100 ), 0) as volumen --equi.capacidad_equipo volumen
               ,resi.residuo residuo
                    --, sucu.sucursal origen
               ,punt.nombre_punto origen 
                       ,vert.vertedero destino 
                       ,CASE WHEN deta.peso_calculado = 1 THEN 'Peso Calculado'
                     ELSE 'Pesaje Directo'
                END tipoPesaje ,
                ISNULL(CASE WHEN deta.peso_calculado = 1 THEN NULL --
                     ELSE CASE WHEN porc.PORL_PORCERTAJELLENADO > 0 AND deta.peso > 0 AND equi.capacidad_equipo > 0--
                               THEN deta.peso --
                                    / ( ( CONVERT(FLOAT, porc.PORL_PORCERTAJELLENADO) --
                                          / 100 ) * equi.capacidad_equipo ) --
                               ELSE 0
                          END
                END, 0) pesajeDirecto  ,
                ISNULL(CASE WHEN deta.densidad IS NULL
                     THEN CASE WHEN deta.peso_calculado = 1 THEN resi.densidad
                               ELSE deta.peso / ( ( equi.capacidad_equipo
                                                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) )
                                                  / 100 )
                          END
                     ELSE deta.densidad
                END,0) as densidad ,
				CASE WHEN deta.peso IS NULL
				--THEN 0
				THEN (( ( equi.capacidad_equipo
                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) ) / 100) * deta.densidad )
					WHEN deta.peso = 0
					THEN (( ( equi.capacidad_equipo
                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) ) / 100) * deta.densidad )
                ELSE 
				deta.peso
				END peso
                       ,punt.email_contacto AS observacion
                       ,punt.celular_contacto AS equipoentrada
                       ,punt.cant_equipos AS codigo_externo
        FROM    hoja_ruta hoja
                LEFT JOIN hoja_ruta_detalle deta ON hoja.id_hoja_ruta = deta.id_hoja_ruta
				LEFT JOIN tipo_servicio ta ON deta.id_tipo_servicio = ta.id_tipo_servicio
                LEFT JOIN puntos_servicio punt ON punt.id_punto_servicio = deta.id_punto_servicio
                LEFT JOIN contratos cont ON cont.id_contrato = punt.id_contrato
                LEFT JOIN sucursales sucu ON sucu.id_sucursal = cont.id_sucursal
                LEFT JOIN modelo_cobro mode ON mode.id_punto_servicio = punt.id_punto_servicio
                LEFT JOIN tipo_modelo tmod ON tmod.id_tipo_modelo = mode.id_tipo_modelo
               LEFT JOIN residuos resi ON resi.id_residuo = punt.id_residuo
                LEFT JOIN camiones cami ON cami.id_camion = hoja.id_camion
                LEFT JOIN conductores cond ON cond.id_conductor = hoja.id_conductor
                LEFT JOIN equipos equi ON equi.id_equipo = punt.id_equipo
                LEFT JOIN TB_SGS_PORCENTAJELLENADO porc ON porc.PORL_CODIGO = deta.PORL_CODIGO
                LEFT JOIN vertederos vert ON vert.id_vertedero = deta.relleno
        WHERE   hoja.estado <> 4 --AND deta.estado = 2
                AND cont.id_unidad = @codigo_unidad
                AND ( hoja.fecha >= @FechaDesde
                      AND hoja.fecha <= @fechaHasta
                    )
    END