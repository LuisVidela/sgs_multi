﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReporteHojaRuta.aspx.cs" Inherits="SGS.ReportForms.ReporteHojaRuta" %>
<%@ Register TagPrefix="rsweb" Namespace="Microsoft.Reporting.WebForms" Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" %>

<style type="text/css">
    #ReportViewer1_ctl09 {
        overflow:visible !important;
    } 
</style>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Strikeout="False" ProcessingMode="Remote" ShowFindControls="False" Width="100%" Height="100%" AsyncRendering="False" ShowBackButton="False" ShowRefreshButton="False">
            </rsweb:ReportViewer>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
    </form>
</body>
</html>
