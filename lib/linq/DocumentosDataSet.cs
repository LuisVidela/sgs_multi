﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.lib.linq
{
    public class DocumentosDataSet
    {

        string CL_RUT { get; set; }
        string DD_RUT { get; set; }
        string DC_NUMERO { get; set; }
        int DC_SALDO { get; set; }
        int DC_MONTO { get; set; }
        int ELIMINADO { get; set; }

    }
}