﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using SGS.Models.excelView;

namespace SGS.lib
{
    public class Utils
    {

        public static object DefaultStringIfNull(object value)
        {

            if (value == null)
            {

                return String.Empty;
            }

            return value;

        }


        public static void exportExcel(HttpResponseBase Response, IEnumerable<object> data)
        {


            StringWriter sw = new StringWriter();

            foreach (var i in data.Select((value, key) => new { key, value }))
            {
                Response.Write(i.key);
            }




        }


        public static string getStringOfDay(int dia)
        {

            string resultado = null;

            switch (dia)
            {
                case 1:
                    resultado = "lunes";
                    break;
                case 2:
                    resultado = "martes";
                    break;
                case 3:
                    resultado = "miercoles";
                    break;
                case 4:
                    resultado = "jueves";
                    break;
                case 5:
                    resultado = "viernes";
                    break;
                case 6:
                    resultado = "sabado";
                    break;
                case 7:
                    resultado = "domingo";
                    break;


            }

            if (resultado == null)
            {

                throw new Exception("la cadena no puede estar vacia");
            }


            return resultado;

        }
    }
}