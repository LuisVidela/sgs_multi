﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.lib
{
    public class JsonObject
    {
        public List<string>[] data { get; set; }
        public uint recordsTotal { get; set; }
        public uint recordsFiltered { get; set; }
    }
}