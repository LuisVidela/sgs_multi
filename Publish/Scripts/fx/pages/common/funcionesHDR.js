﻿$(document).ready(function () {
    $.extend($.gritter.options, {
        position: 'bottom-left',
        fade_in_speed: 'medium',
        fade_out_speed: 2000,
        time: 2000
    });

})


function clearData() {
    global_tabla.find(" tbody tr").remove();
}

function clearTemp() {
    global_tabla.find("tbody .temp").remove();
}

function clearBd() {
    global_tabla.find("tbody .bd").remove();
}

function clearPs() {
    global_wrapper_puntos.find(".todo").remove();
}


function tooggleFade(elIn, elOut) {

    elOut.fadeOut(global_fadeSpeed, function () {
        elIn.fadeIn(global_fadeSpeed);
    });


}

function showGritter(titulo, mensaje) {

    $.gritter.add({
        title: titulo,
        text: mensaje,
        sticky: false,

    });
}


function showNoty(mensaje, tiempo, layout, type) {

    var _tiempo = false;
    var _layout = "center";
    var _type = "information";

    if (tiempo != undefined) {
        _tiempo = tiempo;
    }

    if (layout != undefined) {
        _layout = layout;
    }

    if (type != undefined) {
        _type = type;
    }

    n = noty({
        text: mensaje,
        timeout: _tiempo,
        layout: _layout,
        id: 1,
        force: true,
        type: _type
    });
}



function toogleFadeDesactivate(elIn, elOut, reverse) {
    tooggleFade(elIn, elOut);
    if (reverse) {
        global_agregar.unbind("click");
    } else {
        bindClick();
    }


}

function callAjax(url, success, data, datatype) {

    var _url=null;
    var _success = null;
    var _data = null;
    var _datatype="html";

    if (url != undefined) {
        _url = url;
    }
    if (data != undefined) {
        _data = data;
    }
    if (success != undefined) {
        _success = success;
    }
    if (datatype != undefined) {
        _datatype = datatype;
    }

    $.ajax({
        type: "POST",
        url:_url,
        data: _data,
        success: _success,
        dataType: _datatype
    });
}



function dragAndDrop() {
    /*
    para reutilizacion tenemos como convencion pasar la clase .drag al evento draggable y drop a la celda de las tablas
    */
    $(".drag").draggable({
        helper: "clone"
    });
    $(".drop").droppable({
        drop: function (event, ui) {
            var punto = $(ui.draggable).attr("id");
            var content = $(this).html();
            var _a = $(this).find("a");
            var _ai = $(this).find("a i");
            var _number = 0;

            var remove_class = null;
            var add_class = null;

            if (content != "") {
                var id = _ai.attr("id");
                var _split = id.split("|");

                if (_a.hasClass("btn-info")) {
                    showNoty("Se permiten un maximo de 3 puntos de servicio en esta hora", 2000, "center");
                    return;
                }
                for (var _i = 0; _i < _split.length; _i++) {
                    if (_split[_i] === punto) {
                        showNoty("El punto de servicio ya esta asignado a esta hora", 2000, "center");
                        return;
                    }
                }
                if (_a.hasClass("btn-success")) {
                    remove_class = "btn-success";
                    add_class = "btn-warning";
                    _number = 2;

                } else if (_a.hasClass("btn-warning")) {
                    remove_class = "btn-warning";
                    add_class = "btn-info";
                    _number = 3;
                }

                updateRow($(this), punto, _number);
                _a.removeClass(remove_class);
                _a.addClass(add_class);

                var idd = id + "|" + punto;
                _ai.attr("id", idd);

            } else {
                _number = 1;
                updateRow($(this), punto, _number);
                $(this).html($("#wrapper-success").html());
                $(this).find("a i").attr("id", punto);
            }
        },
        hoverClass: "drop-hover"
    });
}


$(document).ajaxStart(function () {

    showNoty("Cargando datos");
});

$(document).ajaxComplete(function () {

    $.noty.closeAll();

});