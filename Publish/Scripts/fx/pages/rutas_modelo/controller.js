﻿
var global_estructura = null;
var global_tabla = null;
var global_agregar = null;
var global_grabar = null;
var global_temp = null;
var global_dia = null;
var global_servicio = null;
var global_wrapper_puntos = null;
var global_cancel = null;
var global_saveCancel = null;
var global_nombre_ruta = null;

var global_fadeSpeed = 100;
var n = null;
var global_persist_class = "temp";


$(document).ready(function () {


    var estructura = $("#layout_rows>table tbody").html();
    global_estructura = estructura;
    var tabla = $(".table");
    global_tabla = tabla;
    var agregar = $("#add");
    global_agregar = agregar;
    var grabar = $("#save");
    global_grabar = grabar;
    var temp = $(".temp");
    global_temp = temp;
    var dia = $("#dia");
    global_dia = dia;
    var servicio = $("#servicio");
    global_servicio = servicio;
    var wrapper_puntos = $("#wrapper_puntos_servicios");
    global_wrapper_puntos = wrapper_puntos;
    var cancel = $("#cancel");
    global_cancel = cancel;
    var saveCancel = $(".savecancel");
    global_saveCancel = saveCancel;
    var nombre_ruta = $("#nombre_ruta");
    global_nombre_ruta = nombre_ruta;

    
    eventos();
    callAndUpdate();
   // searchListAndTable();


});


function searchListAndTable() {


    var options = {
        valueNames: ['nombre_ps']
    };

    var puntos_servicios = new List('lister', options);
    console.log("d");


}

$(document).on("click",".eliminar",function(event,el,name) {
    var _ids = $(event.currentTarget).attr("id");
    target = $(event.currentTarget);
    callAjax("/RutasModelo/removePuntoServicioAjax", function (data) {
        showGritter("Informacion", "Punto eliminado exitosamente")
        target.parent().parent().remove();
        callClearAndUpdate();

    }, "ids=" + _ids);
});
$(document).on("change", "#tarifaModelo", function (event) {
    console.log('entre!!!');
    var idTarifa = $(this).val();
    console.log($(this).val());
    if ($(this).val() == '' || $(this).val() == null) idTarifa = -1;
    var idRuta_ = $(this).attr('idRuta');
    $.ajax({
                     contentType: "application/json; charset=utf-8",
                     url: "/RutasModelo/cambiaTarifa",
                     type: this.method,
                     data: { idRuta : idRuta_, idTarifa:idTarifa },
                     success: function (respuesta) {
                     }
                 });
});

$(document).on("click", ".ruta_edit", function (event) {

    var _ruta_modelo = $(event.currentTarget).parent().parent().attr("id");
    console.log(_ruta_modelo);
    callAjax("/RutasModelo/allPuntosServicios", function (data) {
        $("#wrapper-ajax-details").html(data);

        Shadowbox.open({
            content: "#wrapper-ajax-details",
            player: "inline",
            title: "Welcome",
            width: "700px"
        });
    }, "ruta=" + _ruta_modelo);

})


$(document).on("click", ".eliminar-ruta", function (event) {

    var seguridad = confirm("Estas seguro de eliminar esta ruta y todos sus puntos de servicios asociados")
    if (seguridad == false) {
        return;
    }

    var _ruta = $(event.currentTarget).attr("id");
    callAjax("/RutasModelo/removeRutaModelo", function (data) {
        showGritter("Informacion", "Ruta Modelo eliminada exitosamente")
        Shadowbox.close();
        callClearAndUpdate();
    },"ruta="+_ruta)
})


function eventos() {


   
    bindClick();
    bindClickCell();

    

     global_grabar.click(function () {

         var _debug = $(".temp").find("#nombre_ruta").val();
         if (_debug == "") {
             showNoty("El Nombre de la ruta no puede estar vacia", 2000, "center","error");
             return;
         }
         var _data = "dia=" + global_dia.val() + "&nombre_ruta="+$(".temp>td:first input").val()+"&servicio=" + global_servicio.val() + "&ruta='NONE'";
         var matriz = new Array();
         $(".temp>.edit").each(function (index,el) {
             var _res = $(this).html();
             if (_res != "") {
                 var id_punto = $(el).find("a i").attr("id");
                 matriz.push("("+(index+1)+"-"+id_punto+")");
             }
         })
         _data += "&data="+matriz;
       
         callAjax("/RutasModelo/puntosServiciosSaveDataAjax",function (data) {
             callClearAndUpdate();
             toogleFadeDesactivate(global_agregar, global_saveCancel, false);
             dragAndDrop();
         }, _data);

     })


     global_dia.change(function () {

         callClearAndUpdate();
         
     })

     global_servicio.change(function () {

         callClearAndUpdate();

     })

     global_cancel.click(function () {

         toogleFadeDesactivate(global_agregar, global_saveCancel, false);
         clearTemp();
     });
}



function updateRow(el,punto,index) {

    if (el.parent().hasClass("bd")) {
        var _parent = el.parent();
        var id_ruta = _parent.attr("id");
     
        callAjax("/RutasModelo/UpdatepuntosServiciosSaveDataAjax", function (data) {
            showGritter("Informacion", "La ruta ha sido actualizada exitosamente")
            callClearAndUpdate();
            dragAndDrop()
        }, "id_ruta=" + id_ruta + "&id_punto=" + punto + "&col=" + el.prevAll().length + "&index=" + index);
    }

}

function callAndUpdate() {

    callAjax("/RutasModelo/tableData", function (data) {
            global_tabla.find("tbody").html(data);
    bindClickCell();
    dragAndDrop();
    }, "dia=" + global_dia.val() + "&tipo_servicio=" + global_servicio.val());


    callAjax("/RutasModelo/puntosServiciosData", function (data) {
        global_wrapper_puntos.html(data);
        bindClickCell();
        dragAndDrop();
        searchListAndTable();
    },"dia=" + global_dia.val());

}



function callClearAndUpdate() {
    clearData();
    callAndUpdate();
}





function bindClick() {
    global_agregar.click(function () {
        global_persist_class = "temp";
        global_tabla.prepend(global_estructura);
        toogleFadeDesactivate(global_saveCancel, global_agregar, true);
        dragAndDrop();
    })

}





function bindClickCell() {

    global_tabla.find("tr td:not(.nombre)").click(function () {
        var _content = $(this).html();
        if (_content == "") {
            return;
        }
        var _ids = $(this).find("a i").attr("id");

        var _parent = $(this).parent();
        var _ruta_modelo = _parent.attr("id");
        var _col = $(this).prevAll().length;


        callAjax("/RutasModelo/editDetailsPuntosServicios", function (data) {
            $("#wrapper-ajax-details").html(data);
            Shadowbox.open({
                content: "#wrapper-ajax-details",
                player: "inline",
                title: "Welcome",
                width: "700px"

            });
        },"ruta=" + _ruta_modelo + "&col=" + _col);
    })

}


function reloadStyles() {
    global_tabla.removeClass("table-bordered");
    global_tabla.removeClass("table-striped");
    global_tabla.addClass("table-bordered");
    global_tabla.addClass("table-striped");
}