﻿$(function () {
    var body = $(document.body);
    $page.loading_all = {
        show: function (title) {
            var div = body.find('div[data-name="loading"]:first');
            div.addClass("loading_all");
            div.find('> img').removeClass("hidden");
            div.find('> span').html(title)
        },
        hide: function () {
            var div = body.find('div[data-name="loading"]:first');
            div.removeClass("loading_all");
            div.find('> img').addClass("hidden");
            div.find('> span').html('')
        }
    }
});
