/*!
* Matricula Core Handler List Event's
* Add Support to handle event's subscription in Matricula Class
* Copyright 2011, David Antonio Muñoz Gaete (http://www.linkedin.com/pub/david-mu%C3%B1oz-gaete/25/2a1/a84)<
* ---------------------------------------------------------------
* Requires : None
*/
$.namespace("Valentys.SDK");

Valentys.SDK.EventSubscriber = function () { };
Valentys.SDK.EventSubscriber.prototype = {
    //---[ Variables
    _$evtlist: {},

    //---[ Method's
    subscribe: function (event, handler, scope) {
        /// <summary>Subscribe to an event</summary>
        /// <param name="event" type="String">Id of the event</param>
        /// <param name="handler" type="Function">Handler</param>
        /// <param name="scope" type="Object">Scope</param>
        (this._getEvent(event, true)).push({ handler: handler, scope: scope });
    },

    unsubscribe: function (event, handler) {
        /// <summary>Unsubscribe to an event</summary>
        /// <param name="event" type="String">Id of the event</param>
        /// <param name="handler" type="Function">Handler</param>
        var evt = this._getEvent(event);
        if (!evt) { return; }
        for (var i = evt.length - 1; i >= 0; i--) {
            var event = evt[i];
            if (event.handler === handler) {
                evt.splice(i, 1);
                return;
            }
        }
    },

    unsubscribeAll: function (event) {
        /// <summary>Clear the Event</summary>
        /// <param name="event" type="String">Id of the event</param>

        if (this._$evtlist[event]) {
            this._$evtlist[event] = [];
        }
    },

    fire: function (event) {
        /// <summary>Fire Event</summary>
        /// <param name="event" type="String">Id of the event</param>

        var evt = this._getEvent(event);
        if (evt) {
            var args = [];
            var lg = arguments.length;
            for (var i = 1; i < lg; i++) {
                args.push(arguments[i]);
            }

            for (var idx in evt) {
                var delegate = evt[idx];
                delegate.handler.apply(delegate.scope, args);
            }
        }
    },

    _getEvent: function (id, create) {
        if (!this._$evtlist[id]) {
            if (!create) { return null; }
            this._$evtlist[id] = [];
        }
        return this._$evtlist[id];
    }
};