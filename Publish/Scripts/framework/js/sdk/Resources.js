/*!
* SDK Resources Manager
* SDK resources manager, for localization or globalization
* Copyright 2011, David Antonio Muñoz Gaete (http://www.linkedin.com/pub/david-mu%C3%B1oz-gaete/25/2a1/a84)
* ---------------------------------------------------------------
* Requires : JQUERY
*/
$.namespace("Valentys.SDK");

Valentys.SDK.Resources = function (resxs) {
    this._resources = resxs;   //Resources Lines

    //Constructor
    this._initialize();
};

Valentys.SDK.Resources.prototype = {
    //--[ Properties
    get_resource: function (resxname) {
        /// <summary>Return the Resource Text</summary>
        var resx = Valentys.SDK.Resources._resources[resxname];

        var args = [];
        var lg = arguments.length;

        args.push(resx);
        for (var i = 1; i < lg; i++) {
            args.push(arguments[i]);
        }
        return $.format.apply(this, args);
    },

    //--[ Constructor
    _initialize: function () {
    /// <summary>Constructor</summary>
    }
};

Valentys.SDK.es_CL = {
    "T40006": "Mensaje de Confirmación",
    	
    "E90000": "Oops!! Debe completar todo los campos obligatorios",
    "E00100": "'Valentys.SDK.UI': El estado '{0}' establecido para el icono de estado no se encuentra registrado",
    "E00101": "'Valentys.SDK.UI': El parametro '{0}' es nulo para el panel de confirmación",
    "E00102": "<strong>Oops!</strong>: esto es embarazoso, pero ha ocurrido un error inesperado en la aplicación",
    
    operator_required: "Valor Requerido",
    operator_numeric: "El Numero no es valido",
    operator_mail: "El e-mail no es valido",
    operator_isRut: "Rut no valido",
    operator_range: "El Valor mo se encuentra dentro del rango valido"
};

Valentys.SDK.Resources = new Valentys.SDK.Resources(Valentys.SDK.es_CL);
var $resx = Valentys.SDK.Resources.get_resource;