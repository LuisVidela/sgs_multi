/*!
* Valentys.SDK Log Tracker
* Log Event Tracker, for Valentys.SDK Event's
* Copyright 2011, David Antonio Muñoz Gaete (http://www.linkedin.com/pub/david-mu%C3%B1oz-gaete/25/2a1/a84)
* ---------------------------------------------------------------
* Requires : Valentys.SDK.SDK.EventSubscriber
*/
$.namespace("Valentys.SDK");


Valentys.SDK.Log = function(cfg){
    /// <summary>Constructor</summary>
    };

Valentys.SDK.Log.prototype = {
    //---[ Enumeration's
    LogTypes: {
        Error: 1,
        Warning: 2,
        Message: 3
    },

    //---[ Variables
    _events: [],

    //---[ Properties
    get_events: function (logtype) {
        /// <summary>Get the event collection</summary>
        /// <param name="logtype" type="Valentys.SDK.Log.LogTypes" mayBeNull="true">Log Type</param>

        if (logtype) {
            var list = [];
            $.each(this._events, function (idx, it) {
                if (it.type == logtype) {
                    list.push(it);
                }
            });
            return list;
        } else {
            return this._events;
        }
    },

    //---[ Method's
    _addEvent: function (message, type, context) {
        /// <summary>Add event into the event collection</summary>
        /// <param name="message" type="String">Event Description</param>
        /// <param name="type" type="Number">Event Type</param>
        /// <param name="context" type="Object">Context</param>

        var evt = {
            date: new Date(),
            message: message,
            type: type,
            context: context
        };

        this._events.push(evt);

        this.fire("trackEvent", evt);   //Dispatch Event
    },

    trackMessage: function (message) {
        /// <summary>Add message into the track</summary>
        /// <param name="message" type="String">Event Description</param>

        this._addEvent(message, Valentys.SDK.Log.LogTypes.Message);
    },

    trackError: function (exception, throwerror) {
        /// <summary>Add exception into the track</summary>
        /// <param name="exception" type="Object">Exception To Track</param>

        //If String , Format
        if (typeof exception == "string") {
            exception = { 
                message: exception 
            };
            
        }else if (exception.responseText) {
            exception = {
                message: exception.statusText,
                status: exception.status,
                fragment: exception.responseText
            };
        }

        Valentys.SDK.UI.toast($resx("E00102"));

        if(Valentys.SDK.isDebuggingEnabled){
            alert($.toJSON(exception));
        }
        
        this._addEvent(exception.message, Valentys.SDK.Log.LogTypes.Error, exception);

        if (throwerror) {
            throw exComplete;
        }
    },

    trackWarning: function (message) {
        /// <summary>Add Warning into the track</summary>
        /// <param name="message" type="String">Message to track</param>

        this._addEvent(message, Valentys.SDK.Log.LogTypes.Warning);
    }
};

$.extendClass(Valentys.SDK.Log,Valentys.SDK.EventSubscriber);   //Extend From Event Subscriber

//Static Instance
Fwk.registerStatic('Valentys.SDK.Log');