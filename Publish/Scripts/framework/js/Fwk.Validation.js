﻿/*!
* Valentys Javascript Client Framework
* http://www.valentys.com
*
* Copyright 2012, David Antonio Muñoz Gaete
*/

$.namespace("Fwk");

/*
* ----------------------------------- 
* Framework Validation Library
* -----------------------------------
*/
Fwk.Validation = function () {
    var ref = this;

    // SUBMIT CALL OUT FORM
    $("body").on("submit", "form", function (evt) {
        var validationGroup = $(this).data("val-group");

        var ret = ref.Validate(validationGroup);
        if (!ret) {
            return false;
        }
    });
};
Fwk.Validation.prototype = {

    /*
    * ----------------------------------- 
    * Validation Initialization
    * -----------------------------------
    */
    _initialize: function (elms) { },

    /*
    * ----------------------------------- 
    * Show/Create a Bubble with the validation message 
    * -----------------------------------
    */
    _showBubble: function (target, key, operator) {
        // First Get if the input has a custom message for the validation
        var message = $(target).data('val-message');

        var id = $(target).attr("name") || $(target).attr("id");
        if (!(message) && key != null) {

            message = $resx(key);
        }


        // Only create a message if: have a custom message or have a key to
        // extract the message, or the custom message is empty
        if (message !== null && message !== "") {
            // If have a message
            var bid = "bubble_" + id + "_" + operator;
            var bubble = $("#" + bid);


            // If exist, remove
            if (bubble.get(0)) {
                bubble.remove();
            }

            //create the bubble =)
            var html = [];
            html.push("<div id=" + bid + " class='validation-bubbleInfo'>");
            html.push("<div class='validation-bubbleInfo-content'>");
            html.push(message);
            html.push("</div>");
            html.push("</div>");
            bubble = $(html.join(''));

            $(document.body).append(bubble);
        }

        // Not Animated yet??
        if (!(bubble.data("ani"))) {
            var tposition = $(target).offset();
            var twidth = $(target).width();

            bubble.css({
                "top": tposition.top + "px",
                "left": (tposition.left + twidth + 10) + "px"
            });

            setTimeout(function () {
                bubble.fadeOut(function () {
                    bubble.data("ani", 0);

                    $('div[class="ui-effects-wrapper"]').hide();    //remove wrapper bug for bounc
                });
            }, 1500);

            bubble.effect("bounce", {
                times: 3
            }, 300);
            bubble.data("ani", 1);
        }
    },

    _hideBubble: function (target, operator) {
        var id = $(target).attr("id");
        var bid = "bubble_" + id + "_" + operator;
        $("#" + bid).hide();
    },

    /*
    * ----------------------------------- 
    * Validation Main Function
    * -----------------------------------
    */
    Validate: function (argument) {
        var elements = [];
        var validators = [];

        //if send validationGroup or not send, get the all elements or the elements that matches the validationGroup
        if (typeof argument == "undefined" || typeof argument == "string" || argument == null) {
            var selector = "[data-val-for]";
            if (argument) {
                selector += $.format("[data-val-group='{0}']", argument);
            }

            $(selector).each(function () {
                elements.push($(this));
            });
        } else {
            //otherwise, send the element to validate
            elements.push(argument)
        }

        //for each elements, get the validators
        $.each(elements, function (idx, it) {
            var id = $(this).data('val-for');
            var rules = $(this).data('val-rules');
            var group = $(this).data('val-group');

            rules = (!rules) ? rules = ["required"] : rules.split("|");
            $.each(rules, function (index, rule) {
                var name = rule;
                var options = "";
                var offset = name.indexOf("[");

                if (offset >= 0) {
                    options = name.substring(offset + 1, name.length - 1);
                    name = name.substring(0, offset);
                }

                rules[index] = {
                    operator: name,
                    arguments: options
                };
            });

            validators.push({
                id: id,
                rules: rules,
                group: group || "",
                source: $(this)
            });
        });


        return this._validateByValidationGroup(validators);
    },

    _validateByValidationGroup: function (validators) {
        var isValid = true; // Global Flag

        for (var index in validators) {
            var validation = validators[index];

            if (!this.__validate(validation)) {
                isValid = false;
            }
        }

        // Cancel Submit??
        return isValid;
    },

    __validate: function (validation) {

        // Has Various ID?? (One Span may occurs for various target's)
        var targets = (validation.id.indexOf("|") >= 0 ? validation.id.split("|") : [validation.id]);

        var error_class = "input-validation-error";  // Validation Error
        // Class
        var isValid = true;

        // Foreach Target (commonly only one)
        for (var ii in targets) {
            var __id = targets[ii];

            var target = __id == "self" ? validation.source : $("#" + targets[ii]);

            // Target Exist??.
            // Target Disable??, don't validate , because is disabled, and means
            // is the user can't put data into them.
            if (target.get(0) == null || target.is(":disabled")) {
                continue;
            }

            var valid = true;
            for (var rii in validation.rules) {
                var rule = validation.rules[rii];

                var operator = Fwk.Validation.Operators[rule.operator];

                if (operator) {
                    valid = operator.isValid(target, rule.arguments, validation.source);
                } else {
                    alert("El operador " + rule.operator + " no existe!");
                }

                var noani = target.attr("data-val-noani");

                if (!valid) {
                    isValid = false;
                    if (!noani) {
                        this._showBubble(target, operator.resxName, rule.operator, operator);
                    }
                    break;
                } else {
                    if (!noani) {
                        this._hideBubble(target, rule.operator);
                    }
                }

            }

            (!valid) ? target.addClass(error_class) : target.removeClass(error_class);
        }

        return isValid;
    },


    /*
    * ----------------------------------- 
    * Available Operators
    * -----------------------------------
    */
    Operators: {

        // Required Constraint
        required: {
            resxName: "operator_required",
            isValid: function (target, args) {
                var ret = false;

                // is Checkbox??
                var nodeName = target.get(0).nodeName.toLowerCase();
                switch (nodeName) {
                    case "input":
                        var type = target.attr("type");
                        switch (type) {
                            case "radio":
                                var name = target.attr("name");
                                var radios = $($.format("[name='{0}']", name));

                                var fn = function (id, v) {
                                    var label = $($.format("label[for='{0}']", id));
                                    label.css("color", (v ? "#000" : "red"));

                                };

                                radios.each(function (idx, it) {
                                    if (!ret) {
                                        ret = it.checked;
                                    }
                                    fn($(it).attr("id"), ret);
                                });

                                if (ret) {
                                    radios.each(function (idx, it) {
                                        fn($(it).attr("id"), ret);
                                    });
                                }
                                break;
                            default:
                                ret = target.val() !== null && target.val() !== "";
                                break;
                        }
                        break;
                    default:
                        ret = target.val() !== null && target.val() !== "";
                }

                return ret;
            }
        },

        // CallBack Function Checker
        callback: {
            isValid: function (target, args) {
                if (typeof (window[args]) == "function") {
                    var _args = {};

                    var ret = window[args](target, _args);
                    if (_args.resx) {

                        Fwk.Validation.Operators.callback.resxName = _args.resx;
                    }

                    return ret;
                } else {
                    alert("la función de callback " + args + " no existe!");
                }
                return false;
            }
        },

        // Is RUT Operator, Check if the value is a valid RUT
        isRut: {
            resxName: "operator_isRut",
            isValid: function (target, args, source) {
                var value = $(target).val();

                // Get the Values
                var ids = $(source).attr("data-val-for").split("|");
                if (ids.length > 1) {
                    value = $("#" + ids[0]).val() + $("#" + ids[1]).val();
                }

                var hasSeparator = (value.indexOf("-") > 0);
                var run = hasSeparator ? value.substring(0, value.indexOf("-")) : value.substring(0, value.length - 1);
                var dv = hasSeparator ? value.substring(value.indexOf("-") + 1) : value.substring(value.length - 1);
                var t = 2;
                var D = "";
                var sum = 0;
                for (var i = run.length; i > 0; i--) {
                    sum = sum + (t * Number(run.charAt(i - 1)));
                    t += 1;
                    if (t == 8) {
                        t = 2;
                    }
                }
                t = 11 - (sum % 11);
                switch (t) {
                    case 10:
                        D = "k";
                        break;
                    case 11:
                        D = "0";
                        break;
                    default:
                        D = "" + t;
                        break;
                }
                return (run && dv.length > 0 && D.toLowerCase() == dv.toLowerCase());
            }
        },

        // Numeric Constraint
        numeric: {
            resxName: "operator_numeric",
            isValid: function (target, args) {
                var value = target.val().replace(/\s/ig, "");
                var pattern = /^\d+$/;
                return pattern.test(value);
            }
        },

        range: {
            resxName: "operator_range",
            isValid: function (target, args, source) {
                var value = accounting.unformat(target.val().replace(/\s/ig, ""), ",");
                ;

                var min = accounting.unformat(source.attr("data-val-range-min"), ",");
                ;
                var max = accounting.unformat(source.attr("data-val-range-max"), ",");
                ;

                return value <= max && value >= min;
            }
        },

        // E-Mail Constraint
        mail: {
            resxName: "operator_mail",
            isValid: function (target, args) {
                var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                return pattern.test(target.val());
            }
        }
    }

};




Fwk.registerStatic("Fwk.Validation");  // Framework Register
