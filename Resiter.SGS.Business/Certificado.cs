﻿using System;
using System.Data;

namespace Resiter.SGS.Business
{
    public class Certificado : CommonBase
    {
        private readonly DataAccess.Certificado _datCertificado;

        public Certificado()
        {
            _datCertificado = new DataAccess.Certificado();
        }


        /// <summary>
        /// Busca la cabecera de certificado
        /// </summary>
        /// <param name="rutCliente">Rut de cliente</param>
        /// <param name="razonSocial">Nombre Razón social</param>
        /// <param name="sucursal">Nombre Sucursal</param>
        /// <param name="fechaHasta">Fecha fin período</param>
        /// <param name="vertedero">Nombre Vertedero</param>
        /// <param name="codigoUnidad">Código ID de unidad</param>
        /// <param name="idPuntoServicio">Id de punto de servicio escogido. Null en caso de no escoger ninguno</param>
        /// <returns>Listado de cabecera de certificado</returns>
        public DataTable GetHeaderCertificado(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, string vertedero, int? codigoUnidad, int? idPuntoServicio)
        {
            DataTable dt = null;
            try
            {
                dt = _datCertificado.GetHeader(rutCliente, razonSocial, sucursal, fechaHasta, vertedero, codigoUnidad, idPuntoServicio);
                Exception = _datCertificado.Exception;
                return dt;
            }
            catch(Exception ex)
            {
                Exception = ex;
            }

            return dt;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutCliente"></param>
        /// <param name="razonSocial"></param>
        /// <param name="sucursal"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="endDate"></param>
        /// <param name="vertedero"></param>
        /// <param name="codigoUnidad"></param>
        /// <returns></returns>
        public DataTable GetDetailCertificado(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, DateTime? endDate, string vertedero, int? codigoUnidad)
        {
            DataTable dt = null;
            try
            {
                dt = _datCertificado.GetDetail(rutCliente, razonSocial, sucursal, fechaHasta, endDate, vertedero, codigoUnidad);
                Exception = _datCertificado.Exception;
                return dt;
            }
            catch (Exception ex)
            {
                Exception = ex;
            }

            return dt;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutCliente"></param>
        /// <param name="razonSocial"></param>
        /// <param name="sucursal"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="vertedero"></param>
        /// <param name="codigoUnidad"></param>
        /// <returns></returns>
        public DataTable GetValidacionCertificado(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, string vertedero, int? codigoUnidad)
        {
            DataTable dt = null;
            try
            {
                dt = _datCertificado.ValidaCertificado(rutCliente, razonSocial, sucursal, fechaHasta, vertedero, codigoUnidad);
                Exception = _datCertificado.Exception;
                return dt;
            }
            catch (Exception ex)
            {
                Exception = ex;
            }

            return dt;
        }
    }
}
