﻿using System;

namespace Resiter.SGS.Business
{
    public class CommonBase
    {
        private Exception exception;

        public CommonBase()
        {
        }
        public Exception Exception
        {
            get
            {
                return exception;
            }
            set
            {
                exception = value;
            }
        }

        public bool HasError
        {
            get
            {
                return exception != null;
            }
        }

        public void ClearException()
        {
            exception = null;
        }
    }
}
