BEGIN TRANSACTION;


BEGIN TRY -- comienzo del try

-- Script Chiloe
-- Constantes
DECLARE @Id_pais INT
,@Id_region INT
,@Id_ciudad INT
,@Id_comuna INT
,@Id_estado INT
,@Residuos INT
,@NombreCentroCosto VARCHAR(150)


SET @Id_pais = 3 -- chile
SET @Id_region = 10 -- los lagos
SET @Id_ciudad = 36 --chiloe
SET @Id_comuna = 251 --osorno
SET @Id_estado = 1 --estado activo
SET @Residuos = 32--residuos domesticos
SET @NombreCentroCosto = 'CC Chiloe'

-- Se corrigen los RUT inv�lidos de clientes:
UPDATE Puntos_Chiloe SET Rut = '84449400-9' WHERE Rut IN ('84449400-10', '84449400-11')


--Se modifican los tipos de industria equivalentes a los rubros en la base de datos
PRINT 'UPDATE Tipo Industria'
UPDATE Puntos_Chiloe SET [Tipo Industria] = 'Alimentaci�n' WHERE [Tipo Industria] = 'Alimenticia'
UPDATE Puntos_Chiloe SET [Tipo Industria] = 'Industrial' WHERE [Tipo Industria] = 'Industria'
UPDATE Puntos_Chiloe SET [Tipo Industria] = 'Mataderos' WHERE [Tipo Industria] = 'Matadero'

PRINT 'Inserci�n nuevos rubros'
--Se insertan los nuevos tipos de rubro
INSERT INTO [RS_SGS_ARAUCO].[dbo].[rubros]
           ([rubro])
SELECT DISTINCT po.[Tipo Industria]
FROM Puntos_Chiloe po
WHERE po.[Tipo Industria] NOT IN (SELECT ru.rubro FROM RS_SGS_ARAUCO.dbo.rubros ru)

PRINT 'Inserci�n nuevos tipo_servicio'
--Se inserta un nuevo tipo servicio "traslado" y "Multibacket" para coincidircon los registros a ingresar
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_servicio]
           ([tipo_servicio])
     VALUES
           ('Traslado')
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_servicio]
           ([tipo_servicio])
     VALUES
           ('Multibacket')

PRINT 'UPDATE comunas'
--Se modifican las comunas Quell�n y Queil�n para coincidir con el nombre en la base de datos
UPDATE Puntos_Chiloe SET Comuna = 'Quell�n' WHERE Comuna = 'Quellon'
UPDATE Puntos_Chiloe SET Comuna = 'Queil�n' WHERE Comuna = 'Queilen'

--Se actualiza la sucursal "Cermaq Chile Chequian", por causar un error al insertar
UPDATE Puntos_Chiloe SET [Sucursal Oficial] = 'Cermaq Chile Chequian N' WHERE Comuna = 'Area Norte'

PRINT 'Se insertan nuevas comunas'
--Se agregan nuevas comunas
INSERT INTO [RS_SGS_ARAUCO].[dbo].[comunas]
           ([nombre_comuna]
           ,[id_ciudad])
SELECT DISTINCT po.Comuna 
,@Id_ciudad
FROM Puntos_Chiloe po
WHERE po.Comuna NOT IN (SELECT com.nombre_comuna FROM RS_SGS_ARAUCO.dbo.comunas com)


--Se modifican los tipos de equipo para coincidir con los campos en la base de datos arauco
--PRINT 'UPDATE Tipo Equipo'
--UPDATE Puntos_Chiloe SET [Tipo Equipo]  = (CASE 
--			WHEN [Nombre Equipo] LIKE 'Estanque Aljibes'
--			THEN CONCAT('ALJIBE',' ',Capacidad)
--			ELSE CONCAT([Tipo Equipo],' ',Capacidad)

--		END
--		)

PRINT 'Inserci�n nuevos tipo_servicio desde los puntos'
--Inserci�n nuevos tipo de servicio
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_servicio]
           ([tipo_servicio])
SELECT DISTINCT
po.Sistema
FROM Puntos_Chiloe po
WHERE po.Sistema NOT IN (SELECT ts.tipo_servicio FROM RS_SGS_ARAUCO.dbo.tipo_servicio ts)

PRINT 'Inserci�n modificaci�n estado de camiones'
--Se modifica el estado de los camiones para su equivalencia en la bd de destino
UPDATE Camiones_Chiloe SET Estado = 'Activo' WHERE Estado = 'Operativo'

PRINT 'Inserci�n nuevos tipo_contrato'
--inserci�n nuevos tipos de contrato
INSERT INTO RS_SGS_ARAUCO.dbo.tipo_contrato
(tipo_contrato)
select DISTINCT [Sistema] FROM Puntos_Chiloe po
WHERE po.[Sistema] NOT IN (SELECT tc.tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc)

PRINT 'Inserci�n centro_responsabilidad'
--Inserci�n de centro de responsabilidad
INSERT INTO RS_SGS_ARAUCO.dbo.centro_responsabilidad
(centro_responsabilidad)
select DISTINCT [Centro de Responsabilidad] FROM Puntos_Chiloe po
WHERE po.[Centro de Responsabilidad] NOT IN (SELECT cr.centro_responsabilidad FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr)

PRINT 'Inserci�n centro_costo'
--Insert de centro_costo
INSERT INTO [RS_SGS_ARAUCO].[dbo].[centro_costo]
           ([nombre])
SELECT @NombreCentroCosto WHERE @NombreCentroCosto NOT IN (SELECT vcc.nombre FROM [RS_SGS_ARAUCO].[dbo].[centro_costo] vcc)

PRINT 'Inserci�n unidades de negocio'
--Inserci�n nueva unidad de negocio
INSERT INTO [RS_SGS_ARAUCO].[dbo].[unidades_negocio]
           ([unidad]
           ,[estado]
           ,[id_empresa]
           ,[id_centro_responsabilidad]
           ,[codigo_unidad])
     SELECT DISTINCT 
	 po.Unidad
	 ,'Activa' --estado de la unidad
	 ,2 --id de empresa, resiter
	 ,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
	 ,po.[Centro de Responsabilidad]
	 FROM Puntos_Chiloe po
WHERE po.Unidad NOT IN (SELECT u.unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio u)
PRINT 'Inserci�n clientes'
-- carga de clientes
INSERT INTO RS_SGS_ARAUCO.dbo.clientes
           ([rut_cliente]
           ,[razon_social]
           ,[direccion_cliente]
           ,[id_pais]
           ,[fono_comercial]
           ,[giro_comercial]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[id_unidad]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_rubro]
           ,[nombre_fantasia]
           ,[cobro_especial]
           ,[id_centro_costo])
SELECT RTRIM(LTRIM(po.RUT))
,po.[Raz�n Social]
,CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero])
,@Id_pais
,null
,null
,null
,null
,null
,null
,null
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,null -- nombre fantasia no se indica
,'False'
,(SELECT cc.id FROM RS_SGS_ARAUCO.dbo.centro_costo cc WHERE cc.nombre = @NombreCentroCosto)
FROM Puntos_Chiloe po
WHERE RTRIM(LTRIM(po.RUT)) NOT IN (SELECT rut_cliente FROM RS_SGS_ARAUCO.dbo.clientes)


--***********************************

PRINT 'Inserci�n sucursales'
--carga de sucursales
INSERT INTO RS_SGS_ARAUCO.dbo.[sucursales]
           ([sucursal]
           ,[descripcion]
           ,[direccion]
           ,[contacto_sucursal]
           ,[email_sucursal]
           ,[fono_sucursal]
           ,[celular_sucursal]
           ,[zona]
           ,[periodo_desde]
           ,[periodo_hasta]
           ,[estado_sucursal]
           ,[id_rubro]
           ,[id_cliente]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[rut_sucursal]
           ,[id_centro]
           ,[franja_horaria])

SELECT DISTINCT po.[Sucursal Oficial] --se utiliz� el est� nombre para la sucursal por la referencia en la base de datos actual de osorno
, null
,CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal])
, null
, null
, null
, null
, 'Sur'
, 1--default como el resto de periodos
, 1--default como el resto de periodos
, 'Activa'
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE RTRIM(LTRIM(po.Rut)))
,@Id_pais
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,null
,null
,null
FROM Puntos_Chiloe po
WHERE po.[Sucursal Oficial] NOT IN (SELECT sucursal FROM RS_SGS_ARAUCO.dbo.sucursales)



----ingreso de vertedero "SIN VERTEDERO"
--INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
--           ([vertedero]
--           ,[direccion_vertedero]
--           ,[id_pais]
--           ,[id_region]
--           ,[id_ciudad]
--           ,[id_comuna]
--           ,[id_unidad]
--           ,[disposicion_final])
--SELECT DISTINCT
--'SIN VERTEDERO'
--,'s/n'
--,@Id_pais
--,@Id_region
--,@Id_ciudad
--,@Id_ciudad
--,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE 'Osorno')
--,'True' --se inserta "true"como en la mayor�a de los registros de la bd
--FROM Puntos_Chiloe po
--WHERE 'SIN VERTEDERO' NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)

PRINT 'Inserci�n nuevos vertederos'
--Ingreso nuevos vertederos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
           ([vertedero]
           ,[direccion_vertedero]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_unidad]
           ,[disposicion_final])
SELECT DISTINCT
po.Vertedero
,'s/n'
,@Id_pais
,@Id_region
,(SELECT com.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas com WHERE com.nombre_comuna LIKE po.Comuna)
,(SELECT com.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas com WHERE com.nombre_comuna LIKE po.Comuna)
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
,'True' --se inserta "true"como en la mayor�a de los registros de la bd
FROM Puntos_Chiloe po
WHERE po.Vertedero NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)



PRINT 'Inserci�n contratos'
--Inserci�n de contratos
INSERT INTO RS_SGS_ARAUCO.dbo.[contratos]
           ([id_tipo_contrato]
           ,[id_cliente]
           ,[id_sucursal]
           ,[fecha_inicio]
           ,[fecha_renovacion]
           ,[estado]
           ,[id_modelo_cobro]
           ,[id_centro_responsabilidad]
           ,[id_unidad])
SELECT DISTINCT (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE RTRIM(LTRIM(po.Rut)))
,(SELECT top 1 suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
,GETDATE() -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
,DATEADD(YEAR, 1, GETDATE()) -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
, 'Activo'
,null--(SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_Chiloe po
WHERE 1 > (
	SELECT COUNT(*) FROM RS_SGS_ARAUCO.dbo.[contratos] con
	WHERE con.id_unidad = (SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad) -- Revisar
AND con.id_centro_responsabilidad = (SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
AND con.id_cliente = (SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE RTRIM(LTRIM(po.Rut)))
AND con.id_tipo_contrato = (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT top 1  suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)


PRINT 'Inserci�n tipo_tarifa'
--carga tipo de tarifa
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_tarifa]
           ([tipo_tarifa]
           ,[retencion]
           ,[inicio_tarifa]
           ,[termino_tarifa]
           ,[valor_tarifa]
           ,[tarifa_escalable]
		   ,[id_unidad])
SELECT
CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
										END
))
,0 -- no existe campo que lo indique
,1 -- se deja segun los registros que exist�an en osorno
,12 -- se deja segun los registros que exist�an en osorno
,(CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
											WHEN po.[Valor Fijo Mensual] = 0 AND po.[Valor Vertedero] = 0 AND po.[Valor Retiro o Volteo] = 0 OR po.[Valor Retiro o Volteo] is null
											THEN 0
										END
)
,'False'
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_Chiloe po
WHERE CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
										END
)) NOT IN (SELECT tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa)

PRINT 'Inserci�n nuevos equipos'
--Carga equipos nuevos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[equipos]
           ([tipo_equipo]
           ,[capacidad_equipo]
           ,[nom_abreviado]
           ,[nom_nemo])
SELECT DISTINCT
 po.[Tipo Equipo]
,IIF(po.Capacidad is null, 0, CAST((RTRIM(SUBSTRING(po.Capacidad, 1, 2))) AS FLOAT))
,RTRIM(SUBSTRING([Tipo Equipo],1, 5))
,null
FROM Puntos_Chiloe po
WHERE [Tipo Equipo] <> NULL AND po.[Tipo Equipo] <> (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos) AND CAST((RTRIM(SUBSTRING(po.Capacidad, 1, 2))) AS FLOAT) NOT IN (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos) OR [Tipo Equipo] <> 'Disposici�n'

--*************************************************************
--SELECT ps.nombre_punto FROM RS_SGS_ARAUCO.dbo.puntos_servicio ps

--*************************************************************
PRINT 'Inserci�n puntos_servicio'
--Carga de puntos de servicio
INSERT INTO RS_SGS_ARAUCO.dbo.puntos_servicio --puntos_servicio
           ([nombre_punto]
           ,[estado_punto]
           ,[id_contrato]
           ,[id_vertedero]
           ,[id_residuo]
           ,[id_tipo_tarifa]
           ,[lunes]
           ,[martes]
           ,[miercoles]
           ,[jueves]
           ,[viernes]
           ,[sabado]
           ,[domingo]
           ,[id_equipo]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[cant_equipos]
           ,[pago_por_peso]
           ,[codigo_externo_asignado])
SELECT DISTINCT
CONCAT(Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado])
--(SELECT DISTINCT po.Sucursal2 FROM Puntos_Chiloe po WHERE Sucursal2 <> 'Aquagen Tocoihue  CRE 3 M3')
,@Id_estado
,(SELECT top 1 con.id_contrato FROM RS_SGS_ARAUCO.dbo.[contratos] con WHERE
con.id_unidad = (SELECT top 1 un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
AND con.id_centro_responsabilidad = (SELECT top 1 cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
AND con.id_cliente = (SELECT top 1 cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE RTRIM(LTRIM(po.Rut)))
AND con.id_tipo_contrato = (SELECT top 1 tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT top 1 suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)
,CASE
WHEN po.[Sucursal Oficial] LIKE '%Traslado%' OR po.[Sucursal Oficial] LIKE '%Instalaci�n%'
THEN (SELECT top 1 ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE '%SIN VERTEDERO%')
ELSE (SELECT top 1 ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE po.Vertedero)
END
,@Residuos
,(SELECT top 1 tt.id_tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa tt WHERE tt.tipo_tarifa = (CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]

										END
))))
,IIF(po.L is null, 'False', 'True')
,IIF(po.M is null, 'False', 'True')
,IIF(po.MI is null, 'False', 'True')
,IIF(po.J is null, 'False', 'True')
,IIF(po.V is null, 'False', 'True')
,IIF(po.S is null, 'False', 'True')
,IIF(po.D is null, 'False', 'True')
,(CASE 
			WHEN po.[Nombre Equipo] LIKE '%Estanque Aljibes%'
			THEN (SELECT eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT('ALJIBE',' ',po.Capacidad)))
			ELSE (SELECT top 1 eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT(po.[Tipo Equipo],' ',po.Capacidad)))

		END
		)
,po.[Contacto Comercial]
,po.[Correo Comercial]
,po.[Fono Comercial]
,null
,po.[N� Equipos]
,(CASE
		WHEN po.Observaci�n LIKE '%kilo transportado%' --cuando la observaci�n dice "kilo" transportado se asume que el cobro es por peso
		THEN 'True'
		ELSE 'False'
	END
)
,'True' -- Se deja como true en relacion a los campos que ya se encuentran en la base de datos
FROM Puntos_Chiloe po
WHERE CONCAT(po.Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado]) NOT IN (SELECT top 1 ps.nombre_punto FROM RS_SGS_ARAUCO.dbo.puntos_servicio ps WHERE ps.nombre_punto LIKE CONCAT(po.Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado]))

--select * from Puntos_Chiloe where Sucursal2 LIKE 'Aquagen Tocoihue  CRE 3 M3%'
--select Sucursal2, [Valor Pago Franquiciado], Vertedero, count(*) from Puntos_Chiloe group by Sucursal2, [Valor Pago Franquiciado], Vertedero order by count(*) desc

PRINT 'Inserci�n modelo_cobro'
-- Modelo de Cobro
INSERT INTO [RS_SGS_ARAUCO].[dbo].[modelo_cobro]
           ([modelo]
           ,[id_tipo_modelo]
           ,[id_punto_servicio])
SELECT
CONCAT(po.Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado])
,1--Industrial, es �nico, porque el otro es minera
,(SELECT ps.id_punto_servicio FROM [RS_SGS_ARAUCO].[dbo].puntos_servicio ps WHERE ps.nombre_punto LIKE CONCAT(po.Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado]))
FROM Puntos_Chiloe po
WHERE CONCAT(po.Sucursal2, ' ', po.Vertedero, ' ', po.[Valor Pago Franquiciado]) NOT IN (SELECT modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro)

------

PRINT 'Inserci�n camiones_marcas'
--carga camiones_marcas
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones_marcas]
           ([marca])
SELECT
cam.[Marca ]
FROM Camiones_Chiloe cam
WHERE cam.[Marca ] NOT IN (SELECT marca FROM RS_SGS_ARAUCO.dbo.camiones_marcas)

PRINT 'Inserci�n franquiciados'
--carga franquiciados
INSERT INTO [RS_SGS_ARAUCO].[dbo].[franquiciado]
           ([id_comuna]
           ,[rut_franquiciado]
           ,[razon_social]
           ,[fecha_contrato]
           ,[estado]
           ,[observacion]
           ,[direccion]
           ,[anticipo]
           ,[porcentaje_anticipo]
           ,[id_unidad])
SELECT
null
,cam.CI
,cam.Franquiciado
,null -- no se indica la fecha
,cam.Estado
,null -- no se indican observaciones
,null -- no se indica direccion
,'False' -- no se indica anticipo
,null -- no se indica porcentaje
,(SELECT un.id_unidad FROM [RS_SGS_ARAUCO].[dbo].unidades_negocio un WHERE un.unidad LIKE 'Osorno' )
FROM Camiones_Chiloe cam
WHERE cam.CI NOT IN (SELECT rut_franquiciado FROM RS_SGS_ARAUCO.dbo.franquiciado)

PRINT 'Inserci�n camiones'
--carga camiones
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones]
           ([camion]
           ,[patente]
           ,[tipo_camion]
           ,[modelo_camion]
           ,[capacidad_camion]
           ,[estado]
           ,[observacion]
           ,[id_camion_marca]
           ,[id_tipo_servicio]
           ,[prop_resiter]
           ,[prop_franquiciado]
           ,[prop_otro]
           ,[fecha_rvt]
           ,[id_franquiciado]
           ,[num_interno])
SELECT DISTINCT
cam.[Tipo Servicio]
,cam.[Placa Patente]
,cam.[Tipo Servicio]
,cam.Modelo	
,null -- no se informa
,Estado
,null --no se informa
,IIF(cam.[Marca ] is null, 6, (SELECT top 1 cm.id_camion_marca FROM [RS_SGS_ARAUCO].[dbo].[camiones_marcas] cm WHERE cam.[Marca ] = cm.marca))
,IIF(cam.[Tipo Servicio] is null, (SELECT ts.id_tipo_servicio FROM [RS_SGS_ARAUCO].[dbo].tipo_servicio ts WHERE ts.tipo_servicio LIKE '%Traslado%')
								, (SELECT top 1 ts.id_tipo_servicio FROM [RS_SGS_ARAUCO].[dbo].tipo_servicio ts WHERE cam.[Tipo Servicio] = ts.tipo_servicio))
,'False' -- se deja como false por aparecer como "tipo chofer franquiciado"
,'True' -- True por aparecer como "tipo chofer franquiciado"
,'False' -- misma raz�n
,null -- no se informa
,(SELECT top 1 fr.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado fr WHERE cam.CI = fr.rut_franquiciado)
,(SELECT (MAX(ca.num_interno) +1) FROM [RS_SGS_ARAUCO].[dbo].camiones ca)
FROM Camiones_Chiloe cam
WHERE cam.[Placa Patente] NOT IN (SELECT patente FROM RS_SGS_ARAUCO.dbo.camiones)


PRINT 'Inserci�n conductores'
--carga conductores
INSERT INTO [RS_SGS_ARAUCO].[dbo].[conductores]
           ([nombre_conductor]
           ,[rut_conductor]
           ,[fecha_ingreso]
           ,[id_tipo_licencia]
           ,[estado]
           ,[observacion]
           ,[id_franquiciado]
           ,[fecha_vencimientolic])
SELECT
cam.[Conductor               Turno 1]
,cam.CI1
,null -- no se informa
,1--(SELECT lic.id_tipo_licencia FROM [RS_SGS_ARAUCO].[dbo].tipo_licencia lic WHERE lic.tipo_licencia LIKE ) se deja como A-1 como la mayr�a de las licencias
,'Activo' -- no se informa, pero se asume que est� activo
,null -- no se informan observaciones
,(SELECT top 1 f.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado f WHERE cam.CI LIKE f.rut_franquiciado)
,null -- no se informa
FROM Camiones_Chiloe cam
WHERE cam.CI1 NOT IN (SELECT rut_conductor FROM RS_SGS_ARAUCO.dbo.conductores)


END TRY
BEGIN CATCH
SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO