USE [RS_SGS_ARAUCO]
GO

/****** Object:  Table [dbo].[Zona]    Script Date: 15-01-2016 11:02:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Zona](
	[id_zona] [int] IDENTITY(1,1) NOT NULL,
	[nombre_zona] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Zonas] PRIMARY KEY CLUSTERED 
(
	[id_zona] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE sucursales ADD zona_id_zona INT NULL;
GO

INSERT INTO [RS_SGS_ARAUCO].[dbo].[Zona]
           ([nombre_zona])
     VALUES
           ('Sin Zona')
GO

