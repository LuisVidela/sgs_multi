BEGIN TRANSACTION;

BEGIN TRY -- comienzo del try

-- Script Osorno
-- Constantes
DECLARE @Id_pais INT
,@Id_region INT
,@Id_ciudad INT
,@Id_comuna INT
,@Id_estado INT
,@Residuos INT
,@NombreCentroCosto VARCHAR(150)


SET @Id_pais = 3 -- chile
SET @Id_region = 10 -- los lagos
SET @Id_ciudad = 37 --osorno
SET @Id_comuna = 251 --osorno
SET @Id_estado = 1 --estado activo
SET @Residuos = 32--residuos domesticos
SET @NombreCentroCosto = 'CC Osorno'

--Se modifican los tipos de industria equivalentes a los rubros en la base de datos
PRINT 'UPDATE Tipo Industria'
UPDATE Puntos_Osorno SET [Tipo Industria] = 'Alimentaci�n' WHERE [Tipo Industria] = 'Alimentos'
UPDATE Puntos_Osorno SET [Tipo Industria] = 'Industrial' WHERE [Tipo Industria] = 'Industria'
UPDATE Puntos_Osorno SET [Tipo Industria] = 'Mataderos' WHERE [Tipo Industria] = 'Matadero'

--Se modifican los tipos de equipo para coincidir con los campos en la base de datos arauco
PRINT 'UPDATE Tipo Equipo'
UPDATE Puntos_Osorno SET [Tipo Equipo]  = (CASE 
			WHEN [Nombre Equipo] LIKE 'Estanque Aljibes'
			THEN CONCAT('ALJIBE',' ',Capacidad)
			ELSE CONCAT([Tipo Equipo],' ',Capacidad)

		END
		)
--Inserci�n nuevo tipo de servicio "batea"
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_servicio]
           ([tipo_servicio])
SELECT
           ('Batea')
WHERE 'Batea' NOT IN (SELECT ts.tipo_servicio FROM RS_SGS_ARAUCO.dbo.tipo_servicio ts)
--Se modifica el estado de los camiones para su equivalencia en la bd de destino
UPDATE Camiones_Osorno SET Estado = 'Activo' WHERE Estado = 'Operativo'

--Inserci�n nuevos rubros
INSERT INTO RS_SGS_ARAUCO.dbo.rubros
([rubro])
select DISTINCT [Tipo Industria] FROM Puntos_Osorno po
WHERE po.[Tipo Industria] NOT IN (SELECT ru.rubro FROM RS_SGS_ARAUCO.dbo.rubros ru)

--inserci�n nuevos tipos de contrato
INSERT INTO RS_SGS_ARAUCO.dbo.tipo_contrato
(tipo_contrato)
select DISTINCT [Sistema] FROM Puntos_Osorno po
WHERE po.[Sistema] NOT IN (SELECT tc.tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc)

--Inserci�n de centro de responsabilidad
INSERT INTO RS_SGS_ARAUCO.dbo.centro_responsabilidad
(centro_responsabilidad)
select DISTINCT [Centro de Responsabilidad] FROM Puntos_Osorno po
WHERE po.[Centro de Responsabilidad] NOT IN (SELECT cr.centro_responsabilidad FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr)

--Insert de centro_costo
INSERT INTO [RS_SGS_ARAUCO].[dbo].[centro_costo]
           ([nombre])
SELECT @NombreCentroCosto WHERE @NombreCentroCosto NOT IN (SELECT vcc.nombre FROM [RS_SGS_ARAUCO].[dbo].[centro_costo] vcc)

--select * from [RS_SGS_ARAUCO].[dbo].[centro_costo]

--Inserci�n nueva unidad de negocio
INSERT INTO [RS_SGS_ARAUCO].[dbo].[unidades_negocio]
           ([unidad]
           ,[estado]
           ,[id_empresa]
           ,[id_centro_responsabilidad]
           ,[codigo_unidad])
     SELECT DISTINCT 
	 po.Unidad
	 ,'Activa' --estado de la unidad
	 ,2 --id de empresa, resiter
	 ,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
	 ,po.[Centro de Responsabilidad]
	 FROM Puntos_Osorno po
WHERE po.Unidad NOT IN (SELECT u.unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio u)


-- carga de clientes
INSERT INTO RS_SGS_ARAUCO.dbo.clientes
           ([rut_cliente]
           ,[razon_social]
           ,[direccion_cliente]
           ,[id_pais]
           ,[fono_comercial]
           ,[giro_comercial]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[id_unidad]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_rubro]
           ,[nombre_fantasia]
           ,[cobro_especial]
           ,[id_centro_costo])
SELECT po.RUT
,po.[Raz�n Social]
,CONCAT(po.[Avenida, Calle o Pasaje], ' #', po.[N�mero], ' ', [Comuna])
,@Id_pais
,null
,null
,null
,null
,null
,null
,null
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,null -- nombre fantasia no se indica
,'False'
,(SELECT cc.id FROM RS_SGS_ARAUCO.dbo.centro_costo cc WHERE cc.nombre = @NombreCentroCosto)
FROM Puntos_Osorno po
WHERE po.RUT NOT IN (SELECT rut_cliente FROM RS_SGS_ARAUCO.dbo.clientes)

--carga de sucursales
INSERT INTO RS_SGS_ARAUCO.dbo.[sucursales]
           ([sucursal]
           ,[descripcion]
           ,[direccion]
           ,[contacto_sucursal]
           ,[email_sucursal]
           ,[fono_sucursal]
           ,[celular_sucursal]
           ,[zona]
           ,[periodo_desde]
           ,[periodo_hasta]
           ,[estado_sucursal]
           ,[id_rubro]
           ,[id_cliente]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[rut_sucursal]
           ,[id_centro]
           ,[franja_horaria])

SELECT DISTINCT po.[Sucursal Oficial] --se utiliz� el est� nombre para la sucursal por la referencia en la base de datos actual de osorno
, null
,CONCAT(po.[Avenida, Calle o Pasaje Sucursal], ' ', po.[N�mero Sucursal])
, null
, null
, null
, null
, 'Sur'
, 1--default como el resto de periodos
, 1--default como el resto de periodos
, 'Activa'
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
,@Id_pais
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,null
,null
,null
FROM Puntos_Osorno po
WHERE po.[Sucursal Oficial] NOT IN (SELECT sucursal FROM RS_SGS_ARAUCO.dbo.sucursales)



--ingreso de vertedero "SIN VERTEDERO"
INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
           ([vertedero]
           ,[direccion_vertedero]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_unidad]
           ,[disposicion_final])
SELECT DISTINCT
'SIN VERTEDERO'
,'s/n'
,@Id_pais
,@Id_region
,@Id_ciudad
,@Id_ciudad
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE 'Osorno')
,'True' --se inserta "true"como en la mayor�a de los registros de la bd
FROM Puntos_Osorno po
WHERE 'SIN VERTEDERO' NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)


--Ingreso nuevos vertederos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
           ([vertedero]
           ,[direccion_vertedero]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_unidad]
           ,[disposicion_final])
SELECT DISTINCT
po.Vertedero
,'s/n'
,@Id_pais
,@Id_region
,@Id_ciudad
,@Id_ciudad
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
,'True' --se inserta "true"como en la mayor�a de los registros de la bd
FROM Puntos_Osorno po
WHERE po.Vertedero NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)


--Inserci�n de contratos
INSERT INTO RS_SGS_ARAUCO.dbo.[contratos]
           ([id_tipo_contrato]
           ,[id_cliente]
           ,[id_sucursal]
           ,[fecha_inicio]
           ,[fecha_renovacion]
           ,[estado]
           ,[id_modelo_cobro]
           ,[id_centro_responsabilidad]
           ,[id_unidad])
SELECT DISTINCT (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
,(SELECT suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calle o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
,GETDATE() -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
,DATEADD(YEAR, 1, GETDATE()) -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
, 'Activo'
,null--(SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_Osorno po
WHERE 1 > (
	SELECT COUNT(*) FROM RS_SGS_ARAUCO.dbo.[contratos] con
	WHERE con.id_unidad = (SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad) -- Revisar
AND con.id_centro_responsabilidad = (SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
AND con.id_cliente = (SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
AND con.id_tipo_contrato = (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calle o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)

-- SELECT * FROM RS_SGS_ARAUCO.dbo.[contratos]

--carga tipo de tarifa
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_tarifa]
           ([tipo_tarifa]
           ,[retencion]
           ,[inicio_tarifa]
           ,[termino_tarifa]
           ,[valor_tarifa]
           ,[tarifa_escalable]
		   ,[id_unidad])
SELECT
CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
										END
))
,0 -- no existe campo que lo indique
,1 -- se deja segun los registros que exist�an en osorno
,12 -- se deja segun los registros que exist�an en osorno
,(CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
											WHEN po.[Valor Fijo Mensual] = 0 AND po.[Valor Vertedero] = 0 AND po.[Valor Retiro o Volteo] = 0 OR po.[Valor Retiro o Volteo] is null
											THEN 0
										END
)
,'False'
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_Osorno po
WHERE CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
										END
)) NOT IN (SELECT tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa)

--Carga equipos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[equipos]
           ([tipo_equipo]
           ,[capacidad_equipo]
           ,[nom_abreviado]
           ,[nom_nemo])
SELECT
 [Tipo Equipo]
,po.Capacidad
,[Tipo Equipo]
,null
FROM Puntos_Osorno po
WHERE [Tipo Equipo] <> NULL AND [Tipo Equipo] NOT IN (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos)

--*************************************************************
select * from RS_SGS_ARAUCO.dbo.[contratos]
--*************************************************************

--Carga de puntos de servicio
INSERT INTO RS_SGS_ARAUCO.dbo.[puntos_servicio]
           ([nombre_punto]
           ,[estado_punto]
           ,[id_contrato]
           ,[id_vertedero]
           ,[id_residuo]
           ,[id_tipo_tarifa]
           ,[lunes]
           ,[martes]
           ,[miercoles]
           ,[jueves]
           ,[viernes]
           ,[sabado]
           ,[domingo]
           ,[id_equipo]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[cant_equipos]
           ,[pago_por_peso]
           ,[codigo_externo_asignado])
SELECT
po.Sucursal2
,@Id_estado
,(SELECT con.id_contrato FROM RS_SGS_ARAUCO.dbo.[contratos] con WHERE
con.id_unidad = (SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
AND con.id_centro_responsabilidad = (SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.Sucursal2)
AND con.id_cliente = (SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
AND con.id_tipo_contrato = (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE po.[Sucursal Oficial] AND suc.direccion LIKE CONCAT(po.[Avenida, Calle o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)
,CASE
WHEN po.[Sucursal Oficial] LIKE '%Traslado%' OR po.[Sucursal Oficial] LIKE '%Instalaci�n%'
THEN (SELECT ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE '%SIN VERTEDERO%')
ELSE (SELECT ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE po.Vertedero)
END
,@Residuos
,(SELECT tt.id_tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa tt WHERE tt.tipo_tarifa = (CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor Vertedero]<>0
											THEN po.[Valor Vertedero]
										END
))))
,IIF(po.L is null, 'False', 'True')
,IIF(po.M is null, 'False', 'True')
,IIF(po.MI is null, 'False', 'True')
,IIF(po.J is null, 'False', 'True')
,IIF(po.V is null, 'False', 'True')
,IIF(po.S is null, 'False', 'True')
,IIF(po.D is null, 'False', 'True')
,(CASE 
			WHEN po.[Nombre Equipo] LIKE '%Estanque Aljibes%'
			THEN (SELECT eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT('ALJIBE',' ',po.Capacidad)))
			ELSE (SELECT eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT(po.[Tipo Equipo],' ',po.Capacidad)))

		END
		)
,po.[Contacto Comercial]
,po.[Correo Comercial]
,po.[Fono Comercial]
,null
,po.[N� Equipos]
,(CASE
		WHEN po.Observaci�n LIKE '%kilo transportado%' --cuando la observaci�n dice "kilo" transportado se asume que el cobro es por peso
		THEN 'True'
		ELSE 'False'
	END
)
,'True' -- Se deja como true en base a los campos que ya se encuentran en la base de datos
FROM Puntos_Osorno po
WHERE po.Sucursal2 NOT IN (SELECT ps.nombre_punto FROM RS_SGS_ARAUCO.dbo.puntos_servicio ps WHERE ps.nombre_punto LIKE po.Sucursal2)

-- Modelo de Cobro
INSERT INTO [RS_SGS_ARAUCO].[dbo].[modelo_cobro]
           ([modelo]
           ,[id_tipo_modelo]
           ,[id_punto_servicio])
SELECT
po.Sucursal2
,1--Industrial, es �nico, porque el otro es minera
,(SELECT ps.id_punto_servicio FROM [RS_SGS_ARAUCO].[dbo].puntos_servicio ps WHERE ps.nombre_punto = po.Sucursal2)
FROM Puntos_Osorno po
WHERE po.Sucursal2 NOT IN (SELECT modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro)

------


--carga camiones_marcas
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones_marcas]
           ([marca])
SELECT
cam.[Marca ]
FROM Camiones_Osorno cam
WHERE cam.[Marca ] NOT IN (SELECT marca FROM RS_SGS_ARAUCO.dbo.camiones_marcas)

--carga franquiciados
INSERT INTO [RS_SGS_ARAUCO].[dbo].[franquiciado]
           ([id_comuna]
           ,[rut_franquiciado]
           ,[razon_social]
           ,[fecha_contrato]
           ,[estado]
           ,[observacion]
           ,[direccion]
           ,[anticipo]
           ,[porcentaje_anticipo]
           ,[id_unidad])
SELECT
null
,cam.CI
,cam.Franquiciado
,null -- no se indica la fecha
,cam.Estado
,null -- no se indican observaciones
,null -- no se indica direccion
,'False' -- no se indica anticipo
,null -- no se indica porcentaje
,(SELECT un.id_unidad FROM [RS_SGS_ARAUCO].[dbo].unidades_negocio un WHERE un.unidad LIKE 'Osorno' )
FROM Camiones_Osorno cam
WHERE cam.CI NOT IN (SELECT rut_franquiciado FROM RS_SGS_ARAUCO.dbo.franquiciado)
--carga camiones
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones]
           ([camion]
           ,[patente]
           ,[tipo_camion]
           ,[modelo_camion]
           ,[capacidad_camion]
           ,[estado]
           ,[observacion]
           ,[id_camion_marca]
           ,[id_tipo_servicio]
           ,[prop_resiter]
           ,[prop_franquiciado]
           ,[prop_otro]
           ,[fecha_rvt]
           ,[id_franquiciado]
           ,[num_interno])
SELECT
cam.[Tipo Servicio]
,cam.[Placa Patente]
,cam.[Tipo Servicio]
,cam.Modelo
,null -- no se informa
,Estado
,null --no se informa
,(SELECT cm.id_camion_marca FROM [RS_SGS_ARAUCO].[dbo].[camiones_marcas] cm WHERE cam.[Marca ] = cm.marca)
,(SELECT ts.id_tipo_servicio FROM [RS_SGS_ARAUCO].[dbo].tipo_servicio ts WHERE cam.[Tipo Servicio] = ts.tipo_servicio)
,'False' -- se deja como false por aparecer como "tipo chofer franquiciado"
,'True' -- True por aparecer como "tipo chofer franquiciado"
,'False' -- misma raz�n
,null -- no se informa
,(SELECT top 1 fr.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado fr WHERE cam.CI = fr.rut_franquiciado)
,(SELECT (MAX(ca.num_interno) +1) FROM [RS_SGS_ARAUCO].[dbo].camiones ca)
FROM Camiones_Osorno cam
WHERE cam.[Placa Patente] NOT IN (SELECT patente FROM RS_SGS_ARAUCO.dbo.camiones)



--carga conductores
INSERT INTO [RS_SGS_ARAUCO].[dbo].[conductores]
           ([nombre_conductor]
           ,[rut_conductor]
           ,[fecha_ingreso]
           ,[id_tipo_licencia]
           ,[estado]
           ,[observacion]
           ,[id_franquiciado]
           ,[fecha_vencimientolic])
SELECT
cam.[Conductor               Turno 1]
,cam.CI1
,null -- no se informa
,1--(SELECT lic.id_tipo_licencia FROM [RS_SGS_ARAUCO].[dbo].tipo_licencia lic WHERE lic.tipo_licencia LIKE ) se deja como A-1 como la mayr�a de las licencias
,'Activo' -- no se informa, pero se asume que est� activo
,null -- no se informan observaciones
,(SELECT top 1 f.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado f WHERE cam.CI LIKE f.rut_franquiciado)
,null -- no se informa
FROM Camiones_Osorno cam
WHERE cam.CI1 NOT IN (SELECT rut_conductor FROM RS_SGS_ARAUCO.dbo.conductores)


END TRY
BEGIN CATCH
SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO