BEGIN TRANSACTION;


BEGIN TRY -- comienzo del try

-- Script Chiloe
-- Constantes
DECLARE @Id_pais INT
,@Id_region INT
,@Id_ciudad INT
,@Id_comuna INT
,@Id_estado INT
,@Residuos INT
,@NombreCentroCosto VARCHAR(150)


SET @Id_pais = 3 -- chile
SET @Id_region = 10 -- los lagos
SET @Id_ciudad = 35 --pto montt
SET @Id_comuna = 253 --pt montt
SET @Id_estado = 1 --estado activo
SET @Residuos = 32--residuos domesticos
SET @NombreCentroCosto = 'CC Chiloe'

--Reemplazan vertederos en "null" por "sin vertedero"
UPDATE Puntos_PtoMontt SET Vertedero = 'SIN VERTEDERO' WHERE Vertedero is null

--Se modifican los tipos de industria equivalentes a los rubros en la base de datos
PRINT 'UPDATE Tipo Industria'
UPDATE Puntos_PtoMontt SET [Tipo Industria] = 'Alimentaci�n' WHERE [Tipo Industria] = 'Alimenticia'
UPDATE Puntos_PtoMontt SET [Tipo Industria] = 'Industrial' WHERE [Tipo Industria] = 'Industria'
UPDATE Puntos_PtoMontt SET [Tipo Industria] = 'Mataderos' WHERE [Tipo Industria] = 'Matadero'
UPDATE Puntos_PtoMontt SET [Tipo Industria] = 'Salmonicultura' WHERE [Tipo Industria] = 'Salmonera'
UPDATE Puntos_PtoMontt SET [Tipo Industria] = 'No Aplica' WHERE [Tipo Industria] is NULL

--Se insertan los nuevos tipos de rubro
INSERT INTO [RS_SGS_ARAUCO].[dbo].[rubros]
           ([rubro])
SELECT DISTINCT po.[Tipo Industria]
FROM Puntos_PtoMontt po
WHERE po.[Tipo Industria] NOT IN (SELECT ru.rubro FROM RS_SGS_ARAUCO.dbo.rubros ru)

--Se modifican lregiones para coincidir con la base de datos
PRINT 'UPDATE Tipo Industria'
UPDATE Puntos_PtoMontt SET Regi�n = 'Regi�n de Los Lagos' WHERE Regi�n LIKE 'D�cima'
UPDATE Puntos_PtoMontt SET Regi�n = 'Regi�n Metropolitana' WHERE Regi�n LIKE 'Metropolitana'
UPDATE Puntos_PtoMontt SET Regi�n = 'Regi�n de Magallanes y la Ant�rtica Chilena' WHERE Regi�n LIKE 'Duod�cima'
UPDATE Puntos_PtoMontt SET Regi�n = 'Regi�n de la Araucan�a' WHERE Regi�n LIKE 'Novena'
UPDATE Puntos_PtoMontt SET Regi�n = 'Regi�n del Libertador General Bernardo O Higgins' WHERE Regi�n LIKE 'VI Regi�n'

PRINT 'Se quitan posibles espacion en blanco en rut'
--Se quitan posibles espacios en en campo rut en puntos_ptomontt
UPDATE Puntos_PtoMontt SET Rut = LTRIM(RTRIM(Rut))

PRINT 'Llenado de campo sistema en caso de ser null'
--Se llena el campo "sistema" en el caso de ser null
UPDATE Puntos_PtoMontt SET Sistema = 'No Aplica' FROM Puntos_PtoMontt WHERE Sistema is null

PRINT 'Inserci�n ruts en puntos con rut = null'
--Se insertan rut por ser requeridos para la inserci�n mas tarde
UPDATE Puntos_PtoMontt SET Rut = '9-1' WHERE [Raz�n Social] LIKE 'Tratamiento del Pac�fico S.A.'
UPDATE Puntos_PtoMontt SET Rut = '9-2' WHERE [Raz�n Social] LIKE '%Vac�o Tramo A%'
UPDATE Puntos_PtoMontt SET Rut = '9-3' WHERE [Raz�n Social] LIKE '%Vac�o Tramo B%'
UPDATE Puntos_PtoMontt SET Rut = '9-4' WHERE [Raz�n Social] LIKE '%Vuelta Interior Vertedero%'
UPDATE Puntos_PtoMontt SET Rut = '9-5' WHERE [Raz�n Social] LIKE '%Alojamiento Franquiciado Ampliroll%'
UPDATE Puntos_PtoMontt SET Rut = '9-6' WHERE [Raz�n Social] LIKE '%Alojamiento Franquiciado Multibucket%'
UPDATE Puntos_PtoMontt SET Rut = '9-7' WHERE [Raz�n Social] LIKE '%Abastecedora del Comercio Ltda.%'
UPDATE Puntos_PtoMontt SET Rut = '9-8' WHERE [Raz�n Social] LIKE '%Flete Puerto Montt - Ecoprial - Puerto Montt%'
UPDATE Puntos_PtoMontt SET Rut = '9-9' WHERE [Raz�n Social] LIKE '%Flete Puerto Montt - Osorno (Ampliroll)%'
UPDATE Puntos_PtoMontt SET Rut = '8-9' WHERE [Raz�n Social] LIKE '%Flete Puerto Montt- Rilesur - Puerto Montt%'
UPDATE Puntos_PtoMontt SET Rut = '8-8' WHERE [Raz�n Social] LIKE '%Flete Vertedero Puerto Montt - Vertedero Chilo� (Ampliroll)%'
UPDATE Puntos_PtoMontt SET Rut = '8-7' WHERE [Raz�n Social] LIKE '%Manuel Araya%'

PRINT 'Inserci�n nuevos tipo_servicio desde los puntos'
--Se insertan direcciones, numeros, regiones y comunas
UPDATE Puntos_PtoMontt
            SET
			[Avenida, Calle o Pasaje] = 'n/a'
			,[N�mero] = 's/n'
			,[Regi�n] = 'Regi�n de Los Lagos'
			,[Comuna] = 'Puerto Montt'
		WHERE	[Avenida, Calle o Pasaje] is null
				AND [N�mero] is null
				AND [Regi�n] is null
				AND [Comuna] is null
		

PRINT 'Cambios en comunas'
--cambio en comunas
UPDATE Puntos_PtoMontt
            SET Comuna = 'Santiago'
			WHERE Comuna LIKE '%Santiago Centro%'			
--Se agregan nuevas comunas y ciudad "carelmapu"
INSERT INTO [RS_SGS_ARAUCO].[dbo].ciudades
           (ciudad
           ,id_region)
		   SELECT TOP 1
		   'Carelmapu'
		   ,@Id_region
FROM Puntos_PtoMontt WHERE 'Carelmapu' NOT IN (SELECT c.ciudad FROM [RS_SGS_ARAUCO].[dbo].ciudades c)		   


INSERT INTO [RS_SGS_ARAUCO].[dbo].[comunas]
           ([nombre_comuna]
           ,[id_ciudad])
		   SELECT TOP 1 
		   'Maulin'
		   ,(SELECT c.id_ciudad FROM [RS_SGS_ARAUCO].[dbo].ciudades c WHERE c.ciudad LIKE 'Carelmapu')
FROM Puntos_PtoMontt WHERE 'Maulin' NOT IN (SELECT c.nombre_comuna FROM [RS_SGS_ARAUCO].[dbo].[comunas] c)		

------------
INSERT INTO [RS_SGS_ARAUCO].[dbo].[comunas]
           ([nombre_comuna]
           ,[id_ciudad])
SELECT DISTINCT po.Comuna 
,35
FROM Puntos_PtoMontt po
WHERE po.Comuna NOT IN (SELECT com.nombre_comuna FROM RS_SGS_ARAUCO.dbo.comunas com)


--Se modifican los tipos de equipo para coincidir con los campos en la base de datos arauco
--PRINT 'UPDATE Tipo Equipo'
--UPDATE Puntos_PtoMontt SET [Tipo Equipo]  = (CASE 
--			WHEN [Nombre Equipo] LIKE 'Estanque Aljibes'
--			THEN CONCAT('ALJIBE',' ',Capacidad)
--			ELSE CONCAT([Tipo Equipo],' ',Capacidad)

--		END
--		)

PRINT 'Inserci�n nuevos tipo_servicio'
--Inserci�n nuevos tipo de servicio
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_servicio]
           ([tipo_servicio])
SELECT DISTINCT
po.Sistema
FROM Puntos_PtoMontt po
WHERE po.Sistema NOT IN (SELECT ts.tipo_servicio FROM RS_SGS_ARAUCO.dbo.tipo_servicio ts)

PRINT 'cambio estado camiones para coincidir'
--Se modifica el estado de los camiones para su equivalencia en la bd de destino
UPDATE Camiones_PtoMontt SET Estado = 'Activo' WHERE Estado = 'Operativo'


PRINT 'Inserci�n de nuevos rubros en caso de existir'
--Inserci�n nuevos rubros
INSERT INTO RS_SGS_ARAUCO.dbo.rubros
([rubro])
select DISTINCT [Tipo Industria] FROM Puntos_PtoMontt po
WHERE po.[Tipo Industria] NOT IN (SELECT ru.rubro FROM RS_SGS_ARAUCO.dbo.rubros ru)

PRINT 'Inserci�n nuevos tipo_contrato'
--inserci�n nuevos tipos de contrato
INSERT INTO RS_SGS_ARAUCO.dbo.tipo_contrato
(tipo_contrato)
select DISTINCT [Sistema] FROM Puntos_PtoMontt po
WHERE po.[Sistema] NOT IN (SELECT tc.tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc)


PRINT 'Inserci�n centro_responsabilidad correspondiente'
--Inserci�n de centro de responsabilidad
INSERT INTO RS_SGS_ARAUCO.dbo.centro_responsabilidad
(centro_responsabilidad)
select DISTINCT [Centro de Responsabilidad] FROM Puntos_PtoMontt po
WHERE po.[Centro de Responsabilidad] NOT IN (SELECT cr.centro_responsabilidad FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr)

PRINT 'Centro costo'
--Insert de centro_costo
INSERT INTO [RS_SGS_ARAUCO].[dbo].[centro_costo]
           ([nombre])
SELECT @NombreCentroCosto WHERE @NombreCentroCosto NOT IN (SELECT vcc.nombre FROM [RS_SGS_ARAUCO].[dbo].[centro_costo] vcc)

PRINT 'Carga unidades de negocio'
--Inserci�n nueva unidad de negocio
INSERT INTO [RS_SGS_ARAUCO].[dbo].[unidades_negocio]
           ([unidad]
           ,[estado]
           ,[id_empresa]
           ,[id_centro_responsabilidad]
           ,[codigo_unidad])
     SELECT DISTINCT 
	 po.Unidad
	 ,'Activa' --estado de la unidad
	 ,2 --id de empresa, resiter
	 ,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
	 ,po.[Centro de Responsabilidad]
	 FROM Puntos_PtoMontt po
WHERE po.Unidad NOT IN (SELECT u.unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio u)

PRINT 'Carga clientes'
-- carga de clientes
INSERT INTO RS_SGS_ARAUCO.dbo.clientes
           ([rut_cliente]
           ,[razon_social]
           ,[direccion_cliente]
           ,[id_pais]
           ,[fono_comercial]
           ,[giro_comercial]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[id_unidad]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_rubro]
           ,[nombre_fantasia]
           ,[cobro_especial]
           ,[id_centro_costo])
SELECT po.RUT
,po.[Raz�n Social]
,CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero])
,@Id_pais
,null
,null
,null
,null
,null
,null
,null
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,null -- nombre fantasia no se indica
,'False'
,(SELECT cc.id FROM RS_SGS_ARAUCO.dbo.centro_costo cc WHERE cc.nombre = @NombreCentroCosto)
FROM Puntos_PtoMontt po
WHERE po.RUT NOT IN (SELECT rut_cliente FROM RS_SGS_ARAUCO.dbo.clientes)

--***********************************

--select * from RS_SGS_ARAUCO.dbo.clientes

PRINT 'Carga sucursales'
--carga de sucursales
INSERT INTO RS_SGS_ARAUCO.dbo.[sucursales]
           ([sucursal]
           ,[descripcion]
           ,[direccion]
           ,[contacto_sucursal]
           ,[email_sucursal]
           ,[fono_sucursal]
           ,[celular_sucursal]
           ,[zona]
           ,[periodo_desde]
           ,[periodo_hasta]
           ,[estado_sucursal]
           ,[id_rubro]
           ,[id_cliente]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[rut_sucursal]
           ,[id_centro]
           ,[franja_horaria])

SELECT DISTINCT CONCAT(po.[Sucursal Oficial], ' ',po.[Tipo Industria]) --se utiliz� el est� nombre para la sucursal por la referencia en la base de datos actual de osorno
, null
,CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal])
, null
, null
, null
, null
, 'Sur'
, 1--default como el resto de periodos
, 1--default como el resto de periodos
, 'Activa'
,(SELECT ru.id_rubro FROM RS_SGS_ARAUCO.dbo.rubros ru WHERE ru.rubro LIKE po.[Tipo Industria])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
,@Id_pais
,(SELECT r.id_region FROM RS_SGS_ARAUCO.dbo.regiones r WHERE r.nombre_region LIKE po.[Regi�n])
,(SELECT co.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,(SELECT co.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas co WHERE co.nombre_comuna LIKE po.Comuna)
,null
,null
,null
FROM Puntos_PtoMontt po
WHERE po.[Sucursal Oficial] NOT IN (SELECT sucursal FROM RS_SGS_ARAUCO.dbo.sucursales)



----ingreso de vertedero "SIN VERTEDERO"
--INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
--           ([vertedero]
--           ,[direccion_vertedero]
--           ,[id_pais]
--           ,[id_region]
--           ,[id_ciudad]
--           ,[id_comuna]
--           ,[id_unidad]
--           ,[disposicion_final])
--SELECT DISTINCT
--'SIN VERTEDERO'
--,'s/n'
--,@Id_pais
--,@Id_region
--,@Id_ciudad
--,@Id_ciudad
--,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE 'Osorno')
--,'True' --se inserta "true"como en la mayor�a de los registros de la bd
--FROM Puntos_PtoMontt po
--WHERE 'SIN VERTEDERO' NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)

PRINT 'Carga vertederos'
--Ingreso nuevos vertederos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[vertederos]
           ([vertedero]
           ,[direccion_vertedero]
           ,[id_pais]
           ,[id_region]
           ,[id_ciudad]
           ,[id_comuna]
           ,[id_unidad]
           ,[disposicion_final])
SELECT DISTINCT
po.Vertedero
,'s/n'
,@Id_pais
,@Id_region
,@Id_ciudad
--,CASE   
--	WHEN po.Comuna is null 
--	THEN 35 
--	ELSE (SELECT com.id_ciudad FROM RS_SGS_ARAUCO.dbo.comunas com WHERE com.nombre_comuna LIKE po.Comuna)
--	END
--,CASE 
--	WHEN po.Comuna is null 
--	THEN 232 
--	ELSE (SELECT com.id_comuna FROM RS_SGS_ARAUCO.dbo.comunas com WHERE com.nombre_comuna LIKE po.Comuna)
--	END
,@Id_comuna
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
,'True' --se inserta "true"como en la mayor�a de los registros de la bd
FROM Puntos_PtoMontt po
WHERE po.Vertedero NOT IN (SELECT ver.vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver)


--SELECT * FROM RS_SGS_ARAUCO.dbo.sucursales
PRINT 'Carga de nuevos contratos'
--Inserci�n de contratos
INSERT INTO RS_SGS_ARAUCO.dbo.[contratos]
           ([id_tipo_contrato]
           ,[id_cliente]
           ,[id_sucursal]
           ,[fecha_inicio]
           ,[fecha_renovacion]
           ,[estado]
           ,[id_modelo_cobro]
           ,[id_centro_responsabilidad]
           ,[id_unidad])
SELECT DISTINCT (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
,(SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
,(SELECT top 1 suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE CONCAT(po.[Sucursal Oficial], ' ',po.[Tipo Industria]) AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
,GETDATE() -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
,DATEADD(YEAR, 1, GETDATE()) -- se selecciona la fecha actual como en los registros (son la misma fecha en inicio y en renovaci�n, sin relevancia)
, 'Activo'
,null--(SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.[Nombre del Cliente - Servicio])
,(SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_PtoMontt po
WHERE 1 > (
	SELECT COUNT(*) FROM RS_SGS_ARAUCO.dbo.[contratos] con
	WHERE con.id_unidad = (SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad) -- Revisar
AND con.id_centro_responsabilidad = (SELECT cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.[Nombre del Cliente - Servicio])
AND con.id_cliente = (SELECT cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
AND con.id_tipo_contrato = (SELECT tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT top 1  suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE CONCAT(po.[Sucursal Oficial], ' ',po.[Tipo Industria]) AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)

-- SELECT * FROM RS_SGS_ARAUCO.dbo.[contratos]
PRINT 'Carga de tipos de tarifa relacionados'
--carga tipo de tarifa
INSERT INTO [RS_SGS_ARAUCO].[dbo].[tipo_tarifa]
           ([tipo_tarifa]
           ,[retencion]
           ,[inicio_tarifa]
           ,[termino_tarifa]
           ,[valor_tarifa]
           ,[tarifa_escalable]
		   ,[id_unidad])
SELECT
CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor m3 / Ton]<>0
											THEN po.[Valor m3 / Ton]
										END
))
,0 -- no existe campo que lo indique
,1 -- se deja segun los registros que exist�an en osorno
,12 -- se deja segun los registros que exist�an en osorno
,(CASE 
											WHEN po.[Valor Fijo Mensual]<>0 OR po.[Valor Fijo Mensual] <> null
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0 OR po.[Valor Retiro o Volteo] <> null
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor m3 / Ton]<>0 OR po.[Valor m3 / Ton] <> null
											THEN po.[Valor m3 / Ton]
											WHEN po.[Valor Fijo Mensual] = 0 AND po.[Valor m3 / Ton] = 0 AND po.[Valor Retiro o Volteo] = 0 OR po.[Valor Retiro o Volteo] is null
											THEN 0
											ELSE 0
										END
)
,'False'
,(SELECT un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
FROM Puntos_PtoMontt po
WHERE CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor m3 / Ton]<>0
											THEN po.[Valor m3 / Ton]
											WHEN po.[Valor Fijo Mensual] = 0 AND po.[Valor m3 / Ton] = 0 AND po.[Valor Retiro o Volteo] = 0 OR po.[Valor Retiro o Volteo] is null
											THEN 0
											ELSE 0
										END
)) NOT IN (SELECT tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa)

PRINT 'Inserci�n de nuevos equipos'
--Carga equipos nuevos
INSERT INTO [RS_SGS_ARAUCO].[dbo].[equipos]
           ([tipo_equipo]
           ,[capacidad_equipo]
           ,[nom_abreviado]
           ,[nom_nemo])
SELECT DISTINCT
 [Tipo Equipo]
,IIF(po.Capacidad is null, 0, CAST((RTRIM(SUBSTRING(po.Capacidad, 1, 2))) AS FLOAT))
,RTRIM(SUBSTRING([Tipo Equipo],1, 5))
,null
FROM Puntos_PtoMontt po
WHERE [Tipo Equipo] <> NULL AND po.[Tipo Equipo] <> (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos) AND CAST((RTRIM(SUBSTRING(po.Capacidad, 1, 2))) AS FLOAT) NOT IN (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos) OR [Tipo Equipo] <> 'Disposici�n'
--WHERE [Tipo Equipo] <> NULL AND CONCAT(po.[Tipo Equipo],' ',po.Capacidad) NOT IN (SELECT tipo_equipo FROM RS_SGS_ARAUCO.dbo.equipos) OR [Tipo Equipo] <> 'Disposici�n'

--*************************************************************
--SELECT ps.nombre_punto FROM RS_SGS_ARAUCO.dbo.puntos_servicio ps

--*************************************************************
PRINT 'Carga de puntos_servicio'
--Carga de puntos de servicio
INSERT INTO RS_SGS_ARAUCO.dbo.[puntos_servicio]
--INSERT INTO puntos_servicio
           ([nombre_punto]
           ,[estado_punto]
           ,[id_contrato]
           ,[id_vertedero]
           ,[id_residuo]
           ,[id_tipo_tarifa]
           ,[lunes]
           ,[martes]
           ,[miercoles]
           ,[jueves]
           ,[viernes]
           ,[sabado]
           ,[domingo]
           ,[id_equipo]
           ,[nombre_contacto]
           ,[email_contacto]
           ,[fono_contacto]
           ,[celular_contacto]
           ,[cant_equipos]
           ,[pago_por_peso]
           ,[codigo_externo_asignado])
SELECT DISTINCT
po.[Nombre del Cliente - Servicio]
,@Id_estado
,(SELECT top 1 con.id_contrato FROM RS_SGS_ARAUCO.dbo.[contratos] con WHERE
con.id_unidad = (SELECT top 1 un.id_unidad FROM RS_SGS_ARAUCO.dbo.unidades_negocio un WHERE un.unidad LIKE po.Unidad)
AND con.id_centro_responsabilidad = (SELECT top 1 cr.id_centro FROM RS_SGS_ARAUCO.dbo.centro_responsabilidad cr WHERE cr.centro_responsabilidad LIKE po.[Centro de Responsabilidad])
--AND con.id_modelo_cobro = (SELECT mc.id_modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro mc WHERE mc.modelo LIKE po.[Nombre del Cliente - Servicio])
AND con.id_cliente = (SELECT top 1 cli.id_cliente FROM RS_SGS_ARAUCO.dbo.clientes cli WHERE cli.rut_cliente LIKE po.Rut)
AND con.id_tipo_contrato = (SELECT top 1 tc.id_tipo_contrato FROM RS_SGS_ARAUCO.dbo.tipo_contrato tc WHERE tc.tipo_contrato LIKE po.[Sistema])
AND con.id_sucursal = (SELECT top 1 suc.id_sucursal FROM RS_SGS_ARAUCO.dbo.sucursales suc WHERE suc.sucursal LIKE CONCAT(po.[Sucursal Oficial], ' ',po.[Tipo Industria]) AND suc.direccion LIKE CONCAT(po.[Avenida, Calla o Pasaje Sucursal], ' ', po.[N�mero Sucursal]))
)
,CASE
WHEN po.[Sucursal Oficial] LIKE '%Traslado%' OR po.[Sucursal Oficial] LIKE '%Instalaci�n%'
THEN (SELECT ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE '%SIN VERTEDERO%')
ELSE (SELECT top 1 ver.id_vertedero FROM RS_SGS_ARAUCO.dbo.vertederos ver WHERE ver.vertedero LIKE po.Vertedero)
END
,@Residuos
,(SELECT top 1 tt.id_tipo_tarifa FROM RS_SGS_ARAUCO.dbo.tipo_tarifa tt WHERE tt.tipo_tarifa = (CONCAT(po.[Sucursal para concatenar], (CASE 
											WHEN po.[Valor Fijo Mensual]<>0
											THEN po.[Valor Fijo Mensual]
											WHEN po.[Valor Retiro o Volteo]<>0
											THEN po.[Valor Retiro o Volteo]
											WHEN po.[Valor m3 / Ton]<>0
											THEN po.[Valor m3 / Ton]
											--WHEN po.[Valor Fijo Mensual] = 0 AND po.[Valor m3 / Ton] = 0 AND po.[Valor Retiro o Volteo] = 0 OR po.[Valor Retiro o Volteo] is null
											--THEN 0
											--ELSE 0
										END
))))
,IIF(po.L is null, 'False', 'True')
,IIF(po.M is null, 'False', 'True')
,IIF(po.MI is null, 'False', 'True')
,IIF(po.J is null, 'False', 'True')
,IIF(po.V is null, 'False', 'True')
,IIF(po.S is null, 'False', 'True')
,IIF(po.D is null, 'False', 'True')
,(CASE 
			WHEN po.[Nombre Equipo] LIKE '%Estanque Aljibes%'
			THEN (SELECT eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT('ALJIBE',' ',po.Capacidad)))
			ELSE (SELECT top 1 eq.id_equipo FROM RS_SGS_ARAUCO.dbo.equipos eq WHERE eq.tipo_equipo LIKE (CONCAT(po.[Tipo Equipo],' ',po.Capacidad)))

		END
		)
,po.[Contacto Comercial]
,po.[Correo Comercial]
,po.[Fono Comercial]
,null
,po.[N� Equipos]
,(CASE
		WHEN po.[Valor m3 / Ton] <> null OR po.[Valor m3 / Ton] <>0
		THEN 'True'
		ELSE 'False'
	END
)
,'True' -- Se deja como true en relacion a los campos que ya se encuentran en la base de datos
FROM Puntos_PtoMontt po
WHERE po.[Nombre del Cliente - Servicio] NOT IN (SELECT top 1 ps.nombre_punto FROM RS_SGS_ARAUCO.dbo.puntos_servicio ps WHERE ps.nombre_punto LIKE po.[Nombre del Cliente - Servicio])

PRINT 'Modelos de cobro'
-- Modelo de Cobro
INSERT INTO [RS_SGS_ARAUCO].[dbo].[modelo_cobro]
           ([modelo]
           ,[id_tipo_modelo]
           ,[id_punto_servicio])
SELECT
po.[Nombre del Cliente - Servicio]
,1--Industrial, es �nico, porque el otro es minera
,(SELECT ps.id_punto_servicio FROM [RS_SGS_ARAUCO].[dbo].puntos_servicio ps WHERE ps.nombre_punto = po.[Nombre del Cliente - Servicio])
FROM Puntos_PtoMontt po
WHERE po.[Nombre del Cliente - Servicio] NOT IN (SELECT modelo FROM RS_SGS_ARAUCO.dbo.modelo_cobro)

------

PRINT 'Inserci�n camiones_marcas nuevas'
--carga camiones_marcas
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones_marcas]
           ([marca])
SELECT
cam.[Marca ]
FROM Camiones_PtoMontt cam
WHERE cam.[Marca ] NOT IN (SELECT marca FROM RS_SGS_ARAUCO.dbo.camiones_marcas)

PRINT 'Nuevos franquiciados'
--carga franquiciados
INSERT INTO [RS_SGS_ARAUCO].[dbo].[franquiciado]
           ([id_comuna]
           ,[rut_franquiciado]
           ,[razon_social]
           ,[fecha_contrato]
           ,[estado]
           ,[observacion]
           ,[direccion]
           ,[anticipo]
           ,[porcentaje_anticipo]
           ,[id_unidad])
SELECT
null
,cam.CI
,cam.Franquiciado
,null -- no se indica la fecha
,cam.Estado
,null -- no se indican observaciones
,null -- no se indica direccion
,'False' -- no se indica anticipo
,null -- no se indica porcentaje
,(SELECT un.id_unidad FROM [RS_SGS_ARAUCO].[dbo].unidades_negocio un WHERE un.unidad LIKE 'Osorno' )
FROM Camiones_PtoMontt cam
WHERE cam.CI NOT IN (SELECT rut_franquiciado FROM RS_SGS_ARAUCO.dbo.franquiciado)

PRINT 'Carga camiones'
--carga camiones
INSERT INTO [RS_SGS_ARAUCO].[dbo].[camiones]
           ([camion]
           ,[patente]
           ,[tipo_camion]
           ,[modelo_camion]
           ,[capacidad_camion]
           ,[estado]
           ,[observacion]
           ,[id_camion_marca]
           ,[id_tipo_servicio]
           ,[prop_resiter]
           ,[prop_franquiciado]
           ,[prop_otro]
           ,[fecha_rvt]
           ,[id_franquiciado]
           ,[num_interno])
SELECT
cam.[Tipo Servicio]
,cam.[Placa Patente]
,cam.[Tipo Servicio]
,cam.Modelo
,null -- no se informa
,Estado
,null --no se informa
,CASE	WHEN cam.[Marca ] is null
		THEN (SELECT top 1 cm.id_camion_marca FROM [RS_SGS_ARAUCO].[dbo].[camiones_marcas] cm WHERE cm.marca LIKE '%Ford%')
		WHEN cam.[Marca ] LIKE '%Ford Cargo%'
		THEN (SELECT top 1 cm.id_camion_marca FROM [RS_SGS_ARAUCO].[dbo].[camiones_marcas] cm WHERE cam.[Marca ] LIKE '%Ford%')
		ELSE (SELECT cm.id_camion_marca FROM [RS_SGS_ARAUCO].[dbo].[camiones_marcas] cm WHERE cm.marca = cam.[Marca ])
		END
,(SELECT top 1 ts.id_tipo_servicio FROM [RS_SGS_ARAUCO].[dbo].tipo_servicio ts WHERE cam.[Tipo Servicio] = ts.tipo_servicio)
,'False' -- se deja como false por aparecer como "tipo chofer franquiciado"
,'True' -- True por aparecer como "tipo chofer franquiciado"
,'False' -- misma raz�n
,null -- no se informa
,(SELECT top 1 fr.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado fr WHERE cam.CI = fr.rut_franquiciado)
,(SELECT (MAX(ca.num_interno) +1) FROM [RS_SGS_ARAUCO].[dbo].camiones ca)
FROM Camiones_PtoMontt cam
WHERE cam.[Placa Patente] NOT IN (SELECT patente FROM RS_SGS_ARAUCO.dbo.camiones)


PRINT 'Carga conductores'
--carga conductores
INSERT INTO [RS_SGS_ARAUCO].[dbo].[conductores]
           ([nombre_conductor]
           ,[rut_conductor]
           ,[fecha_ingreso]
           ,[id_tipo_licencia]
           ,[estado]
           ,[observacion]
           ,[id_franquiciado]
           ,[fecha_vencimientolic])
SELECT
cam.[Conductor               Turno 1]
,cam.CI1
,null -- no se informa
,1--(SELECT lic.id_tipo_licencia FROM [RS_SGS_ARAUCO].[dbo].tipo_licencia lic WHERE lic.tipo_licencia LIKE ) se deja como A-1 como la mayr�a de las licencias
,'Activo' -- no se informa, pero se asume que est� activo
,null -- no se informan observaciones
,(SELECT top 1 f.id_franquiciado FROM [RS_SGS_ARAUCO].[dbo].franquiciado f WHERE cam.CI LIKE f.rut_franquiciado)
,null -- no se informa
FROM Camiones_PtoMontt cam
WHERE cam.CI1 NOT IN (SELECT rut_conductor FROM RS_SGS_ARAUCO.dbo.conductores)


END TRY
BEGIN CATCH
SELECT 
        ERROR_NUMBER() AS ErrorNumber
        ,ERROR_SEVERITY() AS ErrorSeverity
        ,ERROR_STATE() AS ErrorState
        ,ERROR_PROCEDURE() AS ErrorProcedure
        ,ERROR_LINE() AS ErrorLine
        ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        ROLLBACK TRANSACTION;
END CATCH;

IF @@TRANCOUNT > 0
    COMMIT TRANSACTION;
GO