﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using SGS.Models.Enums;
using SGS.Models.modelViews;
using SGS.Reportes.Models;

namespace SGS.Reportes
{
    public static class Reporte
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="formatoExportacion"></param>
        /// <param name="nombreReporte"></param>
        /// <param name="listaParametros"></param>
        /// <returns></returns>
        public static FileContentResult GenerarReporte(FormatoExportacionView formatoExportacion, string nombreReporte, List<Parameter> listaParametros)
        {
            Report modeloReporte = new Report
            {
                nameReport = nombreReporte,
                tipo = formatoExportacion.TipoReporte,
                parameter = listaParametros
            };

            Byte[] reporteBytes = Get(modeloReporte);
            
            return new FileContentResult(reporteBytes, formatoExportacion.TipoAplicacion);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="formatoReporte"></param>
        /// <returns></returns>
        public static FormatoExportacionView GetFormatoExportacion(FormatoExportacion formatoReporte)
        {
            FormatoExportacionView formatoExportacion = new FormatoExportacionView();

            switch (formatoReporte)
            {
                case FormatoExportacion.Pdf:
                    formatoExportacion.Extencion = ".pdf";
                    formatoExportacion.TipoAplicacion = "application/pdf";
                    formatoExportacion.TipoReporte = "PDF";
                    break;
                case FormatoExportacion.Excel:
                    formatoExportacion.Extencion = ".xls";
                    formatoExportacion.TipoAplicacion = "application/vnd.ms-excel";
                    formatoExportacion.TipoReporte = "EXCEL";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            return formatoExportacion;
        }


        /// <summary>
        /// Generación de parápetros para reporte
        /// </summary>
        /// <param name="nombre">Nombre del parámetro a ingresar</param>
        /// <param name="valor">Valor de parámetro según nombre</param>
        /// <returns>Nuevo parametro a adjuntar en listado.</returns>
        public static Parameter CrearNuevoParametro(string nombre, string valor)
        {
            Parameter parametro = new Parameter
            {
                name = nombre,
                value = valor
            };

            return parametro;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <returns></returns>
        public static byte[] Get(Report report)
        {
            ReportExecutionService rs = new ReportExecutionService
            {
                Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings.Get("RS.UserName"),
                    ConfigurationManager.AppSettings.Get("RS.Password"),
                    ConfigurationManager.AppSettings.Get("RS.Domain")),
                Url = ConfigurationManager.AppSettings.Get("RS.URL") + ConfigurationManager.AppSettings.Get("RS.URL.asmx")
            };

            // Render arguments
            string reportPath = ConfigurationManager.AppSettings.Get("RS.Folder") + "/" + report.nameReport;
            string format = report.tipo;
            const string devInfo = @"<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";

            string encoding;
            string mimeType;
            string extension;
            Warning[] warnings;
            string[] streamIDs;

            ExecutionHeader execHeader = new ExecutionHeader();

            rs.ExecutionHeaderValue = execHeader;
            try
            {
                rs.LoadReport(reportPath, null);
            }
            catch (Exception)
            {
                // Gale.Exception.RestException.Guard(() => true, "ERROR_LOAD_REPORT", "El reporte no existe");
            }

            ParameterValue[] reportParameters;
            // Prepare report parameter.
            if (report.parameter != null)
            {
                reportParameters = new ParameterValue[report.parameter.Count];
                int countparam = 0;
                foreach (Parameter item in report.parameter)
                {
                    reportParameters[countparam] = new ParameterValue
                    {
                        Name = item.name,
                        Value = item.value
                    };
                    countparam++;
                }
            }
            else
            {
                reportParameters = new ParameterValue[0];
            }

            try
            {
                rs.SetExecutionParameters(reportParameters, "es-CL");
            }
            catch (Exception)
            {
                // Gale.Exception.RestException.Guard(() => true, "ERROR_LOAD_REPORT", "El reporte no existe");
            }

            return rs.Render(format, devInfo, out extension, out encoding, out mimeType, out warnings, out streamIDs);
        }
    }
}