//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class centro_responsabilidad
    {
        public centro_responsabilidad()
        {
            this.contratos = new HashSet<contratos>();
            this.sucursales = new HashSet<sucursales>();
        }
    
        public int id_centro { get; set; }
        public string centro_responsabilidad1 { get; set; }
    
        public virtual ICollection<contratos> contratos { get; set; }
        public virtual ICollection<sucursales> sucursales { get; set; }
    }
}
