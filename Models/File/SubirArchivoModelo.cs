﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.File
{
    public class SubirArchivoModelo
    {
        public String confirmacion { get; set; }
        public Exception error { get; set; }
        public void SubirArchivo(String ruta, HttpPostedFileBase file)
        {
            try
            {
                file.SaveAs(ruta);
                this.confirmacion = "Fichero Guardado";
            }
            catch (Exception ex)
            {
                this.error = ex;
            }
        }
    }
}