﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class PuntoEstadoRepository
    {

        private sgsbdEntities ctx;


        public PuntoEstadoRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }

        public IEnumerable<object> getPuntoEstadoForSelect2()
        {


            IEnumerable<object> data = ctx.punto_estados.Select(ps => new
            {
                Text = ps.descripcion,
                Value = ps.id_estado_punto
            }).OrderBy(x => x.Text).ToList();



            IEnumerable<object> append = new object[]
            {new{Text = "Seleccione un Valor",Value = "0"}
            };

            IEnumerable<object> final_data = append.Concat(data);

            return final_data;
        }


        public IEnumerable<object> getPuntoEstadoForSelect() {


            IEnumerable<object> data = ctx.punto_estados.Select(ps=>new{
               Text= ps.descripcion, Value=ps.descripcion
            }).OrderBy(x=>x.Text).ToList();



            IEnumerable<object> append = new object[]
            {new{Text = "Seleccione un Valor",Value = "0"}
            };

            IEnumerable<object> final_data = append.Concat(data);

            return final_data;
        }


        public IEnumerable<punto_estados> GetPuntoEstados()
        {
            return ctx.punto_estados.ToList();
        }

        public punto_estados GetPuntoEstadoByID(int ps)
        {
            return ctx.punto_estados.Find(ps);
        }

        public void InsertPuntoEstado(punto_estados ps)
        {
            ctx.punto_estados.Add(ps);
        }

        public void DeletePuntoEstado(int ps)
        {
            punto_estados punto_estado = ctx.punto_estados.Find(ps);
            ctx.punto_estados.Remove(punto_estado);
        }

        public void UpdatePuntoEstado(punto_estados ps)
        {
            ctx.Entry(ps).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}