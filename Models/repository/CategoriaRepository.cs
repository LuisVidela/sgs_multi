﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.repository
{
    public class CategoriaRepository
    {

            private sgsbdEntities ctx;


            public CategoriaRepository(sgsbdEntities ctx)
            {

                this.ctx = ctx;

            }


            public IEnumerable<categoria> GetCategoria()
            {
                return ctx.categoria.ToList();
            }

            public categoria GetCategoriaByID(int c)
            {
                return ctx.categoria.Find(c);
            }

            public void InsertCategoria(categoria c)
            {
                ctx.categoria.Add(c);
            }

        public IEnumerable<categoria> GetCategoria1()
        {
            return ctx.categoria.ToList();
        }

        public void UpdateCategoria(categoria c)
            {
                ctx.Entry(c).State = System.Data.EntityState.Modified;
            }

            public void Save()
            {
                ctx.SaveChanges(); 
            }

            private bool disposed = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        ctx.Dispose();
                    }
                }
                this.disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
    }
    
}
