﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class PerfilesRepository
    {
         private sgsbdEntities ctx;


        public PerfilesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        
        

        public IEnumerable<perfiles> Getperfiles()
        {
            return ctx.perfiles.ToList();
        }

        public perfiles GetPerfilByID(int e)
        {
            return ctx.perfiles.Find(e);
        }

        public void InsertPerfil(perfiles e)
        {
             ctx.perfiles.Add(e);
        }

        public void DeletePerfil(int e)
        {
            perfiles perfil=ctx.perfiles.Find(e);
            ctx.perfiles.Remove(perfil);
        }

        public void UpdatePerfil(perfiles e)
        {
            ctx.Entry(e).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}