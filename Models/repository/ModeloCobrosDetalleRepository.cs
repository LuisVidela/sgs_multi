﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class ModeloCobrosDetalleRepository
    {
        private sgsbdEntities ctx;


        public ModeloCobrosDetalleRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<modelo_cobro_detalle> GetModeloCobroDetalles()
        {
            return ctx.modelo_cobro_detalle.ToList();
        }

        public IEnumerable<ModeloCobrosDetallesView> GetModeloCobroDetallesForDetails(int id)
        {
            return (from mcd in ctx.modelo_cobro_detalle
                    join tm in ctx.tipo_modalidad on mcd.id_tipo_modalidad equals tm.id_tipo_modalidad into res
                    from idss in res.DefaultIfEmpty()
                    where mcd.id_modelo_cobro == id
                    select new ModeloCobrosDetallesView()
                    {
                        id_modelo_cobro_detalle = mcd.id_detalle_modelo_cobro,
                        rango_minimo = mcd.rango_minimo,
                        rango_maximo = mcd.rango_maximo,
                        valor = mcd.valor,
                        tipo_modalidad1 = idss.tipo_modalidad1,
                         id_modelo_cobro=mcd.id_modelo_cobro
                    }).OrderByDescending(m => m.id_modelo_cobro_detalle).ToList();
        }

        public modelo_cobro_detalle GetModeloCobroDetalleByID(int mcd)
        {
            return ctx.modelo_cobro_detalle.Find(mcd);
        }

        public void InsertModeloCobroDetalle(modelo_cobro_detalle mcd)
        {
             ctx.modelo_cobro_detalle.Add(mcd);
        }

        public void DeleteModeloCobroDetalle(int mcd)
        {
            modelo_cobro_detalle mcde=ctx.modelo_cobro_detalle.Find(mcd);
            ctx.modelo_cobro_detalle.Remove(mcde);
        }

        public void UpdateModeloCobroDetalle(modelo_cobro_detalle mcd)
        {
            ctx.Entry(mcd).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}