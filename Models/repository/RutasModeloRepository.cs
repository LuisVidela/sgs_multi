﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models.repository.interfaces;
using SGS.Models.ajax;

namespace SGS.Models.repository
{
    public class RutasModeloRepository:IDisposable
    {

        private sgsbdEntities ctx;
        public RutasModeloRepository(sgsbdEntities ctx) {
            this.ctx = ctx;
        }


        public IEnumerable<ruta_modelo> GetRutasModelo(int unidad)
        {
           return  this.ctx.ruta_modelo.Where(u=>u.id_unidad==unidad).ToList();
        }



        public IEnumerable<ruta_modelo> GetRutasModeloXTipoServicio(int tipo)
        {
            return this.ctx.ruta_modelo.Where(i => i.id_tipo_servicio == tipo).ToList();
        }


      

        public IEnumerable<tableDataView> GetRutaModelForTableData(string dia,int id_tipo_servicio,int unidad)
        {

            var data = (from d in ctx.ruta_modelo.Where(u=>u.id_unidad==unidad)
                        where ((d.dia_ruta == dia) && (d.id_tipo_servicio == id_tipo_servicio))
                        select new tableDataView()
                        {
                            id_ruta = d.id_ruta,
                            nombre_ruta = d.nombre_ruta,
                            horas = (from r in ctx.ruta_punto
                                     where r.id_ruta == d.id_ruta
                                     select new tableDataHorasView() { 
                                      h=r.Orden,
                                       punto_servicio=r.id_punto_servicio
                                     })
                        }).ToList();

            return data;

        }

        

        public ruta_modelo GetRutaModeloByID(int rm)
        {
            return ctx.ruta_modelo.Find(rm);
        }

        public void InsertRutaModelo(ruta_modelo rm)
        {
            ctx.ruta_modelo.Add(rm);
        }

        public void DeleteRutaModelo(int rm)
        {
            ruta_modelo rubro = ctx.ruta_modelo.Find(rm);
            ctx.ruta_modelo.Remove(rubro);
        }

        public void UpdateRutaModelo(ruta_modelo rm)
        {
            ctx.Entry(rm).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}