﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS.Models.repository.interfaces
{
    interface IPuntosServiciosRepository:IDisposable
    {
        IEnumerable<puntos_servicio> GetPuntosServicios();
        puntos_servicio GetPuntosServiciosByID(int ps);
        void InsertPuntoServicio(puntos_servicio ps);
        void DeletePuntoServicio(int ps);
        void UpdatePuntoServicio(puntos_servicio ps);
        void Save();
    }
}
