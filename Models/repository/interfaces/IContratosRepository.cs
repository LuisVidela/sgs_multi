﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SGS.Models;

namespace SGS.Models.repository.interfaces
{
    interface IContratosRepository
    {

        IEnumerable<contratos> GetContratos();
        contratos GetContratoByID(int ct);
        void InsertContrato(contratos ct);
        void DeleteContrato(int ct);
        void UpdateContrato(contratos ct);
        void Save();
    }
}
