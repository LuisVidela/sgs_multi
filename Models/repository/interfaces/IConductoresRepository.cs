﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS.Models.repository.interfaces
{
    interface IConductoresRepository:IDisposable
    {
        IEnumerable<conductores> GetConductores();
        conductores GetConductorByID(int conductor);
        void InsertConductor(conductores conductor);
        void DeleteConductor(int conductor);
        void UpdateConductor(conductores co);
        void Save();
    }
}
