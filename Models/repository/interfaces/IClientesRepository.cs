﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS.Models.repository.interfaces
{
    interface IClientesRepository:IDisposable
    {
        IEnumerable<clientes> GetClientes();
        clientes GetClienteByID(int cl);
        void InsertCliente(clientes cl);
        void DeleteCliente(int cl);
        void UpdateCliente(clientes cl);
        void Save();
    }
}
