﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS.Models.repository.interfaces
{
    interface ICamionesRepository:IDisposable
    {
        IEnumerable<camiones> GetCamiones();
        camiones GetCamionByID(int camion);
        void InsertCamion(camiones camion);
        void DeleteCamion(int camion);
        void UpdateCamion(camiones ca);
        void Save();
    }
}
