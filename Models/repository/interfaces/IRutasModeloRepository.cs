﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SGS.Models.repository.interfaces
{
    interface IRutasModeloRepository:IDisposable
    {

        IEnumerable<ruta_modelo> GetRutasModelo();
        ruta_modelo GetRutaModeloByID(int rm);
        void InsertRutaModelo(camiones rm);
        void DeleteRutaModelo(int rm);
        void UpdateRutaModelo(ruta_modelo rm);
        void Save();
    }
}
