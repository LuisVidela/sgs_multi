﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
 
   public class CentroResponsabilidadRepository:IDisposable
    {
         private sgsbdEntities ctx;


        public CentroResponsabilidadRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }

        public IEnumerable<centro_responsabilidad> GetCentroResponsabilidad()
        {
            return ctx.centro_responsabilidad.ToList();
        }

        public centro_responsabilidad GetCentroResponsabilidadByID(int c)
        {
            return ctx.centro_responsabilidad.Find(c);
        }

        public void InsertCentroResponsabilidad(centro_responsabilidad c)
        {
            ctx.centro_responsabilidad.Add(c);
        }

        public void DeleteCentroResponsabilidad(int c)
        {
            centro_responsabilidad centro_responsabilidad = ctx.centro_responsabilidad.Find(c);
            ctx.centro_responsabilidad.Remove(centro_responsabilidad);
        }

        public void UpdateCentroResponsabilidad(centro_responsabilidad c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public IEnumerable<object> GetCentroResponsabilidadForList()
        {

            return ctx.centro_responsabilidad.Select(cr => new
            {
                text = cr.centro_responsabilidad1,
                value = cr.id_centro
            }).ToList();
        }


        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}