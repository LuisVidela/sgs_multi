﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class CiudadesRepository:IDisposable
    {
         private sgsbdEntities ctx;


        public CiudadesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<ciudades> GetCiudades()
        {
            return ctx.ciudades.ToList();
        }

        public ciudades GetCiudadByID(int c)
        {
            return ctx.ciudades.Find(c);
        }

        public void InsertCiudad(ciudades c)
        {
             ctx.ciudades.Add(c);
        }

        public void DeleteCiudad(int c)
        {
            ciudades ciudad=ctx.ciudades.Find(c);
            ctx.ciudades.Remove(ciudad);
        }

        public void UpdateCiudad(ciudades c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}