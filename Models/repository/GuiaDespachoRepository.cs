﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Data.Entity.Validation;

namespace SGS.Models.repository
{
    public class GuiaDespachoRepository
    {

        private sgsbdEntities ctx;


        public void Insert(TB_GIA_Guia ps)
        {
            ctx = new sgsbdEntities();
            ps.GUIA_ID_UNIDAD = Models.repository.UsuariosRepository.getUnidadSeleccionada();
            ps.GUIA_FECHA = DateTime.Today;
            ps.GUIA_TOKEN = Guid.NewGuid();
            ctx.TB_GIA_Guia.Add(ps);
            ctx.SaveChanges();
        }

        public void Update(TB_GIA_Guia ps)
        {
            //this.ctx.Entry(ps).State = System.Data.EntityState.Modified;
            ctx = new sgsbdEntities();
            TB_GIA_Guia old = (from guia in ctx.TB_GIA_Guia
                               where guia.GUIA_CODIGO == ps.GUIA_CODIGO
                               select guia).FirstOrDefault();
            old.GUIA_IDENTIFICADOR = ps.GUIA_IDENTIFICADOR;
            old.GUIA_NOMBRE = ps.GUIA_NOMBRE;
            old.GUIA_PATENTE = ps.GUIA_PATENTE;
            old.GUIA_ID_VERTEDERO = ps.GUIA_ID_VERTEDERO;
            old.GUIA_FECHALLEGADA = ps.GUIA_FECHALLEGADA;
            old.GUIA_EGIA_CODIGO = ps.GUIA_EGIA_CODIGO;

            try
            {
                ctx.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public void Save()
        {
            this.ctx.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}