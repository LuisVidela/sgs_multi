﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;
using System.Data.Objects.SqlClient;


namespace SGS.Models.repository
{
    public class ContratosRepository:IContratosRepository
    {
          private sgsbdEntities ctx;


        public ContratosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        }
        public IEnumerable<contratos> GetContratos()
        {
            return ctx.contratos.ToList();
        }

        public IEnumerable<ContratosView> GetContratosView2(int unidad)
        {
            var join = (from c in ctx.contratos
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        join t2 in ctx.tipo_contrato on c.id_tipo_contrato equals t2.id_tipo_contrato
                        join cl in ctx.clientes on c.id_cliente equals cl.id_cliente
                        where c.id_unidad == unidad 
                        select new ContratosView()
                        {
                            id_contrato =c.id_contrato,
                            razon_social = cl.razon_social,
                            sucursal = s.sucursal,
                            fecha_inicio = c.fecha_inicio,
                            fecha_renovacion = c.fecha_renovacion.Value,
                            tipo_contrato =t2.tipo_contrato1,
                        }).ToList();

            return join;
        }

        public IEnumerable<ContratosView> GetContratosView()
        {
            var join = (from c in ctx.contratos
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        join t2 in ctx.tipo_contrato on c.id_tipo_contrato equals t2.id_tipo_contrato
                        join cl in ctx.clientes on c.id_cliente equals cl.id_cliente
                        into tmp
                        from res in tmp.DefaultIfEmpty()
                        select new ContratosView()
                        {
                            id_contrato = c.id_contrato,
                            razon_social = res.razon_social,
                            sucursal = s.sucursal,
                            fecha_inicio = c.fecha_inicio,
                            fecha_renovacion = c.fecha_renovacion.Value,
                            tipo_contrato = t2.tipo_contrato1,
                        }).ToList();

            return join;
        }

        public ContratosView GetContratosView(int id)
        {
           
            var join=(from c in ctx.contratos
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        join t2 in ctx.tipo_contrato on c.id_tipo_contrato equals t2.id_tipo_contrato
                        join cl in ctx.clientes on c.id_cliente equals cl.id_cliente
                        into tmp
                        where c.id_contrato==id
                        from res in tmp.DefaultIfEmpty()
                        select new ContratosView()
                        {
                            id_contrato = c.id_contrato,
                            razon_social = res.razon_social,
                            sucursal = s.sucursal,
                            fecha_inicio = c.fecha_inicio,
                            fecha_renovacion = c.fecha_renovacion.Value,
                            tipo_contrato = t2.tipo_contrato1,
                            estado=c.estado
                        }).ToList();
            return join.First();
         
        }


        public IEnumerable<object> GetContratosForList(int? unidad) {

            return ctx.contratos.Where(v => v.id_unidad==unidad)
                                     .Join(ctx.clientes.OrderBy(c => c.razon_social),
                                      c => c.id_cliente,
                                      cl => cl.id_cliente,
                                      (c, cl) => new { cliente = cl, contrato = c })
                                      .Join(ctx.tipo_contrato,
                                      c2 => c2.contrato.id_tipo_contrato,
                                      tt => tt.id_tipo_contrato,
                                      (c2, tt) => new { c2.cliente, c2.contrato, tt }
                                      )
                                      .Select(x => new
                                      {
                                          value = x.contrato.id_contrato,
                                          text = x.cliente.razon_social + " Servicio " + SqlFunctions.StringConvert((double)x.contrato.id_contrato) + " " + x.tt.tipo_contrato1

                                      }
                                      ).ToList().OrderBy(sli => sli.text); ;
        }

        public IEnumerable<ContratosView> GetContratosView2()
        {
            var join = (from c in ctx.contratos
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        join un in ctx.unidades_negocio on c.id_unidad equals un.id_unidad
                        join t2 in ctx.tipo_contrato on c.id_tipo_contrato equals t2.id_tipo_contrato
                        join cl in ctx.clientes on c.id_cliente equals cl.id_cliente
                        into tmp
                        from res in tmp.DefaultIfEmpty()
                        select new ContratosView()
                        {
                            id_contrato = c.id_contrato,
                            razon_social = res.razon_social,
                            sucursal = s.sucursal,
                            fecha_inicio = c.fecha_inicio,
                            fecha_renovacion = c.fecha_renovacion,
                            tipo_contrato = t2.tipo_contrato1,
                            unidad_negocio = un.unidad,
                            //dias = SqlFunctions.DateDiff("dd", DateTime.Now, c.fecha_renovacion ),

                        }).ToList();
        
            return join;
        }


        public IEnumerable<ContratosView> GetContratosView3()
        {
            var join = (from c in ctx.contratos
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        join ca in ctx.categoria_sucursal on s.id_categoria equals ca.id_categoria
                        //into cate
                        //from categ in cate.DefaultIfEmpty()
                        join cl in ctx.clientes on c.id_cliente equals cl.id_cliente 
                        into tmp
                        from res in tmp.DefaultIfEmpty()
                        select new ContratosView()
                        {
                            //dias = ctx.sp_calcular_vencimiento(c.fecha_renovacion),
                            razon_social = res.razon_social,
                            categoria = ca.categoria 

                        }).ToList();

            return join;
        }


        public contratos GetContratoByID(int ct)
        {
            return ctx.contratos.Find(ct);
        }

        public void InsertContrato(contratos ct)
        {
            ctx.contratos.Add(ct);
        }

        public void DeleteContrato(int ct)
        {
            contratos cl2 = ctx.contratos.Find(ct);
            ctx.contratos.Remove(cl2);
        }

        public void UpdateContrato(contratos ct)
        {
            ctx.Entry(ct).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        protected bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}