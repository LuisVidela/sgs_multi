﻿using System;
using System.Collections.Generic;
using SGS.Models.modelViews;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class ResiduosRepository
    {
         private sgsbdEntities ctx;


        public ResiduosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }

        public IEnumerable<ResiduosView> GetResiduosView()
        {
            Int32 unidad = Models.repository.UsuariosRepository.getUnidadSeleccionada();
            var data = (from re in this.ctx.residuos
                        where re.id_unidad == unidad
                        select new ResiduosView()
                        {
                            id_residuo = re.id_residuo,
                            residuo = re.residuo,
                            um_residuo = re.um_residuo,
                            densidad = re.densidad,
                            tiempo_acopio = re.tiempo_acopio
                        });


            return data == null ? null : data.ToList();

        }



        public IEnumerable<residuos> GetResiduos()
        {
            return ctx.residuos.ToList();
        }

        public IEnumerable<object> GetResiduosForList() {

            return ctx.residuos.Select(re => new
            {
                text=re.residuo,
                value=re.id_residuo
            }).ToList();
        }

        public residuos GetResiduoByID(int re)
        {
            return ctx.residuos.Find(re);
        }

        public void InsertResiduo(residuos re)
        {
             ctx.residuos.Add(re);
        }

        public void DeleteResiduo(int re)
        {
            residuos rs=ctx.residuos.Find(re);
            ctx.residuos.Remove(rs);
        }

        public void UpdateResiduo(residuos re)
        {
            ctx.Entry(re).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}