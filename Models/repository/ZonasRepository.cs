﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class ZonasRepository
    {

        private sgsbdEntities ctx;


        public ZonasRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<Zona> GetZonas()
        {
            return ctx.Zona.ToList();
        }

        public Zona GetZonasByID(int e)
        {
            return ctx.Zona.Find(e);
        }

        public void InsertZona(Zona e)
        {
            ctx.Zona.Add(e);
        }

        public void DeleteZona(int e)
        {
            Zona comuna = ctx.Zona.Find(e);
            ctx.Zona.Remove(comuna);
        }

        public void UpdateZona(Zona e)
        {
            ctx.Entry(e).State = System.Data.EntityState.Modified;
        }



        public IEnumerable<object> GetZonasForList()
        {
            List<Models.Zona> zonas = ctx.Zona.ToList();

            return zonas.Select(zo => new
           {
               value = zo.id_zona,
               text = string.Format("{0} / {1} M3", zo.id_zona, zo.nombre_zona)
           }).ToList();

        }




        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }


}