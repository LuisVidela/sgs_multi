﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class HerramientasRepository
    {
         private sgsbdEntities ctx;


        public HerramientasRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<herramientas> GetHerramientas()
        {
            return ctx.herramientas.ToList();
        }

        public IEnumerable<HerramientasView> GetHerramientas2() {


            return ctx.herramientas.Join(ctx.camiones,
                                        h => h.id_camion, 
                                        c => c.id_camion, 
                                        (h, c) =>new HerramientasView() {
                                            id_herramienta= h.id_herramienta,
                                            camion = c.camion,
                                            herramienta = h.herramienta 
                                        
                                        }).ToList();
        
        }

        public herramientas GetHerramientaByID(int h)
        {
            return ctx.herramientas.Find(h);
        }

        public void InsertHerramienta(herramientas h)
        {
             ctx.herramientas.Add(h);
        }

        public void DeleteHerramienta(int h)
        {
            herramientas herramienta=ctx.herramientas.Find(h);
            ctx.herramientas.Remove(herramienta);
        }

        public void UpdateHerramienta(herramientas h)
        {
            ctx.Entry(h).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}