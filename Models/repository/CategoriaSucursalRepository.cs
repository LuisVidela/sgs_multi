﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.repository
{
    public class CategoriaSucursalRepository
    {

            private sgsbdEntities ctx;


            public CategoriaSucursalRepository(sgsbdEntities ctx)
            {

                this.ctx = ctx;

            }


            public IEnumerable<categoria_sucursal> GetCategoriaSucursal()
            {
                return ctx.categoria_sucursal.ToList();
            }

            public categoria_sucursal GetCategoriaSucursalByID(int c)
            {
                return ctx.categoria_sucursal.Find(c);
            }

            public void InsertCategoriaSucursal(categoria_sucursal c)
            {
                ctx.categoria_sucursal.Add(c);
            }

        public IEnumerable<categoria_sucursal> GetCategoriaSucursal1()
        {
            return ctx.categoria_sucursal.ToList();
        }

        public void UpdateCategoriaSucursal(categoria_sucursal c)
            {
                ctx.Entry(c).State = System.Data.EntityState.Modified;
            }

            public void Save()
            {
                ctx.SaveChanges(); 
            }

            private bool disposed = false;

            protected virtual void Dispose(bool disposing)
            {
                if (!this.disposed)
                {
                    if (disposing)
                    {
                        ctx.Dispose();
                    }
                }
                this.disposed = true;
            }

            public void Dispose()
            {
                Dispose(true);
                GC.SuppressFinalize(this);
            }
    }
    
}
