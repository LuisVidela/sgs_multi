﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using System.Data.Entity.Validation;

namespace SGS.Models.repository
{
    public class CamionesRepository : ICamionesRepository
    {

        private sgsbdEntities ctx;



        public IEnumerable<camiones> getCamionesWithAll()
        {


            return ctx.camiones.Include("conductores")
                .Include("camiones_marcas")
                .Include("tipo_servicio")
                .ToList();

        }

        public IEnumerable<camiones> getCamionesWithAll2(int unidad)
        {


            var join = from d in ctx.camiones
                       join c in ctx.franquiciado
                       on d.id_franquiciado equals c.id_franquiciado into res
                       from c in res.DefaultIfEmpty()
                       where c.id_unidad == unidad
                       select d;

            return join.ToList();



        }

        public IEnumerable<camiones> getCamionesWithAll2()
        {


            var join = from d in ctx.camiones
                       join c in ctx.franquiciado
                       on d.id_franquiciado equals c.id_franquiciado into res
                       from c in res.DefaultIfEmpty()
                       select d;

            return join.ToList();



        }

        public IEnumerable<CamionesView> getCamionesWithAll3()
        {
            return (from d in ctx.camiones
                    join c in ctx.franquiciado on d.id_franquiciado equals c.id_franquiciado
                    join s in ctx.tipo_servicio on d.id_tipo_servicio equals s.id_tipo_servicio
                    select new CamionesView()
                    {
                        camion = d.camion,
                        n_interno = d.num_interno,
                        estado = d.estado,
                        //  nombre_franquiciado = c.razon_social,
                        patente = d.patente,
                        //resolucion_transporte = d.resolucion_transporte,
                        tipo_servicio1 = s.tipo_servicio1,
                        restricciones = (from a in ctx.restriccion_camion_cliente where a.id_camion == d.id_camion select a)
                    }).ToList();

        }

        public IEnumerable<CamionesView> getCamionesWithAll32(int unidad)
        {
            return (from d in ctx.camiones
                    join c in ctx.franquiciado on d.id_franquiciado equals c.id_franquiciado
                    join s in ctx.tipo_servicio on d.id_tipo_servicio equals s.id_tipo_servicio
                    join rs in ctx.restriccion_camion_cliente on d.id_camion equals rs.id_camion
                    into res
                    from final in res.DefaultIfEmpty()
                    where c.id_unidad == unidad
                    select new CamionesView()
                    {
                        camion = d.camion,
                        n_interno = d.num_interno,
                        estado = d.estado,
                        nombre_franquiciado = c.razon_social,
                        patente = d.patente,
                        //resolucion_transporte = d.resolucion_transporte,
                        tipo_servicio1 = s.tipo_servicio1,
                        restricciones = res,
                        id_camion = d.id_camion
                    }).ToList();

        }

        public IEnumerable<CamionesView> getCamionesWithAll32()
        {
            return (from d in ctx.camiones
                    join c in ctx.franquiciado on d.id_franquiciado equals c.id_franquiciado
                    join s in ctx.tipo_servicio on d.id_tipo_servicio equals s.id_tipo_servicio
                    join rs in ctx.restriccion_camion_cliente on d.id_camion equals rs.id_camion
                    into res
                    from final in res.DefaultIfEmpty()
                    select new CamionesView()
                    {
                        camion = d.camion,
                        n_interno = d.num_interno,
                        estado = d.estado,
                        nombre_franquiciado = c.razon_social,
                        patente = d.patente,
                        //resolucion_transporte = d.resolucion_transporte,
                        tipo_servicio1 = s.tipo_servicio1,
                        restricciones = res,
                        id_camion = d.id_camion
                    }).ToList();

        }
        public IEnumerable<object> getPatentesCamionesFranquiciadosForList(int id_franquiciado)
        {

            return (from c in ctx.camiones.Where(c => c.id_franquiciado == id_franquiciado)
                    orderby c.id_camion
                    select new CamionesExcelView()
                    {
                        id_camion = c.id_camion,
                        patente = c.patente
                    }).ToList();

        }
        public IEnumerable<object> GetCamionesbyFranq(int id_franquiciado)
        {
            return (from c in ctx.camiones.Where(c => c.id_franquiciado == id_franquiciado)
                    orderby c.id_camion
                    select new CamionesExcelView()
                    {
                        id_camion = c.id_camion,
                        patente = c.patente
                    }).ToList();
        }
        public IEnumerable<object> getCamionesForList()
        {

            return ctx.camiones.Select(c => new
            {
                value = c.id_camion,
                text = c.patente

            }).ToList();

        }
        public camionesShadowBoxView GetPatentesCamionByIdFrancquiciado(int idfranquicia)
        {
            var join = (from c in ctx.camiones.Where(c => c.id_franquiciado == idfranquicia)
                        join
                            cd in ctx.franquiciado.DefaultIfEmpty() on c.id_franquiciado equals cd.id_franquiciado

                        select new camionesShadowBoxView()
                        {
                            // id_camion =c.id_camion,
                            camion = c.camion,
                            patente = c.patente,
                            capacidad_camion = c.capacidad_camion,
                            estado = c.estado,
                            modelo_camion = c.modelo_camion,
                            observacion = c.observacion,
                            tipo_camion = c.tipo_camion,
                            //idfranquicia=c.id_franquiciado
                        }).FirstOrDefault();


            return join;
        }
        public camionesShadowBoxView GetCamionShadowBox(int id)
        {


            var join = (from c in ctx.camiones.Where(c => c.id_camion == id)
                        join
                            cd in ctx.franquiciado.DefaultIfEmpty() on c.id_franquiciado equals cd.id_franquiciado
                        join
                            ts in ctx.tipo_servicio.DefaultIfEmpty() on c.id_tipo_servicio equals ts.id_tipo_servicio
                        join
                            m in ctx.camiones_marcas.DefaultIfEmpty() on c.id_camion_marca equals m.id_camion_marca
                        join
                            rss in ctx.restriccion_camion_cliente.DefaultIfEmpty() on c.id_camion equals rss.id_camion into res
                        from r in res.DefaultIfEmpty()
                        select new camionesShadowBoxView()
                        {
                            //id_camion=c.id_camion,
                            camion = c.camion,
                            patente = c.patente,
                            capacidad_camion = c.capacidad_camion,
                            estado = c.estado,
                            modelo_camion = c.modelo_camion,
                            observacion = c.observacion,
                            tipo_camion = c.tipo_camion,
                            marca = m.marca,
                            nombre_conductor = cd.razon_social,
                            tipo_servicio1 = ts.tipo_servicio1,
                            restricciones = res
                            //restricciones = (from rs in ctx.restriccion_camion_cliente
                            //                 join cl in ctx.clientes.DefaultIfEmpty() on rs.id_cliente equals cl.id_cliente
                            //                 where rs.id_cliente == cl.id_cliente
                            //                 select new restriccion_camion_cliente()
                            //                 {
                            //                     id_restriccion = rs.id_restriccion,
                            //                     id_cliente = rs.id_cliente,
                            //                     id_camion = rs.id_camion,
                            //                     cliente = cl.razon_social,
                            //                     motivo = rs.motivo

                            //                 })


                        }).FirstOrDefault();

            foreach (var item in join.restricciones)
            {
                item.cliente = ctx.clientes.Where(w => w.id_cliente == item.id_cliente).FirstOrDefault().razon_social;
            }


            return join;
        }

        public IEnumerable<CamionesExcelView> getCamionesForExcel()
        {

            return (from c in ctx.camiones
                    join ts in ctx.tipo_servicio.DefaultIfEmpty() on c.id_tipo_servicio equals ts.id_tipo_servicio
                    join cd in ctx.franquiciado.DefaultIfEmpty() on c.id_franquiciado equals cd.id_franquiciado
                    join m in ctx.camiones_marcas.DefaultIfEmpty() on c.id_camion_marca equals m.id_camion_marca
                    orderby c.id_camion
                    select new CamionesExcelView()
                    {
                        camion = c.camion,
                        capacidad_camion = c.capacidad_camion,
                        estado = c.estado,
                        //fecha_ingreso = cd.fecha_ingreso,
                        id_camion = c.id_camion,
                        id_camion_marca = c.id_camion_marca,
                        id_conductor = c.id_franquiciado,
                        id_tipo_licencia = cd.id_franquiciado,
                        id_tipo_servicio = c.id_tipo_servicio,
                        modelo_camion = c.modelo_camion,
                        nombre_conductor = cd.razon_social,
                        observacion = c.observacion,
                        patente = c.patente,
                        rut_conductor = cd.rut_franquiciado,
                        tipo_camion = c.tipo_camion,
                        tipo_servicio1 = ts.tipo_servicio1,
                        marca = m.marca

                    }).ToList();



        }

        public IEnumerable<CamionesExcelView> getCamionesbyfranc(int idfranquicia)
        {

            return (from c in ctx.camiones.Where(c => c.id_franquiciado == idfranquicia)
                    join ts in ctx.tipo_servicio.DefaultIfEmpty() on c.id_tipo_servicio equals ts.id_tipo_servicio
                    join cd in ctx.franquiciado.DefaultIfEmpty() on c.id_franquiciado equals cd.id_franquiciado
                    join m in ctx.camiones_marcas.DefaultIfEmpty() on c.id_camion_marca equals m.id_camion_marca
                    orderby c.id_camion
                    select new CamionesExcelView()
                    {
                        camion = c.camion,
                        capacidad_camion = c.capacidad_camion,
                        estado = c.estado,
                        //fecha_ingreso = cd.fecha_ingreso,
                        id_camion = c.id_camion,
                        id_camion_marca = c.id_camion_marca,
                        id_conductor = c.id_franquiciado,
                        id_tipo_licencia = cd.id_franquiciado,
                        id_tipo_servicio = c.id_tipo_servicio,
                        modelo_camion = c.modelo_camion,
                        nombre_conductor = cd.razon_social,
                        observacion = c.observacion,
                        patente = c.patente,
                        rut_conductor = cd.rut_franquiciado,
                        tipo_camion = c.tipo_camion,
                        tipo_servicio1 = ts.tipo_servicio1,
                        marca = m.marca

                    }).ToList();



        }
        public IEnumerable<camiones> getCamionesWithAllLinq()
        {


            var query = from d in ctx.camiones.Include("camiones_marcas")
                        .Include("conductores")
                        .Include("tipo_servicio")
                        select d;

            return query.ToList();


        }




        public CamionesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<camiones> GetCamiones()
        {
            return ctx.camiones.ToList();
        }

        public camiones GetCamionByID(int camion)
        {
            return ctx.camiones.Find(camion);
        }

        public void InsertCamion(camiones camion)
        {
            ctx.camiones.Add(camion);
        }

        public void DeleteCamion(int camion)
        {
            camiones c = ctx.camiones.Find(camion);
            ctx.camiones.Remove(c);
        }

        public void UpdateCamion(camiones ca)
        {
            ctx.Entry(ca).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            try
            {
                ctx.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                var newException = e;
                throw newException;
            }


            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}