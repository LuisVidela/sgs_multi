﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class EquiposRepository
    {
        private sgsbdEntities ctx;


        public EquiposRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<equipos> GetEquipos()
        {
            return ctx.equipos.ToList();
        }

        public equipos GetEquipoByID(int e)
        {
            return ctx.equipos.Find(e);
        }

        public void InsertEquipo(equipos e)
        {
            ctx.equipos.Add(e);
        }

        public void DeleteEquipo(int e)
        {
            equipos comuna = ctx.equipos.Find(e);
            ctx.equipos.Remove(comuna);
        }

        public void UpdateEquipo(equipos e)
        {
            ctx.Entry(e).State = System.Data.EntityState.Modified;
        }



        public IEnumerable<object> GetEquiposForList()
        {
            List<Models.equipos> equipos = ctx.equipos.ToList();

           return equipos.Select(eq => new
           {
               value = eq.id_equipo,
               text = string.Format("{0} / {1} M3", eq.tipo_equipo, eq.capacidad_equipo)
           }).ToList();

        }




        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}