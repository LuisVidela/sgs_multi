﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class CobroClientesHistoricoRepository : ICobroClientesRepository
    {

        private sgsbdEntities ctx;

        public CobroClientesHistoricoRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoServicioDescView> getPuntoServicioDescripcion(int idpuntoservicio)
        {
            int id_unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_PUNTO_SERVICIO_DESCRIPCION.Where(d => d.id_punto_servicio == idpuntoservicio && d.id_unidad == id_unidad)
                    select new PuntoServicioDescView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_tipo_servicio = d.id_tipo_servicio,
                        tipo_servicio = d.tipo_servicio,
                        nombre_punto = d.nombre_punto,
                        nombre_punto1 = d.nombre_punto1
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <param name="id_unidad"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesView> getCobroClientesCerrado(string whenis, int id_unidad)
        {

            return (from d in ctx.V_CALCULO_COBRO_CLIENTES_CERRADO.Where(d => d.Fecha == whenis && d.id_unidad == id_unidad)
                    select new CobroClientesView()
                    {
                        id_pago = d.id_pago,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Numero_Retiros = d.Numero_retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <param name="glosa"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienteDisposicionView> getDisposicionExcepcion(int idpuntoservicio, string glosa)
        {

            return (from d in ctx.calculo_cobro_disposicion.Where(d => d.id_punto_servicio == idpuntoservicio && d.glosa.ToUpper().Trim() == glosa.ToUpper().Trim())

                    select new CobroClienteDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        Disposicion = d.Disposicion,
                        CobroTonelada = d.CobroTonelada,
                        Valor = d.Valor,
                        valoruf = d.valoruf
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <param name="idTipoModalidad"></param>
        /// <returns></returns>
        public IEnumerable<PuntoCobroToneView> getPuntoCobroTonelada(int idpuntoservicio, int idTipoModalidad)
        {

            return (from d in ctx.V_PTO_SERVICIO_COBRO_TONELADA.Where(d => d.id_punto_servicio == idpuntoservicio && d.id_tipo_modalidad == idTipoModalidad)

                    select new PuntoCobroToneView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_modelo = d.id_modelo,
                        valor = d.valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> getCalculoPuntos(string rutcliente, string whenis)
        {

            return (from d in ctx.V_CALCULO_PUNTO.Where(d => d.rut_cliente == rutcliente)
                                                 .Where(d => d.Fecha == whenis)
                    select new CobroPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.Id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.Sucursal,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> getCalculoPuntosCerrado(string rutcliente, string whenis)
        {
            int id_unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_CALCULO_PUNTO_CERRADO.Where(d => d.rut_cliente == rutcliente && d.id_unidad == id_unidad)
                                                         .Where(d => d.Fecha == whenis)
                    select new CobroPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.Id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.Sucursal,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesPuntosView> getCobroPdfCerradoPtoServ(int idcliente, string whenis, int idpuntoservicio)
        {
            return (from d in ctx.V_CALCULO_COBRO_PUNTO_CERRADO.Where(d => d.id_cliente == idcliente)
                    .Where(d => d.Fecha == whenis)
                    .Where(d => d.id_punto_servicio == idpuntoservicio)
                    .OrderBy(d => d.sorting)
                    .ThenBy(d => d.fechaFull)
                    select new CobroClientesPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.sucursal,
                        fechaFull = d.fechaFull,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Peso = d.peso,
                        Volteos = d.Volteos,
                        Valor = d.Valor,
                        Fecha = d.Fecha,
                        Orden = d.Orden,
                        tipo_modalidad = d.tipo_modalidad,
                        sorting = d.sorting,
                        glosa = d.glosa,
                        cobro_arriendo = d.cobro_arriendo,
                        conductor = d.conductor,
                        camion = d.camion,
                        como_se_calcula = d.como_se_calcula,
                        punto_servicio = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select (cp)).FirstOrDefault(),
                        codigo_externo = (from cp in ctx.hoja_ruta_detalle.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select cp.codigo_externo).FirstOrDefault(),
                        cobro_por_m3 = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                        select cp.cobro_por_m3).FirstOrDefault(),
                        cobro_por_peso = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select cp.cobro_por_peso).FirstOrDefault()
                    }).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<EquipoView> getEquipo(int idpuntoservicio)
        {

            return (from d in ctx.V_EQUIPO.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new EquipoView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_equipo = d.id_equipo,
                        Equipo = d.Equipo,
                        nom_nemo = d.nom_nemo
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoServicioDireView> getPtoServicioDire(int idpuntoservicio)
        {

            return (from d in ctx.V_PUNTO_SERVICIO_DIRE.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new PuntoServicioDireView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        sucursal = d.sucursal,
                        zona = d.zona,
                        ciudad = d.ciudad,
                        nombre_comuna = d.nombre_comuna,
                        pais = d.pais,
                        rubro = d.rubro,
                        id_contrato = d.id_contrato,
                        estado = d.estado
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IEnumerable<ClientesView> getClienteData(int idcliente)
        {

            return (from d in ctx.clientes.Where(d => d.id_cliente == idcliente)

                    select new ClientesView()
                    {
                        id_cliente = d.id_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        rut_cliente = d.rut_cliente
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public int getProcesoCerrado(string whenis)
        {

            return (from p in ctx.calculo_realizado
                    where p.Fecha == whenis
                    select p.Cerrado).FirstOrDefault();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienteDisposicionView> getDisposicionExcepcion(int idpuntoservicio)
        {

            return (from d in ctx.calculo_cobro_disposicion.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new CobroClienteDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        Disposicion = d.Disposicion,
                        CobroTonelada = d.CobroTonelada,
                        Valor = d.Valor,
                        valoruf = d.valoruf
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienDisposicionView> getDisposicionCerrado(int idcliente, string whenis, int idpuntoservicio)
        {

            return (from d in ctx.V_DISPOSICION_CERRADO.Where(d => d.id_cliente == idcliente)
                                                       .Where(d => d.Fecha == whenis)
                                                       .Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new CobroClienDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_cliente = d.id_cliente,
                        Fecha = d.Fecha,
                        Disposicion = d.Disposicion,
                        Valor = d.Valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoCobroToneView> getPuntoCobroTonelada(int idpuntoservicio)
        {

            return (from d in ctx.V_PTO_SERVICIO_COBRO_TONELADA.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new PuntoCobroToneView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_modelo = d.id_modelo,
                        valor = d.valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoNumEquiposView> getPuntoNumEquipos(int idpuntoservicio)
        {

            return (from d in ctx.V_PUNTO_NUM_EQUIPOS.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new PuntoNumEquiposView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        cant_equipos = d.cant_equipos
                    }).ToList();

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}