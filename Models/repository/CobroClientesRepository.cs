﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;
using System.IO;
using System.Data.Objects.SqlClient;

namespace SGS.Models.repository
{
    /// <summary>
    /// 
    /// </summary>
    public class CobroClientesRepository : ICobroClientesRepository
    {

        private sgsbdEntities ctx;
        public string erorris;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctx"></param>
        public CobroClientesRepository(sgsbdEntities ctx)
        {
            this.ctx = ctx;

            // Agregada lógica de time out. Entity framework no soporta por un error ajuste del valor desde cadena de conexión. Usar variable en su lugar
            var adapter = (IObjectContextAdapter)this.ctx;
            var objectContext = adapter.ObjectContext;
            objectContext.CommandTimeout = Convert.ToInt32(ConfigurationManager.AppSettings.Get("EF.TimeOut")) * 60;
        }
        public IEnumerable<CobroClientesPuntosView> GetCobroPdfPtoServ(int idcliente, string whenis, int idpuntoservicio)
        {
            return (from d in ctx.V_CALCULO_COBRO_PUNTO_v2.Where(d => d.id_cliente == idcliente)
                                                       .Where(d => d.Fecha == whenis)
                                                       .Where(d => d.id_punto_servicio == idpuntoservicio)
                                                       .OrderBy(d => d.sorting)
                                                       .ThenBy(d => d.fechaFull)
                    select new CobroClientesPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        //Sucursal = d.sucursal,
                        fechaFull = d.fechaFull,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        //Peso = d.peso,
                        Volteos = d.Volteos,
                        Valor = d.Valor,
                        Fecha = d.Fecha,
                        Orden = d.Orden,
                        tipo_modalidad = d.tipo_modalidad,
                        sorting = d.sorting,
                        glosa = d.glosa,
                        cobro_arriendo = d.cobro_arriendo,
                        conductor = d.conductor,
                        camion = d.camion,
                        como_se_calcula = d.como_se_calcula,
                        codigo_externo = d.codigo_externo,
                        //guia_sesma = d.guia_sesma
                    }).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<PuntoEstadosView> getPuntoEstados()
        {
            return (from d in ctx.punto_estados

                    select new PuntoEstadosView()
                    {
                        estado = d.estado,
                        descripcion = d.descripcion
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<DataCalculosFullView> getCalculoPuntosAll()
        {
            return (from d in ctx.V_DATA_CALCULOS_FULL1.OrderBy(d => d.fechaFull)

                    select new DataCalculosFullView()
                    {
                        idData = d.idData,
                        fechacorta = d.fechacorta,
                        nombre_punto = d.nombre_punto,
                        sistema = d.sistema,
                        camion = d.camion,
                        franquiciado = d.franquiciado,
                        conductor = d.conductor,
                        estadopunto = d.estadopunto,
                        relleno = d.relleno,
                        cantidad = d.cantidad,
                        bruto = d.bruto,
                        Pago = d.Pago,
                        Margen = d.Margen,
                        MargenPorc = d.MargenPorc,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_punto_servicio = d.id_punto_servicio,
                        fechaFull = d.fechaFull
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="who"></param>
        /// <returns></returns>
        public IEnumerable<DataCalculosFullView> getCalculoPuntosFranquiciado(string who)
        {
            return (from d in ctx.V_DATA_CALCULOS_FULL1.Where(d => d.franquiciado == who)
                                                       .OrderBy(d => d.fechaFull)

                    select new DataCalculosFullView()
                    {
                        idData = d.idData,
                        fechacorta = d.fechacorta,
                        nombre_punto = d.nombre_punto,
                        sistema = d.sistema,
                        camion = d.camion,
                        franquiciado = d.franquiciado,
                        conductor = d.conductor,
                        estadopunto = d.estadopunto,
                        relleno = d.relleno,
                        cantidad = d.cantidad,
                        bruto = d.bruto,
                        Pago = d.Pago,
                        Margen = d.Margen,
                        MargenPorc = d.MargenPorc,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_punto_servicio = d.id_punto_servicio,
                        fechaFull = d.fechaFull
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="estado"></param>
        /// <returns></returns>
        public IEnumerable<DataCalculosFullView> getCalculoPuntosEstado(string estado)
        {
            return (from d in ctx.V_DATA_CALCULOS_FULL1.Where(d => d.estadopunto == estado)
                                                       .OrderBy(d => d.fechaFull)

                    select new DataCalculosFullView()
                    {
                        idData = d.idData,
                        fechacorta = d.fechacorta,
                        nombre_punto = d.nombre_punto,
                        sistema = d.sistema,
                        camion = d.camion,
                        franquiciado = d.franquiciado,
                        conductor = d.conductor,
                        estadopunto = d.estadopunto,
                        relleno = d.relleno,
                        cantidad = d.cantidad,
                        bruto = d.bruto,
                        Pago = d.Pago,
                        Margen = d.Margen,
                        MargenPorc = d.MargenPorc,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_punto_servicio = d.id_punto_servicio,
                        fechaFull = d.fechaFull
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sistema"></param>
        /// <returns></returns>
        public IEnumerable<DataCalculosFullView> getCalculoPuntosSistema(string sistema)
        {
            return (from d in ctx.V_DATA_CALCULOS_FULL1.Where(d => d.sistema == sistema)
                                                       .OrderBy(d => d.fechaFull)

                    select new DataCalculosFullView()
                    {
                        idData = d.idData,
                        fechacorta = d.fechacorta,
                        nombre_punto = d.nombre_punto,
                        sistema = d.sistema,
                        camion = d.camion,
                        franquiciado = d.franquiciado,
                        conductor = d.conductor,
                        estadopunto = d.estadopunto,
                        relleno = d.relleno,
                        cantidad = d.cantidad,
                        bruto = d.bruto,
                        Pago = d.Pago,
                        Margen = d.Margen,
                        MargenPorc = d.MargenPorc,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_punto_servicio = d.id_punto_servicio,
                        fechaFull = d.fechaFull
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="BeginIs"></param>
        /// <param name="EndIs"></param>
        /// <returns></returns>
        public string CalculoPuntosFechas(string BeginIs, string EndIs)
        {

            string erorris = "";

            try
            {
                ctx.sp_cobro_clientes_periodo1(BeginIs, EndIs);
            }
            catch (Exception ex)
            {
                erorris = "El Proceso de Calculo ha finalizado con errores";
            }

            return erorris;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="punto"></param>
        /// <returns></returns>
        public IEnumerable<DataCalculosFullView> getCalculoPuntosFechas(Int16 punto)
        {

            if (punto == 0)
            {
                return (from d in ctx.V_DATA_CALCULOS_FULL1.OrderBy(d => d.fechaFull)

                        select new DataCalculosFullView()
                        {
                            idData = d.idData,
                            fechacorta = d.fechacorta,
                            nombre_punto = d.nombre_punto,
                            sistema = d.sistema,
                            camion = d.camion,
                            franquiciado = d.franquiciado,
                            conductor = d.conductor,
                            estadopunto = d.estadopunto,
                            relleno = d.relleno,
                            cantidad = d.cantidad,
                            bruto = d.bruto,
                            Pago = d.Pago,
                            Margen = d.Margen,
                            MargenPorc = d.MargenPorc,
                            id_hoja_ruta = d.id_hoja_ruta,
                            id_punto_servicio = d.id_punto_servicio,
                            fechaFull = d.fechaFull
                        }).ToList();
            }
            else
            {
                return (from d in ctx.V_DATA_CALCULOS_FULL1.Where(d => d.id_punto_servicio == punto)
                                                           .OrderBy(d => d.fechaFull)

                        select new DataCalculosFullView()
                        {
                            idData = d.idData,
                            fechacorta = d.fechacorta,
                            nombre_punto = d.nombre_punto,
                            sistema = d.sistema,
                            camion = d.camion,
                            franquiciado = d.franquiciado,
                            conductor = d.conductor,
                            estadopunto = d.estadopunto,
                            relleno = d.relleno,
                            cantidad = d.cantidad,
                            bruto = d.bruto,
                            Pago = d.Pago,
                            Margen = d.Margen,
                            MargenPorc = d.MargenPorc,
                            id_hoja_ruta = d.id_hoja_ruta,
                            id_punto_servicio = d.id_punto_servicio,
                            fechaFull = d.fechaFull
                        }).ToList();
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CobroClientesFranquiciaView> getListadoCobrosClientesFranquiciados()
        {

            return (from d in ctx.V_listado_CobrosCli_fran
                    select new CobroClientesFranquiciaView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        cantidad = d.cantidad,
                        valor = d.valor,
                        pagofranquiciado = d.pagofranquiciado,
                        ValorTotalDisposicion = d.ValorTotalDisposicion,
                        ValorTotal = d.ValorTotal,
                        margen = d.margen,
                        margenporcer = d.margenporcer
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="fechaini"></param>
        /// <param name="fechafinal"></param>
        /// <returns></returns>
        public string CalculoCobroClientesFranquiciados(string fechaini, string fechafinal)
        {

            string message = "";

            try
            {
                ctx.sp_listar_cobroclientes_franquiciados_tabla(fechaini, fechafinal);
                //message = "El Proceso de Calculo ha finalizado con éxito";
                //message = "0";
            }
            catch (Exception ex)
            {
                //message = ex.InnerException.Message.ToString();
                message = "El Proceso de Calculo ha finalizado con errores";
                //message = "1";
            }

            return message;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string CalculoPuntosFranquiciado()
        {
            string message = "";

            try
            {
                ctx.sp_franquiciado_detalle();
            }
            catch (Exception)
            {
                message = "El Proceso de Calculo ha finalizado con errores";
            }

            return message;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cerrar"></param>
        /// <returns></returns>
        public string CalculoCobroClientes(string cerrar)
        {

            string message = "";

            try
            {
                //ctx.sp_calculo_cobro_clientes(cerrar);
                message = "El Proceso de Calculo ha finalizado con éxito";
                message = "0";
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message.ToString();
                message = "El Proceso de Calculo ha finalizado con errores";
                message = "1";
            }

            return message;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="cerrar"></param>
        /// <param name="periodo"></param>
        /// <returns></returns>
        public string CalculoCobroClientesPeriodo(string cerrar, string periodo)
        {

            int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];

            string message = "";

            try
            {
                ctx.sp_cobro_clientes_periodo_NUEVO(cerrar, periodo, unidad);
                message = "El Proceso de Calculo ha finalizado con éxito";
                message = "0";
            }
            catch (Exception ex)
            {
                message = ex.InnerException.Message.ToString();
                message = "El Proceso de Calculo ha finalizado con errores";
                message = "1";
                WriteLog(ex);
            }

            return message;

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public void WriteLog(Exception ex)
        {
            using (System.IO.StreamWriter sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + @"\logSGS_" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt", true))
            {
                sw.WriteLine(string.Format("{0} - {1}:{2}:{3}:{4}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), ex.Message, ex.StackTrace, ex.Source, ex.InnerException));
                sw.Close();
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesView> getCobroClientes(string whenis)
        {
            int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_COBRO
                    join c in ctx.contratos on d.id_cliente equals c.id_cliente
                    join f in ctx.puntos_servicio on c.id_contrato equals f.id_contrato
                    where d.Fecha == whenis && f.estado_punto == 1 && d.id_unidad == unidad
                    select new CobroClientesView()
                    {
                        //id_pago = d.id_pago,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        Cantidad = d.cantidad,
                        Bano = d.bano,
                        //nombre_fantasia = d.nombre_fantasia,
                        Numero_Retiros = d.retiros,
                        Valor = d.valor,
                        //ValorTotal = d.Valor,
                        Fecha = d.Fecha
                    }).Distinct().ToList();

            //return (from d in ctx.SP_CALCULO_COBRO_CLIENTES_unidad().Where(d => d.Fecha == whenis && d.id_unidad == unidad)
            //        select new CobroClientesView()
            //        {
            //            id_pago = d.id_pago,
            //            id_cliente = d.id_cliente,
            //            rut_cliente = d.rut_cliente,
            //            razon_social = d.razon_social,
            //            nombre_fantasia = d.nombre_fantasia,
            //            Numero_Retiros = d.Numero_retiros,
            //            Valor = d.Valor,
            //            ValorTotal = d.ValorTotal,
            //            Fecha = d.Fecha
            //        }).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoServicioDescView> getPuntoServicioDescripcion(int idpuntoservicio)
        {

            //return (from d in ctx.V_PUNTO_SERVICIO_DESCRIPCION.Where(d => d.id_punto_servicio == idpuntoservicio)
            return (from d in ctx.V_PUNTO_SERVICIO_DESCRIPCION
                    join f in ctx.puntos_servicio on d.id_punto_servicio equals f.id_punto_servicio
                    where d.id_punto_servicio == idpuntoservicio && f.estado_punto == 1
                    select new PuntoServicioDescView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_tipo_servicio = d.id_tipo_servicio,
                        tipo_servicio = d.tipo_servicio,
                        nombre_punto = d.nombre_punto,
                        nombre_punto1 = d.nombre_punto1
                    }).Distinct().ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesView> getCobroClientesExcel(string whenis, int idcliente)
        {

            //return (from d in ctx.V_CALCULO_COBRO_CLIENTES.Where(d => d.Fecha == whenis)
            //.Where(d => d.id_cliente == idcliente)
            return (from d in ctx.V_CALCULO_COBRO_CLIENTES
                    join c in ctx.contratos on d.id_cliente equals c.id_cliente
                    join f in ctx.puntos_servicio on c.id_contrato equals f.id_contrato
                    where d.Fecha == whenis && f.estado_punto == 1 && d.id_cliente == idcliente
                    select new CobroClientesView()
                    {
                        id_pago = d.id_pago,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Numero_Retiros = d.Numero_retiros,
                        Valor = d.ValorTotal,
                        Fecha = d.Fecha
                    }).Distinct().ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesView> getCobroClientesCerrado(string whenis)
        {
            int id_unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_CALCULO_COBRO_CLIENTES_CERRADO.Where(d => d.Fecha == whenis && d.id_unidad == id_unidad)
                    select new CobroClientesView()
                    {
                        id_pago = d.id_pago,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Numero_Retiros = d.Numero_retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesView> getCobroClientesCerradoExcel(string whenis, int idcliente)
        {

            return (from d in ctx.V_CALCULO_COBRO_CLIENTES_CERRADO.Where(d => d.Fecha == whenis)
                                                                  .Where(d => d.id_cliente == idcliente)
                    select new CobroClientesView()
                    {
                        id_pago = d.id_pago,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Numero_Retiros = d.Numero_retiros,
                        Valor = d.Valor,
                        Fecha = d.Fecha
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesContratoView> getCobroContratoCerrado(string rutcliente, string whenis)
        {

            int id_unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_CALCULO_COBRO_CONTRATO_CERRADO.Where(d => d.rut_cliente == rutcliente && d.id_unidad == id_unidad)
                                                                  .Where(d => d.Fecha == whenis)
                    select new CobroClientesContratoView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        id_contrato = d.id_contrato,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.Sucursal,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesPuntosView> getCobroPuntos(string rutcliente, string whenis)
        {

            return (from d in ctx.V_CALCULO_COBRO_PUNTO_prob.Where(d => d.rut_cliente == rutcliente)
                                                       .Where(d => d.Fecha == whenis)
                    select new CobroClientesPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.sucursal,
                        fechaFull = d.fechaFull,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        Fecha = d.Fecha
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> getCalculoPuntos(string rutcliente, string whenis)
        {

            return (from d in ctx.V_CALCULO_PUNTO.Distinct().Where(d => d.rut_cliente == rutcliente)
                                                 .Where(d => d.Fecha == whenis)
                    select new CobroPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.Id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.Sucursal,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> GetCalculoPuntos2Cerrado(string rutcliente, string whenis)
        {
            int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            return (from d in ctx.V_CALCULO_PUNTO_2_CLOSED.Distinct().Where(d => d.rut_cliente == rutcliente)
                                                 .Where(d => d.Fecha == whenis)
                    select new CobroPuntosView()
                    {
                        id_cliente = d.Id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        id_punto_servicio = d.idpunto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> GetCalculoPuntos4(string rutcliente, string whenis)
        {
            //var count = 
            int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
            var list = (from d in ctx.V_CALCULO_COBRO_CLIENTES

                        join c in ctx.contratos on d.id_cliente equals c.id_cliente
                        join f in ctx.puntos_servicio on c.id_contrato equals f.id_contrato
                        where d.Fecha == whenis && d.rut_cliente == rutcliente

                        select new CobroPuntosView()

                        {
                            id_sucursal = c.id_sucursal,
                            id_contrato = c.id_contrato,
                            id_punto_servicio = f.id_punto_servicio,
                            rut_cliente = d.rut_cliente,
                            razon_social = d.razon_social,
                            nombre_fantasia = d.nombre_fantasia,
                            id_cliente = d.id_cliente,
                            //nombre_punto = d.nombre_punto,
                            Numero_Retiros = d.Numero_retiros,
                            // ValorTotal = d.ValorTotal,
                            Fecha = d.Fecha,
                            Valor = d.ValorTotal,
                        }).ToList();
            return list.GroupBy(x => new { x.nombre_punto/*,x.id_punto_servicio,x.id_contrato,x.id_sucursal*/ }).Select(x => new CobroPuntosView()
            {
                //id_sucursal = x.Key.id_sucursal,
                //id_contrato = x.Key.id_contrato,
                //id_punto_servicio = x.Key.id_punto_servicio,
                //rut_cliente = x.Key.rut_cliente,
                //razon_social = x.Key.razon_social,
                //nombre_fantasia = x.Key.nombre_fantasia,
                // id_cliente = x.Key.id_cliente,
                nombre_punto = x.Key.nombre_punto,
                //Numero_Retiros = x.Key.Numero_Retiros,
                // ValorTotal = x.Key.ValorTotal,
                // Fecha = x.Key.Fecha,
                //Valor = x.Key.ValorTotal,

            }).ToList();
            //return null;
        }
        public IEnumerable<CobroPuntosView> GetCalculoPuntos2(string rutcliente, string whenis)
        {
            return (from d in ctx.V_COBRO_DET_PUNTO.Where(d => d.rut_cliente == rutcliente)
                                                .Where(d => d.Fecha == whenis)
                        // join c in ctx.V_CALCULO_COBRO_CLIENTES on d.id_cliente equals c.id_cliente

                    select new CobroPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        //tipo_modalidad = c.tipo_modalidad,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        //nombre_fantasia = d.nombre_fantasia,
                        //Sucursal = d.Sucursal,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.retiros,
                        Valor = d.valor,
                        ValorTotal = d.valor,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();
            //return list.GroupBy(x => new {x.id_sucursal,x.Valor, x.Fecha, x.ValorTotal, x.id_contrato, x.id_punto_servicio, x.id_cliente, x.razon_social, x.rut_cliente, x.nombre_punto, x.Numero_Retiros }).Select(x => new CobroPuntosView()
            //{
            //    id_sucursal = x.Key.id_sucursal,

            //    id_contrato = x.Key.id_contrato,
            //    id_punto_servicio = x.Key.id_punto_servicio,
            //    rut_cliente = x.Key.rut_cliente,
            //    razon_social = x.Key.razon_social,
            //    //nombre_fantasia = x.Key.nombre_fantasia,
            //    id_cliente = x.Key.id_cliente,
            //    nombre_punto = x.Key.nombre_punto,
            //    Numero_Retiros = x.Key.Numero_Retiros,
            //    Valor = x.Key.Valor,
            //    ValorTotal = x.Key.ValorTotal,

            //    Fecha = x.Key.Fecha,

            //}).ToList();
            //return List.GroupBy(x => new { x.}) 
        }
        //public IEnumerable<CobroPuntosView> GetCalculoPuntos3(string rutcliente, string whenis)
        //{
        //    //var count = 
        //    int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
        //    return (from d in ctx.V_CALCULO_COBRO_CLIENTES_unidad

        //            join c in ctx.contratos on d.id_cliente equals c.id_cliente
        //            join f in ctx.puntos_servicio on c.id_contrato equals f.id_contrato
        //            where d.Fecha == whenis && d.rut_cliente == rutcliente

        //            select new CobroPuntosView()

        //            {
        //                rut_cliente = d.rut_cliente,
        //                razon_social = d.razon_social,
        //                nombre_fantasia = d.nombre_fantasia,
        //                id_cliente = d.id_cliente,
        //                nombre_punto = f.nombre_punto,
        //                Numero_Retiros = d.Numero_retiros,
        //                ValorTotal = d.ValorTotal,
        //                Fecha = d.Fecha,
        //                Valor = d.Valor,
        //            }).ToList();
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        /// 

        //public IEnumerable<CobroClientesView> getCobroClientes3(string rut, string whenis)
        //{
        //    int unidad = (int)HttpContext.Current.Session["unidad_seleccionada"];
        //    return (from d in ctx.V_CALCULO_COBRO_CLIENTES_unidad
        //            join c in ctx.contratos on d.id_cliente equals c.id_cliente
        //            join f in ctx.puntos_servicio on c.id_contrato equals f.id_contrato
        //            where d.Fecha == whenis && f.estado_punto == 1 && d.id_unidad == unidad && d.rut_cliente == rut
        //            select new CobroClientesView()
        //            {
        //                id_pago = d.id_pago,
        //                id_cliente = d.id_cliente,
        //                rut_cliente = d.rut_cliente,
        //                razon_social = d.razon_social,
        //                nombre_fantasia = d.nombre_fantasia,
        //                Numero_Retiros = d.Numero_retiros,
        //                Valor = d.Valor,
        //                ValorTotal = d.ValorTotal,
        //                Fecha = d.Fecha
        //            }).Distinct().ToList();
        //}


        public IEnumerable<CobroClientesPuntosView> getCobroPdfPtoServ(int idcliente, string whenis, int idpuntoservicio)
        {
            List<CobroClientesPuntosView> lst = (from d in ctx.V_CALCULO_COBRO_PUNTO_prob.Where(d => d.id_cliente == idcliente)
                                                    .Where(d => d.Fecha == whenis)
                                                    .Where(d => d.id_punto_servicio == idpuntoservicio)
                                                    .OrderBy(d => d.sorting)
                                                    .ThenBy(d => d.fechaFull)
                                                 select new CobroClientesPuntosView()
                                                 {
                                                     id_calculo = d.id_calculo,
                                                     id_hoja_ruta = d.id_hoja_ruta,
                                                     id_cliente = d.id_cliente,
                                                     rut_cliente = d.rut_cliente,
                                                     razon_social = d.razon_social,
                                                     nombre_fantasia = d.nombre_fantasia,
                                                     //Sucursal = d.sucursal,
                                                     fechaFull = d.fechaFull,
                                                     id_punto_servicio = d.id_punto_servicio,
                                                     nombre_punto = d.nombre_punto,
                                                     Numero_Retiros = d.Numero_Retiros,
                                                     Peso = d.peso,
                                                     Volteos = d.Volteos,
                                                     Valor = d.Valor,
                                                     Fecha = d.Fecha,
                                                     Orden = d.Orden,
                                                     tipo_modalidad = d.tipo_modalidad,
                                                     sorting = d.sorting,
                                                     glosa = d.glosa,
                                                     cobro_arriendo = d.cobro_arriendo,
                                                     conductor = d.conductor,
                                                     camion = d.camion,
                                                     como_se_calcula = d.como_se_calcula,
                                                     punto_servicio = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                                                       select (cp)).FirstOrDefault(),
                                                     codigo_externo = (from cp in ctx.hoja_ruta_detalle.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                                                       select cp.codigo_externo).FirstOrDefault(),
                                                     cobro_por_m3 = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                                                     select cp.cobro_por_m3).FirstOrDefault(),
                                                     cobro_por_peso = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                                                       select cp.cobro_por_peso).FirstOrDefault()
                                                 }).ToList();
            return lst;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesPuntosView> getCobroPuntosCerrado(string rutcliente, string whenis)
        {

            return (from d in ctx.V_CALCULO_COBRO_PUNTO_CERRADO.Where(d => d.rut_cliente == rutcliente)
                                                               .Where(d => d.Fecha == whenis)
                    select new CobroClientesPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.sucursal,
                        fechaFull = d.fechaFull,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        Fecha = d.Fecha,
                        Orden = d.Orden,
                        como_se_calcula = d.como_se_calcula
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutcliente"></param>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public IEnumerable<CobroPuntosView> getCalculoPuntosCerrado(string rutcliente, string whenis)
        {

            return (from d in ctx.V_CALCULO_PUNTO_CERRADO.Where(d => d.rut_cliente == rutcliente)
                                                         .Where(d => d.Fecha == whenis)
                    select new CobroPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_cliente = d.Id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.Sucursal,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Valor = d.Valor,
                        ValorTotal = d.ValorTotal,
                        Fecha = d.Fecha,
                        id_contrato = d.id_contrato,
                        id_sucursal = d.id_sucursal
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClientesPuntosView> getCobroPdfCerradoPtoServ(int idcliente, string whenis, int idpuntoservicio)
        {
            return (from d in ctx.V_CALCULO_COBRO_PUNTO_CERRADO.Where(d => d.id_cliente == idcliente)
                    .Where(d => d.Fecha == whenis)
                    .Where(d => d.id_punto_servicio == idpuntoservicio)
                    .OrderBy(d => d.sorting)
                    .ThenBy(d => d.fechaFull)
                    select new CobroClientesPuntosView()
                    {
                        id_calculo = d.id_calculo,
                        id_hoja_ruta = d.id_hoja_ruta,
                        id_cliente = d.id_cliente,
                        rut_cliente = d.rut_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        Sucursal = d.sucursal,
                        fechaFull = d.fechaFull,
                        id_punto_servicio = d.id_punto_servicio,
                        nombre_punto = d.nombre_punto,
                        Numero_Retiros = d.Numero_Retiros,
                        Peso = d.peso,
                        Volteos = d.Volteos,
                        Valor = d.Valor,
                        Fecha = d.Fecha,
                        Orden = d.Orden,
                        tipo_modalidad = d.tipo_modalidad,
                        sorting = d.sorting,
                        glosa = d.glosa,
                        cobro_arriendo = d.cobro_arriendo,
                        conductor = d.conductor,
                        camion = d.camion,
                        como_se_calcula = d.como_se_calcula,
                        punto_servicio = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select (cp)).FirstOrDefault(),
                        codigo_externo = (from cp in ctx.hoja_ruta_detalle.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select cp.codigo_externo).FirstOrDefault(),
                        cobro_por_m3 = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                        select cp.cobro_por_m3).FirstOrDefault(),
                        cobro_por_peso = (from cp in ctx.puntos_servicio.Where(x => x.id_punto_servicio == d.id_punto_servicio)
                                          select cp.cobro_por_peso).FirstOrDefault()
                    }).ToList();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idhojaruta"></param>
        /// <returns></returns>
        public IEnumerable<ConductorCamionView> getConductorCamion(int idhojaruta)
        {

            return (from d in ctx.V_CONDUCTOR_CAMION.Where(d => d.id_hoja_ruta == idhojaruta)

                    select new ConductorCamionView()
                    {
                        id_conductor = d.id_conductor,
                        id_camion = d.id_camion,
                        nombre_conductor = d.nombre_conductor,
                        rut_conductor = d.rut_conductor,
                        camion = d.camion,
                        modelo_camion = d.modelo_camion,
                        patente = d.patente
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<EquipoView> getEquipo(int idpuntoservicio)
        {

            return (from d in ctx.V_EQUIPO.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new EquipoView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_equipo = d.id_equipo,
                        Equipo = d.Equipo,
                        nom_nemo = d.nom_nemo
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoServicioDireView> getPtoServicioDire(int idpuntoservicio)
        {

            return (from d in ctx.V_PUNTO_SERVICIO_DIRE.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new PuntoServicioDireView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        sucursal = d.sucursal,
                        zona = d.zona,
                        ciudad = d.ciudad,
                        nombre_comuna = d.nombre_comuna,
                        pais = d.pais,
                        rubro = d.rubro,
                        id_contrato = d.id_contrato,
                        estado = d.estado
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <returns></returns>
        public IEnumerable<ClientesView> getClienteData(int idcliente)
        {

            return (from d in ctx.clientes.Where(d => d.id_cliente == idcliente)

                    select new ClientesView()
                    {
                        id_cliente = d.id_cliente,
                        razon_social = d.razon_social,
                        nombre_fantasia = d.nombre_fantasia,
                        rut_cliente = d.rut_cliente
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenis"></param>
        /// <returns></returns>
        public int getProcesoCerrado(string whenis)
        {
            int idUnidad = (int)HttpContext.Current.Session["unidad_seleccionada"];

            return (from p in ctx.calculo_realizado
                    where p.Fecha == whenis && p.id_unidad == idUnidad
                    select p.Cerrado).FirstOrDefault();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienDisposicionView> getDisposicion(int idcliente, string whenis, int idpuntoservicio)
        {

            return (from d in ctx.V_DISPOSICION.Where(d => d.id_punto_servicio == idpuntoservicio && d.Fecha == whenis)

                    select new CobroClienDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_cliente = d.id_cliente,
                        Fecha = d.Fecha,
                        Disposicion = d.Disposicion,
                        Valor = d.Valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienteDisposicionView> getDisposicionExcepcion(int idpuntoservicio)
        {

            return (from d in ctx.calculo_cobro_disposicion.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new CobroClienteDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        Disposicion = d.Disposicion,
                        CobroTonelada = d.CobroTonelada,
                        Valor = d.Valor,
                        valoruf = d.valoruf
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <param name="glosa"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienteDisposicionView> getDisposicionExcepcion(int idpuntoservicio, string glosa)
        {

            return (from d in ctx.calculo_cobro_disposicion.Where(d => d.id_punto_servicio == idpuntoservicio && d.glosa.ToUpper().Trim() == glosa.ToUpper().Trim())

                    select new CobroClienteDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        Disposicion = d.Disposicion,
                        CobroTonelada = d.CobroTonelada,
                        Valor = d.Valor,
                        valoruf = d.valoruf
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idcliente"></param>
        /// <param name="whenis"></param>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<CobroClienDisposicionView> getDisposicionCerrado(int idcliente, string whenis, int idpuntoservicio)
        {

            return (from d in ctx.V_DISPOSICION_CERRADO.Where(d => d.id_cliente == idcliente)
                                                       .Where(d => d.Fecha == whenis)
                                                       .Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new CobroClienDisposicionView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_cliente = d.id_cliente,
                        Fecha = d.Fecha,
                        Disposicion = d.Disposicion,
                        Valor = d.Valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <param name="idTipoModalidad"></param>
        /// <returns></returns>
        public IEnumerable<PuntoCobroToneView> getPuntoCobroTonelada(int idpuntoservicio, int idTipoModalidad)
        {

            return (from d in ctx.V_PTO_SERVICIO_COBRO_TONELADA.Where(d => d.id_punto_servicio == idpuntoservicio && d.id_tipo_modalidad == idTipoModalidad)

                    select new PuntoCobroToneView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_modelo = d.id_modelo,
                        valor = d.valor
                    }).ToList();

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="idpuntoservicio"></param>
        /// <returns></returns>
        public IEnumerable<PuntoNumEquiposView> getPuntoNumEquipos(int idpuntoservicio)
        {

            return (from d in ctx.V_PUNTO_NUM_EQUIPOS.Where(d => d.id_punto_servicio == idpuntoservicio)

                    select new PuntoNumEquiposView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        cant_equipos = d.cant_equipos
                    }).ToList();

        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="whenIs"></param>
        /// <returns></returns>
        public int ReversarPeriodo(string whenIs)
        {
            {
                int idUnidad = (int)HttpContext.Current.Session["unidad_seleccionada"];

                return ctx.sp_reversa_cobro_periodo(whenIs, idUnidad);
            }
        }
    }

}
