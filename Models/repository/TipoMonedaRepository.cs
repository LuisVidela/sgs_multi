﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoMonedaRepository 
    {
        private sgsbdEntities ctx;

        public TipoMonedaRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }

        public IEnumerable<object> getTipoMonedaForList()
        {

            return ctx.tipo_moneda.Select(m => new
            {
                value = m.id_moneda,
                text = m.moneda

            }).ToList();

        }
    }
}