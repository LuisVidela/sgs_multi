﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class TipoModalidadRepository
    {
         private sgsbdEntities ctx;


         public TipoModalidadRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<tipo_modalidad> GetTipoModalidades()
        {
            return ctx.tipo_modalidad.ToList();
        }


        public IEnumerable<object> GetTipoModalidadesForList()
        {
            return (from tm in ctx.tipo_modalidad
                    select new
                    {
                        text = tm.tipo_modalidad1,
                        value = tm.id_tipo_modalidad
                    }).OrderBy(m => m.text).ToList(); 
        }

       public IEnumerable<object> GetTipoModalidadesForList2(int id)
       {
           return (from tm in ctx.tipo_modalidad
                       join mcd in ctx.modelo_cobro_detalle on tm.id_tipo_modalidad equals mcd.id_tipo_modalidad
                       join mc in ctx.modelo_cobro on mcd.id_modelo_cobro equals mc.id_modelo
                       join ps in ctx.puntos_servicio on mc.id_punto_servicio equals ps.id_punto_servicio
                       where  id == mc.id_modelo
                   select new
                   {
                       text = tm.tipo_modalidad1,
                       value = tm.id_tipo_modalidad
                   }).OrderBy(m => m.text).ToList();
       }

        public IEnumerable<object> getTipoModalidad1(int id)
        {
            var x = ctx.modelo_cobro.Where(u => u.id_modelo == id).FirstOrDefault().id_punto_servicio;

            return (from ps in ctx.puntos_servicio
                    from tm in ctx.tipo_modalidad
                    where ps.id_punto_servicio == x && ((ps.cobro_por_banos == tm.bano && tm.bano == true)
                    || (ps.cobro_por_cantidad == tm.cantidad && tm.cantidad == true)
                    || (ps.cobro_por_peso == tm.peso && tm.peso == true)
                    || (ps.cobro_por_m3 == tm.volumen && tm.volumen == true))
                    select new 
                    {
                        value = tm.id_tipo_modalidad,
                        text = tm.tipo_modalidad1
                    }).OrderBy(m => m.text).ToList();

        }

        public tipo_modalidad GetTipoModalidadByID(int tm)
        {
            return ctx.tipo_modalidad.Find(tm);
        }

        public void InsertTipoModalidad(tipo_modalidad tm)
        {
             ctx.tipo_modalidad.Add(tm);
        }

        public void DeleteTipoModalidad(int tm)
        {
            tipo_modalidad tmo=ctx.tipo_modalidad.Find(tm);
            ctx.tipo_modalidad.Remove(tmo);
        }

        public void UpdateTipoModalidad(tipo_modalidad tm)
        {
            ctx.Entry(tm).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}