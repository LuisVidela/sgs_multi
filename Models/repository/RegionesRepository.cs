﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class RegionesRepository:IDisposable
    {
         private sgsbdEntities ctx;


        public RegionesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<regiones> GetRegiones()
        {
            return ctx.regiones.OrderBy(region=>region.id_region).ToList();
        }

        public regiones GetRegionByID(int c)
        {
            return ctx.regiones.Find(c);
        }

        public void InsertRegion(regiones c)
        {
             ctx.regiones.Add(c);
        }

        public void DeleteRegion(int c)
        {
            regiones region=ctx.regiones.Find(c);
            ctx.regiones.Remove(region);
        }

        public void UpdateRegion(regiones c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}