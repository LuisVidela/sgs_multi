﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class EmpresasRepository
    {
        private sgsbdEntities ctx;


        public EmpresasRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<empresas> GetEmpresas()
        {
            return ctx.empresas.ToList();
        }

        public IEnumerable<object> GetEmpresasForList() {

            return ctx.empresas.Select(x => new
            {
                text=x.empresa,
                value=x.id_empresa
            }).ToList();
        
        }

        public empresas GetEmpresaByID(int e)
        {
            return ctx.empresas.Find(e);
        }

        public void InsertEmpresa(empresas e)
        {
             ctx.empresas.Add(e);
        }

        public void DeleteEmpresa(int e)
        {
            empresas empresa=ctx.empresas.Find(e);
            ctx.empresas.Remove(empresa);
        }

        public void UpdateEmpresa(empresas e)
        {
            ctx.Entry(e).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}