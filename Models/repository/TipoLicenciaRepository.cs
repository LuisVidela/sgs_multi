﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoLicenciaRepository
    {
        private sgsbdEntities ctx;

        public TipoLicenciaRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
    
         }

        public IEnumerable<tipo_licencia> GetTipoLicencias()
        {
            return ctx.tipo_licencia.ToList();
        }

        public tipo_licencia GetTipoLicenciaByID(int tl)
        {
            return ctx.tipo_licencia.Find(tl);
        }

        public void InsertTipoLicencia(tipo_licencia tl)
        {
            ctx.tipo_licencia.Add(tl);
        }

        public void DeleteTipoLicencia(int tl)
        {
            tipo_licencia c = ctx.tipo_licencia.Find(tl);
            ctx.tipo_licencia.Remove(c);
        }

        public void UpdateTipoLicencia(tipo_licencia tl)
        {
            ctx.Entry(tl).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}