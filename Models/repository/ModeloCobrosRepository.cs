﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Linq.SqlClient;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class ModeloCobrosRepository
    {
         private sgsbdEntities ctx;
         public string ERROR = "";

        public ModeloCobrosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }

        public IEnumerable<PuntoSucursalView> getSucursal(int id_punto_servicio)
        {

            return (from d in ctx.V_PUNTO_SUCURSAL.Where(d => d.id_punto_servicio == id_punto_servicio)
                    select new PuntoSucursalView()
                    {
                        id_punto_servicio = d.id_punto_servicio,
                        id_contrato = d.id_contrato,
                        razon_social = d.razon_social,
                        sucursal = d.sucursal
                    }).ToList();

        }

        public IEnumerable<TipoModalidadView> getTipoModalidad()
        {

            return (from d in ctx.V_TIPO_MODALIDAD
                    select new TipoModalidadView()
                    {
                        id_tipo_modalidad = d.id_tipo_modalidad,
                        tipo_modalidad = d.tipo_modalidad
                    }).ToList();

        }

        public IEnumerable<TipoModalidadView> getTipoModalidad1(int id)
        {

            return (from ps in ctx.puntos_servicio
                    from tm in ctx.tipo_modalidad
                    where ps.id_punto_servicio == id && ((ps.cobro_por_banos == tm.bano && tm.bano == true)
                    || (ps.cobro_por_cantidad == tm.cantidad && tm.cantidad == true)
                    || (ps.cobro_por_peso == tm.peso && tm.peso == true)
                    || (ps.cobro_por_m3 == tm.volumen && tm.volumen == true))
                    select new TipoModalidadView()
                    {
                        id_tipo_modalidad = tm.id_tipo_modalidad,
                        tipo_modalidad = tm.tipo_modalidad1
                    }).ToList();
                    
        }

            public Int32 ModeloCobroAgregar(int id_modelov, string modelov, int id_tipo_modelov, int id_punto_serviciov, int id_new_createdv)
        {

            ERROR = "";

            System.Data.Objects.ObjectParameter IdCreated = new System.Data.Objects.ObjectParameter("id_new_created", typeof(Int32));

            try
            {                

               // ctx.sp_modelo_cobro_crear(id_modelov, modelov, id_tipo_modelov, id_punto_serviciov, IdCreated);

                id_new_createdv = Convert.ToInt32(IdCreated.Value);

            }
            catch (Exception ex)
            {                
                ERROR = ex.InnerException.Message;
                if (ERROR == "Modelo existente" )
                {
                    id_new_createdv = -777;
                }
                else
                {
                    id_new_createdv = -999;
                }
            }
            finally
            {
                ctx.Dispose();
            }

            return id_new_createdv;

        }

        public IEnumerable<modelo_cobro> GetModeloCobros()
        {
            return ctx.modelo_cobro.ToList();
        }
        public ModeloCobrosView GetModeloCobrosTabla(int unidad)
        {

            int[] indices = (from cr in ctx.contratos where cr.id_unidad == unidad select cr.id_contrato).Distinct().ToArray();

            var datos = ctx.modelo_cobro
                        .Join(ctx.puntos_servicio, x => x.id_punto_servicio, ps => ps.id_punto_servicio, (x, ps) => new { x, ps })
                        .Join(ctx.contratos, x => x.ps.id_contrato, cr => cr.id_contrato, (x, cr) => new { x,cr})
                        .Where(x=>indices.Contains(x.cr.id_contrato))
                        .Select(x=>x.x.x)
                       .ToList();
            
            ModeloCobrosView model = new ModeloCobrosView();
            model.tabla = new List<tablaModeloCobrosView>();
            foreach (var item in datos)
            {
                tablaModeloCobrosView row = new tablaModeloCobrosView();
                row.idModelo = item.id_modelo;
                row.nombreModelo = item.modelo;
                row.tipoModelo = item.tipo_modelo.tipo_modelo1;
                if (item.id_punto_servicio != null)
                {
                    row.nombrePuntoServicio = ctx.puntos_servicio.Where(u => u.id_punto_servicio == item.id_punto_servicio.Value).FirstOrDefault().nombre_punto;
                }
                else
                {
                    row.nombrePuntoServicio = "No asignado";
                }
                model.tabla.Add(row);
            }
            return model;
        }

        public modelo_cobro GetModeloCobroByID(int m)
        {
            return ctx.modelo_cobro.Find(m);
        }

        public void InsertModeloCobro(modelo_cobro m)
        {
             ctx.modelo_cobro.Add(m);
        }

        public void DeleteModeloCobro(int m)
        {
            modelo_cobro modelo=ctx.modelo_cobro.Find(m);
            ctx.modelo_cobro.Remove(modelo);
        }

        public void UpdateModeloCobro(modelo_cobro m)
        {
            ctx.Entry(m).State = System.Data.EntityState.Modified;
        }

        public IEnumerable<object> getForAutocomplete(string search , int tipo_servicio) {

            var data = (from ps in ctx.puntos_servicio.Where(w => w.nombre_punto.Contains(search))
                        join
                            c in ctx.contratos on ps.id_contrato equals c.id_contrato
                        join
                            tc in ctx.tipo_contrato on c.id_tipo_contrato equals tc.id_tipo_contrato
                        where tc.id_tipo_contrato == tipo_servicio
                        select new { 
                        id=ps.id_punto_servicio,
                        label=ps.nombre_punto,
                        value=ps.nombre_punto
                        }).OrderBy(m => m.label).ToList();

            return data;
        }


        public IEnumerable<object> getTipoModeloForList()
        {

            return ctx.tipo_modelo.Select(x => new
            {
                text = x.tipo_modelo1,
                value = x.id_tipo_modelo
            }).ToList();

        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}