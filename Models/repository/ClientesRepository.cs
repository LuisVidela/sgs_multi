﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using SGS.Models.ShadowBox;



namespace SGS.Models.repository
{
    public class ClientesRepository : IClientesRepository
    {
        private sgsbdEntities ctx;


        public ClientesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        }
        public IEnumerable<clientes> GetClientes()
        {
            return ctx.clientes.ToList();
        }

        public IEnumerable<object> GetClientesForList2(int id_unidad)
        {

            return ctx.clientes.Where(x => x.id_unidad == id_unidad).Select(x => new
            {
                id_cliente = x.id_cliente,
                razon_social = x.razon_social,
            }).OrderBy(x => x.razon_social);

        }
        public IEnumerable<object> GetClientesForList()
        {

            return ctx.clientes.Select(x => new
            {
                id_cliente = x.id_cliente,
                razon_social = x.razon_social,
            }).OrderBy(x => x.razon_social);

        }

        public IEnumerable<object> GetClientesForListByUN(int unidad)
        {

            return ctx.clientes.Select(x => new
            {
                id_cliente = x.id_cliente,
                razon_social = x.razon_social,
                id_unidad = x.id_unidad
            }).Where(x => x.id_unidad == unidad).OrderBy(x => x.razon_social).ToList();

        }


        public IEnumerable<ClientesView> GetClientesView(int unidad)
        {
            var data = (from cr in this.ctx.clientes.Where(cr => cr.id_unidad == unidad)
                        join ca in ctx.categoria on cr.id_categoria equals ca.id_categoria_ into cate
                        from categ in cate.DefaultIfEmpty()
                        select new ClientesView()
                {
                    id_cliente = cr.id_cliente,
                    direccion_cliente = cr.direccion_cliente,
                    nombre_fantasia = cr.nombre_fantasia.Substring(0, 30),
                    razon_social = cr.razon_social.Substring(0, 30),
                    rut_cliente = cr.rut_cliente.Substring(0, 30),
                    categoria = categ.categoria_nombre,
                }).ToList();
            return data.ToList();
        }

        public IEnumerable<ClientesView> GetClientesView()
        {

            var data = (from cr in this.ctx.clientes
                        join ca in ctx.categoria.DefaultIfEmpty() on cr.id_categoria equals ca.id_categoria_
                        select new ClientesView()
                        {
                            id_cliente = cr.id_cliente,
                            direccion_cliente = cr.direccion_cliente,
                            nombre_fantasia = cr.nombre_fantasia.Substring(0, 30),
                            razon_social = cr.razon_social.Substring(0, 30),
                            rut_cliente = cr.rut_cliente.Substring(0, 30),

                        }).ToList();
            return data.ToList();
        }

        public IEnumerable<ClientesExcelView> GetClientesExcel()
        {

            return (from cl in ctx.clientes
                    join p in ctx.paises.DefaultIfEmpty() on cl.id_pais equals p.id_pais
                    join r in ctx.regiones.DefaultIfEmpty() on cl.id_region equals r.id_region
                    join cd in ctx.ciudades.DefaultIfEmpty() on cl.id_ciudad equals cd.id_ciudad
                    join co in ctx.comunas.DefaultIfEmpty() on cl.id_comuna equals co.id_comuna
                    join ca in ctx.categoria.DefaultIfEmpty() on cl.id_categoria equals ca.id_categoria_
                  //  join u in ctx.unidades_negocio.DefaultIfEmpty() on cl.id_unidad equals u.id_unidad

                    orderby cl.id_cliente descending
                    select new ClientesExcelView()
                    {
                        celular_contacto = cl.celular_contacto,
                        ciudad = cd.ciudad,
                        comuna = co.nombre_comuna,
                        direccion_cliente = cl.direccion_cliente,
                        email_contacto = cl.email_contacto,
                        fono_comercial = cl.fono_comercial,
                        fono_contacto = cl.fono_contacto,
                        giro_comercial = cl.giro_comercial,
                        id_ciudad = cl.id_ciudad,
                        id_cliente = cl.id_cliente,
                        id_comuna = cl.id_comuna,
                        id_pais = cl.id_pais,
                        id_region = cl.id_region,
                    //    id_unidad = cl.id_unidad,
                        nombre_contacto = cl.nombre_contacto,
                        nombre_fantasia = cl.nombre_fantasia,
                        pais = p.pais,
                        razon_social = cl.razon_social,
                        region = r.nombre_region,
                        rut_cliente = cl.rut_cliente,
                        categoria =  ca.categoria_nombre,
                     //   unidad = u.unidad
                    }
                    ).ToList();


        }


        public clientesShadowBoxView GetClienteShadowBox(int id)
        {

            var join = (from c in ctx.clientes.Where(a => a.id_cliente == id)
                        join
                            p in ctx.paises on c.id_pais equals p.id_pais
                        join
                            co in ctx.comunas on c.id_comuna equals co.id_comuna
                        join
                            r in ctx.regiones on c.id_region equals r.id_region
                        join
                            cd in ctx.ciudades on c.id_ciudad equals cd.id_ciudad
                        join
                            ca in ctx.categoria on c.id_categoria equals ca.id_categoria_
                        into cate
                        from categ in cate.DefaultIfEmpty()
                        // joinu in ctx.unidades_negocio on c.id_unidad equals u.id_unidad
                        join
                           b in ctx.rubros on c.id_rubro equals b.id_rubro
                        into res
                        from rs in res.DefaultIfEmpty()

                        select new clientesShadowBoxView()
                        {
                            rut_cliente = c.rut_cliente,
                            razon_social = c.razon_social,
                            pais = p.pais,
                            nombre_region = r.nombre_region,
                            ciudad = cd.ciudad,
                            nombre_comuna = co.nombre_comuna,
                            direccion_cliente = c.direccion_cliente,
                            fono_comercial = c.fono_comercial,
                            giro_comercial = c.giro_comercial,
                            nombre_contacto = c.nombre_contacto,
                            fono_contacto = c.fono_contacto,
                            celular_contacto = c.celular_contacto,
                            email_contacto = c.email_contacto,
                            categoria = categ.categoria_nombre,
                           // unidad = u.unidad,
                            rubro = rs.rubro,

                        }).SingleOrDefault();

            return join;

        }

        public clientes GetClienteByID(int cl)
        {
            return ctx.clientes.Find(cl);
        }

        public void InsertCliente(clientes cl)
        {
            ctx.clientes.Add(cl);
        }

        public void DeleteCliente(int cl)
        {
            clientes cl2 = ctx.clientes.Find(cl);
            ctx.clientes.Remove(cl2);
        }

        public void UpdateCliente(clientes cl)
        {
            ctx.Entry(cl).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        protected bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<object> GetClientesForSelect()
        {

            IEnumerable<object> data = ctx.clientes.Select(x => new
            {
                Value = x.id_cliente,
                Text = x.razon_social
            }).OrderBy(x => x.Text).ToList();

            IEnumerable<object> def = new object[]{
               new { Text="Seleccione un Valor",Value="0"}
           
           };

            IEnumerable<object> final_data = def.Concat(data);


            return final_data;
        }

        public IEnumerable<object> GetClientesForSelectByUnidad(int unidad)
        {

            IEnumerable<object> data = ctx.clientes.Select(x => new
            {
                Value = x.id_cliente,
                Text = x.razon_social,
                id_unidad = x.id_unidad
            }).Where(x => x.id_unidad == unidad).OrderBy(x => x.Text).ToList();





            IEnumerable<object> final_data = data;


            return final_data;
        }


        public clientesShadowBoxView GetClienteByRazonSocial(string razon_social)
        {
            var cliente = (from c in ctx.clientes.Where(a => a.razon_social == razon_social)
                        
                     
                        select new clientesShadowBoxView()
                        {
                           
                            rut_cliente = c.rut_cliente,
                            razon_social = c.razon_social,
                           
                           
                        }).SingleOrDefault();

            return cliente;
        }
        

    }
}