﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;
using SGS.Models.excelView;
using SGS.Models.ShadowBox;


namespace SGS.Models.repository
{
    public class DescuentosRepository
    {
        private sgsbdEntities ctx;


        public DescuentosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        }
        public IEnumerable<descuento> Getdescuentos()
        {
            return ctx.descuento.ToList();
        }

        public IEnumerable<object> GetDescuentosForList() {

            return ctx.descuento.Select(x => new
            {
                id_cliente = x.id_descuento,
                id_franquiciado=x.id_franquiciado,
                iva=x.iva,
                neto=x.neto,
                total=x.total
            }).ToList();
        
        }


        //public DescuentosView GetDescuentoView()
        //{
        //    return ctx.descuento.Select(x => new DescuentosView()
        //    {
        //        id_descuento=x.id_descuento,
        //        id_franquiciado=1,
        //        id_tipo_descuento=1,
        //        neto=1,
        //        iva=1,
        //        total=1

        //    }).ToList();
        //}

        //public IEnumerable<DescuentosView> GetDescuentosExcel() {

            //return (from cl in ctx.clientes
            //        join p in ctx.paises.DefaultIfEmpty() on cl.id_pais equals p.id_pais
            //        join r in ctx.regiones.DefaultIfEmpty() on cl.id_region equals r.id_region
            //        join cd in ctx.ciudades.DefaultIfEmpty() on cl.id_ciudad equals cd.id_ciudad
            //        join co in ctx.comunas.DefaultIfEmpty() on cl.id_comuna equals co.id_comuna
            //        join u in ctx.unidades_negocio.DefaultIfEmpty() on cl.id_unidad equals u.id_unidad
            //        orderby cl.id_cliente descending
            //        select new ClientesExcelView()
            //        {
            //             celular_contacto=cl.celular_contacto,
            //              ciudad=cd.ciudad,
            //               comuna=co.nombre_comuna,
            //                direccion_cliente=cl.direccion_cliente,
            //                 email_contacto=cl.email_contacto,
            //                  fono_comercial=cl.fono_comercial,
            //                   fono_contacto=cl.fono_contacto,
            //                    giro_comercial=cl.giro_comercial,
            //                     id_ciudad=cl.id_ciudad,
            //                      id_cliente=cl.id_cliente,
            //                       id_comuna=cl.id_comuna,
            //                        id_pais=cl.id_pais,
            //                         id_region=cl.id_region,
            //                          id_unidad=cl.id_unidad,
            //                           nombre_contacto=cl.nombre_contacto,
            //                            nombre_fantasia=cl.nombre_fantasia,
            //                             pais=p.pais,
            //                              razon_social=cl.razon_social,
            //                               region=r.nombre_region,
            //                                rut_cliente=cl.rut_cliente,
            //                                 unidad=u.unidad
            //        }
            //        ).ToList();
        
        
        //}


        //public clientesShadowBoxView GetClienteShadowBox(int id) {

        //    var join = (from c in ctx.clientes.Where(a=>a.id_cliente==id)
        //                join
        //                    p in ctx.paises on c.id_pais equals p.id_pais
        //                join
        //                    co in ctx.comunas on c.id_comuna equals co.id_comuna
        //                join
        //                    r in ctx.regiones on c.id_region equals r.id_region
        //                join
        //                    cd in ctx.ciudades on c.id_ciudad equals cd.id_ciudad
        //                join
        //                    u in ctx.unidades_negocio on c.id_unidad equals u.id_unidad
        //                join
        //                   b in ctx.rubros on c.id_rubro equals b.id_rubro  
        //                into res
        //                from rs in res.DefaultIfEmpty()
                        
        //                select new clientesShadowBoxView()
        //                {
        //                    rut_cliente = c.rut_cliente,
        //                    razon_social = c.razon_social,
        //                    pais = p.pais,
        //                    nombre_region = r.nombre_region,
        //                    ciudad = cd.ciudad,
        //                    nombre_comuna = co.nombre_comuna,
        //                    direccion_cliente = c.direccion_cliente,
        //                    fono_comercial = c.fono_comercial,
        //                    giro_comercial = c.giro_comercial,
        //                    nombre_contacto=c.nombre_contacto,
        //                    fono_contacto=c.fono_contacto,
        //                    celular_contacto=c.celular_contacto,
        //                    email_contacto=c.email_contacto,
        //                    unidad = u.unidad,
        //                    rubro=rs.rubro
        //                }).SingleOrDefault();

        //    return join;

        //}

        public descuento GetDescuentoByID(int cl)
        {
            return ctx.descuento.Find(cl);
        }

        public void InsertDescuento(descuento cl)
        {
            ctx.descuento.Add(cl);
        }

        public void DeleteDescuento(int cl)
        {
            descuento cl2 = ctx.descuento.Find(cl);
            ctx.descuento.Remove(cl2);
        }

        public void UpdateDescuento(descuento cl)
        {
            ctx.Entry(cl).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        protected bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


    }
}