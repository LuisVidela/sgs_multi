﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class PaisesRepository:IDisposable
    {
         private sgsbdEntities ctx;


        public PaisesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<paises> GetPaises()
        {
            return ctx.paises.ToList();
        }

        public paises GetPaisByID(int c)
        {
            return ctx.paises.Find(c);
        }

        public void InsertPais(paises c)
        {
             ctx.paises.Add(c);
        }

        public void DeletePais(int c)
        {
            paises pais=ctx.paises.Find(c);
            ctx.paises.Remove(pais);
        }

        public void UpdatePais(paises c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}