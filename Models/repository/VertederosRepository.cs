﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository;
using SGS.Models;

namespace SGS.Models.repository
{
    public class VertederosRepository:IDisposable
    {

        private sgsbdEntities ctx;

        public VertederosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        
        }

        private bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }


        public IEnumerable<object> GetVertederosForSelect()
        {

            IEnumerable<object> data = ctx.vertederos.Select(x => new
            {
                Value = x.id_vertedero,
                Text = x.vertedero
            }).OrderBy(x => x.Text).ToList();

            IEnumerable<object> def = new object[]{
               new { Text="Seleccione un Valor",Value="0"}
           
           };

            IEnumerable<object> final_data = def.Concat(data);


            return final_data;
        }


        public IEnumerable<object> GetVertederosByUnidad(int unidad)
        {

            IEnumerable<object> data = ctx.vertederos.Select(x => new
            {
                Value = x.id_vertedero,
                Text = x.vertedero,
                idUnidad = x.id_unidad 

            }).Where(x => x.idUnidad == unidad).OrderBy(x => x.Text).ToList();


            IEnumerable<object> final_data = data;


            return final_data;
        }

        public vertederos GetVertederoById(int id)
        {
            return this.ctx.vertederos.Find(id);
        }
    }
}