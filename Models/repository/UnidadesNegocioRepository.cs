﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class UnidadesNegocioRepository:IDisposable
    {

        private sgsbdEntities ctx;

        /// <summary>
        /// Constructor de repositorio de unidades de negocio
        /// </summary>
        /// <param name="ctx">Contexto de conexión de EF</param>
        public UnidadesNegocioRepository(sgsbdEntities ctx)
        {
            this.ctx = ctx;
        }


        /// <summary>
        /// Obtiene todas las unidades de negocio desde base de datos
        /// </summary>
        /// <returns>Lista de unidades de negocio</returns>
        public IEnumerable<unidades_negocio> GetunidadesNegocio()
        {
            return ctx.unidades_negocio.ToList();
        }


        /// <summary>
        /// Obtiene todas las unidades de negocio de la vista desde base de datos
        /// </summary>
        /// <returns>Lista de vista resultante de unidades de negocio</returns>
        public IEnumerable<UnidadesNegocioView> GetunidadesNegocioView()
        {
            var data = from un in ctx.unidades_negocio
                       join e in ctx.empresas on un.id_empresa equals e.id_empresa
                       into res
                       from r in res.DefaultIfEmpty()
                       select new UnidadesNegocioView()
                       {
                           id_unidad = un.id_unidad,
                           empresa = r.empresa,
                           estado = un.estado,
                           unidad = un.unidad,
                           represenante_legal = un.represenante_legal,
                           numero_resolucion = un.numero_resolucion,
                           rut_rep_legal = un.rut_rep_legal
                       };

            return data.ToList();
        }



        public unidades_negocio GetUnidadNegocioByID(int un)
        {
            return ctx.unidades_negocio.Find(un);
        }

        public void InsertUnidadNegocio(unidades_negocio un)
        {
            ctx.unidades_negocio.Add(un);
        }

        public void DeleteUnidadNegocio(int un)
        
        {
            var centros = ctx.detalle_centro_responsabilidad.Where(u => u.id_unidad_negocio == un).ToList();
            foreach (var item in centros)
            {
                item.fecha_eliminacion = DateTime.Now;
                ctx.SaveChanges();
            }
            unidades_negocio c = ctx.unidades_negocio.Find(un);
            
            ctx.unidades_negocio.Remove(c);
        }

        public void UpdateUnidadNegocio(unidades_negocio un)
        {
            ctx.Entry(un).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}