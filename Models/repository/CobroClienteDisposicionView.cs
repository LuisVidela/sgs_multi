﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class CobroClienteDisposicionView
    {
        public Nullable<System.Int32> id_punto_servicio { get; set; }
        public Nullable<System.Int32> Disposicion { get; set; }
        public Nullable<System.Double> CobroTonelada { get; set; }
        public Nullable<System.Int32> Valor { get; set; }
        public Nullable<System.Double> valoruf { get; set; }
    }
}