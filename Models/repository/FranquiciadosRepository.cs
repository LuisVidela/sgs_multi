﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.modelViews;
using SGS.Models.ShadowBox;

namespace SGS.Models.repository
{
    public class FranquiciadosRepository 
    {
          private sgsbdEntities ctx;


        public FranquiciadosRepository(sgsbdEntities ctx)
        {
                         this.ctx = ctx;

        }

       public IEnumerable<franquiciado> GetFranquiciados(int unidad)
        {
            return ctx.franquiciado.Where(u => u.id_unidad == unidad).ToList();
        }

       public IEnumerable<object> GetFranquiciadoForList()
       {

           return ctx.franquiciado.Select(x => new
           {
               id_franquiciado = x.id_franquiciado,
               razon_social = x.razon_social
           }).OrderBy(x => x.razon_social).ToList();

       }

       public IEnumerable<object> GetFranquiciadoForSelect()
       {

           IEnumerable<object> data = ctx.franquiciado.Select(f => new
           {
               Text=f.razon_social,
               Value=f.razon_social
           }).OrderBy(f=>f.Text).ToList();


           IEnumerable<object> append = new object[]{
           new{Text="Seleccione un Valor",Value="NaN"},
           };

           IEnumerable<object> final_data = append.Concat(data);

           return final_data;
       }


       public IEnumerable<franquiciado> GetFranquiciadoForList2(int unidad)
       {
           return ctx.franquiciado.Where(u => u.id_unidad == unidad).ToList();
       }




       public IEnumerable<FranquiciadosView> GetFranquiciadosView(int unidad)
       {
           return ctx.franquiciado.Where(x=>x.id_unidad==unidad).Select(x => new FranquiciadosView()
           {
               id_franquiciado = x.id_franquiciado,
               direccion = x.direccion,
               razon_social = x.razon_social,
               fecha_contrato = x.fecha_contrato,
               rut_franquiciado =x.rut_franquiciado,
               observacion=x.observacion,
               estado=x.estado
           }).ToList();
       }

       public IEnumerable<FranquiciadosView> GetFranquiciadosView()
       {
           return ctx.franquiciado.Select(x => new FranquiciadosView()
           {
               id_franquiciado = x.id_franquiciado,
               direccion = x.direccion,
               razon_social = x.razon_social,
               fecha_contrato = x.fecha_contrato,
               rut_franquiciado = x.rut_franquiciado,
               observacion = x.observacion,
               estado = x.estado
           }).ToList();
       }

       public List<FranquiciadosProcesosTabla> GetFranquiciadosProcesos(int unidad,DateTime fechaInicio, DateTime fechaFin)
       {
           fechaFin = fechaFin.AddDays(1).AddMilliseconds(-1);
           var lista = ctx.franquiciado.Where(x => x.id_unidad == unidad && (x.estado == "Operativo" ||x.estado == "Activo"|| x.estado == null)).Select(x => new FranquiciadosProcesosTabla()
           {
               id_franquiciado = x.id_franquiciado,
               direccion = x.direccion,
               razon_social = x.razon_social,
               fecha_contrato = x.fecha_contrato,
               rut_franquiciado = x.rut_franquiciado,
               observacion = x.observacion,
               estado = x.estado
           }).ToList();

            foreach (FranquiciadosProcesosTabla item in lista)
            {
                item.tieneHR = false;
                item.tienePendientes = false;
                item.montoNeto = 0;

                try
                {
                    string[] ultimoPeriodoPagado = ctx.pago_franquiciados.Where(u => u.id_franquiciado == item.id_franquiciado && u.id_unidad == unidad).OrderByDescending(u => u.fechaFin).FirstOrDefault().periodo.Split('-');

                    item.ultimoPago = ultimoPeriodoPagado[1] + "-" + ultimoPeriodoPagado[0].PadLeft(2, '0');
                }
                catch (Exception ex)
                {
                }

                if (ctx.camiones.Where(u => u.id_franquiciado == item.id_franquiciado).FirstOrDefault() != null)
                {
                    var camionesFranquiciado = ctx.camiones.Where(u => u.id_franquiciado == item.id_franquiciado).ToArray();
                    for (int i = 0; i < camionesFranquiciado.Length; i++)
                    {
                        int temp = camionesFranquiciado[i].id_camion;
                        var hrSinCerrar = ctx.hoja_ruta.Where(u => u.id_camion == temp && u.fecha > fechaInicio && u.fecha < fechaFin && u.fecha_pagado == null && (u.estado == 1 || u.estado == 2)).ToList();
                        if (temp == 12)
                        {
                            item.tienePendientes = false;
                        }
                        if (hrSinCerrar.Count != 0)
                        {
                            item.tienePendientes = true;
                        }
                        if (ctx.hoja_ruta.Where(u => u.id_camion == temp && u.fecha >= fechaInicio && u.fecha <= fechaFin).FirstOrDefault() != null)
                        {
                            item.tieneHR = true;

                            var hr = ctx.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value >= fechaInicio && u.fecha.Value <= fechaFin && u.id_camion.Value == temp && u.estado == 3).ToList();

                            foreach (hoja_ruta encabezado in hr)
                            {
                                var hrDetalle = ctx.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == encabezado.id_hoja_ruta).ToList();
                                IEnumerable<ruta_punto> listaPuntosModelo = new List<ruta_punto>();
                                if (encabezado.ruta_modelo != null)
                                {
                                    var rutaModelo = ctx.ruta_modelo.Where(u => u.id_ruta == encabezado.ruta_modelo.Value).FirstOrDefault();
                                    listaPuntosModelo = rutaModelo.ruta_punto;
                                }
                                bool comboUltimoTarifa = false;
                                if (hrDetalle.Count != 0)
                                {
                                    foreach (hoja_ruta_detalle detalle in hrDetalle)
                                    {
                                        bool tarifaEspecial = false;
                                        int valorTarifaEspecial = 0;
                                        int idPuntoServicio = detalle.id_punto_servicio;

                                        if ((encabezado.id_tipo_servicio == 2) || (encabezado.id_tipo_servicio == 3))
                                            detalle.cantidad = 1;
                                        else
                                        {
                                            if (detalle.cantidad != null)
                                                detalle.cantidad = detalle.cantidad.Value;
                                            else
                                                detalle.cantidad = 1;
                                        }
                                        tipo_tarifa tarifa = new tipo_tarifa();
                                        if (listaPuntosModelo.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault() == null)
                                        {
                                            if (detalle.estado == 6 || detalle.estado == 5 || detalle.estado == 7)
                                            {
                                                tarifa = ctx.tipo_tarifa.Where(u => u.id_tipo_tarifa == 3).FirstOrDefault();
                                            }
                                            else
                                            {
                                                if (detalle.estado == 3 || detalle.estado == 4)
                                                {
                                                    tarifa = ctx.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        tarifa = ctx.puntos_servicio.Where(l => l.id_punto_servicio == idPuntoServicio).FirstOrDefault().tipo_tarifa;
                                                    }
                                                    catch (Exception e)
                                                    {
                                                        string error = e.ToString();
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (comboUltimoTarifa == false && detalle.estado == 2)
                                            {
                                                comboUltimoTarifa = true;
                                                int idTipoTarifa = ctx.ruta_modelo.Where(u => u.id_ruta == encabezado.ruta_modelo.Value).FirstOrDefault().idtarifa.Value;
                                                tarifa = ctx.tipo_tarifa.Where(u => u.id_tipo_tarifa == idTipoTarifa).FirstOrDefault();
                                            }
                                            else
                                            {
                                                tarifa = ctx.tipo_tarifa.Where(u => u.id_tipo_tarifa == 1).FirstOrDefault();
                                            }
                                        }
                                        DateTime ahora = DateTime.Now.Date;
                                        if (tarifa.tarifa_escalable == true)
                                        {
                                            var calculoTarifa = ctx.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaInicio.Date && u.fecha_final == fechaFin.Date && ahora == u.fechaEjecucion).FirstOrDefault();
                                            if (calculoTarifa != null)
                                            {
                                                var tarifaFinal = ctx.tipo_tarifa_detalle.Where(u => u.idTarifa == tarifa.id_tipo_tarifa && calculoTarifa.cantidad >= u.rango_min && calculoTarifa.cantidad <= u.rango_max).FirstOrDefault();
                                                if (tarifaFinal != null)
                                                {
                                                    tarifaEspecial = true;
                                                    valorTarifaEspecial = tarifaFinal.valor;
                                                }
                                                else
                                                {
                                                    valorTarifaEspecial = Convert.ToInt32(tarifa.valor_tarifa);
                                                }

                                                if (ctx.detalle_calculo_tarifa_escalable.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa && u.fecha_inicio == fechaInicio.Date && u.fecha_final == fechaFin.Date && ahora == u.fechaEjecucion).FirstOrDefault() == null)
                                                {
                                                    calculoCantidadPuntos(fechaInicio, fechaFin, tarifa);
                                                }
                                            }
                                        }
                                        int valorTarifa = 0;
                                        if (tarifaEspecial == false)
                                            valorTarifa = Convert.ToInt32(Math.Round(tarifa.valor_tarifa, 0, MidpointRounding.AwayFromZero));
                                        else valorTarifa = valorTarifaEspecial;
                                        bool porPeso = true;

                                        //if (tarifa.id_tipo_tarifa == 28) porPeso = true;
                                        //else porPeso = false;

                                        var punto = ctx.puntos_servicio.Where(u => u.id_punto_servicio == idPuntoServicio).FirstOrDefault();

                                        if (punto != null)
                                        {
                                            if (punto.pago_por_peso == true) porPeso = true;
                                            else porPeso = false;
                                        }

                                        if (detalle.cantidad == null) detalle.cantidad = 1;

                                        if (porPeso == false)
                                        {

                                            if (detalle.cantidad == 0) detalle.cantidad = 1;
                                            double valor = valorTarifa * detalle.cantidad.Value;
                                            item.montoNeto = item.montoNeto + valor;


                                        }
                                        else
                                        {
                                            if (detalle.peso == null) detalle.peso = 1;


                                            double valor = 0;
                                            if (detalle.peso != null)
                                            {
                                                valor = valorTarifa * (detalle.peso.Value / 1000);
                                            }
                                            else
                                            {
                                                valor = valorTarifa * detalle.cantidad.Value;
                                            }
                                            item.montoNeto = item.montoNeto + Convert.ToInt32(Math.Round(valor, 0, MidpointRounding.AwayFromZero));


                                        }
                                    }
                                }
                            }
                            item.montoBruto = item.montoNeto * 1.19;
                        }
                    }
                }
            }
            FranquiciadosProcesosView listaFinal = new FranquiciadosProcesosView();
            listaFinal.tabla = lista;
            return listaFinal.tabla;
        }
       public void calculoCantidadPuntos(DateTime fechaInicio, DateTime fechaFin, tipo_tarifa tarifa)
       {
           int cantidad = 0;
           using (var db = new sgsbdEntities())
           {
               var puntos = db.puntos_servicio.Where(u => u.id_tipo_tarifa == tarifa.id_tipo_tarifa).ToList();
               var lista = db.hoja_ruta.Where(u => u.fecha_pagado == null && u.fecha.Value >= fechaInicio && u.fecha.Value <= fechaFin && u.estado == 3).ToList();
               foreach (var header in lista)
               {
                   var lines = db.hoja_ruta_detalle.Where(u => u.id_hoja_ruta == header.id_hoja_ruta).ToList();
                   foreach (var line in lines)
                   {
                       if (puntos.Where(u => u.id_punto_servicio == line.id_punto_servicio).FirstOrDefault() != null && (line.estado == 2 || line.estado == 6)) cantidad++;
                   }
               }
               var nueva = db.detalle_calculo_tarifa_escalable.Create();
               nueva.id = db.detalle_calculo_tarifa_escalable.Count() + 1;
               nueva.id_tipo_tarifa = tarifa.id_tipo_tarifa;
               nueva.fecha_final = fechaFin;
               nueva.fecha_inicio = fechaInicio;
               nueva.cantidad = cantidad;
               nueva.fechaEjecucion = DateTime.Now;
               db.detalle_calculo_tarifa_escalable.Add(nueva);
               db.SaveChanges();

           }
       }
       public franquiciado GetFranquiciadoByID(int cl)
       {
           return ctx.franquiciado.Find(cl);
       }

       public void InsertFranquiciado(franquiciado cl)
       {
           ctx.franquiciado.Add(cl);
       }

       public void DeleteFranquiciado(int cl)
       {
           franquiciado cl2 = ctx.franquiciado.Find(cl);
           ctx.franquiciado.Remove(cl2);
       }

       public void UpdateFranquiciado(franquiciado cl)
       {
           ctx.Entry(cl).State = System.Data.EntityState.Modified;
       }


       public FranquiciadosShadowBoxView getFranquiciadoShadowBox(int id)
       {


           var join = (from f in ctx.franquiciado.Where(f => f.id_franquiciado == id)
                       select new FranquiciadosShadowBoxView()
                       {
                           id_franquiciado=f.id_franquiciado,
	                       razon_social=f.razon_social,
	                       observacion=f.observacion,
	                       direccion=f.direccion,
                           descuentos= (from de in ctx.descuento.Where(de=> de.id_franquiciado==id)
                                         
                                         select new FranquiciadosDescuentosView()
                                         {
                                             id_descuento=de.id_descuento,
                                             neto=1,
                                             iva=1,
                                             total=1,
                                            
                                         }).DefaultIfEmpty()
                       }).FirstOrDefault();

           return join;

       }






       public void Save()
       {
           try
           {
               ctx.SaveChanges();

           }
           catch (Exception e)
           {
               Console.WriteLine("{0} Second exception caught.", e.InnerException);
           }
       }
       protected bool disposed = false;

       protected virtual void Dispose(bool dispose)
       {

           if (!this.disposed)
           {
               if (dispose)
               {
                   ctx.Dispose();
               }
           }
           this.disposed = true;
       }



       public void Dispose()
       {

           this.Dispose(true);
           GC.SuppressFinalize(this);
       }



    }
}
