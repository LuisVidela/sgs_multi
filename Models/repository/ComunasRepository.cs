﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class ComunasRepository
    {

        private sgsbdEntities ctx;


        public ComunasRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<comunas> GetComunas()
        {
            return ctx.comunas.ToList();
        }

        public comunas GetComunaByID(int c)
        {
            return ctx.comunas.Find(c);
        }

        public void InsertComuna(comunas c)
        {
             ctx.comunas.Add(c);
        }

        public void DeleteComuna(int c)
        {
            comunas comuna=ctx.comunas.Find(c);
            ctx.comunas.Remove(comuna);
        }

        public void UpdateComuna(comunas c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}