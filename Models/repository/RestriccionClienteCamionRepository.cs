﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class RestriccionClienteCamionRepository
    {
        private sgsbdEntities ctx;


        public RestriccionClienteCamionRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        
        }

        public IEnumerable<restriccion_camion_cliente> GetRestriccionesClientesCamiones()
        {
            return ctx.restriccion_camion_cliente.ToList();
        }

        public restriccion_camion_cliente GetRestriccionClienteCamionByID(int rs)
        {
            return ctx.restriccion_camion_cliente.Find(rs);
        }

        public void InsertRestriccionClienteCamion(restriccion_camion_cliente rs)
        {
             ctx.restriccion_camion_cliente.Add(rs);
        }

        public void DeleteRestriccionClienteCamion(int rs)
        {
            restriccion_camion_cliente r=ctx.restriccion_camion_cliente.Find(rs);
            ctx.restriccion_camion_cliente.Remove(r);
        }

        public void UpdateRestriccionClienteCamion(restriccion_camion_cliente rs)
        {
            ctx.Entry(rs).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}