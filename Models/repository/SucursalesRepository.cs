﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using SGS.Models.repository;
using SGS.Models.modelViews;
using SGS.Models.ShadowBox;

namespace SGS.Models.repository
{
    public class SucursalesRepository:IDisposable
    {

        
        private sgsbdEntities ctx;

        public SucursalesRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        
        }

        public IEnumerable<object> GetSucursalesForList()
        {

            return ctx.sucursales.Select(x => new
            {
                id_sucursal = x.id_sucursal,
                sucursal = x.sucursal
            }).OrderBy(x => x.sucursal).ToList();

        }



        public IEnumerable<sucursales> GetSucursales()
        {
            return this.ctx.sucursales.ToList();
        }

        public IEnumerable<SucursalesView> GetSucursalesView(int unidad) {


            //int[] indices = (from cr in ctx.contratos select  cr.id_cliente ).Distinct().ToArray();

            var data = (from s in ctx.sucursales
                        join c in ctx.clientes                       
                        on s.id_cliente equals c.id_cliente
                        join ct in ctx.categoria_sucursal
                        on s.id_categoria equals ct.id_categoria into categoria
                        from ct in categoria.DefaultIfEmpty()
                        join z in ctx.Zona
                        on s.zona_id_zona equals z.id_zona into zonax
                        from z in zonax.DefaultIfEmpty()
                        
                        where c.id_unidad == unidad
                        //where indices.Contains(c.id_cliente)
                        select new SucursalesView()
                        {
                            id_sucursal = s.id_sucursal,
                            razon_social = c.razon_social,
                            direccion = s.direccion,
                            sucursal = s.sucursal,
                            nombre_zona = z.nombre_zona,
                            nombre_categoria = ct.categoria
                        });


            return data.ToList();

        }

        public IEnumerable<SucursalesView> GetSucursalesClientes(int cliente)
        {


            var data = (from s in ctx.sucursales
                        join c in ctx.clientes on s.id_cliente equals c.id_cliente
                        where c.id_cliente == cliente
                        select new SucursalesView()
                        {
                            id_sucursal = s.id_sucursal,
                            sucursal = s.sucursal,

                        });
            return data.ToList();

         }


        public sucursalesShadowBoxView getSucursalShadowBox(int id) {

            var join = (from s in ctx.sucursales.Where(s=>s.id_sucursal==id)
                        join r in ctx.rubros on s.id_rubro equals r.id_rubro into rubx
                        from rx in rubx.DefaultIfEmpty()
                         join
                            p in ctx.paises on s.id_pais equals p.id_pais into paisesx
                        from px in paisesx.DefaultIfEmpty() join
                            c in ctx.comunas on s.id_comuna equals c.id_comuna into comunax
                        from cx in comunax.DefaultIfEmpty()
                        join
                            cd in ctx.ciudades on s.id_ciudad equals cd.id_ciudad into cix
                        from cxx in cix.DefaultIfEmpty()
                        join
                            cl in ctx.clientes on s.id_cliente equals cl.id_cliente into clientesx
                            from clx in clientesx.DefaultIfEmpty()
                        join
                            re in ctx.regiones on s.id_region equals re.id_region into final
                        from data in final.DefaultIfEmpty()
                        join
                            zo in ctx.Zona on s.zona_id_zona equals zo.id_zona into zonax
                        from data2 in zonax.DefaultIfEmpty()
                        select new sucursalesShadowBoxView()
                        {
                            cliente = clx.razon_social,
                            sucursal = s.sucursal,
                            descripcion = s.descripcion,
                            pais = px.pais,
                            region = data.nombre_region,
                            ciudad = cxx.ciudad,
                            comuna = cx.nombre_comuna,
                            direccion = s.direccion,
                            contacto_sucursal = s.contacto_sucursal,
                            email_sucursal = s.email_sucursal,
                            fono_sucursal = s.fono_sucursal,
                            zona = data2.nombre_zona,
                            periodo_desde=s.periodo_desde,
                            periodo_hasta=s.periodo_hasta,
                            estado_sucursal = s.estado_sucursal,
                            rubro = rx.rubro
                        }).FirstOrDefault();

            return join;
        }
  

        public sucursales GetSucursalByID(int s)
        {
            return this.ctx.sucursales.Find(s);
        }

        public void InsertSucursal(sucursales s)
        {
            this.ctx.sucursales.Add(s);
        }

        public void DeleteSucursal(int s)
        {
            sucursales psi = this.ctx.sucursales.Find(s);
            this.ctx.sucursales.Remove(psi);
        }

        public void UpdateSucursal(sucursales ps)
        {
            this.ctx.Entry(ps).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            this.ctx.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<object> GetSucursalesForSelect()
        {

            IEnumerable<object> data = ctx.sucursales.Select(x => new
            {
                Value = x.id_sucursal,
                Text = x.sucursal
            }).OrderBy(x => x.Text).ToList();
            
            IEnumerable<object> def = new object[]{
               new { Text="Seleccione un Valor",Value="0"}
           
           };

            IEnumerable<object> final_data = def.Concat(data);


            return final_data;
        }


        public IEnumerable<SucursalComboView> GetSucursalesForSelectByUnidad(int unidad)
        {

            var data = (from s in ctx.sucursales
                        join c in ctx.clientes
                        on s.id_cliente equals c.id_cliente
                        
                        where c.id_unidad == unidad
                        orderby s.sucursal
                        //where indices.Contains(c.id_cliente)
                        select new SucursalComboView()
                        {
                            id_sucursal = s.id_sucursal,
                            sucursal = s.sucursal,
                            id_unidad = c.id_unidad
                        });


            return data.ToList();
        }


    }
}