﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoDescuentoRepository
    {
        private sgsbdEntities ctx;


        public TipoDescuentoRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<tipo_descuento> GetTipoDescuento()
        {
            return ctx.tipo_descuento.ToList();
        }

        public tipo_descuento GetTipodescuentoByID(int t)
        {
            return ctx.tipo_descuento.Find(t);
        }

        public void InsertTipodescuento(tipo_descuento t)
        {
            ctx.tipo_descuento.Add(t);
        }

        public void DeleteTipodescuento(int t)
        {
            tipo_descuento tc = ctx.tipo_descuento.Find(t);
            ctx.tipo_descuento.Remove(tc);
        }

        public void UpdateTipodescuento(tipo_descuento t)
        {
            ctx.Entry(t).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}