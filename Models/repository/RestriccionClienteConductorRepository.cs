﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class RestriccionClienteConductorRepository
    {

        private sgsbdEntities ctx;


        public RestriccionClienteConductorRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        
        }

        public IEnumerable<restriccion_conductor_cliente> GetRestriccionesClientesConductores()
        {
            return ctx.restriccion_conductor_cliente.ToList();
        }

        public restriccion_conductor_cliente GetRestriccionClienteConductorByID(int rs)
        {
            return ctx.restriccion_conductor_cliente.Find(rs);
        }

        public void InsertRestriccionClienteConductor(restriccion_conductor_cliente rs)
        {
             ctx.restriccion_conductor_cliente.Add(rs);
        }

        public void DeleteRestriccionClienteConductor(int rs)
        {
            restriccion_conductor_cliente r=ctx.restriccion_conductor_cliente.Find(rs);
            ctx.restriccion_conductor_cliente.Remove(r);
        }

        public void UpdateRestriccionClienteConductor(restriccion_conductor_cliente rs)
        {
            ctx.Entry(rs).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}