﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.ShadowBox;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class ConductoresRepository
    {

        private sgsbdEntities ctx;

        public ConductoresRepository(sgsbdEntities ctx){

            this.ctx = ctx;
    
         }

        public IEnumerable<conductores> GetConductores(int unidad)
        {
            return  (from c in ctx.conductores join
                     tl in ctx.tipo_licencia on c.id_tipo_licencia equals tl.id_tipo_licencia into res
                     from r in res.DefaultIfEmpty()
                     join f in ctx.franquiciado on c.id_franquiciado equals f.id_franquiciado
                     where f.id_unidad==unidad select c)
            .ToList();
        }

        public IEnumerable<conductores> GetConductores()
        {
            return (from c in ctx.conductores
                    join
                        tl in ctx.tipo_licencia on c.id_tipo_licencia equals tl.id_tipo_licencia into res
                    from r in res.DefaultIfEmpty()
                    join f in ctx.franquiciado on c.id_franquiciado equals f.id_franquiciado
                    select c)
            .ToList();
        }

        public IEnumerable<ConductoresView> getConductoresView(int unidad) {

            return (from c in ctx.conductores
                    join
                        tl in ctx.tipo_licencia.DefaultIfEmpty() on c.id_tipo_licencia equals tl.id_tipo_licencia
                        join f in ctx.franquiciado on c.id_franquiciado equals f.id_franquiciado into cf
                        from fr in cf.DefaultIfEmpty() where fr.id_unidad==unidad
                    select new ConductoresView()
                    {
                        estado = c.estado,
                        fecha_ingreso = c.fecha_ingreso,
                        nombre_conductor = c.nombre_conductor,
                        rut_conductor = c.rut_conductor,
                        tipo_licencia1 = tl.tipo_licencia1,
                        restricciones = (from rs in ctx.restriccion_conductor_cliente
                                         join cl in ctx.clientes.DefaultIfEmpty() on rs.id_cliente equals cl.id_cliente
                                         where rs.id_conductor == c.id_conductor
                                         select new ConductoresRestriccionesView()
                                         {
                                             id_restriccion = rs.id_restriccion,
                                             cliente = cl.razon_social,
                                             motivo = rs.motivo
                                         }),
                        fecha_vencimientolic = c.fecha_vencimientolic,
                        id_conductor = c.id_conductor
                                               
                    }
               ).ToList();


            //return (from c in ctx.conductores
            //        select new ConductoresView()
            //        {
            //            estado = c.estado,
            //            fecha_ingreso = c.fecha_ingreso,
            //            nombre_conductor = c.nombre_conductor,
            //            id_conductor = c.id_conductor
            //        }).ToList();

        
        }

        public IEnumerable<ConductoresView> getConductoresView()
        {

            return (from c in ctx.conductores
                    join
                        tl in ctx.tipo_licencia.DefaultIfEmpty() on c.id_tipo_licencia equals tl.id_tipo_licencia
                    join f in ctx.franquiciado on c.id_franquiciado equals f.id_franquiciado into cf
                    from fr in cf.DefaultIfEmpty()
                    select new ConductoresView()
                    {
                        estado = c.estado,
                        fecha_ingreso = c.fecha_ingreso,
                        nombre_conductor = c.nombre_conductor,
                        rut_conductor = c.rut_conductor,
                        tipo_licencia1 = tl.tipo_licencia1,
                        restricciones = (from rs in ctx.restriccion_conductor_cliente
                                         join cl in ctx.clientes.DefaultIfEmpty() on rs.id_cliente equals cl.id_cliente
                                         where rs.id_conductor == c.id_conductor
                                         select new ConductoresRestriccionesView()
                                         {
                                             id_restriccion = rs.id_restriccion,
                                             cliente = cl.razon_social,
                                             motivo = rs.motivo
                                         }),
                        id_conductor = c.id_conductor,
                        fecha_vencimientolic = c.fecha_vencimientolic

                    }
               ).ToList();


            //return (from c in ctx.conductores
            //        select new ConductoresView()
            //        {
            //            estado = c.estado,
            //            fecha_ingreso = c.fecha_ingreso,
            //            nombre_conductor = c.nombre_conductor,
            //            id_conductor = c.id_conductor
            //        }).ToList();


        }

        public IEnumerable<ConductoresView> getConductoresView2()
        {

            return (from c in ctx.conductores
                    join
                    tl in ctx.tipo_licencia.DefaultIfEmpty() on c.id_tipo_licencia equals tl.id_tipo_licencia
                    join rss in ctx.restriccion_conductor_cliente.DefaultIfEmpty() on c.id_conductor equals rss.id_conductor into tls
                    from tlsss in tls.DefaultIfEmpty()
                    select new ConductoresView()
                    {
                        estado = c.estado,
                        fecha_ingreso = c.fecha_ingreso,
                        nombre_conductor = c.nombre_conductor,
                        rut_conductor = c.rut_conductor,
                        tipo_licencia1 = tl.tipo_licencia1,
                        restricciones = null,
                        id_conductor = c.id_conductor,
                        fecha_vencimientolic = c.fecha_vencimientolic
                    }
               ).ToList();

        }


        public IEnumerable<conductores> GetConductoresForList()
        {
            return ctx.conductores.ToList();
        }


        public IEnumerable<conductoresShadowBoxView> GetConductoresExcel() {

            return (from c in ctx.conductores
                    join tl in ctx.tipo_licencia.DefaultIfEmpty() on c.id_tipo_licencia equals tl.id_tipo_licencia
                    orderby c.id_conductor
                    select new conductoresShadowBoxView()
                    {
                         estado=c.estado,
                          fecha_ingreso=c.fecha_ingreso,
                           id_conductor=c.id_conductor,
                            licencia=tl.tipo_licencia1,
                             nombre_conductor=c.nombre_conductor,
                              observacion=c.observacion,
                               rut_conductor=c.rut_conductor,
                                fecha_vencimientolic = c.fecha_vencimientolic
                    }).ToList();
        
        }

        public conductoresShadowBoxView getConductorShadowBox(int id) {

            var join = (from c in ctx.conductores.Where(c => c.id_conductor == id)
                        join
                        tl in ctx.tipo_licencia.DefaultIfEmpty() on c.id_tipo_licencia equals tl.id_tipo_licencia 
                        select new conductoresShadowBoxView()
                        {
                            estado = c.estado,
                            fecha_ingreso = c.fecha_ingreso,
                            licencia = tl.tipo_licencia1,
                            observacion = c.observacion,
                            rut_conductor = c.rut_conductor,
                            nombre_conductor = c.nombre_conductor,
                            restricciones = (from rs in ctx.restriccion_conductor_cliente
                                             join cl in ctx.clientes.DefaultIfEmpty() on rs.id_cliente equals cl.id_cliente
                                             where rs.id_conductor == c.id_conductor
                                             select new ConductoresRestriccionesView()
                                             {
                                                 id_restriccion = rs.id_restriccion,
                                                 cliente = cl.razon_social,
                                                 motivo = rs.motivo
                                             })
                        }).FirstOrDefault();

            return join;
        
        }

        public conductores GetConductorByID(int conductor)
        {
            return ctx.conductores.Find(conductor);
        }

        public void InsertConductor(conductores conductor)
        {
            ctx.conductores.Add(conductor);
        }

        public void DeleteConductor(int conductor)
        {
            conductores c = ctx.conductores.Find(conductor);
            ctx.conductores.Remove(c);
        }

        public void UpdateConductor(conductores co)
        {
            ctx.Entry(co).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}