﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoContratoRepository
    {
        private sgsbdEntities ctx;


        public TipoContratoRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<tipo_contrato> GetTipoContratos()
        {
            return ctx.tipo_contrato.ToList();
        }

        public tipo_contrato GetTipoContratoByID(int t)
        {
            return ctx.tipo_contrato.Find(t);
        }

        public void InsertTipoContrato(tipo_contrato t)
        {
             ctx.tipo_contrato.Add(t);
        }

        public void DeleteTipoContrato(int t)
        {
            tipo_contrato tc=ctx.tipo_contrato.Find(t);
            ctx.tipo_contrato.Remove(tc);
        }

        public void UpdateTipoContrato(tipo_contrato t)
        {
            ctx.Entry(t).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}