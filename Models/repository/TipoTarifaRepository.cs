﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoTarifaRepository
    {
         private sgsbdEntities ctx;


         public TipoTarifaRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<tipo_tarifa> GetTipoTarifas(int unidad)
        {
            return ctx.tipo_tarifa.Where(tt=>tt.id_unidad==unidad).ToList();
        }

        public IEnumerable<object> GetTipoTarifasForList(int unidad)
        {

            return ctx.tipo_tarifa.Where(tt => tt.id_unidad == unidad).Select(tt => new
            {
                text=tt.tipo_tarifa1,
                value=tt.id_tipo_tarifa,
                estado= tt.estado
            }).ToList();
        }

        public tipo_tarifa GetTipoTarifaByID(int tt)
        {
            return ctx.tipo_tarifa.Find(tt);
        }

        public void InsertTipoTarifa(tipo_tarifa tt)
        {
             ctx.tipo_tarifa.Add(tt);
        }

        public void DeleteTipoTarifa(int tt)
        {
            tipo_tarifa tt2=ctx.tipo_tarifa.Find(tt);
            ctx.tipo_tarifa.Remove(tt2);
        }

        public void UpdateTipoTarifa(tipo_tarifa tt)
        {
            ctx.Entry(tt).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}