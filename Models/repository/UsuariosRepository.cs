﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.repository
{
    public class UsuariosRepository
    {
         private sgsbdEntities ctx;


        public UsuariosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<usuarios> GetUsuarios()
        {
            return ctx.usuarios.ToList();
        }
        
        public UsuariosView GetUsuariosForUser(string us)
        {
            var query = (from x in ctx.usuarios.Where(x => x.user == us)
                         select new UsuariosView()
                             {
                                 id_usuario = x.id_user,
                                 usuario = x.user,
                                 password = x.password
                             });
            return query.FirstOrDefault();
         }


        public usuarios GetUsuarioByID(int u)
        {
            return ctx.usuarios.Find(u);
        }

        public void InsertUsuario(usuarios u)
        {
             ctx.usuarios.Add(u);
        }

        public void DeleteUsuario(int u)
        {
            usuarios usuario=ctx.usuarios.Find(u);
            ctx.usuarios.Remove(usuario);
        }

        public void UpdateUsuario(usuarios u)
        {
            ctx.Entry(u).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public static int getUnidadSeleccionada()
        {

            if (HttpContext.Current.Session["unidad_seleccionada"] != null)
            {
                return (int)HttpContext.Current.Session["unidad_seleccionada"];
            }
            else
            {
                HttpContext.Current.Response.Redirect("/Usuarios/logout");
                return -1;
            }
        }


    }
}