﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class RellenosRepository
    {
         private sgsbdEntities ctx;


        public RellenosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<vertederos> GetRellenos()
        {
            return ctx.vertederos.ToList();
        }

        public IEnumerable<object> GetRellenosForList(int id_unidad) {

            return ctx.vertederos.Where(tt => tt.id_unidad == id_unidad).Select(v => new
            {
                text=v.vertedero,
                value=v.id_vertedero
            }).ToList();
        }

        public vertederos GetRellenoByID(int v)
        {
            return ctx.vertederos.Find(v);
        }

        public void InsertRelleno(vertederos v)
        {
             ctx.vertederos.Add(v);
        }

        public void DeleteRelleno(int v)
        {
            vertederos vertedero=ctx.vertederos.Find(v);
            ctx.vertederos.Remove(vertedero);
        }

        public void UpdateRelleno(vertederos v)
        {
            ctx.Entry(v).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}