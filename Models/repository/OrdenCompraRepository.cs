﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Data.Entity.Validation;

namespace SGS.Models.repository
{
    public class OrdenCompraRepository 
    {

        private sgsbdEntities ctx;


        public OrdenCompraRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<orden_de_compra> GetOrdenCompra()
        {
            return ctx.orden_de_compra.ToList();
        }

        //public orden_de_compra GetOrdenCompraByID(int orden)
        //{
        //    return ctx.orden_de_compra.Find(orden);
        //}

        public orden_de_compra GetOrdenCompraByID(int cl)
        {
            return ctx.orden_de_compra.Find(cl);
        }



        public void InsertOrdenCompra(orden_de_compra c)
        {
            ctx.orden_de_compra.Add(c);
        }


        public void UpdateOrdenCompra(orden_de_compra c)
        {
            ctx.Entry(c).State = System.Data.EntityState.Modified;
        }

        public IEnumerable<OrdenCompraView> GetContrato(int id)
        {
            var data = (from co in ctx.contratos
                        join su in ctx.sucursales on co.id_sucursal equals su.id_sucursal
                        join cl in ctx.clientes on su.id_cliente equals cl.id_cliente
                        join oc in ctx.orden_de_compra on cl.id_cliente equals oc.id_cliente
                        where co.id_contrato == id
                        select new OrdenCompraView()
                        {
                            id_orden_compra = oc.id_orden_de_compra,
                            numero_orden = oc.numero_orden
                        }).ToList();

            return data;
        }
        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

}
