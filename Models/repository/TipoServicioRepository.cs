﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class TipoServicioRepository
    {
        private sgsbdEntities ctx;

        public TipoServicioRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
    
         }

        public IEnumerable<tipo_servicio> GetTiposServicios()
        {
            return ctx.tipo_servicio.ToList();
        }


        public IEnumerable<object> GetTipoServicioForList()
        {

            return ctx.tipo_servicio.Select(tt => new
            {
                text = tt.tipo_servicio1,
                value = tt.id_tipo_servicio
            }).ToList();
        }

        public tipo_servicio GetTipoServicioByID(int ts)
        {
            return ctx.tipo_servicio.Find(ts);
        }

        public void InsertTipoServicio(tipo_servicio ts)
        {
            ctx.tipo_servicio.Add(ts);
        }

        public void DeleteTipoServicio(int ts)
        {
            tipo_servicio c = ctx.tipo_servicio.Find(ts);
            ctx.tipo_servicio.Remove(c);
        }

        public void UpdateTipoServicio(tipo_servicio ts)
        {
            ctx.Entry(ts).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}