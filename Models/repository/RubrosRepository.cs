﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class RubrosRepository
    {
         private sgsbdEntities ctx;


        public RubrosRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }
        

        public IEnumerable<rubros> Getrubros()
        {
            return ctx.rubros.ToList();
        }

        public rubros GetRubroByID(int r)
        {
            return ctx.rubros.Find(r);
        }

        public void InsertRubro(rubros r)
        {
             ctx.rubros.Add(r);
        }

        public void DeleteRubro(int r)
        {
            rubros rubro=ctx.rubros.Find(r);
            ctx.rubros.Remove(rubro);
        }

        public void UpdateRubro(rubros r)
        {
            ctx.Entry(r).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}