﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.ajax;

namespace SGS.Models.repository
{
    public class RutaPuntoRepository
    {
        private sgsbdEntities ctx;


        public RutaPuntoRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;

        }


        public IEnumerable<ruta_punto> GetRutaPuntos()
        {
            return ctx.ruta_punto.ToList();
        }
        
        public IEnumerable<tableDataDetailsAjax> getRutasPuntosDetails(int ruta , int col) {

            int limitA = Int32.Parse(String.Format("{0}{1}", col, "000"));
            int limitB= Int32.Parse(String.Format("{0}{1}", (col + 1),"000"));


            return (from rp in ctx.ruta_punto.Where(i => i.id_ruta == ruta & i.Orden > limitA & i.Orden < limitB)
                    join
                        r in ctx.ruta_modelo on rp.id_ruta equals r.id_ruta
                    join
                        ps in ctx.puntos_servicio on rp.id_punto_servicio equals ps.id_punto_servicio
                    select new tableDataDetailsAjax() { 
                     dia_ruta=r.dia_ruta,
                      nombre_punto=ps.nombre_punto,
                       nombre_ruta=r.nombre_ruta,
                        id_ruta=r.id_ruta,
                         id_ruta_punto=rp.id_ruta_punto,
                        id_punto_servicio=ps.id_punto_servicio
                    }).ToList();
        
        
        }


        public IEnumerable<tableDataDetailsAjax> getRutasPuntosDetailsAll(int ruta)
        {

            return (from rp in ctx.ruta_punto.Where(i => i.id_ruta == ruta)
                    join
                        r in ctx.ruta_modelo on rp.id_ruta equals r.id_ruta
                    join
                        ps in ctx.puntos_servicio on rp.id_punto_servicio equals ps.id_punto_servicio
                    select new tableDataDetailsAjax()
                    {
                        dia_ruta = r.dia_ruta,
                        nombre_punto = ps.nombre_punto,
                        nombre_ruta = r.nombre_ruta,
                        id_ruta = r.id_ruta,
                        id_ruta_punto = rp.id_ruta_punto,
                        id_punto_servicio = ps.id_punto_servicio

                    }).ToList();


        }


        public ruta_punto GetRutaPuntoByID(int r)
        {
            return ctx.ruta_punto.Find(r);
        }

        public void InsertRutaPunto(ruta_punto r)
        {
            ctx.ruta_punto.Add(r);
        }

        public void DeleteRutaPunto(int r)
        {
            ruta_punto rp = ctx.ruta_punto.Find(r);
            ctx.ruta_punto.Remove(rp);
        }

        public void UpdateRutaPunto(ruta_punto r)
        {
            ctx.Entry(r).State = System.Data.EntityState.Modified;
        }



        public void Save()
        {
            
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}