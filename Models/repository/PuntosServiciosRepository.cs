﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Data.Entity.Validation;

namespace SGS.Models.repository
{
    public class PuntosServiciosRepository:IPuntosServiciosRepository
    {

        private sgsbdEntities ctx;

        public PuntosServiciosRepository(sgsbdEntities ctx) {

            this.ctx = ctx;
        
        }

        public IEnumerable<puntos_servicio> GetPuntosServicios()
        {
            return this.ctx.puntos_servicio.ToList();        }

        public IEnumerable<PuntosServiciosView> GetPuntosServiciosView2(int unidad) {

            int[] indices = (from cr in ctx.contratos where cr.id_unidad == unidad select cr.id_contrato).Distinct().ToArray();

            var data = (from ps in this.ctx.puntos_servicio join
                        c in ctx.contratos on ps.id_contrato equals c.id_contrato join
                        cl in ctx.clientes on c.id_cliente equals cl.id_cliente join
                        tt in ctx.tipo_tarifa on ps.id_tipo_tarifa equals tt.id_tipo_tarifa
                        where indices.Contains(c.id_contrato)
                        select new PuntosServiciosView()
                        {
                            id_punto_servicio = ps.id_punto_servicio,
                            nombre_punto = ps.nombre_punto,
                            id_contrato = ps.id_contrato,
                            estado_punto = ps.estado_punto,
                            lunes=ps.lunes,
                            martes = ps.martes,
                            miercoles = ps.miercoles,
                            jueves = ps.jueves,
                            viernes = ps.viernes,
                            sabado = ps.sabado,
                            domingo = ps.domingo,
                            cant_equipos=ps.cant_equipos,
                            nombre_contacto=ps.nombre_contacto,
                            email_contacto=ps.email_contacto,
                            fono_contacto=ps.fono_contacto,
                            celular_contacto=ps.celular_contacto,
                            id_equipo=ps.id_equipo,
                            valor_tarifa =tt.valor_tarifa
                        });


            return data.ToList();
        
        }


        public IEnumerable<object> getPuntosServicioForSelect(){

            IEnumerable<object> data = ctx.puntos_servicio.Select(x=>new
            {
                 Value=x.id_punto_servicio,
                 Text=x.nombre_punto
            }).OrderBy(x=>x.Text).ToList();

            IEnumerable<object> def = new object[]{
               new { Text="Seleccione un Valor",Value="0"}
           
           };

            IEnumerable<object> final_data = def.Concat(data);


            return final_data;
        }

        public PuntosServiciosView GetPuntosServiciosView(int id)
        {

            var data = (from ps in this.ctx.puntos_servicio.Where(ps=>ps.id_punto_servicio==id)
                        select new PuntosServiciosView()
                        {
                            id_punto_servicio = ps.id_punto_servicio,
                            nombre_punto = ps.nombre_punto,
                            id_contrato = ps.id_contrato,
                            estado_punto = ps.estado_punto,
                            lunes = ps.lunes,
                            martes = ps.martes,
                            miercoles = ps.miercoles,
                            jueves = ps.jueves,
                            viernes = ps.viernes,
                            sabado = ps.sabado,
                            domingo = ps.domingo     

                        });


            return data.FirstOrDefault();

        }




        public IEnumerable<PuntosServicios_TipoServicio_View> GetPuntosServicios_TipoServicio_View(int unidad)
        {
            return (from ps in ctx.puntos_servicio
                    join c in ctx.contratos on ps.id_contrato equals c.id_contrato
                    join tc in ctx.tipo_contrato on c.id_tipo_contrato equals tc.id_tipo_contrato
                    where c.id_unidad==unidad
                    select new PuntosServicios_TipoServicio_View()
                    {
                        id_punto_servicio = ps.id_punto_servicio,
                        nombre_punto = ps.nombre_punto,
                        id_contrato = c.id_contrato,
                        estado_punto = ps.estado_punto,
                        id_tipo_contrato = c.id_tipo_contrato,
                        id_cliente = c.id_cliente,
                        id_sucursal=c.id_sucursal,
                        estado_contrato=c.estado,
                        nombre_tipo_contrato=tc.tipo_contrato1,
                        dias = ((bool)(ps.lunes) ? "LUNES" : "") + ((bool)(ps.martes) ? "MARTES" : "") + ((bool)(ps.miercoles) ? "MIERCOLES" : "") + ((bool)(ps.jueves) ? "JUEVES" : "") + ((bool)(ps.viernes) ? "VIERNES" : "") + ((bool)(ps.sabado) ? "SABADO" : "") + ((bool)(ps.domingo) ? "DOMINGO" : "")
                        //+ps.martes.ToString()+ps.miercoles.ToString()+ps.jueves.ToString()+ps.viernes.ToString()+ps.sabado.ToString()+ps.domingo.ToString()
                    }).ToList();

        }


        public IEnumerable<object> getPuntosServiciosForAjax(string dia,int unidad) {

            //var data1=(from ps in ctx.puntos_servicio where (int)ps.GetType().GetProperty(dia).GetValue(new object(),null)==1 select ps).ToList();

           var data = ctx.puntos_servicio.SqlQuery("SELECT * FROM dbo.puntos_servicio"+
            " LEFT JOIN dbo.contratos ON dbo.puntos_servicio.id_contrato=dbo.contratos.id_contrato "+
            "WHERE " + dia + "=1 AND dbo.contratos.id_unidad="+unidad.ToString()).ToList();
            return data;
        
        }

        public IEnumerable<PuntosServiciosView> GetPuntosSucursal(int sucursal)
        {


            var data = (from p in ctx.puntos_servicio
                        join c in ctx.contratos on p.id_contrato equals c.id_contrato
                        join s in ctx.sucursales on c.id_sucursal equals s.id_sucursal
                        where s.id_sucursal == sucursal
                        select new PuntosServiciosView()
                        {
                            id_punto_servicio = p.id_punto_servicio,
                            nombre_punto = p.nombre_punto,

                        });
            return data.ToList();

        }




        public puntos_servicio GetPuntosServiciosByID(int ps)
        {
                return this.ctx.puntos_servicio.Find(ps);
        }

        public void InsertPuntoServicio(puntos_servicio ps)
        {
            this.ctx.puntos_servicio.Add(ps);
        }

        public void DeletePuntoServicio(int ps)
        {
            puntos_servicio psi = this.ctx.puntos_servicio.Find(ps);
            this.ctx.puntos_servicio.Remove(psi);
        }

        public void UpdatePuntoServicio(puntos_servicio ps)
        {
            //this.ctx.Entry(ps).State = System.Data.EntityState.Modified;
            var old=this.ctx.puntos_servicio.Where(u => u.id_punto_servicio == ps.id_punto_servicio).FirstOrDefault();
            old.id_equipo = ps.id_equipo;
            old.cant_equipos = ps.cant_equipos;
            old.celular_contacto = ps.celular_contacto;
            old.domingo = ps.domingo;
            old.estado_punto = ps.estado_punto;
            old.email_contacto = ps.email_contacto;
            old.fono_contacto = ps.fono_contacto;
            old.id_contrato = ps.id_contrato;
            old.id_residuo = ps.id_residuo;
            old.id_tipo_tarifa = ps.id_tipo_tarifa;
            old.id_vertedero = ps.id_vertedero;
            old.lunes = ps.lunes;
            old.martes = ps.martes;
            old.miercoles = ps.miercoles;
            old.jueves = ps.jueves;
            old.viernes = ps.viernes;
            old.sabado = ps.sabado;
            old.domingo = ps.domingo;
            old.nombre_contacto = ps.nombre_contacto;
            old.nombre_punto = ps.nombre_punto;
            old.viernes = ps.viernes;
            old.codigo_externo_asignado = ps.codigo_externo_asignado;
            old.pago_por_peso = ps.pago_por_peso;
            old.pago_por_m3 = ps.pago_por_m3;
            old.cobro_por_peso = ps.cobro_por_peso;
            old.cobro_por_m3 = ps.cobro_por_m3;
            old.cobro_por_cantidad = ps.cobro_por_cantidad;
            old.cobro_por_banos = ps.cobro_por_banos;
            try
            {
                ctx.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }




        public void Save()
        {
            this.ctx.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<object> GetPuntoServicioPorSucursal(int idSucursal)
        {
            var data = (from ps in ctx.puntos_servicio
                join c in ctx.contratos on ps.id_contrato equals c.id_contrato
                where c.id_sucursal == idSucursal
                select new { ps.id_punto_servicio, ps.nombre_punto }).Distinct();

            return data;
        }
    }
}