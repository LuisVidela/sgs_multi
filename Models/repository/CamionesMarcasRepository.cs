﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.repository
{
    public class CamionesMarcasRepository
    {

         private sgsbdEntities ctx;


        public CamionesMarcasRepository(sgsbdEntities ctx)
        {

            this.ctx = ctx;
        
        }

        public IEnumerable<camiones_marcas> GetCamionesMarcas()
        {
            return ctx.camiones_marcas.ToList();
        }

        public camiones_marcas GetCamionMarcaByID(int cm)
        {
            return ctx.camiones_marcas.Find(cm);
        }

        public void InsertCamion(camiones_marcas cm)
        {
             ctx.camiones_marcas.Add(cm);
        }

        public void DeleteCamionMarca(int cm)
        {
            camiones_marcas c=ctx.camiones_marcas.Find(cm);
            ctx.camiones_marcas.Remove(c);
        }

        public void UpdateCamionMarca(camiones_marcas cm)
        {
            ctx.Entry(cm).State = System.Data.EntityState.Modified;
        }

        public void Save()
        {
            ctx.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}