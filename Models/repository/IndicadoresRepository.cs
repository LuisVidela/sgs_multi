﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.repository.interfaces;
using SGS.Models.repository;
using SGS.Models.modelViews;
using System.Data.Entity.Validation;

namespace SGS.Models.repository
{
    public class IndicadoresRepository
    {

        private sgsbdEntities ctx;

        public IndicadoresRepository(sgsbdEntities ctx)
        {
            this.ctx = ctx;
        }

        public IEnumerable<TB_SGS_INDICADOR> Get()
        {
            return this.ctx.TB_SGS_INDICADOR.ToList();
        }

        public TB_SGS_INDICADOR GetByID(Int32 codigo)
        {
            return ctx.TB_SGS_INDICADOR.Find(codigo);
        }

        public void Insert(TB_SGS_INDICADOR ps)
        {
            this.ctx.TB_SGS_INDICADOR.Add(ps);
            ctx.SaveChanges();
        }

        public void Update(TB_SGS_INDICADOR ps)
        {
            //this.ctx.Entry(ps).State = System.Data.EntityState.Modified;
            var old=this.ctx.TB_SGS_INDICADOR.Where(u => u.INDI_CODIGO == ps.INDI_CODIGO).FirstOrDefault();
            old.INDI_NOMBRE = ps.INDI_NOMBRE;
            old.INDI_DESCRIPCION = ps.INDI_DESCRIPCION;
            
            try
            {
                ctx.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public TB_SGS_INDICADOR_VALOR getCurrentValueIndicator(Int32 id, Int32 year, Int32 month)
        {
            return (from v in ctx.TB_SGS_INDICADOR_VALOR
                    where v.INDV_INDI_CODIGO == id &&
                          v.INDV_ANIO == year &&
                          v.INDV_MES == month
                    select v).FirstOrDefault();
        }
        public void InsertValueIndicator(TB_SGS_INDICADOR_VALOR ps)
        {
            this.ctx.TB_SGS_INDICADOR_VALOR.Add(ps);
            ctx.SaveChanges();
        }

        public void UpdateValueIndicator(TB_SGS_INDICADOR_VALOR ps)
        {
            //this.ctx.Entry(ps).State = System.Data.EntityState.Modified;
            var old = this.ctx.TB_SGS_INDICADOR_VALOR.Where(u => u.INDV_INDI_CODIGO == ps.INDV_INDI_CODIGO && u.INDV_ANIO == ps.INDV_ANIO && u.INDV_MES == ps.INDV_MES).FirstOrDefault();
            old.INDV_VALOR = ps.INDV_VALOR;

            try
            {
                ctx.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }

        public void Save()
        {
            this.ctx.SaveChanges();
        }


        private bool disposed = false;

        protected virtual void Dispose(bool dispose)
        {

            if (!this.disposed)
            {
                if (dispose)
                {
                    ctx.Dispose();
                }
            }
            this.disposed = true;
        }



        public void Dispose()
        {

            this.Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}