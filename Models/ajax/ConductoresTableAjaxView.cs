﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.ajax
{
    public class ConductoresTableAjaxView
    {


        public int id_conductor { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public string estado { get; set; }
        public string tipo_licencia1 { get; set; }
    }
}