﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.ajax;

namespace SGS.Models.ajax
{
    public class tableDataView
    {

        public int id_ruta { get; set; }
        public string nombre_ruta { get; set; }

        public virtual IEnumerable<tableDataHorasView> horas { get; set; }
    }
}