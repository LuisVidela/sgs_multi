﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.ajax
{
    public class CamionesTableAjaxView
    {
        public int id_camion { get; set; }
        public string camion { get; set; }
        public string patente { get; set; }
        public string tipo_camion { get; set; }
        public string capacidad_camion { get; set; }

        public string tipo_servicio1 { get; set; }
        public string marca { get; set; }
       
    }
}