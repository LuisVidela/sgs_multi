﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.ajax
{
    public class tableDataDetailsAjax
    {
        public int id_ruta_punto { get; set; }
        public string nombre_ruta { get; set; }
        public string dia_ruta { get; set; }
        public int id_ruta { get; set; }
        public int id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }

    }
}