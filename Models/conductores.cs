//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class conductores
    {
        public conductores()
        {
            this.restriccion_conductor_cliente = new HashSet<restriccion_conductor_cliente>();
        }
    
        public int id_conductor { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public Nullable<System.DateTime> fecha_ingreso { get; set; }
        public Nullable<int> id_tipo_licencia { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public Nullable<int> id_franquiciado { get; set; }
        public Nullable<System.DateTime> fecha_vencimientolic { get; set; }
    
        public virtual ICollection<restriccion_conductor_cliente> restriccion_conductor_cliente { get; set; }
        public virtual franquiciado franquiciado { get; set; }
        public virtual tipo_licencia tipo_licencia { get; set; }
    }
}
