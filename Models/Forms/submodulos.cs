﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(submodulosForm))]
    public partial class submodulos
    {
    }


    public class submodulosForm
    {
        [Key]
        public int id_submodulo { get; set; }

    }
}