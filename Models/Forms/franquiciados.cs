﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace SGS.Models.Forms
{
    [MetadataType(typeof(FranquiciadosForm))]
    public partial class Franquiciados
    {
    }


    public class FranquiciadosForm
    {
        public int id_franquiciado { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public double id_comuna { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string razon_social { get; set; }
        public System.DateTime fecha_contrato { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public string direccion { get; set; }
        public string rut_franquiciado { get; set; }


    }
}