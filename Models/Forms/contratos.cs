﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(contratosForm))]
    public partial class contratos
    {
    }


    public class contratosForm
    {
        [Required(ErrorMessage="Campo Requerido")]
        public System.DateTime fecha_inicio { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public Nullable<System.DateTime> fecha_renovacion { get; set; }
     
    }
}