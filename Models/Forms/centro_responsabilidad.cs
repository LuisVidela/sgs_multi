﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(centro_responsabilidadForm))]
    public partial class centro_responsabilidad
    {
    }


    public class centro_responsabilidadForm
    {
        [Key]
        public int id_centro { get; set; }

        [Required(ErrorMessage = "Requerido")]
        [MinLength(3,ErrorMessage="Minimo 3 caracteres")]
        [MaxLength(20,ErrorMessage="Maximo 20 caracteres")]
        public string centro_responsabilidad1 { get; set; }

    }
}