﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(conductoresForm))]
    public partial class conductores
    {
    }


    public class conductoresForm {

        [Required(ErrorMessage="Requerido")]
        public string nombre_conductor { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string rut_conductor { get; set; }

        public System.DateTime fecha_ingreso { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public int id_tipo_licencia { get; set; }
        public System.DateTime fecha_vencimientolic { get; set; }
     
     
        [Required(ErrorMessage = "Requerido")]
        public string estado { get; set; }
        public string observacion { get; set; }
 
    
    }

}