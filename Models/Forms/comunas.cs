﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(comunasForm))]
    public partial class comunas
    {
    }


    public class comunasForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string nombre_comuna { get; set; }

    }
}