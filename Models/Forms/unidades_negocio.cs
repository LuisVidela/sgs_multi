﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(unidades_negocioForm))]
    public partial class unidades_negocio
    {
    }


    public class unidades_negocioForm {
        [Required(ErrorMessage="Requerido")]
        public string unidad { get; set; }

        [Required(ErrorMessage = "Requerido")]
        [MinLength(2,ErrorMessage="Minimo 2 caracteres")]
        [MaxLength(9,ErrorMessage="Maximo 9 caracteres")]
        public string codigo_unidad { get; set; }
    
    }
}