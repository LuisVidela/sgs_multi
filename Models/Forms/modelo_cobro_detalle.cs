﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(modelo_cobro_detalleForm))]
    public partial class modelo_cobro_detalle
    {
    }



    public class modelo_cobro_detalleForm {

        public Nullable<int> rango_minimo { get; set; }
        public Nullable<int> rango_maximo { get; set; }
       [Required(ErrorMessage="Valor Requerido")]
        public Nullable<int> valor { get; set; }
        //public bool uf { get; set; }
        public int id_tipo_moneda { get; set; }
    }
}