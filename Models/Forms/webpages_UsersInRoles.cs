﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(webpages_UsersInRolesForm))]
    public partial class webpages_UsersInRoles
    {
    }


    public class webpages_UsersInRolesForm
    {
        [Key]
        public int UserId { get; set; }

    }
}