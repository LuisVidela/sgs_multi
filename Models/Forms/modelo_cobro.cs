﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(modelo_cobroForm))]
    public partial class modelo_cobro
    {
    }


    public class modelo_cobroForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string modelo { get; set; }
        public int id_punto_servicio { get; set; }
        public int id_tipo_modelo { get; set; }

    }
}