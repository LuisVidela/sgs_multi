﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(herramientasForm))]
    public partial class herramientas
    {
    }


    public class herramientasForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string herramienta { get; set; }

    }
}