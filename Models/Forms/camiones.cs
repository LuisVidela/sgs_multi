﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SGS.lib;

namespace SGS.Models
{
    [MetadataType(typeof(camionesForm))]
    public partial class camiones
    {
    }


    class camionesForm{    
         public int id_camion { get; set; }
        [Required(ErrorMessage="Requerido")]
        [MinLength(2, ErrorMessage = "Minimo 2 caracteres")]

         public  string camion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [StringLength(6,ErrorMessage="6 caracteres (Patente)",MinimumLength=6)]
         public string patente { get; set; }
         public string tipo_camion { get; set; }
         public string modelo_camion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [MinLength(2,ErrorMessage="Minimo 2 caracteres")]
         public string capacidad_camion { get; set; }

        public int id_camion_marca { get; set; }
        public string observacion { get; set; }
        public string estado { get; set; }

        public int id_tipo_servicio { get; set; }
        public bool prop_resiter { get; set; }
        public bool prop_franquiciado { get; set; }
        public bool prop_otro { get; set; }
        public System.DateTime fecha_rvt { get; set; }
        //public string resolucion_transporte { get; set; }



    }
    
}