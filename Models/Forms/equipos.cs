﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(equiposForm))]
    public partial class equipos
    {
    }


    public class equiposForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string tipo_equipo { get; set; }

        [Required(ErrorMessage = "Requerido")]
        public string capacidad_equipo { get; set; }

    }
}