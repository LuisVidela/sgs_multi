﻿using System.ComponentModel.DataAnnotations;


namespace SGS.Models.Forms
{
    public class ItemReporteForm
    {
        public int IdItemReporte { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public int OrdenItem { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string NombreItem { get; set; }
        public int  IdUmb { get; set; }
        public bool MuestraManejoDris { get; set; }
    }
}