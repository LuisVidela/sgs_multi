﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SGS.lib;

namespace SGS.Models
{
    [MetadataType(typeof(webpages_RolesForm))]
    public partial class webpages_Roles
    {
    }


    class webpages_RolesForm
    {
        [Key]
        public int RoleId { get; set; }
     
    }
    
}