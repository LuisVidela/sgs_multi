﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(clientesForm))]
    public partial class clientes
    {
    }


    public class clientesForm{
        public int id_cliente { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string rut_cliente { get; set; }
        [Required(ErrorMessage = "Requerido")]
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string direccion_cliente { get; set; }
        public bool cobro_especial { get; set; }
        public int id_pais { get; set; }
        public double fono_comercial { get; set; }
        public string giro_comercial { get; set; }
        public string nombre_contacto { get; set; }

        
        public string email_contacto { get; set; }
 



    }
}