﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(ruta_modeloForm))]
    public partial class ruta_modelo
    {
    }


    public class ruta_modeloForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string nombre_ruta { get; set; }

        [Required(ErrorMessage = "Requerido")]
        public System.DateTime fecha_ruta { get; set; }

    }
}