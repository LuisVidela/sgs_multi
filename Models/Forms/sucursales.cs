﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGS.Models
{
    [MetadataType(typeof(sucursalesForm))]
    public partial class sucursales
    {
    }


    public class sucursalesForm {

        public int id_sucursal { get; set; }
        [Required(ErrorMessage="Requerido")]
        public string sucursal { get; set; }
        public string descripcion { get; set; }
        public string direccion { get; set; }
        public string contacto_sucursal { get; set; }
        public string email_sucursal { get; set; }
        public double fono_sucursal { get; set; }
        public double celular_sucursal { get; set; }
        public string zona { get; set; }
        public double periodo_desde { get; set; }
        public double periodo_hasta { get; set; }
        public string estado_sucursal { get; set; }


    }
}