﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(usuariosForm))]
    public partial class usuarios
    {
    }


    public class usuariosForm
    {
        [Key]
        public int id_user { get; set; }

        [Required(ErrorMessage = "Requerido")]
        //[MinLength(9, ErrorMessage = "Minimo 8")]
        public string user { get; set; }
        [Required(ErrorMessage = "Requerido")]
        //[MinLength(7,ErrorMessage="Minimo 6")]
        public string password { get; set; }

    }
}