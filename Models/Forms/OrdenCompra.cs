﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using SGS.lib;

namespace SGS.Models
{
    [MetadataType(typeof(OrdenCompraForm))]
    public partial class orden_de_compra
    {
    }


    class OrdenCompraForm
    {
        [Required(ErrorMessage = "Requerido")]
        public int id_cliente { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public System.DateTime fecha_instalacion { get; set; }

        [Required(ErrorMessage = "Campo Requerido")]
        public Nullable<System.DateTime> fecha_retiro { get; set; }

        //[Required(ErrorMessage = "Requerido")]
        //public int bano { get; set; }


    }

}