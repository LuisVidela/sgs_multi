﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace SGS.Models
{
    [MetadataType(typeof(rubrosForm))]
    public partial class rubros
    {
    }


    public class rubrosForm
    {
        [Required(ErrorMessage = "Requerido")]
        public string rubro { get; set; }

    }
}