﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SGS.Models
{
    [MetadataType(typeof(webpages_MembershipForm))]
    public partial class webpages_Membership
    {
    }


    public class webpages_MembershipForm
    {
        [Key,ForeignKey("UserProfile")]
        public int UserId { get; set; }

    }
}