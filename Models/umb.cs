//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class umb
    {
        public umb()
        {
            this.item_reporte_unificado = new HashSet<item_reporte_unificado>();
        }
    
        public int id_umb { get; set; }
        public string nombre { get; set; }
    
        public virtual ICollection<item_reporte_unificado> item_reporte_unificado { get; set; }
    }
}
