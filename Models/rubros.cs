//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class rubros
    {
        public rubros()
        {
            this.sucursales = new HashSet<sucursales>();
            this.clientes = new HashSet<clientes>();
        }
    
        public int id_rubro { get; set; }
        public string rubro { get; set; }
    
        public virtual ICollection<sucursales> sucursales { get; set; }
        public virtual ICollection<clientes> clientes { get; set; }
    }
}
