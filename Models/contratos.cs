//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class contratos
    {
        public contratos()
        {
            this.TB_SGS_REAJUSTE = new HashSet<TB_SGS_REAJUSTE>();
            this.puntos_servicio = new HashSet<puntos_servicio>();
        }
    
        public int id_contrato { get; set; }
        public int id_tipo_contrato { get; set; }
        public int id_cliente { get; set; }
        public int id_sucursal { get; set; }
        public System.DateTime fecha_inicio { get; set; }
        public Nullable<System.DateTime> fecha_renovacion { get; set; }
        public string estado { get; set; }
        public Nullable<int> id_modelo_cobro { get; set; }
        public Nullable<int> id_centro_responsabilidad { get; set; }
        public Nullable<int> id_unidad { get; set; }
    
        public virtual centro_responsabilidad centro_responsabilidad { get; set; }
        public virtual tipo_contrato tipo_contrato { get; set; }
        public virtual ICollection<TB_SGS_REAJUSTE> TB_SGS_REAJUSTE { get; set; }
        public virtual sucursales sucursales { get; set; }
        public virtual clientes clientes { get; set; }
        public virtual ICollection<puntos_servicio> puntos_servicio { get; set; }
    }
}
