//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class V_CALCULO_PUNTO
    {
        public int id_calculo { get; set; }
        public Nullable<int> Id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public Nullable<int> id_contrato { get; set; }
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string Sucursal { get; set; }
        public Nullable<int> id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public int Numero_Retiros { get; set; }
        public Nullable<decimal> Valor { get; set; }
        public Nullable<int> IVA { get; set; }
        public Nullable<int> NETO { get; set; }
        public Nullable<double> Peso { get; set; }
        public string Fecha { get; set; }
        public Nullable<int> Monthis { get; set; }
        public Nullable<int> Yearis { get; set; }
        public Nullable<int> id_sucursal { get; set; }
        public decimal ValorTotalDisposicion { get; set; }
        public Nullable<decimal> ValorTotal { get; set; }
        public int Numero_No_Retiros { get; set; }
        public Nullable<int> id_unidad { get; set; }
        public Nullable<int> id_tipo_modalidad { get; set; }
    }
}
