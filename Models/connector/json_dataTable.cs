﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.connector
{
    public class json_dataTable
    {

        public string sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public List<List<string>> aaData { get; set; }

    }


    public class json_dataTable2
    {

        public string sEcho { get; set; }
        public int iTotalRecords { get; set; }
        public int iTotalDisplayRecords { get; set; }
        public List<subData> aaData { get; set; }

    }


    public class subData
    {

        public string fecha { get; set; }
        public string hoja_ruta { get; set; }
        public string punto { get; set; }
        public string sistema { get; set; }
        public string patente { get; set; }
        public string franquiciado { get; set; }
        public string chofer { get; set; }
        public string estado_punto { get; set; }
        public string volteos { get; set; }
        public string cobro { get; set; }
        public string pago { get; set; }
        public string margen { get; set; }
        public string margen_ciento { get; set; }

    }
}


