//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    
    public partial class SP_SEL_informe_vertederos1_Result
    {
        public Nullable<System.DateTime> fecha { get; set; }
        public string rut { get; set; }
        public string razonsocial { get; set; }
        public string servicio { get; set; }
        public double m3 { get; set; }
        public Nullable<int> nro_guia { get; set; }
        public string patente { get; set; }
        public string chofer { get; set; }
        public string industria { get; set; }
        public string tipo_residuo { get; set; }
        public string vertedero { get; set; }
    }
}
