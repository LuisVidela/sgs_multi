﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class MantenedorUfView
    {
        public bool puedeAgregar { get; set; }
        public List<tablaUFView> tabla { get; set; }
    }
    public class crearUFView
    {
        public int idUF { get; set; }
        public string uf { get; set; }
        public selectListPeriodoUF SelectListPeriodo { get; set; }
    }
    public class tablaUFView
    {
        public int idUF { get; set; }
        public string valorUF { get; set; }
        public string periodo { get; set; }
        public bool editable { get; set; }
    }
    public class selectListPeriodoUF
    {
        public Nullable<int> periodoSelect { get; set; }
        public IEnumerable<SelectListItem> periodos { get; set; }
    }
}