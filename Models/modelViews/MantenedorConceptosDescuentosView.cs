﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class MantenedorConceptosDescuentosView
    {
        public List<tablaConceptosdescuentos> tabla { get; set; }
    }
    public class tablaConceptosdescuentos
    {
        public int id { get; set; }
        public string concepto { get; set; }
    }
    public class crearConceptoDescuentoView
    {
        public int id { get; set; }
        public string concepto { get; set; }
    }

}