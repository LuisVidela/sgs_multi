﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class CamionesView
    {

        public int id_camion { get; set; }
        public string camion { get; set; }
        public string patente { get; set; }
        public string tipo_servicio1 { get; set; }
        public string estado { get; set; }
        public string nombre_franquiciado { get; set; }
        public string n_interno { get; set; }
        //public string resolucion_transporte { get; set; }

        public IEnumerable<restriccion_camion_cliente> restricciones { get; set; }
    }
    public class crearCamionView
    {
        public int id_camion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [MinLength(2, ErrorMessage = "Minimo 2 caracteres")]

        public string camion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [StringLength(6, ErrorMessage = "6 caracteres (Patente)", MinimumLength = 6)]
        public string patente { get; set; }
        public string tipo_camion { get; set; }
        public string modelo_camion { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [MinLength(2, ErrorMessage = "Minimo 2 caracteres")]
        public string capacidad_camion { get; set; }

        public int id_camion_marca { get; set; }
        public string observacion { get; set; }
        public string estado { get; set; }

        public string num_interno { get; set; }
        public int id_tipo_servicio { get; set; }
        public int id_franquiciado { get; set; }
        public Nullable<bool> prop_resiter { get; set; }
        public Nullable<bool> prop_franquiciado { get; set; }
        public Nullable<bool> prop_otro { get; set; }
        public System.DateTime fecha_rvt { get; set; }

    }
}