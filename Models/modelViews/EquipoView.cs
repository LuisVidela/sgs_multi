﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class EquipoView
    {
        public int id_punto_servicio { get; set; }

        public Nullable<System.Int32> id_equipo { get; set; }

        public string Equipo { get; set; }

        public string nom_nemo { get; set; }

    }
}