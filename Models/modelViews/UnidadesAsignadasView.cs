﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class UnidadesAsignadasView
    {
        public int id_usuarioxunidad { get; set; }
        public int id_usuario{get;set;}
        public bool predeterminada{get;set;}
        public int id_unidad{get;set;}
        public string nombre_unidad { get; set; }
    }

}