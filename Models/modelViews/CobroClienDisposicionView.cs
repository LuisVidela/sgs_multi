﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class CobroClienDisposicionView
    {
        public int id_punto_servicio { get; set; }
        public string Fecha { get; set; }
        public Nullable<System.Int32> id_cliente { get; set; }
        public Nullable<System.Decimal> Disposicion { get; set; }
        public Nullable<System.Decimal> Valor { get; set; }

    }
}