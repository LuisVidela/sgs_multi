﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SGS.Models.Forms;

namespace SGS.Models.modelViews
{
    public class ProcesoPagoView
    {
        public string razon_social {get;set;}
        public string id_franquiciado {get;set;}
        public string tipo_servicio{get;set;}
        public string camion {get;set;}
        public string valor_tarifa { get; set; }
        public string monto_calculado { get; set; }
    }
}