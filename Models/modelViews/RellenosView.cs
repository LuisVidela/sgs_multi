﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class RellenosView
    {
        public int id_vertedero { get; set; }
        public string vertedero { get; set; }
        public string pais { get; set; }
        public string region { get; set; }
        public string comuna { get; set; }
        public string ciudad { get; set; }
        public string direccion { get; set; }
    }
}