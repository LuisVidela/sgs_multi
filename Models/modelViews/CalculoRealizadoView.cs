﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace SGS.Models.modelViews
{
    public class CalculoRealizadoView
    {
        public int id_calculo { get; set; }
        public string Fecha { get; set; }
        public int Cerrado { get; set; }

    }
}