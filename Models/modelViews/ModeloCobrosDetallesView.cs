﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ModeloCobrosDetallesView
    {

        public int id_modelo_cobro_detalle { get; set; }
        public int id_modelo_cobro { get; set; }
        public Nullable<int> rango_minimo { get; set; }
        public Nullable<int> rango_maximo { get; set; }
        public Nullable<double> valor { get; set; }
        public string tipo_modalidad1 { get; set; }

    }
}