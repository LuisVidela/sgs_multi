﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class FranquiciadosDescuentosView
    {
        public int id_descuento { get; set; }
        public Nullable<int> neto  { get; set; }
	    public Nullable<int> iva   { get; set; }
	    public Nullable<int>  total   { get; set; }
	    
    }
}