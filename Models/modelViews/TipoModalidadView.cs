﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class TipoModalidadView
    {
        public int id_tipo_modalidad { get; set; }
        public string tipo_modalidad { get; set; }
    }
}