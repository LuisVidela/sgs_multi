﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class UsuariosView
    {

        public int id_usuario { get; set; }
        public string usuario { get; set; }
        public string password { get; set; }

    }
    public class viewUsuarios
    {
        public List<tablaUsuariosView> row { get; set; }
    }
    public class tablaUsuariosView
    {
        public int idUsuario { get; set; }
        public string userName { get; set; }
        public string nombre { get; set; }
        public string perfil { get; set; }
        public int estado { get; set; }
    }
    public class crearPerfilView
    {
        public int idPerfil { get; set; }
        public string nombre { get; set; }

        public bool conCli { get; set; }
        public bool conCliAgregar { get; set; }
        public bool conCliEditar { get; set; }
        public bool conCliExportar { get; set; }
        public bool conCliEliminar { get; set; }

        public bool conSuc { get; set; }
        public bool conSucAgregar { get; set; }
        public bool conSucEditar { get; set; }
        public bool conSucEliminar { get; set; }
        public bool conSucExportar { get; set; }

        public bool conCon { get; set; }
        public bool conConAgregar { get; set; }
        public bool conConEditar { get; set; }
        public bool conConEliminar { get; set; }
        public bool conConExportar { get; set; }

        public bool conMod { get; set; }
        public bool conModAgregar { get; set; }
        public bool conModEditar { get; set; }
        public bool conModEliminar { get; set; }
        public bool conModExportar { get; set; }

        public bool conPun { get; set; }
        public bool conPunAgregar { get; set; }
        public bool conPunEditar { get; set; }
        public bool conPunEliminar { get; set; }

        public bool franFran { get; set; }
        public bool franFranAgregar { get; set; }
        public bool franFranEditar { get; set; }
        public bool franFranEliminar { get; set; }
        public bool franFranExportar { get; set; }

        public bool franLea { get; set; }
        public bool franLeaAgregar { get; set; }
        public bool franLeaEditar { get; set; }
        public bool franLeaEliminar { get; set; }
        public bool franLeaExportar { get; set; }

        public bool franDesc { get; set; }
        public bool franDescAgregar { get; set; }
        public bool franDescEditar { get; set; }
        public bool franDescEliminar { get; set; }
        public bool franDescExportar { get; set; }

        public bool franProc { get; set; }
        public bool franProcPrepagar { get; set; }
        public bool franProcPagar { get; set; }
        public bool franProcExportar { get; set; }

        public bool franHist { get; set; }
        public bool franHistExportar { get; set; }

        public bool manCon { get; set; }
        public bool manConAgregar { get; set; }
        public bool manConEditar { get; set; }
        public bool manConEliminar { get; set; }
        public bool manConRestriccion { get; set; }
        public bool manConExportar { get; set; }

        public bool manCam { get; set; }
        public bool manCamAgregar { get; set; }
        public bool manCamEditar { get; set; }
        public bool manCamEliminar { get; set; }
        public bool manCamRestriccion { get; set; }
        public bool manCamExportar { get; set; }

        public bool manUN { get; set; }
        public bool manUNAgregar { get; set; }
        public bool manUNEditar { get; set; }
        public bool manUNEliminar { get; set; }
        public bool manUNExportar { get; set; }

        public bool manComu { get; set; }
        public bool manComuAgregar { get; set; }
        public bool manComuEditar { get; set; }
        public bool manComuEliminar { get; set; }

        public bool manEqui { get; set; }
        public bool manEquiAgregar { get; set; }
        public bool manEquiEditar { get; set; }
        public bool manEquiEliminar { get; set; }

        public bool manHerr { get; set; }
        public bool manHerrAgregar { get; set; }
        public bool manHerrEditar { get; set; }
        public bool manHerrEliminar { get; set; }

        public bool manRub { get; set; }
        public bool manRubAgregar { get; set; }
        public bool manRubEditar { get; set; }
        public bool manRubEliminar { get; set; }


        public bool manUF { get; set; }
        public bool manUFAgregar { get; set; }
        public bool manUFEditar { get; set; }

        public bool manDesc { get; set; }
        public bool manDescAgregar { get; set; }
        public bool manDescEditar { get; set; }

        public bool planMod { get; set; }
        public bool planModAgregar { get; set; }
        public bool planModEditar { get; set; }
        public bool planModEliminar { get; set; }

        public bool planHR { get; set; }
        public bool planHRModEditar { get; set; }

        public bool planPC { get; set; }

        public bool planCC { get; set; }

        public bool adminUsu { get; set; }
        public bool adminUsuAgregar { get; set; }
        public bool adminUsuEditar { get; set; }
        public bool adminUsuEliminar { get; set; }

        public bool adminPerf { get; set; }
        public bool adminPerfAgregar { get; set; }
        public bool adminPerfEditar { get; set; }
        public bool adminPerfEliminar { get; set; }
    }
}