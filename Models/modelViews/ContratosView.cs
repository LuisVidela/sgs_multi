﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ContratosView
    {
        public int id_contrato { get; set; }
        public string tipo_contrato { get; set; }
        public string razon_social { get; set; }
        public string sucursal { get; set; }
        public DateTime? fecha_inicio { get; set; }
        public DateTime? fecha_renovacion { get; set; }
        public string estado { get; set; }
        public string categoria { get; set; }
        public string unidad_negocio { get; set; }
        public string dias { get; set; }
    }
}