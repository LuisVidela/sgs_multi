﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class DataCalculosFullView
    {
        public int idData { get; set; }
        public string fechacorta { get; set; }
        public string nombre_punto { get; set; }
        public string sistema { get; set; }
        public string camion { get; set; }
        public string franquiciado { get; set; }
        public string conductor { get; set; }
        public string estadopunto { get; set; }
        public string relleno { get; set; }
        public Nullable<System.Int32> cantidad { get; set; }
        public Nullable<System.Int32> bruto { get; set; }
        public Nullable<System.Int32> Pago { get; set; }
        public Nullable<System.Int32> Margen { get; set; }
        public Nullable<System.Int32> MargenPorc { get; set; }
        public Nullable<System.Int32> id_hoja_ruta { get; set; }
        public Nullable<System.Int32> id_punto_servicio { get; set; }
        public Nullable<System.DateTime> fechaFull { get; set; }

    }
}