﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class MantenedorHDRDView
    {

        public int  id_hoja_ruta { get; set; }
        public int orden { get; set; }
        public int id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public Nullable<int> cantidad { get; set; }
        public Nullable<double>  peso { get; set; }
        public Nullable<int>  estado { get; set; }
        public Nullable<int>  tipo_contrato1 { get; set; }
        public Nullable<int>  id_tipo_contrato { get; set; }
        public string tipo_equipo { get; set; }
        public string equipoentrada { get; set; }
        public string equiposalida { get; set; }
        public string horainicio { get; set; }
        public string horatermino { get; set; }
        public string relleno { get; set; }
        public string observacion { get; set; }
        public string vertedero { get; set; }
        public bool codigo_externo_asignado { get; set; }
        public Nullable<int> cant_equipos { get; set; }
        public string vertededordefecto { get; set; }
        public Nullable<int> codigo_externo { get; set; }
        public Nullable<int> PORL_CODIGO { get; set; }
        public Nullable<Boolean> peso_calculado { get; set; }
        public Nullable<Double> densidad { get; set; }
        public Nullable<Double> densidadporDefecto { get; set; }
        public Double capacidad { get; set; }
        public bool con_carro {get; set; }
        public Nullable<int> guia_concarro { get; set; }
        public Nullable<double> peso_concarro { get; set; }
        public int sucursal { get; set; }
        public bool costos_por_m3 { get; set; }
        public bool costos_por_peso { get; set; }
        public bool costos_por_cantidad { get; set; }
        public bool costos_por_bano { get; set; }
        public Nullable<double> cantidad_bano { get; set; }
    }
}