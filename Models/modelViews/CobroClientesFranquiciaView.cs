﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class CobroClientesFranquiciaView
    {
        public int id_calculo { get; set; }
        public Nullable<System.Int32> id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public string razon_social { get; set; }
        public Nullable<System.Int32> cantidad { get; set; }
        public Nullable<System.Int32> valor { get; set; }
        public Nullable<System.Int32> pagofranquiciado { get; set; }
        public Nullable<System.Int32> ValorTotalDisposicion { get; set; }
        public Nullable<System.Int32> ValorTotal { get; set; }
        public Nullable<System.Int32> margen { get; set; }
        public Nullable<System.Decimal> margenporcer { get; set; }
    }
    public class CobroClientesFranquiciaViewFinal
    {
        public DotNet.Highcharts.Highcharts chart { get; set; }
        public List<CobroClientesFranquiciaView> tabla { get; set; }
    }
    public class grafico
    {
        public string cliente { get; set; }
        public int count { get; set; }
    }
}