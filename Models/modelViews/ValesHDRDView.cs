﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ValesHDRDView
    {

        public int id_hoja_ruta { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public string patente { get; set; }
        public string num_interno { get; set; }
        public string Tipo_Contenedor { get; set; }
        public string punto_servicio { get; set; }
        public string Vertedero { get; set; }
        public DateTime Fecha { get; set; }
        public bool carro { get; set; }


    }
}