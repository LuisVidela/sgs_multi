﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class TipoServicioView
    {
        public int id_tipo_servicio { get; set; }
        public string tipo_servicio { get; set; }
    }
}