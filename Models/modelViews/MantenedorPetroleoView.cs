﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class MantenedorPetroleoView
    {
    }
    public class crearPetroleoView
    {
        public int idPetroleo { get; set; }
        public string valorPetroleo { get; set; }
        public selectListCustom periodo { get; set; }
    }
    public class petroleoIndex
    {
        public bool puedeAgregar { get; set; }
        public IEnumerable<petroleosRow> tabla { get; set; }
    }
    public class petroleosRow
    {
        public int id { get; set; }
        public string periodo { get; set; }
        public string valor { get; set; }
        public bool editable { get; set; }
    }
    public class selectListCustom
    {
        public int select { get; set; }
        public IEnumerable<SelectListItem> items { get; set; }
    }
}