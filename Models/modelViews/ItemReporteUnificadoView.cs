﻿
namespace SGS.Models.modelViews
{
    public class ItemReporteUnificadoView
    {
        public int IdItemPeriodo { get; set; }
        public int OrdenItem { get; set; }
        public string NombreItem { get; set; }
        public string NombreUmb { get; set; }
        public bool MuestraManejoDris { get; set; }
    }
}