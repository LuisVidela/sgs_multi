﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoSucursalView
    {
        public int id_punto_servicio { get; set; }
        public int id_contrato { get; set; }
        public string razon_social { get; set; }
        public string sucursal { get; set; }

    }
}