﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ClientesView
    {
        public int id_cliente { get; set; }

        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string rut_cliente { get; set; }
        public string direccion_cliente { get; set; }
        public string categoria { get; set; }
    }
}