﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class DescuentosMasivos
    {
        public List<rowDescuentosMasivos> tabla { get; set; }
    }
    public class rowDescuentosMasivos
    {
        public int idCamion { get; set; }
        public string patenteCamion { get; set; }
        public string nombreFranquiciado { get; set; }
        public string ampliroll { get; set; }
        public string seguro { get; set; }
        public string tagValor { get; set; }
        public string anticipo { get; set; }
        public string litrosPetroleo{get;set;}
        public string obsTag { get; set; }
        public string repuestoValor { get; set; }
        public string repuestoFactura { get; set; }
        public string repuestoObs { get; set; }
    }
    public class DescuentosView
    {
        public franquiciado franquiciado { get; set; }
        public int idFranquiciado { get; set; }
        public bool agregarMasivo { get; set; }
        public List<DescuentosViewTablaEncabezado> encabezado { get; set; }
        public List<DescuentosViewTablaDetalle> detalle { get; set; }
        public selectListEstados selectListEstados { get; set; }
        public selectListFranquiciadosFiltro selecListFranquiciados { get; set; }
    }
    public class LeasingView
    {
        public bool puedeAgregar { get; set; }
        public selectListFranquiciadosFiltro selecListFranquiciados { get; set; }
        public selectListEstados selectListEstados { get; set; }
        public List<leasingTablaEncabezado> encabezadoLeasing { get; set; }
    }
    public class DescuentosViewTablaEncabezado
    {
        public int id_descuento { get; set; }
        public int id_franquiciado { get; set; }
        public string razonSocial { get; set; }
        public string pateneteCamion { get; set; }
        public int id_camion { get; set; }
        public int cuotas { get; set; }
        public string id_tipo_descuento { get; set; }
        public double neto { get; set; }
        public double iva { get; set; }
        public int cuotasPagadas { get; set; }
        public string oc { get; set; }
        public string factura { get; set; }
        public double total { get; set; }
        public bool enUF { get; set; }
        public string concepto { get; set; }
        public DateTime fecha { get; set; }
        public string estado_descuento { get; set; }
        public bool editable { get; set; }
    }
    public class DescuentosViewTablaDetalle
    {
        public int idDescuento { get; set; }
        public int idDetalle { get; set; }
        public int cuota { get; set; }
        public string valorPagar { get; set; }
        public string valorPagado { get; set; }
        public string valorCuota { get; set; }
        public bool editable { get; set; }
        public string saldo { get; set; }
        public string estado { get; set; }
        public DateTime fecha { get; set; }
    }
    public class selectListEstados
    {
        public int estadoSelect { get; set; }
        public IEnumerable<SelectListItem> estados { get; set; }
    }
    public class selectListUN
    {
        public int UNSelect { get; set; }
        public IEnumerable<SelectListItem> UN { get; set; }
    }
    public class selectListPerfiles
    {
        public int perfilSelect { get; set; }
        public IEnumerable<SelectListItem> perfiles { get; set; }
    }
    public class selectListCR
    {
        public int CRSelect { get; set; }
        public IEnumerable<SelectListItem> CR { get; set; }
    }
    public class CrearDescuentoView
    {
        public int idCamion { get; set; }
        public camiones camion { get; set; }
        public string neto { get; set; }
        public selectListCamiones fechaPrimeraCuota { get; set; }
        public string observacion { get; set; }
        public string total { get; set; }
        public string iva { get; set; }
        public bool llevaIVA { get; set; }
        public int idFranquiciado { get; set; }
        public selectListConceptosDescuentos conceptoSelectList { get; set; }
        public selectListFranquiciados franquiciadosSelectList { get; set; }
        public selectListCamiones camionesSelectList { get; set; }
        public DateTime fecha { get; set; }
        public int cuotas { get; set; }
        public int ok { get; set; }
        public string factura { get; set; }
        public string oc { get; set; }
        public int idDescuento { get; set; }
        public bool enUF { get; set; }
    }
    public class CrearLeasingView
    {
        public int idFranquiciado { get; set; }
        public selectListFranquiciados franquiciadosSelectList { get; set; }
        public selectListCamiones camionesSelectList { get; set; }
        public selectListLeasing fechaPrimeraCuota { get; set; }
        public int idCamion { get; set; }
        public camiones camion { get; set; }
        public string valorUfDia { get; set; }
        public string interes { get; set; }
        public string neto { get; set; }
        public string iva { get; set; }
        public string total { get; set; }
        public string totalUF { get; set; }
        public string pie { get; set; }
        public string pieUF { get; set; }
        public int cuotas { get; set; }
        public int ok { get; set; }
        public int idLeasing { get; set; }
        public string cuotaFinal { get; set; }
    }
    public class detalleDescuentoView
    {
        public descuento descuento { get; set; }
        public camiones camion { get; set; }
        public int pagar { get; set; }
        public List<DescuentosViewTablaDetalle> detalle { get; set; }
    }
    public class detalleLeasingView
    {
        public leasing leasing { get; set; }
        public camiones camion { get; set; }
        public List<leasingTablaDetalle> detalle { get; set; }
    }
    public class selectListConceptosDescuentos
    {
        public int conceptoSelect { get; set; }
        public IEnumerable<SelectListItem> conceptos { get; set; }
    }
    public class selectListCamiones
    {
        public int camionSelect { get; set; }
        public IEnumerable<SelectListItem> camiones { get; set; }
    }

    public class selectListLeasing
    {
        public int leasingSelect { get; set; }
        public IEnumerable<SelectListItem> leasing { get; set; }
    }

    public class selectListFranquiciados
    {
        public int franquiciadoSelect { get; set; }
        public IEnumerable<SelectListItem> franquiciados { get; set; }
    }
    public class selectListFranquiciadosFiltro
    {
        public Nullable<int> franquiciadoSelect { get; set; }
        public IEnumerable<SelectListItem> franquiciados { get; set; }
    }
    public class leasingTablaEncabezado
    {
        public int idLeasing { get; set; }
        public double valorUfDia { get; set; }
        public double interes { get; set; }
        public double iva { get; set; }
        public double totalUF { get; set; }
        public int total { get; set; }
        public int neto { get; set; }
        public bool puedeEditar { get; set; }
        public string patenteCamion { get; set; }
        public double pie { get; set; }
        public int cuotas { get; set; }
        public string fecha { get; set; }
        public string estado { get; set; }
    }
    public class leasingTablaDetalle
    {
        public int idLeasing { get; set; }
        public int idDetalle { get; set; }
        public int cuota { get; set; }
        public string fechaCuota { get; set; }
        public string cuotaNeta { get; set; }
        public string cuotaBruta { get; set; }
        public string saldo { get; set; }
        public string valorPagado { get; set; }
        public string valorPagar { get; set; }
        public string estado { get; set; }
        public bool editable { get; set; }
    }
    public class editarPagoLeasingView
    {
        public string valorPagar { get; set; }
        public string valorCuota { get; set; }
        public string valorPagado { get; set; }
        public int idDetalle { get; set; }
        public string saldo { get; set; }
        public int idLeasing { get; set; }
    }
    public class editarPagoDescuentoView
    {
        public descuento descuento { get; set; }
        public string valorPagar { get; set; }
        public string valorCuota { get; set; }
        public string valorPagado { get; set; }
        public int idDetalle { get; set; }
        public string saldo { get; set; }
        public int idDescuento { get; set; }
    }
}