﻿
namespace SGS.Models.modelViews
{
    public class PuntoServicioReporteUnificadoView
    {
        public int IdPuntoServicio { get; set; }
        public string NombrePuntoServicio { get; set; }
        public int? IdItemPeriodo { get; set; }
    }
}