﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoServicioDescView
    {

        public int id_punto_servicio { get; set; }
        public Nullable<System.Int32> id_tipo_servicio { get; set; }
        public string tipo_servicio { get; set; }
        public string nombre_punto { get; set; }
        public string nombre_punto1 { get; set; }

    }
}