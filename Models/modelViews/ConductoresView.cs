﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ConductoresView
    {
        public int id_conductor { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public Nullable<System.DateTime> fecha_ingreso { get; set; }
        public string tipo_licencia1 { get; set; }
        public int id_franquiciado{ get; set; }
        public Nullable<System.DateTime> fecha_vencimientolic { get; set; }


        public string estado { get; set; }

        public IEnumerable<ConductoresRestriccionesView> restricciones { get; set; }
    }
}