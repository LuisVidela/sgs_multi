﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class HerramientasView
    {
        public int id_herramienta { get; set; }
        public string herramienta { get; set; }
        public string camion { get; set; }
    }
}