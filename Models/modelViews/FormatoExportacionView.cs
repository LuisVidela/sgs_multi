﻿namespace SGS.Models.modelViews
{
    public class FormatoExportacionView
    {
        public string TipoReporte { get; set; }
        public string Extencion { get; set; }
        public string TipoAplicacion { get; set; }
    }
}