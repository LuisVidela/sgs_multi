﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ConductoresRestriccionesView
    {
        public int id_restriccion { get; set; }
        public string cliente { get; set; }
        public string motivo { get; set; }
    }
}