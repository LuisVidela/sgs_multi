﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class SucursalesView
    {
        public int id_sucursal { get; set; }
        public string sucursal { get; set; }
        public string direccion { get; set; }
        public string razon_social { get; set; }
        public string nombre_zona { get; set; }
        public string nombre_categoria { get; set; }
    }
}