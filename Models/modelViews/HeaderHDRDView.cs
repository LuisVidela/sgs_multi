﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class HeaderHDRDView
    {

        public int id_hoja_ruta { get; set; }
        public string franquiciado { get; set; }
        public string rut_conductor { get; set; }
        public string patente { get; set; }
        public string num_interno { get; set; }
        public string tipo_servicio { get; set; }
        public DateTime fecha { get; set; }
        public Boolean? ocultar_llenado { get; set; }
        public Boolean? ocultar_densidad { get; set; }
        public Boolean? ocultar_peso_calc { get; set; }
        public String texto_valor { get; set; }
    }
}