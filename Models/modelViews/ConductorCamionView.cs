﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace SGS.Models.modelViews
{
    public class ConductorCamionView    {

        public int id_hoja_ruta { get; set; }
        public Nullable<int> id_conductor { get; set; }
        public Nullable<int> id_camion { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public string camion { get; set; }
        public string modelo_camion { get; set; }
        public string patente { get; set; }

    }
}