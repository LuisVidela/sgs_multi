﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SGS.Models.modelViews
{
    public class FranquiciadosProcesosView
    {
        public List<FranquiciadosProcesosTabla> tabla {get;set;}
        public selectListPeriodos periodo { get; set; }
        public bool puedePrePagar { get; set; }
        public bool puedePagar { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string periodoPagar { get; set; }
        public selectListMes mes { get; set; }
        public selectListAño año { get; set; }
    }
    public class selectListPeriodos
    {
        public Nullable<int> periodoSelect { get; set; }
        public IEnumerable<SelectListItem> periodos { get; set; }
    }
    public class franquiciadosPagadosView
    {
        public selectListFranquiciadosFiltro selecListFranquiciados { get; set; }
        public selectListPeriodos selecListPeriodos { get; set; }
        public List<franquiciadosPagadosTabla> tabla { get; set; }
    }
    public class franquiciadosPagadosTabla
    {
        public int idPago { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string montoNeto { get; set; }
        public string montoBruto { get; set; }
        public string franquiciado { get; set; }
        public int idFranquiciado { get; set; }
    }
    public class excelFranquiciadoPrePago
    {
        public List<camionExcelFranquiciadoPago> hojaCamion { get; set; }
        public camionExcelFranquiciadoPago hojaTotal { get; set; }
        public List<hojaDetalleCamionExcelPrePago> hojaDetalleCamion { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string franquiciado { get; set; }
        public string rutFranquiciado { get; set; }
        public int anticipo { get; set; }
    }
    public class camionExcelFranquiciadoPago
    {
        public double valorUF { get; set; }
        public string patenteCamion { get; set; }
        public int HRcantidadTotal { get; set; }
        public int HRNetoTotal { get; set; }
        public int HRIvaTotal { get; set; }
        public string periodo { get; set; }
        public int HRTotal { get; set; }
        public List<tablaHojasDeRutaPorTipoExcel> tablaHR { get; set; }
        public List<tablaLeasingPrePagoExcel> tablaLeasing { get; set; }
        public int leasingNeto { get; set; }
        public int leasingIva { get; set; }
        public int leasingTotal { get; set; }
        public int subTotalNeto { get; set; }
        public List<tablaDescuentoPrePagoExcel> tablaDescuentos { get; set; }
        public int descuentoNeto { get; set; }
        public int descuentoIva { get; set; }
        public int descuentoTotal { get; set; }
        public int totalDescuentosTotal { get; set; }
        public int totalDescuentosIva { get; set; }
        public int totalDescuentosNeto { get; set; }
        public int retencionNeto { get; set; }
        public int retencionIva { get; set; }
        public int retencionTotal { get; set; }
        public int totalNeto { get; set; }
        public int totalIva { get; set; }
        public int total { get; set; }
        public int subTotalIva { get; set; }
        public int subTotal { get; set; }
    }
    public class hojaDetalleCamionExcelPrePago
    {
        public string patenteCamion { get; set; }
        public List<tablaDetalleCamionExcelPrePago> tabla { get; set; }
        public int total { get; set; }
    }
    public class tablaDetalleCamionExcelPrePago
    {
        public int numero { get; set; }
        public int id { get; set; }
        public string cliente { get; set; }
        public int tarifa { get; set; }
        public string fecha { get; set; }
        public string trama { get; set; }
        public string conductor { get; set; }
        public string estado{get;set;}
        public string tipo { get; set; }
    }
    public class tablaDescuentoPrePagoExcel
    {
        public int idTipoDescuento { get; set; }
        public string oc { get; set; }
        public string factura { get; set; }
        public string concepto { get; set; }
        public int cuotaActual { get; set; }
        public string cuotaTotal { get; set; }
        public string llevaIVA { get; set; }
        public string fecha { get; set; }
        public string valorCuota { get; set; }
        public string valorPagar { get; set; }
        public string neto { get; set; }
        public string iva { get; set; }
        public string total { get; set; }
    }
    public class tablaLeasingPrePagoExcel
    {
        public string concepto { get; set; }
        public string patente { get; set; }
        public string periodo { get; set; }
        public int cuota { get; set; }
        public int cuotas { get; set; }
        public string valorUF { get; set; }
        public string valorCuota { get; set; }
        public string valorPagar { get; set; }
        public string neto { get; set; }
        public string iva { get; set; }
        public string total { get; set; }
    }
    public class tablaHojasDeRutaPorTipoExcel
    {
        public string tipo { get; set; }
        public int cantidad { get; set; }
        public double neto { get; set; }
        public int iva { get; set; }
        public int total { get; set; }
    }
    public class FranquiciadosProcesosTabla
    {
        public int id_franquiciado { get; set; }

        public double id_comuna { get; set; }

        public string razon_social { get; set; }
        public Nullable<DateTime> fecha_contrato { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public string direccion { get; set; }
        public string rut_franquiciado { get; set; }
        public bool tienePendientes { get; set; }
        public bool tieneHR { get; set; }
        public double montoNeto { get; set; }
        public double montoBruto { get; set; }
        public bool tieneHRenProceso { get; set; }
        public string ultimoPago { get; set; }
    }
    public class hrSinCerrarExcel
    {
        public List<tablaHRSinCerrarExcel> tabla { get; set; }
    }
    public class tablaHRSinCerrarExcel
    {
        public string idHoja { get; set; }
        public string fecha { get; set; }
        public string chofer { get; set; }
        public string patente { get; set; }
        public string franquiciado { get; set; }
    }
    public class selectListMes
    {
        public int mesSelect { get; set; }
        public IEnumerable<SelectListItem> mes { get; set; }
    }
    public class selectListAño
    {
        public int añoSelect { get; set; }
        public IEnumerable<SelectListItem> año { get; set; }
    }
}