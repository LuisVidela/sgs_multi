﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class NumeroServiciosView
    {
        public int id_calculo { get; set; }
        public Nullable<System.Int32> id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public string Fecha { get; set; }
        public string Sucursal { get; set; }
        public Nullable<System.Int32> Numero_Servicios { get; set; }

    }
}