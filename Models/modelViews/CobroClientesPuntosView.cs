﻿namespace SGS.Models.modelViews
{
    public class CobroClientesPuntosView
    {
        public int id_calculo { get; set; }
        public int? id_hoja_ruta { get; set; }
        public int? id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string Sucursal { get; set; }
        public string fechaFull { get; set; }
        public int id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public int? Numero_Retiros { get; set; }
        public decimal? Valor { get; set; }
        public decimal? Peso { get; set; }
        public int? Volteos { get; set; }
        public string Fecha { get; set; }
        public long? Orden { get; set; }
        public string tipo_modalidad { get; set; }
        public int? sorting { get; set; }
        public string glosa { get; set; }
        public string cobro_arriendo { get; set; }
        public string conductor { get; set; }
        public string camion { get; set; }
        public string como_se_calcula { get; set; }
        public puntos_servicio punto_servicio { get; set; }
        public int? codigo_externo { get; set; }
        public bool cobro_por_m3 { get; set; }
        public bool cobro_por_peso { get; set; }
    }

}