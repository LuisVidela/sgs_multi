﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntosServiciosView
    {
        public int id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public int id_contrato { get; set; }
        public int estado_punto { get; set; }

        public Boolean lunes { get; set; }
        public Boolean martes { get; set; }
        public Boolean miercoles { get; set; }
        public Boolean jueves { get; set; }
        public Boolean viernes { get; set; }
        public Boolean sabado { get; set; }
        public Boolean domingo { get; set; }

        public Nullable<int> id_equipo { get; set; }
        public string nombre_contacto { get; set; }
        public string email_contacto { get; set; }
        public string fono_contacto { get; set; }
        public string celular_contacto { get; set; }
        public Nullable<int> cant_equipos { get; set; }


        public Nullable<double> valor_tarifa { get; set; }
    }
}