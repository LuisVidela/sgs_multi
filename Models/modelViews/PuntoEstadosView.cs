﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoEstadosView
    {
        public int estado { get; set; }
        public string descripcion { get; set; }
    }
}