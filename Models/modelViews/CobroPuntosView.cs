﻿namespace SGS.Models.modelViews
{
    public class CobroPuntosView
    {
        public int id_calculo { get; set; }
        public int? id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public int? id_contrato { get; set; }
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string Sucursal { get; set; }
        public int? id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public int? Numero_Retiros { get; set; }
        public decimal? Valor { get; set; }
        public int? Iva { get; set; }
        public int? Neto { get; set; }
        public int? Peso { get; set; }
        public string empresa { get; set; }
        public string Fecha { get; set; }
        public int Yearis { get; set; }
        public int Monthis { get; set; }
        public int? Orden { get; set; }
        public string tipo_modalidad { get; set; }
        public int? id_sucursal { get; set; }
        public decimal? ValorTotalDisposicion { get; set; }
        public decimal? ValorTotal { get; set; }

    }
}