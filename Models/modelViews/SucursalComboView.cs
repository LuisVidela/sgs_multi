﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class SucursalComboView
    {
        public int id_sucursal { get; set; }
        public string sucursal { get; set; }
        public int? id_unidad {get; set;}

    }
}