﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoServicioDireView
    {
        public int id_contrato { get; set; }
        public int id_punto_servicio { get; set; }
        public string estado { get; set; }
        public int id_sucursal { get; set; }
        public string sucursal { get; set; }
        public int id_ciudad { get; set; }
        public int id_comuna { get; set; }
        public int id_pais { get; set; }
        public int id_region { get; set; }
        public int id_rubro { get; set; }
        public System.String zona { get; set; }
        public System.String ciudad { get; set; }
        public System.String nombre_comuna { get; set; }
        public System.String pais { get; set; }
        public System.String rubro { get; set; }

    }
}