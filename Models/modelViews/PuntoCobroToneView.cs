﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoCobroToneView
    {

        public Nullable<System.Int32> id_punto_servicio { get; set; }
        public int id_modelo { get; set; }
        public Nullable<System.Double> valor { get; set; }
        public int id_cliente { get; set; }
        public int id_contrato { get; set; }
        public int id_sucursal { get; set; }
        public int id_tipo_contrato { get; set; }

    }
}