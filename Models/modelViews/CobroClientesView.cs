﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace SGS.Models.modelViews
{
    public class CobroClientesView
    {
        public int id_pago { get; set; }
        public Nullable<System.Int32> id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public Nullable<System.Int32> Numero_Retiros { get; set; }
        public Nullable<System.Decimal> Valor { get; set; }
        public Nullable<System.Int32> Iva { get; set; }
        public Nullable<System.Int32> Neto { get; set; }
        public Nullable<System.Int32> Peso { get; set; }
        public string empresa { get; set; }
        public string Fecha { get; set; }
        public int Yearis { get; set; }
        public int Monthis { get; set; }
        public Nullable<System.Int32> ValorTotalDisposicion { get; set; }
        public Nullable<System.Decimal> ValorTotal { get; set; }
        public string Foo { get; set; }
        public Nullable<System.Int32> Cantidad { get; set; }
        public Nullable<System.Int32> Bano { get; set; }
    }
}