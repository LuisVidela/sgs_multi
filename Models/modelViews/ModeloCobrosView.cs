﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ModeloCobrosView
    {
        public List<tablaModeloCobrosView> tabla { get; set; }
    }
    public class tablaModeloCobrosView
    {
        public int idModelo { get; set; }
        public string nombreModelo { get; set; }
        public string nombrePuntoServicio { get; set; }
        public string tipoModelo { get; set; }
        public string tipoModalidad { get; set; }
        public Nullable<double> valor { get; set; }
        public string rango { get; set; }
        public string cliente { get; set; }
        public string tiposervicio { get; set; }
        public string tipopago { get; set; }
    }
    public class editarModeloCobrosView
    {
        public string nombreModelo { get; set; }
        public int idTipoModelo { get; set; }
        public int idPuntoServicio { get; set; }
        public int idTipoServicio { get; set; }
    }
}