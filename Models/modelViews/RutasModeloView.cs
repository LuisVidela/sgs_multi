﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class RutasModeloView
    {
        public int id_ruta { get; set; }
        public string nombre_ruta { get; set; }

        public IEnumerable<int> puntos { get; set; }

        public System.DateTime fecha_ruta { get; set; }

    }
}