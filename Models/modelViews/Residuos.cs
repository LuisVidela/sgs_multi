﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ResiduosView
    {
        public int id_residuo { get; set; }
        public string residuo { get; set; }
        public string um_residuo { get; set; }
        public Double densidad { get; set; }
        public int tiempo_acopio { get; set; }
    }
}