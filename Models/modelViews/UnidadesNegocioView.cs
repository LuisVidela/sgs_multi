﻿namespace SGS.Models.modelViews
{
    public class UnidadesNegocioView
    {
        public int id_unidad { get; set; }
        public string unidad { get; set; }
        public string estado { get; set; }
        public string empresa { get; set; }
        public string numero_resolucion { get; set; }
        public string represenante_legal { get; set; }
		public string rut_rep_legal { get; set; }
    }
}