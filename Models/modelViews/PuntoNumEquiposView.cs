﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntoNumEquiposView
    {
        public int id_punto_servicio { get; set; }
        public Nullable<System.Int32> cant_equipos { get; set; }

    }
}