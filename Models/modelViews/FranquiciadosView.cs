﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class FranquiciadosView
    {
        public int id_franquiciado { get; set; }
        
        public double id_comuna { get; set; }
       
        public string razon_social { get; set; }
        public Nullable<DateTime> fecha_contrato { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public string direccion { get; set; }
        public string rut_franquiciado { get; set; }
    }
}