﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class PuntosServicios_TipoServicio_View
    {
        public int id_punto_servicio { get; set; }
        public string nombre_punto { get; set; }
        public int id_contrato { get; set; }
        public int estado_punto { get; set; }
        public int id_tipo_contrato { get; set; }
        public int id_cliente { get; set; }
        public int id_sucursal { get; set; }
        public string estado_contrato { get; set; }
        public string nombre_tipo_contrato { get; set; }
        public string dias { get; set; }
    }
}