﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class ZonaView
    {

        public int id_zona { get; set; }

        public string nombre_zona { get; set; }



    }
    public class CrearZonaView
    {

        public string nombre_zona { get; set; }
        [Required(ErrorMessage = "Requerido")]
        [StringLength(1, ErrorMessage = "1 caracter", MinimumLength = 1)]
        
        public int id_zona { get; set; }

    }
}