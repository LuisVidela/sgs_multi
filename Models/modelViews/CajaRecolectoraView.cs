﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class CajaRecolectoraView
    {
        public int id_punto_servicio { get; set; }
        public Nullable<System.Int32> id_tipo_modalidad { get; set; }
    }
}