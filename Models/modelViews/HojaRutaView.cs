﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.modelViews
{
    public class HojaRutaView
    {
        public int id_hoja_ruta { get; set; }
        public int id_conductor { get; set; }
        public int id_camion { get; set; }
        public DateTime fecha { get; set; }
        public int id_tipo_servicio { get; set; }

    }
}