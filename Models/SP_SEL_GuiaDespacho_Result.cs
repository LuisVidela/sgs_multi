//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    
    public partial class SP_SEL_GuiaDespacho_Result
    {
        public System.Guid GUIA_TOKEN { get; set; }
        public string EGIA_NOMBRE { get; set; }
        public System.DateTime GUIA_FECHA { get; set; }
        public Nullable<System.DateTime> GUIA_FECHALLEGADA { get; set; }
        public int GUIA_IDENTIFICADOR { get; set; }
        public string GUIA_NOMBRE { get; set; }
        public string GUIA_PATENTE { get; set; }
        public string vertedero { get; set; }
    }
}
