//------------------------------------------------------------------------------
// <auto-generated>
//    Este código se generó a partir de una plantilla.
//
//    Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//    Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class descuento
    {
        public int id_descuento { get; set; }
        public Nullable<int> id_franquiciado { get; set; }
        public Nullable<int> id_tipo_descuento { get; set; }
        public Nullable<double> neto { get; set; }
        public Nullable<double> iva { get; set; }
        public Nullable<double> total { get; set; }
        public Nullable<int> estado_descuento { get; set; }
        public Nullable<int> id_camion { get; set; }
        public Nullable<System.DateTime> fecha { get; set; }
        public Nullable<int> cuotas { get; set; }
        public bool lleva_iva { get; set; }
        public string observacion { get; set; }
        public string oc { get; set; }
        public string numero_factura { get; set; }
        public bool en_uf { get; set; }
        public System.DateTime fecha_creacion { get; set; }
    
        public virtual franquiciado franquiciado { get; set; }
    }
}
