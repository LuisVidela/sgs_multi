﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.ShadowBox
{
    public class conductoresShadowBoxView
    {
        public int id_conductor { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public Nullable<System.DateTime> fecha_ingreso { get; set; }
        public string licencia { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public int id_franquiciado { get; set; }
        public Nullable<System.DateTime> fecha_vencimientolic { get; set; }

        public IEnumerable<ConductoresRestriccionesView> restricciones { get; set; }
    }
}