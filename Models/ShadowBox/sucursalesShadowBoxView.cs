﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.ShadowBox
{
    public class sucursalesShadowBoxView
    {
        public string sucursal { get; set; }
        public string descripcion { get; set; }
        public string direccion { get; set; }
        public string estado_sucursal { get; set; }
        public string contacto_sucursal { get; set; }
        public string email_sucursal { get; set; }
        public Nullable<double> fono_sucursal { get; set; }
        public Nullable<double> periodo_desde { get; set; }
        public Nullable<double> periodo_hasta { get; set; }
        public string zona { get; set; }
        public string rubro { get; set; }
        public string cliente { get; set; }
        public string pais { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
        public string comuna { get; set; }
    }
}