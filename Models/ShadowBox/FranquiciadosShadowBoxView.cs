﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SGS.Models.modelViews;

namespace SGS.Models.ShadowBox
{
    public class FranquiciadosShadowBoxView
    {
        public int id_franquiciado { get; set; }
        public string razon_social { get; set; }
        public string rut_franquiciado { get; set; }
        public string observacion { get; set; }
        public string direccion { get; set; }  

        


        public IEnumerable<FranquiciadosDescuentosView> descuentos { get; set; }
    }
}