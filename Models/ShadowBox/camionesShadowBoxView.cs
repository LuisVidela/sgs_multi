﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.ShadowBox
{
    public class camionesShadowBoxView
    {
        public string camion { get; set; }
        public string patente { get; set; }
        public string tipo_camion { get; set; }
        public string modelo_camion { get; set; }
        public string capacidad_camion { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }


        public string nombre_conductor { get; set; }
        public string tipo_servicio1 { get; set; }
        public string marca { get; set; }

        public IEnumerable<restriccion_camion_cliente> restricciones { get; set; }
    }
}