﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.excelView
{
    public class CamionesExcelView
    {
        public int id_camion { get; set; }
        public string camion { get; set; }
        public string patente { get; set; }
        public string tipo_camion { get; set; }
        public string modelo_camion { get; set; }
        public string capacidad_camion { get; set; }
        public string marca { get; set; }
        public string estado { get; set; }
        public string observacion { get; set; }
        public int id_camion_marca { get; set; }
        public int id_conductor { get; set; }
        public int id_tipo_servicio { get; set; }
        public string nombre_conductor { get; set; }
        public string rut_conductor { get; set; }
        public Nullable<System.DateTime> fecha_ingreso { get; set; }
        public int  id_tipo_licencia { get; set; }
        public string tipo_servicio1 { get; set; }
    }
}