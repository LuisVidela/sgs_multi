﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SGS.Models.excelView
{
    public class ClientesExcelView
    {
        public int id_cliente { get; set; }
        public string rut_cliente { get; set; }
        public string razon_social { get; set; }
        public string nombre_fantasia { get; set; }
        public string direccion_cliente { get; set; }
        public Nullable<int>  id_pais { get; set; }
        public Nullable<double> fono_comercial { get; set; }
        public string giro_comercial { get; set; }
        public string nombre_contacto { get; set; }
        public string email_contacto { get; set; }
        public Nullable<double> fono_contacto { get; set; }
        public Nullable<double> celular_contacto { get; set; }
        public string unidad { get; set; }
        public string region { get; set; }
        public string ciudad { get; set; }
        public string pais { get; set; }
        public string comuna { get; set; }
        public Nullable<int>  id_unidad { get; set; }
        public Nullable<int>  id_region { get; set; }
        public Nullable<int>  id_ciudad { get; set; }
        public Nullable<int>  id_comuna { get; set; }
        public string categoria { get; set; }
    }
}