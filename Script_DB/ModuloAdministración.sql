--**********************************************************************
-- crea campo descripción en la tabla webpage_Roles
--**********************************************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_Roles ADD
	description varchar(500) NULL
GO
ALTER TABLE dbo.webpages_Roles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


--**********************************************************************
-- elimina vinculación entre la tabla webpages_UsersInRoles y UserProfile
-- crea vinculo entre la tabla webpages_UsersInRoles y perfiles
--**********************************************************************
BEGIN TRANSACTION
GO
ALTER TABLE dbo.perfiles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles
	DROP CONSTRAINT fk_UserId
GO
ALTER TABLE dbo.UserProfile SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.webpages_UsersInRoles ADD CONSTRAINT
	FK_webpages_UsersInRoles_perfiles FOREIGN KEY
	(
	UserId
	) REFERENCES dbo.perfiles
	(
	id_perfil
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.webpages_UsersInRoles SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
