--------------------------------------------
--agraga campo densidad a la hoja ruta detalle
--------------------------------------------
BEGIN TRANSACTION
GO
ALTER TABLE dbo.hoja_ruta_detalle ADD
	densidad float(53) NULL
GO
ALTER TABLE dbo.hoja_ruta_detalle SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--------------------------------------------
--Corrige equipo
--------------------------------------------

ALTER VIEW [dbo].[V_EQUIPO]
AS
SELECT  pto.id_punto_servicio ,
        pto.id_equipo ,
        equip.tipo_equipo + ' ' + CAST(equip.capacidad_equipo AS VARCHAR(10)) AS Equipo ,
        equip.nom_nemo
FROM    dbo.puntos_servicio AS pto
        INNER JOIN dbo.equipos AS equip ON pto.id_equipo = equip.id_equipo

GO

--------------------------------------------
--actualiza la lista de servicio
--------------------------------------------


-- =============================================
-- Author:		Jonathan Anc�n
-- Create date: 13-08-2015
-- Description:	Obtiene Lista de Servicios
-- =============================================
-- SP_SEL_ListaServicio '2015-09-01', '2015-09-10', 2
-- Modificaci�n: 30-09-2015
-- Modif. Por: Luis Nu�ez
-- Se agrega ((equi.capacidad_equipo * Cast(porc.PORL_PORCERTAJELLENADO as varchar(4)))/100) volumen para calculo de volumen real del retiro
-- =============================================
-- Modificaci�n: 23-10-2015
-- Modif. Por: jonathan Anc�n
-- Descripci�n: se modifica la dencidad por mejora de historificaci�n de densidad.
-- =============================================
ALTER PROCEDURE [dbo].[SP_SEL_ListaServicio]
    @FechaDesde DATETIME ,
    @fechaHasta DATETIME ,
    @codigo_unidad INT
AS
    BEGIN
        SET NOCOUNT ON;

        SELECT  hoja.fecha ,
                hoja.id_hoja_ruta folio ,
                tmod.tipo_modelo servicio ,
                cond.nombre_conductor conductor ,
                cami.patente ,
                equi.tipo_equipo tipoContenedor ,
                CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) + ' %' nivelLlenado ,
                ISNULL(( ( equi.capacidad_equipo
                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) ) / 100 ), 0) volumen --equi.capacidad_equipo volumen
                ,
                resi.residuo residuo
			 --, sucu.sucursal origen
                ,
                punt.nombre_punto origen ,
                vert.vertedero destino ,
                CASE WHEN deta.peso_calculado = 1 THEN 'Peso Calculado'
                     ELSE 'Pesaje Directo'
                END tipoPesaje ,
                ISNULL(CASE WHEN deta.peso_calculado = 1 THEN NULL
                     ELSE CASE WHEN porc.PORL_PORCERTAJELLENADO > 0
                               THEN deta.peso
                                    / ( ( CONVERT(FLOAT, porc.PORL_PORCERTAJELLENADO)
                                          / 100 ) * equi.capacidad_equipo )
                               ELSE 0
                          END
                END, 0) pesajeDirecto ,
                ISNULL(CASE WHEN deta.densidad IS NULL
                     THEN CASE WHEN deta.peso_calculado = 1 THEN resi.densidad
                               ELSE deta.peso / ( ( equi.capacidad_equipo
                                                    * CAST(porc.PORL_PORCERTAJELLENADO AS VARCHAR(4)) )
                                                  / 100 )
                          END
                     ELSE deta.densidad
                END,0) densidad ,
                CONVERT(FLOAT, ISNULL(deta.peso, 0.0)) peso
        FROM    hoja_ruta hoja
                INNER JOIN hoja_ruta_detalle deta ON hoja.id_hoja_ruta = deta.id_hoja_ruta
                INNER JOIN puntos_servicio punt ON punt.id_punto_servicio = deta.id_punto_servicio
                INNER JOIN contratos cont ON cont.id_contrato = punt.id_contrato
                INNER JOIN sucursales sucu ON sucu.id_sucursal = cont.id_sucursal
                LEFT JOIN modelo_cobro mode ON mode.id_punto_servicio = punt.id_punto_servicio
                LEFT JOIN tipo_modelo tmod ON tmod.id_tipo_modelo = mode.id_tipo_modelo
                INNER JOIN residuos resi ON resi.id_residuo = punt.id_residuo
                INNER JOIN camiones cami ON cami.id_camion = hoja.id_camion
                INNER JOIN conductores cond ON cond.id_conductor = hoja.id_conductor
                INNER JOIN equipos equi ON equi.id_equipo = punt.id_equipo
                LEFT JOIN TB_SGS_PORCENTAJELLENADO porc ON porc.PORL_CODIGO = deta.PORL_CODIGO
                INNER JOIN vertederos vert ON vert.id_vertedero = deta.relleno
        WHERE   deta.estado = 2
                AND cont.id_unidad = @codigo_unidad
                AND ( hoja.fecha >= @FechaDesde
                      AND hoja.fecha <= @fechaHasta
                    )
    END
--------------------------------------------
--Vincula unidad de negocio con residuo
--------------------------------------------
BEGIN TRANSACTION
GO
ALTER TABLE dbo.unidades_negocio SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.residuos ADD
	id_unidad int NULL
GO
ALTER TABLE dbo.residuos ADD CONSTRAINT
	FK_residuos_unidades_negocio FOREIGN KEY
	(
	id_unidad
	) REFERENCES dbo.unidades_negocio
	(
	id_unidad
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.residuos SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

--------------------------------------------
--vincula los residuos con la unidad de negocio arauco horcones
--------------------------------------------
UPDATE dbo.residuos
  SET id_unidad = 2
  GO
  


--------------------------------------------
--modifica campo id_unida de la tabla residuos para que no acepte nulos
--------------------------------------------
BEGIN TRANSACTION
GO
ALTER TABLE dbo.residuos
	DROP CONSTRAINT FK_residuos_unidades_negocio
GO
ALTER TABLE dbo.unidades_negocio SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.residuos
	DROP CONSTRAINT DF_residuos_densidad
GO
ALTER TABLE dbo.residuos
	DROP CONSTRAINT DF_residuos_tiempo_acopio
GO
CREATE TABLE dbo.Tmp_residuos
	(
	id_residuo int NOT NULL IDENTITY (1, 1),
	residuo varchar(100) NOT NULL,
	um_residuo varchar(20) NOT NULL,
	densidad float(53) NOT NULL,
	tiempo_acopio int NOT NULL,
	id_unidad int NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_residuos SET (LOCK_ESCALATION = TABLE)
GO
ALTER TABLE dbo.Tmp_residuos ADD CONSTRAINT
	DF_residuos_densidad DEFAULT ((0)) FOR densidad
GO
ALTER TABLE dbo.Tmp_residuos ADD CONSTRAINT
	DF_residuos_tiempo_acopio DEFAULT ((0)) FOR tiempo_acopio
GO
SET IDENTITY_INSERT dbo.Tmp_residuos ON
GO
IF EXISTS(SELECT * FROM dbo.residuos)
	 EXEC('INSERT INTO dbo.Tmp_residuos (id_residuo, residuo, um_residuo, densidad, tiempo_acopio, id_unidad)
		SELECT id_residuo, residuo, um_residuo, densidad, tiempo_acopio, id_unidad FROM dbo.residuos WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_residuos OFF
GO
ALTER TABLE dbo.puntos_servicio
	DROP CONSTRAINT fk_residuo
GO
DROP TABLE dbo.residuos
GO
EXECUTE sp_rename N'dbo.Tmp_residuos', N'residuos', 'OBJECT' 
GO
ALTER TABLE dbo.residuos ADD CONSTRAINT
	PK__residuos__68BDC6126A30C649 PRIMARY KEY CLUSTERED 
	(
	id_residuo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX idx_residuo ON dbo.residuos
	(
	residuo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX idx_um_residuo ON dbo.residuos
	(
	um_residuo
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE dbo.residuos ADD CONSTRAINT
	FK_residuos_unidades_negocio FOREIGN KEY
	(
	id_unidad
	) REFERENCES dbo.unidades_negocio
	(
	id_unidad
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.puntos_servicio WITH NOCHECK ADD CONSTRAINT
	fk_residuo FOREIGN KEY
	(
	id_residuo
	) REFERENCES dbo.residuos
	(
	id_residuo
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.puntos_servicio SET (LOCK_ESCALATION = TABLE)
GO
COMMIT