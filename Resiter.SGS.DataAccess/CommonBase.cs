﻿using System;

// references to application block namespace(s) for these examples
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Resiter.SGS.DataAccess
{
    public class CommonBase
    {
        protected DatabaseProviderFactory factoryDb;
        protected Database db;

        public CommonBase()
        {
            factoryDb = new DatabaseProviderFactory();
            db = factoryDb.Create("DefaultConnection");
        }

        public Exception Exception { get; set; }

        public bool HasError 
        {
            get
            {
                return Exception != null;
            }
        }

        public void ClearException()
        {
            Exception = null;
        }

    }
}
