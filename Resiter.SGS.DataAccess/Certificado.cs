﻿using System;
using System.Data.Common;
using System.Data;

namespace Resiter.SGS.DataAccess
{
    public class Certificado : CommonBase
    {
        private const string SpCertificadoCabecera = "SP_SEL_Mostrar_Certificado";
        private const string SpCertificadoDetalle = "SP_SEL_Mostrar_Certificado_det";
        private const string SpValidaCertificado = "SP_SEL_validar_certificado";


        /// <summary>
        /// Busca la cabecera de certificado en base de datos
        /// </summary>
        /// <param name="rutCliente">Rut de cliente</param>
        /// <param name="razonSocial">Nombre Razón social</param>
        /// <param name="sucursal">Nombre Sucursal</param>
        /// <param name="fechaHasta">Fecha fin período</param>
        /// <param name="vertedero">Nombre Vertedero</param>
        /// <param name="codigoUnidad">Código ID de unidad</param>
        /// <param name="idPuntoServicio">Id de punto de servicio escogido. Null en caso de no escoger ninguno</param>
        /// <returns>Listado de cabecera de certificado</returns>
        public DataTable GetHeader(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, string vertedero, int? codigoUnidad, int? idPuntoServicio)
        {
            DataTable dt;
            try
            {
                using (DbConnection conn = db.CreateConnection())
                {
                    try
                    {
                        conn.Open();
                        IDataReader dr = db.ExecuteReader(SpCertificadoCabecera, rutCliente, razonSocial, sucursal, fechaHasta, vertedero, codigoUnidad, idPuntoServicio);
                        dt = new DataTable();
                        dt.Load(dr);
                        ClearException();
                    }
                    catch (Exception ex)
                    {
                        Exception = ex;
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exception = ex;
                return null;
            }

            return dt;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutCliente"></param>
        /// <param name="razonSocial"></param>
        /// <param name="sucursal"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="endDate"></param>
        /// <param name="vertedero"></param>
        /// <param name="codigoUnidad"></param>
        /// <returns></returns>
        public DataTable GetDetail(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, DateTime? endDate, string vertedero, int? codigoUnidad)
        {
            DataTable dt;
            try
            {
                using (DbConnection conn = db.CreateConnection())
                {
                    try
                    {
                        conn.Open();
                        IDataReader dr = db.ExecuteReader(SpCertificadoDetalle, rutCliente, razonSocial, sucursal, fechaHasta, endDate, vertedero, codigoUnidad);
                        dt = new DataTable();
                        dt.Load(dr);
                        ClearException();
                    }
                    catch (Exception ex)
                    {
                        Exception = ex;
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exception = ex;
                return null;
            }
            return dt;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="rutCliente"></param>
        /// <param name="razonSocial"></param>
        /// <param name="sucursal"></param>
        /// <param name="fechaHasta"></param>
        /// <param name="vertedero"></param>
        /// <param name="codigoUnidad"></param>
        /// <returns></returns>
        public DataTable ValidaCertificado(string rutCliente, string razonSocial, string sucursal, DateTime? fechaHasta, string vertedero, int? codigoUnidad)
        {
            DataTable dt;
            try
            {
                using (DbConnection conn = db.CreateConnection())
                {
                    try
                    {
                        conn.Open();
                        IDataReader dr = db.ExecuteReader(SpValidaCertificado, rutCliente, razonSocial, sucursal, fechaHasta, vertedero, codigoUnidad);
                        dt = new DataTable();
                        dt.Load(dr);
                        ClearException();
                    }
                    catch (Exception ex)
                    {
                        Exception = ex;
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                Exception = ex;
                return null;
            }
            return dt;
        }
    }
}
