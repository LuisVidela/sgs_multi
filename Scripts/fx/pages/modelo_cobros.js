﻿var modelo=null;
var servicio = null;
var id_tipo_servicio = null;
var id_tipo_modelo = null;
var punto_servicio = null;
var sel_punto = false;
var submit = null;
var punto_servicio_value = null;
var formulario = $("#formulario");



$(document).ready(function () {

    var _modelo = $("#modelo");
    modelo = _modelo;

    var _servicio = $("#punto_servicio");
    servicio = _servicio;

    var _id_tipo_servicio = $("#id_tipo_servicio");
    id_tipo_servicio = _id_tipo_servicio;

    var _id_tipo_modelo = $("#id_tipo_modelo");
    id_tipo_modelo = _id_tipo_modelo;

    var _punto_servicio = $("#punto_servicio");
    punto_servicio = _punto_servicio;

    var _submit = $(".submit");
    submit = _submit;

    var _punto_servicio_value = $("#punto_servicio_value");
    punto_servicio_value = _punto_servicio_value;


    initialize();
    eventos();

});



function initialize() {

    var _ts = $("#id_tipo_servicio").val();

    if (punto_servicio_value != "") {

        sel_punto = true;

    }

    punto_servicio.autocomplete({
        source: function (request,response) {

            callAjax("/ModeloCobros/listForAutocomplete", function (data) {
                response(data);

            },"tipo_servicio="+id_tipo_servicio.val()+"&search="+request.term,"json");

        },
        minLength: 2,
        autoFocus:true,
        select: function (event, ui) {
            var id = ui.item.id;
            punto_servicio_value.val(ui.item.id);
            sel_punto = true;
            event.stopPropagation();
           
           
        }
    });
}



function eventos() {


    id_tipo_modelo.change(function () {


        //a espera de proxima implementacion

    });


    id_tipo_servicio.change(function () {


        punto_servicio.val("");
        punto_servicio_value.val("");


    });


    punto_servicio.keyup(function () {

        punto_servicio_value.val("");

    });

    submit.click(function () {

        if ( punto_servicio_value.val()=="") {
            alert("no ha seleccionado un punto valido");
            return;
        }

        if (modelo.val() == "") {
            alert("el campo modelo no puede estar vacio ");
            return;
        }


        formulario.submit();

    });



}


