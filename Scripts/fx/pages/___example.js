$(document).ready(function(){
    
    $('.datatable tfoot th').each( function () {
        var title = $('.datatable thead th').eq( $(this).index() ).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
    
    
    
	$('.datatable').DataTable({
                "processing": true,
		"serverSide": true,
		"ajax": "Documentos/load",
		"sDom": "<'row'<'col-lg-6'l><'col-lg-6'f>r>t<'row'<'col-lg-12'i><'col-lg-12 center'p>>",
		"sPaginationType": "bootstrap",
		"oLanguage": {
			"sLengthMenu": "_MENU_ records per page"
		}
	});
        
});



