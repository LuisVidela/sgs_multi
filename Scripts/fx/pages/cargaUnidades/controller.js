﻿
$(document).ready(function () {


    var sheetNames = $("#sheetNames");
    var hidden = $("hidden#archivo");
    var idx_unidad = $("select#unidades").val();


    $("select#unidades").change(function () {


        idx_unidad=$(this).val();
    });

    $(".subir").on("click", function () {

        $("#file").upload("/cargaUnidades/cargaArchivo", function (success) {

            
            
            sheetNames.html("");

            $.each(success, function (index, el) {

                sheetNames.append("<option value='" + el + "'>" + el + "</option>");   
            });

            $("#sheetNames-container").show(200);
            $(".subir").removeClass("subir")
                       .removeClass("btn-primary")
                       .addClass("cargar")
                       .addClass("btn-success")
                       .html("Cargar Unidad")
                       .unbind("click")
                       .click(function () {
                           $.ajax({
                               url: "/cargaUnidades/Procesar",
                               type: "HTML",
                               method: "POST",
                               data:{ hoja:sheetNames.val(),unidad:idx_unidad}
                           })
                       });

        },$("#prog"));

    });


});