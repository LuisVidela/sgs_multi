﻿/*!
* Valentys Javascript Client Framework
* http://www.valentys.com
*
* Copyright 2012, David Antonio Muñoz Gaete
*/

$.namespace("Fwk");

/* -----------------------------------
Framework Format Library
----------------------------------- */
Fwk.Format = function () { };
Fwk.Format.prototype = {
    get_selector: function () {

        return '[data-format]';
    },

    /* -----------------------------------
    Format Initialization
    ----------------------------------- */
    _initialize: function (elms) {
        //JAVASCRIPT UNOBSTRIVE FORMAT'S LOADER
        elms.each(function () {
            var format = $(this).data('format');

            var operator = Fwk.Format.Formats[format];
            if (operator) {
                operator.hook($(this));
            }
        });
    }
};
Fwk.Format.Formats = {
    letters: "abcdefghijklmnopkrstuvwxyz",
    numbers: "1234567890",
    getValue: function (el) {
        return $(el).get(0).nodeName.toLowerCase() == "input" ? $(el).val() : $(el).html();
    },
    setValue: function (el, value) {
        var isInput = $(el).get(0).nodeName.toLowerCase() == "input"
        if (isInput) {
            $(el).val(value);
        } else {
            $(el).html(value);
        }
    },
    specialKeyCode: [
                8, //DELETE
                9, //TAB
                16, //SHIFT
                32, //SPACE
                27, //ESCAPE
                18, //ALT
                13, //RETURN
                17, //CTRL
                37, //ARROW LEFT <
                39, //ARROW RIGHT >
                38, //ARROW UP
                40, //ARROW DOWN
                36, //START
                35, //FIN
                46  //SUPR
            ],

    //Number Format
    number: {
        hook: function (target, args) {

            target.bind({
                keydown: function (e) {

                    //if is an action keyCode , let them pass
                    if ($.inArray(e.which, Fwk.Format.Formats.specialKeyCode) >= 0) {
                        return true;
                    }

                    // Ensure that it is a number and stop the keypress
                    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                        return false;
                    } else {
                        //If a special key is pressed , don't let them pass because the combination is dangerous :P
                        if (e.shiftKey || e.ctrlKey || e.altKey) {
                            return false;
                        }
                    }
                },
                focus: function (e) {
                    var t = this;
                    setTimeout(function () {
                        try {
                            t.select();
                        } catch (e) { }
                    }, 50);
                }
            });
        }
    },

    decimal: {
        hook: function (target, args) {

            target.bind({
                keydown: function (e) {

                    //if is an action keyCode , let them pass
                    if ($.inArray(e.which, Fwk.Format.Formats.specialKeyCode) >= 0) {
                        return true;
                    }

                    //188: Comma
                    // Ensure that it is a number and stop the keypress
                    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105) && e.keyCode !== 188) {
                        return false;
                    } else {
                        //If a special key is pressed , don't let them pass because the combination is dangerous :P
                        if (e.shiftKey || e.ctrlKey || e.altKey) {
                            return false;
                        }
                    }
                },
                focus: function (e) {
                    var t = this;
                    setTimeout(function () {
                        try {
                            t.select();
                        } catch (e) { }
                    }, 50);
                }
            });
        }
    },

    currency: {
        hook: function (target, args) {
            target.bind({
                keydown: function (e) {

                    //if is an action keyCode , let them pass
                    if ($.inArray(e.which, Fwk.Format.Formats.specialKeyCode) >= 0) {
                        return true;
                    }

                    // Ensure that it is a number and stop the keypress
                    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                        return false;
                    } else {
                        //If a special key is pressed , don't let them pass because the combination is dangerous :P
                        if (e.shiftKey || e.ctrlKey || e.altKey) {
                            return false;
                        }
                    }
                },

                blur: function (e) {
                    var value = Fwk.Format.Formats.getValue(this);
                    if (value !== "" && value.indexOf(".") <= 0) {
                        Fwk.Format.Formats.setValue(this, accounting.formatMoney(value));
                    }

                },
                focus: function (e) {
                    var value = Fwk.Format.Formats.getValue(this);
                    if (value !== "") {
                        Fwk.Format.Formats.setValue(this, accounting.unformat(value, ","));
                    }
                    var t = this;
                    setTimeout(function () {
                        try {
                            t.select();
                        } catch (e) { }
                    }, 50);
                }
            });

            target.trigger("blur");
        }
    },

    percent: {
        hook: function (target, args) {
            target.bind({
                keydown: function (e) {

                    //if is an action keyCode , let them pass
                    if ($.inArray(e.which, Fwk.Format.Formats.specialKeyCode) >= 0) {
                        return true;
                    }

                    // Ensure that it is a number and stop the keypress
                    if ((e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
                        return false;
                    } else {
                        //If a special key is pressed , don't let them pass because the combination is dangerous :P
                        if (e.shiftKey || e.ctrlKey || e.altKey) {
                            return false;
                        }
                    }
                }
            });
        }
    },

    //Alpha Numeric Format
    alphanumeric: {
        hook: function (target, args) {
            target.bind({
                keydown: function (e) {


                    //if is an action keyCode , let them pass
                    if ($.inArray(e.which, Fwk.Format.Formats.specialKeyCode) >= 0) {
                        return true;
                    }

                    var key = String.fromCharCode(e.which).toLowerCase();
                    var pattern = /[0-9a-zA-Z]/ig;
                    if (!pattern.test(key)) {
                        return false;
                    } else {
                        //If a special key is pressed , don't let them pass because the combination is dangerous :P
                        if (e.ctrlKey || e.altKey) {
                            return false;
                        }
                    }
                }
            });
        }
    },

    //E-mail Format
    mail: {
        hook: function (target, args) {
            //TODO: Restrict Keys??
        }
    }
};
Fwk.registerModule("Fwk.Format");  //Framework Register