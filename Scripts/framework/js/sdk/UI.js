/*!
 * Valentys.SDK UI Commander 
 * Copyright 2011, David Antonio Muñoz Gaete (http://www.linkedin.com/pub/david-mu%C3%B1oz-gaete/25/2a1/a84)<
 * ---------------------------------------------------------------
 * Requires : JQUERY
 */
$.namespace("Valentys.SDK");

Valentys.SDK.UI = function() {

    // window's variables
    this._last_window = null;
    this._last_modal = null;
    this._modal = null;
    this._window = null;
    this._toast = null;
    this._confirm = null;

    // Constructor
    this._initialize();
}

Valentys.SDK.UI.prototype = {
    modal : function(settings) {
        var content = null;

        // Load content when has content
        var loadComplete = $.proxy(function() {
            var m =this._modal; 
            m.html("");
            m.append(content);
            m.modal("show");

        }, this);

        // apply css temporary item
        if (settings.css) {
            this._modal.css(settings.css);
        }

        // if remote , setting in other ways
        if (settings.remote) {

            var url = settings.remote.url;
		
            $page.loading.show();

            content =  $("<div />");
            content.load(url, settings.remote.data, $.proxy(function(data,status, xhr) {
                if (status == "error") {
					
                    Valentys.SDK.Log.trackError(xhr);
					
                } else {
                    loadComplete();
					
                    if (settings.onLoadComplete) {
                        settings.onLoadComplete(this._modal);
                    }
					
                    Fwk.triggerUpdate(); // Raise Update
                }
		
                $page.loading.hide();

            }, this));
			
			
			
            this._modal.attr("data-mode", "remote");
			
			
        } else {
            content = settings.content;
            loadComplete();
			
            this._modal.attr("data-mode", "content");
        }

        // Save the last modal
        this._last_modal = content;

        return this._modal;
    },

    window : function(element) {
        this._last_window = element;

        var ref = this;
        element.children().each(function(idx, elm) {
            ref._window.append(elm);
        });
        this._window.modal("show");

        return this._window;
    },

    confirm : function(settings) {
        var win = this._confirm;
        var w_title = win.find("[data-relation='confirm-title']:first");
        var w_body = win.find("[data-relation='confirm-body']:first");
        var w_ok = win.find("[data-relation='confirm-ok']:first");
        var w_cancel = win.find("[data-relation='confirm-cancel']:first");

        if (!settings.body) {
            Valentys.SDK.Log.trackError({
                message : $resx("E00101", "body")
            }, true);
        }
        if (!settings.onAction) {
            Valentys.SDK.Log.trackError({
                message : $resx("E00101", "onAction")
            }, true);
        }

        // Set title
        w_title.html($.format("<h3>{0}</h3>",
            (settings.title || $resx("T40006"))));

        // Set body
        w_body.html(settings.body);

        w_ok.unbind("click").bind("click", function(e) {
            try {
                var closeFunction = function() {
                    win.modal("hide");
                };

                w_ok.button("loading").attr("disabled", true);
                w_cancel.attr("disabled", true);

                settings.onAction("ok", closeFunction);
            } catch (e) {
                win.modal("hide");

                Valentys.SDK.Log.trackError(e);

            }
        });

        w_cancel.unbind("click").bind("click", function(e) {
            try {
                w_ok.button("loading").attr("disabled", true);
                w_cancel.attr("disabled", true);

                settings.onAction("cancel", function(){});
                
                win.modal("hide");
            } catch (e) {
                win.modal("hide");

                Valentys.SDK.Log.trackError(e);
            }
        });

        win.modal("show");

        return win;
    },

    toast : function(message, className) {
        var w = this._toast;
        var m = w.find(".toast-box");
        m.find(".message").html(message);

        clsName = className||"error";		
        $(m.children()[0]).attr("class", "alert alert-block algCenter pTop5 alert-" + clsName);
      
        w.show();
       
        m.effect("bounce", {
            times: 3
        }, 300);
        
        setTimeout(function(){
            w.hide();
        },3000);
        
		
    },

    _initialize : function() {
        var modal = this._modal = $("[data-relation='ui-modal-dialog']:first");
        var window = this._window = $("[data-relation='ui-window-dialog']:first");
        var toast = this._toast = $("[data-relation='ui-toast-dialog']:first");
        var confirm = this._confirm = $("[data-relation='ui-confirm-dialog']:first");

        var ref = this;
        var refuse = function() {
            return false
        };

        // When windows hide, re-append the element into the original element
        confirm.on("hidden", function() {
            var w_ok = confirm.find("[data-relation='confirm-ok']:first");
            var w_cancel = confirm
            .find("[data-relation='confirm-cancel']:first");
            w_ok.button("reset");
            w_cancel.removeAttr("disabled");
        });

        // Maximize the z-index to show the window at the top always
        confirm.on("show", function() {

            maxZindex = 999999999;

            $(confirm).css("z-index", maxZindex + 1)

            setTimeout(function() {
                $(confirm.data("modal").$backdrop).css("z-index", maxZindex)
            }, 0);

        });

        // ---[ REFUSE BIND's
        modal.bind("click", refuse);
        window.bind("click", refuse);
        confirm.bind("click", refuse);
        toast.bind("click", refuse);
		
        modal.bind("keydown",function(e){
            if(e.which == 13){
                e.stopPropagation();
                e.preventDefault();
                e.stopImmediatePropagation();
                return false;
            }
        })
		
    }
}

// Static Instance
Fwk.registerStatic('Valentys.SDK.UI');

$page.loading = {
    dialogid : "[data-type='loading-panel']:first",

    show : function(title) {
        $(this.dialogid).removeClass("hidden")
    },
    hide : function() {
        $(this.dialogid).addClass("hidden")
    }
}
