﻿/*!
* Valentys Javascript Core Client Framework
* http://www.valentys.com
*
* Copyright 2011, David Antonio Muñoz Gaete
*/

/* -----------------------------------
Framework Core Library
----------------------------------- */

var Fwk = {
    __firstLoad: true,
    __modules: [],
    __statics: [],

    _initialize: function (parent) {
        var container = $(parent || document.body);

        //MODULE INITIALIZATION
        for (var idx in Fwk.__modules) {
            var module = Fwk.__modules[idx];
            var reflectedClass = Fwk.__reflectClass(module);

            //STATIC Initialization of the Module (Singleton Pattern)
            if (!(reflectedClass.get_staticInstance)) {

                reflectedClass.__instance = new reflectedClass(); //Create the reflected class
                reflectedClass.get_staticInstance = function () {
                    return this.__instance;    //Return the unique instance of the static instance
                };
            }

            var instance = reflectedClass.get_staticInstance();
            var selector = instance.get_selector();
            if (!selector) {
                alert($.format("you must override the property \"get_selector\" in the class {0}", (typeof module)));
            }

            var mark = "data-initialize-" + idx;                                    //flag unique marked for the , validation , so when pass in this run , only the flag with the unique idx, is the one who wich pass again the rule
            var elements = container.find(selector).not($.format("[{0}]", mark));   //Find the elements who wich interact the instance Class and not yet initialized
            instance._initialize(elements);                                         //Process the elements in the class
            elements.attr(mark, true);                                              //Put the mark of overrides
        }

        if(Fwk.__firstLoad){
            //STATIC'S INITIALIZATION (ONLY FIRST LOAD)
            for (var idx in Fwk.__statics) {
                var _static = Fwk.__statics[idx];
                reflectedClass = Fwk.__reflectClass(_static);

                eval(_static + " = new reflectedClass();");	           
            }
        }
        
        if(Fwk.__firstLoad){
            //Call The page_load for initialization's
            if (typeof pageLoad != "undefined") {
                pageLoad();
            }
        }
        
        Fwk.__firstLoad = false;
    },

    __reflectClass: function (className) {
        var arr = className.split(".");
        var fn = (window || this);

        for (var i = 0, len = arr.length; i < len; i++) {
            fn = fn[arr[i]];
        }

        if (typeof fn !== "function") {
            alert($.format("La clase {0}, no existe", className));
        }
        
        return fn;
    },

    registerModule: function (instance) {
        Fwk.__modules.push(instance);
    },

    registerStatic: function(instance){
        Fwk.__statics.push(instance);
    },
    
    triggerUpdate: function (fragment) {
        Fwk._initialize(fragment);  //Force Update
    }
};

/* -----------------------------------
Framework Page Library
----------------------------------- */
Fwk.Page = {
    ajax: function (cfg) {
        var block = (cfg.block == undefined ? true : cfg.block);

        if (block) {
            $page.loading.show();
        }
        
        setTimeout(function () {
            //If data is not an object, try to evaluate;
            try{
                if(typeof cfg.data === "string"){
                    cfg.data = $.evalJSON(cfg.data)
                }
            }catch(ex){}
        	
            if(cfg.contentType == "application/json; charset=utf-8"){
                cfg.data =  $.toJSON(cfg.data||{});
            }else{
            	
                if(typeof cfg.data === "object"){
                    //------------------------------------------------------------------------
                    //----[ BODY Process ]
                    //In FORM , re-process the Json format, to send in the "POST" format method
                    var body = [];
                    var obj = cfg.data;
                    for(var name in obj){
                        body.push(name + "=" + obj[name]);
                    }
                    cfg.data = body.join('&');
                //------------------------------------------------------------------------
                }
            }
        	
            //------------------------------------------------------------------------
            var fnError = function (error) {
            	
                if (cfg.error) {
                    $.proxy(cfg.error, this)(error, true);
                } else {
                    Valentys.SDK.Log.trackError(error);
                }
                
                fnFinale(error,true);
            }
            //------------------------------------------------------------------------
            var fnFinale = function(data, hasError){
                if (block) {
                    $page.loading.hide();
                }
            	 
                if (cfg.finale) {
                    $.proxy(cfg.finale, this)(data, hasError);
                }
            }
            //------------------------------------------------------------------------
            
            var settings = {
                url: cfg.url,
                type: cfg.verb || 'POST',
                dataType: cfg.datatype || 'json',
                data: cfg.data,
                contentType: cfg.contentType || "application/x-www-form-urlencoded",
                success: function (data) {
                    var result = data;
                    var hasError = false;
                	 
                    //-----------------------------------------------
                    //Check if the data return with the valentys 
                    //format	[{errorCode: int, errorMessage: string, result:object}
                    if(typeof data.errorCode != "undefined"){
                        if(data.errorCode!="0"){	//has Error??
                            fnError({
                                message :data.errorMessage,
                                exception : {
                                    status : data.errorCode
                                }
                            });
                            hasError=true;
                        }
						
                        result = data.result;
						 
                        if(typeof result === "string"){
                            //try to evalute the valentys result to valid json
                            try{
                                result = $.evalJSON(result);
                            }catch(e){}
                        }
                    }
                    //-----------------------------------------------
                	 
                    if(!hasError){
                        if (cfg.success) {
                            $.proxy(cfg.success, this)(result);
                        }
                        fnFinale(result,false);
                    }
                },
                error: function (error) {
                    fnError(error);
                }
            };
        
            $.ajax(settings);
            
        }, 200);
    },

    loading: {
        dialogid: "[data-type='loading-panel']:first",

        show: function (title) {
            $(this.dialogid).dialog("open");
        },
        hide: function () {
            $(this.dialogid).dialog("close");
        }
    }
};
var $page = Fwk.Page;

/* -----------------------------------
Frmework Valentys Initialization
----------------------------------- */
(function () {
    //Re-initialize function
    var fn = function (parent) {
        Fwk._initialize(parent);  //Initialize FWK
    };

    $(function () {
        fn();

        //For Microsoft Update Panel
        if (typeof (Sys) !== 'undefined') {
            Sys.Application.add_load(function () {
                fn();
            });
        }
    });
})();

/* -----------------------------------
Frmework Valentys JQUERY Extension's
----------------------------------- */
$.goTo = function (url) {
    document.location.href = url;
};

$.format = function (text) {
    //check if there are two arguments in the arguments list
    if (arguments.length <= 1) {
        //if there are not 2 or more arguments there's nothing to replace
        //just return the text
        return text;
    }
    //decrement to move to the second argument in the array
    var tokenCount = arguments.length - 2;
    for (var token = 0; token <= tokenCount; ++token) {
        //iterate through the tokens and replace their placeholders from the text in order
        text = text.replace(new RegExp("\\{" + token + "\\}", "gi"), arguments[token + 1]);
    }
    return text;
};

$.toNumber = function (value) {
    return accounting.unformat( value ,accounting.settings.currency.decimal);
};

$.toCurrency = function (value){
    return accounting.formatMoney(value, accounting.settings.decimal);
}

$.namespace = function () {
    var a = arguments, o = null, i, j, d;
    for (i = 0; i < a.length; i = i + 1) {
        d = a[i].split(".");
        o = window;
        for (j = 0; j < d.length; j = j + 1) {
            o[d[j]] = o[d[j]] || {};
            o = o[d[j]];
        }
    }
    return o;
};

$.extendClass = function (tclass, superclass) {
    //var f = function () { };
    //f.prototype = sc.prototype;superclass.prototype[m]
    //bc.prototype = new f();
    //bc.prototype.constructor = bc;

    tclass.prototype["super"] = {};

    //Extending in base class the prototype 
    for (var m in superclass.prototype) {
        tclass.prototype["super"][m] = superclass.prototype[m]; //reference in super variable

        if (!(tclass.prototype[m])) {    //if not exist , add the function , otherwise the method can be accesed from "super".(method) command
            tclass.prototype[m] = superclass.prototype[m];
        }

        tclass.prototype["initializeBase"] = function () {
            var reference = arguments[0];   //instance
            var args = [];
            for (var index = 1; index < arguments.length; index++) {
                args.push(arguments[index]);
            }
            reference._constructor.apply(this, args);
        }
    }
}

$.fn.serializeObject = function () {
    var arrayData, objectData;
    arrayData = this.serializeArray();
    objectData = {};

    $.each(arrayData, function () {
        var value;

        if (this.value != null) {
            value = this.value;
        } else {
            value = '';
        }

        if (objectData[this.name] != null) {
            if (!objectData[this.name].push) {
                objectData[this.name] = [objectData[this.name]];
            }

            objectData[this.name].push(value);
        } else {
            objectData[this.name] = value;
        }
    });

    return objectData;
};



String.prototype.padLeft = function(width, replacement) {
    if (("" + this).length >= width) {	
        return "" + this;
    } else {
        return arguments.callee.call(  replacement + this, width, replacement||" ");
    }
};
String.prototype.padRight = function(width, replacement) {
    if ((this+"").length >= width) {	
        return this + "";
    } else {
        return arguments.callee.call( this + replacement,  width,  replacement||" ");
    }
};

