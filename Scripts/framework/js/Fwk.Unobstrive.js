﻿/*!
* Valentys Javascript Client Framework
* http://www.valentys.com
*
* Copyright 2012, David Antonio Muñoz Gaete
*/

$.namespace("Fwk");

/* -----------------------------------
Framework Unobstrive JQUERY Initialization
----------------------------------- */
Fwk.Unobstrive = function () {};
Fwk.Unobstrive.prototype = {
    get_selector: function () {
        return "[data-jq]";
    },

    _initialize: function (elms) {
        elms.each(function () {
            var className = $(this).data('jq');
            var options = {};
            var cls = $(this).data("options");
            if (cls) {
                var items = [];
                var tmp = "] [";
                if (cls.indexOf(tmp) > 0) {
                    cls = cls.substring(1, cls.length - 1);
                    items = cls.split(tmp);
                } else {
                    items.push(cls.substring(1, cls.length - 1));
                }

                $.each(items, function (index, text) {
                    var sf = ":";
                    var key = text.substring(0, text.indexOf(sf));
                    var value = text.substring(text.indexOf(sf) + 1);
                    switch (key) {
                        case "icons":
                            value = { primary: value };
                            break;
                        default:
                            try {
                                var evaluation = eval(value);
                                if (evaluation != undefined) {
                                    value = evaluation;
                                }
                            } catch (e) { };
                            break;
                    }
                    options[key] = value;
                });
            }
            $(this)[className](options);  //Run Extension
        });
    }
};
Fwk.registerModule("Fwk.Unobstrive");  //Framework Register